<?php

namespace App\Providers;

use Queue;
use DateTime;
use Carbon\Carbon;
use App\Models\Batch;
use App\Models\Survey;
use Illuminate\Support\ServiceProvider;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\JobProcessing;
use App\Jobs\SendReminderPostSurvey;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        header('Expires: Mon, 1 Jul 1998 01:00:00 GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0', FALSE);
        header('Pragma: no-cache');
        header( "Last-Modified: " . gmdate( "D, j M Y H:i:s" ) . " GMT" );

        Queue::after(function (JobProcessed $event) {
            $payload = $event->job->payload();
            $commandName = $payload['data']['commandName'];
            if($commandName === "App\Jobs\SendReminderPostSurvey") {
                $data = unserialize($payload['data']['command']);
                $batch = Batch::find($data->batch->id);
                $survey = Survey::find($data->survey->id);
                if(!$batch->is_done) {
                    $reminderType = $survey->type;
                    $reminderValue = $survey->value;

                    $startDate = new DateTime(date('Y-m-d H:i:s'));
                    $endDate = new DateTime(date('Y-m-d H:i:s', strtotime('+' . $reminderValue . ' ' . $reminderType)));
                    $result = $startDate->diff($endDate);
                    // $reminderInHours = $result->h;
                    $reminderInMinutes = $result->i;
                    // fixed_cmp_reminder checking
                    if ($survey->fixed_cmp_reminder == 0) {
                        $job = (new SendReminderPostSurvey($batch, $survey))
                            ->delay(Carbon::now()->addMinutes($reminderInMinutes));                        
                    }

                    dispatch($job);
                }    
            }
            
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
