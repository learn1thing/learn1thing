<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Helpers\RouteHelper;
use App\Models\UserSurvey;
use Client;

class Mail extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $userSurvey = $notifiable->batch->userSurvey;
        //$responseApi = Client::setEndpoint('getDataUser/'.$userSurvey->user_id)->get();
        //edited by puw k5 5/12/17
        $responseApi = Client::setEndpoint('getDataUser/'.$userSurvey->email)->get();
         \Log::info($userSurvey);

        if ($responseApi['status']['success'] == true) {
            $purchaserName = $responseApi['data']['first_name'] . ' ' . $responseApi['data']['last_name'];
        } else {
            $purchaserName = '';
        }

        return (new MailMessage)
                    ->subject($userSurvey->survey->title)
                    ->greeting($notifiable->full_name())
                    ->line($userSurvey->survey->title)
                    ->line($purchaserName)
                    // ->line($userSurvey->message_for_observer)
                    ->action('Click Here!!!', RouteHelper::check('surveys.preview', ['type' => 'batch', 'code' => $notifiable->batch ? $notifiable->batch->batch : '']))
                    ->line($notifiable->categoryProfile->name);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
