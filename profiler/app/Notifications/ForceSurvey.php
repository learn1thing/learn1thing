<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ForceSurvey extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $first_name = $notifiable->full_name();
        $batch = $notifiable->batch ? $notifiable->batch->batch : null;
        // return (new MailMessage)
        //     ->subject('Retake Survey')
        //     ->greeting("Hello {$first_name}")
        //     ->line('Please retake the survey that assingned to you.')
        //     ->action('Click Here!!!', \RouteHelper::check('surveys.preview', [
        //             'type' => 'batch'
        //             , 'code' => $batch
        //         ])
        //     );
        return (new MailMessage)
                    ->view('emails.retake-profiler', ['greeting' => $notifiable->full_name(), 'title_profiler' => $notifiable->batch->survey->title, 'actionUrl' => \RouteHelper::check('surveys.preview', ['type' => 'customer', 'code' => $notifiable->batch ? $notifiable->batch->batch : ''])])
                    ->subject('Retake Survey');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
