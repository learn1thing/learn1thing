<?php

namespace App\Helpers;

class FrontendHelper {

    public static function fullName()
    {
        if (session('user')) {
            return ucfirst(session('user')['first_name']) . ' ' . ucfirst(session('user')['last_name']);
        }

        return '-';
    }

    public static function link($icon, $url, $text)
    {
        return '<a href="' . $url . '"><i class="fa ' . $icon . '"></i> <span>' . $text . '</span></a>';
    }

    public static function email()
    {

    }

}