<?php

namespace App\Helpers;

class RouteHelper {

    public static function check($name, $attribute) {
        if (\Route::has($name)) {
            return route($name, $attribute);
        }

        return null;
    }

    public static function token($name, array $attribute = [])
    {
    	if (\Route::has($name)) {
            return route($name, array_merge([
                'authtoken' => request()->authtoken
            ], $attribute));
        }

        abort(404);
    }

}