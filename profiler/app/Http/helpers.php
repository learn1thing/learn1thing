<?php

function romanic_number($integer, $upcase = true)
{
	$table = array('M'=>1000, 'CM'=>900, 'D'=>500, 'CD'=>400, 'C'=>100, 'XC'=>90, 'L'=>50, 'XL'=>40, 'X'=>10, 'IX'=>9, 'V'=>5, 'IV'=>4, 'I'=>1);
	$return = '';
	while($integer > 0)
	{
		foreach($table as $rom=>$arb)
		{
			if($integer >= $arb)
			{
				$integer -= $arb;
				$return .= $rom;
				break;
			}
		}
	}

	return $return;
}

function arabic_number($number, $upcase = true)
{
	$list = array(
		'A' => 0,
		'B' => 1,
		'C' => 2,
		'D' => 3,
		'E' => 4,
		'F' => 5,
		'G' => 6,
		'H' => 7,
		'I' => 8,
		'J' => 9
		);
	$temp = '';
	$arr_num = str_split ($number);
	foreach($arr_num as $data)
	{
		$temp .= array_search($data,$list);
	}
	$result = $temp;
	return $result;
}