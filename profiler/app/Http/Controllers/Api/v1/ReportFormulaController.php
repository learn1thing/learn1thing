<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\Survey;
use App\Models\ReportFormula;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\v1\StoreReportFormulaRequest;
use App\Http\Requests\Api\v1\GetFormulasBySurveyRequest;

class ReportFormulaController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreReportFormulaRequest $request)
    {
        try {
            $surveyId = $request->input('survey_id');
            $formulaParams = $request->input('formula');
            $surveyTemp = array();
            for($i = count($formulaParams)-1; $i >= 0; $i--) {
                $formulaParams[$i]['survey_id'] = $surveyId;
                $index = $i;
                if(isset($surveyTemp[$i])) {
                    $formulaParams[$i]['reference_formula_id'] = $surveyTemp[$i]['id'];
                }
                if($i == 0) {
                    $formulaParams[$i]['is_parent'] = true;
                }
                $formula = ReportFormula::create($formulaParams[$i]);
                // print_r($formula->toArray());
                $surveyTemp[] = $formula->toArray();
            }
            // foreach ($formulaParams as $key => $value) {
            //     $value['survey_id'] = $surveyId;
            //     $index = $key-1;
            //     if(isset($surveyTemp[$index])) {
            //         $value['reference_formula_id'] = $surveyTemp[$index]['id'];
            //     }
            //     $formula = ReportFormula::create($value);
            //     // print_r($formula->toArray());
            //     $surveyTemp[] = $formula->toArray();
            // }
            // $survey = Survey::find($surveyId);
            // $reportFormulas = $survey->reportFormulas()->createMany($formulaParams);
            return response()->success([
                'data' => $surveyTemp
            ]);
        } catch (Exception $e) {
            return response()->error([
                'message' => $e->getMessage()
            ]);
        }
    }

    public function index(Request $request)
    {
        try {
            $surveyId = $request->input('survey_id');
            if($surveyId) {
                $formulas = ReportFormula::where('survey_id', $surveyId)
                    ->get();
            } else {
                $formulas = ReportFormula::all();
            }
            return response()->success([
                'data' => $formulas
            ]);
        } catch (Exception $e) {
            return response()->error([
                'message' => $e->getMessage()
            ]);
        }
    }

    public function update($id, StoreReportFormulaRequest $request)
    {
        try {
            $reportFormula = ReportFormula::find($id);
            
            // remove formula references
            if(isset($reportFormula->reference_formula_id)) {
                $isStop = false;
                $referenceId = $reportFormula->reference_formula_id;
                while(!$isStop) {
                    $referenceFormula = ReportFormula::find($referenceId);
                    if($referenceFormula) {
                        if(isset($referenceFormula->reference_formula_id)) {
                            $referenceId = $referenceFormula->reference_formula_id;
                        } else {
                            $isStop = true;
                        }
                        $referenceFormula->delete();
                    } else {
                        $isStop = true;
                    }
                }
            }

            $surveyId = $request->input('survey_id');
            $formulaParams = $request->input('formula');
            $formulaTemp = array();
            for($i = count($formulaParams)-1; $i >= 0; $i--) {
                $formulaParams[$i]['survey_id'] = $surveyId;
                if(!isset($formulaParams[$i]['name'])) {
                    $formulaParams[$i]['name'] = "";
                }

                $lastFormula = last($formulaTemp);
                if($lastFormula) {
                    $formulaParams[$i]['reference_formula_id'] = $lastFormula['id'];
                }
                if($i == 0) {
                    $formulaParams[$i]['is_parent'] = true;
                    $reportFormula->update($formulaParams[$i]);
                    $formulaTemp[] = $reportFormula->toArray();
                } else {
                    $formula = ReportFormula::create($formulaParams[$i]);
                    $formulaTemp[] = $formula->toArray();
                }
            }
            // $formulaParams = $request->input('formula');
            // foreach($formulaParams as &$value) {
            //     if(strlen($value) == 0) {
            //         $value = null;
            //     }
            // }
            // $formula = ReportFormula::find($id);
            // $formula->update($formulaParams);
            
            // // $reportFormulas = $survey->reportFormulas()->createMany($formulaParams);
            return response()->success([
                'data' => $formulaTemp
            ]);
        } catch (Exception $e) {
            return response()->error([
                'message' => $e->getMessage()
            ]);
        }
    }

}
