<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\Survey;
use App\Models\Category;
use App\Models\CategoryProfileDetail;
use App\Models\Observer;
use App\Models\Batch;
use App\Models\UserSurvey;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\v1\SurveyRequest;
use App\Http\Requests\Api\v1\BuySurveyRequest;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $email;
    public function index(SurveyRequest $request)
    {
        $surveys = Survey::getPublishedSurveys($request)->get();

        //Added by puw k5
        $dataSurvey=Survey::join('categories','categories.survey_id','surveys.id')
                          ->join('observers','observers.category_id','=','categories.id')
                          ->select('observers.id','observers.email')
                          ->where('categories.name','=','owner')
                          ->where('surveys.creator_id','=',$request->input('creator_id'))
                          ->take(1)
                          ->get();
        $this->email=$dataSurvey[0]->email;

        $surveys->transform(function($survey) {
            // return $this->mapData($survey);
            return $this->mapDataProfiler($survey);
        });

        if ($request->has('type') && $request->has('type') == 'full') {
            $surveys->transform(function($survey) {
                return $this->mappingData($survey);
            });
        }

        return response()->success([
            'data' => compact('surveys')
        ]);
    }

    private function mapData($survey)
    {
        $sur = [
            'id'            => $survey->id,
            'creator_id'    => $survey->creator_id,
            'title'         => $survey->title,
            'url'           => $survey->url,
            'email'         => $survey->ownerObserver() ? $survey->ownerObserver()->first()->email : '-',
        ];

        return array_merge($sur);
    }

    //Add by puw on 21-12-2017
    private function mapDataProfiler($survey)
    {
        $sur = [
            'id'            => $survey->id,
            'creator_id'    => $survey->creator_id,
            'title'         => $survey->title,
            'url'           => $survey->url,
            'email'         => $this->email,
        ];

        return array_merge($sur);
    }

    private function mappingData($survey)
    {
        $sur = [
            'id'            => $survey->id,
            'creator_id'    => $survey->creator_id,
            'title'         => $survey->title,
            'description'   => $survey->description,
            'period_start'  => $survey->period_start,
            'period_end'    => $survey->period_end,
            'is_other_reviewed' => $survey->is_other_reviewed,
            'type'          => $survey->type,
            'value'         => $survey->value,
            'is_repeated'   => $survey->is_repeated,
            'url'           => $survey->url,
            'email'         => $survey->ownerObserver() ? $survey->ownerObserver()->first()->email : '-',
        ];

        $sec = $survey->sections->transform(function ($section) {
            return $this->setSection($section);
        })->groupBy('question_type')->toArray();

        $sec['preliminary'] = isset($sec['pre']) ? $sec['pre'] : [];
        $sec['questionnaire'] = isset($sec['question']) ? $sec['question'] : [];
        $sec['post'] = isset($sec['post']) ? $sec['post'] : [];

        unset($sec['pre'], $sec['question']);

        return array_merge($sur, $sec);
    }

    private function setSection($section)
    {
        return [
            'title' => $section->title,
            'description' => $section->description,
            'ordering_type' => $section->is_number_continued,
            'question_type' => $section->question_type,
            'type' => $section->type,
            'questions' => $section->questions->transform(function ($question) {
                return [
                    'content' => $question->content,
                    'answer_type' => $question->answer_type,
                    'answer_configs' => $question->answer_configs,
                    'answer_attributes' => $question->answer_attributes,
                    'answer_scores' => $question->answer_scores,
                    'is_required' => $question->is_required,
                ];
            })->toArray()
        ];
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $survey = Survey::with('sections.questions')->findOrFail($id);

        return response()->success([
            'data' => $this->mappingData($survey)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function edit(Survey $survey)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Survey $survey)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function destroy(Survey $survey)
    {
        //
    }

    /**
     * @author Rangga <ranggapl@windowslive.com>
     * @return [json] [return object]
     */
    public function buySurvey(BuySurveyRequest $request)
    {
        try {
            $productId = $request->input('product_id');
            $surveyId = $request->input('survey_id');
            $userId = $request->input('user_id');
            $model = $request->input('model');
            $email = $request->input('email');
            $firstName = $request->input('first_name');
            $lastName = $request->input('last_name');
            $phoneNumber = $request->input('phone_number');

            // find or create buyer category
            $categoryParams = array(
                'survey_id' => $surveyId,
                'name' => 'buyer'
            );
            $category = Category::firstOrCreate($categoryParams);

            // Find Survey
            $survey = Survey::find($surveyId);
            if ($survey) {
                if ($survey->profiler_type == "360-profiler") {
                    // Insert all Category Profile
                    $categoryProfileDetails = CategoryProfileDetail::where('category_profile_id', $survey->category_profile_id);
                    $arrayCategory = array();
                    foreach ($categoryProfileDetails as $categoryProfileDetail) {
                        $arrayCategory[] = array(
                            'survey_id' => $surveyId,
                            'user_id' => $userId,
                            'name' => $categoryProfileDetail->name,
                            'created_at'    => date('Y-m-d H:i:s'),
                            'updated_at'    => date('Y-m-d H:i:s'),
                        );
                    }
                    $categoryAll = Category::insert($arrayCategory);
                }
            }

            // find or create observer
            $observerParams = array(
                'category_id' => $category->id,
                'first_name' => $firstName,
                'last_name' => $lastName,
                'email' => $email,
                'phone_number' => $phoneNumber ? $phoneNumber:''
            );
            $observer = Observer::create($observerParams);

            // generate batch
            $encryptIds = $productId . '+' . $surveyId . '+' . $userId . '+' . $observer->id;
            $code = strtr(base64_encode(openssl_encrypt($encryptIds, 'AES-128-ECB', 'l1tprofiler')), '+/=', '-_,');

            // create user survey
            $userSurveyParams = array(
                'survey_id' => $surveyId,
                'observer_id' => $observer->id,
                'user_id' => $userId,
                'product_id' => $productId,
                'model' => $model,
                'email' => $email
            );
            $userSurvey = UserSurvey::create($userSurveyParams);

            // find or create batches
            $batchParams = array(
                'observer_id' => $observer->id,
                'survey_id' => $surveyId,
                'user_survey_id' => $userSurvey->id,
                'is_done' => false,
                'batch' => $code,
            );
            $batch = Batch::firstOrCreate($batchParams);

            // generate url
            $link = url('preview/customer/' . $code);
            return response()->success([
                'data' => array('product_id' => $productId, 'model' => $model, 'link' => $link)
            ]);
        } catch (Exception $e) {
            return response()->error([
                'message' => $e->getMessage()
            ]);
        }
    }

}
