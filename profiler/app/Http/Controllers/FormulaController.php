<?php

namespace App\Http\Controllers;

use App\Models\Formula;
use App\Models\Question;
use App\Models\Survey;
use App\Models\FormulaDetail;
use Illuminate\Http\Request;
use App\Http\Requests\FormulaRequest;
use App\DataTables\FormulaDataTable;
use DB;

class FormulaController extends BaseController
{
    public function params()
    {
        $data['types'] = [
            'sum'           => 'Sum / total',
            'difference'    => 'Difference',
            'deviation'     => 'Standard Deviation',
            'maximum'       => 'Maximum',
            'minimum'       => 'Minimum',
            'mode'          => 'Mode',
            'mean'          => 'Mean',
            'medium'        => 'Medium',
            'average'       => 'Average' ,
        ];
        $data['operands'] = [
            '*'     => '*',
            '/'     => '/',
            '+'     => '+',
            '-'     => '-',
        ];
        return $data;
    }
    /**
     * Display a listing of the resource.
     *
     * @param App\DataTables\FormulaDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index($token, FormulaDataTable $dataTable)
    {
        return $dataTable->with('user_id', session('user')['id'])->render('formulas.index');
    }

    /**
     * Show the form for creating new resource.
     *
     * @param  \App\Models\Formula  $formula
     * @return \Illuminate\Http\Response
     */
    public function create($token, Formula $formula)
    {
        // $questions = Question::pluck('content', 'id')->all();
        $questions = array();
        $surveys = Survey::select('id', DB::raw('CONCAT(batch_reference, " - ", title) AS concat_title'))
            ->where('creator_id', session('user')['id'])
            ->pluck('concat_title', 'id')
            ->all();
        $types = $this->params()['types'];
        $operands = $this->params()['operands'];
        return view('formulas.create', compact('questions', 'types', 'operands', 'surveys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($token, FormulaRequest $request)
    {
        $formula = Formula::create(['name' => $request->name, 'survey_id' => $request->survey_id]);
        foreach ($request->type as $key => $value) {
            # code...
            if ($formula) {
                FormulaDetail::insert([
                    'formula_id'    => $formula->id,
                    'question_id'   => (!empty($request->question_id[$key])) ? $request->question_id[$key] : 0,
                    'type'          => $request->type[$key],
                    'operand'       => $request->operand[$key],
                    'created_at'    => date('Y-m-d H:i:s'),
                    'updated_at'    => date('Y-m-d H:i:s'),
                ]);
            }
        }
        return $this->actionRedirect('create', $formula->name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Formula  $formula
     * @return \Illuminate\Http\Response
     */
    // public function edit($token, Formula $formula)
    // {
    //     return $this->renderForm($formula, true);
    // }
    public function edit($token, $id)
    {
        $formula = Formula::findOrFail($id);
        $surveys = Survey::select('id', DB::raw('CONCAT(batch_reference, " - ", title) AS concat_title'))
            ->where('creator_id', session('user')['id'])
            ->pluck('concat_title', 'id')
            ->all();
        $questions = Question::select('questions.content', 'questions.id')
            ->leftJoin('sections', 'sections.id', '=', 'questions.section_id')
            ->where('sections.survey_id', $formula->survey_id)
            ->pluck('content', 'id')
            ->all();
        $types = $this->params()['types'];
        $operands = $this->params()['operands'];
        $newQuestions = array();

        foreach ($questions as $key => $value) {
            $newQuestions[$key] = strip_tags(html_entity_decode($value));
        }

        $questions = $newQuestions;
        return view('formulas.edit', [
            'formula'       => $formula,
            'details'       => FormulaDetail::whereFormulaId($formula->id)->get(),
            'types'         => $types,
            'operands'      => $operands,
            'questions'     => $questions,
            'surveys'       => $surveys,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Formula  $formula
     * @return \Illuminate\Http\Response
     */
    public function update(FormulaRequest $request, $token, $id)
    {
        $formula = Formula::findOrFail($id);
        if ($formula) {
            $update = $formula->update(['name' => $request->name, 'survey_id' => $request->survey_id]);
            if ($update) {
                FormulaDetail::whereFormulaId($id)->delete();
                foreach ($request->type as $key => $value) {
                    FormulaDetail::insert([
                        'formula_id'    => $formula->id,
                        'question_id'   => (!empty($request->question_id[$key])) ? $request->question_id[$key] : 0,
                        'type'          => $request->type[$key],
                        'operand'       => $request->operand[$key],
                        'created_at'    => date('Y-m-d H:i:s'),
                        'updated_at'    => date('Y-m-d H:i:s'),
                    ]);
                }
            }
        }
        return $this->actionRedirect('update', $formula->name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Formula  $formula
     * @return \Illuminate\Http\Response
     */
    public function destroy($token, Formula $formula)
    {
        $name = $formula->name;
        $id = $formula->id;
        $formula->delete();
        FormulaDetail::whereFormulaId($id)->delete();
        return $this->actionRedirect('delete', $name);
    }

    /**
     * Handle store and update.
     *
     * @param  \App\Models\Attribute  $formula
     * @param  boolean  $type
     * @return \Illuminate\Http\Response
     */
    private function renderForm($formula, $type)
    {
        return response()->json(
            view('formulas._form', $this->prepareData([
                'formula' => $formula
            ], ['form-horizontal'], $type))->render()
        );
    }

    /**
     * Handle store and update.
     *
     * @param  string  $method
     * @param  string  $name
     * @return \Illuminate\Http\Response
     */
    private function actionRedirect($mehtod, $name)
    {
        return redirect(\RouteHelper::token('formulas.index'))->withSuccess(
            trans("messages.success.$mehtod", [
                'name' => sprintf('formula %s', strtolower($name))
            ])
        );
    }

    /**
     * Handle get question per survey.
     *
     * @param  string  $method
     * @param  string  $name
     * @return \Illuminate\Http\Response
     */
    public function getQuestion($token, $survey_id)
    {
        $questions = Question::select('questions.content', 'questions.id')
            ->leftJoin('sections', 'sections.id', '=', 'questions.section_id')
            ->where('sections.survey_id', $survey_id)
            ->pluck('content', 'id')
            ->all();
        $types = $this->params()['types'];
        $operands = $this->params()['operands'];
        $newQuestions = array();

        foreach ($questions as $key => $value) {
            $newQuestions[$key] = strip_tags(html_entity_decode($value));
        }

        $questions = $newQuestions;

        return view('formulas.forms.detail', compact('questions', 'types', 'operands'));
    }
}
