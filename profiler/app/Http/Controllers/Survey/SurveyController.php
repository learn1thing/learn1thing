<?php

namespace App\Http\Controllers\Survey;

use Illuminate\Http\Request;
use App\DataTables\SurveyDataTable;
use App\DataTables\BatchDataTable;
use App\DataTables\TemplateDataTable;
use App\Models\Survey;
use App\Models\Section;
use App\Models\Attribute;
use App\Models\Batch;
use App\Models\Category;
use App\Models\Observer;
use App\Models\AnswerHeader;
use App\Models\UserSurvey;
use App\Models\CategoryProfile;
use App\Models\CategoryProfileDetail;
use App\Models\EmailBuilder as ReportTemplate;
use App\Http\Requests\SurveyRequest;
use App\Http\Controllers\BaseController;
use App\Notifications\Mail as MailNotification;
use App\Notifications\ForceSurvey as ForceSurveyNotification;
use DB;
use Storage;

class SurveyController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param App\DataTables\SurveyDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index(SurveyDataTable $dataTable, Request $request)
    {
        // if (session('user')['role']['slug'] == "business-partner") {
        return $dataTable->with('user_id', session('user')['id'])->render('survey.index');
        // } else {
            // return $dataTable->render('survey.index');
        // };
    }

    public function create($token, Request $request)
    {
        $sessionId = $request->input('session_id');
        $alias = "preview_survey_" . $sessionId;
        $survey = array();
        $attributes = array();
        if($sessionId) {
            if(Storage::disk('local')->exists($alias)) {
                $contents = Storage::get($alias);
                $contents = json_decode($contents, true);
                if($contents) {
                    $survey = $contents;
                }
            }
        }

        $reportTemplates = ReportTemplate::spesificUser(session('user')['id'])->pluck('name', 'id');
        $categoryProfiles = CategoryProfile::all();
        $category_profiles = array();
        foreach ($categoryProfiles as $key => $value) {
            $category_profiles[$value->id] = $value->name;
        }
        $is_exist_answer = false;
        return view('survey.create', compact('attributes', 'category_profiles', 'is_exist_answer', 'survey', 'reportTemplates'));
    }

    public function store($token, Request $request)
    {
        $request->merge(['creator_id' => session('user')['id']]);

        // for testing
        // $request->merge(['value' => empty($request->input('value')) ? null : $request->input('value')]);
        $surveyParams = $request->except('_token', 'is_exist_answer', 'statistic_text', 'attributes', 'preliminary', 'questionnaire', 'post');
        if(!isset($surveyParams['type']) || strlen($surveyParams['type']) == 0) {
            $surveyParams['type'] = null;
        }
        if(!isset($surveyParams['value']) || strlen($surveyParams['value']) == 0) {
            $surveyParams['value'] = null;
        }
        if(isset($surveyParams['random_question'])) {
            $surveyParams['is_random_questionnaire'] = $surveyParams['random_question'] == 'on' ? true:false;
            unset($surveyParams['random_question']);
        } else {
            $surveyParams['is_random_questionnaire'] = false;
        }

        if(isset($surveyParams['same_observer_desc'])) {
            $surveyParams['same_observer_desc'] = $surveyParams['same_observer_desc'] == 'on' ? true:false;
        }
        if(isset($surveyParams['fixed_cmp_reminder'])) {
            $surveyParams['fixed_cmp_reminder'] = $surveyParams['fixed_cmp_reminder'] == 'on' ? true:false;
            $surveyParams['cmp_reminder_date'] = $surveyParams['cmp_reminder_date'] . " " . $surveyParams['cmp_reminder_time'] . ":00";
        }
        if(isset($surveyParams['redirect_after_completion'])) {
            $surveyParams['redirect_after_completion'] = $surveyParams['redirect_after_completion'] == 'on' ? true:false;
        } else {
            $surveyParams['redirect_after_completion'] = false;
            $surveyParams['redirect_after_completion_url'] = null;
        }
        // if($surveyParams['allow_set_observer'] == 'disable') {
        //     $surveyParams['observer_count'] = 0;
        //     $surveyParams['category_count'] = 0;
        // }
        // $surveyParams['is_allow_set_observer'] = $surveyParams['allow_set_observer'] == 'enable' ? 1:0;
        // unset($surveyParams['allow_set_observer']);

        if($surveyParams['state'] != 'preview_as_consumer') {
            if(isset($surveyParams['email_builder_id']) && strlen($surveyParams['email_builder_id']) > 0) {
                $survey = Survey::create($surveyParams);
            } else {
                $surveyParams['email_builder_id'] = 0;
                DB::statement('SET FOREIGN_KEY_CHECKS=0');

                $survey = Survey::create($surveyParams);

                //
                DB::statement('SET FOREIGN_KEY_CHECKS=1');
            }

            // hampura balatak
            // $userSurvey = $survey->userSurveys()->create(array('id' => $survey->id));
            $category = $survey->categories()->create(array('name' => 'owner'));
            $observer = $category->observers()->create(array(
                    'first_name' => session('user')['first_name']
                    , 'last_name' => session('user')['last_name']
                    , 'email' => openssl_decrypt(base64_decode(strtr(session('user')['email'], '-_,', '+/=')), 'AES-128-ECB', 'l1tmiddleware')
                    , 'phone_number' => '-'
                )
            );
            // $userSurvey = $survey->userSurveys->create(array(
            //         'observer_id' => $observer->id
            //         , 'user_id' => '0'
            //         , 'product_id' => '0'
            //         , 'model' => ''
            //         , 'email' => openssl_decrypt(base64_decode(strtr(session('user')['email'], '-_,', '+/=')), 'AES-128-ECB', 'l1tmiddleware')
            //         , 'position' => 'pre'
            //     )
            // );
            // end hampura balatak

            return $this->createUpdate($survey, $request, 'create');
        } else {
            $uniqueId = time();
            $alias = 'preview_survey_' . $uniqueId;

            if(isset($surveyParams['fixed_cmp_reminder'])) {
                $surveyParams['cmp_reminder_time'] = date('H:i', strtotime($surveyParams['cmp_reminder_date']));
                $surveyParams['cmp_reminder_date'] = date('Y-m-d', strtotime($surveyParams['cmp_reminder_date']));
            } else {
                $surveyParams['cmp_reminder_time'] = null;
                $surveyParams['cmp_reminder_date'] = null;
            }

            $surveyParams['attributes'] = $request->input('attributes');
            $surveyParams['random_question'] = $surveyParams['is_random_questionnaire'];
            $surveyParams['preliminary'] = $request->input('preliminary');
            $surveyParams['questionnaire'] = $request->input('questionnaire');
            $surveyParams['post'] = $request->input('post');
            $surveyParams['_token'] = $token;

            Storage::disk('local')->put($alias, json_encode($surveyParams));
            return redirect()->route('preview.consumer', ['id' => $uniqueId]);
        }
    }

    public function destroy($token, Survey $survey)
    {
        $title = $survey->title;
        
	/*
	$survey->answerHeader()->delete();
        // uncomment when attribute implement with survey
        $survey->attributes()->delete();
        $survey->categories()->delete();
        $survey->batches()->delete();
        // foreach ($survey->formulas as $formula) {
        //     $formula->details()->delete();
        // }
        $survey->formulas()->delete();
        foreach ($survey->sections as $section) {
            $section->questions()->delete();
        }
        $survey->sections()->delete();
	*/        

	$survey->delete();
        return $this->actionRedirect('delete', $title);
    }

    public function show($token, $id)
    {
        $survey = Survey::with('sections.questions')->findOrFail($id);
        // change the view please
        return view('survey.edit', [
            'survey' => $this->mappingData($survey),
        ]);
    }

    public function edit($token, $id, Request $request)
    {
        $sessionId = $request->input('session_id');
        $alias = "preview_survey_" . $sessionId;
        $surveyOnSession = array();
        if($sessionId) {
            if(Storage::disk('local')->exists($alias)) {
                $contents = Storage::get($alias);
                $contents = json_decode($contents, true);
                if($contents) {
                    $surveyOnSession = $contents;
                }
            }
        }
        $survey = Survey::with('sections.questions')->findOrFail($id);
        $isExistAnswer = $survey->answerHeader()->count() > 0 ? true:false;

	//$isExistAnswer=false; 

        $reportTemplates = ReportTemplate::spesificUser(session('user')['id'])->pluck('name', 'id');
        $categoryProfiles = CategoryProfile::all();
        $category_profiles = array();
        foreach ($categoryProfiles as $key => $value) {
            $category_profiles[$value->id] = $value->name;
        }
        return view('survey.edit', [
            'survey'     => $surveyOnSession ? $surveyOnSession : $this->mappingData($survey),
            'is_exist_answer' => $isExistAnswer,
            'attributes' => Attribute::where('survey_id', $survey->id)->get(),
            'reportTemplates' => $reportTemplates,
            'category_profiles' => $category_profiles
        ]);
    }

    // addedd by rdw 081217
    public function duplicateData($token, $id, Request $request)
    {
        $dataOld = Survey::find($id);
        DB::select('call duplicateData(?)',array($id));
        return redirect(\RouteHelper::token('surveys.index'));
    }
    public function duplicateDataReport($token, $id, Request $request)
    {
        //$dataOld = Survey::find($id);
        DB::select('call duplicateDataReport(?)',array($id));
        return redirect(\RouteHelper::token('template-builder.index'));
    }

    public function update($token, Request $request, $id)
    {
        $request->merge(['creator_id' => session('user')['id']]);
        $survey = Survey::findOrFail($id);
        $surveyParams = $request->except('_token', 'is_exist_answer', 'statistic_text', 'attributes', 'preliminary', 'questionnaire', 'post');
        if(!isset($surveyParams['type']) || strlen($surveyParams['type']) == 0) {
            $surveyParams['type'] = null;
        }
        if(!isset($surveyParams['value']) || strlen($surveyParams['value']) == 0) {
            $surveyParams['value'] = null;
        }
        if(isset($surveyParams['random_question'])) {
            $surveyParams['is_random_questionnaire'] = $surveyParams['random_question'] == 'on' ? true:false;
            unset($surveyParams['random_question']);
        } else {
            $surveyParams['is_random_questionnaire'] = false;
        }

        if(isset($surveyParams['same_observer_desc'])) {
            $surveyParams['same_observer_desc'] = $surveyParams['same_observer_desc'] == 'on' ? true:false;
        } else {
            $surveyParams['same_observer_desc'] = false;
        }

        if(isset($surveyParams['fixed_cmp_reminder'])) {
            $surveyParams['fixed_cmp_reminder'] = $surveyParams['fixed_cmp_reminder'] == 'on' ? true:false;
            $surveyParams['cmp_reminder_date'] = $surveyParams['cmp_reminder_date'] . " " . $surveyParams['cmp_reminder_time'] . ":00";
        } else {
            $surveyParams['fixed_cmp_reminder'] = false;
        }

        if(isset($surveyParams['redirect_after_completion'])) {
            $surveyParams['redirect_after_completion'] = $surveyParams['redirect_after_completion'] == 'on' ? true:false;
        } else {
            $surveyParams['redirect_after_completion'] = false;
        }
        // if($surveyParams['allow_set_observer'] == 'disable') {
        //     $surveyParams['observer_count'] = 0;
        //     $surveyParams['category_count'] = 0;
        // }
        // $surveyParams['is_allow_set_observer'] = $surveyParams['allow_set_observer'] == 'enable' ? 1:0;
        // unset($surveyParams['allow_set_observer']);

        if($surveyParams['state'] != 'preview_as_consumer') {

            if(isset($surveyParams['email_builder_id']) && strlen($surveyParams['email_builder_id']) > 0) {
                $survey->update($surveyParams);
            } else {
                $surveyParams['email_builder_id'] = 0;
                DB::statement('SET FOREIGN_KEY_CHECKS=0');

                $survey->update($surveyParams);

                //
                DB::statement('SET FOREIGN_KEY_CHECKS=1');
            }

            if(!$request->input('is_exist_answer')) {
                $survey->sections()->delete();
                $survey->attributes()->delete();
            }
            return $this->createUpdate($survey, $request, 'update');
        } else {
            $uniqueId = time();
            $alias = 'preview_survey_' . $uniqueId;
            $surveyParams['id'] = $id;
            if($surveyParams['fixed_cmp_reminder']) {
                $surveyParams['cmp_reminder_time'] = date('H:i', strtotime($surveyParams['cmp_reminder_date']));
                $surveyParams['cmp_reminder_date'] = date('Y-m-d', strtotime($surveyParams['cmp_reminder_date']));
            } else {
                $surveyParams['cmp_reminder_time'] = null;
                $surveyParams['cmp_reminder_date'] = null;
            }

            if(!$surveyParams['redirect_after_completion']) {
                $surveyParams['redirect_after_completion_url'] = null;
            }

            $surveyParams['attributes'] = $request->input('attributes');
            $surveyParams['random_question'] = $surveyParams['is_random_questionnaire'];
            $surveyParams['preliminary'] = $request->input('preliminary');
            $surveyParams['questionnaire'] = $request->input('questionnaire');
            $surveyParams['post'] = $request->input('post');
            $surveyParams['_token'] = $token;

            Storage::disk('local')->put($alias, json_encode($surveyParams));
            return redirect()->route('preview.consumer', ['id' => $uniqueId]);
        }
    }

    private function actionRedirect($mehtod, $name)
    {
        return redirect(\RouteHelper::token('surveys.index'))->withSuccess(
            trans("messages.success.$mehtod", [
                'name' => sprintf('attribute %s', strtolower($name))
            ])
        );
    }

    private function mappingData($survey)
    {
        // $randomQuestion = $survey->is_random_questionnaire == 1 ? 'yes':'no';
        $allowSetObserver = $survey->is_allow_set_observer == 1 ? 'enable':'disable';
        $sur = [
            'id'                => $survey->id,
            'title'             => $survey->title,
            'batch_reference'   => $survey->batch_reference,
            'description'       => $survey->description,
            'observer_description' => $survey->observer_description,
            'period_start'      => $survey->period_start,
            'period_end'        => $survey->period_end,
            'type'              => $survey->type,
            'value'             => $survey->value,
            'observer_count'    => $survey->observer_count,
            'category_count'    => $survey->category_count,
            'email_builder_id'  => $survey->email_builder_id,
            'random_question'   => $survey->is_random_questionnaire,
            'allow_set_observer' => $allowSetObserver,
            'profiler_type'     => $survey->profiler_type,
            'instruction'       => $survey->instruction,
            'fixed_cmp_reminder' => $survey->fixed_cmp_reminder,
            'cmp_reminder_date' => isset($survey->cmp_reminder_date) ? date('Y-m-d', strtotime($survey->cmp_reminder_date)):null,
            'cmp_reminder_time' => isset($survey->cmp_reminder_date) ? date('H:i', strtotime($survey->cmp_reminder_date)):null,
            'same_observer_desc' => $survey->same_observer_desc,
            'category_profile_id' => $survey->category_profile_id,
            'thanks_message'    => $survey->thanks_message,
            'redirect_after_completion' => $survey->redirect_after_completion,
            'redirect_after_completion_url' => $survey->redirect_after_completion_url,
            'state' => $survey->state

        ];

        $sec = $survey->sections->transform(function ($section) {
            return $this->setSection($section);
        })->groupBy('question_type')->toArray();

        $sec['preliminary'] = isset($sec['pre']) ? $sec['pre'] : [];
        $sec['questionnaire'] = isset($sec['question']) ? $sec['question'] : [];
        $sec['post'] = isset($sec['post']) ? $sec['post'] : [];

        unset($sec['pre'], $sec['question']);

        return array_merge($sur, $sec);
    }

    private function setSection($section)
    {
        $orderingType = "";
        $orderingFormat = "";
        if($section->is_number_automated) {
            $orderingType = $section->is_number_continued ? "continue":"reset";
            $orderingFormat = $section->type;
        }
        $automaticNumbering = $section->is_number_automated ? "yes":"no";

        return [
            'title' => $section->title,
            'description' => $section->description,
            'automatic_numbering' => $automaticNumbering,
            'ordering_type' => $orderingType,
            'question_type' => $section->question_type,
            'type' => $orderingFormat,
            'question' => $section->questions->transform(function ($question) {
                $answerConfigs = $question->answer_configs;
                $answerAttributes = $question->answer_attributes;
                if($question->answer_type === "matching" || $question->answer_type === "likert_scale") {
                    foreach($answerConfigs as $key => &$config) {
                        $config = (Array) $config;
                    }
                } else if($question->answer_type === "ranking" || $question->answer_type === "multiple_choice_with_constraint" || $question->answer_type === "distributed_points"){
                    $answerConfigs = (Array) $answerConfigs;
                }

                // convert answer attributes from object to array
                if($question->answer_type === "multichoice") {
                    $answerAttributes = (Array) $answerAttributes;
                }
                return [
                    'question_number' => $question->question_number,
                    'name' => $question->name,
                    'content' => $question->content,
                    'coordinate' => $question->coordinate,
                    'answer_type' => $question->answer_type,
                    'answer_configs' => $answerConfigs,
                    'answer_attributes' => $answerAttributes,
                    'answer_scores' => $question->answer_scores,
                    'is_required' => $question->is_required,
                ];
            })->toArray()
        ];
    }

    private function mappingSection(array $sections, $type)
    {
        foreach ($sections as $key => &$section) {
            $section['question_type'] = $type;
            $section['is_number_continued'] = isset($section['ordering_type']) && $section['ordering_type'] === "continue" ? true:false;
            $section['is_number_automated'] = $section['automatic_numbering'] === "yes" ? true:false;
            unset($section['ordering_type']);
            unset($section['automatic_numbering']);
        }
        return $sections;
    }

    private function createQuest(array $types, array $fields, array $attributes = array())
    {
        foreach ($types as $key => $type) {
            foreach ($fields[$key]['question'] as $k => &$question) {
                if(!isset($question['is_required'])) {
                    $question['is_required'] = false;
                }

                if(in_array($question['answer_type'], ["multichoice", "checkboxes", "matching", "likert_scale"])) {
                    $configCounts = count($question['answer_configs']);
                    $attributeCounts = isset($question['answer_attributes']) ? count($question['answer_attributes']):0;
                    $question['answer_attributes'] = isset($question['answer_attributes']) ? array_values($question['answer_attributes']):array();
                    // if($attributeCounts < $configCounts) {
                        for($i = 0; $i < $configCounts;$i++) {
                            if(!isset($question['answer_attributes'][$i])) {
                                $question['answer_attributes'][$i] = array();
                            } else {
                                foreach($question['answer_attributes'][$i] as &$attr) {
                                    $attributeKey = array_search($attr, array_column($attributes, 'name'));
                                    // replace the attribute name with attribute id
                                    if($attributeKey !== FALSE) {
                                        $attr = $attributes[$attributeKey]['id'];
                                    }
                                }
                            }
                        }
                        ksort($question['answer_attributes']);
                    // }

                    if($question['answer_type'] == 'matching' || $question['answer_type'] == 'likert_scale') {
                        $question['answer_configs'] = array_values($question['answer_configs']);
                    }
                } else if(in_array($question['answer_type'], ["multiple_choice_with_constraint", "distributed_points"])) {
                    $configCounts = count($question['answer_configs']['options']);
                    $attributeCounts = isset($question['answer_attributes']) ? count($question['answer_attributes']) : 0;
                    $question['answer_attributes'] = isset($question['answer_attributes']) ? array_values($question['answer_attributes']):array();
                    // if($attributeCounts < $configCounts) {
                        for($i = 0; $i < $configCounts;$i++) {
                            if(!isset($question['answer_attributes'][$i])) {
                                $question['answer_attributes'][$i] = array();
                            } else {
                                foreach($question['answer_attributes'][$i] as &$attr) {
                                    $attributeKey = array_search($attr, array_column($attributes, 'name'));
                                    // replace the attribute name with attribute id
                                    if($attributeKey !== FALSE) {
                                        $attr = $attributes[$attributeKey]['id'];
                                    }
                                }
                            }
                        }
                        ksort($question['answer_attributes']);
                    // }
                } else {
                    if(!isset($question['answer_attributes'])) {
                        $question['answer_attributes'] = array();
                    } else {
                        foreach($question['answer_attributes'] as &$attr) {
                            $attributeKey = array_search($attr, array_column($attributes, 'name'));
                            // replace the attribute name with attribute id
                            if($attributeKey !== FALSE) {
                                $attr = $attributes[$attributeKey]['id'];
                            }
                        }
                    }
                }
            }
            $type->questions()->createMany($fields[$key]['question']);
        }
    }

    private function createUpdate($survey, $request, $type)
    {
        if(!$request->input('is_exist_answer')) {
            $this->setAttributes($survey, $request->input('attributes'));

            if ($request->has('preliminary')) {
                $preliminaries = $this->mappingSection($request->input('preliminary'), 'pre');
                $preliminary = $survey->sections()->createMany($preliminaries);
                $this->createQuest($preliminary, $preliminaries, $survey->attributes->toArray());
            }

            if ($request->has('post')) {
                $posts = $this->mappingSection($request->input('post'), 'post');
                $post = $survey->sections()->createMany($posts);
                $this->createQuest($post, $posts, $survey->attributes->toArray());
            }

            $questionnaires = $this->mappingSection($request->input('questionnaire'), 'question');
            $questionnaire = $survey->sections()->createMany($questionnaires);
            $this->createQuest($questionnaire, $questionnaires, $survey->attributes->toArray());
        }

        return $this->actionRedirect($type, $survey->title);
    }

    // $attributes = ['Attr1', 'Attr2', 'Attr3']
    private function setAttributes($survey, $attributes = array())
    {
        $data = array();
        if(count($attributes) > 0) {
            foreach($attributes as $attribute) {
                array_push($data, array('name' => $attribute));
            }
            return $survey->attributes()->createMany($data);
        } else {
            return FALSE;
        }
    }

    public function setObserver(Request $request, $id)
    {
        // Base code for encrypt id
        // strtr(base64_encode(openssl_encrypt($applicationId, 'AES-128-ECB', $key)), '+/=', '-_,')

        // Base code for decrypt id
        $baseId = $id;
        $id = openssl_decrypt(base64_decode(strtr($id, '-_,', '+/=')), 'AES-128-ECB', 'l1tprofiler');
        $survey = Survey::findOrFail($id);
        // base code
        // $categories = $survey->categories()->createMany($request->input('categories'));

        // $observers = [];

        // foreach ($categories as $key => $category) {
        //     $observers = $category->observers()->createMany($request->input('categories')[$key]['observers']);

        //     foreach ($observers as $keyOb => $observer) {
        //         $batch = strtr(base64_encode(openssl_encrypt(date('Y-m-d H:i:s'), 'AES-128-ECB', 'l1tprofiler')), '+/=', '-_,');
        //         $batch = Batch::create([
        //             // 'batch'     => $request->input('categories')[$key]['observers'][$keyOb]['batch'],
        //             'batch' => $batch,
        //             'is_done'   => false,
        //             'survey_id' => $survey->id,
        //             'observer_id' => $observer->id,
        //         ]);

        //         $observer->notify(new MailNotification);
        //     }
        // }
        // end base code
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        foreach ($request->input('categories') as $key => $value) {
            $observers = $value['observers'];
            // unset($value['observers']);
            // $category = Category::where(DB::raw('lower(name)'), $value['name'])->where('user_id', $value['user_id'])->first();
            foreach ($observers as &$observer) {
                $observer['category_id'] = 0;
            }
            $category = CategoryProfileDetail::find($value['category_profile_detail_id']);
            $observers = $category->observers()->createMany($observers);

            // if (!$category) {
            //     $value['survey_id'] = $survey->id;
            //     $category = Category::firstOrCreate($value);
            // }
            // $observers = $category->observers()->createMany($observers);


            foreach ($observers as $keyOb => $observer) {
                $batch = strtr(base64_encode(openssl_encrypt(date('Y-m-d H:i:s'), 'AES-128-ECB', 'l1tprofiler')), '+/=', '-_,');
                $batch = Batch::create([
                    // 'batch'     => $request->input('categories')[$key]['observers'][$keyOb]['batch'],
                    'batch' => $batch,
                    'is_done'   => false,
                    'survey_id' => $survey->id,
                    'observer_id' => $observer->id,
                ]);

                $observer->notify(new MailNotification);
            }
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        return redirect(
            \RouteHelper::check('surveys.preview', [
                // 'type' => 'survey'
                // , 'code' => $baseId
                'type' => $request->input('link_type')
                , 'code' => $request->input('link_code')
            ])
        )->withSuccess("Email has been sent to observers");

        // return $this->generatePDF();
    }

    public function getBatch(BatchDataTable $dataTable, $id, $surveyId)
    {
        $surveyId = openssl_decrypt(base64_decode(strtr($surveyId, '-_,', '+/=')), 'AES-128-ECB', 'l1tprofiler');
        return $dataTable->with('flag', 'observer')->with('survey_id', $surveyId)->render('survey.batch');
    }

    public function getBuyer(BatchDataTable $dataTable, $id, $surveyId)
    {
        $surveyId = openssl_decrypt(base64_decode(strtr($surveyId, '-_,', '+/=')), 'AES-128-ECB', 'l1tprofiler');
        return $dataTable->with('flag', 'buyer')->with('survey_id', $surveyId)->render('survey.batch');
    }

    public function getReportTemplate(TemplateDataTable $dataTable, $id, $surveyId)
    {
        $surveyId = openssl_decrypt(base64_decode(strtr($surveyId, '-_,', '+/=')), 'AES-128-ECB', 'l1tprofiler');
        return $dataTable
            ->with('user_id', session('user')['id'])
            ->with('survey_id', $surveyId)
            ->render('survey.report-template');
    }

    public function getRetakeSurvey($id, $observerId, $surveyId)
    {
        $survey = Survey::findOrFail($surveyId);
        $observer = Observer::findOrFail($observerId);

        $status = 'error';
        $name = '-';
        if ($survey && $observer) {
            UserSurvey::where('observer_id', $observerId)
                ->where('survey_id', $surveyId)->update(['position' => 'pre']);
            AnswerHeader::where('observer_id', $observerId)
                ->where('survey_id', $surveyId)
                ->delete();
            $observer->notify(new ForceSurveyNotification);
            $observer->batch->update(['is_done' => false]);
            $status = 'success';
            $name = $observer->full_name();
        }

        return [
            'message' => trans("messages.$status.retake", [
                'name' => sprintf('attribute %s', strtolower($name))
            ])
            , 'status' => $status
            , 'position' => 'batch'
        ];
        // return $this->actionRedirect('retake', $survey ? $survey->title : '');
    }

    public function getReminder($id, $surveyId)
    {
        $surveyId = openssl_decrypt(base64_decode(strtr($surveyId, '-_,', '+/=')), 'AES-128-ECB', 'l1tprofiler');
        $survey = Survey::findOrFail($surveyId);

        // if ($survey) {
        //     $observer_id = 0;
        //     if ($survey->ownerObserver()) {
        //         if ($survey->ownerObserver()[0]) {
        //             $observer_id = $survey->ownerObserver()[0];
        //         }
        //     }
        //     $survey->answerHeader()->where('observer_id', $observer_id)->delete();
        //     $survey->notify(new ForceSurveyNotification);
        // }

        return $this->actionRedirect('reminder', $survey ? $survey->title : '');
    }

    public function generatePDF()
    {
        $filename = 'report/learn1thing.pdf';
        $path = storage_path($filename);

        return \Response::make(file_get_contents($path), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$filename.'"'
        ]);
    }
}
