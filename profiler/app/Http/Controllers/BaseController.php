<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    /**
     * Prepare data for form.
     *
     * @param  object   $dataToBind
     * @param  array   	$attributes
     * @param  boolean  $edited
     * @return array
     */
    protected function prepareData($dataToBind, array $attributes, $edited = false)
    {
    	$class =  str_replace('App\Http\Controllers\\', '', static::class);

        return [
            'title' => $edited ? trans('title.edit') : trans('title.add'),
            'isEdit' => $edited,
            'form'  => [
                'url'    => $edited
                    ? action("$class@update", array_merge($dataToBind, ['authtoken' => request()->authtoken])) 
                    : action("$class@store", ['authtoken' => request()->authtoken]),
                'method' => $edited ? 'PATCH' : 'POST',
                'id'	 => str_replace('controller', '-form', class_basename(strtolower($class))),
                'class'	 => implode(' ', $attributes)
            ],
            'data'  => $dataToBind
        ];
    }
}
