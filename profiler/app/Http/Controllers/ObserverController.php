<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ObserverRequest;
use App\DataTables\ObserverDataTable;
use App\Models\Observer;
use App\Models\Category;

class ObserverController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param App\DataTables\ObserverDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index($token, ObserverDataTable $dataTable)
    {
        return $dataTable->render('observers.index');
    }

    /**
     * Show the form for creating new resource.
     *
     * @param  \App\Models\Observer  $observer
     * @return \Illuminate\Http\Response
     */
    public function create($token, Observer $observer)
    {
        return $this->renderForm($observer, false);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($token, ObserverRequest $request)
    {
        $observer = Observer::create($request->except('_token'));
        return $this->actionRedirect('create', $observer->name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Observer  $observer
     * @return \Illuminate\Http\Response
     */
    public function edit($token, Observer $observer)
    {
        return $this->renderForm($observer, true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Observer  $observer
     * @return \Illuminate\Http\Response
     */
    public function update($token, ObserverRequest $request, Observer $observer)
    {
        $observer->update($request->except('_token', '_method'));
        return $this->actionRedirect('update', $observer->name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Observer  $observer
     * @return \Illuminate\Http\Response
     */
    public function destroy($token, Observer $observer)
    {
        $name = $observer->name;
        $observer->delete();
        return $this->actionRedirect('delete', $name);
    }

    /**
     * Handle store and update.
     *
     * @param  \App\Models\Observer  $observer
     * @param  boolean  $type
     * @return \Illuminate\Http\Response
     */
    private function renderForm($observer, $type)
    {
        return response()->json(
            view('observers._form', $this->prepareData([
                'observer' => $observer
                , 'categoryData' => $this->getCategory()
            ], ['form-horizontal'], $type))->render()
        );
    }

    /**
     * Handle store and update.
     *
     * @param  string  $method
     * @param  string  $name
     * @return \Illuminate\Http\Response
     */
    private function actionRedirect($mehtod, $name)
    {
        return redirect(\RouteHelper::token('observers.index'))
            ->withSuccess(
                trans("messages.success.$mehtod", [
                    'name' => sprintf('observer %s', strtolower($name))
                ])
            );
    }

    /**
     * Get observer category.
     *
     * @return \Illuminate\Http\Response
     */
    private function getCategory()
    {
        return Category::pluck('name', 'id');
    }
}
