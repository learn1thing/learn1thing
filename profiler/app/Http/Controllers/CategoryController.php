<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\DataTables\CategoryDataTable;
use App\Models\Survey;

class CategoryController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param App\DataTables\CategoryDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index($token, CategoryDataTable $dataTable)
    {
        return $dataTable->render('categories.index');
    }

    /**
     * Show the form for creating new resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function create($token, Category $category)
    {
        return $this->renderForm($category, false);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($token, CategoryRequest $request)
    {
        $category = Category::create($request->except('_token'));
        return $this->actionRedirect('create', $category->name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($token, Category $category)
    {
        return $this->renderForm($category, true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update($token, CategoryRequest $request, Category $category)
    {
        $category->update($request->except('_token', '_method'));
        return $this->actionRedirect('update', $category->name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($token, Category $category)
    {
        $name = $category->name;
        $category->delete();
        return $this->actionRedirect('delete', $name);
    }

    /**
     * Handle store and update.
     *
     * @param  \App\Models\Category  $category
     * @param  boolean  $type
     * @return \Illuminate\Http\Response
     */
    private function renderForm($category, $type)
    {
        return response()->json(
            view('categories._form', $this->prepareData([
                'category' => $category
                , 'surveyData' => $this->getSurvey()
            ], ['form-horizontal'], $type))->render()
        );
    }

    /**
     * Handle store and update.
     *
     * @param  string  $method
     * @param  string  $name
     * @return \Illuminate\Http\Response
     */
    private function actionRedirect($mehtod, $name)
    {
        return redirect(\RouteHelper::token('categories.index'))->withSuccess(
            trans("messages.success.$mehtod", [
                'name' => sprintf('category %s', strtolower($name))
            ])
        );
    }

    /**
     * Get survey data.
     *
     * @return \Illuminate\Http\Response
     */
    private function getSurvey()
    {
        return Survey::pluck('title', 'id');
    }
}
