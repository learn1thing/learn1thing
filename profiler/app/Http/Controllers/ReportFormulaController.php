<?php

namespace App\Http\Controllers;

use App\Models\ReportFormula;
use App\Models\Survey;
use App\Models\CategoryProfileDetail;
use Illuminate\Http\Request;
use App\DataTables\ReportFormulaDataTable;
use Client;

class ReportFormulaController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param App\DataTables\ReportFormulaDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index($token, ReportFormulaDataTable $dataTable)
    {
        return $dataTable->with('user_id', session('user')['id'])->render('report-formulas.index');
    }

    /**
     * Show the form for creating new resource.
     *
     * @param  \App\Models\ReportFormula $reportFormula
     * @return \Illuminate\Http\Response
     */
    public function create($token, ReportFormula $reportFormula)
    {
        $departments = [];
        $roles = [];
        $departmentRequest = Client::setEndpoint('department')->get();
        if ($departmentRequest['status']['success'] == true) {
            $departmentData = $departmentRequest['data'];
            foreach ($departmentData as $value) {
                $departments[] = array('id' => $value['id'], 'text' => $value['name']);
            }
        }
        $roleRequest = Client::setEndpoint('role')->get();
        if ($roleRequest['status']['success'] == true) {
            $roleData = $roleRequest['data'];
            foreach ($roleData as $value) {
                $roles[] = array('id' => $value['id'], 'text' => $value['name']);
            }
        }
        $profilers = Survey::where('creator_id', session('user')['id'])
            ->with('sections.questions')
            ->with(['formulas' => function ($query)
            {
                $query->where('source', 'raw_data');
                $query->where('is_parent', true);
            }])
            ->get()
            ->toArray();
        $profilersArray = array();
        foreach ($profilers as $key => $value) {
            $profiler = array(
                'id' => $value['id'],
                'title' => $value['title'],
                'questions' => array_collapse(array_pluck($value['sections'], 'questions')),
                'categories' => CategoryProfileDetail::where('category_profile_id', $value['category_profile_id'])->get()->toArray(),
                'formulas' => $value['formulas']
            );
            $profilersArray[] = $profiler;
        }
        
        return response()->json(
            view('report-formulas.create', $this->prepareData([
                'formula' => $reportFormula,
                'profilers' => $profilersArray,
                'departments' => $departments,
                'roles' => $roles
            ], [], false))->render()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($token, Request $request)
    {
        $reportFormula = ReportFormula::create($request->except('_token'));
        return $this->actionRedirect('create', $reportFormula->name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ReportFormula  $reportFormula
     * @return \Illuminate\Http\Response
     */
    public function edit($token, $id)
    {
        $departments = [];
        $roles = [];
        $departmentRequest = Client::setEndpoint('department')->get();
        if ($departmentRequest['status']['success'] == true) {
            $departmentData = $departmentRequest['data'];
            foreach ($departmentData as $value) {
                $departments[] = array('id' => $value['id'], 'text' => $value['name']);
            }
        }
        $roleRequest = Client::setEndpoint('role')->get();
        if ($roleRequest['status']['success'] == true) {
            $roleData = $roleRequest['data'];
            foreach ($roleData as $value) {
                $roles[] = array('id' => $value['id'], 'text' => $value['name']);
            }
        }
        $profilers = Survey::where('creator_id', session('user')['id'])
            ->with('sections.questions')
            ->with(['formulas' => function ($query)
            {
                $query->where('source', 'raw_data');
                $query->where('is_parent', true);
            }])
            ->get()
            ->toArray();
        $profilersArray = array();
        foreach ($profilers as $key => $value) {
            $profiler = array(
                'id' => $value['id'],
                'title' => $value['title'],
                'questions' => array_collapse(array_pluck($value['sections'], 'questions')),
                'categories' => CategoryProfileDetail::where('category_profile_id', $value['category_profile_id'])->get()->toArray(),
                'formulas' => $value['formulas']
            );
            $profilersArray[] = $profiler;
        }
        
        $reportFormula = ReportFormula::find($id);
        $formulaChildren = array();
        if(isset($reportFormula->reference_formula_id)) {
            $isStop = false;
            $formulaId = $reportFormula->reference_formula_id;
            while(!$isStop) {
                $formulaChild = ReportFormula::find($formulaId);
                if($formulaChild) {
                    $formulaChildren[] = $formulaChild->toArray();
                    if(isset($formulaChild->reference_formula_id)) {
                        $formulaId = $formulaChild->reference_formula_id;
                    } else {
                        $isStop = true;
                    }
                } else {
                    $isStop = true;
                }
            }
        }
        return response()->json(
            view('report-formulas.edit', $this->prepareData([
                'formula' => $reportFormula,
                'formulaChildren' => $formulaChildren,
                'profilers' => $profilersArray,
                'departments' => $departments,
                'roles' => $roles
            ], [], true))->render()
        );
    }

    /**
     * Handle store and update.
     *
     * @param  \App\Models\ReportFormula $reportFormula
     * @param  boolean  $type
     * @return \Illuminate\Http\Response
     */
    private function renderForm($reportFormula, $type)
    {
        $departments = [];
        $roles = [];
        $departmentRequest = Client::setEndpoint('department')->get();
        if ($departmentRequest['status']['success'] == true) {
            $departmentData = $departmentRequest['data'];
            foreach ($departmentData as $value) {
                $departments[] = array('id' => $value['id'], 'text' => $value['name']);
            }
        }
        $roleRequest = Client::setEndpoint('role')->get();
        if ($roleRequest['status']['success'] == true) {
            $roleData = $roleRequest['data'];
            foreach ($roleData as $value) {
                $roles[] = array('id' => $value['id'], 'text' => $value['name']);
            }
        }
        $profilers = Survey::where('creator_id', session('user')['id'])
            ->with('sections.questions')
            ->with('formulas')
            ->get()
            ->toArray();
        $profilersArray = array();
        foreach ($profilers as $key => $value) {
            $profiler = array(
                'id' => $value['id'],
                'title' => $value['title'],
                'questions' => array_collapse(array_pluck($value['sections'], 'questions')),
                'categories' => CategoryProfileDetail::where('category_profile_id', $value['category_profile_id'])->get()->toArray(),
            );
            $profilersArray[] = $profiler;
        }
        
        return response()->json(
            view('report-formulas._form', $this->prepareData([
                'formula' => $reportFormula,
                'profilers' => $profilersArray,
                'departments' => $departments,
                'roles' => $roles
            ], [], $type))->render()
        );
    }

    /**
     * Get survey data.
     *
     * @return \Illuminate\Http\Response
     */
    private function getSurvey()
    {
        return Survey::pluck('title', 'id');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Formula  $reportFormula
     * @return \Illuminate\Http\Response
     */
    public function destroy($token, $id)
    {
        $reportFormula = ReportFormula::find($id);
        $name = $reportFormula->name;
        $id = $reportFormula->id;

        // remove formula references
        if(isset($reportFormula->reference_formula_id)) {
            $isStop = false;
            $referenceId = $reportFormula->reference_formula_id;
            while(!$isStop) {
                $referenceFormula = ReportFormula::find($referenceId);
                if($referenceFormula) {
                    if(isset($referenceFormula->reference_formula_id)) {
                        $referenceId = $referenceFormula->reference_formula_id;
                    } else {
                        $isStop = true;
                    }
                    $referenceFormula->delete();
                } else {
                    $isStop = true;
                }
            }
        }

        // remove the parent formula
        $reportFormula->delete();

        return $this->actionRedirect('delete', $name);
    }

    /**
     * Handle store and update.
     *
     * @param  string  $method
     * @param  string  $name
     * @return \Illuminate\Http\Response
     */
    private function actionRedirect($mehtod, $name)
    {
        return redirect(\RouteHelper::token('formulas.index'))->withSuccess(
            trans("messages.success.$mehtod", [
                'name' => sprintf('formula %s', strtolower($name))
            ])
        );
    }
}
