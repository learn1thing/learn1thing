<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Client;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        // Client::setEndpoint('logout')->setHeaders([
        //     'Authorization' => session('user.token')
        // ])->post();

        // request()->session()->flush();

        // request()->session()->regenerate();
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, env('IPSERVER_URL').'/api/v1/logout');IPSERVER_URL
        //curl_setopt($ch,CURLOPT_URL, "http://13.228.157.23/dashboard/api/v1/logout");
        //curl_setopt($ch,CURLOPT_URL, env('DASHBOARD_URL').'/api/v1/logout');
        //curl_setopt($ch,CURLOPT_HTTPHEADER, $parameters);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
         'Authorization:'. $_COOKIE['token']
        ));
        //execute post
        $result = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        //close connection
        curl_close($ch);

        //return redirect(env('DASHBOARD_URL', 'http://beta.learn1thing.com/dashboard'));
        return redirect(env('DASHBOARD_URL', 'https://store.learn1thing.com/dashboard'));
    }
}
