<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Batch;
use App\Models\Survey;
use App\Models\AnswerHeader;
use App\Models\AnswerDetail;
use App\Models\UserSurvey;
use App\Models\Observer;
use App\Models\Attribute;
use App\Models\Question;
use App\Models\Category;
use App\Models\CategoryProfileDetail;
use App\Models\ReportFormula;
use App\Models\EmailBuilder;
use App\Jobs\SendReminderPostSurvey;
use App\Helpers\RouteHelper;
use Response;
use DateTime;
use App\Notifications\Mail as MailNotification;
use Client;
use Mail;
use Validator;
use DB;
use Storage;
use Illuminate\Support\Facades\Log;

include(app_path().'/Helpers/simple_html_dom.php');

class PreviewController extends Controller
{

    /**
     * Show the form for creating new resource.
     *
     * @param  string  $type
     * @param  string  $code
     * @return \Illuminate\Http\Response
     */
    public function show($type, $code, Request $request)
    {
	Log::info(session('user'));
        if(!empty($request->get('address'))){
            return $this->handleMoodle($code, $request);
        }

        $base_code = $code;
        $batch = Batch::where('batch', '=', $code)->first();

        if(!$batch && $type !== "survey") {
            return redirect(ENV('DASHBOARD_URL'));
        } else if(isset($batch->is_done) && $batch->is_done == 1) {
            $this->thanks($type, $code);
        }

        if ($type === "batch" || $type === 'customer') {
            Log::info('batch & customer');
            if ($batch->is_done) {
                return $this->thanks($type, $code);
            }

            $survey = $batch ? $batch->survey : null;
            $observer = $batch ? $batch->observer : null;
            
            // variable replacement on observer description
            $surveyTitle = $survey->title;
            $buyerName = null;
            if(isset($batch->userSurvey->observer)) {
                $buyerName = $batch->userSurvey->observer->first_name . ' ' . $batch->userSurvey->observer->last_name;
            }
            $observerName = isset($batch->observer) ? $batch->observer->first_name . ' ' . $batch->observer->last_name : '';
            $survey->description = str_replace('[survey-title]', $surveyTitle, $survey->description);
            $survey->description = str_replace('[buyer-name]', $buyerName, $survey->description);
            $survey->description = str_replace('[observer-name]', $observerName, $survey->description);

            $survey->instruction = str_replace('[survey-title]', $surveyTitle, $survey->instruction);
            $survey->instruction = str_replace('[buyer-name]', $buyerName, $survey->instruction);
            $survey->instruction = str_replace('[observer-name]', $observerName, $survey->instruction);

            $survey->observer_description = str_replace('[survey-title]', $surveyTitle, $survey->observer_description);
            $survey->observer_description = str_replace('[buyer-name]', $buyerName, $survey->observer_description);
            $survey->observer_description = str_replace('[observer-name]', $observerName, $survey->observer_description);
        } else {
            Log::info('Selain batch & customer');
            $code = openssl_decrypt(base64_decode(strtr($code, '-_,', '+/=')), 'AES-128-ECB', 'l1tprofiler');
            $survey = Survey::find($code);
            if($survey) {
                $observer = count($survey->ownerObserver()) > 0 ? $survey->ownerObserver()[0] : [];
            } else {
                return redirect(ENV('DASHBOARD_URL'));
            }
        }

        

        if ($survey) {
            $userSurvey = UserSurvey::where('survey_id', $survey->id)->where('observer_id', ($observer) ? $observer->id : 0)->first();
            $observerCategories = array();
            $profileCategories = array();
            if($userSurvey) {
                $observerCategories = CategoryProfileDetail::with(['observers' => function ($query) use($userSurvey, $survey)
                {
                    $query->with('batch');
                    $query->with('answer');
                    $query->whereHas('batch', function ($q) use($userSurvey)
                    {
                        $q->where('user_survey_id', $userSurvey->id);
                    });
                }])
                ->where('category_profile_id', $survey->category_profile_id)
                ->get()
                ->toArray();

                $profileCategories = CategoryProfileDetail::where('category_profile_id', $survey->category_profile_id)
                    ->get()
                    ->toArray();
                $categoryIds = array_pluck($observerCategories, 'id');
                if(count($categoryIds) > 0) {
                    $profileCategories = array_where($profileCategories, function ($value, $key) use($categoryIds) {
                        return !in_array($value['id'], $categoryIds);
                    });
                }

                foreach ($observerCategories as &$category) {
                    foreach($category['observers'] as &$observer) {
                        $observer['url_link'] = RouteHelper::check('surveys.preview', ['type' => 'batch', 'code' => $observer['batch']['batch']]);
                        $observer['status'] = 'pending';
                        if($observer['answer']) {
                            if(!is_null($observer['answer']['start_time']) && !is_null($observer['answer']['end_time'])) {
                                $observer['status'] = 'submitted';
                            }
                        }
                        unset($observer['answer']);
                    }
                }
            }

            // get previous answers
            $file = 'answer-backups/' . $code . '.txt';
            Log::info('lokasi backupAnswers : '.$file);
            $isExists = Storage::disk('local')->exists($file);
            $answers = array();
            $startTime = date('Y-m-d h:i:s');
            if($isExists) {
                $content = Storage::get($file);
                $content = json_decode($content, true);
                if(isset($content['start_time'])) {
                    $startTime = $content['start_time'];
                }
                if(isset($content['questionnaire'])) {
                    $questionnaire = $content['questionnaire'];
                    foreach ($questionnaire as $key => $value) {
                        if(is_array($value) || is_object($value)) {
                            $answers[$key] = json_encode($value);
                        } else {
                            $answers[$key] = $value;
                        }
                    }
                }
                if(isset($content['preliminary'])) {
                    $preliminary = $content['preliminary'];
                    foreach ($preliminary as $key => $value) {
                        if(is_array($value) || is_object($value)) {
                            $answers[$key] = json_encode($value);
                        } else {
                            $answers[$key] = $value;
                        }
                    }
                }
                if(isset($content['post'])) {
                    $post = $content['post'];
                    foreach ($post as $key => $value) {
                        if(is_array($value) || is_object($value)) {
                            $answers[$key] = json_encode($value);
                        } else {
                            $answers[$key] = $value;
                        }
                    }
                }
            }
            $answers = collect($answers);

            $data = [
                'survey' => $survey
                , 'observer' => $observer
                , 'start_time' => $startTime
                // , 'category' => $observer->category ? $observer->category->name : '-'
                , 'categories' => $profileCategories
                , 'observer_categories' => $observerCategories
                , 'id' => strtr(base64_encode(openssl_encrypt($survey->id, 'AES-128-ECB', 'l1tprofiler')), '+/=', '-_,')
                , 'type' => $type
                , 'code' => $base_code
                , 'userSurvey' => $userSurvey
                , 'user_id' => $userSurvey ? $userSurvey->user_id : 0
                , 'status' => 'on-progress'
            ];

            if(count($answers) > 0) {
                $data['answers'] = $answers;
            }
            // print_r($data['observer']->toArray());
            // die();

            if($type === "customer"){
                $position = $userSurvey->position;
                if($position == 'post' && isset($survey->type)) {
                    $answerHeader = AnswerHeader::where('survey_id', $survey->id)
                        ->where('observer_id', $observer->id)
                        ->first();
                    $reminderType = $survey->type;
                    $reminderValue = $survey->value;
                    $reminderTime = strtotime($answerHeader->end_time . '+' . $reminderValue . ' ' . $reminderType);
                    $postSurveys = $survey->postSurveys();
                    $currentTime = strtotime(date('Y-m-d H:i:s'));
                    if($currentTime < $reminderTime) {
                        Log::info('Masuk thanks');
                        return redirect()->route('surveys.thanks', ['type' => $type, 'code' => $code]);
                    }
                }
            }
            Log::info('Masuk disini....');
            return view('previews.show', $data);
        }
        return redirect(ENV('DASHBOARD_URL'));
    }

    public function store(Request $request) {
        $base_code = $request->input('link_code');
        if ($request->input('link_type') == "batch") {
            $batch = Batch::where('batch', '=', $request->input('link_code'))->first();
            $survey = $batch->survey;
            $observer = $batch->observer;
        } else if($request->input('link_type') == 'customer') {
            $code = openssl_decrypt(base64_decode(strtr($request->input('link_code'), '-_,', '+/=')), 'AES-128-ECB', 'l1tprofiler');
            // index 0: product_id, 1: survey_id, 2: user_id, 3: observer_id
            $ids = explode('+', $code);
            $surveyId = $ids[1];
            $observerId = $ids[3];
            $batch = Batch::where('batch', '=', $base_code)->first();
            if(isset($surveyId)) {
                $survey = Survey::find($surveyId);
                $observer = Observer::find($observerId);//$survey->ownerObserver() ? $survey->ownerObserver()[0] : [];
            } else {
                $survey = null;
                $observer = null;
            }
        } else {
            $code = openssl_decrypt(base64_decode(strtr($request->input('link_code'), '-_,', '+/=')), 'AES-128-ECB', 'l1tprofiler');
            $survey = Survey::find($code);
            $observer = $survey->ownerObserver() ? $survey->ownerObserver()[0] : [];
        }

        $answerHeader = AnswerHeader::where('survey_id', $survey->id)->where('observer_id', $observer->id)->first();

        if ($request->exists('preliminary')) {
            $request->merge(['position' => 'question']);
        } else if ($request->exists('questionnaire')) {
            $request->merge(['position' => 'post']);
        }

        if (!$answerHeader) {
            $request->merge(['survey_id' => $survey->id]);
            $request->merge(['observer_id' => $observer->id]);
            $request->merge(['end_time' => date('Y-m-d h:i:s')]);
            $request->merge(['is_invalid' => false]);

            $answerParams = array();
            if(!$request->exists('questionnaire')) {
                $answerParams = $request->except('link_code', 'link_type', '_token', 'preliminary', 'questionnaire', 'post', 'start_time', 'end_time');
            } else {
                $answerParams = $request->except('link_code', 'link_type', '_token', 'preliminary', 'questionnaire', 'post');
            }
            $answerHeader = AnswerHeader::create($answerParams);
        } else {
            if($request->exists('questionnaire')) {
                $answerHeaderParams = $request->only('start_time');
                $answerHeaderParams['end_time'] = date('Y-m-d H:i:s');
                $answerHeader->update($answerHeaderParams);
            }
        }
        if ($request->exists('preliminary')) {
            $answers = $request->input('preliminary');
        } else if ($request->exists('questionnaire')) {
            $answers = $request->input('questionnaire');
        } else if ($request->exists('post')) {
            $answers = $request->input('post');
        } else {
            $answers = null;
        }

        if ($answers) {
            $insert = array();
            foreach ($answers as $question_id => $answer) {
                $insert[] = array(
                    'question_id' => $question_id
                    , 'answer' => gettype($answer) == "array" || gettype($answer) == "object" ? json_encode($answer) : $answer
                );
            }
            $answerHeader->answerDetails()->createMany($insert);
        }

        // update the position
        UserSurvey::where('survey_id', $survey->id)
                ->where('observer_id', $observer->id)
                ->update(['position' => $request->position]);

        // remove the backup file that stored the answers data
        $file = 'answer-backups/' . $batch->batch . '.txt';
        $isExists = Storage::disk('local')->exists($file);
        if($isExists) {
            Storage::disk('local')->delete($file);
        }

        if ($request->exists('preliminary')) {
            return redirect(
                \RouteHelper::check('surveys.preview', [
                    'type' => $request->input('link_type')
                    , 'code' => $request->input('link_code')
                ])
            );
        } else {
            $postSurveys = $survey->postSurveys();
            $reminderType = $survey->type;
            $reminderValue = $survey->value;
            if($batch && $request->exists('questionnaire') && count($postSurveys) > 0 && $reminderType && $request->input('link_type') === 'customer') {
                $startDate = new DateTime(date('Y-m-d H:i:s'));
                $endDate = new DateTime(date('Y-m-d H:i:s', strtotime('+' . $reminderValue . ' ' . $reminderType)));
                // fixed_cmp_reminder is_checked
                if ($survey->fixed_cmp_reminder == 1) {
                    $endDate = new DateTime(date('Y-m-d H:i:s', strtotime($request->cmp_reminder_date)));
                }

                $result = $startDate->diff($endDate);
                // $reminderInHours = $result->h;
                $reminderInMinutes = $result->i;
                $job = (new SendReminderPostSurvey($batch, $survey))
                    ->delay(Carbon::now()->addMinutes($reminderInMinutes));
                $this->dispatch($job);
            } else if ($batch && $request->exists('questionnaire') && count($postSurveys) == 0 || $batch && $request->exists('post') || $batch && $request->exists('questionnaire') && count($postSurveys) > 0 && $request->input('link_type') === 'batch') {
                $batch->update(['is_done' => true]);
            } else if($batch && $request->exists('questionnaire') && count($postSurveys) > 0 && !$reminderType) {
                return redirect(
                    \RouteHelper::check('surveys.preview', [
                        'type' => $request->input('link_type')
                        , 'code' => $request->input('link_code')
                    ])
                );
            }
            return redirect(
                \RouteHelper::check('surveys.thanks', [
                    'type' => $request->input('link_type')
                    , 'code' => $request->input('link_code')
                ])
            );
        }
        return redirect(ENV('DASHBOARD_URL'));
    }

    public function thanks($type, $code)
    {
        $base_code = $code;
        if ($type == "batch") {
            $batch = Batch::where('batch', '=', $code)->first();
            $survey = $batch->survey;
            $observer = $batch->observer;
            
            $buyerName = null;
            if(isset($batch->userSurvey->observer)) {
                $buyerName = $batch->userSurvey->observer->first_name . ' ' . $batch->userSurvey->observer->last_name;
            }

            $survey->thanks_message = str_replace('[survey-title]', $survey->title, $survey->thanks_message);
            $survey->thanks_message = str_replace('[buyer-name]', $buyerName, $survey->thanks_message);

            $data = [
                'survey' => $survey
                , 'observer' => $observer
                , 'id' => strtr(base64_encode(openssl_encrypt($survey->id, 'AES-128-ECB', 'l1tprofiler')), '+/=', '-_,')
                , 'type' => $type
                , 'code' => $base_code
                , 'thanks_message' => $survey->thanks_message
                , 'redirect_after_completion' => $survey->redirect_after_completion
                , 'redirect_after_completion_url' => $survey->redirect_after_completion_url
            ];
            return view('previews.thanks', $data);

        } else if($type == "customer") {
            $rawCode = $code;
            $code = openssl_decrypt(base64_decode(strtr($code, '-_,', '+/=')), 'AES-128-ECB', 'l1tprofiler');
            // index 0: product_id, 1: survey_id, 2: user_id, 3: observer_id
            $ids = explode('+', $code);
            $surveyId = $ids[1];
            $observerId = $ids[3];
            $batch = Batch::where('batch', '=', $rawCode)->first();
            if(isset($surveyId)) {
                $survey = Survey::find($surveyId);
                $observer = Observer::find($observerId);//$survey->ownerObserver() ? $survey->ownerObserver()[0] : [];
            } else {
                $survey = null;
                $observer = null;
            }
        } else {
            $code = openssl_decrypt(base64_decode(strtr($code, '-_,', '+/=')), 'AES-128-ECB', 'l1tprofiler');
            $survey = Survey::find($code);
            $observer = $survey->ownerObserver() ? $survey->ownerObserver()[0] : [];
        }

        // variable replacement on observer description
        $surveyTitle = $survey->title;
        $buyerName = null;
        if(isset($batch->userSurvey->observer)) {
            $buyerName = $batch->userSurvey->observer->first_name . ' ' . $batch->userSurvey->observer->last_name;
        }
        $observerName = isset($batch->observer) ? $batch->observer->first_name . ' ' . $batch->observer->last_name : '';
        $survey->description = str_replace('[survey-title]', $surveyTitle, $survey->description);
        $survey->description = str_replace('[buyer-name]', $buyerName, $survey->description);
        $survey->description = str_replace('[observer-name]', $observerName, $survey->description);

        $survey->instruction = str_replace('[survey-title]', $surveyTitle, $survey->instruction);
        $survey->instruction = str_replace('[buyer-name]', $buyerName, $survey->instruction);
        $survey->instruction = str_replace('[observer-name]', $observerName, $survey->instruction);

        $survey->observer_description = str_replace('[survey-title]', $surveyTitle, $survey->observer_description);
        $survey->observer_description = str_replace('[buyer-name]', $buyerName, $survey->observer_description);
        $survey->observer_description = str_replace('[observer-name]', $observerName, $survey->observer_description);

        if ($survey) {
            $userSurvey = UserSurvey::where('survey_id', $survey->id)->where('observer_id', ($observer) ? $observer->id : 0)->first();
            
            $observerCategories = array();
            $profileCategories = array();
            $answers = null;
            if ($userSurvey) {
                $answers = AnswerHeader::where('answer_headers.survey_id', $userSurvey->survey_id)
                    ->where('answer_headers.observer_id', $userSurvey->observer_id)
                    ->join('answer_details', 'answer_details.answer_header_id', '=', 'answer_headers.id')
                    ->pluck('answer_details.answer', 'answer_details.question_id');

                $observerCategories = CategoryProfileDetail::with(['observers' => function ($query) use($userSurvey, $survey)
                {
                    $query->with('batch');
                    $query->with('answer');
                    $query->whereHas('batch', function ($q) use($userSurvey)
                    {
                        $q->where('user_survey_id', $userSurvey->id);
                    });
                }])
                ->where('category_profile_id', $survey->category_profile_id)
                ->get()
                ->toArray();

                $profileCategories = CategoryProfileDetail::where('category_profile_id', $survey->category_profile_id)
                    ->get()
                    ->toArray();
                $categoryIds = array_pluck($observerCategories, 'id');
                if(count($categoryIds) > 0) {
                    $profileCategories = array_where($profileCategories, function ($value, $key) use($categoryIds) {
                        return !in_array($value['id'], $categoryIds);
                    });
                }

                foreach ($observerCategories as &$category) {
                    foreach($category['observers'] as &$observer) {
                        $observer['url_link'] = RouteHelper::check('surveys.preview', ['type' => 'batch', 'code' => $observer['batch']['batch']]);
                        $observer['status'] = 'pending';
                        if($observer['answer']) {
                            if(!is_null($observer['answer']['start_time']) && !is_null($observer['answer']['end_time'])) {
                                $observer['status'] = 'submitted';
                            }
                        }
                        unset($observer['answer']);
                    }
                }
            }

            $data = [
                'survey' => $survey
                , 'observer' => $observer
                // , 'category' => $observer->category ? $observer->category->name : '-',
                , 'start_time' => null
                , 'categories' => $profileCategories
                , 'observer_categories' => $observerCategories
                , 'id' => strtr(base64_encode(openssl_encrypt($survey->id, 'AES-128-ECB', 'l1tprofiler')), '+/=', '-_,')
                , 'type' => $type
                , 'code' => $base_code
                , 'userSurvey' => $userSurvey
                , 'answers' => $answers
                , 'user_id' => $userSurvey ? $userSurvey->user_id : 0
                , 'thanks_message' => $survey->thanks_message
                , 'status' => 'done'
            ];
            return view('previews.show', $data);
        }
        return redirect(ENV('DASHBOARD_URL'));
    }

    public function prints($userSurvey)
    {
        $userSurveyId = openssl_decrypt(base64_decode(strtr($userSurvey, '-_,', '+/=')), 'AES-128-ECB', 'l1tprofiler');

        $userSurvey = UserSurvey::find($userSurveyId);
        $answers = AnswerHeader::where('answer_headers.survey_id', $userSurvey->survey_id)
            ->where('answer_headers.survey_id', $userSurvey->survey_id)
            ->join('answer_details', 'answer_details.answer_header_id', '=', 'answer_headers.id')
            ->pluck('answer_details.answer', 'answer_details.question_id');
        $data = [
            'userSurvey' => $userSurvey
            , 'answers' => $answers
        ];

        if (empty($userSurvey)) {
            return redirect(env('DASHBOARD_URL', 'http://beta.learn1thing.com/dashboard'));
        }

        $baseUrl = storage_path()."/report/".$userSurveyId;
        $renderFile = view('report.merge', $data)->render();
        // dd($renderFile);
        exec("mkdir ".$baseUrl);
        file_put_contents($baseUrl.'/report.html', $renderFile);

        return view('report.merge', $data);
    }

    // public function printReport($token, $batch)
    public function printReport($batch)
    {
	//Log::info('Masuk print sini');
	//Log::info($batch);
        // dd(openssl_decrypt(base64_decode(strtr('dEJBM0tOeVN4NzNvWWlLTWxhcTlsQT09', '-_,', '+/=')), 'AES-128-ECB', 'l1tprofiler'));
        // index 0: product_id, 1: survey_id, 2: user_id, 3: observer_id
        // [survey-title]
        // [survey-batch_reference]
        // [survey-description]
        // [survey-period_start]
        // [survey-period_end]
        // [section-section_title-title]
        // [section-section_title-description]
        // [question-section_title-question_number]
        // [formula-formula_name]

        $oriBatch = $batch;
        $batch = openssl_decrypt(base64_decode(strtr($batch, '-_,', '+/=')), 'AES-128-ECB', 'l1tprofiler');
        // index 0: product_id, 1: survey_id, 2: user_id, 3: observer_id
        $ids = explode('+', $batch);

	    Log::info($ids);
        

        $buyer = Observer::findOrFail($ids[3]);
        $survey = Survey::findOrFail($ids[1]);
        $userSurvey = UserSurvey::where('product_id', $ids[0])
            ->where('survey_id', $ids[1])
            ->where('user_id', $ids[2])
            ->where('observer_id', $ids[3])
            ->first();

        if (@$ids[4]) {
            $reportData = EmailBuilder::find($ids[4]);

        } else {
            if ($ids[1]) {
                $email_builder_id=$survey->email_builder_id;
                $reportData = EmailBuilder::find($email_builder_id);
            }else{
                $reportData = $survey->defaultReport;
            }
        }


    // Log::info('Survey');
    // Log::info($survey);

        // new
        $html = @$reportData ? (empty($reportData->content) ? '<div></div>' : htmlspecialchars_decode($reportData->content)) : '<div></div>';
        $htmlDOM = str_get_html($html);

        $style = '<style type="text/css">' . view('template-builders.covers.css')->render() . '</style>';
        $cover = @$reportData ? $reportData->cover_style : 'style_1';
        $covers = 'template-builders.' . $cover . '.cover';


        if (in_array($cover, ['style_0', 'style_1', 'style_2', 'style_3', 'style_4', 'style_5'])) {
            if ( view()->exists('template-builders.covers.' . $cover) ) {
                $covers = 'template-builders.covers.' . $cover;
            }
            $base = $style . htmlspecialchars_decode(view($covers)->render()) . htmlspecialchars_decode(view('template-builders.covers.' . $cover . '_format')->render());
            $footer = htmlspecialchars_decode(view('template-builders.covers.' . $cover . '_footer')->render());
            //print_r($base);exit;
        } else {
            if ( view()->exists('template-builders.' . $cover . '.cover') ) {
                $covers = 'template-builders.' . $cover . '.cover';
            }
            $base = $style . htmlspecialchars_decode(view($covers)->render()) . htmlspecialchars_decode(view('template-builders.' . $cover . '.format')->render());
            $footer = htmlspecialchars_decode(view('template-builders.' . $cover . '.footer')->render());
        }


        Log::info('[communication-style]');
        foreach ($htmlDOM->find('code') as $code) {
            if ($code->innertext() == "[communication-style]") {
                Log::info('passed');
                $questionsComStyle = array();
                $comStyleAttributes = array();
                foreach ($survey->attributes as $attribute) {
                    $comStyleAttributes[$attribute->id] = $attribute->name;
                }
                $index = 0;
                foreach ($survey->sections as $section) {
                    foreach ($section->questions as $question) {
                        $questionsComStyle[$index] = $question->toArray();
                        $answerHeaderData = AnswerHeader::where('answer_headers.survey_id', $ids[1])
                            ->where('answer_details.question_id', $question->id)
                            ->join('answer_details', 'answer_details.answer_header_id', '=', 'answer_headers.id')
                            ->where('answer_headers.observer_id', $ids[3])
                            ->select('answer_details.answer', 'answer_headers.survey_id', 'answer_details.question_id', 'answer_details.answer_header_id', 'answer_headers.observer_id')
                            ->first();

                        if ($answerHeaderData) {
                            $questionsComStyle[$index]['answers'] = $answerHeaderData->toArray();
                        } else {
                            $questionsComStyle[$index]['answers'] = array('answer' => '');
                        }

                        $index += 1;
                    }
                }
                Log::info('QuestionStyle');
                Log::info($questionsComStyle);
                $html = str_replace($code, htmlspecialchars_decode(view('template-builders.communication-style')->withQuestions($questionsComStyle)->withAttributes($comStyleAttributes)->render()), $html);
            }
        }

        $htmlDOM = str_get_html($html);
        //print_r($htmlDOM);exit;
        foreach ($htmlDOM->find('code') as $key => $code) {
            if ($code->innertext() == '[new-page]') {
                $target = $code->parent()->parent();
                $className = $target->getAttribute('class') . " force-float";

                $target->setAttribute('class', $className);
                $code->setAttribute('data-page', $key + 2);
            }
        }

        $html = $htmlDOM;

        // [starhub-johari] per attribute
        Log::info('starhub-johari');
        foreach ($htmlDOM->find('.starhub-johari') as $code) {
            $text = explode('-', $code->innertext());
            $attribute = Attribute::find($text[2]);

            if ($attribute) {
                Log::info('Passed');
                $johariDetail = '';
                $attributes = array();

                $dataPersonal = $this->dataPersonalAttribute($ids[1], $ids[3], $text[2]);
                $attributes = $this->getPersonalScoreAttribute($dataPersonal, 'Self', $text[2], $attributes);

                foreach($survey->savedCategories($ids[2])->get() as $category) {
                    foreach ($category->observers as $observer) {
                        $tempDataPersonal = $this->dataPersonalAttribute($ids[1], $observer->id, $text[2]);

                        if (count($tempDataPersonal) > 0) {
                            $dataPersonal = $tempDataPersonal;
                            $attributes = $this->getPersonalScoreAttribute($dataPersonal, $category->name, $text[2], $attributes);

                        } else {
                            if (!isset($attributes[$text[2]])){
                                $attributes[$text[2]] = array();
                            }

                            if (!isset($attributes[$text[2]]['Self'])) {
                                $newVar = array();
                                foreach ($attributes[$text[2]] as $values) {
                                    foreach ($values as $key => $value) {
                                        $newVar['Self'][$key] = 0;
                                    }
                                }
                                $attributes[$text[2]] = array_merge($newVar, $attributes[$text[2]]);
                            }

                            $attributes[$text[2]][$category->name] = $attributes[$text[2]]['Self'];

                            foreach ($attributes[$text[2]][$category->name] as $key => $value) {
                                $attributes[$text[2]][$category->name][$key] = 0;
                            }
                        }
                    }

                    if (isset($attributes[$text[2]][$category->name])) {
                        $totalObs = 0;
                        foreach ($category->observers as $observer) {
                            if ($observer->answerHeader->count() > 0) {
                                $totalObs += 1;
                            }
                        }

                        foreach ($dataPersonal as $master) {
                            $attributes[$text[2]][$category->name][$master->question_id] = $attributes[$text[2]][$category->name][$master->question_id] / ($totalObs > 0 ? $totalObs : 1);
                        }
                    }
                }

                foreach ($dataPersonal as $master) {
                    $self = array('Self' => $attributes[$master->attribute_id]['Self']);
                    unset($attributes[$master->attribute_id]['Self']);
                    $obsAve = array('Obs. Ave' => array());
                    $attributes[$master->attribute_id] = array_merge($self, $obsAve, $attributes[$master->attribute_id]);

                    foreach ($attributes as $key => $data) {
                        $count = 0;
                        $last = 0;
                        foreach ($data as $name => $values) {
                            if ($name != 'Self' && $name != 'Obs. Ave') {
                                $last += $values[$master->question_id];
                                $count += 1;
                            }
                        }

                        $attributes[$master->attribute_id]['Obs. Ave'][$master->question_id] = round($last / ($count == 0 ? 1 : $count), 2);
                    }
                }

                foreach (array_chunk($attributes, 2, true) as $attributeData) {
                    $newId = array();
                    foreach ($attributeData as $key => $value) {
                        $newId[] = $key;
                    }
                    $attribute = Attribute::whereIn('id', $newId)->get();
                    if ($attribute) {
                        // $johariDetail .= view('template-builders.' . $cover . '.johari', compact('attribute', 'attributeData', 'dataPersonal'))->render();
                        $johariDetail .= view('template-builders.starhub.johari', compact('attribute', 'attributeData', 'dataPersonal'))->render();
                    }
                }
                $html = str_replace($code, htmlspecialchars_decode($johariDetail), $html);
            }
        }

        // render report formulas
        foreach($htmlDOM->find('.replace-formula code') as $wrapper) {
            $formulaId = str_replace('formula-', '', $wrapper->innertext());
            $reportFormula = ReportFormula::find($formulaId);
            $result = $this->mappingReportFormula($reportFormula);
            $html = str_replace($wrapper, $reportFormula->name . ' : ' . $result, $html);
        }
        // the end of report formulas
        

        Log::info('tag code selain [communication-style]');        
        foreach ($htmlDOM->find('code') as $code) {

            $text = explode('-', $code->innertext());

            if ($code->innertext() !== "[communication-style]") {
                $question = Question::find($text[1]);

                Log::info('Passed');
                if ($question) {
                    if ($text[0] == "answer") {
                        $answers = AnswerHeader::where('answer_headers.survey_id', $ids[1])
                            ->where('answer_details.question_id', $text[1])
                            ->join('answer_details', 'answer_details.answer_header_id', '=', 'answer_headers.id');

                        if ($text[2] == "primary_user") {
                            $answers = $answers->where('answer_headers.observer_id', $ids[3]);
                        }

                        $answers = $answers->pluck('answer_details.answer', 'answer_details.question_id');

                        if (count($answers) == 0) {
                            $answers = null;
                        }

                        $html = str_replace($code, htmlspecialchars_decode(view('template-builders.answer', compact('question'))->withAnswers($answers)->render()), $html);

                    } else if ($text[0] == "percentage") {
                        $answers = AnswerHeader::where('answer_headers.survey_id', $ids[1])
                            ->where('answer_details.question_id', $text[1])
                            ->join('answer_details', 'answer_details.answer_header_id', '=', 'answer_headers.id')
                            ->pluck('answer_details.answer', 'answer_details.question_id');

                        $percentage = array();

                        $rawData = AnswerHeader::select('answer_details.answer', DB::raw('count(answer_details.id) as count'))
                            ->where('answer_headers.survey_id', $ids[1])
                            ->where('answer_details.question_id', $text[1])
                            ->join('answer_details', 'answer_details.answer_header_id', '=', 'answer_headers.id')
                            ->groupBy('answer_details.answer');

                        $rawCount = count($rawData->get());

                        foreach ($rawData->get() as $data) {
                            $percentage[$data->answer] = round(($data->count / $rawCount)*100, 2);
                        }

                        // if($question->answer_type == "checkboxes" || $question->answer_type == "multiple_choice_with_constraint") {

                        //     if($question->answer_type == "multiple_choice_with_constraint") {
                        //         $options = $settings->options;

                        //     } else {
                        //         $options = $question->answer_configs;

                        //     }

                        // } else if($question->answer_type == "multichoice" || $question->answer_type == "boolean" || $question->answer_type == "likert_scale") {
                        // }

                        if (count($answers) == 0) {
                            $answers = null;
                        }

                        $html = str_replace($code, htmlspecialchars_decode(view('template-builders.percentage', compact('question'))->withAnswers($answers)->withPercentage($percentage)->render()), $html);

                    } else if ($text[0] == "johari") {
                        // johari detail
                        Log::info('create johari');
                        $johariDetail = '';
                        $questions = array();

                        $dataPersonal = $this->dataPersonal($ids[1], $ids[3], $text[1]);
                        $questions = $this->getPersonalScore($dataPersonal, 'Self', $question->id, $questions);

                        foreach($survey->savedCategories($ids[2])->get() as $category) {
                            foreach ($category->observers as $observer) {
                                $dataPersonal = $this->dataPersonal($ids[1], $observer->id, $question->id);
                                $questions = $this->getPersonalScore($dataPersonal, $category->name, $question->id, $questions);
                            }

                            if (isset($questions[$question->id][$category->name])) {
                                $totalObs = 0;
                                foreach ($category->observers as $observer) {
                                    if ($observer->answerHeader->count() > 0) {
                                        $totalObs += 1;
                                    }
                                }
                                // $questions[$question->id][$category->name] = $questions[$question->id][$category->name] / ($totalObs > 0 ? $totalObs : 1);
                                $questions[$question->id][$category->name] =round(($questions[$question->id][$category->name] / ($totalObs > 0 ? $totalObs : 1)),2);
                                Log::info('category name: '.$category->name);
                            }
                        }

                        foreach (array_chunk($questions, 2, true) as $questionData) {
                            $newId = array();
                            foreach ($questionData as $key => $value) {
                                $newId[] = $key;
                            }
                            $question = Question::whereIn('id', $newId)->get();
                            if ($question) {
                                $johariDetail .= view('template-builders.johari', compact('question', 'questionData'))->render();
                            }
                        }

                        $html = str_replace($code, htmlspecialchars_decode($johariDetail), $html);

                    }
                }
            }
        }

        Log::info('Create Response table');
        //Create table respon table
        foreach ($htmlDOM->find('code') as $code) {

            if ($code->innertext() == '[response-table]' || $code->innertext() == '[response-tquestion]') {
                // summary
                Log::info('Passed');
                $responseTable = '';
                $dataPersonal = $this->dataPersonal($ids[1], $ids[3], 0);
                // $dataPersonal = $this->dataPersonal($ids[1], $ids[3], $question->id);
                $attributes = array();

                
                // if ($code->innertext() == '[response-table]') {
                //     foreach ($survey->attributes as $attribute) {
                //         $attributes[$attribute->id] = array('Self' => 0);
                //     }
                // } else {
                //     foreach ($survey->sections as $section) {
                //         foreach ($section->questions as $question) {
                //             $attributes[$question->id] = array('Self' => 0);
                //         }
                //     }   
                // }
                
               
               // foreach ($survey->sections as $section) {
               //      foreach ($section->questions as $question) {
               //          $attributes[$question->id] = array('Self' => 0);
               //      }
               //  }


                
                if ($code->innertext() == '[response-table]') {
                    $attributes = $this->getScore($dataPersonal, 'Self', 0, $attributes);
                    // $attributes = $this->getScore($dataPersonal, 'Self', $question->id, $attributes);
                } else {
                    $attributes = $this->getScoreTQuestion($dataPersonal, 'Self', 0, $attributes);
                }

                foreach($survey->savedCategories($ids[2])->get() as $category) {
                    foreach ($category->observers as $observer) {
                        // $dataPersonal = $this->dataPersonal($ids[1], $observer->id, $question->id);
                        // Log::info($category);
                        // Log::info($category->observers);
                        $dataPersonal = $this->dataPersonal($ids[1], $observer->id, 0);
                        if ($code->innertext() == '[response-table]') {
                            $attributes = $this->getScore($dataPersonal, $category->name, $observer->id, $attributes);
                            //Log::info('Array attributes');
                            //Log::info($attributes);
                        } else {
                            $attributes = $this->getScoreTQuestion($dataPersonal, $category->name, $observer->id, $attributes);
                            //Log::info('Array Question');
                            //Log::info($attributes);
                        }
                    }
                }


                $responseTable = '<br><br><br><br><br><table class="table-style-table">';

                $responseTableHeader = '<tr>';
                $responseTableHeader .= '<td class="exclude-style-table"></td>';
                $responseTableHeader .= '<td>S</td>';

                $responseTable .= '<thead>';
                $responseTable .= '<tr>';
                $responseTable .= '<td class="exclude-style-table"></td>';
                $responseTable .= '<td><strong>Self</strong></td>';

                if ($code->innertext() != '[response-table]') {
                    $responseTableFooter = '<tr><td>Summary</td>';
                }

                foreach($survey->savedCategories($ids[2])->get() as $category) {
                    $responseTable .= '<td class="class-bg-'.$category->name[0].'" colspan="'.$category->observers()->count().'"><strong>'.$category->name.'(s)</strong></td>';

                    foreach($category->observers as $key => $observer) {
                        $responseTableHeader .= '<td class="class-bg-'.$category->name[0].'">'.$category->name[0].($key+1).'</td>';
                    }

                    if (count($category->observers) == 0) {
                        $responseTableHeader .= '<td class="class-bg-'.$category->name[0].'">-</td>';
                    }

                }
                $responseTable .= '</tr>';
                $responseTable .= $responseTableHeader . '</tr>';
                $responseTable .= '</thead>';

                $total = array('Self' => 0);

                //add variable for sum total each category
                //added by puw on 29-01-2018
                $total_each_category=array();
                $total_each_category_ittr=array();
                $category_name_ittr=array();
                $attributes_before=array();
                $tag_total_each_category='';
                $tag_total_each_category_last='';

                $attributes_ittr=0;
                //$responseTable_row='';
                foreach ($attributes as $key => $attribute) {
                    $responseTable_row='';
                    $tag_total_each_category_last='';
                    if ($code->innertext() == '[response-table]') {
                        $attributeData = Attribute::find($key);
                    } else {
                        $attributeData = Question::find($key);
                        $AttributeByQuestion=$this->getAttributeByQuestion($ids[1],$key)[0];
                    }

                   
                    //$attributeData = Question::find($key);

                    if ($attributeData) {
                        

                        $responseTable_row .= '<tr>';
                        if ($code->innertext() == '[response-table]') {
                            $responseTable_row .= '<td><strong>'.$attributeData->name.'</strong></td>';
                        } else {
                            $responseTable_row .= '<td>'.strip_tags($attributeData->name).'</td>';
                        }
                        
                        $total['Self'] += $attribute['Self'];
                
                        $responseTable_row .= '<td>'.$attribute['Self'].'</td>';

                        if ($code->innertext() != '[response-table]') {

                            $attributes_before[$attributes_ittr]['Self']=$AttributeByQuestion['attributes_id'];

                            $category_name_ittr[$attributes_ittr]=$AttributeByQuestion['attributes'];

                            if($attributes_ittr>0){
                                if ($attributes_before[$attributes_ittr-1]['Self']==$AttributeByQuestion['attributes_id']) {
                                        $value_before=$total_each_category_ittr[$attributes_ittr-1]['Self'];
                                        $total_each_category_ittr[$attributes_ittr]['Self']=$value_before+$attribute['Self'];

                                        $tag_total_each_category_last .='<tr><td><strong> Total '.$category_name_ittr[$attributes_ittr].'</strong></td>';
                                        $tag_total_each_category_last .='<td><strong>'.$total_each_category_ittr[$attributes_ittr]['Self'].'</strong></td>';

                                   }else{
                                        $total_each_category_ittr[$attributes_ittr]['Self']=$attribute['Self'];
                                        $tag_total_each_category .='<tr><td><strong> Total '.$category_name_ittr[$attributes_ittr-1].'</strong></td>';
                                        $tag_total_each_category .='<td><strong>'.$total_each_category_ittr[$attributes_ittr-1]['Self'].'</strong></td>';

                                        $tag_total_each_category_last='';
                                   }   
                            }else{
                                //Pertanyaan pertama
                                $total_each_category_ittr[$attributes_ittr]['Self']=$attribute['Self'];
                            }
                        }

                        foreach($survey->savedCategories($ids[2])->get() as $category) {
                            if (!isset($total[$category->name])) {
                                $total[$category->name] = array();
                            }
                            foreach ($category->observers as $observer) {
                                if (!isset($total[$category->name][$observer->id])) {
                                    $total[$category->name][$observer->id] = 0;
                                    
                                }
                                $responseTable_row .= '<td>';
                                //$total_each_category_ittr[$attributes_ittr]=array();

                                if (isset($attribute[$category->name])) {
                                    if (isset($attribute[$category->name][$observer->id])) {
                                        $responseTable_row .= $attribute[$category->name][$observer->id];
                                        
                                        if ($code->innertext() != '[response-table]') {
                                            // $total[$category->name][$observer->id] += $attribute[$category->name][$observer->id];
                                            $total[$category->name][$observer->id] += $attribute[$category->name][$observer->id];

                                            //added by puw on 29-0-2018
                                            $attributes_before[$attributes_ittr][$category->name.$observer->id]=$AttributeByQuestion['attributes_id'];
                                            if($attributes_ittr>0){
                                                if ($attributes_before[$attributes_ittr-1][$category->name.$observer->id]==$AttributeByQuestion['attributes_id']) {

                                                        $value_before=$total_each_category_ittr[$attributes_ittr-1][$category->name.$observer->id];
                                                        $total_each_category_ittr[$attributes_ittr][$category->name.$observer->id]=$value_before+$attribute[$category->name][$observer->id];
                                                        $tag_total_each_category_last .='<td><strong>'.$total_each_category_ittr[$attributes_ittr][$category->name.$observer->id].'</strong></td>';
                                                   }else{
                                                        $total_each_category_ittr[$attributes_ittr][$category->name.$observer->id]=$attribute[$category->name][$observer->id];

                                                        $tag_total_each_category .='<td><strong>'.$total_each_category_ittr[$attributes_ittr-1][$category->name.$observer->id].'</strong></td>';

                                                        $tag_total_each_category_last='';
                                                        
                                                        //$tag_total_each_category .='</tr>';
                                                   }   
                                            }else{
                                                //Pertanyaan pertama
                                                $total_each_category_ittr[$attributes_ittr][$category->name.$observer->id]=$attribute[$category->name][$observer->id];
                                            }
                                        }

                                    } else {
                                        $responseTable_row .= '-';
                                    }
                                } else {
                                    $responseTable_row .= '-';
                                }
                                $responseTable_row .= '</td>';
                            }

                            if (count($category->observers) == 0) {
                                $responseTable_row .= '<td>-</td>';
                            }
                        }
                        $tag_total_each_category .='</tr>';

                        $responseTable_row .= '</tr>';
                        $tag_total_each_category_last .='</tr>';

                        
                        //
                        //Add by puw on 26-01-2018
                        //to show up
                        if (($code->innertext() != '[response-table]') && $survey->profiler_type=='360-profiler') {
                            if($attributes_ittr>0){
                                if ($attributes_before[$attributes_ittr-1]['Self']==$AttributeByQuestion['attributes_id']) {
                                   }else{
                                        $responseTable .=$tag_total_each_category;
                                   }   
                            }
                        }
                        $responseTable .=$responseTable_row;

                    }

                    $attributes_ittr +=1;
                    $tag_total_each_category='';
                }

                //Added to last row for last category
                if (($code->innertext() != '[response-table]') && $survey->profiler_type=='360-profiler') {
                    if($attributes_ittr>0){
                         $responseTable .=$tag_total_each_category_last;
                    }
                }

                //Showup Sum total of all row in last Table
                if ($code->innertext() != '[response-table]') {
                    foreach ($total as $key => $values) {
                        if (is_array($values)) {
                            foreach ($values as $value) {
                                $responseTableFooter .= '<td><strong>'.$value.'</strong></td>';
                            }
                        } else {
                            $responseTableFooter .= '<td><strong>'.$values.'</strong></td>';
                        }
                    }
                    //$responseTable .= $responseTableFooter.'</tr>';
                }
                
                $responseTable .= '</table>';

                //edited by puw on 24-12-2018
                $html = str_replace($code, htmlspecialchars_decode($responseTable), $html);
            }
        }

        

        foreach ($htmlDOM->find('.editable') as $editable) {
            if ($editable->getAttribute('data-selector') != "title") {
                $html = str_replace($editable, htmlspecialchars_decode('<div class="content-text">' . $editable->innertext() . '</div>'), $html);
            }
        }

        $html = str_replace('[change-with-element]', $html, $base);

        foreach ($htmlDOM->find('code') as $key => $code) {
            if ($code->innertext() == '[new-page]') {
                $html = str_replace($code, $footer, $html);
                $html = str_replace('[page-number]', $code->getAttribute('data-page'), $html);
            }
        }
        // $html = str_replace('<code>[new-page]</code>', $footer, $html);

        $html = str_replace('[report-header]', ( @$reportData ? $reportData->header_text : '' ), $html);
        
        $html = str_replace('[report-header-align]', ( @$reportData ? $reportData->header_alignment : '' ), $html);
        $html = str_replace('[report-footer]', ( @$reportData ? $reportData->footer_text : '' ), $html);
        $html = str_replace('[report-footer-align]', ( @$reportData ? $reportData->footer_alignment : '' ), $html);

        $html = str_replace('[report-background]', ( @$reportData ? $reportData->cover_background : '' ), $html);

        // added by rdw 151217
        $html = str_replace('[report-description]', ( @$reportData ? $reportData->report_desc : '' ), $html);
        $html = str_replace('https://storage.googleapis.com/learn1thing/img/brand-logo.png', ( @$reportData ? ($reportData->logo_url=='' ? 'https://storage.googleapis.com/learn1thing/img/brand-logo.png' : $reportData->logo_url ): '' ), $html);

        //editeb by puw on 24-01-2018
        //$html = str_replace('<button type="button" class="close removeElement">&times;</button>', '', $html);

        $html = str_replace('<button class="close removeElement" type="button">×</button>', '', $html);
        
        
        $html = str_replace('[product-purchaser]', ($buyer->full_name()), $html);
        $html = str_replace('[survey-title]', $survey->title, $html);
        $html = str_replace('[survey-type]', $survey->profiler_type, $html);
        $html = str_replace('[survey-batch_reference]', $survey->batch_reference, $html);
        $html = str_replace('[survey-description]', nl2br($survey->description), $html);
        $html = str_replace('[survey-period_start-simple]', date('d-m-Y'), $html);
        $html = str_replace('[survey-period_start-style]', date('d ') . '<span class="month">' . date('F') . '</span>' . date('Y'), $html);
        $html = str_replace('[survey-period_start-flat]', date('F d,Y'), $html);
        // return $html;

        // old
        // $html = '<body style="width: 793.700787px;">' . str_replace('contenteditable="true"', 'contenteditable="false"', htmlspecialchars_decode($survey->reportTemplate->content)) . '</body>';

        /*
        // observers
        $observers = '';
        $observers2 = '';
        if (strpos($html, '[observers]') != false) {
            $index = 0;
            $colors = ['#76B4E3', '#BEBEBC', '#CE4A96', '#DACC37'];
            $baseCategory = $survey->savedCategories($ids[2]);
            $baseCount = $baseCategory->count();

            if ($baseCount > 4) {
                $categories = $baseCategory->get()->chunk(floor($baseCategory->count()/2));
                foreach ($categories[0] as $key => $category) {
                    $observers .= '<p style="color: '.$colors[$index].';width: 100%;">'.$category->observers()->count().' '.$category->name.'</p>';

                    $index = $index==3 ? 0 : $index+1;
                }
                $observers2 = '<div>';
                foreach ($categories[1] as $key => $category) {
                    $observers2 .= '<p style="color: '.$colors[$index].'">'.$category->observers()->count().' '.$category->name.'</p>';

                    $index = $index==3 ? 0 : $index+1;
                }
                $observers2 .= '</div>';
            } else {
                foreach ($baseCategory->get() as $key => $category) {
                    $observers .= '<p style="color: '.$colors[$index].';width: 100%;">'.$category->observers()->count().' '.$category->name.'</p>';

                    $index = $index==3 ? 0 : $index+1;
                }
            }
        }

        // replace data
        $html = str_replace('[johari-question]', $johariDetail, $html);
        $html = str_replace('[response-table]', $responseTable, $html);
        $html = str_replace('[observers]', $observers, $html);
        $html = str_replace('<div style="display:none">[observers-2]</div>', $observers2, $html);
        $html = str_replace('[product-purchaser]', ($buyer->full_name()), $html);
        $html = str_replace('[survey-title]', $survey->title, $html);
        $html = str_replace('[survey-batch_reference]', $survey->batch_reference, $html);
        $html = str_replace('[survey-description]', nl2br($survey->description), $html);
        $html = str_replace('[survey-period_start-simple]', date('d-m-Y', strtotime($survey->period_start)), $html);
        $html = str_replace('[survey-period_start-style]', date('d ', strtotime($survey->period_start)) . '<span class="month">' . date('F', strtotime($survey->period_start)) . '</span>' . date('Y', strtotime($survey->period_start)), $html);
        // $html = str_replace('[survey-period_end]', date('d F Y', strtotime($survey->period_end)), $html);

        // foreach ($survey->sections as $section) {
        //     $html = str_replace('[section-'.$section->title.'-title]', $section->title, $html);
        //     $html = str_replace('[section-'.$section->title.'-description]', $section->description, $html);

        //     if ($section->question_type == 'question') {
        //         foreach ($section->questions as $question) {
        //             $html = str_replace('[question-'.$section->title.'-'.$question->question_number.']', view('previews.question', compact('question', 'section'))->render(), $html);
        //         }
        //     }
        // }
        */
        

        // buat PDF
        $baseUrl = storage_path()."/report/".($userSurvey ? $userSurvey->id : '0');
        exec("mkdir ".$baseUrl);
        file_put_contents($baseUrl.'/report.html', $html);

        // new
        exec("prince " . $baseUrl . "/report.html -o " . $baseUrl . "/report.pdf --pdf-title='" . $survey->title . "' --page-margin=0 -s " . public_path('/css/report-template.css'));

        $filename = "/report/".($userSurvey ? $userSurvey->id : '0').'/report.pdf';
        
        $path = storage_path($filename);
        Log::info('Report name: '.$path);
        //print_r($path);exit;

        if ($userSurvey && false) {
            \Mail::send('previews.attachment', array('buyer' => $buyer, 'survey' => $survey), function($message) use ($buyer, $survey, $path) {
                $message->to($buyer->email);
                $message->subject('Your '.$survey->title.' Report');
                $message->from(ENV('MAIL_FROM', 'info@beta.learn1thing.com'));
                $message->attach($path, array(
                    'as' => 'report.pdf',
                    'mime' => 'application/pdf')
                );
            });
        }

        //return $html;
        return Response::make(file_get_contents($path), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$filename.'"'
        ]);
    }

    private function mappingReportFormula($reportFormula)
    {
        $isStop = false;
        $finalResult = null;
        $resultByFormula = array();
        $operandByFormula = array();
        while (!$isStop) {
            if($reportFormula->source == "raw_data") {
                $answers = $this->getAnswerByFormula($reportFormula);
                $resultByFormula[] = $this->calculateAnswersByFormula($reportFormula, $answers);
            } else if($reportFormula->source == "formula") {
                $isStopFormula = false;
                $resultSourceFormula = array();
                $operandBySourceFormula = array();
                $reportSourceFormula = ReportFormula::find($reportFormula->formula_id);
                while (!$isStopFormula) {
                    $answersBySourceFormula = $this->getAnswerByFormula($reportSourceFormula);
                    $operandBySourceFormula[] = $reportSourceFormula->operand;
                    $resultSourceFormula[] = $this->calculateAnswersByFormula($reportSourceFormula, $answersBySourceFormula);
                    if(isset($reportSourceFormula->reference_formula_id)) {
                        $reportSourceFormula = ReportFormula::find($reportSourceFormula->reference_formula_id);
                        if(!$reportSourceFormula) {
                            $isStopFormula = true;
                        }
                    } else {
                        $isStopFormula = true;
                    }

                }
                $result2 = "";
                foreach ($resultSourceFormula as $k => $v) {
                    $result2 .= $v;
                    if(isset($operandBySourceFormula[$k])) {
                        $result2 .= $operandBySourceFormula[$k];
                    }
                }
                $isInt2 = intval($result2);
                if(strlen($result2) > 0 && strpos($result2, ',') == false && $isInt2 > 0) {
                    $result2 = eval('return '.$result2.';');
                }
                $resultByFormula[] = $result2;
            }
            $operandByFormula[] = $reportFormula->operand;


            if(isset($reportFormula->reference_formula_id)) {
                $reportFormula = ReportFormula::find($reportFormula->reference_formula_id);
                if(!$reportFormula) {
                    $isStop = true;
                }
            } else {
                $isStop = true;
            }
        }

        $result = "";
        foreach ($resultByFormula as $key => $value) {
            $result .= $value;
            if(isset($operandByFormula[$key])) {
                $result .= $operandByFormula[$key];
            }
        }
        // for($i=count($resultByFormula)-1; $i >= 0; $i--) {
        //     $result .= $resultByFormula[$i];
        //     if(isset($operandByFormula[$i])) {
        //         $result .= $operandByFormula[$i];
        //     }
        // }
        $isInt = intval($result);
        if(strlen($result) > 0 && strpos($result, ',') == false && $isInt > 0) {
            $result = eval('return '.$result.';');
        }
        return $result;
    }

    private function getAnswerByFormula($reportFormula)
    {
        if($reportFormula->profiler_role == "user") {
            $query = 'SELECT answer_details.id as answer_id, answer_details.answer as answer, questions.answer_type as answer_type, questions.answer_configs as answer_configs, questions.answer_attributes as answer_attributes, questions.answer_scores, user_surveys.user_id as customer_id FROM answer_details INNER JOIN answer_headers ON answer_headers.id = answer_details.answer_header_id INNER JOIN observers ON observers.id = answer_headers.observer_id INNER JOIN questions ON answer_details.question_id = questions.id INNER JOIN batches ON batches.observer_id = observers.id INNER JOIN user_surveys ON user_surveys.id = batches.user_survey_id where batches.observer_id IN(select us.observer_id from user_surveys us) WHERE batches.survey_id = ' . $reportFormula->survey_id . ' AND questions.id = ' . $reportFormula->question_id;
        } else if($reportFormula->profiler_role == "observer") {
            $query = 'SELECT answer_details.id as answer_id, answer_details.answer as answer, questions.answer_type as answer_type, questions.answer_configs as answer_configs, questions.answer_attributes as answer_attributes, questions.answer_scores, user_surveys.user_id as customer_id FROM answer_details INNER JOIN answer_headers ON answer_headers.id = answer_details.answer_header_id INNER JOIN observers ON observers.id = answer_headers.observer_id INNER JOIN questions ON answer_details.question_id = questions.id INNER JOIN batches ON batches.observer_id = observers.id INNER JOIN user_surveys ON user_surveys.id = batches.user_survey_id where batches.observer_id NOT IN(select us.observer_id from user_surveys us) AND batches.survey_id = ' . $reportFormula->survey_id . ' AND questions.id = ' . $reportFormula->question_id;

            if($reportFormula->observer_category_id) {
                $query .= ` AND observers.category_profile_id = ` . $reportFormula->observer_category_id;
            }
        } else {
            $query = 'SELECT answer_details.id as answer_id, answer_details.answer as answer, questions.answer_type as answer_type, questions.answer_configs as answer_configs, questions.answer_attributes as answer_attributes, questions.answer_scores, user_surveys.user_id as customer_id FROM answer_details INNER JOIN answer_headers ON answer_headers.id = answer_details.answer_header_id INNER JOIN observers ON observers.id = answer_headers.observer_id INNER JOIN questions ON answer_details.question_id = questions.id INNER JOIN batches ON batches.observer_id = observers.id INNER JOIN user_surveys ON user_surveys.id = batches.user_survey_id WHERE batches.survey_id = ' . $reportFormula->survey_id . ' AND questions.id = ' . $reportFormula->question_id;
        }
        $answers = DB::select($query);
        return $answers;
    }

    private function calculateAnswersByFormula($reportFormula, $answers)
    {
        $result = null;
        // if($reportFormula->user_category == "department") {
        //     $departmentId = $reportFormula->group_by;
        //     $customerIds = array_pluck($answers, 'customer_id');
        //     $params = array('department_id' => $departmentId, 'customer_ids' => implode(',', $customerIds));
        //     $responseApi = Client::setEndpoint('department/customer-validation')
        //         ->setQuery($params)->get();
        //     $customerIds = [];
        //     if ($responseApi['status']['success'] == true) {
        //         $stringCustomerIds = $responseApi['data']['customer_ids'];
        //         if(strlen($stringCustomerIds) > 0) {
        //             $customerIds = explode(',', $customerIds);
        //         }
        //     }

        //     $answers = array_where($answers, function ($value, $key) use($customerIds)
        //     {
        //         return in_array($value->customer_id, $customerIds);
        //     });
        // } else if($reportFormula->user_category == "individual") {
        //     $roleId = $reportFormula->group_by;
        //     $customerIds = array_pluck($answers, 'customer_id');
        //     $params = array('role_id' => $roleId, 'customer_ids' => implode(',', $customerIds));
        //     $responseApi = Client::setEndpoint('role/user-validation')
        //         ->setQuery($params)->get();
        //     $customerIds = [];
        //     if ($responseApi['status']['success'] == true) {
        //         $stringCustomerIds = $responseApi['data']['customer_ids'];
        //         if(strlen($stringCustomerIds) > 0) {
        //             $customerIds = explode(',', $customerIds);
        //         }
        //     }
        //     $answers = array_where($answers, function ($value, $key) use($customerIds)
        //     {
        //         $userId = array_get($value, 'answer_header.observer._user_survey.user_id');
        //         return in_array($value->customer_id, $customerIds);
        //     });
        // }

        if($reportFormula->question_spec == "value") {
            $answerResult = [];
            foreach ($answers as $key => $value) {
                if($value->answer_type == "free_text" ||
                    $value->answer_type == "date" ||
                    $value->answer_type == "time" ||
                    $value->answer_type == "ranking") {
                    $answerResult[] = (int) $value->answer_scores;
                } else if($value->answer_type == "multichoice" || $value->answer_type == "boolean") {
                    $answerScores = json_decode($value->answer_scores);
                    $answerConfigs = json_decode($value->answer_configs);
                    $answerIndex = array_search($value->answer, $answerConfigs);
                    $answerResult[] = isset($answerScores[$answerIndex]) ? (int) $answerScores[$answerIndex]:0;
                } else if($value->answer_type == "checkboxes") {
                    $answerValue = json_decode($value->answer);
                    $answerScores = json_decode($value->answer_scores);
                    $answerConfigs = json_decode($value->answer_configs);
                    $totalScore = 0;
                    foreach($answerValue as $val) {
                        $answerIndex = array_search($val, $answerConfigs);
                        $totalScore += isset($answerScores[$answerIndex]) ? (int) $answerScores[$answerIndex]:0;
                    }
                    $answerResult[] = $totalScore;
                } else if($value->answer_type == "matching") {
                    $answerValue = json_decode($value->answer);
                    $answerScores = json_decode($value->answer_scores);
                    $answerConfigs = json_decode($value->answer_configs);
                    $totalScore = 0;
                    foreach ($answerValue as $k => $val) {
                        $rightAnswer = array_first($answerConfigs, function ($v) use($k, $val)
                        {
                            return $v->label == $k && $v->value == $val;
                        });
                        if($rightAnswer) {
                            $index = array_search($rightAnswer->value, array_pluck($answerConfigs, 'value'));
                            $totalScore += $answerScores[$index];
                        }
                    }
                    $answerResult[] = $totalScore;
                } else if($value->answer_type == "likert_scale") {
                    $answerScores = json_decode($value->answer_scores);
                    $answerConfigs = json_decode($value->answer_configs);
                    $rightAnswer = array_first($answerConfigs, function ($val) use($value)
                    {
                        return $val->value == $value->answer;
                    });
                    $totalScore = 0;
                    if($rightAnswer) {
                        $index = array_search($rightAnswer->value, array_pluck($answerConfigs, 'value'));
                        $totalScore = isset($answerScores[$index]) ? (int) $answerScores[$index]:0;
                    }
                    $answerResult[] = $totalScore;
                } elseif ($value->answer_type == "multiple_choice_with_constraint" || $value->answer_type == "distributed_points") {
                    $answerValue = json_decode($value->answer);
                    $answerScores = json_decode($value->answer_scores);
                    $answerConfigs = json_decode($value->answer_configs);
                    $options = $answerConfigs->options;
                    $totalScore = 0;
                    foreach($answerValue as $val) {
                        $answerIndex = array_search($val, $options);
                        $totalScore += isset($answerScores[$answerIndex]) ? (int) $answerScores[$answerIndex]:0;
                    }
                    $answerResult[] = $totalScore;
                }
            }

            if($reportFormula->function == "raw_value") {
                $result = implode(', ', $answerResult);
            } else if($reportFormula->function == "sum") {
                $result = array_sum($answerResult);
            } else if($reportFormula->function == "average") {
                $total = array_sum($answerResult);
                $count = count($answerResult);
                $result = $total/$count;
            } else if($reportFormula->function == "count") {
                $result = count($answerResult);
            } else if($reportFormula->function == "maximum") {
                $result = max($answerResult);
            } else if($reportFormula->function == "minimum") {
                $result = min($answerResult);
            }
        } else if($reportFormula->question_spec == "answer_attributes") {
            $answerResult = [];
            $attributeIds = [];
            foreach ($answers as $key => $value) {
                if($value->answer_type == "free_text" ||
                    $value->answer_type == "date" ||
                    $value->answer_type == "time" ||
                    $value->answer_type == "ranking") {
                    $attributeIds = array_merge($attributeIds, json_decode($value->answer_attributes));
                } else if($value->answer_type == "multichoice" || $value->answer_type == "boolean") {
                    $answerAttributes = json_decode($value->answer_attributes);
                    $answerConfigs = json_decode($value->answer_configs);
                    $answerIndex = array_search($value->answer, $answerConfigs);
                    $attributeIdsTemp = isset($answerAttributes[$answerIndex]) ? $answerAttributes[$answerIndex]:[];
                    $attributeIds = array_merge($attributeIds, $attributeIdsTemp);
                } else if($value->answer_type == "checkboxes") {
                    $answerValue = json_decode($value->answer);
                    $answerAttributes = json_decode($value->answer_attributes);
                    $answerConfigs = json_decode($value->answer_configs);
                    $attributeIdsTemp = [];
                    foreach($answerValue as $val) {
                        $answerIndex = array_search($val, $answerConfigs);
                        $attrs = isset($answerAttributes[$answerIndex]) ? $answerAttributes[$answerIndex]:[];
                        $attributeIdsTemp = array_merge($attributeIdsTemp, $attrs);
                    }
                    $attributeIds = array_merge($attributeIds, $attributeIdsTemp);
                } else if($value->answer_type == "matching") {
                    $answerValue = json_decode($value->answer);
                    $answerAttributes = json_decode($value->answer_attributes);
                    $answerConfigs = json_decode($value->answer_configs);
                    $attributeIdsTemp = [];
                    foreach ($answerValue as $k => $val) {
                        $rightAnswer = array_first($answerConfigs, function ($v) use($k, $val)
                        {
                            return $v->label == $k && $v->value == $val;
                        });
                        if($rightAnswer) {
                            $index = array_search($rightAnswer->value, array_pluck($answerConfigs, 'value'));
                            $attrs = isset($answerAttributes[$index]) ? $answerAttributes[$index]:[];
                            $attributeIdsTemp = array_merge($attributeIdsTemp, $attrs);
                        }
                    }
                    $attributeIds = array_merge($attributeIds, $attributeIdsTemp);
                } else if($value->answer_type == "likert_scale") {
                    $answerAttributes = json_decode($value->answer_attributes);
                    $answerConfigs = json_decode($value->answer_configs);
                    $attributeIdsTemp = [];
                    $rightAnswer = array_first($answerConfigs, function ($val) use($value)
                    {
                        return $val->value == $value->answer;
                    });
                    $totalScore = 0;
                    if($rightAnswer) {
                        $index = array_search($rightAnswer->value, array_pluck($answerConfigs, 'value'));
                        $attrs = isset($answerAttributes[$index]) ? $answerAttributes[$index]:[];
                        $attributeIdsTemp = array_merge($attributeIdsTemp, $attrs);
                    }
                    $attributeIds = array_merge($attributeIds, $attributeIdsTemp);
                } else if($value->answer_type == "multiple_choice_with_constraint" || $value->answer_type == "distributed_points") {
                    $answerValue = json_decode($value->answer);
                    $answerAttributes = json_decode($value->answer_attributes);
                    $answerConfigs = json_decode($value->answer_configs);
                    $options = $answerConfigs->options;
                    $attributeIdsTemp = [];
                    foreach($answerValue as $val) {
                        $answerIndex = array_search($val, $options);
                        $attrs = isset($answerAttributes[$answerIndex]) ? $answerAttributes[$answerIndex]:[];
                        $attributeIdsTemp = array_merge($attributeIdsTemp, $attrs);
                    }
                    $attributeIds = array_merge($attributeIds, $attributeIdsTemp);
                }
            }
            $attributeIds = array_unique($attributeIds);
            $attributes = Attribute::whereIn('id', $attributeIds)
                ->get()
                ->toArray();
            $result = implode(', ', array_pluck($attributes, 'name'));
        } else if($reportFormula->question_spec == "free_text") {
            $answerResult = array_pluck($answers, 'answer');
            $result = implode(', ', $answerResult);
        }
        return $result;
    }

    // change in future
    public function generatePDF() {
        $filename = 'report/learn1thing.pdf';
        $path = storage_path($filename);

        return Response::make(file_get_contents($path), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$filename.'"'
        ]);
    }

    // remove in future
    public function fakePreview() {
        return view('previews.fake')
            ->withId(
                strtr(base64_encode(openssl_encrypt(1, 'AES-128-ECB', 'l1tprofiler')), '+/=', '-_,')
            );
    }

    /**
     * @description save observer to store
     * @author Rangga <ranggapl@windowslive.com>
     * @return [json] [return object]
     */
    public function storeMessageObserver(Request $request)
    {
        $status = true;
        $code = 200;
        $message = "Message for observer has been stored";
        $data = array();
        try {
            $validator = Validator::make($request->all(), [
                'user_survey_id' => 'required|numeric|exists:user_surveys,id'
            ]);
            if ($validator->fails()) {
                $status = false;
                $code = 400;
                $message = "";
                foreach ($validator->errors()->messages() as $key => $value) {
                    $message .= implode(' ', $value);
                }
            } else {
                $params = $request->all();
                $userSurvey = UserSurvey::find($params['user_survey_id']);
                $userSurvey->message_for_observer = $params['message_for_observer'];
                $userSurvey->save();
                $data = $userSurvey;
            }
        } catch(Exception $e) {
            $status = false;
            $code = 400;
            $message = $e->getMessage();
        }

        $response = array(
            'code' => $code,
            'status' => $status,
            'message' => $message,
            'data' => $data
        );
        return response()->json($response);
    }

    /**
     * @description save observer to store
     * @author Rangga <ranggapl@windowslive.com>
     * @return [json] [return object]
     */
    public function storeObserver(Request $request)
    {
        $status = true;
        $code = 200;
        $message = "Observer has been stored";
        $data = array();
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'last_name' => 'required',
                'first_name' => 'required',
                'category_id' => 'required|numeric|exists:category_profile_details,id'
            ]);
            if ($validator->fails()) {
                $status = false;
                $code = 400;
                $message = "";
                foreach ($validator->errors()->messages() as $key => $value) {
                    $message .= implode(' ', $value);
                }
            } else {
                $params = $request->except('survey_id');
                $userSurvey = UserSurvey::find($params['user_survey_id']);

                // store category profile detail to categories table for solving report problem
                $categoryProfileDetailId = $request->input('category_id');
                $categoryProfileDetail = CategoryProfileDetail::find($categoryProfileDetailId);
                $categoryParams = array(
                    'survey_id' => $userSurvey->survey_id,
                    'user_id' => $userSurvey->user_id,
                    'name' => $categoryProfileDetail->name
                );
                $category = Category::firstOrCreate($categoryParams);

                $params['category_profile_detail_id'] = $params['category_id'];
                $params['category_id'] = $category->id;
                $params['phone_number'] = isset($params['phone_number']) ? $params['phone_number'] : '';
                $observer = Observer::create($params);
                $batchCode = strtr(base64_encode(openssl_encrypt(date('Y-m-d H:i:s'), 'AES-128-ECB', 'l1tprofiler')), '+/=', '-_,');
                $batch = Batch::create([
                    'batch' => $batchCode,
                    'is_done'   => false,
                    'survey_id' => $userSurvey->survey_id,
                    'observer_id' => $observer->id,
                    'user_survey_id' => $params['user_survey_id']
                ]);
                $batch->observer->notify(new MailNotification);
                $data = $request->all();
                $data['observer_id'] = $observer->id;
                $data['batch_code'] = $batch->batch;
                $data['url_link'] = RouteHelper::check('surveys.preview', ['type' => 'batch', 'code' => $batch->batch]);
                $data['remind_url'] = RouteHelper::check('observer.invite', ['code' => $batch->batch]);
            }
        } catch(Exception $e) {
            $status = false;
            $code = 400;
            $message = $e->getMessage();
        }

        $response = array(
            'code' => $code,
            'status' => $status,
            'message' => $message,
            'data' => $data
        );
        return response()->json($response);
    }

    /**
     * @description send invitaion email to observer to fill out the profiler
     * @author Rangga <ranggapl@windowslive.com>
     * @return [json] [return object]
     */
    public function inviteObserver($batchCode)
    {
        $status = true;
        $code = 200;
        $message = "Email has been sent";
        try {
            $batch = Batch::where('batch', $batchCode)
                ->with('observer')
                ->first();
            if($batch) {
                $batch->observer->notify(new MailNotification);
            } else {
                $status = false;
                $code = 400;
                $message = "Invalid parameter batch";
            }
        } catch(Exception $e) {
            $status = false;
            $code = 400;
            $message = $e->getMessage();
        }

        $response = array(
            'code' => $code,
            'status' => $status,
            'message' => $message
        );
        return response()->json($response);
    }

    /**
     * @description send invitaion email to observer to fill out the profiler
     * @author Rangga <ranggapl@windowslive.com>
     * @return [json] [return object]
     */
    public function revokeObserver($id)
    {
        $status = true;
        $code = 200;
        $message = "Observer has been revoked";
        try {
            $batch = Batch::where('observer_id', $id)
                ->with('observer')
                ->first();
            if($batch) {
                $batch->observer->delete();
                $batch->delete();
            } else {
                $status = false;
                $code = 400;
                $message = "Invalid parameter batch";
            }
        } catch(Exception $e) {
            $status = false;
            $code = 400;
            $message = $e->getMessage();
        }

        $response = array(
            'code' => $code,
            'status' => $status,
            'message' => $message
        );
        return response()->json($response);
    }

    public function getScore($dataPersonal, $target, $id, $attributes)
    {
        foreach ($dataPersonal as $data) {
            if($data->answer_type == "free_text" || $data->answer_type == "ranking" || $data->answer_type == "date" || $data->answer_type == "time") {
                foreach (json_decode($data->answer_attributes) as $attribute) {
                    if ($target == 'Self') {
                        if (isset($attributes[$attribute])) {
                            $attributes[$attribute][$target] += (integer)$data->answer_scores;
                        } else {
                            $attributes[$attribute] = array($target => (integer)$data->answer_scores);
                        }
                    } else {
                        if (isset($attributes[$attribute])) {
                            if (isset($attributes[$attribute][$target])) {
                                if (isset($attributes[$attribute][$target][$id])) {
                                    $attributes[$attribute][$target][$id] += (integer)$data->answer_scores;
                                } else {
                                    $attributes[$attribute][$target][$id] = (integer)$data->answer_scores;
                                }
                            } else {
                                $attributes[$attribute][$target] = array($id => (integer)$data->answer_scores);
                            }
                        } else {
                            $attributes[$attribute] = array($target => array($id => (integer)$data->answer_scores));
                        }
                    }
                }

            } else if($data->answer_type == "multichoice" || $data->answer_type == "boolean" || $data->answer_type == "likert_scale" || $data->answer_type == "checkboxes" || $data->answer_type == "multiple_choice_with_constraint") {
                $answerIndex = array_search($data->answer, json_decode($data->answer_configs));
                $score = (integer)json_decode($data->answer_scores)[$answerIndex];

                foreach (json_decode($data->answer_attributes)[$answerIndex] as $attribute) {
                    if ($target == 'Self') {
                        if (isset($attributes[$attribute])) {
                            $attributes[$attribute][$target] += $score;
                        } else {
                            $attributes[$attribute] = array('Self' => 0, $target => $score);
                        }
                    } else {
                        if (isset($attributes[$attribute])) {
                            if (isset($attributes[$attribute][$target])) {
                                if (isset($attributes[$attribute][$target][$id])) {
                                    $attributes[$attribute][$target][$id] += $score;
                                } else {
                                    $attributes[$attribute][$target][$id] = $score;
                                }
                            } else {
                                $attributes[$attribute][$target] = array($id => $score);
                            }
                        } else {
                            $attributes[$attribute] = array('Self' => 0, $target => array($id => $score));
                        }
                    }
                }

            } else if($data->answer_type == "matching") {
            } else if($data->answer_type == "datetime") {
            } else if($data->answer_type == "distributed_points") {
            }
        }

        return $attributes;
    }

    public function getScoreTQuestion($dataPersonal, $target, $id, $attributes)
    {
        foreach ($dataPersonal as $data) {
            $attribute = $data->question_id; 
            if($data->answer_type == "free_text" || $data->answer_type == "ranking" || $data->answer_type == "date" || $data->answer_type == "time") {
                    if ($target == 'Self') {
                        if (isset($attributes[$attribute])) {
                            $attributes[$attribute][$target] += (integer)$data->answer_scores;
                        } else {
                            $attributes[$attribute] = array($target => (integer)$data->answer_scores);
                        }
                    } else {
                        if (isset($attributes[$attribute])) {
                            if (isset($attributes[$attribute][$target])) {
                                if (isset($attributes[$attribute][$target][$id])) {
                                    $attributes[$attribute][$target][$id] += (integer)$data->answer_scores;
                                } else {
                                    $attributes[$attribute][$target][$id] = (integer)$data->answer_scores;
                                }
                            } else {
                                $attributes[$attribute][$target] = array($id => (integer)$data->answer_scores);
                            }
                        } else {
                            $attributes[$attribute] = array($target => array($id => (integer)$data->answer_scores));
                        }
                    }

            } else if($data->answer_type == "multichoice" || $data->answer_type == "boolean" || $data->answer_type == "likert_scale" || $data->answer_type == "checkboxes" || $data->answer_type == "multiple_choice_with_constraint") {
                $answerIndex = array_search($data->answer, json_decode($data->answer_configs));
                $score = (integer)json_decode($data->answer_scores)[$answerIndex];

                    if ($target == 'Self') {
                        if (isset($attributes[$attribute])) {
                            $attributes[$attribute][$target] += $score;
                        } else {
                            $attributes[$attribute] = array('Self' => 0, $target => $score);
                        }
                    } else {
                        if (isset($attributes[$attribute])) {
                            if (isset($attributes[$attribute][$target])) {
                                if (isset($attributes[$attribute][$target][$id])) {
                                    $attributes[$attribute][$target][$id] += $score;
                                } else {
                                    $attributes[$attribute][$target][$id] = $score;
                                }
                            } else {
                                $attributes[$attribute][$target] = array($id => $score);
                            }
                        } else {
                            $attributes[$attribute] = array('Self' => 0, $target => array($id => $score));
                        }
                    }

            } else if($data->answer_type == "matching") {
            } else if($data->answer_type == "datetime") {
            } else if($data->answer_type == "distributed_points") {
            }
        }

        return $attributes;
    }

    public function getPersonalScore($dataPersonal, $target, $id, $questions)
    {
        foreach ($dataPersonal as $data) {
            if($data->answer_type == "free_text" || $data->answer_type == "ranking" || $data->answer_type == "date" || $data->answer_type == "time") {
                if (!isset($questions[$id])) {
                    $questions[$id] = array($target => (integer)$data->answer_scores);
                }

                if ($target != 'Self') {
                    if (isset($questions[$id][$target])) {
                        $questions[$id][$target] += (integer)$data->answer_scores;
                    } else {
                        $questions[$id][$target] = (integer)$data->answer_scores;
                    }
                }

            } else if($data->answer_type == "multichoice" || $data->answer_type == "boolean" || $data->answer_type == "likert_scale" || $data->answer_type == "checkboxes" || $data->answer_type == "multiple_choice_with_constraint") {
                $answerIndex = array_search($data->answer, json_decode($data->answer_configs));
                $score = (integer)json_decode($data->answer_scores)[$answerIndex];

                if (!isset($questions[$id])) {
                    $questions[$id] = array($target => $score);
                }

                if ($target != 'Self') {
                    if (isset($questions[$id][$target])) {
                        $questions[$id][$target] += $score;
                    } else {
                        $questions[$id][$target] = $score;
                    }
                }

            } else if($data->answer_type == "matching") {
            } else if($data->answer_type == "datetime") {
            } else if($data->answer_type == "distributed_points") {
            }
        }

        return $questions;
    }

    public function dataPersonal($survey_id, $observer_id, $question_id)
    {
        $baseQuery = AnswerHeader::select(['answer_headers.survey_id', 'answer_headers.observer_id', 'answer_details.question_id', 'answer_details.answer', 'questions.answer_configs', 'questions.answer_scores', 'questions.answer_attributes', 'questions.answer_type'])
                ->leftJoin('answer_details', 'answer_headers.id', '=', 'answer_details.answer_header_id')
                ->leftJoin('questions', 'answer_details.question_id', '=', 'questions.id')
                ->where('answer_headers.survey_id', $survey_id)
                ->where('answer_headers.observer_id', $observer_id)
                ->orderBy('questions.answer_attributes','ASC');

        if ($question_id != 0) {
            $baseQuery = $baseQuery->where('answer_details.question_id', $question_id);
        }

        return $baseQuery->get();
    }

    // edit disini
    public function dataPersonalAttribute($survey_id, $observer_id, $attribute_id)
    {
        // $survey_id = 14;
        // $observer_id = 667;
        // $attribute_id = 491;
        // 873, 1511
        // dd($observer_id);
        $baseQuery = AnswerHeader::select([DB::Raw($attribute_id . ' as attribute_id'), 'answer_headers.survey_id', 'answer_headers.observer_id', 'answer_details.question_id', 'answer_details.answer', 'questions.answer_configs', 'questions.answer_scores', 'questions.answer_attributes', 'questions.answer_type', 'questions.content'])
                ->leftJoin('answer_details', 'answer_headers.id', '=', 'answer_details.answer_header_id')
                ->leftJoin('questions', 'answer_details.question_id', '=', 'questions.id')
                ->where('answer_headers.survey_id', $survey_id)
                ->where('answer_headers.observer_id', $observer_id)
                ->orderBy('questions.answer_attributes','asc');

        if ($attribute_id != 0) {
            $baseQuery = $baseQuery->where(function($query) use ($attribute_id) {
                $query->where('questions.answer_attributes', 'like', '%['.$attribute_id.']%')
                    ->orWhere('questions.answer_attributes', 'like', '%['.$attribute_id.',%')
                    ->orWhere('questions.answer_attributes', 'like', '%,'.$attribute_id.',%')
                    ->orWhere('questions.answer_attributes', 'like', '%,'.$attribute_id.']%');
            });
        }

        return $baseQuery->get();
    }

    public function getPersonalScoreAttribute($dataPersonal, $target, $id, $attributes)
    {
        $count = 0;
        foreach ($dataPersonal as $data) {
            if($data->answer_type == "free_text" || $data->answer_type == "ranking" || $data->answer_type == "date" || $data->answer_type == "time") {

                if ( !isset($attributes[$id]) ) {
                    $attributes[$id] = array($target => array($data->question_id => (integer)$data->answer_scores));
                } else {
                    $attributes[$id][$target][$data->question_id] = (integer)$data->answer_scores;
                }

                /*
                if (!isset($attributes[$id])) {
                    $attributes[$id] = array($target => (integer)$data->answer_scores);
                }

                // if ($target != 'Self') {
                    if (isset($attributes[$id][$target])) {
                        $attributes[$id][$target] += (integer)$data->answer_scores;
                    } else {
                        $attributes[$id][$target] = (integer)$data->answer_scores;
                    }
                // }
                */

            } else if($data->answer_type == "multichoice" || $data->answer_type == "boolean" || $data->answer_type == "likert_scale" || $data->answer_type == "checkboxes" || $data->answer_type == "multiple_choice_with_constraint") {
                $answerIndex = array_search($data->answer, json_decode($data->answer_configs));
                $score = (integer)json_decode($data->answer_scores)[$answerIndex];

                if ( !isset($attributes[$id]) ) {
                    $attributes[$id] = array($target => array($data->question_id => $score));
                } else {
                    $attributes[$id][$target][$data->question_id] = $score;
                }

                /*
                if (!isset($attributes[$id])) {
                    $attributes[$id] = array($target => $score);
                }

                // if ($target != 'Self') {
                    if (isset($attributes[$id][$target])) {
                        $attributes[$id][$target] += $score;
                    } else {
                        $attributes[$id][$target] = $score;
                    }
                // }
                */

            } else if($data->answer_type == "matching") {
            } else if($data->answer_type == "datetime") {
            } else if($data->answer_type == "distributed_points") {
            }

            $count +=1;
        }

        // if (isset($attributes[$id][$target])) {
        //     $attributes[$id][$target] = ceil($attributes[$id][$target] / ($count == 0 ? 1 : $count));
        // } else {
        //     $attributes[$id][$target] = 0;
        // }

        return $attributes;
    }
    // sampai disini

    public function handleMoodle($code, $request)
    {
        // try {
            $code = openssl_decrypt(base64_decode(strtr($code, '-_,', '+/=')), 'AES-128-ECB', 'l1tprofiler');
            $email = openssl_decrypt(base64_decode(strtr($request->get('address'), '-_,', '+/=')), 'AES-128-ECB', 'l1tprofiler');
            $product_id = empty($request->get('product_id')) ? 0 : openssl_decrypt(base64_decode(strtr($request->get('product_id'), '-_,', '+/=')), 'AES-128-ECB', 'l1tprofiler');
            $product_id = $product_id ? $product_id : 0;
            $responseApi = Client::setEndpoint('getDataUser/'.$email)->get();

            // dd($responseApi);

            if ($responseApi['status']['success'] == true) {
                $user = $responseApi['data'];

                $productId = $product_id;
                $surveyId = $code;
                $userId = $user['id'];
                $model = '';
                $email = $email;
                $firstName = $user['first_name'];
                $lastName = $user['last_name'];
                $phoneNumber = $user['phone'] ? $user['phone'] : '-' ;

                // find or create buyer category
                $categoryParams = array(
                    'survey_id' => $surveyId,
                    'name' => 'buyer'
                );
                $category = Category::firstOrCreate($categoryParams);

                // find or create observer
                $observerParams = array(
                    'category_id' => $category->id,
                    'first_name' => $firstName,
                    'last_name' => $lastName,
                    'email' => $email,
                    'phone_number' => $phoneNumber ? $phoneNumber:''
                );
                $observer = Observer::create($observerParams);

                // generate batch
                $encryptIds = $productId . '+' . $surveyId . '+' . $userId . '+' . $observer->id;
                $code = strtr(base64_encode(openssl_encrypt($encryptIds, 'AES-128-ECB', 'l1tprofiler')), '+/=', '-_,');

                // find or create batches
                $batchParams = array(
                    'observer_id' => $observer->id,
                    'survey_id' => $surveyId,
                    'is_done' => false,
                    'batch' => $code,
                );
                $batch = Batch::firstOrCreate($batchParams);

                // create user survey
                $userSurveyParams = array(
                    'survey_id' => $surveyId,
                    'observer_id' => $observer->id,
                    'user_id' => $userId,
                    'product_id' => $productId,
                    'model' => $model,
                    'email' => $email
                );
                $userSurvey = UserSurvey::firstOrCreate($userSurveyParams);

                // generate url
                $link = url('preview/customer/' . $code);
                return redirect($link);
            } else {
                return true;
            }
        // } catch (Exception $e) {
        //     return response()->error([
        //         'message' => $e->getMessage()
        //     ]);
        // }
    }

    /**
     * @author Rangga <ranggapl@windowslive.com>
     * @return [json] [return object]
     */
    public function previewAsConsumer(Request $request)
    {
        $uniqueId = $request->input('id');
        $alias = "preview_survey_" . $uniqueId;
        $surveyParams = array();
        if(Storage::disk('local')->exists($alias)) {
            $contents = Storage::get($alias);
            $contents = json_decode($contents, true);
            if($contents) {
                $surveyParams = (object) $contents;
            }
        }

        $categories = array();
        if($surveyParams->profiler_type != 'self-profiler') {
            $categories = CategoryProfileDetail::where('category_profile_id', $surveyParams->category_profile_id)
            ->get();
        }
        $surveyId = isset($surveyParams->id) ? $surveyParams->id : $alias;
        $data = [
            'session_id' => $uniqueId,
            'survey' => $surveyParams,
            // 'observer' => $observer,
            // 'category' => $observer->category ? $observer->category->name : '-',
            'categories' => $categories,
            'id' => strtr(base64_encode(openssl_encrypt($surveyId, 'AES-128-ECB', 'l1tprofiler')), '+/=', '-_,'),
            'type' => 'survey',
            'code' => '',
            'userSurvey' => array(),
            'user_id' => 0
        ];
        return view('previews.as-consumer', $data);
    }

    /**
     * @author Rangga <ranggapl@windowslive.com>
     * @return [json] [return object]
     */
    public function backupAnswers(Request $request)
    {
        $status = true;
        $code = 200;
        $message = "Answers has been backuped";
        try {
            $answerParams = $request->except('_token');
            $file = 'answer-backups/' . $answerParams['link_code'] . '.txt';
            $content = json_encode($answerParams);
            Storage::disk('local')->put($file, $content);
        } catch(Exception $e) {
            $status = false;
            $code = 400;
            $message = $e->getMessage();
        }

        $response = array(
            'code' => $code,
            'status' => $status,
            'message' => $message
        );
        return response()->json($response);
    }

    public function getScoreDataBySurveyId($survey_id)
    {
        //Get data all by suevery id
        $query='SELECT a.survey_id,a.observer_id,e.name as observer_name,b.question_id,f.name as question_name,b.answer,c.answer_configs,c.answer_scores,c.answer_attributes,c.answer_type from answer_headers a inner join answer_details b on a.id=b.answer_header_id
                inner join questions c on b.question_id=c.id
                left join observers d on d.id=a.observer_id
                left join categories e on d.category_id=e.id
                left join questions f on b.question_id=f.id
                where a.survey_id='.$survey_id.' order by c.answer_attributes';
        $result=DB::select($query);

        $raw_data = array();
        $question_attribut=array();

        foreach ($result  as $key=>$value) {
           $raw_data[$key]=$this->getScoreAnswerBySurvey($value);
        }

        return $raw_data;
    }

    public function getScoreAnswerBySurvey($data)
    {

        if($data->answer_type == "free_text" || $data->answer_type == "ranking" || $data->answer_type == "date" || $data->answer_type == "time") {

        }else if($data->answer_type == "multichoice" || $data->answer_type == "boolean" || $data->answer_type == "likert_scale" || $data->answer_type == "checkboxes" || $data->answer_type == "multiple_choice_with_constraint") {
            $answerIndex = array_search($data->answer, json_decode($data->answer_configs));
            $score = (integer)json_decode($data->answer_scores)[$answerIndex];

            if($data->observer_name=='buyer'){
                $obersever_name='Self';
            }else{
                $obersever_name=$data->observer_name;
            }

            $id_attributes=json_decode($data->answer_attributes)[$answerIndex];

            $question_attribut=Attribute::where('survey_id',$data->survey_id)
                                    ->where('id',$id_attributes)
                                    ->select('name')->pluck('name');

            $result = array('survey_id'=>$data->survey_id,
                            'observer_id'=>$data->observer_id,
                            'observer_name'=>$obersever_name,
                            'question_id'=>$data->question_id,
                            'question_name'=>$data->question_name,
                            'answer'=>$data->answer,
                            'score'=>$score,
                            'attributes'=>$question_attribut[0]);                                    
        }
        return $result;
    }

    public function getRawDataGroupByQuestion($data)
    {
        if($data->answer_type == "free_text" || $data->answer_type == "ranking" || $data->answer_type == "date" || $data->answer_type == "time") {

        }else if($data->answer_type == "multichoice" || $data->answer_type == "boolean" || $data->answer_type == "likert_scale" || $data->answer_type == "checkboxes" || $data->answer_type == "multiple_choice_with_constraint") {
            $answerIndex = array_search($data->answer, json_decode($data->answer_configs));
            $score = (integer)json_decode($data->answer_scores)[$answerIndex];

            $id_attributes=json_decode($data->answer_attributes)[$answerIndex];

            $question_attribut=Attribute::where('survey_id',$data->survey_id)
                                    ->where('id',$id_attributes)
                                    ->select('name')->pluck('name');

            $result = array('survey_id'=>$data->survey_id,
                            'question_id'=>$data->question_id,
                            'question_name'=>$data->question_name,
                            'attributes_id'=>$id_attributes[0],
                            'attributes'=>$question_attribut[0]);                                    
        }
        return $result;
    }

    public function getAttributeByQuestion($survey_id,$question_id)
    {
        //Get data all by suevery id
        $query='SELECT a.survey_id,a.observer_id,e.name as observer_name,b.question_id,f.name as question_name,b.answer,c.answer_configs,c.answer_scores,c.answer_attributes,c.answer_type from answer_headers a inner join answer_details b on a.id=b.answer_header_id
                inner join questions c on b.question_id=c.id
                left join observers d on d.id=a.observer_id
                left join categories e on d.category_id=e.id
                left join questions f on b.question_id=f.id
                where a.survey_id='.$survey_id.' and b.question_id='.$question_id.' order by c.answer_attributes';
        $result=DB::select($query);

        $raw_data = array();
        $question_attribut=array();

        foreach ($result  as $key=>$value) {
           $raw_data[$key]=$this->getRawDataGroupByQuestion($value);
        }

        return $raw_data;
    }

    /*
    
     */
    public function printReportPreview(Request $request)
    {
        $survey_current = Survey::findOrFail($request->data_source);
        $observer_current = $survey_current->ownerObserver()->first();
        $object_dummy_id=4;

        $encryptIds = '0+' . $survey_current->id . '+' . $survey_current->creator_id . '+' . ((is_object($observer_current))?$observer_current->id:1) . '+' . $object_dummy_id;
        $batch = strtr(base64_encode(openssl_encrypt($encryptIds, 'AES-128-ECB', 'l1tprofiler')), '+/=', '-_,');

        $oriBatch = $batch;
        $batch = openssl_decrypt(base64_decode(strtr($batch, '-_,', '+/=')), 'AES-128-ECB', 'l1tprofiler');
        // index 0: product_id, 1: survey_id, 2: user_id, 3: observer_id
        $ids = explode('+', $batch);

        Log::info($ids);
        
        //$buyer = Observer::findOrFail($ids[3]);
        $survey = Survey::findOrFail($ids[1]);
        $userSurvey = UserSurvey::where('product_id', $ids[0])
            ->where('survey_id', $ids[1])
            ->where('user_id', $ids[2])
            ->where('observer_id', $ids[3])
            ->first();


        // new
        $html = (!empty($request->content) ? $request->content : '<div></div>');
        $htmlDOM = str_get_html($html);

        $style = '<style type="text/css">' . view('template-builders.covers.css')->render() . '</style>';
        $cover =  $request->cover_style;
        $covers = 'template-builders.' . $cover . '.cover';


        if (in_array($cover, ['style_0', 'style_1', 'style_2', 'style_3', 'style_4', 'style_5'])) {
            if ( view()->exists('template-builders.covers.' . $cover) ) {
                $covers = 'template-builders.covers.' . $cover;
            }
            $base = $style . htmlspecialchars_decode(view($covers)->render()) . htmlspecialchars_decode(view('template-builders.covers.' . $cover . '_format')->render());
            $footer = htmlspecialchars_decode(view('template-builders.covers.' . $cover . '_footer')->render());
            //print_r($base);exit;
        } else {
            if ( view()->exists('template-builders.' . $cover . '.cover') ) {
                $covers = 'template-builders.' . $cover . '.cover';
            }
            $base = $style . htmlspecialchars_decode(view($covers)->render()) . htmlspecialchars_decode(view('template-builders.' . $cover . '.format')->render());
            $footer = htmlspecialchars_decode(view('template-builders.' . $cover . '.footer')->render());
        }


        Log::info('[communication-style]');
        foreach ($htmlDOM->find('code') as $code) {
            if ($code->innertext() == "[communication-style]") {
                $questionsComStyle = array();
                $comStyleAttributes = array();
                foreach ($survey->attributes as $attribute) {
                    $comStyleAttributes[$attribute->id] = $attribute->name;
                }
                $index = 0;
                foreach ($survey->sections as $section) {
                    foreach ($section->questions as $question) {
                        $questionsComStyle[$index] = $question->toArray();
                        $answerHeaderData = AnswerHeader::where('answer_headers.survey_id', $ids[1])
                            ->where('answer_details.question_id', $question->id)
                            ->join('answer_details', 'answer_details.answer_header_id', '=', 'answer_headers.id')
                            ->where('answer_headers.observer_id', $ids[3])
                            ->select('answer_details.answer', 'answer_headers.survey_id', 'answer_details.question_id', 'answer_details.answer_header_id', 'answer_headers.observer_id')
                            ->first();

                        if ($answerHeaderData) {
                            $questionsComStyle[$index]['answers'] = $answerHeaderData->toArray();
                        } else {
                            $questionsComStyle[$index]['answers'] = array('answer' => '');
                        }

                        $index += 1;
                    }
                }
                Log::info('QuestionStyle');
                
                $html = str_replace($code, htmlspecialchars_decode(view('template-builders.communication-style')->withQuestions($questionsComStyle)->withAttributes($comStyleAttributes)->render()), $html);
            }
        }

        $htmlDOM = str_get_html($html);
        //print_r($htmlDOM);exit;
        foreach ($htmlDOM->find('code') as $key => $code) {
            if ($code->innertext() == '[new-page]') {
                $target = $code->parent()->parent();
                $className = $target->getAttribute('class') . " force-float";

                $target->setAttribute('class', $className);
                $code->setAttribute('data-page', $key + 2);
            }
        }

        $html = $htmlDOM;

        // [starhub-johari] per attribute
        foreach ($htmlDOM->find('.starhub-johari') as $code) {
            $text = explode('-', $code->innertext());
            $attribute = Attribute::find($text[2]);

            if ($attribute) {

                $johariDetail = '';
                $attributes = array();

                $dataPersonal = $this->dataPersonalAttribute($ids[1], $ids[3], $text[2]);
                $attributes = $this->getPersonalScoreAttribute($dataPersonal, 'Self', $text[2], $attributes);

                foreach($survey->savedCategories($ids[2])->get() as $category) {
                    foreach ($category->observers as $observer) {
                        $tempDataPersonal = $this->dataPersonalAttribute($ids[1], $observer->id, $text[2]);

                        if (count($tempDataPersonal) > 0) {
                            $dataPersonal = $tempDataPersonal;
                            $attributes = $this->getPersonalScoreAttribute($dataPersonal, $category->name, $text[2], $attributes);

                        } else {
                            if (!isset($attributes[$text[2]])){
                                $attributes[$text[2]] = array();
                            }

                            if (!isset($attributes[$text[2]]['Self'])) {
                                $newVar = array();
                                foreach ($attributes[$text[2]] as $values) {
                                    foreach ($values as $key => $value) {
                                        $newVar['Self'][$key] = 0;
                                    }
                                }
                                $attributes[$text[2]] = array_merge($newVar, $attributes[$text[2]]);
                            }

                            $attributes[$text[2]][$category->name] = $attributes[$text[2]]['Self'];

                            foreach ($attributes[$text[2]][$category->name] as $key => $value) {
                                $attributes[$text[2]][$category->name][$key] = 0;
                            }
                        }
                    }

                    if (isset($attributes[$text[2]][$category->name])) {
                        $totalObs = 0;
                        foreach ($category->observers as $observer) {
                            if ($observer->answerHeader->count() > 0) {
                                $totalObs += 1;
                            }
                        }

                        foreach ($dataPersonal as $master) {
                            $attributes[$text[2]][$category->name][$master->question_id] = $attributes[$text[2]][$category->name][$master->question_id] / ($totalObs > 0 ? $totalObs : 1);
                        }
                    }
                }

                foreach ($dataPersonal as $master) {
                    $self = array('Self' => $attributes[$master->attribute_id]['Self']);
                    unset($attributes[$master->attribute_id]['Self']);
                    $obsAve = array('Obs. Ave' => array());
                    $attributes[$master->attribute_id] = array_merge($self, $obsAve, $attributes[$master->attribute_id]);

                    foreach ($attributes as $key => $data) {
                        $count = 0;
                        $last = 0;
                        foreach ($data as $name => $values) {
                            if ($name != 'Self' && $name != 'Obs. Ave') {
                                $last += $values[$master->question_id];
                                $count += 1;
                            }
                        }

                        $attributes[$master->attribute_id]['Obs. Ave'][$master->question_id] = round($last / ($count == 0 ? 1 : $count), 2);
                    }
                }

                foreach (array_chunk($attributes, 2, true) as $attributeData) {
                    $newId = array();
                    foreach ($attributeData as $key => $value) {
                        $newId[] = $key;
                    }
                    $attribute = Attribute::whereIn('id', $newId)->get();
                    if ($attribute) {
                        // $johariDetail .= view('template-builders.' . $cover . '.johari', compact('attribute', 'attributeData', 'dataPersonal'))->render();
                        $johariDetail .= view('template-builders.starhub.johari', compact('attribute', 'attributeData', 'dataPersonal'))->render();
                    }
                }
                $html = str_replace($code, htmlspecialchars_decode($johariDetail), $html);
            }
        }

        // render report formulas
        foreach($htmlDOM->find('.replace-formula code') as $wrapper) {
            $formulaId = str_replace('formula-', '', $wrapper->innertext());
            $reportFormula = ReportFormula::find($formulaId);
            $result = $this->mappingReportFormula($reportFormula);
            $html = str_replace($wrapper, $reportFormula->name . ' : ' . $result, $html);
        }
        // the end of report formulas
               
        foreach ($htmlDOM->find('code') as $code) {

            $text = explode('-', $code->innertext());

            if ($code->innertext() !== "[communication-style]") {
                $question = Question::find($text[1]);

                if ($question) {
                    if ($text[0] == "answer") {
                        $answers = AnswerHeader::where('answer_headers.survey_id', $ids[1])
                            ->where('answer_details.question_id', $text[1])
                            ->join('answer_details', 'answer_details.answer_header_id', '=', 'answer_headers.id');

                        if ($text[2] == "primary_user") {
                            $answers = $answers->where('answer_headers.observer_id', $ids[3]);
                        }

                        $answers = $answers->pluck('answer_details.answer', 'answer_details.question_id');

                        if (count($answers) == 0) {
                            $answers = null;
                        }

                        $html = str_replace($code, htmlspecialchars_decode(view('template-builders.answer', compact('question'))->withAnswers($answers)->render()), $html);

                    } else if ($text[0] == "percentage") {
                        $answers = AnswerHeader::where('answer_headers.survey_id', $ids[1])
                            ->where('answer_details.question_id', $text[1])
                            ->join('answer_details', 'answer_details.answer_header_id', '=', 'answer_headers.id')
                            ->pluck('answer_details.answer', 'answer_details.question_id');

                        $percentage = array();

                        $rawData = AnswerHeader::select('answer_details.answer', DB::raw('count(answer_details.id) as count'))
                            ->where('answer_headers.survey_id', $ids[1])
                            ->where('answer_details.question_id', $text[1])
                            ->join('answer_details', 'answer_details.answer_header_id', '=', 'answer_headers.id')
                            ->groupBy('answer_details.answer');

                        $rawCount = count($rawData->get());

                        foreach ($rawData->get() as $data) {
                            $percentage[$data->answer] = round(($data->count / $rawCount)*100, 2);
                        }

                        if (count($answers) == 0) {
                            $answers = null;
                        }

                        $html = str_replace($code, htmlspecialchars_decode(view('template-builders.percentage', compact('question'))->withAnswers($answers)->withPercentage($percentage)->render()), $html);

                    } else if ($text[0] == "johari") {
                        // johari detail
                        $johariDetail = '';
                        $questions = array();

                        $dataPersonal = $this->dataPersonal($ids[1], $ids[3], $text[1]);
                        $questions = $this->getPersonalScore($dataPersonal, 'Self', $question->id, $questions);

                        foreach($survey->savedCategories($ids[2])->get() as $category) {
                            foreach ($category->observers as $observer) {
                                $dataPersonal = $this->dataPersonal($ids[1], $observer->id, $question->id);
                                $questions = $this->getPersonalScore($dataPersonal, $category->name, $question->id, $questions);
                            }

                            if (isset($questions[$question->id][$category->name])) {
                                $totalObs = 0;
                                foreach ($category->observers as $observer) {
                                    if ($observer->answerHeader->count() > 0) {
                                        $totalObs += 1;
                                    }
                                }
                                // $questions[$question->id][$category->name] = $questions[$question->id][$category->name] / ($totalObs > 0 ? $totalObs : 1);
                                $questions[$question->id][$category->name] =round(($questions[$question->id][$category->name] / ($totalObs > 0 ? $totalObs : 1)),2);
                            }
                        }

                        foreach (array_chunk($questions, 2, true) as $questionData) {
                            $newId = array();
                            foreach ($questionData as $key => $value) {
                                $newId[] = $key;
                            }
                            $question = Question::whereIn('id', $newId)->get();
                            if ($question) {
                                $johariDetail .= view('template-builders.johari', compact('question', 'questionData'))->render();
                            }
                        }

                        $html = str_replace($code, htmlspecialchars_decode($johariDetail), $html);

                    }
                }
            }
        }

        //Create table respon table
        foreach ($htmlDOM->find('code') as $code) {

            if ($code->innertext() == '[response-table]' || $code->innertext() == '[response-tquestion]') {
                // summary

                $responseTable = '';
                $dataPersonal = $this->dataPersonal($ids[1], $ids[3], 0);
                // $dataPersonal = $this->dataPersonal($ids[1], $ids[3], $question->id);
                $attributes = array();
                
                if ($code->innertext() == '[response-table]') {
                    $attributes = $this->getScore($dataPersonal, 'Self', 0, $attributes);
                    // $attributes = $this->getScore($dataPersonal, 'Self', $question->id, $attributes);
                } else {
                    $attributes = $this->getScoreTQuestion($dataPersonal, 'Self', 0, $attributes);
                }

                foreach($survey->savedCategories($ids[2])->get() as $category) {
                    foreach ($category->observers as $observer) {

                        $dataPersonal = $this->dataPersonal($ids[1], $observer->id, 0);
                        if ($code->innertext() == '[response-table]') {
                            $attributes = $this->getScore($dataPersonal, $category->name, $observer->id, $attributes);

                        } else {
                            $attributes = $this->getScoreTQuestion($dataPersonal, $category->name, $observer->id, $attributes);

                        }
                    }
                }


                $responseTable = '<br><br><br><br><br><table class="table-style-table">';

                $responseTableHeader = '<tr>';
                $responseTableHeader .= '<td class="exclude-style-table"></td>';
                $responseTableHeader .= '<td>S</td>';

                $responseTable .= '<thead>';
                $responseTable .= '<tr>';
                $responseTable .= '<td class="exclude-style-table"></td>';
                $responseTable .= '<td><strong>Self</strong></td>';

                if ($code->innertext() != '[response-table]') {
                    $responseTableFooter = '<tr><td>Summary</td>';
                }

                foreach($survey->savedCategories($ids[2])->get() as $category) {
                    $responseTable .= '<td class="class-bg-'.$category->name[0].'" colspan="'.$category->observers()->count().'"><strong>'.$category->name.'(s)</strong></td>';

                    foreach($category->observers as $key => $observer) {
                        $responseTableHeader .= '<td class="class-bg-'.$category->name[0].'">'.$category->name[0].($key+1).'</td>';
                    }

                    if (count($category->observers) == 0) {
                        $responseTableHeader .= '<td class="class-bg-'.$category->name[0].'">-</td>';
                    }

                }
                $responseTable .= '</tr>';
                $responseTable .= $responseTableHeader . '</tr>';
                $responseTable .= '</thead>';

                $total = array('Self' => 0);

                //add variable for sum total each category
                //added by puw on 29-01-2018
                $total_each_category=array();
                $total_each_category_ittr=array();
                $category_name_ittr=array();
                $attributes_before=array();
                $tag_total_each_category='';
                $tag_total_each_category_last='';

                $attributes_ittr=0;
                //$responseTable_row='';
                foreach ($attributes as $key => $attribute) {
                    $responseTable_row='';
                    $tag_total_each_category_last='';
                    if ($code->innertext() == '[response-table]') {
                        $attributeData = Attribute::find($key);
                    } else {
                        $attributeData = Question::find($key);
                        $AttributeByQuestion=$this->getAttributeByQuestion($ids[1],$key)[0];
                    }

                   
                    //$attributeData = Question::find($key);

                    if ($attributeData) {
                        

                        $responseTable_row .= '<tr>';
                        if ($code->innertext() == '[response-table]') {
                            $responseTable_row .= '<td><strong>'.$attributeData->name.'</strong></td>';
                        } else {
                            $responseTable_row .= '<td><strong>'.strip_tags($attributeData->name).'</strong></td>';
                        }
                        
                        $total['Self'] += $attribute['Self'];
                
                        $responseTable_row .= '<td>'.$attribute['Self'].'</td>';

                        if ($code->innertext() != '[response-table]') {

                            $attributes_before[$attributes_ittr]['Self']=$AttributeByQuestion['attributes_id'];

                            $category_name_ittr[$attributes_ittr]=$AttributeByQuestion['attributes'];

                            if($attributes_ittr>0){
                                if ($attributes_before[$attributes_ittr-1]['Self']==$AttributeByQuestion['attributes_id']) {
                                        $value_before=$total_each_category_ittr[$attributes_ittr-1]['Self'];
                                        $total_each_category_ittr[$attributes_ittr]['Self']=$value_before+$attribute['Self'];

                                        $tag_total_each_category_last .='<tr><td><strong> Total '.$category_name_ittr[$attributes_ittr].'</strong></td>';
                                        $tag_total_each_category_last .='<td><strong>'.$total_each_category_ittr[$attributes_ittr]['Self'].'</strong></td>';

                                   }else{
                                        $total_each_category_ittr[$attributes_ittr]['Self']=$attribute['Self'];
                                        $tag_total_each_category .='<tr><td><strong> Total '.$category_name_ittr[$attributes_ittr-1].'</strong></td>';
                                        $tag_total_each_category .='<td><strong>'.$total_each_category_ittr[$attributes_ittr-1]['Self'].'</strong></td>';

                                        $tag_total_each_category_last='';
                                   }   
                            }else{
                                //Pertanyaan pertama
                                $total_each_category_ittr[$attributes_ittr]['Self']=$attribute['Self'];
                            }
                        }

                        foreach($survey->savedCategories($ids[2])->get() as $category) {
                            if (!isset($total[$category->name])) {
                                $total[$category->name] = array();
                            }
                            foreach ($category->observers as $observer) {
                                if (!isset($total[$category->name][$observer->id])) {
                                    $total[$category->name][$observer->id] = 0;
                                    
                                }
                                $responseTable_row .= '<td>';
                                //$total_each_category_ittr[$attributes_ittr]=array();

                                if (isset($attribute[$category->name])) {
                                    if (isset($attribute[$category->name][$observer->id])) {
                                        $responseTable_row .= $attribute[$category->name][$observer->id];
                                        
                                        if ($code->innertext() != '[response-table]') {
                                            // $total[$category->name][$observer->id] += $attribute[$category->name][$observer->id];
                                            $total[$category->name][$observer->id] += $attribute[$category->name][$observer->id];

                                            //added by puw on 29-0-2018
                                            $attributes_before[$attributes_ittr][$category->name.$observer->id]=$AttributeByQuestion['attributes_id'];
                                            if($attributes_ittr>0){
                                                if ($attributes_before[$attributes_ittr-1][$category->name.$observer->id]==$AttributeByQuestion['attributes_id']) {

                                                        $value_before=$total_each_category_ittr[$attributes_ittr-1][$category->name.$observer->id];
                                                        $total_each_category_ittr[$attributes_ittr][$category->name.$observer->id]=$value_before+$attribute[$category->name][$observer->id];
                                                        $tag_total_each_category_last .='<td><strong>'.$total_each_category_ittr[$attributes_ittr][$category->name.$observer->id].'</strong></td>';
                                                   }else{
                                                        $total_each_category_ittr[$attributes_ittr][$category->name.$observer->id]=$attribute[$category->name][$observer->id];

                                                        $tag_total_each_category .='<td><strong>'.$total_each_category_ittr[$attributes_ittr-1][$category->name.$observer->id].'</strong></td>';

                                                        $tag_total_each_category_last='';
                                                        
                                                        //$tag_total_each_category .='</tr>';
                                                   }   
                                            }else{
                                                //Pertanyaan pertama
                                                $total_each_category_ittr[$attributes_ittr][$category->name.$observer->id]=$attribute[$category->name][$observer->id];
                                            }
                                        }

                                    } else {
                                        $responseTable_row .= '-';
                                    }
                                } else {
                                    $responseTable_row .= '-';
                                }
                                $responseTable_row .= '</td>';
                            }

                            if (count($category->observers) == 0) {
                                $responseTable_row .= '<td>-</td>';
                            }
                        }
                        $tag_total_each_category .='</tr>';

                        $responseTable_row .= '</tr>';
                        $tag_total_each_category_last .='</tr>';

                        
                        //
                        //Add by puw on 26-01-2018
                        //to show up
                        if (($code->innertext() != '[response-table]') && $survey->profiler_type=='360-profiler') {
                            if($attributes_ittr>0){
                                if ($attributes_before[$attributes_ittr-1]['Self']==$AttributeByQuestion['attributes_id']) {
                                   }else{
                                        $responseTable .=$tag_total_each_category;
                                   }   
                            }
                        }
                        $responseTable .=$responseTable_row;

                    }

                    $attributes_ittr +=1;
                    $tag_total_each_category='';
                }

                //Added to last row for last category
                if (($code->innertext() != '[response-table]') && $survey->profiler_type=='360-profiler') {
                    if($attributes_ittr>0){
                         $responseTable .=$tag_total_each_category_last;
                    }
                }

                //Showup Sum total of all row in last Table
                if ($code->innertext() != '[response-table]') {
                    foreach ($total as $key => $values) {
                        if (is_array($values)) {
                            foreach ($values as $value) {
                                $responseTableFooter .= '<td><strong>'.$value.'</strong></td>';
                            }
                        } else {
                            $responseTableFooter .= '<td><strong>'.$values.'</strong></td>';
                        }
                    }
                    //$responseTable .= $responseTableFooter.'</tr>';
                }
                
                $responseTable .= '</table>';

                //edited by puw on 24-12-2018
                $html = str_replace($code, htmlspecialchars_decode($responseTable), $html);
            }
        }

        

        foreach ($htmlDOM->find('.editable') as $editable) {
            if ($editable->getAttribute('data-selector') != "title") {
                $html = str_replace($editable, htmlspecialchars_decode('<div class="content-text">' . $editable->innertext() . '</div>'), $html);
            }
        }

        $html = str_replace('[change-with-element]', $html, $base);

        foreach ($htmlDOM->find('code') as $key => $code) {
            if ($code->innertext() == '[new-page]') {
                $html = str_replace($code, $footer, $html);
                $html = str_replace('[page-number]', $code->getAttribute('data-page'), $html);
            }
        }
        // $html = str_replace('<code>[new-page]</code>', $footer, $html);

        $html = str_replace('[report-header]', $request->header_text, $html);
        
        $html = str_replace('[report-header-align]', $request->header_alignment, $html);
        $html = str_replace('[report-footer]', $request->footer_text, $html);
        $html = str_replace('[report-footer-align]', $request->footer_alignment, $html);

        $html = str_replace('[report-background]', $request->cover_background, $html);

        // added by rdw 151217
        $html = str_replace('[report-description]', $request->report_desc, $html);
        $html = str_replace('https://storage.googleapis.com/learn1thing/img/brand-logo.png', ($request->logo_url=='' ? 'https://storage.googleapis.com/learn1thing/img/brand-logo.png' : $request->logo_url ), $html);


        $html = str_replace('<button class="close removeElement" type="button">×</button>', '', $html);
        
        
        //$html = str_replace('[product-purchaser]', ($buyer->full_name()), $html);
        $html = str_replace('[survey-title]', $survey->title, $html);
        $html = str_replace('[survey-type]', $survey->profiler_type, $html);
        $html = str_replace('[survey-batch_reference]', $survey->batch_reference, $html);
        $html = str_replace('[survey-description]', nl2br($survey->description), $html);
        $html = str_replace('[survey-period_start-simple]', date('d-m-Y'), $html);
        $html = str_replace('[survey-period_start-style]', date('d ') . '<span class="month">' . date('F') . '</span>' . date('Y'), $html);
        $html = str_replace('[survey-period_start-flat]', date('F d,Y'), $html);

        $folder = date('ymd');
        $baseUrl = public_path()."/report/";
        exec("mkdir ".$baseUrl);
        file_put_contents($baseUrl.'/preview-'.$folder.'.html', $html);

        // new
        exec("prince " . $baseUrl . "/preview-".$folder.".html -o " . $baseUrl . "/preview-".$folder.".pdf --pdf-title='" . $survey->title . "' --page-margin=0 -s " . public_path('/css/report-template.css'));

        $filename = "report/preview-".$folder.".pdf";
        
        $app_url=(ENV('APP_URL'))?ENV('APP_URL'):'https://store.learn1thing.com/profiler';

        return  $app_url.'/'.$filename;

        $path = public_path($filename);
        
        return Response::make(file_get_contents($path), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$filename.'"'
        ]);
        
    }

}
