<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EmailBuilder;
use App\Models\Survey;
use App\Models\Question;
use App\Models\CategoryProfileDetail;
use App\DataTables\TemplateDataTable;
use Client;
use Response;

use Illuminate\Support\Facades\Log; 

include(app_path().'/Helpers/simple_html_dom.php');

class TemplateBuilderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function new_index(TemplateDataTable $dataTable, Request $request)
    {
        return $dataTable
            ->with('user_id', session('user')['id'])
            ->with('survey_id', null)
            ->render('template-builders.new-index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = [];
        $roles = [];
        $departmentRequest = Client::setEndpoint('department')->get();
        if ($departmentRequest['status']['success'] == true) {
            $departmentData = $departmentRequest['data'];
            foreach ($departmentData as $value) {
                $departments[] = array('id' => $value['id'], 'text' => $value['name']);
            }
        }
        $roleRequest = Client::setEndpoint('role')->get();
        if ($roleRequest['status']['success'] == true) {
            $roleData = $roleRequest['data'];
            foreach ($roleData as $value) {
                $roles[] = array('id' => $value['id'], 'text' => $value['name']);
            }
        }

        $data['profilers'] = Survey::where('creator_id', session('user')['id'])->pluck('title', 'id')->toArray();
        $data['emailBuilder'] = null;
        $data['departments'] = $departments;
        $data['roles'] = $roles;
        return view('template-builders.new')->withData($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function new_store(Request $request)
    {
        $request->merge(['user_id' => session('user')['id']]);
        $data = $request->all();
        $data['content'] = htmlentities($data['content']);
        $emailBuilder = EmailBuilder::create($data);

        if ($emailBuilder) {
            $this->doUpload($data['cover_background'], $emailBuilder->id, 'cover_background');
            $this->doUpload($data['content'], $emailBuilder->id, 'content');

            echo 'ok';
        } else {
            echo 'error';
        }
        return $this->actionRedirect('create', $data['name']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($token, $id, Request $request)
    {
        $data['profilers'] = Survey::where('creator_id', session('user')['id'])->pluck('title', 'id')->toArray();
        $data['emailBuilder'] = EmailBuilder::find($id);
        return view('template-builders.edit')->withData($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($token, Request $request, $emailBuilder)
    {
        Log::info('Masuk update template report');
        Log::info('emailBuilder_id'.$emailBuilder);
        $emailBuilder = EmailBuilder::find($emailBuilder);
        $request->merge(['user_id' => session('user')['id']]);
        

        Log::info('user_id: '.session('user')['id']);

        $data = $request->all();
        
        $data['content'] = htmlentities($data['content']);
        
        $emailBuilder->update($data);

        if ($emailBuilder) {
            $this->doUpload($data['cover_background'], $emailBuilder->id, 'cover_background');
            $this->doUpload($data['content'], $emailBuilder->id, 'content');
            Log::info('Success Update Template builder');
        } else {
           Log::info('Error Update Template builder');
        }
        return $this->actionRedirect('update', $data['name']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($token, $emailBuilder)
    {
        $emailBuilder = EmailBuilder::find($emailBuilder);
        $title = $emailBuilder->name;
        $survey = $emailBuilder->survey;

        $emailBuilder->delete();
        if ($survey) {
            if ($survey->email_builder_id == $emailBuilder->id) {
                $survey->update(['email_builder_id' => $survey->reportTemplate->first()->id]);
            }
        }

        return $this->actionRedirect('delete', $title);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function setDefault($token, Request $request, $emailBuilder)
    {
        $builder = EmailBuilder::find($emailBuilder);
        $return = [
            'position' => 'template'
            , 'status' => 'danger'
            , 'message' => 'Failed to update template'
        ];

        if ($builder) {
            $survey = $builder->survey->update(['email_builder_id' => $emailBuilder]);

            if ($survey) {
                $return['status'] = 'success';
                $return['message'] = "Success to update template";
            }
        }

        return $return;
    }

    /**
     * Handle store and update.
     *
     * @param  string  $method
     * @param  string  $name
     * @return \Illuminate\Http\Response
     */
    private function actionRedirect($mehtod, $name)
    {
        return redirect(\RouteHelper::token('template-builder.index'))->withSuccess(
            trans("messages.success.$mehtod", [
                'name' => sprintf('report %s', strtolower($name))
            ])
        );
    }

    public function doUpload($html, $id, $field) {
        if ($field == "content") {
            if (!empty($html)) {
                $htmlDOM = str_get_html(htmlspecialchars_decode($html));
                foreach ($htmlDOM->find('img') as $key => $img) {
                    $res = $this->toImage($img->getAttribute('src'), $key, $id);

                    if ($res) {
                        $html = str_replace($img->getAttribute('src'), $res['path'], $html);
                    }
                }
            }
        } elseif($field=="logo_url") {
            $res = $this->toImage($html, 'logo_url', $id);
            if ($res) {
                $html = $res['path'];
            }
        } else {
            $res = $this->toImage($html, 'cover', $id);
            if ($res) {
                $html = $res['path'];
            }
        }

        $emailBuilder = EmailBuilder::find($id);
        $emailBuilder->update([$field => $html]);
    }

    public function toImage($baseImage, $file, $folder) {
        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode( ',', $baseImage );
        $status = false;

        if (@$data[ 1 ]) {
            $ext = str_replace('data:image/', '', $data[0]);
            $ext = str_replace(';base64', '', $ext);
            $folderPath = "/report/";
            //$folderPath = "\\report\\";
            shell_exec("mkdir " . public_path() . $folderPath);
            $folderPath .= $folder . "/";
            //$folderPath .= $folder . "\\";
            shell_exec("mkdir " . public_path() . $folderPath);
            $filePath =  $file . "." . $ext;
            //decode base64 string

            // open the output file for writing
            $ifp = fopen( public_path() . $folderPath . $filePath, 'wb' );

            // we could add validation here with ensuring count( $data ) > 1
            fwrite( $ifp, base64_decode( $data[ 1 ] ) );

            // clean up the file resource
            fclose( $ifp );

            $return = ENV('APP_URL') . $folderPath . $filePath;
        } else {
            $return = $baseImage;
        }

        return array('status' => $status, 'path' => $return);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('template-builders.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge(['user_id' => session('user')['id']]);
        $data = $request->all();
        $data['content'] = htmlentities($data['content']);
        $emailBuilder = EmailBuilder::create($data);
        if ($emailBuilder) {
            echo 'ok';
        } else {
           echo 'error';
        }
    }

    public function getDetails($token, Request $request) {
        $survey = Survey::with(['formulas' => function ($query)
        {
            $query->where('is_parent', true);
        }])->find($request->input('profiler_id'));
        $questions = collect([]);
        $observerCategories = collect([]);
        if ($survey) {
            foreach ($survey->sections as $section) {
                foreach ($section->questions as $question) {
                    $questions[] = $question;
                }
            }

            $survey->attributes;

            $categories = CategoryProfileDetail::where('category_profile_id', $survey->category_profile_id)
                ->get();
            foreach($categories as $category) {
                $observerCategories[] = array('id' => $category->id, 'text' => $category->name);
            }

            $survey->observer_categories = $observerCategories;
            $survey->questions = $questions;
        }

        return $survey;
    }

    public function getTemplates()
    {
        $baseQuery = EmailBuilder::where('user_id', session('user')['id']);

        $response['code'] = 1;
        if ($baseQuery->count() > 0) {
            $response['code'] = 0;
        }
        $response['files'] = $baseQuery->get()->toArray();

        echo json_encode($response);
    }

    public function postExport(Request $request)
    {
        $todayh = getdate();
        $filename= "email-editor-".$todayh["seconds"].$todayh["minutes"].$todayh["hours"].$todayh["mday"]. $todayh["mon"].$todayh["year"];

        $newHtmlFilename=public_path('Bal/exports/'.$filename.'.html');
        // $zipFilename=public_path('Bal/exports/'.$filename.'.zip');
        // $zipFileUrl='/Bal/exports/'.$filename.'.zip';
        $htmlFileUrl=env('APP_URL').'/Bal/exports/'.$filename.'.html';

        //read email template
        $templateContent=file_get_contents(public_path('Bal/template.html'),true);

        //create new document
        $new_content = $request->input('html');

        //view in browser link
        $new_content=str_replace('#view_web',$htmlFileUrl,$new_content);


        $content=str_replace('[email-body]',$new_content,$templateContent);
        $fp = fopen($newHtmlFilename,"wb");
        fwrite($fp,$content);
        fclose($fp);

        //create zip document
        // $zip = new \ZipArchive();

        // $zip->open($zipFilename, \ZipArchive::CREATE);
        // $zip->addFile($newHtmlFilename, 'index.html');
        // $zip->close();
        //remove html file
        // unlink($newHtmlFilename);

        $response=array();
        $response['code']=0;
        // $response['url']=$zipFileUrl;
        $response['url']=$htmlFileUrl;
        $response['preview_url']=$htmlFileUrl;
        $response['html']=$new_content;

        return $response;
    }

    public function postUpdateTemplate(Request $request)
    {
        $data = $request->all();
        $data['content'] = htmlentities($data['content']);
        $emailBuilder = EmailBuilder::find($data['id']);
        if ($emailBuilder) {
            $emailBuilder->update($data);
            echo 'ok';
        } else {
           echo 'error';
        }
    }

    public function postPreview(Request $request)
    {    
	   //var_dump($request);die();
        //Log::info($request);die();
        // $oriBatch = $batch;
        // $batch = openssl_decrypt(base64_decode(strtr($batch, '-_,', '+/=')), 'AES-128-ECB', 'l1tprofiler');
        // index 0: product_id, 1: survey_id, 2: user_id, 3: observer_id
        // $ids = explode('+', $batch);

        // $buyer = Observer::findOrFail($ids[3]);
        $survey = Survey::findOrFail($request->data_source);
        // $userSurvey = UserSurvey::where('product_id', $ids[0])
        //     ->where('survey_id', $ids[1])
        //     ->where('user_id', $ids[2])
        //     ->where('observer_id', $ids[3])
        //     ->first();

        // if (@$ids[4]) {
        //     $reportData = EmailBuilder::find($ids[4]);;

        // } else {
        //     $reportData = $survey->defaultReport;

        // }

        // new
        $html = (!empty($request->content) ? $request->content : '<div></div>');
        $htmlDOM = str_get_html($html);

        $style = '<style type="text/css">' . view('template-builders.covers.css')->render() . '</style>';
        $cover = $request->cover_style;
        $covers = 'template-builders.' . $cover . '.cover';

        if (in_array($cover, ['style_0', 'style_1', 'style_2', 'style_3', 'style_4', 'style_5'])) {
            if ( view()->exists('template-builders.covers.' . $cover) ) {
                $covers = 'template-builders.covers.' . $cover;
            }
            $base = $style . htmlspecialchars_decode(view($covers)->render()) . htmlspecialchars_decode(view('template-builders.covers.' . $cover . '_format')->render());
            $footer = htmlspecialchars_decode(view('template-builders.covers.' . $cover . '_footer')->render());
        } else {
            if ( view()->exists('template-builders.' . $cover . '.cover') ) {
                $covers = 'template-builders.' . $cover . '.cover';
            }
            $base = $style . htmlspecialchars_decode(view($covers)->render()) . htmlspecialchars_decode(view('template-builders.' . $cover . '.format')->render());
            $footer = htmlspecialchars_decode(view('template-builders.' . $cover . '.footer')->render());
        }

        foreach ($htmlDOM->find('code') as $code) {
            if ($code->innertext() == "[communication-style]") {
                $questionsComStyle = array();
                foreach ($survey->sections as $section) {
                    foreach ($section->questions as $question) {
                        $questionsComStyle[] = $question;
                    }
                }
                $html = str_replace($code, htmlspecialchars_decode(view('template-builders.fake.communication-style')->withQuestions($questionsComStyle)->render()), $html);
            }
        }

        $htmlDOM = str_get_html($html);

        foreach ($htmlDOM->find('code') as $key => $code) {
            if ($code->innertext() == '[new-page]') {
                $target = $code->parent()->parent();
                $className = $target->getAttribute('class') . " force-float";

                $target->setAttribute('class', $className);
                $code->setAttribute('data-page', $key + 2);
            }
        }

        $html = $htmlDOM;

        // [starhub-johari] per attribute
        foreach ($htmlDOM->find('.starhub-johari') as $code) {
            $html = str_replace($code, htmlspecialchars_decode(view('template-builders.fake.starhub-johari')->render()), $html);
        }

        // render report formulas
        foreach($htmlDOM->find('.replace-formula code') as $wrapper) {
            $html = str_replace($code, htmlspecialchars_decode(view('template-builders.fake.formula')->render()), $html);
        }

        // the end of report formulas
        foreach ($htmlDOM->find('code') as $code) {
            $text = explode('-', $code->innertext());
            if ($code->innertext() != "[communication-style]") {

                $question = Question::find($text[1]);

                if ($question) {
                    if ($text[0] == "answer") {
                        $html = str_replace($code, htmlspecialchars_decode(view('template-builders.fake.answer')->render()), $html);

                    } else if ($text[0] == "percentage") {
                        $html = str_replace($code, htmlspecialchars_decode(view('template-builders.fake.percentage')->render()), $html);

                    } else if ($text[0] == "johari") {
                        $html = str_replace($code, htmlspecialchars_decode(view('template-builders.fake.johari')->render()), $html);

                    }
                }
            }
        }

        foreach ($htmlDOM->find('code') as $code) {
            if ($code->innertext() == '[response-table]' || $code->innertext() == '[response-tquestion]') {
                $html = str_replace($code, htmlspecialchars_decode(view('template-builders.fake.table')->render()), $html);
            }
        }

        foreach ($htmlDOM->find('.editable') as $editable) {
            if ($editable->getAttribute('data-selector') != "title") {
                $html = str_replace($editable, htmlspecialchars_decode('<div class="content-text">' . $editable->innertext() . '</div>'), $html);
            }
        }

        $html = str_replace('[change-with-element]', $html, $base);

        foreach ($htmlDOM->find('code') as $key => $code) {
            if ($code->innertext() == '[new-page]') {
                $html = str_replace($code, $footer, $html);
                $html = str_replace('[page-number]', $code->getAttribute('data-page'), $html);
            }
        }
        // $html = str_replace('<code>[new-page]</code>', $footer, $html);

        $html = str_replace('[report-header]', $request->header_text, $html);
        $html = str_replace('[report-header-align]', $request->header_alignment, $html);
        $html = str_replace('[report-footer]', $request->footer_text, $html);
        $html = str_replace('[report-footer-align]', $request->footer_alignment, $html);

        $html = str_replace('[report-background]', $request->cover_background, $html);

        $html = str_replace('<button type="button" class="close removeElement">×</button>', '', $html);
        // $html = str_replace('[product-purchaser]', ($buyer->full_name()), $html);
        $html = str_replace('[survey-title]', $survey->title, $html);
        $html = str_replace('[survey-type]', $survey->profiler_type, $html);
        $html = str_replace('[survey-batch_reference]', $survey->batch_reference, $html);
        $html = str_replace('[survey-description]', nl2br($survey->description), $html);
        $html = str_replace('[survey-period_start-simple]', date('d-m-Y'), $html);
        $html = str_replace('[survey-period_start-style]', date('d ') . '<span class="month">' . date('F') . '</span>' . date('Y'), $html);
        $html = str_replace('[survey-period_start-flat]', date('F d,Y'), $html);
        // return $html;

        $folder = date('ymd');
        $baseUrl = public_path()."/report/";
        exec("mkdir ".$baseUrl);
        file_put_contents($baseUrl.'/preview-'.$folder.'.html', $html);

        // new
        exec("prince " . $baseUrl . "/preview-".$folder.".html -o " . $baseUrl . "/preview-".$folder.".pdf --pdf-title='" . $survey->title . "' --page-margin=0 -s " . public_path('/css/report-template.css'));

        $filename = "report/preview-".$folder.".pdf";
        
        $app_url=(ENV('APP_URL'))?ENV('APP_URL'):'https://store.learn1thing.com/profiler';

        return  $app_url.'/'.$filename;

        $path = public_path($filename);
        
        return Response::make(file_get_contents($path), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$filename.'"'
        ]);
    }
}
