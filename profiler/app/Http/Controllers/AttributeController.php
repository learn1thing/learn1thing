<?php

namespace App\Http\Controllers;

use App\Models\Attribute;
use Illuminate\Http\Request;
use App\Http\Requests\AttributeRequest;
use App\DataTables\AttributeDataTable;

class AttributeController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param App\DataTables\AttributeDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index($token, AttributeDataTable $dataTable)
    {
        return $dataTable->render('attributes.index');
    }

    /**
     * Show the form for creating new resource.
     *
     * @param  \App\Models\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function create($token, Attribute $attribute)
    {
        return $this->renderForm($attribute, false);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($token, AttributeRequest $request)
    {
        $attribute = Attribute::create($request->except('_token'));
        return $this->actionRedirect('create', $attribute->name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function edit($token, Attribute $attribute)
    {
        return $this->renderForm($attribute, true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function update($token, AttributeRequest $request, Attribute $attribute)
    {
        $attribute->update($request->except('_token', '_method'));
        return $this->actionRedirect('update', $attribute->name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function destroy($token, Attribute $attribute)
    {
        $name = $attribute->name;
        $attribute->delete();
        return $this->actionRedirect('delete', $name);
    }

    /**
     * Handle store and update.
     *
     * @param  \App\Models\Attribute  $attribute
     * @param  boolean  $type
     * @return \Illuminate\Http\Response
     */
    private function renderForm($attribute, $type)
    {
        return response()->json(
            view('attributes._form', $this->prepareData([
                'attribute' => $attribute
            ], ['form-horizontal'], $type))->render()
        );
    }

    /**
     * Handle store and update.
     *
     * @param  string  $method
     * @param  string  $name
     * @return \Illuminate\Http\Response
     */
    private function actionRedirect($mehtod, $name)
    {
        return redirect(\RouteHelper::token('attributes.index'))->withSuccess(
            trans("messages.success.$mehtod", [
                'name' => sprintf('attribute %s', strtolower($name))
            ])
        );
    }
}
