<?php

namespace App\Http\Controllers\Section;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\SectionDataTable;
use App\Models\Section;
use Validator;

class SectionController extends Controller
{
    public function index(SectionDataTable $dataTable) {
        $items = Section::orderBy('id','DESC')->paginate(4);
        return view('section.index',compact('items'));
    }

    public function create() {
        $items = Section::all();
    	return view('section.create' ,compact('items'));
    }

    public function store(Request $request) {
    	$validate = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required|max:100:',
        ]);
        if ($validate->fails()) {
            return redirect()->route('section.create')->withErrors($validate);
        } else {
            try {
                $section = new Section;
                $section->title = $request->input('title');
                $section->description = $request->input('description');
                $section->survey_id = 1;
                $section->save();   
            } catch (Exception $e) {
                return redirect()->route('section.index');
            }
            return redirect()->route('section.index')->with('success','Section created successfully');;
        }
    }
}
