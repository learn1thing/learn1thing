<?php

namespace App\Http\Controllers;

use App\Models\Batch;
use Illuminate\Http\Request;
use App\Http\Requests\BatchRequest;
use App\DataTables\BatchDataTable;
use App\Models\Survey;
use App\Models\Observer;
use DB;

class BatchController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param App\DataTables\BatchDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index($token, BatchDataTable $dataTable)
    {
        return $dataTable->render('batches.index');
    }

    /**
     * Show the form for creating new resource.
     *
     * @param  \App\Models\Batch  $batch
     * @return \Illuminate\Http\Response
     */
    public function create($token, Batch $batch)
    {
        return $this->renderForm($batch, false);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($token, BatchRequest $request)
    {
        $batch = Batch::create($request->except('_token'));
        return $this->actionRedirect('create', $batch->name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Batch  $batch
     * @return \Illuminate\Http\Response
     */
    public function edit($token, Batch $batch)
    {
        return $this->renderForm($batch, true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Batch  $batch
     * @return \Illuminate\Http\Response
     */
    public function update($token, BatchRequest $request, Batch $batch)
    {
        $batch->update($request->except('_token', '_method'));
        return $this->actionRedirect('update', $batch->name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Batch  $batch
     * @return \Illuminate\Http\Response
     */
    public function destroy($token, Batch $batch)
    {
        $name = $batch->name;
        $batch->delete();
        return $this->actionRedirect('delete', $name);
    }

    /**
     * Handle store and update.
     *
     * @param  \App\Models\Batch  $batch
     * @param  boolean  $type
     * @return \Illuminate\Http\Response
     */
    private function renderForm($batch, $type)
    {
        return response()->json(
            view('batches._form', $this->prepareData([
                'batch' => $batch
                , 'surveyData' => $this->getSurvey()
                , 'observerData' => $this->getObserver($batch->survey_id, $batch->observer_id)
            ], ['form-horizontal'], $type))->render()
        );
    }

    /**
     * Handle store and update.
     *
     * @param  string  $method
     * @param  string  $name
     * @return \Illuminate\Http\Response
     */
    private function actionRedirect($mehtod, $name)
    {
        return redirect(\RouteHelper::token('batches.index'))->withSuccess(
            trans("messages.success.$mehtod", [
                'name' => sprintf('batch %s', strtolower($name))
            ])
        );
    }

    /**
     * Get survey data.
     *
     * @return \Illuminate\Http\Response
     */
    private function getSurvey()
    {
        return Survey::pluck('title', 'id');
    }

    /**
     * Get observer data.
     *
     * @param  integer  $surveyId
     * @return \Illuminate\Http\Response
     */
    public function getObserver($surveyId = null, $observerId = null)
    {
        $observers = Observer::select('observers.id', DB::Raw('CONCAT(observers.first_name, " ", observers.last_name, " / ", observers.email) as full_name'))
            ->leftJoin('batches', 'batches.observer_id', '=', 'observers.id');

        if ($observerId) {
            $observers = $observers->where(function($query) use ($observerId) {
                $query->whereNull('batches.id')
                    ->orWhere('observers.id', '=', $observerId);
            });
        } else {
            $observers = $observers->whereNull('batches.id');
        }

        $survey = Survey::first();
        if ($surveyId) {
            $survey = Survey::find($surveyId);
        }

        $observers = $observers->whereIn('category_id', $survey->categories()->pluck('id'));
        return $observers->pluck('full_name', 'observers.id');
    }
}
