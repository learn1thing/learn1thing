<?php

namespace App\Http\Controllers;

use App\Models\CategoryProfile;
use App\Models\CategoryProfileDetail;
use Illuminate\Http\Request;
use App\DataTables\CategoryProfileDataTable;
use App\Http\Requests\StoreCategoryProfile;

class CategoryProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param App\DataTables\CategoryProfileDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index($token, CategoryProfileDataTable $dataTable)
    {
        return $dataTable->render('category-profile.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Models\CategoryProfile  $categoryProfile
     * @return \Illuminate\Http\Response
     */
    public function create($token, CategoryProfile $categoryProfile)
    {
        return view('category-profile.create', array('categories' => array()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($token, StoreCategoryProfile $request)
    {
        $categoryProfile = CategoryProfile::create(['name' => $request->name]);
        $arrCategory = [];
        foreach($request->categories as $category) {
            array_push($arrCategory, ['name' => $category]);
        }
        $categories = $categoryProfile->category_profile_details()->createMany($arrCategory);
        return $this->actionRedirect('create', $categoryProfile->name);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CategoryProfile  $categoryProfile
     * @return \Illuminate\Http\Response
     */
    public function show(CategoryProfile $categoryProfile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CategoryProfile  $categoryProfile
     * @return \Illuminate\Http\Response
     */
    public function edit($token, $id)
    {
        $categoryProfile = CategoryProfile::with('category_profile_details')
            ->find($id)
            ->toArray();
        $categoryProfile['category_profile_details'] = array_pluck($categoryProfile['category_profile_details'], 'name');
        return view('category-profile.edit', [
            'category_profile'     => $categoryProfile
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CategoryProfile  $categoryProfile
     * @return \Illuminate\Http\Response
     */
    public function update($token, StoreCategoryProfile $request, $id)
    {
        $categoryProfile = CategoryProfile::findOrFail($id);
        $categories = $categoryProfile->category_profile_details;
        $categoryProfileParams = $request->except('_token', '_method', 'categories');
        
        $deleteIds = [];
        $newCategories = [];
        // collect category ids that will delete
        foreach ($categories as $key => $value) {
            if(!in_array($value->name, $request->categories)) {
                array_push($deleteIds, $value->id);
            }
        }

        // collect new category that will add
        $tempCategories = array_pluck($categories, 'name');
        foreach ($request->categories as $key => $value) {
            if(!in_array($value, $tempCategories)) {
                array_push($newCategories, array('name' => $value));
            }
        }
        
        $categoryProfile->update($categoryProfileParams);

        if($deleteIds) {
            CategoryProfileDetail::destroy($deleteIds);
        }
        if($newCategories) {
            $categoryProfile->category_profile_details()->createMany($newCategories);
        }
        return $this->actionRedirect('update', $request->name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CategoryProfile  $categoryProfile
     * @return \Illuminate\Http\Response
     */
    public function destroy($token, $id)
    {
        $categoryProfile = CategoryProfile::find($id);
        $name = $categoryProfile->name;
        $categoryProfile->delete();
        return $this->actionRedirect('delete', $name);
    }

    /**
     * Handle store and update.
     *
     * @param  string  $method
     * @param  string  $name
     * @return \Illuminate\Http\Response
     */
    private function actionRedirect($mehtod, $name)
    {
        return redirect(\RouteHelper::token('category-profile.index'))->withSuccess(
            trans("messages.success.$mehtod", [
                'name' => sprintf('category profile %s', strtolower($name))
            ])
        );
    }
}
