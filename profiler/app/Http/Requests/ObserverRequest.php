<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ObserverRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'phone_number' => 'required|regex:/^(\+\d{1,2})?\(?\d{3}\)?\d{3}\d{4,7}$/',
            'email' => 'email',
            'last_name' => 'required',
            'first_name' => 'required',
            // 'category_id' => 'required|numeric|exists:categories,id',
        ];
    }
}
