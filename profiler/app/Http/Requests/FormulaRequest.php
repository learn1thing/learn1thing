<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class FormulaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:formulas,name,'.$this->segment(3),
            'survey_id'  => 'required|numeric|exists:surveys,id',
            'question_id' => 'required',
            'type' => 'required',
            'operand' => 'required',
        ];
    }
}
