<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SurveyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(
            $this->surveyRules(),
            $this->questionnaireRules(),
            $this->preliminaryRules(),
            $this->postRules()
        );
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function surveyRules()
    {
        return [
            'title'             => 'required',
            'batch_reference'   => 'required|min:3|max:10',
            'description'       => 'required',
            'period_start'      => 'required|date|before:period_end',
            'period_end'        => 'required|date|after:period_start',
            'type'              => 'required_with:value|in:minute,hour,day,week,month',
            'value'             => 'required_with:type',
            // 'category_count'    => 'required|numeric',
            'category_count'    => 'required_if:allow_set_observer,enable|numeric',
            'observer_count'    => 'required_if:allow_set_observer,enable|numeric',
            'email_builder_id'  => 'required|numeric|exists:email_builders,id',
            'random_question'   => 'required|in:yes,no',
            'allow_set_observer' => 'required|in:enable,disable'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function questionnaireRules()
    {
        return [
// <<<<<<< HEAD
            'questionnaire.*.title' => 'required',
            'questionnaire.*.description' => 'required',
            'questionnaire.*.automatic_numbering' => 'required|in:yes,no',
            'questionnaire.*.ordering_type' => 'required_if:questionnaire.*.automatic_numbering,yes|'.$this->orderingType(),
            'questionnaire.*.type' => 'required_if:questionnaire.*.automatic_numbering,yes|'.$this->type(),
            'questionnaire.*.question.*.question_number' => 'required_if:questionnaire.*.automatic_numbering,no|max:255',
            'questionnaire.*.question.*.content' => 'required',
            'questionnaire.*.question.*.answer_type' => 'required|'.$this->answerType(),
            'questionnaire.*.question.*.coordinate' => 'required_if:questionnaire.*.question.*.answer_type,checkboxes,multichoice|in:horizontal,vertical',
            'questionnaire.*.question.*.answer_configs.*' => 'required_if:questionnaire.*.question.*.answer_type,multichoice,checkboxes,boolean',
            'questionnaire.*.question.*.answer_configs.*.label' => 'required_if:questionnaire.*.question.*.answer_type,matching,likert_scale',
            'questionnaire.*.question.*.answer_configs.*.value' => 'required_if:questionnaire.*.question.*.answer_type,matching,likert_scale',
            'questionnaire.*.question.*.answer_configs.minimal' => 'required_if:questionnaire.*.question.*.answer_type,ranking',
            'questionnaire.*.question.*.answer_configs.interval' => 'required_if:questionnaire.*.question.*.answer_type,ranking',
            'questionnaire.*.question.*.answer_configs.maximal' => 'required_if:questionnaire.*.question.*.answer_type,ranking',

            'questionnaire.*.question.*.answer_configs.points' => 'required_if:questionnaire.*.question.*.answer_type,multiple_choice_with_constraint',

            'questionnaire.*.question.*.answer_configs.point' => 'required_if:questionnaire.*.question.*.answer_type,distributed_points',
            'questionnaire.*.question.*.answer_configs.options.*' => 'required_if:questionnaire.*.question.*.answer_type,distributed_points,multiple_choice_with_constraint'

            // 'questionnaire.*.question.*.is_required' => 'required',
// =======
//             'title' => 'required',
//             'description' => 'required',
//             'period_start' => 'required',
//             'period_end' => 'required'
// >>>>>>> dev-rangga
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function preliminaryRules()
    {
        return [
            'preliminary.*.title' => 'required',
            'preliminary.*.description' => 'required',
            'preliminary.*.automatic_numbering' => 'required|in:yes,no',
            'preliminary.*.ordering_type' => 'required_if:preliminary.*.automatic_numbering,yes|'.$this->orderingType(),
            'preliminary.*.type' => 'required_if:preliminary.*.automatic_numbering,yes|'.$this->type(),
            'preliminary.*.question.*.question_number' => 'required_if:preliminary.*.automatic_numbering,no|max:255',
            'preliminary.*.question.*.content' => 'required',
            'preliminary.*.question.*.answer_type' => 'required|'.$this->answerType(),
            'preliminary.*.question.*.coordinate' => 'required_if:preliminary.*.question.*.answer_type,checkboxes,multichoice|in:horizontal,vertical',
            'preliminary.*.question.*.answer_configs.*' => 'required_if:preliminary.*.question.*.answer_type,multichoice,checkboxes,boolean',
            'preliminary.*.question.*.answer_configs.*.label' => 'required_if:preliminary.*.question.*.answer_type,matching,likert_scale',
            'preliminary.*.question.*.answer_configs.*.value' => 'required_if:preliminary.*.question.*.answer_type,matching,likert_scale',
            'preliminary.*.question.*.answer_configs.minimal' => 'required_if:preliminary.*.question.*.answer_type,ranking',
            'preliminary.*.question.*.answer_configs.interval' => 'required_if:preliminary.*.question.*.answer_type,ranking',
            'preliminary.*.question.*.answer_configs.maximal' => 'required_if:preliminary.*.question.*.answer_type,ranking',

            'preliminary.*.question.*.answer_configs.points' => 'required_if:preliminary.*.question.*.answer_type,multiple_choice_with_constraint',

            'preliminary.*.question.*.answer_configs.point' => 'required_if:preliminary.*.question.*.answer_type,distributed_points',
            'preliminary.*.question.*.answer_configs.options.*' => 'required_if:preliminary.*.question.*.answer_type,distributed_points,multiple_choice_with_constraint'
        ];
        // return $this->globalRules('preliminary');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function postRules()
    {
        return [
            'post.*.title' => 'required',
            'post.*.description' => 'required',
            'post.*.automatic_numbering' => 'required|in:yes,no',
            'post.*.ordering_type' => 'required_if:post.*.automatic_numbering,yes|'.$this->orderingType(),
            'post.*.type' => 'required_if:post.*.automatic_numbering,yes|'.$this->type(),
            'post.*.question.*.question_number' => 'required_if:post.*.automatic_numbering,no|max:255',
            'post.*.question.*.content' => 'required',
            'post.*.question.*.answer_type' => 'required|'.$this->answerType(),
            'post.*.question.*.coordinate' => 'required_if:post.*.question.*.answer_type,checkboxes,multichoice|in:horizontal,vertical',
            'post.*.question.*.answer_configs.*' => 'required_if:post.*.question.*.answer_type,multichoice,checkboxes,boolean',
            'post.*.question.*.answer_configs.*.label' => 'required_if:post.*.question.*.answer_type,matching,likert_scale',
            'post.*.question.*.answer_configs.*.value' => 'required_if:post.*.question.*.answer_type,matching,likert_scale',
            'post.*.question.*.answer_configs.minimal' => 'required_if:post.*.question.*.answer_type,ranking',
            'post.*.question.*.answer_configs.interval' => 'required_if:post.*.question.*.answer_type,ranking',
            'post.*.question.*.answer_configs.maximal' => 'required_if:post.*.question.*.answer_type,ranking',

            'post.*.question.*.answer_configs.points' => 'required_if:post.*.question.*.answer_type,multiple_choice_with_constraint',

            'post.*.question.*.answer_configs.point' => 'required_if:post.*.question.*.answer_type,distributed_points',
            'post.*.question.*.answer_configs.options.*' => 'required_if:post.*.question.*.answer_type,distributed_points,multiple_choice_with_constraint'
        ];
        // return $this->globalRules('post');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    private function globalRules($type)
    {
        return [
            // "{$type}.*.title" => 'required',
            // "{$type}.*.description" => 'required',
            "{$type}.*.ordering_type" => $this->orderingType(),
            "{$type}.*.type" => $this->type(),
            // "{$type}.*.question.*.content" => 'required',
            "{$type}.*.question.*.answer_type" => $this->answerType(),
            // "{$type}.*.question.*.is_required" => 'required',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    private function orderingType()
    {
        return 'in:reset,continue';
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    private function type()
    {
        return 'in:arabic,alphabet,roman';
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    private function answerType()
    {
        return 'in:free_text,multichoice,checkboxes,matching,likert_scale,boolean,ranking,multiple_choice_with_constraint,date,time,datetime,distributed_points';
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        $attributes = [];

        foreach ($this->rules() as $attribute => $value) {
            $lang = str_replace(['post.*.', 'preliminary.*.', 'questionnaire.*.', 'question.*.', '_'], ' ', $attribute);
            $attributes[$attribute] = strtolower($lang);
        }

        return $attributes;
    }
}
