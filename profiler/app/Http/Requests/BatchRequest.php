<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BatchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'batch' => 'required|alpha_dash|max:10|unique:batches,batch,'.$this->segment(2),
            // 'survey_id' => 'required|numeric|exists:surveys,id',
            // 'observer_id' => 'required|numeric|exists:observers,id',
        ];
    }
}
