<?php

namespace App\Http\Requests\Api\v1;

class BuySurveyRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required',
            'survey_id' => 'required|max:10|exists:surveys,id',
            'user_id' => 'required|max:11',
            'model' => 'required',
            'email' => 'required|email|max:255',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255'
        ];
    }

    public function response(array $errors)
    {
        return response()->error($errors, 400);
    }
}
