<?php

namespace App\Http\Requests\Api\v1;

class StoreReportFormulaRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'survey_id' => 'required|exists:surveys,id',
            'formula' => 'required',
            // 'formula.*.name' => 'required',
            'formula.*.source' => 'required|in:formula,raw_data',
            'formula.*.question_id' => 'required_if:source,raw_data|exists:questions,id',
            'formula.*.formula_id' => 'required_if:source,formula|exists:report_formulas,id',
            'formula.*.answer' => 'required_if:source,raw_data',
            'formula.*.profiler_role' => 'in:all,user,observer',
            'formula.*.observer_category_id' => 'exists:category_profile_details,id',
            'formula.*.user_category' => 'in:all,department,individual',
            'formula.*.group_by' => 'required_if:user_category,department,individual',
            'formula.*.question_spec' => 'required_if:source,raw_data|in:value,answer_attributes,label,label_group,free_text',
            'formula.*.function' => 'required_if:source,raw_data|in:raw_value,sum,average,maximum,minimum,count'
        ];
    }

    public function response(array $errors)
    {
        return response()->error($errors, 400);
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        $attributes = [];

        foreach ($this->rules() as $attribute => $value) {
            $lang = str_replace(['formula.*.', '_'], ' ', $attribute);
            $attributes[$attribute] = strtolower($lang);
        }

        return $attributes;
    }
}
