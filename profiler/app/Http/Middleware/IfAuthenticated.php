<?php

namespace App\Http\Middleware;

use Closure;
use Client;

class IfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = Client::setEndpoint('profile')->setHeaders(['Authorization' => $request->authtoken])->get();
	\Log::info($response);

        if ($response['status']['code'] != 200) {
            return redirect(env('DASHBOARD_URL', 'http://beta.learn1thing.com/dashboard'));
            // abort($response['status']['code']);
        }

        session(['user' => $response['data']]);

        return $next($request);
    }
}
