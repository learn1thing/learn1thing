<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnswerHeader extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @author Candra Sudirman <o.candra18@gmail.com>
     * @var array
     */
    protected $fillable = [
        'survey_id'
        , 'observer_id'
        , 'start_time'
        , 'end_time'
        , 'is_invalid'
    ];

    protected $hidden = [
        'created_at'
        , 'updated_at'
    ];

    /**
     * Get survey relation
     */
    public function survey()
    {
        return $this->belongsTo('App\Models\Survey', 'survey_id');
    }

    /**
     * Get observer relation
     */
    public function observer()
    {
        return $this->belongsTo('App\Models\Observer', 'observer_id');
    }

    /**
     * Get observer relation
     */
    public function answerDetails()
    {
        return $this->hasMany('App\Models\AnswerDetail', 'answer_header_id');
    }
}
