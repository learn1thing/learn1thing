<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSurvey extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @author Candra Sudirman <o.candra18@gmail.com>
     * @var array
     */
    protected $fillable = [
        'user_id'
        , 'survey_id'
        , 'observer_id'
        , 'email'
        , 'position'
        , 'product_id'
        , 'model'
        , 'message_for_observer'
    ];

    protected $hidden = [
        'created_at'
        , 'updated_at'
    ];

    /**
     * Get survey relation
     */
    public function survey()
    {
        return $this->belongsTo('App\Models\Survey', 'survey_id');
    }

    /**
     * Get observer relation
     */
    public function observer()
    {
        return $this->belongsTo('App\Models\Observer', 'observer_id');
    }
}
