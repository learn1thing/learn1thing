<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Survey;

class Formula extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @author Candra Sudirman <o.candra18@gmail.com>
     * @var array
     */
    protected $fillable = [
		'name'
        , 'survey_id'
    ];

    protected $hidden = [
        'created_at'
        , 'updated_at'
    ];

    /**
     * Get survey relation
     */
    public function survey()
    {
        return $this->belongsTo('App\Models\Survey', 'survey_id');
    }

    /**
     * Get formula details relation relation
     */
    public function details()
    {
        return $this->hasMany('App\Models\FormulaDetail', 'formula_id');
    }

    /**
     * Get data tables data relation
     */
    public function scopeDatatables($query, $user_id) {
        $query = $query->select('formulas.id', 'formulas.name', 'surveys.title')
            ->leftJoin('surveys', 'formulas.survey_id', '=', 'surveys.id')
            ->where('surveys.creator_id', $user_id);

        return $query;
    }
}
