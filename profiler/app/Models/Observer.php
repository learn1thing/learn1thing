<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Observer extends Model
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @author Candra Sudirman <o.candra18@gmail.com>
     * @var array
     */
    protected $fillable = [
        'phone_number',
        'email',
        'last_name',
        'first_name',
        'category_id',
        'category_profile_detail_id'
    ];

    /**
     * Get answer header relation
     */
    public function answerHeader()
    {
        return $this->hasMany('App\Models\AnswerHeader', 'observer_id');
    }

    /**
     * Get answer header relation
     */
    public function answer()
    {
        return $this->hasOne('App\Models\AnswerHeader', 'observer_id');
    }

    /**
     * Get category relation
     */
    public function category() {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    /**
     * Get category profile relation
     */
    public function categoryProfile() {
        return $this->belongsTo('App\Models\CategoryProfileDetail', 'category_profile_detail_id');
    }

    /**
     * Get batch relation
     */
    public function batch() {
        return $this->hasOne('App\Models\Batch', 'observer_id');
    }

    /**
     * Get user survey header relation
     */
    public function userSurveys()
    {
        return $this->hasMany('App\Models\UserSurveys', 'observer_id');
    }

    /**
     * Get user survey header relation
     */
    public function userSurvey()
    {
        return $this->belongsTo('App\Models\UserSurvey', 'observer_id', 'id');
    }

    /**
     * Get user survey header relation
     */
    public function _user_survey()
    {
        return $this->belongsTo('App\Models\UserSurvey', 'id', 'observer_id');
    }

    public function full_name() {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function scopeDatatables($query) {
        return $query->with('category');
    }
}
