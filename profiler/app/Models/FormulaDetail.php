<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormulaDetail extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @author Candra Sudirman <o.candra18@gmail.com>
     * @var array
     */
    protected $fillable = [
        'name',
        'formula_id',
        'question_id',
        'type',
		'operand',
    ];

    protected $hidden = [
        'created_at'
        , 'updated_at'
    ];

    /**
     * Get formula relation
     */
    public function formula()
    {
        return $this->belongsTo('App\Models\Formula', 'formula_id');
    }
}
