<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'section_id'
        , 'question_number'
        , 'name'
        , 'content'
        , 'answer_type'
        , 'coordinate'
        , 'answer_configs'
        , 'answer_attributes'
        , 'answer_scores'
        , 'is_required'
    ];

    protected $hidden = [
        'created_at'
        , 'updated_at'
    ];

    protected $casts = [
        'is_required' => 'boolean'
    ];

    protected $typeOfArray = [
        'free_text', 'ranking', 'date', 'time', 'datetime'
    ];

    /**
     * Get section relation
     */
    public function section()
    {
        return $this->belongsTo(Section::class, 'section_id');
    }

    /**
     * Get answer detail relation
     */
    public function answerDetails()
    {
        return $this->belongsTo('App\Models\AnswerDetail', 'question_id');
    }

    public function setIsRequiredAttribute($value)
    {
        return $this->attributes['is_required'] = $value === 'on' ? true : false;
    }

    public function setContentAttribute($value)
    {
        return $this->attributes['content'] = htmlspecialchars($value);
    }

    public function setAnswerConfigsAttribute($value)
    {
        return $this->attributes['answer_configs'] = json_encode($value);
    }

    public function setAnswerAttributesAttribute($value)
    {
        return $this->attributes['answer_attributes'] = json_encode($value);
    }

    public function setAnswerScoresAttribute($value)
    {
        if (!in_array($this->attributes['answer_type'], $this->typeOfArray)) {
            $value = json_encode($value);
        }

        return $this->attributes['answer_scores'] = $value;
    }

    public function getAnswerConfigsAttribute($value)
    {
        return json_decode($value);
    }

    public function getContentAttribute($value)
    {
        return htmlspecialchars_decode($value);
    }

    public function getAnswerAttributesAttribute($value)
    {
        return json_decode($value);
    }

    public function getAnswerScoresAttribute($value)
    {
        if (!in_array($this->attributes['answer_type'], $this->typeOfArray)) {
            $value = json_decode($value);
        }

        return $value;
    }
}
