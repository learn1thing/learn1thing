<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportFormula extends Model
{
	protected $table = "report_formulas";
	/**
     * The attributes that are mass assignable.
     *
     * @author Rangga Lesmana <ranggapl@windowslive.com>
     * @var array
     */
    protected $fillable = [
			'name',
			'source',
			'survey_id',
            'question_id',
			'formula_id',
			'answer',
			'profiler_role',
			'observer_category_id',
            'reference_formula_id',
			'user_category',
			'group_by',
			'question_spec',
			'function',
            'operand',
            'is_parent'
    ];

    /**
     * Get survey relation
     */
    public function survey()
    {
        return $this->belongsTo('App\Models\Survey', 'survey_id');
    }

    /**
     * Get data tables data relation
     */
    public function scopeDatatables($query, $user_id) {
        $query = $query->select('report_formulas.id', 'report_formulas.name', 'surveys.title')
            ->leftJoin('surveys', 'report_formulas.survey_id', '=', 'surveys.id')
            ->where('surveys.creator_id', $user_id)
            ->where('is_parent', true);

        return $query;
    }
}
