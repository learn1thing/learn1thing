<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnswerDetail extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @author Candra Sudirman <o.candra18@gmail.com>
     * @var array
     */
    protected $fillable = [
        'answer_header_id'
        , 'question_id'
        , 'answer'
    ];

    protected $hidden = [
        'created_at'
        , 'updated_at'
    ];

    /**
     * Get header relation
     */
    public function answerHeader()
    {
        return $this->belongsTo('App\Models\AnswerHeader', 'answer_header_id');
    }

    /**
     * Get questions relation
     */
    public function question()
    {
        return $this->belongsTo('App\Models\Question', 'question_id');
    }
}
