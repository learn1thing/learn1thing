<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{

    protected $fillable = [
        'survey_id'
        , 'is_number_continued'
        , 'is_number_automated'
        , 'type'
        , 'question_type'
        , 'title'
        , 'description'
    ];

    protected $hidden = [
        'created_at'
        , 'updated_at'
    ];

    /**
     * Get survey relation
     */
    public function survey()
    {
        return $this->belongsTo('App\Models\Survey', 'survey_id');
    }

    /**
     * Get questions relation
     */
    public function questions()
    {
        return $this->hasMany('App\Models\Question', 'section_id');
    }

    /**
     * Set is_number_continued
     */
    public function setOrderingTypeAttribute($value)
    {
        return $this->attributes['is_number_continued'] = $value === 'continue' ? true : false;
    }
}
