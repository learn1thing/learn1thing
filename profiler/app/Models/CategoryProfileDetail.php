<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryProfileDetail extends Model
{
    protected $table = "category_profile_details";
    /**
     * The attributes that are mass assignable.
     *
     * @author Rangga Lesmana <ranggapl@windowslive.com>
     * @var array
     */
    protected $fillable = [
		'name'
    ];

    protected $hidden = [
    		'category_profile_detail_id',
        'created_at',
        'updated_at'
    ];

    /**
     * Get categoryProfile relation
     */
    public function categoryProfile()
    {
        return $this->belongsTo('App\Models\CategoryProfile', 'category_profile_id');
    }

    /**
     * Get observers relation
     */
    public function observers()
    {
        return $this->hasMany('App\Models\Observer', 'category_profile_detail_id');
    }
}
