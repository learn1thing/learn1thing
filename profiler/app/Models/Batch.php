<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{

    protected $fillable = [
        'batch'
        , 'is_done'
        , 'survey_id'
        , 'user_survey_id'
        , 'observer_id'
    ];

    protected $hidden = [
        'created_at'
        , 'updated_at'
    ];

    /**
     * Get survey relation
     */
    public function survey() {
        return $this->belongsTo('App\Models\Survey', 'survey_id');
    }

    /**
     * Get survey relation
     */
    public function userSurvey() {
        return $this->belongsTo('App\Models\UserSurvey', 'user_survey_id');
    }

    /**
     * Get observers relation
     */
    public function observer() {
        return $this->belongsTo('App\Models\Observer', 'observer_id');
    }

    public function scopeDatatables($query, $survey_id, $flag) {
        $baseQuery = $query
            ->select(['observers.first_name', 'observers.last_name', 'observers.email', 'surveys.title', 'batches.is_done', 'batches.batch', 'categories.name', 'categories.survey_id', 'batches.observer_id'])
            ->join('observers', 'observers.id', '=', 'batches.observer_id')
            ->join('categories', 'categories.id', '=', 'observers.category_id')
            ->join('surveys', 'surveys.id', '=', 'categories.survey_id')
            ->where('surveys.id', $survey_id);

        if ($flag == 'buyer') {
            return $baseQuery->where('categories.name', 'buyer');
        }

        return $baseQuery->whereNotIn('categories.name', ['owner', 'buyer']);
    }
}
