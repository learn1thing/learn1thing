<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryProfile extends Model
{
		protected $table = "category_profiles";
    /**
     * The attributes that are mass assignable.
     *
     * @author Rangga Lesmana <ranggapl@windowslive.com>
     * @var array
     */
    protected $fillable = [
		'name'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * Get category profile details relation
     */
    public function category_profile_details()
    {
        return $this->hasMany('App\Models\CategoryProfileDetail', 'category_profile_id');
    }

    /**
     * Get data tables data relation
     */
    public function scopeDatatables($query) {
        $query = $query->select('category_profiles.id', 'category_profiles.name')
        	->with('category_profile_details');

        return $query;
    }
}
