<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailBuilder extends Model
{
    protected $fillable = [
        'user_id'
        , 'name'
        , 'content'
        , 'report_name'
        , 'report_type'
        , 'header_text'
        , 'header_alignment'
        , 'footer_text'
        , 'footer_alignment'
        , 'cover_style'
        , 'cover_background'
        , 'data_source'
        , 'individual_report'

        //added by rdw 151217
        , 'logo_url'
        , 'report_desc'
    ];

    protected $hidden = [
        'created_at'
        , 'updated_at'
    ];

    /**
     * Get survey relation relation
     */
    public function surveys()
    {
        return $this->hasMany('App\Models\Survey', 'email_builder_id');
    }

    /**
     * Get survey relation relation
     */
    public function survey()
    {
        return $this->belongsTo('App\Models\Survey', 'data_source');
    }

    /**
     * Get data spesific user relation
     */
    public function scopeSpesificUser($query, $user_id) {
        return $query->where('user_id', $user_id);
    }

    public function scopeDatatables($query, $user_id, $survey_id) {
        $query = $query->select('email_builders.id', 'email_builders.name', 'email_builders.report_name', 'surveys.title', 'surveys.email_builder_id', 'email_builders.data_source')
            ->leftJoin('surveys', 'surveys.id', '=', 'email_builders.data_source');

        if ($user_id) {
            $query = $query->where('email_builders.user_id', $user_id);
        } else {
            $query = $query->where('email_builders.user_id', 0);
        }

        if ($survey_id) {
            $query = $query->where('email_builders.data_source', $survey_id);
        }

        return $query;
    }
}
