<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $fillable = [
        'creator_id'
        , 'category_profile_id'
        , 'email_builder_id'
        , 'title'
        , 'batch_reference'
        , 'description'
        , 'instruction'
        , 'profiler_type'
        , 'observer_description'
        , 'same_observer_desc'
        , 'fixed_cmp_reminder'
        , 'cmp_reminder_date'
        , 'thanks_message'
        , 'redirect_after_completion'
        , 'redirect_after_completion_url'
        , 'period_start'
        , 'period_end'
        , 'is_other_reviewed'
        , 'type'
        , 'value'
        , 'is_repeated'
        , 'is_random_questionnaire'
        , 'is_allow_set_observer'
        , 'category_count'
        , 'observer_count'
        , 'state'
    ];

    protected $hidden = [
        'created_at'
        , 'updated_at'
    ];

    protected $appends = [
        'url'
    ];

    protected $attributes = [
        'observer_description' => ''
    ];

    /**
     * Get attribute url survey
     */
    public function getUrlAttribute()
    {
        $code = strtr(base64_encode(openssl_encrypt($this->attributes['id'], 'AES-128-ECB', 'l1tprofiler')), '+/=', '-_,');

        return route('surveys.preview', [
            'type' => 'survey',
            'code' => $code
        ]);
    }

    /**
     * Get template relation
     */
    public function defaultReport() {
        return $this->belongsTo('App\Models\EmailBuilder', 'email_builder_id');
    }

    /**
     * Get template relation
     */
    public function reportTemplate() {
        return $this->hasMany('App\Models\EmailBuilder', 'data_source');
    }

    /**
     * Get answer header relation
     */
    public function answerHeader()
    {
        return $this->hasMany('App\Models\AnswerHeader', 'survey_id');
    }

    /**
     * Get observer owner
     */
    public function ownerObserver()
    {
        $category = $this->categories->where('name', 'owner')->first();
        return $category ? $category->observers : [];
    }

    /**
     * Get user servey relation
     */
    public function userSurveys()
    {
        return $this->hasMany('App\Models\UserSurvey', 'survey_id');
    }

    /**
     * Get sections relation
     */
    public function sections()
    {
        return $this->hasMany('App\Models\Section', 'survey_id');
    }

    /**
     * Get attributes relation
     */
    public function attributes()
    {
        return $this->hasMany('App\Models\Attribute', 'survey_id');
    }

    /**
     * Get categories relation
     */
    public function categories()
    {
        return $this->hasMany('App\Models\Category', 'survey_id');
    }

    public function savedCategories($userId)
    {
        return $this->categories()->whereNotIn('name', ['owner', 'buyer'])
            ->where('user_id', $userId);
    }

    /**
     * Get batches relation
     */
    public function batches()
    {
        return $this->hasMany('App\Models\Batch', 'survey_id');
    }

    /**
     * Get preliminaries relation
     */
    public function preliminaries()
    {
        return $this->sections->where('question_type', 'pre');
    }

    /**
     * Get questionnaires relation
     */
    public function questionnaires()
    {
        return $this->sections->where('question_type', 'question');
    }

    /**
     * Get postSurveys relation
     */
    public function postSurveys()
    {
        return $this->sections->where('question_type', 'post');
    }

    // /**
    //  * Get formulas relation
    //  */
    // public function formulas()
    // {
    //     return $this->hasMany('App\Models\Formula', 'survey_id');
    // }
    // 
    /**
     * Get formulas relation
     */
    public function formulas()
    {
        return $this->hasMany('App\Models\ReportFormula', 'survey_id');
    }


    /**
     * Return the query.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetSurveys($query, $request)
    {
        $data =  $query->where(function ($survey) use ($request) {
            return $this->nestedQuery($survey, $request);
        })->select('id', 'creator_id', 'title');

        if ($request->has('type') && $request->has('type') == 'full') {
            $data->addSelect($this->fillable)->with('sections.questions');
        }

        return $data;

    }

    /**
     * Return the query.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetPublishedSurveys($query, $request)
    {
        $data =  $query->where(function ($survey) use ($request) {
            return $this->nestedQuery($survey, $request);
        })
        ->whereState('published')
        ->select('id', 'creator_id', 'title');

        if ($request->has('type') && $request->has('type') == 'full') {
            $data->addSelect($this->fillable)->with('sections.questions');
        }

        return $data;

    }

    private function nestedQuery($survey, $request)
    {
        if ($request->has('title')) {
            $survey->where('title', 'like', "{$request->input('title')}");
        }

        if ($request->has('creator_id')) {
            $survey->where('creator_id', "{$request->input('creator_id')}");
        }

        return $survey;
    }

    /**
     * Get data tables data relation
     */
    // public function scopeDatatables($query, $user_id) {
    //     $query = $query->select('surveys.id', 'surveys.title', 'surveys.description', 'surveys.period_start', 'surveys.period_end', 'user_surveys.position')
    //         ->leftJoin('answer_headers', 'answer_headers.survey_id', 'surveys.id')
    //         ->leftJoin('user_surveys', 'user_surveys.survey_id', 'surveys.id')
    //         ->leftJoin('categories', function ($join) {
    //             $join->on('categories.survey_id', 'user_surveys.id')
    //                 ->where('categories.name', 'owner');
    //         })
    //         ->leftJoin('observers', function ($join) {
    //             $join->on('observers.category_id', 'categories.id')
    //                 ->on('answer_headers.observer_id', 'observers.id');
    //         });

    //     if ($user_id) {
    //         $query = $query->where('creator_id', $user_id);
    //     }

    //     return $query;
    // }
    public function scopeDatatables($query, $user_id) {
        $query = $query->select('surveys.id', 'surveys.title', 'surveys.description', 'surveys.state', 'surveys.period_start', 'surveys.period_end', 'surveys.email_builder_id', 'email_builders.name')
            ->leftJoin('email_builders', 'surveys.email_builder_id', '=', 'email_builders.id');

        if ($user_id) {
            $query = $query->where('surveys.creator_id', $user_id);
        } else {
            $query = $query->where('surveys.creator_id', 0);
        }

        return $query;
    }

    public function profileCategory()
    {
        return $this->belongsTo('App\Models\CategoryProfileDetail', 'category_profile_id');
    }

    /**
     * Get report formulas relation
     */
    public function reportFormulas()
    {
        return $this->hasMany('App\Models\ReportFormula', 'survey_id');
    }
}

