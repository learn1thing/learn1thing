<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Batch;
use App\Models\Survey;
use App\Notifications\FillPostSurvey;

class SendReminderPostSurvey implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $batch;
    public $survey;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Batch $batch, Survey $survey)
    {
        $this->batch = $batch;
        $this->survey = $survey;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $isDone = $this->batch->is_done;
        if(!$isDone) {
            $observer = $this->batch->observer;
            $observer->notify(new FillPostSurvey);
        }
    }
}
