<?php

namespace App\RestMiddleware\v1;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ClientException;

class Client
{
    /**
     * The GuzzleHttp Client instance.
     *
     * @var \GuzzleHttp\Client
     */
    protected $http;

    /**
     * The Endpoint instance.
     *
     * @var string
     */
    protected $endpoint;

    /**
     * The headers that will be sent when call the API.
     *
     * @var array
     */
    protected $headers = [];

    /**
     * The body that will be sent when call the API.
     *
     * @var array
     */
    protected $body = [];

    /**
     * The query that will be sent when call the API.
     *
     * @var array
     */
    protected $query = [];

     /**
     * Create a new Class instance.
     *
     * @param  \GuzzleHttp\Client  $http
     * @return void
     */
    public function __construct(HttpClient $http)
    {
        $this->http = $http;

        $this->headers = $this->headers();
    }

    /**
     * The headers that will be sent when call the API.
     *
     * @var array
     */
    public function headers()
    {
        return $this->headers = [
            'content-type' => 'application/json'
        ];
    }

    /**
     * The headers that will be sent when call the API.
     *
     * @var array
     */
    public function uri()
    {
        return config('restapi.uri').$this->endpoint;
    }

    /**
     * Set request endpoint.
     *
     * @param  \GuzzleHttp\Client  $http
     * @return App\RestMiddleware\Client
     */
    public function setEndpoint($endpoint = '')
    {
        $this->endpoint = $endpoint;

        return $this;
    }

    /**
     * Set header for request.
     *
     * @param  array  $headers
     * @return App\RestMiddleware\Client
     */
    public function setHeaders(array $headers)
    {
        $this->headers = array_merge($this->headers(), $headers);

        return $this;
    }

    /**
     * Set body for request.
     *
     * @param  array  $body
     * @return App\RestMiddleware\Client
     */
    public function setBody(array $body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Set body for request.
     *
     * @param  array  $query
     * @return App\RestMiddleware\Client
     */
    public function setQuery(array $query)
    {
        $this->query = http_build_query($query);

        return $this;
    }

    /**
     * Get request from middleware.
     *
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        try {
            $request  = $this->http->request('GET', $this->uri(), [
                'headers'  => $this->headers,
                'query'    => $this->query
            ]);
            $response = json_decode($request->getBody(), true);
        } catch (ClientException $e) {
            $body = $e->getResponse()->getBody();
            $response = json_decode($body->getContents(), true);
        }

        return $response;
    }

    /**
     * Post request to middleware.
     *
     * @return \Illuminate\Http\Response
     */
    public function post()
    {
        try {
            $request  = $this->http->request('POST', $this->uri(), [
                'headers'  => $this->headers,
                'query'    => $this->query,
                'json'     => $this->body
            ]);
            $response = json_decode($request->getBody(), true);
        } catch (ClientException $e) {
            $body = $e->getResponse()->getBody();
            $response = json_decode($body->getContents(), true);
        }

        return $response;
    }

    /**
     * Post request to middleware.
     *
     * @return \Illuminate\Http\Response
     */
    public function put()
    {
        try {
            $request  = $this->http->request('PUT', $this->uri(), [
                'headers'  => $this->headers,
                'query'    => $this->query,
                'json'     => $this->body
            ]);
            $response = json_decode($request->getBody(), true);
        } catch (ClientException $e) {
            $body = $e->getResponse()->getBody();
            $response = json_decode($body->getContents(), true);
        }

        return $response;
    }

    /**
     * Delete request to middleware.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleted()
    {
        try {
            $request  = $this->http->request('DELETE', $this->uri(), [
                'headers'  => $this->headers,
                'query'    => $this->query,
                'json'     => $this->body
            ]);
            $response = json_decode($request->getBody(), true);
        } catch (ClientException $e) {
            $body = $e->getResponse()->getBody();
            $response = json_decode($body->getContents(), true);
        }

        return $response;
    }
}