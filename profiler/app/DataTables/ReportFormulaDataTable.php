<?php

namespace App\DataTables;

use App\Models\ReportFormula;
use Yajra\Datatables\Services\DataTable;
use App\Helpers\RouteHelper;

class ReportFormulaDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function ($object) {
                $formula = $object->id;

                return view('layouts.action-form', [
                    'tableName' => 'formulas'
                    , 'editModal' => RouteHelper::token('formulas.edit', compact('formula'))
                    , 'delete' => RouteHelper::token('formulas.destroy', compact('formula'))
                ])->render();

            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        // $query = ReportFormula::query();
        $query = ReportFormula::datatables(isset($this->attributes['user_id']) ? $this->attributes['user_id'] : null);

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction([
                        'width' => '100px'
                    ])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'name' => [
                'title' => 'Formula Name'
            ],
            'title' => [
                'title' => 'Survey Title'
            ],
            // 'source',
            // 'question_name',
            // 'profiler_role',
            // 'observer_category',
            // 'user_category',
            // 'category_detail',
            // 'question_spec',
            // 'function',
            // 'operand',
            // 'formula_reference'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'formulas_' . time();
    }
}
