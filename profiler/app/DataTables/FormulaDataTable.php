<?php

namespace App\DataTables;

use App\Models\Formula;
use Yajra\Datatables\Services\DataTable;
use App\Helpers\RouteHelper;

class FormulaDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function ($object) {
                $formula = $object->id;

                return view('layouts.action-form', [
                    'tableName' => 'formula'
                    , 'edit' => RouteHelper::token('formulas.edit', compact('formula'))
                    , 'delete' => RouteHelper::token('formulas.destroy', compact('formula'))
                ])->render();

            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        // $query = Formula::query();
        $query = Formula::datatables(isset($this->attributes['user_id']) ? $this->attributes['user_id'] : null);

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction([
                        'width' => '100px'
                    ])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'name'
            , 'title' => [
                'title' => 'Survey Title'
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'attributes_' . time();
    }
}
