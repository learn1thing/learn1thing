<?php

namespace App\DataTables;

use App\Models\Survey;
use Yajra\Datatables\Services\DataTable;
use App\Helpers\RouteHelper;

class SurveyDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function ($object) {
                $survey = $object->id;
                $encryptedId = strtr(base64_encode(openssl_encrypt($object->id, 'AES-128-ECB', 'l1tprofiler')), '+/=', '-_,');

                $returnView = [
                    'tableName' => 'survey'
                    , 'edit' => RouteHelper::token('surveys.edit', compact('survey'))
                    , 'delete' => RouteHelper::token('surveys.destroy', compact('survey'))
                    , 'preview' => RouteHelper::check('surveys.preview', ['type' => 'survey', 'code' => $encryptedId])
                    , 'observer' => RouteHelper::token('surveys.batches', compact('encryptedId'))
                    , 'buyer' => RouteHelper::token('surveys.buyers', compact('encryptedId'))
                    , 'reportTemplate' => RouteHelper::token('surveys.report-template', compact('encryptedId'))
                    //addedd by rdw 081217
                    , 'duplicateData' => RouteHelper::token('surveys.duplicate', compact('survey'))
                    
                    , 'groupBy' => true
                ];

                // $returnView = view('layouts.action-form', [
                //     'tableName' => 'survey'
                //     // , 'edit' => RouteHelper::token('surveys.edit', compact('survey'))
                //     , 'delete' => RouteHelper::token('surveys.destroy', compact('survey'))
                //     , 'preview' => RouteHelper::check('surveys.preview', ['type' => 'survey', 'code' => $encryptedId])
                //     // , 'generate' => RouteHelper::check('surveys.print', ['userSurvey' => $encryptedId])
                //     , 'observer' => RouteHelper::token('surveys.batches', compact('encryptedId'))
                //     , 'buyer' => RouteHelper::token('surveys.buyers', compact('encryptedId'))
                //     // , 'retake' => RouteHelper::token('surveys.retake', compact('encryptedId'))
                //     // , 'reminder' => RouteHelper::token('surveys.reminder', compact('encryptedId'))
                //     , 'groupBy' => true
                // ])->render();

                if ($object->answerHeader->count() == 0) {
                    $returnView['edit'] = RouteHelper::token('surveys.edit', compact('survey'));
                }

                return view('layouts.action-form', $returnView)->render();
            })
            ->editColumn('position', function ($object) {
                switch ($object->position) {
                    case 'pre':
                        $return = 'Preliminary';
                        break;
                    case 'post':
                        $return = 'Post Survey';
                        break;
                    case 'question':
                        $return = 'Questionnaire';
                        break;

                    default:
                        $return = '-';
                        break;
                }
                return $return;
            })
            ->editColumn('description', function ($object) {
                return strlen($object->description) > 150 ? substr($object->description,0,150)."..." : $object->description;
            })
            ->editColumn('state', function ($object) {
                $status = null;
                if($object->state == "draft") {
                    $status = "Saved as draft";
                } else {
                    $status = "Published";
                }
                return $status;
            })
            ->editColumn('email_builder_id', function ($object) {
                return $object->email_builder_id == 0 ? '<span class="btn btn-danger" title="Report Not Set">Report Not Set</span>' : $object->name;
            })
            // ->editColumn('period_start', function ($object) {
            //     return date('d F Y', strtotime($object->period_start));
            // })
            // ->editColumn('period_end', function ($object) {
            //     return date('d F Y', strtotime($object->period_end));
            // })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Survey::datatables(isset($this->attributes['user_id']) ? $this->attributes['user_id'] : null);

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction([
                        'width' => '100px'
                    ])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id'
            , 'title'
            , 'description'
            , 'state'
            , 'email_builder_id' => [
                'title' => 'Report'
            ]
            // , 'period_start'
            // , 'period_end'
            // , 'position' => [
                // 'name' => 'answer_headers.position'
                // , 'data' => 'position'
                // , 'title' => trans('app.label.current_state')
            // ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'surveys_' . time();
    }
}
