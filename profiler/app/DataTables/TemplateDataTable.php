<?php

namespace App\DataTables;

use App\Models\EmailBuilder;
use Yajra\Datatables\Services\DataTable;
use App\Helpers\RouteHelper;
use Illuminate\Support\Facades\Log;

class TemplateDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {

        if ($this->attributes['survey_id']) {
            return $this->datatables
                ->eloquent($this->query())
                ->editColumn('email_builder_id', function($object) {
                    return $object->email_builder_id == $object->id ? 'Default' : '-';
                })
                ->addColumn('action', function ($object) {
                    $emailBuilder = $object->id;
                    $encryptedId = strtr(base64_encode(openssl_encrypt($object->id, 'AES-128-ECB', 'l1tprofiler')), '+/=', '-_,');
                    $returnView = [
                        'tableName' => 'email_builder'
                        , 'default' => RouteHelper::token('template-builder.default', compact('emailBuilder'))
                        , 'data' => $object
                    ];

                    return view('layouts.action-form', $returnView)->render();
                })
                ->make(true);

        } else {
            return $this->datatables
                ->eloquent($this->query())
                ->editColumn('email_builder_id', function($object) {
                    return $object->email_builder_id == $object->id ? 'Default' : '-';
                })
                ->addColumn('action', function ($object) {
                    $emailBuilder = $object->id;
                    $encryptedId = strtr(base64_encode(openssl_encrypt($object->id, 'AES-128-ECB', 'l1tprofiler')), '+/=', '-_,');

                    $returnView = [
                        'tableName' => 'email_builder'
                        , 'edit' => RouteHelper::token('template-builder.edit', compact('emailBuilder'))
                        , 'delete' => RouteHelper::token('template-builder.destroy', compact('emailBuilder'))
                    ];

                    $survey = $object->survey;
                    if ($survey) {
                        $observer = $survey->ownerObserver()->first();

                        if(is_object($survey)&&is_object($observer)&&is_object($object)){
                            
                            $encryptIds = '0+' . $survey->id . '+' . $survey->creator_id . '+' . $observer->id . '+' . $object->id;
                            $batch = strtr(base64_encode(openssl_encrypt($encryptIds, 'AES-128-ECB', 'l1tprofiler')), '+/=', '-_,');

                            $returnView['generate'] = RouteHelper::token('surveys.report', ['batch' => $batch]);
                            Log::info($returnView['generate']);
                        }

                    }
                    $returnView['duplicate'] = RouteHelper::token('surveys.duplicatereport', compact('emailBuilder'));
                    return view('layouts.action-form', $returnView)->render();
                })
                ->make(true);

        }
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = EmailBuilder::datatables(
            isset($this->attributes['user_id']) ? $this->attributes['user_id'] : null
            , isset($this->attributes['survey_id']) ? $this->attributes['survey_id'] : null
        );

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        $encryptedId = strtr(base64_encode(openssl_encrypt($this->attributes['survey_id'], 'AES-128-ECB', 'l1tprofiler')), '+/=', '-_,');

        $url = RouteHelper::token('surveys.report-template', compact('encryptedId'));

        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax($url)
                    ->addAction(['width' => '150px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => [ 'title' => 'ID' ],
            'name' => [ 'title' => 'Name' ],
            'report_name' => [ 'title' => 'Code' ],
            'title' => [
                'title' => 'Survey Title'
                , 'name' => 'surveys.title'
            ],
            'email_builder_id' => [
                'title' => 'Status'
                , 'name' => 'surveys.email_builder_id'
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'templatedatatables_' . time();
    }
}
