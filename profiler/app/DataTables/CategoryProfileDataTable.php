<?php

namespace App\DataTables;

use App\Models\CategoryProfile;
use Yajra\Datatables\Services\DataTable;
use App\Helpers\RouteHelper;

class CategoryProfileDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function ($object) {
                $categoryProfile = $object->id;

                return view('layouts.action-form', [
                    'tableName' => 'category_profiles'
                    , 'edit' => RouteHelper::token('category-profile.edit', compact('categoryProfile'))
                    , 'delete' => RouteHelper::token('category-profile.destroy', ['categoryProfile' => $categoryProfile])
                ])->render();

            })
            ->editColumn('categories', function (CategoryProfile $categoryProfile) 
            {
                $categorieNames = array_pluck($categoryProfile->category_profile_details, 'name');
                return implode(', ', $categorieNames);
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        // $query = CategoryProfile::query();
        $query = CategoryProfile::datatables();

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction([
                        'width' => '100px'
                    ])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'name',
            'categories' => ['orderable' => false, 'searchable' => false]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'attributes_' . time();
    }
}
