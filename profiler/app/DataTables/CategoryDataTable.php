<?php

namespace App\DataTables;

use App\Models\Category;
use Yajra\Datatables\Services\DataTable;
use App\Helpers\RouteHelper;

class CategoryDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function ($object) {
                $category = $object->id;

                return view('layouts.action-form', [
                    'tableName' => 'categories',
                    'editModal' => RouteHelper::token('categories.edit', compact('category')),
                    'delete' => RouteHelper::token('categories.destroy', compact('category')),
                ])->render();

            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Category::datatables();

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction([
                        'width' => '100px'
                    ])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'name' => [
                'title' => trans('app.category.category_name')
            ]
            , 'survey.title' => [
                'name' => 'survey.title'
                , 'data' => 'survey.title'
                , 'title' => trans('app.category.title')
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'categories_' . time();
    }
}
