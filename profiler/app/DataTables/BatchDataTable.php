<?php

namespace App\DataTables;

use App\Models\Batch;
use Yajra\Datatables\Services\DataTable;
use App\Helpers\RouteHelper;

class BatchDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function ($object) {
                $batch = $object->id;
                $encryptedId = strtr(base64_encode(openssl_encrypt($object->id, 'AES-128-ECB', 'l1tprofiler')), '+/=', '-_,');

                $data = [
                    'tableName' => 'batches'
                ];

                if ($object->name == 'buyer') {
                    if ($object->is_done) {
                        $data['retake'] = RouteHelper::token('surveys.retake', ['surveyId' => $object->survey_id, 'observerId' => $object->observer_id]);
                    } else {
                        $data['preview'] = RouteHelper::token('surveys.preview', ['type' => 'customer', 'code' => $object->batch]);
                    }
                    $data['generate'] = RouteHelper::token('surveys.report', ['batch' => $object->batch]);
                } else {
                    $data['preview'] = RouteHelper::token('surveys.preview', ['type' => 'batch', 'code' => $object->batch]);
                }

                return view('layouts.action-form', $data)->render();

            })
            ->addColumn('status', function ($object) {
                return $object->is_done ? 'Complete' : 'Pending';
            })
            ->addColumn('full_name', function ($object) {
                return $object->first_name . ' ' . $object->last_name . ' / ' . $object->email;
            })

            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        // \Log::info($this->attributes['flag']);
        $query = Batch::datatables($this->attributes['survey_id'], $this->attributes['flag']);

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        $encryptedId = strtr(base64_encode(openssl_encrypt($this->attributes['survey_id'], 'AES-128-ECB', 'l1tprofiler')), '+/=', '-_,');

        $url = RouteHelper::token('surveys.batches', compact('encryptedId'));

        if ($this->attributes['flag'] == 'buyer') {
            $url = RouteHelper::token('surveys.buyers', compact('encryptedId'));
        }

        return $this->builder()
            ->columns($this->getColumns())
            ->ajax($url)
            ->addAction([
                'width' => '100px'
            ])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'observers.email' => [
                'name' => 'observers.email'
                , 'data' => 'full_name'
                , 'title' => trans('app.label.email')
            ]
            , 'surveys.title' => [
                'name' => 'surveys.title'
                , 'data' => 'title'
                , 'title' => trans('app.label.title')
            ]
            , 'batches.is_done' => [
                'data' => 'status'
                , 'title' => trans('app.label.status')
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'batches_' . time();
    }
}
