<?php

namespace App\DataTables;

use App\Models\Observer;
use Yajra\Datatables\Services\DataTable;
use App\Helpers\RouteHelper;

class ObserverDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function($object){
                $observer = $object->id;

                return view('layouts.action-form', [
                    'tableName' => 'observers'
                    , 'editModal' => RouteHelper::token('observers.edit', compact('observer'))
                    , 'delete' => RouteHelper::token('observers.destroy', compact('observer'))
                ])->render();
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Observer::datatables();

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction([
                        'width' => '100px'
                    ])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'email' => [
                'title' => trans('app.label.email')
            ]
            , 'last_name' => [
                'title' => trans('app.label.last_name')
            ]
            , 'first_name' => [
                'title' => trans('app.label.first_name')
            ]
            , 'phone_number' => [
                'title' => trans('app.label.phone_number')
            ]
            , 'category.name' => [
                'name' => 'category.name'
                , 'data' => 'category.name'
                , 'title' => trans('app.label.category_name')
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'observers_' . time();
    }
}
