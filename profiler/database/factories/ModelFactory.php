<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Survey::class, function (Faker\Generator $faker) {
	return [
		'user_id' => $faker->randomDigit,
		'title' => $faker->title,
		'description' => $faker->text,
		'period_start' => $faker->date,
		'period_end' => $faker->date,
	];
});

$factory->define(App\Models\Question::class, function (Faker\Generator $faker) {
	$answerType = ['multichoice', 'matching', 'likert_scale', 'boolean', 'ranking', 'free_text', 'multichoice_with_constraint', 'date', 'time', 'datetime', 'distributed_point'];

	return [
		'title' => $faker->title,
		'answer_type' => $faker->randomElement($answerType),
		'answer_configs' => $faker->text,
		'is_required' => $faker->randomElement([true, false]),
	];
});

$factory->define(App\Models\AnswerHeader::class, function (Faker\Generator $faker) {
	return [
		'user_id' => $faker->randomDigit,
		'start_time' => $faker->date,
		'end_time' => $faker->date,
	];
});

$factory->define(App\Models\AnswerDetail::class, function (Faker\Generator $faker) {
	return [
		'answer' => $faker->text,
	];
});

$factory->define(App\Models\Attribute::class, function (Faker\Generator $faker) {
	return [
		'name' => $faker->name,
	];
});
