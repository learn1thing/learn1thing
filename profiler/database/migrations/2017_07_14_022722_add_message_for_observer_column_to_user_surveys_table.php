<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMessageForObserverColumnToUserSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_surveys', function (Blueprint $table) {
            $table->text('message_for_observer')
                ->nullable()
                ->after('position');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_surveys', function (Blueprint $table) {
            $table->dropColumn([
                'message_for_observer'
            ]);
        });
    }
}
