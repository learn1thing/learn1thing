<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReferenceIdAndOperandColumnsToReportFormulas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('report_formulas', function (Blueprint $table) {
            $table->dropColumn([
                'answer',
                'profiler_role',
                'question_spec',
                'function'
            ]);
        });
        Schema::table('report_formulas', function (Blueprint $table) {
            $table->enum('profiler_role', ['all', 'observer', 'user'])->nullable()->after('source');
            $table->enum('question_spec', ['value', 'answer_attributes', 'label', 'label_group', 'free_text'])
                ->nullable()
                ->after('group_by');
            $table->enum('function', ['raw_value', 'sum', 'average'])->nullable()->after('question_spec');
            $table->integer('reference_formula_id')->nullable()->after('question_id');
            $table->string('operand', 3)->nullable()->after('function');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('report_formulas', function (Blueprint $table) {
            $table->dropColumn([
                'reference_formula_id',
                'operand'
            ]);
            $table->text('answer')->nullable();
        });
    }
}
