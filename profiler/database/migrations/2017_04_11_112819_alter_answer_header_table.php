<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAnswerHeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('answer_headers', function (Blueprint $table) {
            $table->enum('position', ['question', 'pre', 'post'])->default('pre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('answer_headers', function (Blueprint $table) {
            $table->dropColumn(['position']);
        });
    }
}
