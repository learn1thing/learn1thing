<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PivotSurveyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('surveys', function (Blueprint $table) {
            $table->dropColumn(['email']);
        });

        Schema::table('answer_headers', function (Blueprint $table) {
            $table->dropColumn(['position']);
        });

        Schema::create('user_surveys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('survey_id')->unsigned();
            $table->integer('user_id')->nullable();
            $table->string('email')->nullable();
            $table->enum('position', ['question', 'pre', 'post'])->default('pre');
            $table->timestamps();

            $table->foreign('survey_id')
                ->references('id')->on('surveys')
                ->onDelete('cascade');
        });

        Schema::table('batches', function (Blueprint $table) {
            $table->foreign('survey_id')
                ->references('id')->on('surveys')
                ->onDelete('cascade');
            $table->foreign('observer_id')
                ->references('id')->on('observers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surveys', function (Blueprint $table) {
            $table->string('email')->after('value')->nullable();
        });

        Schema::table('answer_headers', function (Blueprint $table) {
            $table->enum('position', ['question', 'pre', 'post'])->after('is_invalid')->default('pre');
        });

        Schema::dropIfExists('user_surveys');

        Schema::table('batches', function (Blueprint $table) {
            $table->dropForeign(['survey_id']);
            $table->dropForeign(['observer_id']);
        });
    }
}
