<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetNullTypeAndValueColumnsOnSurveyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surveys', function (Blueprint $table) {
            $table->dropColumn(['type', 'value']);
        });
        Schema::table('surveys', function (Blueprint $table) {
            $table->enum('type', ['hour', 'day', 'week', 'month'])->nullable()->after('is_repeated');
            $table->integer('value')->nullable()->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surveys', function (Blueprint $table) {
            $table->dropColumn(['type', 'value']);
        });
        Schema::table('surveys', function (Blueprint $table) {
            $table->enum('type', ['hour', 'day', 'week', 'month'])->after('is_other_reviewed');
            $table->integer('value')->after('type');
        });
    }
}
