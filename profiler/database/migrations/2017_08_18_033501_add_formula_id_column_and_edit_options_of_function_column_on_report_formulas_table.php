<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFormulaIdColumnAndEditOptionsOfFunctionColumnOnReportFormulasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('report_formulas', function (Blueprint $table) {
            $table->dropColumn([
                'function'
            ]);
        });
        Schema::table('report_formulas', function (Blueprint $table) {
            $table->integer('formula_id')->nullable()->after('question_id');
            $table->enum('function', ['average', 'count', 'maximum', 'minimum', 'raw_value', 'sum'])
                ->nullable()
                ->default('raw_value')
                ->after('question_spec');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('report_formulas', function (Blueprint $table) {
            $table->dropColumn([
                'function',
                'formula_id'
            ]);
        });
        Schema::table('report_formulas', function (Blueprint $table) {
            $table->enum('function', ['raw_value', 'sum', 'average'])
                ->nullable()
                ->after('question_spec');
        });
    }
}
