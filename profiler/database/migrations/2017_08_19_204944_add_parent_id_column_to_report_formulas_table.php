<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParentIdColumnToReportFormulasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('report_formulas', function (Blueprint $table) {
            $table->boolean('is_parent')->default(false)->after('operand');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('report_formulas', function (Blueprint $table) {
            $table->dropColumn([
                'is_parent'
            ]);
        });
    }
}
