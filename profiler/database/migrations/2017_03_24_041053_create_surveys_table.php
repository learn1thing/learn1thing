<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('creator_id');
            $table->string('title');
            $table->text('description');
            $table->date('period_start')->nullable();
            $table->date('period_end')->nullable();
            $table->boolean('is_other_reviewed')->default(false);

            $table->enum('type', ['hour', 'day', 'week', 'month']);
            $table->integer('value');
            $table->boolean('is_repeated')->default(false);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surveys');
    }
}
