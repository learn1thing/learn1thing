<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryProfileDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_profile_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_profile_id')->unsigned();
            $table->string('name');
            $table->timestamps();

            $table->foreign('category_profile_id')
                ->references('id')->on('category_profiles')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_profile_details');
    }
}
