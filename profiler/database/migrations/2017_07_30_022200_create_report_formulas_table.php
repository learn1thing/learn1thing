<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportFormulasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_formulas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('survey_id')->unsigned()->nullable();
            $table->integer('question_id')->unsigned()->nullable();
            $table->text('answer')->nullable();
            $table->string('name');
            $table->enum('source', ['formula', 'raw_data']);
            $table->enum('profiler_role', ['all', 'observer', 'user'])->default('all');
            $table->integer('observer_category_id')->unsigned()->nullable();
            $table->enum('user_category', ['all', 'department', 'individual'])->nullable();
            $table->integer('group_by')->nullable();
            $table->enum('question_spec', ['value', 'answer_attributes', 'label', 'label_group'])->default('value');
            $table->enum('function', ['raw_value', 'sum', 'average'])->default('raw_value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_formulas');
    }
}
