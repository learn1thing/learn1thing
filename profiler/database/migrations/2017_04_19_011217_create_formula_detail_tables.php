<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormulaDetailTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formula_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('formula_id')->default(0);
            $table->integer('question_id')->default(0);
            $table->string('type')->default('');
            $table->string('operand')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formula_details');
    }
}
