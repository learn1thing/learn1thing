<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sections', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('survey_id')->unsigned();
            $table->string('title');
            $table->text('description')->nullable();
            $table->enum('question_type', ['question', 'pre', 'post'])->default('question');
            $table->enum('type', ['arabic', 'alphabet', 'roman'])->default('arabic');
            $table->boolean('is_number_automated')->default(true);
            $table->boolean('is_number_continued')->default(true);
            $table->timestamps();

            $table->foreign('survey_id')
                ->references('id')->on('surveys')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sections');
    }
}
