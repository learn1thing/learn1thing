<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddObserverToUserSurveys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('user_surveys', function (Blueprint $table) {
            $table->integer('observer_id')->after('survey_id')->unsigned();
            $table->foreign('observer_id')
                ->references('id')->on('observers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_surveys', function (Blueprint $table) {
            $table->dropForeign(['observer_id']);
            $table->dropColumn(['observer_id']);
        });
    }
}
