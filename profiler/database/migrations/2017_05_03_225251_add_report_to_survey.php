<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReportToSurvey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('surveys', function (Blueprint $table) {
            $table->integer('email_builder_id')->after('creator_id')->unsigned();
            $table->foreign('email_builder_id')
                ->references('id')->on('email_builders')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surveys', function (Blueprint $table) {
            $table->dropForeign(['email_builder_id']);
            $table->dropColumn(['email_builder_id']);
        });
    }
}
