<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryProfileIdColumnToObserverTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('observers', function (Blueprint $table) {
            $table->integer('user_survey_id')
                ->unsigned()
                ->nullable()
                ->after('category_id');
            $table->integer('category_profile_detail_id')
                ->unsigned()
                ->nullable()
                ->after('user_survey_id');

            $table->foreign('user_survey_id')
                ->references('id')->on('user_surveys')
                ->onDelete('cascade');
            $table->foreign('category_profile_detail_id')
                ->references('id')->on('category_profile_details')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('observers', function (Blueprint $table) {
            $table->dropForeign(['user_survey_id']);
            $table->dropForeign(['category_profile_detail_id']);
            $table->dropColumn([
                'user_survey_id',
                'category_profile_detail_id'
            ]);
        });
    }
}
