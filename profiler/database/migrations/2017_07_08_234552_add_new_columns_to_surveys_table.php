<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsToSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surveys', function (Blueprint $table) {
            $table->integer('category_profile_id')
                ->unsigned()
                ->nullable()
                ->after('creator_id');
            $table->text('instruction')->after('description');
            $table->enum('profiler_type', ['self-profiler', '360-profiler'])
                ->default('self-profiler')
                ->after('instruction');
            $table->tinyInteger('same_observer_desc')->default(0)->after('observer_description');
            $table->tinyInteger('fixed_cmp_reminder')->default(0)->after('same_observer_desc');
            $table->datetime('cmp_reminder_date')->nullable()->after('fixed_cmp_reminder');
            $table->text('thanks_message')->after('cmp_reminder_date');
            $table->tinyInteger('redirect_after_completion')->default(0)->after('thanks_message');
            $table->string('redirect_after_completion_url')->nullable()->after('redirect_after_completion');
            $table->enum('state', ['draft', 'published'])
                ->default('draft')
                ->after('redirect_after_completion');

            $table->foreign('category_profile_id')
                ->references('id')->on('category_profiles')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surveys', function (Blueprint $table) {
            $table->dropForeign(['category_profile_id']);
            $table->dropColumn([
                'category_profile_id',
                'instruction', 
                'profiler_type', 
                'same_observer_desc',
                'fixed_cmp_reminder',
                'cmp_reminder_date',
                'thanks_message',
                'redirect_after_completion',
                'redirect_after_completion_url',
                'state'
            ]);
        });
    }
}
