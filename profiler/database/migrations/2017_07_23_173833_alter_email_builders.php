<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEmailBuilders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('email_builders', function (Blueprint $table) {
            $table->string('report_name')->default('def_name')->nullable();
            $table->string('report_type')->default('def_type')->nullable();
            $table->text('header_text')->nullable();
            $table->string('header_alignment')->default('def_header_align')->nullable();
            $table->text('footer_text')->nullable();
            $table->string('footer_alignment')->default('def_footer_align')->nullable();
            $table->string('cover_style')->default('def_cover')->nullable();
            $table->text('cover_background')->nullable();
            $table->integer('data_source')->default(0)->nullable();
            $table->string('individual_report')->default('def_individual')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
