<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('section_id')->unsigned();
            $table->text('content')->nullable();
            $table->enum('answer_type', ['free_text','multichoice','checkboxes','matching','likert_scale','boolean','ranking','multiple_choice_with_constraint','date','time','datetime','distributed_points']);
            $table->enum('coordinate', ['horizontal', 'vertical']);
            
            $table->text('answer_configs')->nullable(); 
            $table->text('answer_attributes')->nullable(); 
            $table->text('answer_scores')->nullable(); 
            // ex: multichoice -> ['Apple', 'Orange', 'Mango'] | matching -> [['Sails', 'Rails', 'Laravel'],['PHP', 'NodeJS', 'Ruby']] | likert_scale -> [{"label": "Sangat Kurang", "value": 1}, {"label": "Kurang", "value": 2}, {"label": "Sedang", "value": 3}, {"label": "Cukup", "value": 4}, {"label": "Sangat Cukup", "value": 5}] | ranking -> 5 (output on view are 1,2,3,4,5) | multichoice_with_constraint -> {"answer_max": 2, options: ['Apple', 'Orange', 'Mango']} | distributed_point -> {point: 10, options: ['PHP', 'NodeJS', 'Ruby']}
            
            $table->boolean('is_required')->default(true);
            $table->timestamps();

            $table->foreign('section_id')
                ->references('id')->on('sections')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
