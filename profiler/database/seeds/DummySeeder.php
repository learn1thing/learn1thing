<?php

use Illuminate\Database\Seeder;
use App\Models\Survey;
use App\Models\Question;
use App\Models\AnswerHeader;
use App\Models\AnswerDetail;

class DummySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->clearData();
    	
        $surveys = factory(Survey::class, 10)->create();

        foreach ($surveys as $key => $survey) {
        	$questions = factory(Question::class, 3)->make()->toArray();
        	$answerHeaders = factory(AnswerHeader::class, 3)->make()->toArray();
        	$survey->questions()->createMany($questions);
        	$survey->answerHeaders()->createMany($answerHeaders);

        	$answerDetails = factory(AnswerDetail::class, 3)->create([
        		'question_id' => $survey->questions->pluck('id')->random(),
        		'answer_header_id' => $survey->answerHeaders->pluck('id')->random()
        	]);
        }
    }

    /**
     * Clear data.
     *
     * @return void
     */
    private function clearData()
    {
        Schema::disableForeignKeyConstraints();
		Survey::truncate();
		Question::truncate();
		AnswerHeader::truncate();
		AnswerDetail::truncate();
    }
}
