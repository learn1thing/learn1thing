<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::get('/', function () {
    return redirect(RouteHelper::token('surveys.index'));
});

Route::group(['prefix' => '{authtoken}', 'middleware' => 'auth.middleware'], function ($token) {
	Route::get('/', function () {
	    return view('layouts/admin_template');
	});

	Route::get('formula-report', 'FormulaController@formulaQueryReport');

	Route::resource('attributes', 'AttributeController');
	Route::resource('observers', 'ObserverController');
	Route::resource('categories', 'CategoryController');
	// Route::resource('formulas', 'FormulaController');
	Route::resource('formulas', 'ReportFormulaController');
	Route::resource('category-profile', 'CategoryProfileController');
	Route::get('formulas/getQuestions/{survey_id}', ['uses' => 'FormulaController@getQuestion', 'as' => 'formulas.questions']);

	Route::resource('batches', 'BatchController');
	Route::get('batches/{surveyId}/observers', ['uses' => 'BatchController@getObserver', 'as' => 'batches.observers']);

	Route::group(['namespace' => 'Survey'], function () {
		Route::resource('surveys', 'SurveyController');
		Route::get('surveys/{surveyId}/batches', ['uses' => 'SurveyController@getBatch', 'as' => 'surveys.batches']);
		Route::get('surveys/{surveyId}/buyers', ['uses' => 'SurveyController@getBuyer', 'as' => 'surveys.buyers']);
		Route::get('surveys/{surveyId}/report-template', ['uses' => 'SurveyController@getReportTemplate', 'as' => 'surveys.report-template']);
		Route::get('surveys/{observerId}/{surveyId}/retake', ['uses' => 'SurveyController@getRetakeSurvey', 'as' => 'surveys.retake']);
		Route::get('surveys/{surveyId}/reminder', ['uses' => 'SurveyController@getReminder', 'as' => 'surveys.reminder']);
		//addedd by rdw 111217
		Route::get('surveys/{surveyId}/duplicate', ['uses' => 'SurveyController@duplicateData', 'as' => 'surveys.duplicate']);
		Route::get('surveys/{surveyId}/duplicatereport', ['uses' => 'SurveyController@duplicateDataReport', 'as' => 'surveys.duplicatereport']);
	});

	Route::group(['namespace' => 'Section'], function () {
		Route::resource('section', 'SectionController');
	});

	Route::post('template-builder/preview', ['uses' => 'TemplateBuilderController@postPreview', 'as' => 'template-builder.preview']);

	//Added by puw on 30-01-2018
	//For Preview Report like Generate.
	Route::post('template-builder/preview_v2', ['uses' => 'PreviewController@printReportPreview', 'as' => 'template-builder.preview_v2']);
	
	Route::post('template-builder/export', ['uses' => 'TemplateBuilderController@postExport', 'as' => 'template-builder.export']);
	Route::get('template-builder/templates', ['uses' => 'TemplateBuilderController@getTemplates', 'as' => 'template-builder.templates']);
	Route::post('template-builder/update-template', ['uses' => 'TemplateBuilderController@postUpdateTemplate', 'as' => 'template-builder.update-template']);
	Route::post('template-builder/get-detail', ['uses' => 'TemplateBuilderController@getDetails', 'as' => 'template-builder.detail']);

	Route::resource('template-builder', 'TemplateBuilderController');

	Route::get('template-builder', ['uses' => 'TemplateBuilderController@new_index', 'as' => 'template-builder.index']);
	Route::post('template-builder/store-formula', ['uses' => 'TemplateBuilderController@storeReportFormula', 'as' => 'template-builder.store-formula']);
	Route::get('new-template-builder', ['uses' => 'TemplateBuilderController@create', 'as' => 'template-builder.new']);
	Route::post('new-store-template', ['uses' => 'TemplateBuilderController@new_store', 'as' => 'template-builder.save']);
	Route::post('update-template-builder/{template_builder}', ['uses' => 'TemplateBuilderController@update', 'as' => 'template-builder.update']);
	Route::get('default-template-builder/{template_builder}', ['uses' => 'TemplateBuilderController@setDefault', 'as' => 'template-builder.default']);
	// Route::delete('template-builder/{emailBuilder}', ['uses' => 'TemplateBuilderController@destroy', 'as' => 'template-builder.destroy']);
});


Route::get('preview/consumer', ['uses' => 'PreviewController@previewAsConsumer', 'as' => 'preview.consumer']);
Route::post('preview/invitation/{code}', ['uses' => 'PreviewController@inviteObserver', 'as' => 'observer.invite']);
Route::post('preview/observer/store', ['uses' => 'PreviewController@storeObserver', 'as' => 'preview.observer.store']);
Route::post('preview/observer/revoke/{id}', ['uses' => 'PreviewController@revokeObserver', 'as' => 'preview.observer.revoke']);
Route::post('preview/survey/message-for-observer', ['uses' => 'PreviewController@storeMessageObserver', 'as' => 'preview.survey.message-for-observer']);
Route::post('preview/survey/backup-answers', ['uses' => 'PreviewController@backupAnswers', 'as' => 'preview.survey.backup_answers']);
Route::get('print-report/{batch}', ['uses' => 'PreviewController@printReport', 'as' => 'surveys.report']);
Route::get('thanks/{type}/{code}', ['uses' => 'PreviewController@thanks', 'as' => 'surveys.thanks']);

Route::post('preview/{id}', ['uses' => 'PreviewController@store', 'as' => 'previews.store']);
Route::get('preview/{type}/{code}', ['uses' => 'PreviewController@show', 'as' => 'surveys.preview']);
Route::get('print/{survey}', ['uses' => 'PreviewController@prints', 'as' => 'surveys.print']);

Route::post('survey/{id}', ['uses' => 'Survey\SurveyController@setObserver', 'as' => 'survey.post']);

Route::get('print/6b98b7d5c2e625c1eb5c3abb4fb4048f', ['uses' => 'PreviewController@generatePDF', 'as' => 'surveys.pdf']);
Route::get('preview/6b98b7d5c2e625c1eb5c3abb4fb4048f', ['uses' => 'PreviewController@fakePreview', 'as' => 'surveys.fakepreview']);

Route::get('user/logout', ['uses' => 'Auth\LoginController@logout', 'as' => 'auth.logout']);
