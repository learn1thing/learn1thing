<?php

return [

    /**
     * APP.LABEL
     */
    'label' => [
        // Symbol

        // Alphabet A
        'add' => 'Add',


        // Alphabet B
        'batch' => 'Batch',

        // Alphabet C
        'create' => 'Create',
        'cancel' => 'Cancel',
        'category_name' => 'Category Name',
        'category_id' => 'Category ID',
        'current_state' => 'Current State',

        // Alphabet D


        // Alphabet E
        'email' => 'Email',
        'edit' => 'Edit',


        // Alphabet F
        'first_name' => 'First Name',


        // Alphabet G


        // Alphabet H


        // Alphabet I

        // Alphabet J

        // Alphabet K

        // Alphabet L
        'last_name' => 'Last Name',
        // Alphabet M


        // Alphabet N
        'name' => 'Name',

        // Alphabet O,
        'observer' => 'Observer',

        // Alphabet P
        'phone_number' => 'Phone Number',

        // Alphabet Q

        // Alphabet R

        // Alphabet S
        'status' => 'Status',
        'start_date' => 'Start Date',
        'end_date' => 'End Date',
        'survey' => 'Survey',
        // Alphabet T
        'title' => 'Title',

        // Alphabet U

        // Alphabet V

        // Alphabet W

        // Alphabet X

        // Alphabet Y

        // Alphabet Z
    ],

    'attribute' => [
        // Symbol

        // Alphabet A



        // Alphabet B

        // Alphabet C


        // Alphabet D


        // Alphabet E


        // Alphabet F


        // Alphabet G


        // Alphabet H


        // Alphabet I

        // Alphabet J

        // Alphabet K

        // Alphabet L

        // Alphabet M


        // Alphabet N
        'name' => 'Name',

        // Alphabet O,


        // Alphabet P

        // Alphabet Q

        // Alphabet R

        // Alphabet S

        // Alphabet T

        // Alphabet U

        // Alphabet V

        // Alphabet W

        // Alphabet X

        // Alphabet Y

        // Alphabet Z
    ],

    'category' => [
        // Symbol

        // Alphabet A



        // Alphabet B

        // Alphabet C


        // Alphabet D


        // Alphabet E


        // Alphabet F


        // Alphabet G


        // Alphabet H


        // Alphabet I

        // Alphabet J

        // Alphabet K

        // Alphabet L

        // Alphabet M


        // Alphabet N
        'category_name' => 'Category Name',
        'title' => 'Title',

        // Alphabet O,


        // Alphabet P

        // Alphabet Q

        // Alphabet R

        // Alphabet S
        'survey_id' => 'Survey ID',
        // Alphabet T

        // Alphabet U

        // Alphabet V

        // Alphabet W

        // Alphabet X

        // Alphabet Y

        // Alphabet Z
    ],

    'button' => [
        'update' => 'Update'
        , 'delete' => 'Delete'
        , 'preview' => 'Preview Survey'
        , 'generate' => 'Generate Report'
        , 'retake' => 'Retake Survey'
        , 'list_observer' => 'Observer List'
        , 'list_buyer' => 'Buyer List'
        , 'submit' => 'Submit'
    ],

    'profiler' => [
        'settings' => 'Settings',
        'questions' => 'Questions',
        'statistics' => 'Statistics',
        'save_as_draft' => 'Save as draft',
        'publish' => 'Publish',
        'preview_as_consumer' => 'Preview as consumer',
        'variable_help' => 'Variable help',
        'name' => 'Profiler name',
        'description' => 'Profiler desc',
        'instruction' => 'Profiler instruction',
        'profiler_type' => 'Profiler type',
        'observer_count' => 'Max observer per category',
        'observer_category' => 'Observer category',
        'observer_info' => 'Observer info',
        'observer_reminder' => 'Observer reminder',
        'reminder_value' => 'Reminder value',
        'same_observer_desc' => 'Same observer description',
        'cmp_reminder' => 'Fixed OMP reminder',
        'random_question' => 'Randomize questions',
        'thanks_message' => 'Thank you message',
        'redirect_after_completion' => 'Redirect after completion',
        'label_consumer_bought' => 'Consumer bought this profiler',
        'label_corporate_bought' => 'Corporate bought this profiler',
        'persons' => 'Persons',
        'organizations' => 'Organizations',
        'statistic_text' => 'Some text',
        'batch_reference' => 'Batch Reference',
        'attributes' => 'Attributes',
        'report_template' => 'Report template',
        'placeholder' => [
            'name' => 'Enter profiler name',
            'description' => 'Enter profiler desc',
            'instruction' => 'Enter profiler instruction',
            'observer_info' => 'Enter observer info',
            'thanks_message' => 'Enter thank you message',
            'redirect_url_input' => 'URL to redirect',
            'category_profiler' => 'Select observer category profile',
            'observer_count' => 'Enter max observer per category',
            'input_reminder_date' => 'Enter the date for reminder',
            'input_reminder_time' => 'Enter the time for reminder',
            'report_template' => 'Select default template for report'
        ]
    ]
];
