<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Messages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'success' => [
        'create' => '<strong>Success !</strong> Data :name is successfully created.',
        'update' => '<strong>Success !</strong> Data :name is successfully updated.',
        'delete' => '<strong>Success !</strong> Data :name is successfully deleted.',
        'retake' => '<strong>Success !</strong> retake survey for observer :name.',
    ],

    'error' => [
        'retake' => '<strong>Failed !</strong> retake survey for observer :name.',
    ]
];
