<div class="modal fade" id="formulas" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">{{ trans('app.label.add') }}</h4>
			</div>
			{!! Form::model($data['formula'], $form) !!}
			<div class="modal-body">
				<div class="panel-body">
					<div class="box-body">
						<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
							<label for="name" class="col-sm-2 control-label">{!! trans('app.formula.name') !!}</label>

							<div class="col-sm-10">
								{!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
								{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">{!! trans('app.label.cancel') !!}</button>
				{!! Form::submit(trans('app.label.add'), ['class'=>'btn blue']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>