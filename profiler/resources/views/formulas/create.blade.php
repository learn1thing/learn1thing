@extends('layouts.admin_template')

@push('plugins-css')
	{!! Html::style('vendor/AdminLTE/plugins/datatables/dataTables.bootstrap.css') !!}
	{!! Html::style('vendor/AdminLTE/plugins/datepicker/datepicker3.css') !!}
	{!! Html::style('vendor/bootstrap-toggle/css/bootstrap-toggle.css') !!}
	{!! Html::style('vendor/AdminLTE/plugins/select2/select2.css') !!}
	{!! Html::style('vendor/jquery-toast-plugin/src/jquery.toast.css') !!}
@endpush

@push('custom-css')
	<style type="text/css">
		.nav-tabs-custom {
			box-shadow: none;
		}
	</style>
@endpush

@section('content')
	<div class="row">
		<!-- left column -->
		<div class="col-md-10 col-md-offset-1">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Formula Form</h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				{!! Form::open(array('url' => RouteHelper::token('formulas.store'), 'method'=>'POST', 'id'=>'form-formula')) !!}
				<div class="box-body">
					<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
						<label for="inputTitle">Formula Name</label>
						{{ Form::text('name', old('name'), array("id" => "inputTitle", "class" => "form-control", "placeholder" => "Formula Name" )) }}
						@if ($errors->has('name'))
							<span class="help-block">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group {{ $errors->has('survey_id') ? ' has-error' : '' }}">
						<label for="inputTitle">Survey Name</label>
						{!! Form::select('survey_id', [''=>'Select Survey'] + $surveys, old('survey_id'), ['class' => 'form-control select2 survey_id', 'data-url' => RouteHelper::token('formulas.questions', ['survey_id' => ':survey_id'])]) !!}
						@if ($errors->has('survey_id'))
							<span class="help-block">
								<strong>{{ $errors->first('survey_id') }}</strong>
							</span>
						@endif
					</div>
					<!-- Custom Tabs -->
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#details" data-toggle="tab">Details</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="details" data-section-type="details">
								<div class="box-group section-wrapper" id="accordion">
									<div class="sections">
										<!-- render sections here -->
										<!-- @include('formulas.forms.detail', ['sections' => old('detail'), 'section_type' => 'detail']) -->
									</div>
									<div class="box-footer" style="border-top: none;"></div>
								</div>
							</div>
						</div>
						<!-- /.tab-content -->
					</div>
					<!-- nav-tabs-custom -->
				</div>
				<!-- /.box-body -->

				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
				{!! Form::close() !!}
			</div>
			<!-- /.box -->

		</div>
		<!--/.col (left) -->
	</div>
@endsection

@push('plugins-js')
	{!! Html::script('vendor/underscore/underscore.js') !!}
	{!! Html::script('vendor/AdminLTE/plugins/daterangepicker/moment.js') !!}
	{!! Html::script('vendor/AdminLTE/plugins/datepicker/bootstrap-datepicker.js') !!}
	{!! Html::script('vendor/bootstrap-toggle/js/bootstrap-toggle.js') !!}
	{!! Html::script('vendor/AdminLTE/plugins/ckeditor/ckeditor.js') !!}
	{!! Html::script('vendor/AdminLTE/plugins/select2/select2.js') !!}
	{!! Html::script('vendor/jquery-toast-plugin/src/jquery.toast.js') !!}
	{!! Html::script('js/formula.js') !!}
	{!! Html::script('vendor/jsvalidation/js/jsvalidation.js') !!}
@endpush

@push('custom-js')
	{!! Html::script('js/global.js') !!}
	<script type="text/javascript">
		function rules() { // This Rule for form | Mandatory
			{!! JsValidator::formRequest(App\Http\Requests\FormulaRequest::class)->render(
				'vendor.jsvalidation.custom',
				'#attribute-form'
			) !!}
		}
	</script>
@endpush