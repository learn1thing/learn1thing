@if(count(@$details) === 0)
<div class="panel box box-primary section">
	<div id="section" class="panel-collapse collapse in">
      	<div class="box-body">
	      	<div class="col-md-12 details">
	      		<div class="detail-temp row">
					<div class="col-md-3 form-group">
						{!! Form::select('question_id[]', [''=>'Select Question'] + $questions, old('question_id'), ['class' => 'form-control select2 question_id']) !!}
					</div>
					<div class="col-md-3 form-group">
						{!! Form::select('type[]', [''=>'Select Type'] + $types, old('type'), ['class' => 'form-control select2 type', 'required'=>'required']) !!}
					</div>
					<div class="col-md-3 form-group">
						{!! Form::select('operand[]', [''=>'Select Operand'] + $operands, old('operand'), ['class' => 'form-control select2 operand']) !!}
					</div>
				</div>
			</div>
	    </div>
	</div>
</div>
@else
<div class="panel box box-primary section">
	<div id="section" class="panel-collapse collapse in">
      	<div class="box-body">
	      	<div class="col-md-12 details">
				@foreach(@$details as $key => $detail)
	      		<div class="detail-temp row">
					<div class="col-md-3 form-group">
						{!! Form::select('question_id[]', [''=>'Select Question'] + $questions, (@$detail['question_id']) ? @$detail['question_id'] : old('question_id'), ['class' => 'form-control select2 question_id']) !!}
					</div>
					<div class="col-md-3 form-group">
						{!! Form::select('type[]', [''=>'Select Type'] + @$types, (@$detail['type']) ? @$detail['type'] : old('type'), ['class' => 'form-control select2 type']) !!}
					</div>
					<div class="col-md-3 form-group">
						{!! Form::select('operand[]', [''=>'Select Operand'] + @$operands, (@$detail['operand']) ? @$detail['operand'] : old('operand'), ['class' => 'form-control select2 operand']) !!}
					</div>
						@if($key > 0)
						<div class="col-md-3 rm"> 
							<a href="#" class="btn btn-danger btn-sm remove-append"> <i class="fa fa-trash"></i> </a>
						</div>
						@endif
				</div>
				@endforeach
			</div>
	    </div>
	</div>
</div>    
@endif