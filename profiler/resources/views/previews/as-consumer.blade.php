@extends('layouts.survey')

@push('plugins-css')
    <!-- Select2 -->
    {!! Html::style('vendor/AdminLTE/plugins/select2/select2.min.css') !!}
    <!-- Rating -->
    {!! Html::style('vendor/rateYo/src/jquery.rateyo.css') !!}
    <!-- Datepicker -->
    {!! Html::style('vendor/AdminLTE/plugins/datepicker/datepicker3.css') !!}
    <!-- Timepicker -->
    {!! Html::style('vendor/AdminLTE/plugins/timepicker/bootstrap-timepicker.css') !!}
    {!! Html::style('vendor/jquery-toast-plugin/src/jquery.toast.css') !!}
@endpush

@push('custom-css')
@endpush

@push('plugins-js')
    <!-- Underscore -->
    {!! Html::script('vendor/underscore/underscore.js') !!}
    <!-- JS Validation -->
    {!! Html::script('vendor/jsvalidation/js/jsvalidation.js') !!}
    <!-- Select2 -->
    {!! Html::script('vendor/AdminLTE/plugins/select2/select2.full.min.js') !!}
    <!-- Rating -->
    {!! Html::script('vendor/rateYo/src/jquery.rateyo.js') !!}
    <!-- Datepicker -->
    {!! Html::script('vendor/AdminLTE/plugins/datepicker/bootstrap-datepicker.js') !!}
    <!-- Timepicker -->
    {!! Html::script('vendor/AdminLTE/plugins/timepicker/bootstrap-timepicker.js') !!}
    {!! Html::script('vendor/jquery-toast-plugin/src/jquery.toast.js') !!}
@endpush

@push('custom-js')
    <script type="text/javascript">
        var varUserId = '{{ $user_id }}';
    </script>
    <!-- template for section -->
    <script id="section-template" type="text/template">
        <div class="panel box box-primary section">
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool remove-section" data-widget="remove" title="Remove"><i class="fa fa-remove"></i></button>
            </div>
            <div class="box-body form-horizontal">
                {{-- <fieldset class=""> --}}
                    <div class="form-group clearfix">
                        <label class="col-sm-2 control-label">Observer category:</label>
                        <div class="col-sm-3">
                            <select name="category_profile_detail_id" class="form-control">
                                @foreach($categories as $category)
                                    <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <label class="col-sm-3 control-label">Observer category code:</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="Unique code">
                        </div>
                    </div>
                    <div class="questions">
                    </div>
                    <div class="">
                        <a href="#" class="btn btn-default pull-right add-question" data-index="<%= section_index %>">Add row</a>
                    </div>
                {{-- </fieldset> --}}
            </div>
        </div>
    </script>
    <!-- end template for section -->

    <!-- template for question -->
    <script id="question-template" type="text/template">
        <div class="form-horizontal question callout">
            <div class="box-body">
                <div class="col-sm-2">
                    <input class="form-control" name="first_name" type="text" placeholder="First name" required>
                </div>
                <div class="col-sm-2">
                    <input class="form-control" name="last_name" type="text" placeholder="Last name" required>
                </div>
                <div class="col-sm-2">
                    <input class="form-control" name="email" type="email" placeholder="Email" required>
                </div>
                <div class="col-sm-2">
                    <input class="form-control" name="phone_number" type="text" placeholder="Phone number">
                </div>
                <div class="col-sm-2">
                    <input class="form-control" name="url_link" type="text" maxlength="10" placeholder="URL Link" disabled>
                </div>
                <div class="col-sm-1">
                    
                </div>
                <div class="col-sm-1">
                    {{-- <button class="btn btn-default btn-save-observer form-control" title="Save" data-index="0">Save</button> --}}
                    <div class="form-group">
                        <button class="btn btn-default btn-save-observer form-control" title="Save" data-index="0">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </script>
    <!-- end template for question -->

    <script id="observer-saved-template" type="text/template" type="text/template">
        <div class="form-horizontal question callout">
            <div class="box-body">
                <input type="hidden" name="batchcode" value="<%= batch_code %>">
                <input type="hidden" name="observer_id" value="<%= observer_id %>">
                <div class="col-sm-2">
                    <input class="form-control" name="first_name" type="text" placeholder="First name" value="<%= first_name %>" disabled>
                </div>
                <div class="col-sm-2">
                    <input class="form-control" name="last_name" type="text" placeholder="Last name" value="<%= last_name %>" disabled>
                </div>
                <div class="col-sm-2">
                    <input class="form-control" name="email" type="email" placeholder="Email" value="<%= email %>" disabled>
                </div>
                <div class="col-sm-2">
                    <input class="form-control" name="phone_number" type="text" placeholder="Phone number" value="<%= phone_number %>" disabled>
                </div>
                <div class="col-sm-2">
                    <input class="form-control" name="url_link" type="text" maxlength="10" placeholder="URL Link" value="<%= url_link %>" disabled>
                </div>
                <div class="col-sm-1 no-padding">
                    Pending
                </div>
                <div class="col-sm-1">
                    {{-- <button class="btn btn-default btn-save-observer form-control" title="Save" data-index="0">Save</button> --}}
                    <div class="form-group">
                        
                    <button class="btn btn-default resend-observer" data-href="<%= remind_url %>" title="Remind" data-index="0"><span class="glyphicon glyphicon-send"></span></button>
                    <button class="btn btn-default revoke-observer" title="Revoke" data-index="0"><span class="glyphicon glyphicon-remove"></span></button>
                    </div>
                </div>
            </div>
        </div>
    </script>

    <script type="text/javascript">
        function showCategoryButton(number) {
            curVal = $("input.category-limit").val();
            $("input.category-limit").val(parseInt(curVal) + parseInt(number));
            if (curVal - 1 == 1) {
                $(".add-section").hide();
            } else {
                $(".add-section").show();
            }
        }

        function showObserverButton(index, number) {
            curVal = $('input.categories-'+index+'-observer_limit').val();
            $('input.categories-'+index+'-observer_limit').val(parseInt(curVal) + parseInt(number));
            if (curVal - 1 == 1) {
                $('a[data-index='+index+']').hide();
            } else {
                $('a[data-index='+index+']').show();
            }
        }

        $('.section-wrapper').delegate('.add-section', 'click', function (e) {
            e.preventDefault();
            var parent = $(this).parents('.tab-pane');
            let surveyType = parent.data('survey-type');
            var sectionTemplate = _.template($('#section-template').html());
            var sectionCount = $(this).parents('.section-wrapper').find('.sections .panel.box.section').length;
            var sectionId = 'section-'+ (parseInt(sectionCount)+1);
            var sections = $(this).parents('.section-wrapper').find('.sections').append(sectionTemplate({survey_type: surveyType, section_index: sectionCount, obs_count: $('input.observer-limit').val(), user_id: varUserId}));
            var lastSection = sections.find('.box.section').last();
            lastSection.find('.box-title a').attr('href', '#' + sectionId);
            lastSection.find('.box-title a').html('Section ' + (sectionCount + 1));
            lastSection.find('.panel-collapse').attr('id', sectionId);
            showCategoryButton('-1');
        });

        $('.sections').delegate('.remove-section', 'click', function (e) {
            e.preventDefault();
            $(this).parents('.section').remove();
            showCategoryButton('1');
        });

        $('.sections').delegate('.remove-question', 'click', function (e) {
            e.preventDefault();
            $(this).parents('.question.callout').remove();
            showObserverButton($(this).data('index'), '1');
        });

        $('.sections').delegate('.add-question', 'click', function (e) {
            e.preventDefault();
            var parent = $(this).parents('.tab-pane');
            var section = $(this).parents('.box.section');
            let surveyType = parent.data('survey-type');
            var sectionNumber = section.index();
            var questionTemplate = _.template($('#question-template').html());
            var questionCount = $(this).parent().parent().find('.questions .question.callout').length;
            var questionInputId = surveyType + '-' + 'section-' + sectionNumber + '-question-'+ questionCount;
            var questions = section.find('.box-body .questions').append(questionTemplate({survey_type: surveyType, section_index: sectionNumber, question_index: questionCount}));
            questions.find('.question.callout').last().find('textarea#question').attr('id', questionInputId);
            showObserverButton($(this).data('index'), '-1');
        });
    </script>

    <script type="text/javascript">
        $(document).on('click', '.resend-observer', function(event){
            var userSurveyId = $('.questions input[name=user_survey_id]').val();
            event.preventDefault();
            $.ajax({
                url: $(this).data("href")
                , data: {
                    user_survey_id: userSurveyId
                }
                , method: 'POST'
                , beforeSend: function() {
                    $(".spinner").removeClass('hide');
                    $(".spinner-wrapper").css('z-index', 0);
                }
                , success: function(result) {
                    $.toast({
                      heading: result ? 'Success' : 'Failed',
                      text: result.message,
                      icon: result ? 'success' : 'error',
                      position: 'top-right'
                    });
                }
                , error: function(result) {
                    alert("error");
                }
                , complete: function() {
                    $(".spinner").addClass('hide');
                    $(".spinner-wrapper").css('z-index', -1);
                }
            });
        });

        $('.tab-content').delegate('button.btn-save-observer', 'click', function (event) {
            event.preventDefault();

            var self = this;
            $(self).attr('disabled', 'disabled');
            var categoryParent = $(this).parents('.questions').parent();
            var parent = $(self).parents('.form-horizontal.question.callout');

            var firstName = parent.find('input[name=first_name]').val();
            var lastName = parent.find('input[name=last_name]').val();
            var email = parent.find('input[name=email]').val();
            var phoneNumber = parent.find('input[name=phone_number]').val();
            var userSurveyId = $('.questions input[name=user_survey_id]').val();
            var categoryProfileDetailId = categoryParent.find('select[name=category_profile_detail_id]').val();
            console.log('rang', categoryParent.find('select[name=category_profile_detail_id]'));
            
            var params = {
                first_name: firstName,
                last_name: lastName,
                email: email,
                phone_number: phoneNumber,
                user_survey_id: userSurveyId,
                category_id: categoryProfileDetailId
            };
            $.ajax({
                url: '/preview/observer/store'
                , method: 'POST'
                , data: params
                , beforeSend: function() {
                    $(".spinner").removeClass('hide');
                    $(".spinner-wrapper").css('z-index', 0);
                }
                , success: function(result) {
                    $(self).removeAttr('disabled');
                    $.toast({
                      heading: result.code == 200 ? 'Success' : 'Failed',
                      text: result.message,
                      icon: result.code == 200 ? 'success' : 'error',
                      position: 'top-right'
                    });

                    if(result.code == 200) {
                        var observerElement = $(self).parents('.question.callout');
                        var observerSavedTemplate = _.template($('#observer-saved-template').html());
                        observerElement.replaceWith(observerSavedTemplate(result.data));
                    }
                }
                , error: function(result) {
                    $(self).removeAttr('disabled');
                    alert("error");
                }
                , complete: function() {
                    $(self).removeAttr('disabled');
                    $(".spinner").addClass('hide');
                    $(".spinner-wrapper").css('z-index', -1);
                }
            });
        });

        $('.tab-content').delegate('button.revoke-observer', 'click', function (event) {
            event.preventDefault();

            var self = this;
            $(self).attr('disabled', 'disabled');
            var parent = $(this).parents('.question.callout');
            var observerId = parent.find('input[name=observer_id]').val();

            $.ajax({
                url: '/preview/observer/revoke/' + observerId
                , method: 'POST'
                , beforeSend: function() {
                    $(".spinner").removeClass('hide');
                    $(".spinner-wrapper").css('z-index', 0);
                }
                , success: function(result) {
                    $(self).removeAttr('disabled');
                    $.toast({
                      heading: result.code == 200 ? 'Success' : 'Failed',
                      text: result.message,
                      icon: result.code == 200 ? 'success' : 'error',
                      position: 'top-right'
                    });

                    if(result.code == 200) {
                        parent.remove();
                        showObserverButton($(this).data('index'), '1');
                    }
                    
                }
                , error: function(result) {
                    $(self).removeAttr('disabled');
                    alert("error");
                }
                , complete: function() {
                    $(self).removeAttr('disabled');
                    $(".spinner").addClass('hide');
                    $(".spinner-wrapper").css('z-index', -1);
                }
            });
        });
        $('#settings.tab-pane .section-wrapper .form-horizontal').delegate('button.save-observer-message', 'click', function (event) {
            event.preventDefault();

            var self = this;
            $(self).attr('disabled', 'disabled');
            var parent = $(this).parents('.form-horizontal');
            var userSurveyId = $('.questions input[name=user_survey_id]').val();
            var messageForObserver = parent.find('textarea[name=message_for_observer]').val();
            var params = {
                user_survey_id: userSurveyId,
                message_for_observer: messageForObserver
            };
            $.ajax({
                url: '/preview/survey/message-for-observer'
                , method: 'POST'
                , data: params
                , beforeSend: function() {
                    $(".spinner").removeClass('hide');
                    $(".spinner-wrapper").css('z-index', 0);
                }
                , success: function(result) {
                    $(self).removeAttr('disabled');
                    $.toast({
                      heading: result.code == 200 ? 'Success' : 'Failed',
                      text: result.message,
                      icon: result.code == 200 ? 'success' : 'error',
                      position: 'top-right'
                    });
                    
                }
                , error: function(result) {
                    $(self).removeAttr('disabled');
                    alert("error");
                }
                , complete: function() {
                    $(self).removeAttr('disabled');
                    $(".spinner").addClass('hide');
                    $(".spinner-wrapper").css('z-index', -1);
                }
            });
        })
    </script>
@endpush

@section('web-title', $survey->title)

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(isset($survey->id))
                <a href="{{ RouteHelper::token('surveys.edit', ['authtoken' => $survey->_token, 'id' => $survey->id, 'session_id' => $session_id]) }}" class="btn btn-default"><span class="glyphicon glyphicon-menu-left"></span> Back to form</a>
            @else
                <a href="{{ RouteHelper::token('surveys.create', ['authtoken' => $survey->_token, 'session_id' => $session_id]) }}" class="btn btn-default"><span class="glyphicon glyphicon-menu-left"></span> Back to form</a>
            @endif
            <br>
            <br>
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $survey->title }}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <blockquote>
                        {{-- @if($type === 'batch' && strlen($survey->observer_description) > 0)
                            <p>{!! nl2br(e($survey->observer_description)) !!}</p>
                        @else
                            <p>{!! nl2br(e($survey->description)) !!}</p>
                        @endif --}}
                        @if($type == 'customer' || (isset($survey->same_observer_desc) && $survey->same_observer_desc == 1) || $survey->profiler_type == 'self-profiler')
                            <p>{{ nl2br($survey->description) }}</p>
                            <p>{{ nl2br($survey->instruction) }}</p>
                        @else
                            {!! nl2br($survey->observer_description) !!}
                        @endif

                    </blockquote>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#activity" data-toggle="tab">Response</a></li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane {{ ($type != 'customer') || ($type == 'customer' && $survey->profiler_type === 'self-profiler') ? 'active' : '' }}" id="activity">
                        @include('previews.activity-as-consumer', ['survey' => $survey, 'type' => $type, 'code' => $code, 'userSurvey' => $userSurvey, 'answers' => (isset($answers) ? $answers : null)])
                    </div>
                </div>
            </div>
        </div>
        <!-- /.col -->
    </div>
@endsection