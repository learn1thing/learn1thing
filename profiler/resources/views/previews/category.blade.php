<!-- render sections here -->
@foreach($observer_categories as $key => $observer_category)
<div class="panel box no-border section">
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool remove-section" data-widget="remove" title="Remove"><i class="fa fa-remove"></i></button>
    </div>
    <div class="box-body form-horizontal">
        {{-- <fieldset class=""> --}}
            <input type="hidden" name="user_id" value="{{ $user_id }}">
            <div class="form-group clearfix">
                <label class="col-sm-2 control-label">Observer category:</label>
                <div class="col-sm-3">
                    {{-- <input type="text" placeholder="Category name" class="form-control" name="categories[$key][name]" required> --}}
                    <select name="category_profile_detail_id" class="form-control" disabled>
                        <option value="{{ $observer_category['id'] }}">{{ $observer_category['name'] }}</option>
                        @foreach($categories as $category)
                            <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                        @endforeach
                    </select>
                    {{-- <input type="hidden" placeholder="Category name" class="form-control" name="categories[$key][user_id]" required value="{{ $user_id }}"> --}}
                    <input type="hidden" class="categories-{{ $key }}-observer_limit" value="{{ $obs_count }}">
                </div>
                <label class="col-sm-3 control-label">Observer category code:</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" placeholder="Unique code">
                </div>
            </div>
            <div class="questions">
                @include('previews.observer', ['userSurvey' => $userSurvey, 'observers' => $observer_category['observers'], 'max_observer' => $obs_count])
            </div>
            @if(count($observer_category['observers']) < $obs_count)
            <div class="">
                <a href="#" class="btn btn-default pull-right add-question" data-index="{{ $key }}">Add row</a>
            </div>    
            @endif
        {{-- </fieldset> --}}
        
    </div>
</div>
@endforeach
@if(count($observer_categories) == 0)
<div class="panel box no-border section">
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool remove-section" data-widget="remove" title="Remove"><i class="fa fa-remove"></i></button>
    </div>
    <div class="box-body form-horizontal">
        {{-- <fieldset class=""> --}}
            <input type="hidden" name="user_id" value="{{ $user_id }}">
            <div class="form-group clearfix">
                <label class="col-sm-2 control-label">Observer category:</label>
                <div class="col-sm-3">
                    {{-- <input type="text" placeholder="Category name" class="form-control" name="categories[0][name]" required> --}}
                    <select name="category_profile_detail_id" class="form-control">
                        @foreach($categories as $category)
                            <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                        @endforeach
                    </select>
                    {{-- <input type="hidden" placeholder="Category name" class="form-control" name="categories[0][user_id]" required value="{{ $user_id }}"> --}}
                    <input type="hidden" class="categories-0-observer_limit" value="{{ $obs_count }}">
                </div>
                <label class="col-sm-3 control-label">Observer category code:</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" placeholder="Unique code">
                </div>
            </div>
            <div class="questions">
                @include('previews.observer', ['userSurvey' => $userSurvey, 'observers' => array(), 'max_observer' => $obs_count])
            </div>
            <div class="">
                <a href="#" class="btn btn-default pull-right add-question" data-index="0">Add row</a>
            </div>    
        {{-- </fieldset> --}}
        
    </div>
</div>
@endif