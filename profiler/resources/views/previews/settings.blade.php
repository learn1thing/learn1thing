<div class="row">
    <div class="col-xs-12">
        <!-- {!! Form::open(array('url' => RouteHelper::check('survey.post', $id),'method'=>'POST')) !!} -->

            <input type="hidden" name="link_type" value="{{ $type }}">
            <input type="hidden" name="link_code" value="{{ $code }}">
            <input type="hidden" class="category-limit" value="{{ $survey->category_count }}">
            <input type="hidden" class="observer-limit" value="{{ $survey->observer_count }}">
            <!-- /.tab-pane -->
            <div class="tab-pane" id="post" data-survey-type="post">
                <div class="box-group section-wrapper" id="accordion">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-2 control-label">
                                Message to observer:
                            </label>
                            <div class="col-sm-8">
                                <textarea name="message_for_observer" class="form-control" rows="3">{{ $userSurvey->message_for_observer }}</textarea>
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-default save-observer-message">Save</button>
                            </div>
                        </div>
                    </div>
                    <div class="sections">
                        @include('previews.category', ['obs_count' => $survey->observer_count, 'user_id' => $user_id, 'observer_categories' => $observer_categories])
                    </div>
                    {{-- <div class="box-footer"> --}}
                        {{-- <button class="btn btn-default pull-right add-section">Add new category</button> --}}
                        {{-- <button type="submit" class="btn btn-primary pull-left">Send</button> --}}
                    {{-- </div> --}}
                </div>
            </div>
        <!-- {!! Form::close() !!} -->
    </div>
</div>