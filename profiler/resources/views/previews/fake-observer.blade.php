<div class="form-horizontal question callout">
    <div class="box-body">
        <div class="form-group ">
            <label for="email" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
                <input class="form-control" name="categories[0][observers][0][email]" type="email" placeholder="Email" required>
            </div>
        </div>
        <div class="form-group ">
            <label for="first_name" class="col-sm-2 control-label">First name</label>
            <div class="col-sm-10">
                <input class="form-control" name="categories[0][observers][0][first_name]" type="text" placeholder="First name" required>
            </div>
        </div>
        <div class="form-group ">
            <label for="last_name" class="col-sm-2 control-label">Last name</label>
            <div class="col-sm-10">
                <input class="form-control" name="categories[0][observers][0][last_name]" type="text" placeholder="Last name" required>
            </div>
        </div>
        <div class="form-group ">
            <label for="phone_number" class="col-sm-2 control-label">Phone number</label>
            <div class="col-sm-10">
                <input class="form-control" name="categories[0][observers][0][phone_number]" type="text" placeholder="Phone number" required>
            </div>
        </div>
        <div class="form-group hide">
            <label for="batch" class="col-sm-2 control-label">Batch reference</label>
            <div class="col-sm-10">
                <input class="form-control" name="categories[0][observers][0][batch]" type="text" placeholder="Batch reference" value="6b98b7d5c2e625c1eb5c3abb4fb4048f" required>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <div class="row">
            <div class="pull-right">
                <button class="btn btn-default remove-question" title="Remove question"><i class="fa fa-fw fa-trash"></i></button>
            </div>
        </div>
    </div>
</div>