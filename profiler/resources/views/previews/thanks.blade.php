@extends('layouts.survey')

@push('plugins-css')
@endpush

@push('custom-css')
    <style type="text/css">
        .footer {
            position: fixed;
            bottom: 0;
        }
        .content-wrapper {
            overflow: hidden;
            min-height: 514px !important;
        }
        .box-body blockquote {
            background: white !important;
            color: inherit !important;
        }
        .box-header {
            margin-top: 40px;
            padding: 20px;
        }
        .box-body {
            padding: 20px !important;
        }
    </style>
@endpush

@push('plugins-js')
@endpush

@push('custom-js')
    <script type="text/javascript">
        function close_window() {
            if (confirm("You'll redirecting to {{ ENV('HOME_URL', 'http://beta.learn1thing.com') }}")) {
                window.location.replace("{{ ENV('HOME_URL', 'http://beta.learn1thing.com') }}");
            }
        }
    </script>
@endpush

@section('web-title', $survey->title)

@push('custom-js')
<script type="text/javascript">
    $(document).ready(function() {
        if ("{{$redirect_after_completion}}" == 1) {
            setTimeout(function() {
                window.location.href = "{{$redirect_after_completion_url}}";
            }, 3000);
        }
    });
</script>
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border text-center">
                <h3 class="box-title">{!! (!empty($thanks_message)) ? $thanks_message : 'Thanks for your time' !!}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body text-center">
                <button class="btn btn-info" onclick="close_window();return false;">Close</button>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
@endsection