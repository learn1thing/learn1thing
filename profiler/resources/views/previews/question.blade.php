@php ($baseName = 'question['.$question->id.']')
@php ($number = $question->question_number)

<div>
    <!-- <span style="display: inline">
        @if(!$section->is_number_automated)
            {{ $question->question_number }}
        @elseif($section->type === "arabic")
            {{ $number }}
        @elseif($section->type === "alphabet")
            {{ arabic_number($number - 1) }}
        @else
            {{ romanic_number($number) }}
        @endif.
    </span> -->
    {!! $question->content !!}
</div>

@if($question->answer_type == "checkboxes" || $question->answer_type == "multiple_choice_with_constraint")

    @if($question->answer_type == "multiple_choice_with_constraint")
        @php ( $settings = $question->answer_configs )
        @php ( $options = $settings->options )
        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-11">
                <label>Max Selected: {{ $settings->points }}</label>
            </div>
        </div>
    @else
        @php ( $options = $question->answer_configs )
    @endif

    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-md-11 col-sm-11 wrapper-answer-config" {{ $question->answer_type == "multiple_choice_with_constraint" ? "data-max-selected=" . $settings->points:"" }}>
            @foreach( $options as $label )
                <div class="checkbox {{ $question->coordinate == 'horizontal' ? 'checkbox-inline' : '' }}">
                    <label>
                        <input name="{{ $baseName }}[]" class="{{ $question->answer_type == 'multiple_choice_with_constraint' ? 'with-constraint' : '' }}" value="{{ $label }}" type="checkbox">
                        {{ $label }}
                    </label>
                </div>
            @endforeach
            <span class="help-block"></span>
        </div>
    </div>

@elseif($question->answer_type == "free_text")
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-md-11 col-sm-11">
            <div class="row">
                <div class="col-md-6">
                    <textarea name="{{ $baseName }}" class="form-control" rows="3" placeholder="Fill answer ..." {{ $question->is_required ? 'required' : '' }}></textarea>
              </div>
          </div>
            <span class="help-block"></span>
        </div>
    </div>

@elseif($question->answer_type == "multichoice" || $question->answer_type == "boolean" || $question->answer_type == "likert_scale")
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-md-11 col-sm-11">
            @foreach( $question->answer_configs as $answer_config )
                @if($question->answer_type == "likert_scale")
                    @php ($label = $answer_config->label)
                    @php ($value = $answer_config->value)
                @else
                    @php ($label = $answer_config)
                    @php ($value = $answer_config)
                @endif

                <div class="radio {{ $question->coordinate == 'horizontal' ? 'radio-inline' : '' }}">
                    <label>
                        <input name="{{ $baseName }}" value="{{ $value }}" type="radio" {{ $question->is_required ? 'required' : '' }}>
                        {{ $label }}<br/>
                    </label>
                </div>
            @endforeach
            <span class="help-block"></span>
        </div>
    </div>

@elseif($question->answer_type == "matching")
    @php ( $settings = $question->answer_configs )
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-md-11 col-sm-11">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="form-group clearfix">
                        <div class="col-sm-6">
                            <strong>Option</strong>
                        </div>
                        <div class="col-sm-6">
                            <strong>Value</strong>
                        </div>
                    </div>
                    @foreach( $settings as $setting )
                        <div class="form-group clearfix">
                            <div class="col-sm-6">
                                <strong> {{ $setting->label }} </strong>
                            </div>
                            <div class="col-sm-6">
                                <select class="form-control" name="{{ $baseName }}{{ str_replace(' ', '_', $setting->label) }}" placeholder="" {{ $question->is_required ? 'required' : '' }}>
                                    <option value="">Choose Option</option>
                                    @foreach( $settings as $data )
                                        <option value="{{ $data->value }}"> {{ $data->value }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <span class="help-block"></span>
        </div>
    </div>

@elseif($question->answer_type == "ranking")
    @php ( $settings = $question->answer_configs )
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-md-11 col-sm-11">
            <div class="form-group">
                <div class="radio tes-rating"></div>
                <input type="hidden" name="{{ $baseName }}" {{ $question->is_required ? 'required' : '' }}>
            </div>
            <span class="help-block"></span>
        </div>
    </div>

@elseif($question->answer_type == "date")
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-md-11 col-sm-11">
            <div class="row">
                <div class="col-md-6">
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="{{ $baseName }}" class="form-control pull-right datepicker selected-datepicker" {{ $question->is_required ? 'required' : '' }}>
                    </div>
                </div>
                <span class="help-block"></span>
            </div>
        </div>
    </div>

@elseif($question->answer_type == "time")
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-md-11 col-sm-11">
            <div class="row">
                <div class="col-md-6 bootstrap-timepicker">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        <input type="text" name="{{ $baseName }}" class="form-control timepicker selected-timepicker" {{ $question->is_required ? 'required' : '' }}>
                  </div>
                </div>
            </div>
            <span class="help-block"></span>
        </div>
    </div>

@elseif($question->answer_type == "datetime")
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-md-11 col-sm-11">
            <div class="row">
                <div class="col-md-3">
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="{{ $baseName }}[date]" class="form-control pull-right datepicker selected-datepicker-datetime" {{ $question->is_required ? 'required' : '' }}>
                    </div>
                </div>
                <div class="col-md-3 bootstrap-timepicker">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        <input type="text" name="{{ $baseName }}[time]" class="form-control timepicker selected-timepicker-datetime" {{ $question->is_required ? 'required' : '' }}>
                    </div>
                </div>
            </div>
            <span class="help-block"></span>
        </div>
    </div>

@elseif($question->answer_type == "distributed_points")
    @php ( $settings = $question->answer_configs )
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-md-11 col-sm-11">
            <div class="row wrapper-distributed_points">
                <div class="col-md-6">
                    <label class="control-label label-max-point" data-max-point="{{ isset($settings->point) ? $settings->point : 0 }}">Max Point: {{ isset($settings->point) ? $settings->point : 0 }}</label>
                    <table class="table table-bordered">
                        <tr>
                            <th>List</th>
                            <th>Point</th>
                        </tr>
                        @if(isset($settings->options))
                            @foreach( $settings->options as $label )
                                <tr>
                                    <td>{{ $label }}</td>
                                    <td><input type="number" name="{{ $baseName }}{{ str_replace(' ', '_', $label) }}" class="form-control input-distributed_points" placeholder="Point" {{ $question->is_required ? 'required' : '' }}></td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
            </div>
            <span class="help-block"></span>
        </div>
    </div>
@endif