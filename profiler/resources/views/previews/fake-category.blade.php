<!-- render sections here -->
<div class="panel box no-border section">
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="remove" title="Remove"><i class="fa fa-remove"></i></button>
    </div>
    <div class="box-body form-horizontal">
        <div class="form-group clearfix">
            <label class="col-sm-2 control-label">Name:</label>
            <div class="col-sm-5">
                <input type="text" placeholder="Category name" class="form-control" name="categories[0][name]" required>
            </div>
        </div>
        <div class="questions">
            @include('previews.fake-observer')
        </div>
        <div class="">
            <a href="#" class="btn btn-default pull-right add-question">Add Observer</a>
        </div>
    </div>
</div>
