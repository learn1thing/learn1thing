{{-- <div class="form-horizontal question callout">
    <div class="box-body">
        <div class="col-sm-6">
            <label for="first_name" class="control-label">First name</label>
            <input class="form-control" name="categories[0][observers][0][first_name]" type="text" placeholder="First name" required>
        </div>
        <div class="col-sm-6">
            <label for="last_name" class="control-label">Last name</label>
            <input class="form-control" name="categories[0][observers][0][last_name]" type="text" placeholder="Last name" required>
        </div>
        <div class="col-sm-6">
            <label for="email" class="control-label">Email</label>
            <input class="form-control" name="categories[0][observers][0][email]" type="email" placeholder="Email" required>
        </div>
        <div class="col-sm-5">
            <label for="phone_number" class="control-label">Phone number</label>
            <input class="form-control" name="categories[0][observers][0][phone_number]" type="text" placeholder="Phone number">
        </div>
        <div class="col-sm-3 hide">
            <label for="batch" class="control-label">Batch reference</label>
            <input class="form-control" name="categories[0][observers][0][batch]" type="text" maxlength="10" placeholder="Batch reference">
        </div>
        <div class="col-sm-1">
            <label>&nbsp;</label>
            <button class="btn btn-default remove-question form-control" title="Remove question" data-index="0"><i class="fa fa-fw fa-trash"></i></button>
        </div>
    </div>
</div> --}}
<input type="hidden" name="user_survey_id" value="{{ $userSurvey->id }}">
@foreach($observers as $observer)
<div class="form-horizontal question callout">
    <div class="box-body">
        <input type="hidden" name="batchcode" value="{{ $observer['batch']['batch'] }}">
        <input type="hidden" name="observer_id" value="{{ $observer['id'] }}">
        <div class="col-sm-2">
            <input class="form-control" name="first_name" type="text" placeholder="First name" value="{{ $observer['first_name'] }}" disabled>
        </div>
        <div class="col-sm-2">
            <input class="form-control" name="last_name" type="text" placeholder="Last name" value="{{ $observer['last_name'] }}" disabled>
        </div>
        <div class="col-sm-2">
            <input class="form-control" name="email" type="email" placeholder="Email" value="{{ $observer['email'] }}" disabled>
        </div>
        <div class="col-sm-2">
            <input class="form-control" name="phone_number" type="text" placeholder="Phone number" value="{{ $observer['phone_number'] }}" disabled>
        </div>
        <div class="col-sm-2">
            <input class="form-control" name="url_link" type="text" maxlength="10" placeholder="Shareable Link" value="{{ $observer['url_link'] }}" disabled data-toggle="tooltip" data-placement="top" title="Shareable Link">
        </div>
        <div class="col-sm-1 no-padding">
            {{ $observer['status'] == 'pending' ? 'Pending':'Submitted' }}
        </div>
        <div class="col-sm-1">
            {{-- <button class="btn btn-default btn-save-observer form-control" title="Save" data-index="0">Save</button> --}}
            <div class="form-group">
                
            <button class="btn btn-default resend-observer" data-href="{{ RouteHelper::check('observer.invite', ['code' => $observer['batch']['batch']]) }}" title="Remind" data-index="0" {{ $observer['status'] == 'submitted' ? 'disabled':'' }}><span class="glyphicon glyphicon-send"></span></button>
            <button class="btn btn-default revoke-observer" title="Revoke" data-index="0" {{ $observer['status'] == 'submitted' ? 'disabled':'' }}><span class="glyphicon glyphicon-remove"></span></button>
            </div>
        </div>
    </div>
</div>
@endforeach
@if(count($observers) < $max_observer)
<div class="form-horizontal question callout">
    <div class="box-body">
        <div class="col-sm-2">
            <input class="form-control" name="first_name" type="text" placeholder="First name" required>
        </div>
        <div class="col-sm-2">
            <input class="form-control" name="last_name" type="text" placeholder="Last name" required>
        </div>
        <div class="col-sm-2">
            <input class="form-control" name="email" type="email" placeholder="Email" required>
        </div>
        <div class="col-sm-2">
            <input class="form-control" name="phone_number" type="text" placeholder="Phone number">
        </div>
        <div class="col-sm-2">
            <input class="form-control" name="url_link" type="text" maxlength="10" placeholder="Shareable Link" disabled>
        </div>
        <div class="col-sm-1">
            
        </div>
        <div class="col-sm-1">
            <div class="form-group">
                <button class="btn btn-default btn-save-observer form-control" title="Save" data-index="0">Save</button>
            </div>
        </div>
    </div>
</div>
@endif