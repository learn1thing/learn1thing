@extends('layouts.survey')

@push('plugins-css')
<!-- Select2 -->
{!! Html::style('vendor/AdminLTE/plugins/select2/select2.min.css') !!}
<!-- Rating -->
{!! Html::style('vendor/rateYo/src/jquery.rateyo.css') !!}
<!-- Datepicker -->
{!! Html::style('vendor/AdminLTE/plugins/datepicker/datepicker3.css') !!}
<!-- Timepicker -->
{!! Html::style('vendor/AdminLTE/plugins/timepicker/bootstrap-timepicker.css') !!}
@endpush

@push('custom-css')
@endpush

@push('plugins-js')
{!! Html::script('vendor/underscore/underscore.js') !!}
{{-- {!! Html::script('vendor/jsvalidation/js/jsvalidation.js') !!} --}}
<!-- Select2 -->
{!! Html::script('vendor/AdminLTE/plugins/select2/select2.full.min.js') !!}
<!-- Rating -->
{!! Html::script('vendor/rateYo/src/jquery.rateyo.js') !!}
<!-- Datepicker -->
{!! Html::script('vendor/AdminLTE/plugins/datepicker/bootstrap-datepicker.js') !!}
<!-- Timepicker -->
{!! Html::script('vendor/AdminLTE/plugins/timepicker/bootstrap-timepicker.js') !!}
@endpush

@push('custom-js')
@endpush

@section('web-title', '360&deg; Influence Grid')


@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="">
        <div class="box-header">

          <h3 class="box-title">360&deg; Influence Grid - Introduction</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <blockquote>
              <p>Rate yourself in terms of whether each statement reflects what you leverage when influencing others. There is no right or wrong answer. The instrument will only be of value if you give an accurate and objective description of yourself in an influence context in the workplace.</p>
              <p>Using the following scale, please rate {CLIENT_NAME}, in terms of whether each statement reflects what he/she leverages when influencing others. There is no right or wrong answer. Please try and be as objective as possible.</p>
              1 - Do not exhibit</br>
              2 – Rarely exhibit</br>
              3 – Occasionally exhibit</br>
              4 – Frequently exhibit</br>
              5 – Always exhibit</br>
          </blockquote>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#settings" data-toggle="tab">Setup</a></li>
                <li><a href="#activity" data-toggle="tab">Response</a></li>
                {{-- <li><a href="#timeline" onclick="window.location='{{ url("/print/6b98b7d5c2e625c1eb5c3abb4fb4048f") }}'">Report</a></li> --}}
            </ul>
            <div class="tab-content">
                <div class="tab-pane" id="activity">
                    <div class="row">
                        <div class="col-md-1 col-sm-1 hidden-sm hidden-md hidden-xs text-center">
                            <h4>1.</h4>
                        </div>
                        <div class="col-md-11 col-sm-11">
                            <h4>
                                <span class="visible-xs-inline visible-sm-inline visible-md-inline">1. </span>
                                Ability to focus on what matters and communicate it to others
                            </h4>
                        </div>
                    </div>
                    <div class="row" id="answerRadio1">
                        <div class="col-lg-1">
                        </div>
                        <div class="col-md-11 col-sm-11">
                            <div class="radio">
                                <label>
                                    <input name="answerRadio1" value="184" type="radio">
                                    Do not exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio1" value="185" type="radio">
                                    Rarely exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio1" value="186" type="radio">
                                    Occasionally exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio1" value="187" type="radio">
                                    Frequently exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio1" value="188" type="radio">
                                    Always exhibit
                                </label>
                            </div>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1 col-sm-1 hidden-sm hidden-md hidden-xs text-center">
                            <h4>2.</h4>
                        </div>
                        <div class="col-md-11 col-sm-11">
                            <h4>
                                <span class="visible-xs-inline visible-sm-inline visible-md-inline">2. </span>
                                Authority attached to position or title as derived from legitimate or positional power. Associated with legitimacy is also the use of reward and coercion.
                            </h4>
                        </div>
                    </div>
                    <div class="row" id="answerRadio2">
                        <div class="col-lg-1">
                        </div>
                        <div class="col-md-11 col-sm-11">
                            <div class="radio">
                                <label>
                                    <input name="answerRadio2" value="184" type="radio">
                                    Do not exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio2" value="185" type="radio">
                                    Rarely exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio2" value="186" type="radio">
                                    Occasionally exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio2" value="187" type="radio">
                                    Frequently exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio2" value="188" type="radio">
                                    Always exhibit
                                </label>
                            </div>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1 col-sm-1 hidden-sm hidden-md hidden-xs text-center">
                            <h4>3.</h4>
                        </div>
                        <div class="col-md-11 col-sm-11">
                            <h4>
                                <span class="visible-xs-inline visible-sm-inline visible-md-inline">3. </span>
                                The give and take needed to make relationships work.
                            </h4>
                        </div>
                    </div>
                    <div class="row" id="answerRadio3">
                        <div class="col-lg-1">
                        </div>
                        <div class="col-md-11 col-sm-11">
                            <div class="radio">
                                <label>
                                    <input name="answerRadio3" value="184" type="radio">
                                    Do not exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio3" value="185" type="radio">
                                    Rarely exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio3" value="186" type="radio">
                                    Occasionally exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio3" value="187" type="radio">
                                    Frequently exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio3" value="188" type="radio">
                                    Always exhibit
                                </label>
                            </div>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1 col-sm-1 hidden-sm hidden-md hidden-xs text-center">
                            <h4>4.</h4>
                        </div>
                        <div class="col-md-11 col-sm-11">
                            <h4>
                                <span class="visible-xs-inline visible-sm-inline visible-md-inline">4. </span>
                                Exhibiting a high degree of functional or technical knowledge as derived from expert power
                            </h4>
                        </div>
                    </div>
                    <div class="row" id="answerRadio4">
                        <div class="col-lg-1">
                        </div>
                        <div class="col-md-11 col-sm-11">
                            <div class="radio">
                                <label>
                                    <input name="answerRadio4" value="184" type="radio">
                                    Do not exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio4" value="185" type="radio">
                                    Rarely exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio4" value="186" type="radio">
                                    Occasionally exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio4" value="187" type="radio">
                                    Frequently exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio4" value="188" type="radio">
                                    Always exhibit
                                </label>
                            </div>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1 col-sm-1 hidden-sm hidden-md hidden-xs text-center">
                            <h4>5.</h4>
                        </div>
                        <div class="col-md-11 col-sm-11">
                            <h4>
                                <span class="visible-xs-inline visible-sm-inline visible-md-inline">5. </span>
                                Persuasive communication enlisting others to support (or not block) ideas and efforts
                            </h4>
                        </div>
                    </div>
                    <div class="row" id="answerRadio5">
                        <div class="col-lg-1">
                        </div>
                        <div class="col-md-11 col-sm-11">
                            <div class="radio">
                                <label>
                                    <input name="answerRadio5" value="184" type="radio">
                                    Do not exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio5" value="185" type="radio">
                                    Rarely exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio5" value="186" type="radio">
                                    Occasionally exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio5" value="187" type="radio">
                                    Frequently exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio5" value="188" type="radio">
                                    Always exhibit
                                </label>
                            </div>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1 col-sm-1 hidden-sm hidden-md hidden-xs text-center">
                            <h4>6.</h4>
                        </div>
                        <div class="col-md-11 col-sm-11">
                            <h4>
                                <span class="visible-xs-inline visible-sm-inline visible-md-inline">6. </span>
                                Ability to make and keep connections by adding value to networks
                            </h4>
                        </div>
                    </div>
                    <div class="row" id="answerRadio6">
                        <div class="col-lg-1">
                        </div>
                        <div class="col-md-11 col-sm-11">
                            <div class="radio">
                                <label>
                                    <input name="answerRadio6" value="184" type="radio">
                                    Do not exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio6" value="185" type="radio">
                                    Rarely exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio6" value="186" type="radio">
                                    Occasionally exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio6" value="187" type="radio">
                                    Frequently exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio6" value="188" type="radio">
                                    Always exhibit
                                </label>
                            </div>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1 col-sm-1 hidden-sm hidden-md hidden-xs text-center">
                            <h4>7.</h4>
                        </div>
                        <div class="col-md-11 col-sm-11">
                            <h4>
                                <span class="visible-xs-inline visible-sm-inline visible-md-inline">7. </span>
                                Ability to reach out as opposed to wait for things to happen.
                            </h4>
                        </div>
                    </div>
                    <div class="row" id="answerRadio7">
                        <div class="col-lg-1">
                        </div>
                        <div class="col-md-11 col-sm-11">
                            <div class="radio">
                                <label>
                                    <input name="answerRadio7" value="184" type="radio">
                                    Do not exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio7" value="185" type="radio">
                                    Rarely exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio7" value="186" type="radio">
                                    Occasionally exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio7" value="187" type="radio">
                                    Frequently exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio7" value="188" type="radio">
                                    Always exhibit
                                </label>
                            </div>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1 col-sm-1 hidden-sm hidden-md hidden-xs text-center">
                            <h4>8.</h4>
                        </div>
                        <div class="col-md-11 col-sm-11">
                            <h4>
                                <span class="visible-xs-inline visible-sm-inline visible-md-inline">8. </span>
                                Exhibiting honesty and integrity in actions.
                            </h4>
                        </div>
                    </div>
                    <div class="row" id="answerRadio8">
                        <div class="col-lg-1">
                        </div>
                        <div class="col-md-11 col-sm-11">
                            <div class="radio">
                                <label>
                                    <input name="answerRadio8" value="184" type="radio">
                                    Do not exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio8" value="185" type="radio">
                                    Rarely exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio8" value="186" type="radio">
                                    Occasionally exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio8" value="187" type="radio">
                                    Frequently exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio8" value="188" type="radio">
                                    Always exhibit
                                </label>
                            </div>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1 col-sm-1 hidden-sm hidden-md hidden-xs text-center">
                            <h4>9.</h4>
                        </div>
                        <div class="col-md-11 col-sm-11">
                            <h4>
                                <span class="visible-xs-inline visible-sm-inline visible-md-inline">9. </span>
                                Being able to shift focus when necessary.
                            </h4>
                        </div>
                    </div>
                    <div class="row" id="answerRadio9">
                        <div class="col-lg-1">
                        </div>
                        <div class="col-md-11 col-sm-11">
                            <div class="radio">
                                <label>
                                    <input name="answerRadio9" value="184" type="radio">
                                    Do not exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio9" value="185" type="radio">
                                    Rarely exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio9" value="186" type="radio">
                                    Occasionally exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio9" value="187" type="radio">
                                    Frequently exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio9" value="188" type="radio">
                                    Always exhibit
                                </label>
                            </div>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1 col-sm-1 hidden-sm hidden-md hidden-xs text-center">
                            <h4>10.</h4>
                        </div>
                        <div class="col-md-11 col-sm-11">
                            <h4>
                                <span class="visible-xs-inline visible-sm-inline visible-md-inline">10. </span>
                                Ability to inspire enthusiasm and commitment as derived from referent power.
                            </h4>
                        </div>
                    </div>
                    <div class="row" id="answerRadio10">
                        <div class="col-lg-1">
                        </div>
                        <div class="col-md-11 col-sm-11">
                            <div class="radio">
                                <label>
                                    <input name="answerRadio10" value="184" type="radio">
                                    Do not exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio10" value="185" type="radio">
                                    Rarely exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio10" value="186" type="radio">
                                    Occasionally exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio10" value="187" type="radio">
                                    Frequently exhibit
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="answerRadio10" value="188" type="radio">
                                    Always exhibit
                                </label>
                            </div>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-default pull-right fake-submit-btn">Submit</button>
                        </div>
                    </div>
                </div>

                <div class="tab-pane active" id="settings">
                    <div class="row">
                        <div class="col-xs-12">

                        {!! Form::open(array('url' => RouteHelper::check('survey.post', $id),'method'=>'POST')) !!}
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="post" data-survey-type="post">
                                <div class="box-group section-wrapper" id="accordion">
                                    <div class="sections" style="background: white">
                                        <!-- render sections here -->
                                        @include('previews.fake-category')
                                    </div>
                                    <div class="box-footer" style="border-top: none;">
                                        <button class="btn btn-default pull-right add-section">
                                            Add Category
                                        </button>
                                        <button type="submit" class="btn btn-primary pull-left">Send</button>
                                    </div>
                                </div>
                            </div>

                            <!-- <button type="button" class="btn btn-default pull-right" onclick="window.location='{{ url("print/6b98b7d5c2e625c1eb5c3abb4fb4048f") }}'">Submit</button> -->

                        {!! Form::close() !!}
                        </div>
                        <!-- /.col -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('custom-js')
    <!-- template for section -->
    <script id="section-template" type="text/template">
        <div class="panel box box-primary section">
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="remove" title="Remove"><i class="fa fa-remove"></i></button>
            </div>
            <div class="box-body">
                <div class="form-group clearfix">
                    <label class="col-sm-2 control-label">Name:</label>
                    <div class="col-sm-5">
                        <input type="text" placeholder="Category name" class="form-control" name="categories[<%= section_index %>][name]" required>
                    </div>
                </div>
                <div class="questions">
                </div>
                <div class="">
                    <a href="#" class="btn btn-default pull-right add-question">Add Observer</a>
                </div>
            </div>
        </div>
    </script>
    <!-- end template for section -->

    <!-- template for question -->
    <script id="question-template" type="text/template">
        <div class="form-horizontal question callout">
            <div class="box-body">
                <div class="form-group ">
                    <label for="email" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="categories[<%= section_index %>][observers][<%= question_index %>][email]" type="email" required>
                    </div>
                </div>
                <div class="form-group ">
                    <label for="first_name" class="col-sm-2 control-label">First name</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="categories[<%= section_index %>][observers][<%= question_index %>][first_name]" type="text" required>
                    </div>
                </div>
                <div class="form-group ">
                    <label for="last_name" class="col-sm-2 control-label">Last name</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="categories[<%= section_index %>][observers][<%= question_index %>][last_name]" type="text" required>
                    </div>
                </div>
                <div class="form-group ">
                    <label for="phone_number" class="col-sm-2 control-label">Phone number</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="categories[<%= section_index %>][observers][<%= question_index %>][phone_number]" type="text" required>
                    </div>
                </div>
                <div class="form-group hide">
                    <label for="batch" class="col-sm-2 control-label">Batch reference</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="categories[<%= section_index %>][observers][<%= question_index %>][batch]" type="text" value="6b98b7d5c2e625c1eb5c3abb4fb4048f" required>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="pull-right">
                        <button class="btn btn-default remove-question" title="Remove question"><i class="fa fa-fw fa-trash"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </script>
    <!-- end template for question -->

    <script type="text/javascript">
        $('.section-wrapper').delegate('.add-section', 'click', function (e) {
            e.preventDefault();
            var parent = $(this).parents('.tab-pane');
            let surveyType = parent.data('survey-type');
            var sectionTemplate = _.template($('#section-template').html());
            var sectionCount = $(this).parents('.section-wrapper').find('.sections .panel.box.section').length;
            var sectionId = 'section-'+ (parseInt(sectionCount)+1);
            var sections = $(this).parents('.section-wrapper').find('.sections').append(sectionTemplate({survey_type: surveyType, section_index: sectionCount}));
            var lastSection = sections.find('.box.section').last();
            lastSection.find('.box-title a').attr('href', '#' + sectionId);
            lastSection.find('.box-title a').html('Section ' + (sectionCount + 1));
            lastSection.find('.panel-collapse').attr('id', sectionId);
        });

        $('.sections').delegate('.remove-question', 'click', function (e) {
            e.preventDefault();
            $(this).parents('.question.callout').remove();
        });

        $('.sections').delegate('.add-question', 'click', function (e) {
            e.preventDefault();
            var parent = $(this).parents('.tab-pane');
            var section = $(this).parents('.box.section');
            let surveyType = parent.data('survey-type');
            var sectionNumber = section.index();
            var questionTemplate = _.template($('#question-template').html());
            var questionCount = $(this).parent().parent().find('.questions .question.callout').length;
            var questionInputId = surveyType + '-' + 'section-' + sectionNumber + '-question-'+ questionCount;
            var questions = section.find('.box-body .questions').append(questionTemplate({survey_type: surveyType, section_index: sectionNumber, question_index: questionCount}));
            questions.find('.question.callout').last().find('textarea#question').attr('id', questionInputId);
        });
    </script>
    <script type="text/javascript">
        //Date picker
        $('.datepicker').datepicker({
          format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true
        });

        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });

        $(document).on('click', ".fake-submit-btn", function(){
            // Free text: Fill the answer.
            // Multiple Choice: Choose an answer.
            // Checkboxes: Check at least 1 option.
            // Matching: Match the answer.
            // Likert Scale: Choose an answer.
            // True or False: Choose an answer.
            // Ranking:Fill the answer.
            // Multiple Qstn wth constrant: Choose an answer.
            // Date: Choose the date.
            // Time: Choose the time.
            // Date n Time: Choose the time.
            // Distributed points: Fill the answer.

            errorCount = 0;
            // if (typeof $("input[name=answerRadio1]:checked").val() == "undefined") {
            //     $("div[class=row][id=answerRadio1]").addClass("has-error");
            //     errorCount += 1;
            //     $("div[id='answerRadio1']").find("span.help-block").html("Choose an answer");
            // } else {
            //     $("div[class='row has-error'][id=answerRadio1]").removeClass("has-error");
            //     $("div[id='answerRadio1']").find("span.help-block").html("");
            // }

            // if ($("input[name='answerCheck2[]']:checked").length <= 0) {
            //     $("div[class=row][id=answerCheck2]").addClass("has-error");
            //     errorCount += 1;
            //     $("div[id='answerCheck2']").find("span.help-block").html("Check at least 1 option.");
            // } else {
            //     $("div[class='row has-error'][id=answerCheck2]").removeClass("has-error");
            //     $("div[id='answerCheck2']").find("span.help-block").html("");
            // }

            // if (typeof $("input[name=answerRadio1]:checked").val() == "undefined") {
            //     $("div[class=row][id=answerRadio1]").addClass("has-error");
            //     errorCount += 1;
            //     $("div[id='answerRadio1']").find("span.help-block").html("Choose an answer.");
            // } else {
            //     $("div[class='row has-error'][id=answerRadio1]").removeClass("has-error");
            //     $("div[id='answerRadio1']").find("span.help-block").html("");
            // }

            // matchNum = 0;
            // for (i = 0; i < 3; i++) {
            //     element = $("select[name='answerOption3["+i+"]']");
            //     if(element.val() == "") {
            //         element.parent("div.col-sm-6").parent("div.form-group").addClass("has-error");
            //         errorCount += 1;
            //         matchNum += 1;
            //         $("div[id='answerSelect3']").find("span.help-block").html("Match the answer.");
            //     } else {
            //         element.parent("div.col-sm-6").parent("div.form-group").removeClass("has-error");
            //         $("div[id='answerSelect3']").find("span.help-block").html("");
            //     }
            // }
            // for (i = 0; i < 3; i++) {
            //     element = $("select[name='answerValue3["+i+"]']");
            //     if(element.val() == "") {
            //         element.parent("div.col-sm-6").parent("div.form-group").addClass("has-error");
            //         errorCount += 1;
            //         matchNum += 1;
            //         $("div[id='answerSelect3']").find("span.help-block").html("Match the answer.");
            //     } else {
            //         element.parent("div.col-sm-6").parent("div.form-group").removeClass("has-error");
            //         $("div[id='answerSelect3']").find("span.help-block").html("");
            //     }
            // }
            // if (matchNum == 0) {
            //     $("div[class='row has-error'][id=answerSelect3]").removeClass("has-error");
            //     $("div[id='answerSelect3']").find("span.help-block").html("");
            // } else {
            //     $("div[class=row][id=answerSelect3]").addClass("has-error");
            //     $("div[id='answerSelect3']").find("span.help-block").html("Match the answer.");
            // }

            // if (typeof $("input[name=answerRadio4]:checked").val() == "undefined") {
            //     $("div[class=row][id=answerRadio4]").addClass("has-error");
            //     errorCount += 1;
            //     $("div[id='answerRadio4']").find("span.help-block").html("Choose an answer.");
            // } else {
            //     $("div[class='row has-error'][id=answerRadio4]").removeClass("has-error");
            //     $("div[id='answerRadio4']").find("span.help-block").html("");
            // }

            // if (typeof $("input[name=answerBoolean5]:checked").val() == "undefined") {
            //     $("div[class=row][id=answerBoolean5]").addClass("has-error");
            //     errorCount += 1;
            //     $("div[id='answerBoolean5']").find("span.help-block").html("Choose an answer");
            // } else {
            //     $("div[class='row has-error'][id=answerBoolean5]").removeClass("has-error");
            //     $("div[id='answerBoolean5']").find("span.help-block").html("");
            // }

            // if (typeof $("input[name=answerRadio6]:checked").val() == "undefined") {
            //     $("div[class=row][id=answerRadio6]").addClass("has-error");
            //     errorCount += 1;
            //     $("div[id='answerRadio6']").find("span.help-block").html("Fill the answer.");
            // } else {
            //     $("div[class='row has-error'][id=answerRadio6]").removeClass("has-error");
            //     $("div[id='answerRadio6']").find("span.help-block").html("");
            // }

            // if ($("input[name='answerCheck7[]']:checked").length > 3){
            //     $("div[class=row][id=answerCheck7]").addClass("has-error");
            //     errorCount += 1;
            //     $("div[id='answerCheck7']").find("span.help-block").html("Check maximum 1 option.");
            // } else if ($("input[name='answerCheck7[]']:checked").length < 1) {
            //     $("div[class=row][id=answerCheck7]").addClass("has-error");
            //     errorCount += 1;
            //     $("div[id='answerCheck7']").find("span.help-block").html("Check at least 1 option.");
            // } else {
            //     $("div[class='row has-error'][id=answerCheck7]").removeClass("has-error");
            //     $("div[id='answerCheck7']").find("span.help-block").html("");
            // }

            // element = $("input[name=answerText8]");
            // if(element.val() == "") {
            //     element.parent("div.input-group.date").parent("div.col-md-6").addClass("has-error");
            //     errorCount += 1;
            //     $("div[id='answerText8']").find("span.help-block").html("Choose the date.");
            //     $("div[class=row][id=answerText8]").addClass("has-error");
            // } else {
            //     element.parent("div.input-group.date").parent("div.col-md-6").removeClass("has-error");
            //     $("div[id='answerText8']").find("span.help-block").html("");
            //     $("div[class='row has-error'][id=answerText8]").addClass("has-error");
            // }

            // element = $("input[name=answerText9]");
            // if(element.val() == "") {
            //     element.parent('div.input-group').parent("div.col-md-6").addClass("has-error");
            //     errorCount += 1;
            //     $("div[id='answerText9']").find("span.help-block").html("Choose the time.");
            //     $("div[class=row][id=answerText9]").addClass("has-error");
            // } else {
            //     element.parent('div.input-group').parent("div.col-md-6").removeClass("has-error");
            //     $("div[id='answerText9']").find("span.help-block").html("");
            //     $("div[class='row has-error'][id=answerText9]").addClass("has-error");
            // }

            // msg = '';
            // element = $("input[name='answerText10[date]']");
            // if(element.val() == "") {
            //     element.parent("div.input-group.date").parent("div.col-md-3").addClass("has-error");
            //     errorCount += 1;
            //     msg = "Choose the date.";
            //     // $("div[id='answerText10']").find("span.help-block").html("Choose the date.");
            // } else {
            //     element.parent("div.input-group.date").parent("div.col-md-3").removeClass("has-error");
            //     // $("div[id='answerText10']").find("span.help-block").html("");
            // }

            // element = $("input[name='answerText10[time]']");
            // if(element.val() == "") {
            //     element.parent("div.input-group").parent("div.col-md-3").addClass("has-error");
            //     errorCount += 1;
            //     msg += " Choose the time.";
            //     // $("div[id='answerText10']").find("span.help-block").html("Choose the time.");
            // } else {
            //     element.parent("div.input-group").parent("div.col-md-3").removeClass("has-error");
            //     // $("div[id='answerText10']").find("span.help-block").html("");
            // }
            // if (msg == '') {
            //     $("div[class='row has-error'][id=answerText10]").removeClass("has-error");
            //     $("div[id='answerText10']").find("span.help-block").html("");
            // } else {
            //     $("div[class=row][id=answerText10]").addClass("has-error");
            //     $("div[id='answerText10']").find("span.help-block").html(msg);
            // }

            // max_data = 10;
            // current_data = 0;
            // errCnt = 0;
            // for (i = 0; i < 3; i++) {
            //     element = $("input[name='answerNumber11["+i+"]']");
            //     if(element.val() == "") {
            //         element.parent("td").parent("tr").addClass("has-error");
            //         errorCount += 1;
            //         errCnt += 1;
            //         $("div[id='answerNumber11']").find("span.help-block").html("Fill the answer");
            //     } else {
            //         element.parent("td").parent("tr").removeClass("has-error");
            //         if(parseInt(element.val()) != NaN) {
            //             current_data += parseInt(element.val());
            //         }
            //         $("div[id='answerNumber11']").find("span.help-block").html("");
            //     }
            // }

            // if (max_data == current_data) {
            //     $(".answerNumber11").parent("td").parent("tr").removeClass('has-error');
            //     $("div[class='row has-error'][id=answerNumber11]").removeClass("has-error");
            //     $("div[id='answerNumber11']").find("span.help-block").html("");
            // } else {
            //     $(".answerNumber11").parent("td").parent("tr").addClass('has-error');
            //     $("div[class=row][id=answerNumber11]").addClass("has-error");
            //     errorCount += 1;
            //     if (errCnt == 0) {
            //         $("div[id='answerNumber11']").find("span.help-block").html("Maximum point is 10.");
            //     }
            // }

            // element = $("textarea[name=answerTextarea12]");
            // if(element.val() == "") {
            //     element.parent("div.col-md-6").addClass("has-error");
            //     errorCount += 1;
            //     $("div[id='answerTextarea12']").find("span.help-block").html("Fill the answer");
            //     $("div[class=row][id=answerTextarea12]").addClass("has-error");
            // } else {
            //     element.parent("div.col-md-6").removeClass("has-error");
            //     $("div[id='answerTextarea12']").find("span.help-block").html("");
            //     $("div[class='row has-error'][id=answerTextarea12]").removeClass("has-error");
            // }

            if (errorCount == 0) {
                window.location='{{ url("print/6b98b7d5c2e625c1eb5c3abb4fb4048f") }}';
            }
        })
    </script>
@endpush
