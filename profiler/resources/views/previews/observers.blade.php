<div class="observer-sections">
    @foreach($survey->savedCategories($user_id)->get() as $index => $category)
        <div class="panel box {{ $index == 0 ? 'no-border' : 'box-primary' }}">
            <div class="box-body">
                <div class="container">
                    <h2>{{ $category->name }}</h2>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>First name</th>
                                <th>Last name</th>
                                <th>Email</th>
                                <th>Phone number</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($category->observers as $observer)
                                <tr>
                                    <td>{{ $observer->first_name }}</td>
                                    <td>{{ $observer->last_name }}</td>
                                    <td>{{ $observer->email }}</td>
                                    <td>{{ $observer->phone_number }}</td>
                                    <td>
                                        <button class="btn btn-default resend-observer" data-href="{{ RouteHelper::check('observer.invite', ['code' => $observer->batch->batch]) }}" title="Resend Email">
                                            <i class="fa fa-fw fa-send"></i> Remind
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endforeach
</div>