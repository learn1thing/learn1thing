<table class="main" data-types="background,padding" data-last-type="padding" style="border-spacing: 0px; border-collapse: collapse; text-size-adjust: 100%; overflow: hidden;" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td class="element-content" style="background-color: rgb(255, 255, 255); border-collapse: collapse; text-size-adjust: 100%;" align="left">
                <style type="text/css">
                    @page { margin:0; size:A4; }
                    body{
                        margin:0;
                        padding:0;
                        font-family: 'Rubik', sans-serif;
                        height: 29.7cm;
                        margin:auto;
                        background-color: gray;
                    }

                    @font-face {
                        font-family: 'Rubik';
                        src: url(fonts/rubik/Rubik-Light.ttf);
                        font-weight: 300;
                    }

                    @font-face {
                        font-family: 'Rubik';
                        src: url(fonts/rubik/Rubik-Regular.ttf);
                        font-weight: 400;
                    }

                    @font-face {
                        font-family: 'Rubik';
                        src: url(fonts/rubik/Rubik-Medium.ttf);
                        font-weight: 500;
                    }

                    @font-face {
                        font-family: 'Rubik';
                        src: url(fonts/rubik/Rubik-Bold.ttf);
                        font-weight: 700;
                    }

                    @font-face {
                        font-family: 'Rubik';
                        src: url(fonts/rubik/Rubik-Black.ttf);
                        font-weight: 900;
                    }

                    @font-face {
                        font-family: ProximaNova;
                        src: url('fonts/proxima-nova-58f4b5b9b407b.otf');
                    }

                    .wrapper{
                        position: relative;
                        overflow: hidden;
                        height: 29.7cm;
                    }

                    .container{
                        background-color: #ffffff;
                        height: 100%;
                    }

                    .intro-content{
                        height: 1650px;
                    }

                    .observers-content{
                        height: 1650px;
                    }

                    .intro-logo{
                        background-image: url(https://storage.googleapis.com/learn1thing/img/brand-logo.png);
                        background-size: 100%;
                        height: 100px;
                        width: 240px;
                        margin: 25px 35px;
                        position: absolute;
                        right: 0;
                    }

                    .observers-logo{
                        background-image: url(https://storage.googleapis.com/learn1thing/img/brand-logo-white.png);
                        background-size: 100%;
                        height: 58px;
                        width: 138px;
                        margin: 22px 20px;
                        position: absolute;
                        left: 0;
                    }

                    .intro-content header{
                        display: block;
                        height: 165px;
                    }

                    .observers-content header{
                        display: block;
                        height: 175px;
                    }

                    .observers-content header img{
                        width: 100%;
                        height: 235px;
                    }

                    .banner-overlay{
                        background-color: #000000;
                        opacity: 0.3;
                        position:absolute;
                        height: 100%;
                        width: 100%;
                    }

                    .intro-title{
                        height: 450px;
                        position: absolute;
                        width: 640px;
                        top: 0;
                        overflow: hidden;
                    }

                    .shape-2-container{
                        height: 345px;
                        position: absolute;
                        width: 100%;
                        top: 0;
                        overflow: hidden;
                        right: -24px;
                    }

                    .shape-3-container{
                        height: 195px;
                        position: absolute;
                        width: 100%;
                        top: 0;
                        overflow: hidden;
                        right: 0px;
                    }

                    .shape-1 {
                        border-right: 28px solid #e1bf13;
                        width: 1290px;
                        height: 2400px;
                        position: absolute;
                        background-color: #ffffff;
                        -ms-transform: rotate(45deg);
                        -webkit-transform: rotate(45deg);
                        top: -927px;
                        right: 565px;
                        transform: rotate(45deg);
                    }

                    .shape-2 {
                        border-right: 28px solid #001e61;
                        width: 750px;
                        height: 1890px;
                        position: absolute;
                        background-color: #ffffff;
                        -ms-transform: rotate(45deg);
                        -webkit-transform: rotate(45deg);
                        top: -560px;
                        right: 640px;
                        transform: rotate(45deg);
                    }

                    .shape-3 {
                        width: 750px;
                        height: 1890px;
                        position: absolute;
                        background-color: #001e61;
                        -ms-transform: rotate(45deg);
                        -webkit-transform: rotate(45deg);
                        top: -560px;
                        right: 640px;
                        transform: rotate(45deg);
                    }

                    .title-container{
                        background-color: #001e61;
                        color: #ffffff;
                        position: relative;
                        font-weight: 400;
                        font-size: 47px;
                        display: inline-block;
                        padding:8% 0;
                        width: 37%;
                        padding-left: 50px;
                    }

                    article.article-observers{
                        font-size: 1.1em;
                        padding: 20px 70px 20px 90px;
                        color: #202020;
                    }

                    article.article-observers p{
                    line-height: 1.35em;
                    }

                    article.article-johari-detail{
                    padding: 50px 30px;
                    color: #001e61;
                    font-size: 0.95em;
                    }

                    article.article-johari-detail .h3{
                    font-weight: 500;
                    font-size: 1.2em;
                    display: block;
                    }

                    article.article-johari-detail p{
                    margin-bottom: 30px;
                    margin-top:0;
                    }

                    footer{
                    position: absolute;
                    bottom: 0;
                    background-color: #001e61;
                    height: 115px;
                    width: 100%;
                    }

                    footer .intro-title{
                    bottom: 0;
                    top: initial;
                    left: 0px;
                    min-height: auto;
                    height: 145px;
                    width: 320px
                    }

                    .l1t-logo-container{
                    height: 90px;
                    position: absolute;
                    overflow: hidden;
                    top: 55px;
                    width: 223px;
                    }

                    .l1t-logo-container .shape-1{
                    border-right: 40px solid transparent;
                    background-color: #2e2e2e;
                    }

                    .l1t-logo{
                    background-image: url(https://storage.googleapis.com/learn1thing/img/l1t-logo.png);
                    height: 88px;
                    position: absolute;
                    width: 128px;
                    background-size: 105px;
                    background-position: center center;
                    background-repeat: no-repeat;
                    }

                    .l1t-logo.l1t-logo-small{
                    background-size: 80px;
                    }

                    .footer-content{
                    color: #ffffff;
                    text-align: right;
                    width: 60%;
                    padding-right: 30px;
                    padding-top: 37px;
                    position: absolute;
                    font-size: 0.5em;
                    font-weight: 300;
                    right: 0;
                    }

                    .bold {
                    font-weight: 700;
                    }

                    footer .contacts{
                    font-size: 1.5em;
                    font-weight: 400;
                    margin-top: 11px;
                    }

                    footer .contacts > span{
                    margin-left: 20px;
                    }

                    footer .contacts > span .icon img{
                    width: 20px;
                    vertical-align: middle;
                    margin-right: 5px;
                    }

                    .title.title-observers{
                    font-size: 2.5em;
                    text-transform: uppercase;
                    font-weight: 700;
                    text-align: center;
                    color: #001e5f;
                    font-family: ProximaNova;
                    margin-bottom: 20px;
                    }

                    .title.title-johari-detail{
                    font-size: 1.75em;
                    text-transform: uppercase;
                    font-weight: 700;
                    text-align: center;
                    color: #001e5f;
                    margin-bottom:65px;
                    }

                    .subtitle.title-johari-detail{
                    font-size: 1.55em;
                    text-transform: uppercase;
                    font-weight: 500;
                    text-align: center;
                    color: #001e5f;
                    margin-bottom: 30px;
                    }

                    .post-image{
                    text-align: center;
                    }

                    .post-image img{
                    width: 570px;
                    }

                    .two-column-detail{
                    width: 100%;
                    float: none;
                    max-width: 740px;
                    margin: auto;
                    }

                    .two-column-detail + div + .two-column-detail {
                    margin-top: 30px;
                    }

                    .two-column-detail > div{
                    width: 35%;
                    float: left;
                    padding: 1% 1% 1% 70px;
                    }

                    .two-column-detail > div:first-of-type{
                    height: 320px;
                    background: url('https://storage.googleapis.com/learn1thing/img/johari.png');
                    background-size: 280px;
                    background-repeat: no-repeat;
                    background-position: 60px 10px;
                    position: relative;
                    }

                    .two-column-detail > div + div{
                    width: 43%;
                    float: left;
                    padding: 1% 0;
                    }

                    .two-column-detail dl
                    {
                    width: 200px;
                    background: #fff;
                    border: 1px solid #9BCDE8;
                    padding: 5px 15px;
                    border-radius: 5px;
                    }

                    .two-column-detail dt, .two-column-detail dd
                    {
                    display: inline;
                    margin: 0;
                    }

                    .two-column-detail > div:first-of-type > span{
                    position: absolute;
                    bottom: 65px;
                    left: 98px;
                    }

                    .y2 {
                    bottom: 118px !important;
                    }

                    .y3 {
                    bottom: 171px !important;
                    }

                    .y4 {
                    bottom: 226px !important;
                    }

                    .y5 {
                    bottom: 278px !important;
                    }

                    .x2 {
                    left: 152px !important;
                    }

                    .x3 {
                    left: 204px !important;
                    }

                    .x4 {
                    left: 260px !important;
                    }

                    .x5 {
                    left: 312px !important;
                    }
                </style>
                <div class="container">
                    <div class="wrapper">
                        <div class="observers-content">
                            <header>
                                <div class="observers-logo"></div>
                                <img src="https://storage.googleapis.com/learn1thing/img/banner.png">
                            </header>
                            <article class="article-johari-detail">
                                @foreach($question as $data)
                                    @php($self = isset($questionData[$data->id]['Self']) ? $questionData[$data->id]['Self'] : 1)
                                    <div class="two-column-detail">
                                        <div>
                                            @foreach($questionData[$data->id] as $key => $value)
                                                @if($key != 'Self')
                                                    <span class="y{{ $self }} x{{ $value }}">{{ $key[0] }}</span>
                                                @endif
                                            @endforeach
                                        </div>
                                        <div>
                                            <p style="text-size-adjust: 100%;">
                                                <span class="h3">{{ $data->name }}</span>
                                                {!! $data->content !!}
                                            </p>
                                            <p>
                                                <dl>
                                                    @foreach($questionData[$data->id] as $key => $value)
                                                        <dt>{{ $key }} :</dt>
                                                        <dd> {{ $value }} </dd>
                                                        <br/>
                                                    @endforeach
                                                </dl>
                                            </p>
                                        </div>
                                    </div>
                                    <div style="clear: both"></div>
                                @endforeach
                            </article>
                            <footer>
                                <div class="intro-title">
                                    <div class="shape-1"></div>
                                    <div class="l1t-logo-container">
                                        <div class="shape-1"></div>
                                        <div class="l1t-logo l1t-logo-small"></div>
                                    </div>
                                </div>
                                <div class="footer-content">
                                    <p style="text-size-adjust: 100%;"><span class="bold">learn1thing.com</span> is the E-commerce store dedicated to give a wholesome &amp; engaging learning experience. <br>The various learning tools, provide an ideal opportunity for those who seek to future-proof themselves.</p>

                                    <div class="contacts">
                                        <span><span class="icon"><img src="https://storage.googleapis.com/learn1thing/img/document-icon.png"></span>beta.learn1thing.com</span><span><span class="icon"><img src="https://storage.googleapis.com/learn1thing/img/phone-icon.png"></span>+65 63238022</span>
                                    </div>
                                </div>
                            </footer>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>