<div id="form-preview" class="form-horizontal">
    <div class="box-body question-list-sections">
        @php ($disabled = '')
        @php ($sections = $survey->questionnaire)
        @php ($position = 'questionnaire')

        @php ($number = 0)
        @foreach($sections as $section)
            <div class="panel box {{ $number == 0 ? 'no-border' : 'box-primary' }} section">

                <div class="box-header">
                    <strong>{{ $section['title'] }}</strong>
                    <p>{!! nl2br(e($section['description'])) !!}</p>
                </div>

                @php ($number = (isset($section['ordering_type']) ? $number : 0))
                @php ($questions = $section['question'])
                @foreach($questions as $key => $question)
                    @php ($baseName = $position.'['. $key .']')
                    @if(isset($section['automatic_numbering']))
                        @php ($number += 1)
                    @endif
                    <div class="row">
                        <div class="col-md-1 col-sm-1 hidden-sm hidden-md hidden-xs text-center">
                            <h4>
                                @if($section['automatic_numbering'] !== 'yes')
                                    {{ $question['question_number'] }}
                                @elseif($section['type'] === "arabic")
                                    {{ $number }}
                                @elseif($section['type'] === "alphabet")
                                    {{ arabic_number($number - 1) }}
                                @else
                                    {{ romanic_number($number) }}
                                @endif
                            </h4>
                        </div>
                        <div class="col-md-11 col-sm-11">
                            <h4>
                                <span class="visible-xs-inline visible-sm-inline visible-md-inline">{{ $number }}. </span>
                                {!! $question['content'] !!}
                            </h4>
                        </div>
                    </div>

                    @if($question['answer_type'] == "checkboxes" || $question['answer_type'] == "multiple_choice_with_constraint")

                        @if($question['answer_type'] == "multiple_choice_with_constraint")
                            @php ( $settings = $question['answer_configs'] )
                            @php ( $options = $settings['options'] )
                            <div class="row">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-11">
                                    <label>Max Selected: {{ $settings['points'] }}</label>
                                </div>
                            </div>
                        @else
                            @php ( $options = $question['answer_configs'] )
                        @endif

                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-md-11 col-sm-11 wrapper-answer-config" {{ $question['answer_type'] == "multiple_choice_with_constraint" ? "data-max-selected=" . $settings['points']:"" }}>
                                @foreach( $options as $label )
                                    <div class="checkbox {{ $question['coordinate'] == 'horizontal' ? 'checkbox-inline' : '' }}">
                                        <label>
                                            <input name="{{ $baseName }}[]" class="{{ $question['answer_type'] == 'multiple_choice_with_constraint' ? 'with-constraint' : '' }}" value="{{ $label }}" type="checkbox">
                                            {{ $label }}
                                        </label>
                                    </div>
                                @endforeach
                                <span class="help-block"></span>
                            </div>
                        </div>


                        @if($question['answer_type'] == "multiple_choice_with_constraint")
                            @push('custom-js')
                                <script type="text/javascript">
                                    $(document).on('change', '.with-constraint', function(){
                                        var wrapperAnswer = $(this).parents('.wrapper-answer-config');
                                        var selectedCount = wrapperAnswer.data('max-selected');
                                        var countAll = $(".with-constraint:checked").length;
                                        if (countAll == selectedCount) {
                                            wrapperAnswer.find('input.with-constraint:unchecked').attr('disabled', true);
                                        } else {
                                            wrapperAnswer.find('input.with-constraint:unchecked').attr('disabled', false);
                                        }
                                    });
                                </script>
                            @endpush
                        @endif

                        @if($question['is_required'])
                            @push('custom-js')
                                <script type="text/javascript">
                                    $(document).on('click', '.save-preview', function(){
                                        $('input[name="{{ $baseName }}[]"]').removeAttr('required');
                                        checked = $('input[name="{{ $baseName }}[]"]:checked').length;

                                        if(!checked) {
                                            $('input[name="{{ $baseName }}[]"]').eq(0).attr('required', true);
                                            // return false;
                                        }
                                    })
                                </script>
                            @endpush
                        @endif

                    @elseif($question['answer_type'] == "free_text")
                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-md-11 col-sm-11">
                                <div class="row">
                                    <div class="col-md-6">
                                        <textarea name="{{ $baseName }}" class="form-control" rows="3" placeholder="Fill answer ..." {{ isset($question['is_required']) && $question['is_required'] ? 'required' : '' }} {{ $disabled }}></textarea>
                                  </div>
                              </div>
                                <span class="help-block"></span>
                            </div>
                        </div>

                    @elseif($question['answer_type'] == "multichoice" || $question['answer_type'] == "boolean" || $question['answer_type'] == "likert_scale")
                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-md-11 col-sm-11">
                                @foreach( $question['answer_configs'] as $answer_config )
                                    @if($question['answer_type'] == "likert_scale")
                                        @php ($label = $answer_config['label'])
                                        @php ($value = $answer_config['value'])
                                    @else
                                        @php ($label = $answer_config)
                                        @php ($value = $answer_config)
                                    @endif

                                    <div class="radio {{ isset($question['coordinate']) && $question['coordinate'] == 'horizontal' ? 'radio-inline' : '' }}">
                                        <label>
                                            <input name="{{ $baseName }}" value="{{ $value }}" type="radio" {{ isset($question['is_required']) && $question['is_required'] ? 'required' : '' }} {{ $disabled }}>
                                            {{ $label }}<br/>
                                        </label>
                                    </div>
                                @endforeach
                                <span class="help-block"></span>
                            </div>
                        </div>

                    @elseif($question['answer_type'] == "matching")
                        @php ( $settings = $question['answer_configs'] )

                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-md-11 col-sm-11">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group clearfix">
                                            <div class="col-sm-6">
                                                <strong>Option</strong>
                                            </div>
                                            <div class="col-sm-6">
                                                <strong>Value</strong>
                                            </div>
                                        </div>
                                        @foreach( $settings as $setting )
                                            <div class="form-group clearfix">
                                                <div class="col-sm-6">
                                                    <strong> {{ $setting['label'] }} </strong>
                                                </div>
                                                <div class="col-sm-6">
                                                    <select class="form-control" name="{{ $baseName }}[{{ str_replace(' ', '_', $setting['label']) }}]" placeholder="" {{ isset($question['is_required']) && $question['is_required'] ? 'required' : '' }} {{ $disabled }}>
                                                        <option value="">Choose Option</option>
                                                        @foreach( $settings as $data )
                                                            <option value="{{ $data['value'] }}">{{ $data['value'] }} </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>

                    @elseif($question['answer_type'] == "ranking")
                        @php ( $settings = $question['answer_configs'] )
                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-md-11 col-sm-11">
                                <div class="form-group">
                                    <div class="radio tes-rating"></div>
                                    <input type="hidden" name="{{ $baseName }}" {{ isset($question['is_required']) && $question['is_required'] ? 'required' : '' }} {{ $disabled }}>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        @push('custom-js')
                            <script type="text/javascript">
                                $(".tes-rating").rateYo({
                                    fullStar: true,
                                    numStars: {{ ($settings['maximal'] - $settings['minimal']) / $settings['interval'] }}
                                })
                                .on("rateyo.set", function (e, data) {
                                    $('input[name="{{ $baseName }}"]').val(data.rating);
                                });
                            </script>
                        @endpush

                    @elseif($question['answer_type'] == "date")
                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-md-11 col-sm-11">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" name="{{ $baseName }}" class="form-control pull-right datepicker selected-datepicker" {{ isset($question['is_required']) && $question['is_required'] ? 'required' : '' }} {{ $disabled }}>
                                        </div>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>

                        @push('custom-js')
                            <script type="text/javascript">
                                //Date picker
                                $('.selected-datepicker').datepicker({
                                  format: "yyyy-mm-dd",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            </script>
                        @endpush

                    @elseif($question['answer_type'] == "time")
                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-md-11 col-sm-11">
                                <div class="row">
                                    <div class="col-md-6 bootstrap-timepicker">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                            <input type="text" name="{{ $baseName }}" class="form-control timepicker selected-timepicker" {{ isset($question['is_required']) && $question['is_required'] ? 'required' : '' }} {{ $disabled }}>
                                      </div>
                                    </div>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        @push('custom-js')
                            <script type="text/javascript">
                                //Timepicker
                                $(".selected-timepicker").timepicker({
                                    showInputs: false
                                });
                            </script>
                        @endpush

                    @elseif($question['answer_type'] == "datetime")
                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-md-11 col-sm-11">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" name="{{ $baseName }}[date]" class="form-control pull-right datepicker selected-datepicker-datetime" {{ isset($question['is_required']) && $question['is_required'] ? 'required' : '' }} {{ $disabled }}>
                                        </div>
                                    </div>
                                    <div class="col-md-3 bootstrap-timepicker">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                            <input type="text" name="{{ $baseName }}[time]" class="form-control timepicker selected-timepicker-datetime" {{ isset($question['is_required']) && $question['is_required'] ? 'required' : '' }} {{ $disabled }}>
                                        </div>
                                    </div>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        @push('custom-js')
                            <script type="text/javascript">
                                //Date picker
                                $('.selected-datepicker-datetime').datepicker({
                                  format: "yyyy-mm-dd",
                                    autoclose: true,
                                    todayHighlight: true
                                });

                                //Timepicker
                                $(".selected-timepicker-datetime").timepicker({
                                    showInputs: false
                                });
                            </script>
                        @endpush

                    @elseif($question['answer_type'] == "distributed_points")
                        @php ( $settings = $question['answer_configs'] )

                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-md-11 col-sm-11">
                                <div class="row wrapper-distributed_points">
                                    <div class="col-md-6">
                                        <label class="control-label label-max-point" data-max-point="{{ isset($settings['point']) ? $settings['point'] : 0 }}">Max Point: {{ isset($settings['point']) ? $settings['point'] : 0 }}</label>
                                        <table class="table table-bordered">
                                            <tr>
                                                <th>List</th>
                                                <th>Point</th>
                                            </tr>
                                            @if(isset($settings['options']))
                                                @foreach( $settings['options'] as $label )
                                                    <tr>
                                                        <td>{{ $label }}</td>
                                                        <td><input type="number" name="{{ $baseName }}[{{ str_replace(' ', '_', $label) }}]" class="form-control input-distributed_points" placeholder="Point" {{ isset($question['is_required']) && $question['is_required'] ? 'required' : '' }} {{ $disabled }}></td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </table>
                                    </div>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        @push('custom-js')
                            <script type="text/javascript">
                                $(document).on('keyup', '.input-distributed_points', function(){
                                    var wrapperQuestion = $(this).parents('.wrapper-distributed_points');
                                    var maxPoint = wrapperQuestion.find('.label-max-point').data('max-point');
                                    var pointCounts = 0;
                                    wrapperQuestion.find('.input-distributed_points').each(function()
                                    {
                                        var value = $(this).val();
                                        if(value) {
                                            pointCounts += parseInt(value);
                                        }
                                    });
                                    if(pointCounts > maxPoint) {
                                        wrapperQuestion.parent().addClass('has-error');
                                        $('.save-preview').attr('disabled', true);
                                    } else {
                                        wrapperQuestion.parent().removeClass('has-error');
                                        $('.save-preview').attr('disabled', false);
                                    }
                                });
                            </script>
                        @endpush
                    @endif

                @endforeach

            </div>
        @endforeach
    </div>

{{--     <div class="box-footer">
        @if(!$answers)
            <button type="submit" class="btn btn-primary save-preview">Submit</button>
        @endif
    </div> --}}

</div>