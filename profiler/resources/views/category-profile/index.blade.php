@extends('layouts.admin_template')

@push('plugins-css')
	{!! Html::style('vendor/AdminLTE/plugins/datatables/dataTables.bootstrap.css') !!}
@endpush

@push('custom-css')
@endpush

@push('plugins-js')
	{!! Html::script('vendor/AdminLTE/plugins/datatables/jquery.dataTables.min.js') !!}
	{!! Html::script('vendor/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js') !!}
@endpush

@push('custom-js')
    {!! $dataTable->scripts() !!}

	<script type="text/javascript">
	    $(document).ready(function(){
	        $("table[id^='datatable-']").parent().addClass('table-responsive');
	    })
	</script>

    {!! Html::script('js/global.js') !!}
@endpush

@section('web-title', 'Category Profile')

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Observer Categories </h3>
				<div class="pull-right box-tools">
					<a href="{{ RouteHelper::token('category-profile.create') }}" class="btn btn-primary btn-create">
						{!! trans('app.label.create') !!}
					</a>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
                {!! $dataTable->table(['class' => 'table table-bordered table-striped', 'cellspacing'=>"0", 'width'=>"100%"]) !!}
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
	<!-- /.col -->
</div>

<div id="category-profile"></div>

@include('layouts.delete')
@endsection