@extends('layouts.admin_template')

@push('plugins-css')
	{!! Html::style('vendor/AdminLTE/plugins/select2/select2.css') !!}
	{!! Html::style('vendor/jquery-toast-plugin/src/jquery.toast.css') !!}
@endpush

@push('custom-css')
	<style type="text/css">
		.nav-tabs-custom {
			box-shadow: none;
		}
	</style>
@endpush

@section('web-title', 'Category Profile - Create')

@section('content')
	<div class="row">
		<!-- left column -->
		<div class="col-md-10 col-md-offset-1">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Category Profile Form</h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				{!! Form::open(array('url' => RouteHelper::token('category-profile.store'), 'method'=>'POST', 'id'=>'form-category-profile')) !!}
				<div class="box-body">
					<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
						<label for="inputTitle">Profile Name</label>
						{{ Form::text('name', old('name'), array("id" => "inputTitle", "class" => "form-control", "placeholder" => "Profile Name" )) }}
						@if ($errors->has('name'))
							<span class="help-block">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group {{ $errors->has('categories') ? ' has-error' : '' }}">
						<label for="inputCategories">Categories</label>
						{!! Form::select('categories[]', $categories, old('categories'), ['class' => 'form-control select2', 'multiple' => 'multiple']) !!}
						@if ($errors->has('categories'))
							<span class="help-block">
								<strong>{{ $errors->first('categories') }}</strong>
							</span>
						@endif
					</div>
				</div>
				<!-- /.box-body -->

				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
				{!! Form::close() !!}
			</div>
			<!-- /.box -->

		</div>
		<!--/.col (left) -->
	</div>
@endsection

@push('plugins-js')
	{!! Html::script('vendor/AdminLTE/plugins/select2/select2.js') !!}
	{!! Html::script('vendor/jsvalidation/js/jsvalidation.js') !!}
@endpush

@push('custom-js')
	{!! Html::script('js/global.js') !!}
	<script type="text/javascript">
		function rules() { // This Rule for form | Mandatory
			{!! JsValidator::formRequest(App\Http\Requests\StoreCategoryProfile::class)->render(
				'vendor.jsvalidation.custom',
				'#form-category-profile'
			) !!}
		}

		$(".select2").select2({
		  tags: true,
		  placeholder: 'Enter the categories'
		});
	</script>
@endpush