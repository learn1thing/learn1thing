<!-- <div>
    <p>How you did on your last holiday ? <strong><i>(kalo pilih component question)</i></strong></p>
    <strong><i>(kalo pilih component answer => hasilnya pasti 1 row, d ambil yg terbesar dan di filter sesuai dengan settingan)</i></strong>
    <ul>
        <li>Swimming</li>
    </ul>

    <strong><i>(kalo pilih component percentage)</i></strong>
    <ul>
        <li>Swimming (20%)</li>
        <li>Dying (20%)</li>
        <li>Sleeping (60%)</li>
    </ul>

    <strong><i>(kalo pilih component attribute)</i></strong>
    <ul>
        <li>Swimming: beach, fun, sun</li>
    </ul>
</div> -->
<form method="POST" action="{{ @$data['emailBuilder'] ? RouteHelper::token('template-builder.update', ['emailBuilder' => $data['emailBuilder']->id]) : RouteHelper::token('template-builder.save') }}" accept-charset="UTF-8" id="form-setting-template">
    <!-- <input name="_token" type="hidden" value="S4A4J0W5n04EohsEMXK9pLACuCes0rnLjLQwX85o"> -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="data_source">Data Source (<span class="text-red">*</span>) :</label>
                    {{
                        Form::select('data_source'
                            , array('' => 'Select Data Source') + $data['profilers']
                            , (@$data['emailBuilder'] ? $data['emailBuilder']->data_source : '')
                            , array(
                                'class' => 'form-control select2'
                                , 'required' => 'required'
                                , 'data-href' => RouteHelper::token('template-builder.detail')
                            )
                        )
                    }}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="individual_report">Individual Report (<span class="text-red">*</span>) :</label>
                    {{
                        Form::select(
                            'individual_report'
                            , array('' => 'Select Type', 'individual' => 'Individual', 'group' => 'Group')
                            , (@$data['emailBuilder'] ? $data['emailBuilder']->individual_report : '')
                            , array(
                                'class' => 'form-control select2'
                                , 'required' => 'required'
                                , 'data-href' => RouteHelper::token('template-builder.detail')
                            )
                        )
                    }}
                </div>
                <textarea name="content" class="hide"></textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Report Name (<span class="text-red">*</span>) :</label>
                    {{
                        Form::text(
                            'name'
                            , (@$data['emailBuilder'] ? $data['emailBuilder']->name : '')
                            , array(
                                'class' => 'form-control'
                                , 'required' => 'required'
                                , 'placeholder' => 'Report Name'
                            )
                        )
                    }}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="report_type">Report Type (<span class="text-red">*</span>) :</label>
                    {{
                        Form::select(
                            'report_type'
                            , array('' => 'Select Report Type', 'independent' => 'Independent', 'with observer' => 'With Observer')
                            , (@$data['emailBuilder'] ? $data['emailBuilder']->report_type : '')
                            , array(
                                'class' => 'form-control select2'
                                , 'required' => 'required'
                            )
                        )
                    }}
                </div>
            </div>
        </div>


        <!-- Start added by rdw k5 151217 -->
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="report_name">Logo (<span class="text-red">*</span>) :</label>
                    

                    <input id="fake_logo_url" class="form-control" placeholder="Cover Background" type="file" size="256KB image/*">
                    {{
                        Form::text(
                            'logo_url'
                            , (@$data['emailBuilder'] ? $data['emailBuilder']->logo_url : '')
                            , array('class' => 'hide')
                        )
                    }}
                    <br/>

                   
                    <img id="img_logo_url" src="{{ (@$data['emailBuilder']->logo_url ? $data['emailBuilder']->logo_url : '/profiler/img/noImage.jpg') }}" width="200">
                   <a href="#" id="delete_logo_url" class="btn btn-xs btn-danger">delete</a>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="report_desc">Report Description (<span class="text-red">*</span>) :</label>
                    {{
                        Form::textArea(
                            'report_desc'
                            , (@$data['emailBuilder'] ? $data['emailBuilder']->report_desc : '')
                            , array(
                                'class' => 'form-control ckeditor-selector'
                                , 'required' => 'required'
                                , 'placeholder' => 'Report Description'
                            )
                        )
                    }}
                </div>
            </div>
        </div>
        <!-- End added by rdw k5 151217 -->



        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="report_name">Report Code (<span class="text-red">*</span>) :</label>
                    {{
                        Form::text(
                            'report_name'
                            , (@$data['emailBuilder'] ? $data['emailBuilder']->report_name : '')
                            , array(
                                'class' => 'form-control'
                                , 'required' => 'required'
                                , 'placeholder' => 'Report Code'
                            )
                        )
                    }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="header_text">Header Text (<span class="text-red">*</span>) :</label>
                    {{
                        Form::textArea(
                            'header_text'
                            , (@$data['emailBuilder'] ? $data['emailBuilder']->header_text : '')
                            , array(
                                'class' => 'form-control ckeditor-selector'
                                , 'required' => 'required'
                                , 'placeholder' => 'Header Text'
                            )
                        )
                    }}
                </div>
            </div>
        </div>
        {{
            Form::text(
                'header_alignment'
                , (@$data['emailBuilder'] ? $data['emailBuilder']->header_alignment : 'Left')
                , array('class' => 'hide')
            )
        }}
        <!-- 
            komen by rdw k5 181217
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="header_alignment">Header Alignment (<span class="text-red">*</span>) :</label>
                    {{
                        Form::select(
                            'header_alignment'
                            , array('left' => 'Left', 'center' => 'Center', 'right' => 'Right')
                            , (@$data['emailBuilder'] ? $data['emailBuilder']->header_alignment : '')
                            , array(
                                'class' => 'form-control select2'
                                , 'required' => 'required'
                            )
                        )
                    }}
                </div>
            </div>
        </div> -->
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="footer_text">Footer Text (<span class="text-red">*</span>) :</label>
                    {{
                        Form::textArea(
                            'footer_text'
                            , (@$data['emailBuilder'] ? $data['emailBuilder']->footer_text : '')
                            , array(
                                'class' => 'form-control ckeditor-selector'
                                , 'required' => 'required'
                                , 'placeholder' => 'Footer Text'
                            )
                        )
                    }}
                </div>
            </div>
        </div>
        {{
            Form::text(
                'footer_alignment'
                , (@$data['emailBuilder'] ? $data['emailBuilder']->footer_alignment : 'Left')
                , array('class' => 'hide')
            )
        }}
        <!-- 
            Komen by rdw k5 181217
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="footer_alignment">Footer Alignment (<span class="text-red">*</span>) :</label>
                    {{
                        Form::select(
                            'footer_alignment'
                            , array('left' => 'Left', 'center' => 'Center', 'right' => 'Right')
                            , (@$data['emailBuilder'] ? $data['emailBuilder']->footer_alignment : '')
                            , array(
                                'class' => 'form-control select2'
                                , 'required' => 'required'
                            )
                        )
                    }}
                </div>
            </div>
        </div> -->
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="cover_style">Cover Style (<span class="text-red">*</span>) :</label>
                    <select class="form-control select2" name="cover_style" required>

                        @for($i = 1; $i < 5; $i++)
                            <option data-img-src="{{ url('image/cover'.$i.'.jpg') }}" value="style_{{ $i }}" {{ @$data['emailBuilder'] ? ($data['emailBuilder']->cover_style == 'style_'.$i ? 'selected' : '') : '' }}>Style {{ $i }}</option>
                        @endfor
                        <!-- comment by rdw k5 151617 
                            <option data-img-src="{{ url('image/starhub.jpg') }}" value="starhub" {{ @$data['emailBuilder'] ? ($data['emailBuilder']->cover_style == 'starhub' ? 'selected' : '') : '' }}>StarHub</option> -->
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="fake_cover_background">Cover Background :</label>

                    

                    <input id="fake_cover_background" class="form-control" placeholder="Cover Background" type="file" size="256KB image/*">
                    {{
                        Form::text(
                            'cover_background'
                            , (@$data['emailBuilder'] ? $data['emailBuilder']->cover_background : '')
                            , array('class' => 'hide')
                        )
                    }}
                    <br/>

                    <!-- added by rdw k5 151217 -->
                    
                    <img id="img_cover_background" src="{{ (@$data['emailBuilder']->cover_background ? $data['emailBuilder']->cover_background : '/profiler/img/noImage.jpg') }}" width="200">
                    <a href="#" id="delete_cover_background" class="btn btn-xs btn-danger">delete</a>
                    
                </div>
            </div>
        </div>
    </div>

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>