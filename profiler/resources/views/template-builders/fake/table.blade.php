<br><br><br><br><br>
<table  class="table-style-table">

	<thead>
		<tr>
			<td width="150"></td>
			<td style="text-align:center;padding:5px 10px;border:1px solid"><strong>Self</strong></td>
			<td style="text-align:center;padding:5px 10px;border:1px solid"><strong>Manager</strong></td>
			<td style="text-align:center;padding:5px 10px;border:1px solid"><strong>Direct Report</strong></td>
			<td style="text-align:center;padding:5px 10px;border:1px solid"><strong>Peer</strong></td>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td width="150"></td>
			<td style="text-align:center;padding:5px 10px;border:1px solid">S</td>
			<td style="text-align:center;padding:5px 10px;border:1px solid">M</td>
			<td style="text-align:center;padding:5px 10px;border:1px solid">D</td>
			<td style="text-align:center;padding:5px 10px;border:1px solid">P</td>
		</tr>

		<tr>
			<td style="padding:5px 10px;border:1px solid"><strong>Passion</strong></td>
			<td style="text-align:center;border:1px solid">0</td>
			<td style="text-align:center;border:1px solid">3</td>
			<td style="text-align:center;border:1px solid">4</td>
			<td style="text-align:center;border:1px solid">5</td>
		</tr>

		<tr>
			<td style="padding:5px 10px;border:1px solid"><strong>Empathy</strong></td>
			<td style="text-align:center;border:1px solid">0</td>
			<td style="text-align:center;border:1px solid">4</td>
			<td style="text-align:center;border:1px solid">2</td>
			<td style="text-align:center;border:1px solid">1</td>
		</tr>
	</tbody>

</table>