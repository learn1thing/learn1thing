<style type="text/css">
    @page { margin:0; size:A4; }
    body{
        margin:0;
        padding:0;
        font-family: 'Arial', sans-serif;
        height: 29.7cm;
        margin:auto;
        /*background-color: gray;*/
    }

    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Light.ttf);
        font-weight: 300;
    }


    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Regular.ttf);
        font-weight: 400;
    }

    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Medium.ttf);
        font-weight: 500;
    }

    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Bold.ttf);
        font-weight: 700;
    }

    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Black.ttf);
        font-weight: 900;
    }

    @font-face {
        font-family: ProximaNova;
        src: url('/fonts/proxima-nova-58f4b5b9b407b.otf');
    }

    .wrapper {
        margin:0;
        width: 100%;
        padding:0;
    }

    section {
        display: block;
        position: relative;
        width: 100%;
        height: 29.7cm;
        background-color:#ffffff;
        border-bottom:1px solid #000000;
    }

    .wrapper > section {
        background-image: url([report-background]);
    }

    footer{
        position: relative;
        bottom: -20px;
        font-family: 'Times New Roman';
        background-color: #73c20e;
        color: #fff;
        font-size: 11px;
        width: 100%;
        height: 70px;
        display: table;
        text-align: [report-footer-align];
    }

    footer span{
        display: table-cell;
        vertical-align: middle;
        padding: 20px;
    }

    footer span + span{
        text-align: center;
    }

    footer span + span + span{
        text-align: right;
    }

    .header-logo{
        background-image: url(https://storage.googleapis.com/learn1thing/starhub-img/StarHub_Inline_Green.jpg);
        background-repeat: no-repeat;
        background-size: 100% auto;
        width: 120px;
        height: 30px;
        position: absolute;
        right: 20px;
        bottom: 0;
    }

    header{
        display: block;
        height: 70px;
        width: 100%;
        position: relative;
        top:0;
        margin-bottom: 15px;
    }

    header hr{
        border: 1.5px solid #8bc83b;
        width: 71%;
        position: absolute;
        right: 143px;
        bottom: 0;
        margin:0;
    }

    header hr.gray{
        border: 1.5px solid #f3f3f3;
        left:0;
        bottom: 6px;
        margin-left: 0;
        z-index: 2;
    }

    header .title a{
        text-transform: uppercase;
        font-family: 'Times New Roman';
        color: #376092;
        font-size: 20px;
        font-weight: bold;
        padding: 20px 20px;
        position: absolute;
        text-decoration: none;
    }

    .content{
        width: 100%;
        display: block;
        position: relative;
        height: auto;
        top:0;
    }

    .content img.center-img{
        display: block;
        margin: auto;
    }

    span.highlights{
        color:#73c20e;
        font-size: 16px;
    }

    div.content-text{
        font-size: 12px;
        padding: 20px 50px 0px 40px;
        line-height: 18px;
    }

    .content-text ul{
        margin:3px;
        padding-left: 40px;
    }

    .content-text ul li{
        margin-top:3px;
    }

    .vc-top{
        vertical-align: top;
        padding-top:5px;
    }

    .padTop0{
        padding-top:0 !important;
    }

    div.content-boxed{
        margin: 20px 65px;
        border: 1.5px solid #e3dede;
        padding: 15px 0;
    }

    div.content-boxed div.content-text{
        padding-right:20px !important;
        text-align: center;
    }

    .comments-icon{
        width: 65px;
        height: 44px;
        display: inline-block;
        background-image: url(https://storage.googleapis.com/learn1thing/starhub-img/comments-icon.png);
        background-size: cover;
        vertical-align: middle;
    }

    .roadmap-icon{
        width: 47px;
        height: 50px;
        display: inline-block;
        background-image: url(https://storage.googleapis.com/learn1thing/starhub-img/roadmap-icon.png);
        background-size: cover;
        vertical-align: middle;
        margin-right: 5px;
    }

    .comments-title{
        margin-left: 10px;
        font-weight: bold;
        font-size: 21px;
    }

    .comments-content{
        font-family: 'Courier New';
        font-size: 11px;
        padding-left: 23px;
        padding-right: 30px;
        margin-top: 15px;
        line-height: 40px;
    }

    .roadmap-table{
        width: 97%;
        text-align: center;
        border-collapse: collapse;
        margin: 30px auto;
    }

    .roadmap-table table, .roadmap-table th, .roadmap-table td {
        border: 1px solid #73c20e;
    }

    .roadmap-table tr{
        height: 60px;
    }

    .roadmap-table th, .roadmap-table td{
        width: 33%;
    }

    .roadmap-table th{
        font-size: 13px;
    }

    .roadmap-table th img{
        width: 46px;
        vertical-align: middle;
    }

    div[data-edit="false"].exclude-margin {
        position: relative;
        margin-left: -15px !important;
    }

    div[data-edit="false"] {
        margin-right: 15px !important;
    }

    .observers-table{
      width: 95%;
      margin: auto;
      font-size: 12px;
      vertical-align: middle;
      min-width: 770px;
      border-collapse: separate;
      border-spacing: 0;
    }

    .observers-table tr{
      height: 22px;
      line-height: 16px;
    }

    .observers-table tr td{
      border-right: 1pt solid #ffffff;
      border-top: 1pt solid #ffffff;
      position: relative;
      padding-top: 2px;
      padding-bottom: 2px;
    }

    .observers-table tr.exclude-style:nth-child(2) td:nth-child(n+4), .observers-table tr.exclude-style:first-child td:last-child, .observers-table tr.exclude-style:first-child td:nth-last-child(2) {
        border: none;
    }

    .observers-table tr:nth-child(2) td:nth-child(n+4), .observers-table tr:first-child td:last-child{
      border-top: 1px solid #8bc83b;
      border-right: 1px solid #8bc83b;
      width: 90px;
    }

    .observers-table tr:first-child td:nth-last-child(2){
      border-right: 1px solid #8bc83b;
    }

    .observers-table tr:first-child td:last-child{
      color: #8bc83b;
      font-weight: bold;
      width: 340px;
    }

    .observers-table tr td:nth-child(n+3), .observers-table tr:nth-child(3) td, .observers-table tr:nth-child(2) td:nth-child(2){
      text-align: center;
    }

    .observers-table tr:nth-child(2) td:nth-child(2), .observers-table tr:nth-child(3) td:nth-child(1){
      background-color: #ff9019;
      width: 65px;
    }

    .observers-table tr:nth-child(2) td:nth-child(3){
      width: 65px;
    }

    .observers-table tr:nth-child(2) td:nth-child(3), .observers-table tr:nth-child(3) td:nth-child(n+2){
      background-color: #8bc83b;
    }

    .observers-table tr:nth-child(n+4):nth-child(even) td:nth-child(2){
      background-color: #7dc70e;
    }

    .observers-table tr:nth-child(4) td:nth-child(2) {
      border-top:0 !important;
    }

    .observers-table tr:nth-child(n+4):nth-child(odd) td:nth-child(2){
      background-color: #92e710;
    }

    .observers-table tr:nth-child(n+4):nth-child(even) td:nth-child(n+3){
      background-color: #d6f99f;
    }

    .observers-table tr:nth-child(n+4):nth-child(odd) td:nth-child(n+3){
      background-color: #c8f581;
    }

    .observers-table tr:nth-child(n+4) td:nth-child(1) {
      border-top: 1px solid #8bc83b;
      border-left: 1px solid #8bc83b;
      text-align: center;
    }

    .observers-table tr:last-child td:nth-child(1){
      border-bottom: 1px solid #8bc83b;
    }

    .observers-table tr:nth-child(n+4) td:nth-child(2) {
      padding-left: 21px;
    }

    .observers-table img.icon-ob{
      display: block;
      width: 25px;
      margin: auto;
      margin-top: 3px;
    }

    .arrow-observe{
      vertical-align: middle;
      width: 84px;
    }

    .arrow-observe.arrow-left{

    }

    .arrow-observe.arrow-right{
      -ms-transform: rotate(180deg); /* IE 9 */
      -webkit-transform: rotate(180deg); /* Chrome, Safari, Opera */
      transform: rotate(180deg);
    }

    .observers-text{
      width: 84px;
      display: inline-block;
    }

    .label-observe-table{
      font-size: 11px;
      font-style: italic;
      text-align: center;
      margin-top: 5px;
      line-height: 15px;
    }

    .badge-point{
      width: 19px;
      margin: 1px;
      vertical-align: middle;
    }

    .alphabet{
      position: absolute;
      left: 3px;
      top: 50%;
      margin-top: -8px;
    }

    .badge-tt{
      height: 60px;
      display: inline-block;
      vertical-align: middle;
    }
</style>

<div class="wrapper">
    <section>
        <div class="content">
            [change-with-element]
        </div>
    </section>
</div>