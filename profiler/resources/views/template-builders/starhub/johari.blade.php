<style type="text/css">
    .johari-img-wrap {
        position: relative;
    }

    .johari-img-wrap > span, .johari-img-wrap > img.badge-point{
        position: absolute;
        display: none;
    }

    .exclude-p-style p {
        margin: 0;
        padding: 0;
    }

    .y1 {
        display: block !important;
        bottom: 95px !important;
    }

    .y2 {
        display: block !important;
        bottom: 165px !important;
    }

    .y3 {
        display: block !important;
        bottom: 235px !important;
    }

    .y4 {
        display: block !important;
        bottom: 305px !important;
    }

    .y5 {
        display: block !important;
        bottom: 383px !important;
    }

    .y6 {
        display: block !important;
        bottom: 453px !important;
    }

    .x1 {
        display: block !important;
        left: 210px !important;
    }

    .x2 {
        display: block !important;
        left: 285px !important;
    }

    .x3 {
        display: block !important;
        left: 360px !important;
    }

    .x4 {
        display: block !important;
        left: 435px !important;
    }

    .x5 {
        display: block !important;
        left: 505px !important;
    }

    .x6 {
        display: block !important;
        left: 575px !important;
    }
</style>

@php (
    $image = array(
        'https://storage.googleapis.com/learn1thing/starhub-img/ico-triangle.png'
        , 'https://storage.googleapis.com/learn1thing/starhub-img/ico-hexagon.png'
        , 'https://storage.googleapis.com/learn1thing/starhub-img/ico-circle.png'
        , 'https://storage.googleapis.com/learn1thing/starhub-img/ico-square.png'
        , 'https://storage.googleapis.com/learn1thing/starhub-img/ico-star.png'
    )
)

@php ( $alphabet = range('A', 'Z') )

@foreach($attribute as $data)
    <!-- <section class="page7"> -->
        <!-- <div class="content"> -->
        <div>
            <div class="johari-img-wrap">
                <img src="https://storage.googleapis.com/learn1thing/starhub-img/johari.png" class="center-img" style="width: 435px;">
                @if(isset($dataPersonal))
                    @php ( $newIndex = 0 )
                    @foreach($dataPersonal as $index => $master)
                        @if($master->attribute_id == $data->id)
                            @php ($self = 1)
                            @foreach($attributeData[$data->id] as $key => $values)
                                @if($key == 'Self')
                                    @php ($self = isset($values[$master->question_id]) ? $values[$master->question_id] : 0)
                                @endif
                            @endforeach
                            @foreach($attributeData[$data->id] as $key => $values)
                                @if($key == 'Obs. Ave')
                                    @if($self > 0 && $values[$master->question_id] > 0)
                                        @if(isset($image[$newIndex]))
                                            <img src="{{ $image[$newIndex] }}" class="badge-point y{{ $self }} x{{ isset($values[$master->question_id]) ? $values[$master->question_id] : 0 }}" />
                                        @endif
                                        <!-- <span class="y{{ $self }} x{{ isset($values[$master->question_id]) ? $values[$master->question_id] : 0 }}">{{ $alphabet[$newIndex] }}</span> -->
                                    @endif
                                @endif
                            @endforeach

                            @php ( $newIndex += 1 )
                        @endif
                    @endforeach
                @endif
            </div>
            <br/>
            <table class="observers-table">
                <tr class="{{ count($attributeData[$data->id]) > 2 ? '' : 'exclude-style' }}">
                    <td></td><td></td><td></td><td></td>
                    @if(count($attributeData[$data->id]) > 2)
                        <td colspan="3">
                            <img src="https://storage.googleapis.com/learn1thing/starhub-img/arrow-left.png" class="arrow-observe arrow-left"/>
                            <span class="observers-text">Obervers</span>
                            <img src="https://storage.googleapis.com/learn1thing/starhub-img/arrow-right.png" class="arrow-observe arrow-right"/>
                        </td>
                    @endif
                </tr>
                <tr>
                    <td colspan="2" rowspan="2" class="comments-title">
                        <img src="https://storage.googleapis.com/learn1thing/starhub-img/badge-{{ strtolower($data->name) }}.png" class="badge-tt">
                        {{ $data->name }}
                    </td>
                    @foreach($attributeData[$data->id] as $key => $value)
                        <td>{{ $key }}</td>
                    @endforeach
                </tr>
                <tr>
                    @foreach($attributeData[$data->id] as $key => $value)
                        <td>{{ $key!='Obs. Ave' ? $key[0] : 'OA' }}</td>
                    @endforeach
                </tr>
                @if(isset($dataPersonal))
                    @php ( $newIndex = 0 )
                    @foreach($dataPersonal as $index => $master)
                        @if($master->attribute_id == $data->id)
                            <tr>
                                <td>
                                    @if(isset($image[$newIndex]))
                                        <img src="{{ $image[$newIndex] }}" class="badge-point" />
                                    @endif
                                </td>
                                <td class="exclude-p-style">
                                    <span class="alphabet">{{ $alphabet[$newIndex] }})</span>
                                    {!! $master->content !!}
                                </td>
                                @foreach($attributeData[$data->id] as $key => $values)
                                    <td>{{ isset($values[$master->question_id]) ? $values[$master->question_id] : 0 }}</td>
                                @endforeach

                                @php ( $newIndex += 1 )
                            </tr>
                        @endif
                    @endforeach
                @endif
            </table>
            <div class="label-observe-table">
                *The Observers Average (OA) rating is rounded to the nearest 1 decimal point.<br/>
                *Where there is only 1 assigned Direct Report, the associated ratings will only be captured in the Observer Average to ensure confidentiality.
            </div>
        </div>
    <!-- </section> -->
    <!-- <section class="page8">
        <header>
            <div class="title">
                SECTION 1 – 5E2P LEADERSHIP BEHAVIOURS RATING
            </div>
            <div class="header-logo">
            </div>
            <hr class="gray" />
            <hr/>
        </header>
        <div class="content">
            <div class="comments-title"><span class="comments-icon"></span>  EMPATHY: Comments</div>
            <div class="comments-content">
                dsgp93@%$&%^*&(&*()(mfsp <br/>
                fhop[i-i-32kjm43pi-439o-49o6-430430o6=-46-460
            </div>
        </div>
        <footer>
            <span>
                StarHub 5E2P Leadership Behaviours 360 Profiler
            </span>
            <span>
                8
            </span>
            <span>
                © 2016 learn1thing.<br/>
                © ODE Consulting Pte. Ltd. 2016. All rights reserved.
            </span>
        </footer>
    </section> -->
@endforeach