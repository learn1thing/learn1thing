<div style="text-align: [report-footer-align]">
    [report-footer]
    <footer>
        <span>
            [survey-title]
        </span>
        <span>[page-number]</span>
        <span>
            &copy; 2016 learn1thing.<br/>
            &copy; ODE Consulting Pte. Ltd. 2016. All rights reserved.
        </span>
    </footer>
</div>
<div class="clearfix"></div>