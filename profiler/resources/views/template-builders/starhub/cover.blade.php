<style type="text/css">
    @page { margin:0; size:A4; }
    body{
        margin:0;
        padding:0;
        font-family: 'Arial', sans-serif;
        height: 29.7cm;
        margin:auto;
        background-color: gray;
    }

    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Light.ttf);
        font-weight: 300;
    }


    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Regular.ttf);
        font-weight: 400;
    }

    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Medium.ttf);
        font-weight: 500;
    }

    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Bold.ttf);
        font-weight: 700;
    }

    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Black.ttf);
        font-weight: 900;
    }

    @font-face {
        font-family: ProximaNova;
        src: url('/fonts/proxima-nova-58f4b5b9b407b.otf');
    }

    .wrapper-starhub {
        margin:0;
        width: 100%;
        padding:0;
    }

    section {
        display: block;
        position: relative;
        width: 100%;
        height: 29.7cm;
        background-color:#ffffff;
        border-bottom:1px solid #000000;
    }

    section.cover img.logo{
        width: 230px;
        margin: 5px 5px 50px;
    }

    section.cover h1.title{
        font-size: 40px;
        color: #73c20e;
        text-align: right;
        margin: 0 20px 10px;
    }

    section.cover h5{
        font-size: 16px;
    }

    section.cover h5.prepared{
        color: #666666;
        text-align: right;
        margin: 10px 20px;
        font-weight: normal;
    }

    section.cover .powered-by{
        font-size: 16px;
        text-align: center;
        margin-top: 160px;
    }

    section.cover .powered-by img{
        width: 140px;
        vertical-align: middle;
        margin-left: 15px;
    }
</style>

<div class="wrapper-starhub">
    <section class="cover">
        <img src="https://storage.googleapis.com/learn1thing/starhub-img/StarHub_Inline_Green.jpg" class="logo"/>
        <img src="https://storage.googleapis.com/learn1thing/starhub-img/banner.png" width="100%"/>
        <h1 class="title">[survey-title]</h1><h1 class="title">[survey-type]</h1>
        <h5 class="prepared"><b>Prepared for:</b> [product-purchaser] | [survey-period_start-flat]</h5>
        <div class="powered-by">
            <span>Powered By:</span><img src="https://storage.googleapis.com/learn1thing/starhub-img/Learn1Thing.jpg"/>
        </div>
    </section>
</div>