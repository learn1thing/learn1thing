<style type="text/css">
    .johari-article-johari-detail{
    /*padding: 50px 30px;*/
    color: #001e61;
    font-size: 0.95em;
    page-break-after: always;
    }

    .johari-article-johari-detail .h3{
    font-weight: 500;
    font-size: 1.2em;
    display: block;
    }

    .johari-article-johari-detail p{
    margin-bottom: 30px;
    margin-top:0;
    }

    .two-column-detail{
    width: 100%;
    float: none;
    max-width: 740px;
    margin: auto;
    }

    .two-column-detail + div + .two-column-detail {
    margin-top: 30px;
    }

    .two-column-detail > div{
    width: 35%;
    float: left;
    padding: 1% 1% 1% 70px;
    }

    .two-column-detail > div:first-of-type{
    height: 400px;
    background: url('https://storage.googleapis.com/learn1thing/img/johari.png');
    background-size: 340px;
    background-repeat: no-repeat;
    /*background-position: 60px 10px;*/
    background-position: -10px 0px;
    position: relative;
    }

    .two-column-detail > div + div{
    width: 43%;
    float: left;
    padding: 1% 0;
    }

    .two-column-detail dl
    {
    width: 200px;
    background: #fff;
    border: 1px solid #9BCDE8;
    padding: 5px 15px;
    border-radius: 5px;
    }

    .two-column-detail dt, .two-column-detail dd
    {
    display: inline;
    margin: 0;
    }

    .two-column-detail > div:first-of-type > span{
    position: absolute;
    bottom: 105px;
    left: 45px;
    }

    .y2 {
    bottom: 160px !important;
    }

    .y3 {
    bottom: 227px !important;
    }

    .y4 {
    bottom: 290px !important;
    }

    .y5 {
    bottom: 348px !important;
    }

    .x2 {
    left: 106px !important;
    }

    .x3 {
    left: 168px !important;
    }

    .x4 {
    left: 235px !important;
    }

    .x5 {
    left: 290px !important;
    }

    
    @for ($i = 10; $i <= 50; $i++)
        .x{{ $i}}{
            left: {{ 75+(($i-10)*4.9) }}px !important;
        }
    @endfor

    @for ($i = 10; $i <= 50; $i++)
        .y{{ $i}}{
            bottom: {{ 135+(($i-10)*4.9) }}px !important;
        }
    @endfor

    .class-M {
        color: red !important;
    }

    .class-D {
        color: blue !important;
    }

    .class-P {
        color: #ffd400 !important;
    }

    .class-A {
        color: green !important;
    }
    .shape-johari-P{
       display: inline-block;
       width: 10px;
       height: 10px;
       z-index: -1;
       content:url("{!! url('/img/johari_peer.png') !!}");
    }
    .shape-johari-A{
       display: inline-block;
       width: 10px;
       height: 10px;
       z-index: 2;
       content:url("{!! url('/img/johari_average.png') !!}");
    }
    .shape-johari-D{
       display: inline-block;
       width: 10px;
       height: 10px;
       z-index: -2;
       content:url("{!! url('/img/johari_direktur.png') !!}");
    }
    .shape-johari-M{
       display: inline-block;
       width: 10px;
       height: 10px;
       z-index: 1;
       content:url("{!! url('/img/johari_manager.png') !!}");
    }
    .shape-johari-S{
       display: inline-block;
       width: 10px;
       height: 10px;
       content:url("{!! url('/img/johari_self.png') !!}");
    }


</style>

<div class="johari-article-johari-detail">
    @foreach($question as $data)
        @php($self = isset($questionData[$data->id]['Self']) ? $questionData[$data->id]['Self'] : 1)
        @php($average=0)
        @php($jk=0)
        <div class="two-column-detail">
            <div>
                @foreach($questionData[$data->id] as $key => $value)
                    @if($key != 'Self')
                        @php($jk++ )
                        @php($average+=round($value,1))
                        <span class="y{{ $self*10 }} x{{ round($value,1)*10 }}"><img class="shape-johari-{{ $key[0] }}"></span>
                    @endif
                @endforeach

                @php($average=($average/$jk))
                <span class="y{{ $self*10 }} x{{ round($average,1)*10 }}"><img class="shape-johari-A"></span>

                {{-- <span class="y10 x10">S</span>
                <span class="y20 x20">Q</span>
                <span class="y3 x30">E</span>
                <span class="y40 x40">x</span>
                <span class="y50 x50">H</span> --}}
            </div>
            <div>
                <p style="text-size-adjust: 100%;">
                    <span class="h3">{{ $data->name }}</span>
                    {!! $data->content !!}
                </p>
                <p>
                    <dl>
                        @foreach($questionData[$data->id] as $key => $value)
                            <dt>
                                <img class="shape-johari-{{ $key[0] }}"><span> {{ $key[0] }}</span>{{ substr($key, 1) }} :
                            </dt>
                            <dd> {{ $value }} </dd>
                            <br/>
                        @endforeach
                        <dt>
                            <img class="shape-johari-A"><span> </span>Average :
                        </dt>
                        <dd>
                            {{ round($average,2) }}
                        </dd><br/>
                    </dl>
                </p>
            </div>
        </div>
        <div style="clear: both"></div>
    @endforeach
</div>