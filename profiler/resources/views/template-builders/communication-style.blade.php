<style type="text/css">
	.com-style-h2 {
		font-style: italic;
		font-size: 1em;
	}

	.com-style-wrapper {
		height: auto;
	}

	.com-style-container {
		height: auto;
	}

	.com-style-table {
		width: 100%;
		font-size: .6em;
	}

	.com-style-table thead {
		background-color: #035AA4;
		color: white;
	}

	.com-style-table th, .com-style-table td {
		padding: 5px;
		text-align: center;
	}

	.com-style-table tbody tr:nth-child(odd) td {
		background-color: #efefef;
	}

	.com-style-table tbody tr:nth-child(odd) td:nth-child(1) {
		background-color: #86cfea;
	}

	.com-style-table tbody tr:nth-child(even) td {
		background-color: #f5f5f5;
	}

	.com-style-table tbody tr:nth-child(even) td:nth-child(1) {
		background-color: #4dbae2;
	}

	.com-style-table tfoot tr td {
		font-weight: bold;
	}

	.com-style-container {
		background-color: white;
	}

	.com-style-johari-table {
		background-image: url("{{ asset('https://storage.googleapis.com/learn1thing/repport-assets/Communication-table.png') }}");
		width: 500px;
	    height: 500px;
	    background-position: center;
	    background-repeat: no-repeat;
	    background-size: 500px 500px;
	    text-align: center;
	    display: inline-block;
	    position: relative;
	    margin-left: 70px;
	}

	.com-style-johari-table > div {
		/*border: 1px solid;*/
	    position: absolute;
	    top: 60px;
	    left: 40px;
	    width: 185px;
	    height: 155px;
	    text-align: center;
	    font-size: .7em;
	    padding: 10px;
	}

	.com-style-johari-table > .com-style-energic {
	    left: 252px;
	}

	.com-style-johari-table > .com-style-rational {
	    top: 280px;
	}

	.com-style-johari-table > .com-style-results {
	    top: 280px;
	    left: 252px;
	}
</style>

<div class="com-style-wrapper">
	<div class="container com-style-container">
		<h2 class="com-style-h2">How You Responded:</h2>
		<table class="com-style-table">
			<thead>
				<th></th>
				<th>RESULTS ORIENTED</th>
				<th>RATIONAL</th>
				<th>EMPATHETIC</th>
				<th>ENERGETIC</th>
			</thead>
			<tbody>
				@php (
					$total = array(
						'RESULTS ORIENTED' => 0
						, 'RATIONAL' => 0
						, 'EMPATHETIC' => 0
						, 'ENERGETIC' => 0
					)
				)

				@foreach($questions as $index => $question)
					@php ($searchValue = array_search($question['answers']['answer'], $question['answer_configs']))
					<tr>
						 
						{{--<td>Question {{ $index+1 }}</td>--}}
						<td>{{ $question['name'] }}</td>

						<td>
							@if( is_array($question['answer_attributes'][$searchValue]) )
								@if( $attributes[$question['answer_attributes'][$searchValue][0]] == "RESULTS ORIENTED")
									X
									@php ($total['RESULTS ORIENTED'] += 1)
								@else
									-
								@endif
							@else
								@if( $attributes[$question['answer_attributes'][$searchValue]] == "RESULTS ORIENTED")
									X
									@php ($total['RESULTS ORIENTED'] += 1)
								@else
									-
								@endif
							@endif
						</td>
						<td>
							@if( is_array($question['answer_attributes'][$searchValue]) )
								@if( $attributes[$question['answer_attributes'][$searchValue][0]] == "RATIONAL")
									X
									@php ($total['RATIONAL'] += 1)
								@else
									-
								@endif
							@else
								@if( $attributes[$question['answer_attributes'][$searchValue]] == "RATIONAL")
									X
									@php ($total['RATIONAL'] += 1)
								@else
									-
								@endif
							@endif
						</td>
						<td>
							@if( is_array($question['answer_attributes'][$searchValue]) )
								@if( $attributes[$question['answer_attributes'][$searchValue][0]] == "EMPATHETIC")
									X
									@php ($total['EMPATHETIC'] += 1)
								@else
									-
								@endif
							@else
								@if( $attributes[$question['answer_attributes'][$searchValue]] == "EMPATHETIC")
									X
									@php ($total['EMPATHETIC'] += 1)
								@else
									-
								@endif
							@endif
						</td>
						<td>
							@if( is_array($question['answer_attributes'][$searchValue]) )
								@if( $attributes[$question['answer_attributes'][$searchValue][0]] == "ENERGETIC")
									X
									@php ($total['ENERGETIC'] += 1)
								@else
									-
								@endif
							@else
								@if( $attributes[$question['answer_attributes'][$searchValue]] == "ENERGETIC")
									X
									@php ($total['ENERGETIC'] += 1)
								@else
									-
								@endif
							@endif
						</td>
						<!-- <td>{{ print_r($question) }}</td> -->
						<!-- <td>{{ $searchValue }}</td> -->
						<!-- <td>{{ print_r($question['answer_attributes'][$searchValue][0]) }}</td> -->
					</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<td>Total</td>
					<td>{{ $total['RESULTS ORIENTED'] }}</td>
					<td>{{ $total['RATIONAL'] }}</td>
					<td>{{ $total['EMPATHETIC'] }}</td>
					<td>{{ $total['ENERGETIC'] }}</td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
@php ( arsort($total) )
@php ( $index = 0 )
@php ( $isExit = false )

@foreach($total as $id => $val)
	@if ($val > 0)
		@if ($index == 0)
			@php ( $baseData = $val )
			@php ( $baseId = $id )
			@php ( $total[$id] = 'Primary Skill' )
		
		@else
			@if ( $val > $baseData )
				@php ( $baseData = $val )
				@php ( $baseId = $id )
				@php ( $total[$id] = 'Primary Skill' )
			@elseif ( $val == $baseData )
				@php ( $total[$id] = $total[$baseId] )
			@else
				@if ( $total[$baseId] == 'Secondary Skill' || $total[$baseId] == '' )
					@php ( $total[$id] = '' )
				@else
					@php ( $total[$id] = 'Secondary Skill' )
				@endif
				@php ( $baseData = $val )
				@php ( $baseId = $id )
			@endif

		@endif

		@php ( $index += 1 )
	@else
		@php ( $total[$id] = '' )
	@endif
@endforeach

<code>[new-page]</code>

<div class="com-style-wrapper">
	<div class="container com-style-container">
		<h2 class="com-style-h2">Your Communication Style:</h2>
		<div class="com-style-johari-table">
			<div class="com-style-empathy">{{ $total['EMPATHETIC'] }}</div>
			<div class="com-style-energic">{{ $total['ENERGETIC'] }}</div>
			<div class="com-style-rational">{{ $total['RATIONAL'] }}</div>
			<div class="com-style-results">{{ $total['RESULTS ORIENTED'] }}</div>
		</div>

		<h2 class="com-style-h2">Interpretation:</h2>

		<table class="com-style-table">
			<thead>
				<th>Communication Style</th>
				<th>Your strengths would be...</th>
				<th>Your weaknesses would be...</th>
			</thead>
			<tbody>
				<tr>
					<td>RESULTS ORIENTED</td>
					<td>Determined Objective</td>
					<td>Domineering Unfeeling</td>
				</tr>
				<tr>
					<td>RATIONAL</td>
					<td>Precise Systematic</td>
					<td>Nit-picking Inflexible</td>
				</tr>
				<tr>
					<td>EMPATHETIC</td>
					<td>Supportive Easygoing</td>
					<td>Avoids conflicts Permissive</td>
				</tr>
				<tr>
					<td>ENERGETIC</td>
					<td>Enthusiastic Imaginative</td>
					<td>Overbearing Unrealistic</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<code>[new-page]</code>

<div class="com-style-wrapper">
	<div class="container com-style-container">
		<h2 class="com-style-h2">Communication Style Descriptors:</h2>
		<div class="com-style-johari-table">
			<div class="com-style-empathy">
				(Co-operative, Helpful & Caring,
				Supportive, Diplomatic, Patient, Loyal,
				Sensitive, Sympathetic, Understanding,
				Trusting, Patient, Thorough & Tolerant,
				"Champion" of Others, Team Player, Good
				Listener, Makes a Point to Acknowledge
				People)
			</div>
			<div class="com-style-energic">
				(Outgoing, Fun Loving, Animated,
				Spontaneous, Persuasive , Enthusiastic,
				Look at the Big Picture, Likes New Ideas,
				Highly Creative, Look at Fresh/Novel
				Approaches, Take Risks to Seize
				Opportunities, Thrives Socially, Highly
				intuitive)
			</div>
			<div class="com-style-rational">
				(Logical, Thorough, Serious, Systematic,
				Prudent, Industrious, Objective & Wellorganised,
				Careful Decision Makers,
				Appreciates Rules, Good in Follow-up &
				Follow-through, Structured, Expert
				Analysts )
			</div>
			<div class="com-style-results">
				(Task-oriented, Independent , Candid,
				Decisive, Assertive, Possibility Thinkers,
				Hard Workers, Creative, Goal-oriented,
				Practical, Efficient, Competitive, Take
				Sound Risks, Fast-paced, Get Things
				Done!)
			</div>
		</div>
	</div>
</div>
<code>[new-page]</code>

<div class="com-style-wrapper">
	<div class="container com-style-container">
		<h2 class="com-style-h2">Overview of the Four Communication Styles:</h2>

		<table class="com-style-table">
			<thead>
				<th>Social Style</th>
				<th>Rational</th>
				<th>Results-oriented</th>
				<th>Empathetic</th>
				<th>Energetic</th>
			</thead>
			<tbody>
				<tr>
					<td>Reaction</td>
					<td>Slow</td>
					<td>Swift</td>
					<td>Unhurried</td>
					<td>Rapid</td>
				</tr>
				<tr>
					<td>Orientation</td>
					<td>Thinking & Fast</td>
					<td>Action & Goal</td>
					<td>Relationship & Peace</td>
					<td>Involvement & Intuition</td>
				</tr>
				<tr>
					<td>Likes</td>
					<td>Organisation</td>
					<td>To be in Charge</td>
					<td>Close Relationship</td>
					<td>Much Interaction</td>
				</tr>
				<tr>
					<td>Dislikes</td>
					<td>Involvement</td>
					<td>Inaction</td>
					<td>Conflict</td>
					<td>To be Alone</td>
				</tr>
				<tr>
					<td>Maximum Effort</td>
					<td>To Organise</td>
					<td>To Control</td>
					<td>To Relate</td>
					<td>To Involve</td>
				</tr>
				<tr>
					<td>Minimum Effort</td>
					<td>For Relationships</td>
					<td>For Relationships</td>
					<td>For Affecting</td>
					<td>Change For Routine</td>
				</tr>
				<tr>
					<td>Actions</td>
					<td>Cautions</td>
					<td>Decisive</td>
					<td>Slow</td>
					<td>Impulsive</td>
				</tr>
				<tr>
					<td>Skills</td>
					<td>Good Problem-solving Skills</td>
					<td>Good Administrative Skills</td>
					<td>Good Counseling Skills</td>
					<td>Good Persuasive Skills</td>
				</tr>
				<tr>
					<td>Decision Making</td>
					<td>Avoid Risks Based on Facts</td>
					<td>Takes Risks Based on Intuition</td>
					<td>Avoids Risk Based on Opinions</td>
					<td>Takes Risks Based on Hunches</td>
				</tr>
				<tr>
					<td>Timeframe</td>
					<td>Historical</td>
					<td>Present</td>
					<td>Present</td>
					<td>Future</td>
				</tr>
				<tr>
					<td>Use of Time</td>
					<td>Slow, Deliberate, Disciplined</td>
					<td>Swift, Efficient, Impatient</td>
					<td>Slow, Calm, Undisciplined</td>
					<td>Rapid, Quick, Undisciplined</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>