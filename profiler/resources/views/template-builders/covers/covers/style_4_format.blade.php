  <style type="text/css">
    @page { margin:0; size:A4; }
    body{
      margin:0;
      padding:0;
      font-family: 'Arial', sans-serif;
      height: 29.7cm;
      margin:auto;
      background-color: gray;
    }

    section.section-wrapper{
      display: block;
      position: relative;
      width: 100%;
      height: 29.7cm;
      background-color:#ffffff;
      border-bottom:1px solid #000000;
      overflow:hidden;
    }

    footer{
      position: absolute;
      bottom: 0;
      font-family: 'Times New Roman';
      background-color: #14b35d;
      color: #fff;
      font-size: 11px;
      width: 100%;
      height: 70px;
      display: table;
    }

    .header-logo img{
      width: 90px;
      margin: auto;
      position: relative;
      z-index: 111;
      top: 56px;
      display: block;
      left: -16px;
      -ms-transform: rotate(4deg); /* IE 9 */
      -webkit-transform: rotate(4deg); /* Chrome, Safari, Opera */
      transform: rotate(4deg);
    }

    .header-logo{
      width: 190px;
      height: 150%;
      position: absolute;
    }

    section > header{
      display: block;
      height: 130px;
      width: 110%;
      position: absolute;
      top: -50px;
      left: -10px;
      background: rgb(22,188,83);
      background: -moz-linear-gradient(left, rgba(22,188,83,1) 0%, rgba(0,93,175,1) 78%);
      background: -webkit-linear-gradient(left, rgba(22,188,83,1) 0%,rgba(0,93,175,1) 78%);
      background: linear-gradient(to right, rgba(22,188,83,1) 0%,rgba(0,93,175,1) 78%);
      filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#16bc53', endColorstr='#005daf',GradientType=1 );
      -ms-transform: rotate(-4deg); /* IE 9 */
      -webkit-transform: rotate(-4deg); /* Chrome, Safari, Opera */
      transform: rotate(-4deg);
      border-bottom: 4px solid #dacc37;
    }

    hr.gray{
      top: 79px;
      position: absolute;
      width: 100%;
      margin: 0;
      padding: 0;
      border-top: none;
      border-bottom: 5px solid #dacc37;
    }

    section > header .title-base{
      text-transform: uppercase;
      color: #ffffff;
      font-size: 18px;
      font-weight: bold;
      padding: 25px 0;
      position: absolute;
      left: 0;
      width: 100%;
      top: 58px;
      text-align: center;background: rgb(22,188,83);
      background: -moz-linear-gradient(left, rgba(22,188,83,1) 0%, rgba(0,93,175,1) 78%);
      background: -webkit-linear-gradient(left, rgba(22,188,83,1) 0%,rgba(0,93,175,1) 78%);
      background: linear-gradient(to right, rgba(22,188,83,1) 0%,rgba(0,93,175,1) 78%);
      filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#16bc53', endColorstr='#005daf',GradientType=1 );
      -ms-transform: rotate(4deg);
      -webkit-transform: rotate(4deg);
      transform: rotate(4deg);
      z-index: 2;
      padding-left: 160px;
      min-height: 27px;
    }

    .post-image{
      text-align: center;
    }

    .post-image img{
      width: 570px;
    }

    .content-wrapper{
      position: relative;
      /*margin-top:110px;*/
      top: 110px;
    }

    article.article-content{
      font-size: 1em;
      padding: 20px 40px;
      color: #202020;
    }

    .footer-content{
      color: #ffffff;
      text-align: right;
      width: 60%;
      padding-right: 30px;
      position: absolute;
      font-size: 0.5em;
      font-weight: 300;
      right: 0;
      padding-top: 10px;
    }

    .bold {
      font-weight: 700;
    }

    footer .contacts{
      font-size: 1.5em;
      font-weight: 400;
      margin-top: 11px;
    }

    footer .contacts > span{
      margin-left: 20px;
    }

    footer .contacts > span .icon img{
      width: 20px;
      vertical-align: middle;
      margin-right: 5px;
    }


    .l1t-logo-container{
      height: 90px;
      position: absolute;
      overflow: hidden;
      width: 223px;
    }

    .l1t-logo{
      background-image: url(https://storage.googleapis.com/learn1thing/img/l1t-logo.png);
      height: 73px;
      position: absolute;
      width: 128px;
      background-size: 105px;
      background-position: center center;
      background-repeat: no-repeat;
    }

    .l1t-logo.l1t-logo-small{
      background-size: 80px;
    }

    .force-float {
        float: none !important;
        width: 100% !important;
    }

    @page {
        @bottom {
            content: flow(footer);
            vertical-align: top;
            margin-top: -70px;
        }
    }
    footer {
        flow: static(footer);
    }
</style>

<section class="section-wrapper" style="background-image: url([report-background]); background-size: 100%">
    <hr class="gray" />
    <header>
      <div class="title-base" style="text-align: [report-header-align]">
        [report-header]
      </div>
      <div class="header-logo">
        <img src="https://storage.googleapis.com/learn1thing/img/brand-logo-white.png"/>
      </div>
    </header>
    <div class="content-wrapper">
      <article class="article-content">
        [change-with-element]

      </article>
    </div>
    <footer>
      <div class="l1t-logo-container">
        <div class="shape-1">
        </div>
        <div class="l1t-logo l1t-logo-small">
        </div>
      </div>
      <div class="footer-content" style="text-align: [report-footer-align]">
        [report-footer]

        <div class="contacts">
          <span><span class="icon"><img src="https://storage.googleapis.com/learn1thing/img/document-icon.png"/></span>store.learn1thing.com</span><span><span class="icon"><img src="https://storage.googleapis.com/learn1thing/img/phone-icon.png"/></span>+65 63238022</span>
        </div>
      </div>
    </footer>
</section>