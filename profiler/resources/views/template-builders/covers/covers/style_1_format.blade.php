<style type="text/css">
    @page { margin:0; size:A4; }
    body{
      margin:0;
      padding:0;
      font-family: 'Arial', sans-serif;
      height: 29.7cm;
      margin:auto;
      background-color: gray;
    }

    section.section-wrapper{
      display: block;
      position: relative;
      width: 100%;
      height: 29.7cm;
      background-color:#ffffff;
      border-bottom:1px solid #000000;
      overflow:hidden;
    }

    footer{
      position: absolute;
      bottom: 0;
      font-family: 'Times New Roman';
      background-color: #136de1;
      color: #fff;
      font-size: 11px;
      width: 100%;
      height: 70px;
      display: table;
    }

    footer span.paging:nth-child(2){
      text-align: center;
      background-color: #ffffff;
      position: absolute;
      top: -10px;
      width: 20px;
      padding: 35px 0px 5px;
      height: 40px;
      left:160px;
    }

    footer span.paging:nth-child(2)::after{
      content: "";
      height: 0;
      position: absolute;
      right: 19px;
      width: 0;
      top: 0;
      border-right: 0 solid transparent;
      border-top: 80px solid #ffffff;
      border-left: 35px solid transparent;
      border-bottom: 140px solid transparent;
      z-index: 2;
    }

    footer span.paging:nth-child(2)::before{
      content: "";
      height: 0;
      position: absolute;
      right: -35px;
      width: 0;
      top: 0;
      border-right: 0 solid #ffffff;
      border-top: 80px solid transparent;
      border-left: 35px solid #ffffff;
      border-bottom: 0 solid transparent;
      z-index: 2;
    }

/*    footer span+span+span{
      text-align: right;
      background-color: #009d77;
      top:0;
    }*/

    .header-logo{
      background-image: url(https://storage.googleapis.com/learn1thing/img/brand-logo-white.png);
      background-repeat: no-repeat;
      background-size: 110px auto;
      background-position: 65px center;
      background-color: #001e61;
      width: 190px;
      height: 150%;
      position: absolute;
      left: 0;
      top: 0;
      z-index: 3;
    }

    .header-logo::after{
      content: "";
      height: 0;
      position: absolute;
      right: -1px;
      width: 0;
      top: 0;
      border-right: 25px solid #fff;
      border-top: 0 solid transparent;
      border-left: 0 solid #fff;
      border-bottom: 40px solid transparent;
      z-index: 2;
    }

    .header-logo::before{
      content: "";
      height: 0;
      position: absolute;
      left: 0;
      width: 0;
      top: 0;
      border-right: 73px solid transparent;
      border-top: 0 solid #ffffff;
      border-left: 0 solid transparent;
      border-bottom: 110px solid #ffffff;
      z-index: 2;
    }

    section > header{
      display: block;
      height: 70px;
      width: 100%;
      position: absolute;
      top: 0;
      background-color: #ffffff;
    }

    section > header hr{
      /*border: 1.5px solid #8bc83b;*/
      border: none;
      width: 100%;
      position: absolute;
      right: 0;
      bottom: 0;
      margin: 0;
    }

    section > header hr.gray{
      /*border: 1.5px solid #f3f3f3;*/
      border: none;
      left:0;
      bottom: 6px;
      margin-left: 0;
      z-index: 2;
    }

    section > header .title-base{
      text-transform: uppercase;
      font-family: 'Times New Roman';
      color: #ffffff;
      font-size: 18px;
      font-weight: bold;
      padding: 22px 0;
      padding-left: 60px;
      position: absolute;
      left: 0;
      width: 100%;
      top: 40px;
      text-align: center;
      background-color: #136de1;
    }

    section > header .title-base::before{
      left: 190px;
      content: "";
      height: 0;
      position: absolute;
      right: 0;
      width: 0;
      top: 0;
      border-right: 40px solid transparent;
      border-top: 0 solid transparent;
      border-left: 0 solid #fff;
      border-bottom: 65px solid #001e61;
      z-index: 2;
    }

    .post-image{
      text-align: center;
    }

    .post-image img{
      width: 570px;
    }

    .content-wrapper{
      position: relative;
      /*margin-top:110px;*/
      top: 110px;
    }

    article.article-content{
      font-size: 1em;
      padding: 20px 40px;
      color: #202020;
    }

    .footer-content{
      color: #ffffff;
      text-align: right;
      width: 60%;
      padding-right: 30px;
      position: absolute;
      font-size: 0.5em;
      font-weight: 300;
      right: 0;
      padding-top: 10px;
    }

    .bold {
      font-weight: 700;
    }

    footer .contacts{
      font-size: 1.5em;
      font-weight: 400;
      margin-top: 11px;
    }

    footer .contacts > span{
      margin-left: 20px;
    }

    footer .contacts > span .icon img{
      width: 20px;
      vertical-align: middle;
      margin-right: 5px;
    }


    .l1t-logo-container{
      height: 90px;
      position: absolute;
      overflow: hidden;
      width: 223px;
    }

    .l1t-logo{
      background-image: url(https://storage.googleapis.com/learn1thing/img/l1t-logo.png);
      height: 73px;
      position: absolute;
      width: 128px;
      background-size: 105px;
      background-position: center center;
      background-repeat: no-repeat;
    }

    .l1t-logo.l1t-logo-small{
      background-size: 80px;
    }

    footer {
        margin-left: -15px;
        page-break-after: always;
    }

    .force-float {
        float: none !important;
        width: 100% !important;
    }

    @page {
        @bottom {
            content: flow(footer);
            vertical-align: top;
            margin-top: -70px;
        }
    }
    footer {
        flow: static(footer);
    }
</style>

<section class="section-wrapper" style="background-image: url([report-background]); background-size: 100%">
    <header>
      <div class="title-base" style="text-align: [report-header-align]">
        [report-header]
      </div>
      <div class="header-logo">
      </div>
      <hr class="gray" />
      <hr/>
    </header>
    <div class="content-wrapper">
      <article class="article-content">
        [change-with-element]
      </article>
    </div>
</section>