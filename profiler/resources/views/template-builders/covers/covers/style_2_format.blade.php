  <style type="text/css">
    @page { margin:0; size:A4; }
    body{
      margin:0;
      padding:0;
      font-family: 'Arial', sans-serif;
      height: 29.7cm;
      margin:auto;
      background-color: gray;
    }

    section.section-wrapper{
      display: block;
      position: relative;
      width: 100%;
      height: 29.7cm;
      background-color:#ffffff;
      border-bottom:1px solid #000000;
      overflow:hidden;
    }

    footer{
      position: absolute;
      bottom: 0;
      font-family: 'Times New Roman';
      background-color: #053750;
      color: #fff;
      font-size: 11px;
      width: 100%;
      height: 70px;
      display: table;
    }

    footer span.paging:nth-child(2){
      text-align: center;
      background-color: #e8eced;
      position: absolute;
      top: 0;
      width: 20px;
      padding: 35px 0px 5px;
      height: 70px;
      left:120px;
    }

    footer span.paging:nth-child(2)::after{
      content: "";
      height: 0;
      position: absolute;
      right: -15px;
      width: 0;
      top: 0;
      border-right: 0 solid #053750;
      border-top: 35px solid transparent;
      border-left: 35px solid #053750;
      border-bottom: 35px solid transparent;
      z-index: 2;
    }

    footer span.paging:nth-child(2)::before{
      content: "";
      height: 0;
      position: absolute;
      right: -35px;
      width: 0;
      top: 0;
      border-right: 0 solid #ffffff;
      border-top: 35px solid transparent;
      border-left: 35px solid #e8eced;
      border-bottom: 35px solid transparent;
      z-index: 2;
    }

    .header-logo img{
      width: 90px;
      margin: auto;
      position: relative;
      z-index: 111;
      top: 50px;
      display: block;
      left: -16px;
    }

    .header-logo{
      width: 190px;
      height: 150%;
      position: absolute;
    }

    .header-logo::after{
      content: "";
      height: 0;
      position: absolute;
      right: 40px;
      width: 0;
      top: 70px;
      border-right: 70px solid transparent;
      border-top: 70px solid #053750;
      border-left: 70px solid transparent;
      border-bottom: 0 solid #ffffff;
      z-index: 2;
    }

    .header-logo::before{
      content: "";
      height: 0;
      position: absolute;
      left: 10px;
      width: 0;
      top: 0;
      border-right: 70px solid transparent;
      border-top: 0 solid #76b4e3;
      border-left: 70px solid transparent;
      border-bottom: 70px solid #053750;
      z-index: 0;
    }

    section > header{
      display: block;
      height: 70px;
      width: 100%;
      position: absolute;
      top: 0;
      background-color: #ffffff;
    }

    section > header hr{
      /*border: 1.5px solid #8bc83b;*/
      border: none;
      width: 100%;
      position: absolute;
      right: 0;
      bottom: 0;
      margin: 0;
    }

    section > header hr.gray{
      /*border: 1.5px solid #f3f3f3;*/
      border: none;
      left:0;
      bottom: 6px;
      margin-left: 0;
      z-index: 2;
    }

    section > header .title-base{
      text-transform: uppercase;
      font-family: 'Times New Roman';
      color: #053750;
      font-size: 18px;
      font-weight: bold;
      padding: 22px 0;
      padding-left: 40px;
      position: absolute;
      left: 0;
      width: 100%;
      top: 38px;
      text-align: center;
      background-color: #e8eced;
    }

    .post-image{
      text-align: center;
    }

    .post-image img{
      width: 570px;
    }

    .content-wrapper{
      position: relative;
      /*margin-top:110px;*/
      top: 110px;
    }

    article.article-content{
      font-size: 1em;
      padding: 20px 40px;
      color: #202020;
    }

    .footer-content{
      color: #ffffff;
      text-align: right;
      width: 60%;
      padding-right: 30px;
      position: absolute;
      font-size: 0.5em;
      font-weight: 300;
      right: 0;
      padding-top: 10px;
    }

    .bold {
      font-weight: 700;
    }

    footer .contacts{
      font-size: 1.5em;
      font-weight: 400;
      margin-top: 11px;
    }

    footer .contacts > span{
      margin-left: 20px;
    }

    footer .contacts > span .icon img{
      width: 20px;
      vertical-align: middle;
      margin-right: 5px;
    }


    .l1t-logo-container{
      height: 90px;
      position: absolute;
      overflow: hidden;
      width: 223px;
    }

    .l1t-logo{
      background-image: url(https://storage.googleapis.com/learn1thing/img/l1t-logo.png);
      height: 73px;
      position: absolute;
      width: 128px;
      background-size: 105px;
      background-position: center center;
      background-repeat: no-repeat;
    }

    .l1t-logo.l1t-logo-small{
      background-size: 80px;
    }

    footer {
        margin-left: -15px;
        page-break-after: always;
    }

    .force-float {
        float: none !important;
        width: 100% !important;
    }

    @page {
        @bottom {
            content: flow(footer);
            vertical-align: top;
            margin-top: -70px;
        }
    }
    footer {
        flow: static(footer);
    }
  </style>


<section class="section-wrapper" style="background-image: url([report-background]); background-size: 100%">
    <header>
      <div class="title-base" style="text-align: [report-header-align]">
        [report-header]
      </div>
      <div class="header-logo">
        <img src="https://storage.googleapis.com/learn1thing/img/brand-logo-white.png"/>
      </div>
      <hr class="gray" />
      <hr/>
    </header>
    <div class="content-wrapper">
      <article class="article-content">
        [change-with-element]

      </article>
    </div>
</section>