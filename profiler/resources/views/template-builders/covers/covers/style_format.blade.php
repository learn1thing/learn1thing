<style type="text/css">
    @page { margin:0; size:A4; }
    body:not(.force-body-builder){
        font-family: 'Rubik', sans-serif;
    }
    body{
        margin:-10px;
        padding:0;
        height: 29.7cm;
        margin:auto;
        background-color: white;
        background-image: url([report-background]);
        background-size: 100% 100%;
    }

    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Light.ttf);
        font-weight: 300;
    }


    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Regular.ttf);
        font-weight: 400;
    }

    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Medium.ttf);
        font-weight: 500;
    }

    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Bold.ttf);
        font-weight: 700;
    }

    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Black.ttf);
        font-weight: 900;
    }

    @font-face {
        font-family: ProximaNova;
        src: url('fonts/proxima-nova-58f4b5b9b407b.otf');
    }

    .wrapper-style{
        position: relative;
        overflow: hidden;
        height: 29.7cm;
        page-break-after: always;
    }

    .container-style{
        background-color: #ffffff;
        height: 100%;
        background-image: url([report-background]);
        background-size: 100% 100%;
        page-break-after: always;
    }

    .with-element {
        margin: 0 100px;
    }

    .default-header, .default-footer {
        margin: 0 40px;
        padding: 20px;
    }

    .clearfix:after {
        clear: both;
        content: ".";
        display: block;
        height: 0;
        visibility: hidden;
    }

    .clearfix {
        display: inline-block;
    }

    .clearfix {
        display: block;
    }


    @media screen and (min-width: 1260px) {
        .title-container{
            width: auto;
            padding: 135px 0;
            padding-left: 50px;
        }

    }
</style>

<div class="container-style">
    <div class="wrapper-style">
        <div class="observers-content">
            <header class="default-header">
                <div style="text-align: [report-header-align]">
                    [report-header]
                </div>
            </header>
            <div class="with-element">
                [change-with-element]
            </div>
            <div class="clearfix"></div>
            <footer class="default-footer">
                <div style="text-align: [report-footer-align]">
                    [report-footer]
                </div>
            </footer>
        </div>
    </div>
</div>