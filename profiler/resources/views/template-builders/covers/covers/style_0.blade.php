<style type="text/css">
	@page { margin:0; size:A4; }
	body:not(.force-body-builder){
		font-family: 'Rubik', sans-serif;
	}
	body{
		margin:-10px;
		padding:0;
		height: 29.7cm;
		margin:auto;
		background-color: gray;
	}

	@font-face {
		font-family: 'Rubik';
		src: url(/fonts/rubik/Rubik-Light.ttf);
		font-weight: 300;
	}


	@font-face {
		font-family: 'Rubik';
		src: url(/fonts/rubik/Rubik-Regular.ttf);
		font-weight: 400;
	}

	@font-face {
		font-family: 'Rubik';
		src: url(/fonts/rubik/Rubik-Medium.ttf);
		font-weight: 500;
	}

	@font-face {
		font-family: 'Rubik';
		src: url(/fonts/rubik/Rubik-Bold.ttf);
		font-weight: 700;
	}

	@font-face {
		font-family: 'Rubik';
		src: url(/fonts/rubik/Rubik-Black.ttf);
		font-weight: 900;
	}

	@font-face {
		font-family: ProximaNova;
		src: url('/fonts/proxima-nova-58f4b5b9b407b.otf');
	}

	.wrapper{
		position: relative;
		overflow: hidden;
		height: 29.7cm;
	}

	.container{
		background-color: #ffffff;
		height: 100%;
	}

	.triangle{
		position: absolute;
		overflow: hidden;
	}

	.triangle > div {
		position: absolute;
		-ms-transform: rotate(45deg); /* IE 9 */
		-webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
		transform: rotate(45deg);
	}


	.triangle-1{
		height: 505px;
		width: 505px;
	}

	.triangle-1 .coloured-shape{
		background-color: #ffffff;
		top: -70%;
		left: -70%;
		height: 140%;
		width: 140%;
	}

	.triangle-2{
		padding-top: 100%;
		width: 100%;
	}

	.triangle-2 .coloured-shape{
		background-color: #001e61;
		top: 31%;
		left: -75.1%;
		height: 150%;
		width: 150%;
	}

	.cover-content{
		background-image: url(https://storage.googleapis.com/learn1thing/img/l1t-cover.png);
		background-size: cover;
		background-position: top center;
		height: 100%;
	}

	.gradient-overlay{
		width: 100%;
		height: 100%;
		position: absolute;
		background: -moz-linear-gradient(top,  rgba(0,0,0,0.35) 0%, rgba(0,0,0,0) 100%);
		background: -webkit-linear-gradient(top,  rgba(0,0,0,0.35) 0%,rgba(0,0,0,0) 100%);
		background: linear-gradient(to bottom,  rgba(0,0,0,0.35) 0%,rgba(0,0,0,0) 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a6000000', endColorstr='#00000000',GradientType=0 );

	}

	.cover-content .content{
		position: relative;
		padding: 45px 40px;
		z-index:1;
	}

	.brand-logo{
		background-image: url(https://storage.googleapis.com/learn1thing/img/brand-logo.png) ;
		background-repeat: no-repeat;
		background-size: 100%;
        
        height: 180px;
        margin-top: 50px;

        /* height: 80px; */
		width: 180px;
		margin-left: 20px;
	}

	.author{
		display: block;
		font-weight: 700;
	}

	.slogan{
		/* margin-top:10px;
		margin-bottom: 80px; */
		margin-top:0px;
		margin-bottom: 0px;
	}

	.slogan .second{
		color: #132660;
		font-family: ProximaNova;
		font-style: italic;
		font-size: 0.83em;
	}

	.slogan .first{
		font-size: 1em;
		color: #363636;
		margin-bottom: 3px;
		font-weight: 500;
	}

	.date-cover{
		height: 130px;
		width: 130px;
		position: absolute;
		text-align: center;
		display: table;
		left: 200px;
		color: #363636;
		font-size: 1.2em;
		top: 355px;
	}

	.date-cover .rotated-box-1{
		width: 100%;
		height: 100%;
		-ms-transform: rotate(45deg); /* IE 9 */
		-webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
		transform: rotate(45deg);
		background: #ffffff;
		position: absolute;
	}

	.date-cover .rotated-box-2{
		width: 50%;
		height: 100%;
		-ms-transform: rotate(45deg); /* IE 9 */
		-webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
		transform: rotate(45deg);
		background: #001e61;
		position: absolute;
		top: -53%;
		left: -28%;
	}

	.date-cover .rotated-box-3{
		width: 100%;
		height: 50%;
		-ms-transform: rotate(45deg);
		-webkit-transform: rotate(45deg);
		transform: rotate(45deg);
		background: #ffffff;
		position: absolute;
		bottom: -28%;
		left: -53%;
		background-image: url(https://storage.googleapis.com/learn1thing/img/shadow.png);
		background-position: top;
		background-size: 100% auto;
		background-repeat: no-repeat;
	}

	.date-content{
		position: relative;
		display: table-cell;
		vertical-align: middle;
		z-index: 1;
	}

	.date-cover .month{
		display: block;
		margin: 6px 0;
		font-weight: bold;
	}

	.prepared{
		font-size: 0.74em;
		font-weight: 400;

	}

	.cover-badge{
		color: #ffffff;
		background-color: #e1bf13;
		left: 0;
		position: absolute;
		padding-left:25px;
		height: 136px;
		margin-bottom: 50px;
		z-index: 1;
		font-weight: 300;
		top:585px;
	}

	.cover-badge h1{
		padding: 40px 0;
		font-weight: 500;
		font-size: 1.48em;
	}

	.cover-badge header{
		font-weight: 500;
		margin-bottom: 5px;
	}

	.cover-badge::before{
		content: "";
		height: 0;
		position: absolute;
		right: -136px;
		width: 0;
		top: -136px;
		border-right: 136px solid transparent;
		border-top: 136.6px solid transparent;
		border-left: 136px solid transparent;
		border-bottom: 136px solid #e1bf13;
		z-index: -1;
	}

	.cover-badge::after{
		content: "";
		height: 0;
		position: absolute;
		right: -136px;
		background-color: transparent;
		width: 0;
		bottom: -42px;
		border-right: 21px solid transparent;
		border-top: 21px solid #8e7904;
		border-left: 21px solid #001e61;
		border-bottom: 21px solid #001e61;
	}

	.cover-badge.cb-1{
		width: 262px;
	}

	.cover-badge.cb-2{
		padding: 20px 25px;
		height: 96px;
		font-size: 0.9em;
		width: 409px;
		margin-top: 172px;
	}

</style>

<div class="container">
	<div class="wrapper">
		<div class="cover-content">
			<div class="gradient-overlay">
			</div>
			<div class="triangle triangle-1">
				<div class="coloured-shape">
				</div>
			</div>
			<div class="date-cover">
				<div class="rotated-box-1"></div>
				<div class="rotated-box-2"></div>
				<div class="rotated-box-3"></div>
				<div class="date-content">[survey-period_start-style]</div>
			</div>
			<div class="content">
				<div class="brand-logo">
				</div>
				<div class="slogan">
					<!-- Comment by rdw 151217
					<div class="first">THE FUTURE OF LEARNING IS HERE</div>
					<div class="second">Learn from the comfort of your home</div> -->
				</div>
				<div class="prepared">
					Prepared for:
					<span class="author">[product-purchaser]</span>
					[survey-period_start-simple]
				</div>
				<div class="cover-badge cb-1">
					<h1>[survey-title]</h1>
				</div>
				<div class="cover-badge cb-2">
					[report-description]
					<!-- Comment by rdw 151217
					<header>Real-Time Learning in a Virtual Environment</header>
					learn1thing.com is the E-commerce store dedicated to give a wholesome &amp; engaging learning experience. <br>The various learning tools provide an ideal opportunity for those who seek to future-proof themselves. -->
				</div>
			</div>
			<div class="triangle triangle-2">
				<div class="coloured-shape">
				</div>
			</div>
		</div>
	</div>
</div>