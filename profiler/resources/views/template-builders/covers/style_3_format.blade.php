<style type="text/css">
    @page { margin:0; size:A4; }
    body{
      margin:0;
      padding:0;
      font-family: 'Arial', sans-serif;
      height: 29.7cm;
      margin:auto;
      background-color: gray;
    }

    section.section-wrapper{
      display: block;
      position: relative;
      width: 100%;
      height: 29.7cm;
      background-color:#ffffff;
      border-bottom:1px solid #000000;
      overflow:hidden;
    }

    footer{
      position: absolute;
      bottom: 0;
      font-family: 'Times New Roman';
      background-color: #053750;
      color: #fff;
      font-size: 11px;
      width: 100%;
      height: 70px;
      display: table;
    }

    footer span.paging:nth-child(2){
      text-align: center;
      background-color: #ffffff;
      position: absolute;
      top: -24px;
      width: 117px;
      padding: 35px 0px 5px;
      height: 94px;
      left: 120px;
      overflow: hidden;
    }

    .l1t-logos-container::after{
      content: "";
      height: 0;
      position: absolute;
      right: -15px;
      width: 0;
      top: 2px;
      border-right: 50px solid #053750;
      border-top: 72px solid transparent;
      border-left: 0 solid #053750;
      border-bottom: 0 solid transparent;
      z-index: 2;
    }

    footer span.paging:nth-child(2)::before{
      content: "";
      height: 0;
      position: absolute;
      right: -35px;
      width: 0;
      top: 0;
      border-right: 0 solid #ffffff;
      border-top: 35px solid transparent;
      border-left: 35px solid #e8eced;
      border-bottom: 35px solid transparent;
      z-index: 2;
    }

    .header-logo img{
      width: 90px;
      margin: auto;
      position: relative;
      z-index: 111;
      top: 20px;
      display: block;
      left: -31px;
    }

    .header-logo{
      width: 190px;
      height: 150%;
      position: absolute;
      background-color: #fff;
    }

    .header-logo::after{
      content: "";
      height: 0;
      position: absolute;
      right: 48px;
      width: 0;
      top: 92px;
      border-right: 0 solid transparent;
      border-top: 20px solid #ffffff;
      border-left: 20px solid transparent;
      border-bottom: 0 solid #ffffff;
      z-index: 2;
    }

    .header-logo::before{
      content: "";
      height: 0;
      position: absolute;
      left: 141px;
      width: 0;
      top: 0;
      border-right: 0px solid transparent;
      border-top: 0 solid #76b4e3;
      border-left: 50px solid transparent;
      border-bottom: 73px solid #3a3a3a;
      z-index: 0;
    }

    section > header{
      display: block;
      height: 130px;
      width: 100%;
      position: absolute;
      top: 0;
      background-color: #ffffff;
      overflow: hidden;
    }

    section > header hr{
      border: 7px solid #2888bb;
      width: 106px;
      position: relative;
      left: 104px;
      bottom: 0;
      margin: 0;
      top: 20px;
      -ms-transform: rotate(7deg);
      -webkit-transform: rotate(7deg);
      transform: rotate(125deg);
      z-index: 1;
    }

    section > header hr.gray{
      left: 16px;
      bottom: 106px;
      margin-left: 0;
      width: 250px;
      border: 10px solid #bebebc;
    }

    footer hr{
      border: 7px solid #2888bb;
      width: 92px;
      position: absolute;
      left: 156px;
      bottom: 2px;
      margin-bottom: 31px;
      -ms-transform: rotate(7deg);
      -webkit-transform: rotate(7deg);
      transform: rotate(125deg);
    }

    footer hr.gray{
      left: -3px;
      bottom: 73px;
      margin: 0;
      width: 120px;
      border: 10px solid #bebebc;
    }

    section > header .title-base{
      text-transform: uppercase;
      font-family: 'Times New Roman';
      color: #ffffff;
      font-size: 18px;
      font-weight: bold;
      padding: 26px 0;
      padding-left: 40px;
      position: absolute;
      left: 190px;
      width: 100%;
      top: 0;
      text-align: center;
      background-color: #3a3a3a;
      min-height: 21px;
      padding-left: 205px;
    }

    .post-image{
      text-align: center;
    }

    .post-image img{
      width: 570px;
    }

    .content-wrapper{
      position: relative;
      margin-top:110px;
    }

    article.article-content{
      font-size: 1em;
      padding: 20px 40px;
      color: #202020;
    }

    .footer-content{
      color: #ffffff;
      text-align: right;
      width: 60%;
      padding-right: 30px;
      position: absolute;
      font-size: 12px;
      font-weight: 300;
      right: 0;
      padding-top: 10px;
    }

    .bold {
      font-weight: 700;
    }

    footer .contacts{
      font-size: 1.5em;
      font-weight: 400;
      margin-top: 11px;
    }

    footer .contacts > span{
      margin-left: 20px;
    }

    footer .contacts > span .icon img{
      width: 20px;
      vertical-align: middle;
      margin-right: 5px;
    }


    .l1t-logos-container{
      height: 90px;
      position: absolute;
      width: 223px;
      background-color: #fff;
      top:-2px;
    }

    .l1t-logos{
      background-image: url(https://storage.googleapis.com/learn1thing/img/l1t-logo-blue.png);
      height: 73px;
      position: absolute;
      width: 128px;
      background-size: 105px;
      background-position: center center;
      background-repeat: no-repeat;
    }

    .l1t-logos.l1t-logos-small{
      background-size: 110px;
      left: 20px;
      z-index: 2;
      top: -20px;
    }

    .shape-1{
      position: absolute;
      right: -21px;
      width: 0;
      top: -25px;
      border-left: 19px solid transparent;
      border-bottom: 17px solid #ffffff;
      z-index: 2;
    }

    .force-float {
        float: none !important;
        width: 100% !important;
    }

    @page {
        @bottom {
            content: flow(footer);
            vertical-align: top;
            margin-top: -70px;
        }
    }
    footer {
        flow: static(footer);
    }
  </style>

<section class="section-wrapper" style="background-image: url([report-background]); background-size: 100%">
    <header>
      <div class="title-base" style="text-align: [report-header-align]">
        [report-header]
      </div>
      <div class="header-logo">
        <img src="https://storage.googleapis.com/learn1thing/img/brand-logo.png"/>
      </div>
      <hr class="gray" />
      <hr/>
    </header>
    <div class="content-wrapper">
      <article class="article-content">
        [change-with-element]

      </article>
    </div>
    <footer>
      <div class="l1t-logos-container">
        <div class="shape-1">
        </div>
        <div class="l1t-logos l1t-logos-small">
        </div>
      </div>
      <span class="paging">
        <hr class="gray" />
      </span>
        <hr/>
      <div class="footer-content" style="text-align: [report-footer-align]">
        [report-footer]

        <div class="contacts">
          <span><span class="icon"><img src="https://storage.googleapis.com/learn1thing/img/document-icon.png"/></span>store.learn1thing.com</span><span><span class="icon"><img src="https://storage.googleapis.com/learn1thing/img/phone-icon.png"/></span>+65 63238022</span>
        </div>
      </div>
    </footer>
  </section>
