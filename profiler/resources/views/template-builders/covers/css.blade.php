button.accordion {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
}

button.accordion.active, button.accordion:hover {
    background-color: #ddd;
}

button.accordion:after {
    content: '\002B';
    color: #777;
    font-weight: bold;
    float: right;
    margin-left: 5px;
}

button.accordion.active:after {
    content: "\2212";
}

div.panel-content {
    padding: 0 18px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
}

div.panel-content div {
    padding: 10px;
}

.template-composer {
    min-height: 300px;
    max-height: 29.7cm;
    height: auto;
    overflow-y: auto;
    overflow-x: auto;
}

#left-copy-1tomany * {
    cursor: move;
}

.editable-container.editable-inline {
    position: relative;
    z-index: 10;
}

a[data-wysihtml5-command="createLink"], a[data-wysihtml5-command="insertImage"] {
    display: none !important;
}

.row-old .element-wrapper {
    margin-bottom: 15px;
    padding-top: 15px;
    padding-bottom: 15px;
    min-height: 50px;
}

.row-old {
    margin: 0 !important;
}

div[data-edit="false"] {
    position: relative;
    margin-left: 15px;
    margin-right: 15px;
}

div[data-edit="false"] > button.close {
    position: absolute;
    top: 0;
    right: 0;
}

/*bootstrap*/
ul, ol {
    list-style: none;
}

.editable {
    text-decoration: none;
    color: black;
    border-bottom: none;
}

.text-center {
    text-align: center;
}

.row {
    margin-right: -15px;
    margin-left: -15px;
}
.col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
    position: relative;
    min-height: 1px;
    /* padding-right: 15px; */
    /* padding-left: 15px; */
}

.col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
    float: left;
}
.col-md-12 {
    width: 100%;
}
.col-md-11 {
    width: 91.66666667%;
}
.col-md-10 {
    width: 83.33333333%;
}
.col-md-9 {
    width: 75%;
}
.col-md-8 {
    width: 66.66666667%;
}
.col-md-7 {
    width: 58.33333333%;
}
.col-md-6 {
    width: 50%;
}
.col-md-5 {
    width: 41.66666667%;
}
.col-md-4 {
    width: 33.33333333%;
}
.col-md-3 {
    width: 25%;
}
.col-md-2 {
    width: 16.66666667%;
}
.col-md-1 {
    width: 8.33333333%;
}

.clearfix:after {
    clear: both;
    content: ".";
    display: block;
    height: 0;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

.clearfix {
    display: block;
}
/*end bootstrap*/

footer {
    margin-left: -15px;
    page-break-after: always;
}

.exclude-break footer {
    /* page-break-after: avoid !important; */
}

.force-float {
    float: none !important;
    width: 100% !important;
}

@page {
    @bottom {
        content: flow(footer);
        vertical-align: top;
        margin-top: -90px;
    }
}
footer {
    flow: static(footer);
}

.table-style-table {
    width: 100%;
    font-size: .6em;
}

.table-style-table thead {
    background-color: #035AA4;
    color: white;
}

.table-style-table .exclude-style-table {
    background-color: white !important;
}

.table-style-table th, .table-style-table td {
    padding: 5px;
    text-align: center;
}

.table-style-table tbody tr:nth-child(odd) td {
    background-color: #efefef;
}

.table-style-table tbody tr:nth-child(odd) td:nth-child(1) {
    background-color: #86cfea;
}

.table-style-table tbody tr:nth-child(even) td {
    background-color: #f5f5f5;
}

.table-style-table tbody tr:nth-child(even) td:nth-child(1) {
    background-color: #4dbae2;
}

.table-style-table tfoot tr td {
    font-weight: bold;
}
.class-bg-M {
    background-color: red !important;
}

.class-bg-D {
    background-color: blue !important;
}

.class-bg-P {
    background-color: #ffd400 !important;
}

.class-bg-A {
    background-color: green !important;
}

.removeElement{
    display:none;
}