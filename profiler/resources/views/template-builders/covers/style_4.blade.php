<style type="text/css">
    @page { margin:0; size:A4; }
    body{
        margin:0;
        padding:0;
        font-family: 'Rubik', sans-serif;
        height: 29.7cm;
        margin:auto;
        background-color: gray;
    }

    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Light.ttf);
        font-weight: 300;
    }


    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Regular.ttf);
        font-weight: 400;
    }

    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Medium.ttf);
        font-weight: 500;
    }

    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Bold.ttf);
        font-weight: 700;
    }

    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Black.ttf);
        font-weight: 900;
    }

    @font-face {
        font-family: ProximaNova;
        src: url('/fonts/proxima-nova-58f4b5b9b407b.otf');
    }

    .wrapper{
        position: relative;
        overflow: hidden;
        height: 29.7cm;
    }

    .container{
        background-color: #000000;
        height: 100%;
    }

    .cover-content{
        background-color: #ffffff;
        height: 100%;
    }

    .cover-content .content{
        position: relative;
        padding: 45px 40px;
        z-index:1;
    }

    .brand-logo{
        background-image: url(https://storage.googleapis.com/learn1thing/img/brand-logo.png) ;
        background-repeat: no-repeat;
        background-size: 100%;
        background-position: center;
        
        height: 180px;
        margin: 50px auto ;

        /* height: 80px; */
        width: 180px;
        /* margin: auto; */
    }

    .author{
        display: block;
        font-weight: 700;
    }

    .slogan{
        /* margin-top:10px; */
        margin-top:0px;
    }

    .slogan .second{
        color: #132660;
        font-family: ProximaNova;
        font-style: italic;
        font-size: 0.83em;
    }

    .slogan .first{
        font-size: 1em;
        color: #363636;
        margin-bottom: 3px;
        font-weight: 500;
    }

    .date-cover{
        height: 130px;
        width: 130px;
        position: relative;
        text-align: center;
        display: table;
        left: 50%;
        margin-left: -66px;
        color: #eaeaea;
        font-size: 1.2em;
        top: 476px;
        z-index: 4;
    }

    .date-content{
        position: relative;
        display: table-cell;
        vertical-align: middle;
        z-index: 4;
    }

    .date-cover .month{
        display: block;
        margin: 6px 0;
        font-weight: bold;
    }

    .prepared{
        font-size: 0.74em;
        font-weight: 400;
        -ms-transform: rotate(-15deg);
        -webkit-transform: rotate(-15deg);
        transform: rotate(-15deg);
        top: 160px;
        position: relative;
        z-index: 4;
        left: 70px;
        color: #ffffff;
    }

    .cover-badge{
        color: #363e49;
        right: 0;
        position: absolute;
        height: 136px;
        margin-bottom: 50px;
        z-index: 4;
        font-weight: 300;
        top: 725px;
    }

    .cover-badge h1{
        padding: 40px 0;
        font-weight: 500;
        font-size: 2.88em;
        -ms-transform: rotate(-15deg);
        -webkit-transform: rotate(-15deg);
        transform: rotate(-15deg);
        color: #ffffff;
        top: -70px;
        left: -70px;
        position: relative;
    }

    .cover-badge header{
        font-weight: 500;
        margin-bottom: 5px;
    }

    .cover-badge.cb-1{
    }

    .cover-badge.cb-2{
        padding: 20px 25px;
        height: 96px;
        font-size: 0.9em;
        width: 409px;
        margin-top: 150px;
        text-align: right;
    }

    .crossed-shape{
        width: 113%;
        position: absolute;
        left: -50px;
        display: block;
        height: 250px;
        top: 200px;
        -ms-transform: rotate(-15deg);
        -webkit-transform: rotate(-15deg);
        transform: rotate(-15deg);
        background: rgb(22,188,83);
        background: -moz-linear-gradient(left, rgba(22,188,83,1) 0%, rgba(0,93,175,1) 78%);
        background: -webkit-linear-gradient(left, rgba(22,188,83,1) 0%,rgba(0,93,175,1) 78%);
        background: linear-gradient(to right, rgba(22,188,83,1) 0%,rgba(0,93,175,1) 78%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#16bc53', endColorstr='#005daf',GradientType=1 );
        z-index: 1;
    }

    .crossed-shape.second{
        top: 630px;
    }

    .crossed-shape.third{
        top: 426px;
        -ms-transform: rotate(15deg);
        -webkit-transform: rotate(15deg);
        transform: rotate(15deg);
        z-index: 0;
        background: #363e49;
        width: 860px;
        left: 10px;
    }

    .brand-logo-wrapper{
        border: 10px solid #ffffff;
        border-radius: 50%;
        background-color: #eaeaea;
        height: 300px;
        width: 300px;
        display: block;
        z-index: 4;
        position: absolute;
        right: 20%;
    }

    .bltbl{
        display: table;
        height: 100%;
        width: 100%;
        position: relative;
        z-index: 4;
    }

    .brand-logo-wrapper .containerww{
        display: table-cell;
        background: none;
        vertical-align: middle;
        text-align: center;
        position: relative;
        z-index: 4;

    }

</style>

<div class="container">
    <div class="wrapper">
        <div class="cover-content">
            <div class="brand-logo-wrapper">
                <div class="bltbl">
                    <div class="containerww">
                        <div class="brand-logo">
                        </div>
                        <div class="slogan">
                            <!-- <div class="first">THE FUTURE OF LEARNING IS HERE</div>
                            <div class="second">Learn from the comfort of your home</div> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="crossed-shape">
            </div>
            <div class="crossed-shape second">
            </div>
            <div class="crossed-shape third">
            </div>
            <div class="cover-badge cb-1">
                <h1>[survey-title]</h1>
            </div>
            <div class="cover-badge cb-2">
                <!-- <header>Real-Time Learning in a Virtual Environment</header>
                learn1thing.com is the E-commerce store dedicated to give a wholesome &amp; engaging learning experience. <br/>The various learning tools, provide an ideal opportunity for those who seek to future-proof themselves. -->
                [report-description]
            </div>
            <div class="date-cover">
                <div class="date-content">[survey-period_start-style]</div>
            </div>
            <div class="prepared">
                Prepared for:
                <span class="author">[product-purchaser]</span>
                [survey-period_start-simple]
            </div>
            <div class="content">
            </div>
        </div>
    </div>
</div>