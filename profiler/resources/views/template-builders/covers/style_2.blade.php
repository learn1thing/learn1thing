<style type="text/css">
    @page { margin:0; size:A4; }
    body{
        margin:0;
        padding:0;
        font-family: 'Rubik', sans-serif;
        height: 29.7cm;
        margin:auto;
        background-color: gray;
    }

    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Light.ttf);
        font-weight: 300;
    }


    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Regular.ttf);
        font-weight: 400;
    }

    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Medium.ttf);
        font-weight: 500;
    }

    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Bold.ttf);
        font-weight: 700;
    }

    @font-face {
        font-family: 'Rubik';
        src: url(/fonts/rubik/Rubik-Black.ttf);
        font-weight: 900;
    }

    @font-face {
        font-family: ProximaNova;
        src: url('/fonts/proxima-nova-58f4b5b9b407b.otf');
    }

    .wrapper{
        position: relative;
        overflow: hidden;
        height: 29.7cm;
        background-image: url(https://storage.googleapis.com/learn1thing/img/tilebg.jpg);
    }

    .container{
        background-color: #000000;
        height: 100%;
    }

    .cover-content{
        background: rgb(239,240,242);
        background: -moz-linear-gradient(left, rgba(239,240,242,1) 0%, rgba(255,255,255,0) 40%, rgba(255,255,255,0) 50%, rgba(232,232,234,1) 100%);
        background: -webkit-linear-gradient(left, rgba(239,240,242,1) 0%,rgba(255,255,255,0) 40%,rgba(255,255,255,0) 50%,rgba(232,232,234,1) 100%);
        background: linear-gradient(to right, rgba(239,240,242,1) 0%,rgba(255,255,255,0) 40%,rgba(255,255,255,0) 50%,rgba(232,232,234,1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eff0f2', endColorstr='#e8e8ea',GradientType=1 ); /* IE6-9 */
        height: 100%;
    }

    .gradient-overlay{
    }

    .cover-content .content{
        position: relative;
        padding: 45px 40px;
        z-index:1;
    }

    .brand-logo{
        background-image: url(https://storage.googleapis.com/learn1thing/img/brand-logo.png) ;
        background-repeat: no-repeat;
        background-size: 100%;
        
        height: 180px;
        margin: 30px auto;
        background-position: center;

        /* height: 80px; */
        width: 180px;
        /* margin: auto; */
    }

    .author{
        display: block;
        font-weight: 700;
    }

    .slogan{
        /* margin-top:10px;
        text-align: center; */
        margin-top:0px;
        margin-bottom: 0px;
    }

    .slogan .second{
        color: #132660;
        font-family: ProximaNova;
        font-style: italic;
        font-size: 0.7em;
    }

    .slogan .first{
        font-size: 0.8em;
        color: #363636;
        margin-bottom: 3px;
        font-weight: 500;
    }

    .date-cover{
        height: 115px;
        width: 115px;
        position: absolute;
        text-align: center;
        display: table;
        left: 215px;
        color: #363636;
        font-size: 1.2em;
        top: 589px;
        z-index: 0;

    }

    .date-cover .rotated-box-1{
        width: 100%;
        height: 100%;
        -ms-transform: rotate(45deg); /* IE 9 */
        -webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
        transform: rotate(45deg);
        background: #053750;
        position: absolute;
        border: 5px solid #fff;
        top: -2px;
        left: -2px;
    }

    .date-cover .rotated-box-2{
        width: 100%;
        height: 100%;
        -ms-transform: rotate(45deg);
        -webkit-transform: rotate(45deg);
        transform: rotate(45deg);
        background: #ababab;
        position: absolute;
        top: -71%;
        left: -71%;
    }

    .date-cover .rotated-box-3{
        width: 100%;
        height: 100%;
        -ms-transform: rotate(45deg);
        -webkit-transform: rotate(45deg);
        transform: rotate(45deg);
        background: #ffffff;
        position: absolute;
        bottom: -76%;
        left: -71%;
        background-color: #1892d1;
    }

    .date-cover .rotated-box-4{
        width: 100%;
        height: 100%;
        -ms-transform: rotate(45deg);
        -webkit-transform: rotate(45deg);
        transform: rotate(45deg);
        background: #cbcbcb;
        position: absolute;
        top: -145%;
        left: -145%;
    }

    .date-content{
        position: relative;
        display: table-cell;
        vertical-align: middle;
        color:#fff;
        z-index: 2;
    }

    .date-cover .month{
        display: block;
        margin: 6px 0;
        font-weight: bold;
    }

    .prepared{
        font-size: 0.74em;
        font-weight: 400;
        left: 135px;
        position: absolute;
        color: #eeeeef;
        top: 710px;
        text-align: center;
    }

    .cover-badge{
        color: #053750;
        right: 0;
        position: absolute;
        padding-left:25px;
        height: 136px;
        margin-bottom: 50px;
        z-index: 0;
        font-weight: 300;
        top:261px;
        text-align: center;
    }

    .cover-badge h1{
        padding: 40px 0;
        font-size: 1.78em;
        text-transform: uppercase;
        font-weight: bolder;
        position: relative;
        top: -40px;
    }

    .cover-badge header{
        font-weight: 500;
        margin-bottom: 5px;
        color: #1992d1;
        font-size: 17px;
    }

    .cover-badge.cb-1{
        width: 420px;
    }

    .cover-badge.cb-2{
        padding: 60px 20px;
        height: 96px;
        font-size: 0.9em;
        /* width: 100%; */
        margin-top: 172px;
        background-color: #053750;
        color: #ffffff;
        font-weight: 500;
        left: 275px;
        padding-left: 90px;
    }

    .brand-logo-container{
        display: table;
        width: 240px;
        height: 240px;
        position: relative;
        top: 227px;
        left: 116px;
        z-index: 2;
    }

    .brand-logo-container .rotated-box{
        width: 100%;
        height: 100%;
        background-color: #e8eced;
        -ms-transform: rotate(45deg);
        -webkit-transform: rotate(45deg);
        transform: rotate(45deg);
        position: absolute;
        top: 0;
        left:0;
    }
    .brand-logo-container .rotated-box-shadow-1{
        width: 100%;
        height: 50%;
        -ms-transform: rotate(45deg);
        -webkit-transform: rotate(45deg);
        transform: rotate(45deg);
        position: absolute;
        bottom: -28%;
        left: -53%;
         background: -moz-linear-gradient(top, rgba(0,0,0,0.43) 0%, rgba(0,0,0,0.43) 6%, rgba(0,0,0,0) 17%, rgba(0,0,0,0) 99%);
        background: -webkit-linear-gradient(top, rgba(0,0,0,0.43) 0%,rgba(0,0,0,0.43) 6%,rgba(0,0,0,0) 17%,rgba(0,0,0,0) 99%);
        background: linear-gradient(to bottom, rgba(0,0,0,0.43) 0%,rgba(0,0,0,0.43) 6%,rgba(0,0,0,0) 17%,rgba(0,0,0,0) 99%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#9e000000', endColorstr='#00000000',GradientType=0 );
        z-index: 2;
    }

    .brand-logo-container .rotated-box-shadow-2{
        width: 100%;
        height: 50%;
        -ms-transform: rotate(-45deg);
        -webkit-transform: rotate(-45deg);
        transform: rotate(-45deg);
        position: absolute;
        bottom: -28%;
        right: -53%;
        background: -moz-linear-gradient(top, rgba(0,0,0,0.43) 0%, rgba(0,0,0,0.43) 6%, rgba(0,0,0,0) 17%, rgba(0,0,0,0) 99%);
        background: -webkit-linear-gradient(top, rgba(0,0,0,0.43) 0%,rgba(0,0,0,0.43) 6%,rgba(0,0,0,0) 17%,rgba(0,0,0,0) 99%);
        background: linear-gradient(to bottom, rgba(0,0,0,0.43) 0%,rgba(0,0,0,0.43) 6%,rgba(0,0,0,0) 17%,rgba(0,0,0,0) 99%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#9e000000', endColorstr='#00000000',GradientType=0 );
        z-index: 2;
    }

    .brand-logo-container .brand-logo-wrapper{
        display: table-cell;
        vertical-align: middle;
        position: relative;
        z-index: 1;
    }

</style>

<div class="container">
    <div class="wrapper">
        <div class="cover-content">
            <div class="cover-badge cb-1">
                <h1>[survey-title]</h1>
            </div>
            <div class="cover-badge cb-2">
               <!--  <header>Real-Time Learning in a Virtual Environment</header>
               learn1thing.com is the E-commerce store dedicated to give a wholesome &amp; engaging learning experience. <br/>The various learning tools, provide an ideal opportunity for those who seek to future-proof themselves. -->
               [report-description]
            </div>
            <div class="content">
                <div class="date-cover">
                    <div class="rotated-box-1"></div>
                    <div class="rotated-box-2"></div>
                    <div class="rotated-box-3"></div>
                    <div class="rotated-box-4"></div>
                    <div class="date-content">[survey-period_start-style]</div>
                </div>
                <dic class="brand-logo-container">
                    <div class="rotated-box">
                    </div>
                    <div class="brand-logo-wrapper">
                        <div class="brand-logo">
                        </div>
                        <div class="slogan">
                            <!-- <div class="first">THE FUTURE OF LEARNING IS HERE</div>
                            <div class="second">Learn from the comfort of your home</div> -->
                        </div>
                    </div>
                    <div class="rotated-box-shadow-1">
                    </div>
                    <div class="rotated-box-shadow-2">
                    </div>
                </dic>
                <div class="prepared">
                    Prepared for:
                    <span class="author">[product-purchaser]</span>
                    [survey-period_start-simple]
                </div>
            </div>
        </div>
    </div>
</div>