<button class="accordion">Layout</button>
<div id="left-copy-1tomany" class="panel-content row">
    <div class="col-md-4 text-center element-one-column" data-max="0">
        <a href="#">
            <i class="fa fa-square-o fa-2x"></i><br/>
            One Column
        </a>
    </div>
    <div class="col-md-4 text-center element-8-4-column" data-max="2">
        <a href="#">
            <i class="fa fa-square-o fa-2x"></i><br/>
            8-4 Column
        </a>
    </div>
    <div class="col-md-4 text-center element-4-8-column" data-max="2">
        <a href="#">
            <i class="fa fa-square-o fa-2x"></i><br/>
            4-8 Column
        </a>
    </div>
    <div class="col-md-4 text-center element-6x2-column" data-max="2">
        <a href="#">
            <i class="fa fa-square-o fa-2x"></i><br/>
            6x2 Column
        </a>
    </div>
    <div class="col-md-4 text-center element-4x3-column" data-max="3">
        <a href="#">
            <i class="fa fa-square-o fa-2x"></i><br/>
            4x3 Column
        </a>
    </div>
    <div class="col-md-4 text-center element-3x4-column" data-max="4">
        <a href="#">
            <i class="fa fa-square-o fa-2x"></i><br/>
            3x4 Column
        </a>
    </div>
</div>

<button class="accordion">Components</button>
<div class="panel-content row draggable-element">
    <div class="col-md-4 text-center element-component-image">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Image
        </a>
    </div>
    <div class="col-md-4 text-center element-component-title">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Title
        </a>
    </div>
    <div class="col-md-4 text-center element-component-subtitle">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Subtitle
        </a>
    </div>
    <div class="col-md-4 text-center element-component-text">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Text
        </a>
    </div>
    <div class="col-md-4 text-center element-component-question">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Question
        </a>
    </div>
    <div class="col-md-4 text-center element-component-answer">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Answer
        </a>
    </div>
    <!--
    <div class="col-md-4 text-center element-component-attribute">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Attribute
        </a>
    </div>
    <div class="col-md-4 text-center element-component-percentage">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Percentage
        </a>
    </div>
     -->
    <div class="col-md-4 text-center element-component-johari">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Johari
        </a>
    </div>
    <div class="col-md-4 text-center element-component-table">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Table Attribute
        </a>
    </div>
    <div class="col-md-4 text-center element-component-tquestion">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Table Question
        </a>
    </div>
    <div class="col-md-4 text-center element-component-break">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Break
        </a>
    </div>
    <div class="col-md-4 text-center element-component-page">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            New Page
        </a>
    </div>
    <div class="col-md-4 text-center element-component-communication">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Communication Style
        </a>
    </div>
</div>

<!--
<button class="accordion">Charts & Tables</button>
<div class="panel-content row draggable-element">
    <div class="col-md-4 text-center element-chart">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Johari
        </a>
    </div>
    <div class="col-md-4 text-center element-chart">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Pie
        </a>
    </div>
    <div class="col-md-4 text-center element-chart">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Line
        </a>
    </div>
    <div class="col-md-4 text-center element-chart">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Bar
        </a>
    </div>
    <div class="col-md-4 text-center element-chart">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Wind-rose
        </a>
    </div>
    <div class="col-md-4 text-center element-chart">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Spiderweb
        </a>
    </div>
    <div class="col-md-4 text-center element-chart">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Spline
        </a>
    </div>
    <div class="col-md-4 text-center element-chart">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Scatter
        </a>
    </div>
    <div class="col-md-4 text-center element-chart">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Gauge
        </a>
    </div>
    <div class="col-md-4 text-center element-chart">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Extreme
        </a>
    </div>
    <div class="col-md-4 text-center element-chart">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Wheel
        </a>
    </div>
    <div class="col-md-4 text-center element-chart">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Data table
        </a>
    </div>
</div>
 -->

<button class="accordion">Formula</button>
<div class="panel-content row draggable-element">
    <div class="col-md-4 text-center element-formula">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Formula
        </a>
    </div>
</div>

<button class="accordion">StarHub</button>
<div class="panel-content row draggable-element">
    <div class="col-md-4 text-center element-starhub-title">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Header
        </a>
    </div>
    <div class="col-md-4 text-center element-starhub-johari">
        <a href="#" onmousedown="return false">
            <i class="fa fa-square-o fa-2x"></i><br/>
            Johari
        </a>
    </div>
</div>