@extends('layouts.admin_template')

@push('plugins-css')
    {!! Html::style('/css/template-builder.css') !!}
    {!! Html::style('/vendor/dragula.js/dist/dragula.css') !!}
    {!! Html::style('/vendor/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css') !!}

    {!! Html::style('/x-editable/bootstrap-wysihtml5-0.0.3.css') !!}
    {!! Html::style('/image-picker/image-picker.css') !!}
    {{-- {!! Html::style('vendor/AdminLTE/plugins/select2/select2.css') !!} --}}

    <!-- Rating -->
    {!! Html::style('vendor/rateYo/src/jquery.rateyo.css') !!}
    <!-- Timepicker -->
    {!! Html::style('vendor/AdminLTE/plugins/timepicker/bootstrap-timepicker.css') !!}
@endpush

@push('plugins-js')
    {!! Html::script('/vendor/dragula.js/dist/dragula.js') !!}
    {!! Html::script('/vendor/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.js') !!}

    {!! Html::script('x-editable/wysihtml5-0.3.0.min.js') !!}
    {!! Html::script('x-editable/bootstrap-wysihtml5-0.0.3.min.js') !!}
    {!! Html::script('x-editable/wysihtml5-0.0.3.js') !!}
    {!! Html::script('vendor/bootstrap-validator/dist/validator.js') !!}

    {!! Html::script('vendor/AdminLTE/plugins/ckeditor/ckeditor.js') !!}
    {!! Html::script('image-picker/image-picker.js') !!}
    {{-- {!! Html::script('vendor/AdminLTE/plugins/select2/select2.js') !!} --}}
    {!! Html::script('vendor/underscore/underscore.js') !!}

    <!-- Rating -->
    {!! Html::script('vendor/rateYo/src/jquery.rateyo.js') !!}
    <!-- Timepicker -->
    {!! Html::script('vendor/AdminLTE/plugins/timepicker/bootstrap-timepicker.js') !!}
@endpush

@push('custom-js')
    {!! Html::script('js/global.js') !!}
    {!! Html::script('js/template-builder.js') !!}
    {!! Html::script('js/covers/starhub.js') !!}

    <script type="text/javascript">
        var isEdit = true;

        $(document).ready(function(){
            editElement('.editable');

            $('select[name=data_source]').change();
        })
    </script>
@endpush

@section('web-title', 'Template Builder')

@section('content')
    <div class="row">
        <div class="menu-sidebar-template col-md-3">
            @include('template-builders._accordion')
        </div>

        <div class="col-md-9">
            <ul  class="nav nav-tabs">
                <li class="active">
                    <a href="#1a" data-toggle="tab">Settings</a>
                </li>
                <li class="hide-composer">
                    <a href="#2a" data-toggle="tab">Report Composer</a>
                </li>
                <li class="hide-composer">
                    <button class="btn preview-template" data-url="{{ RouteHelper::token('template-builder.preview_v2') }}">Preview</button>
                </li>
            </ul>

            <div class="tab-content clearfix">
                <div class="tab-pane active" id="1a">
                    <div class="nav-tabs-custom"> @include('template-builders._form', array('data' => $data)) </div>
                </div>
                <div class="tab-pane" id="2a">
                    <div id="right-copy-1tomany" class="nav-tabs-custom template-composer">
                        {!! @$data['emailBuilder'] ? htmlspecialchars_decode($data['emailBuilder']->content) : '' !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Append Component</h4>
                </div>
                    <form id="form-component">
                        <input type="hidden" value="{{ route('report-formula.store') }}" id="formula-url">
                        <div class="modal-body">
                                <div class="form-group">
                                    <label for="choose_layout">Choose Layout</label>
                                    <select class="form-control select2" name="choose_layout" required>
                                        <option value="">Choose Layout</option>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="element-content">

                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-default add-element">Add</button>
                        </div>
                    </form>
            </div>

        </div>
    </div>

    <script id="element-chart-template" type="text/template">
        <form class="form-horizontal">
            <table class="table table-bordered">
                <tr>
                  <th style="width: 20%;">Label</th>
                  <th style="width: 40%;"><label class="col-sm-5 control-label">X axis</label></th>
                  <th><label class="col-sm-5 control-label">Y axis</label></th>
                </tr>
                <tr>
                  <td>
                  <div class="form-group">
                    <input type="text" class="form-control" name="label" placeholder="Enter the label" required>
                    <div class="help-block with-errors"></div>
                  </div>
                </td>
                  <td>
                    <div class="form-group clearfix">
                      <label for="inputDataType" class="col-sm-5 control-label">Data type:</label>
                      <div class="col-sm-7">
                        <select id="inputDataType" name="data_type" class="form-control select2" style="width: 100%;">
                          <option value="answer" selected="selected">Answer</option>
                          <option value="formula">Formula</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group clearfix">
                      <label for="inputDataType" class="col-sm-5 control-label">Data type:</label>
                      <div class="col-sm-7">
                        <select id="inputDataType" name="data_type" class="form-control select2" style="width: 100%;">
                          <option value="user" selected="selected">Primary user</option>
                          <option value="observer">Observer</option>
                          <option value="mixed">Mixed</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group clearfix">
                      <label for="inputAggregateBy" class="col-sm-5 control-label">Aggregate by:</label>
                      <div class="col-sm-7">
                        <select id="inputAggregateBy" name="data_type" class="form-control select2" style="width: 100%;">
                          <option value="value" selected="selected">Value</option>
                          <option value="observer_category">Observer category</option>
                          <option value="answer_attributes">Answer attributes</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group clearfix">
                      <label for="inputDataAggregate" class="col-sm-5 control-label">Data aggregate:</label>
                      <div class="col-sm-7">
                        <select id="inputDataAggregate" name="data_type" class="form-control select2" style="width: 100%;">
                          <option value="average" selected="selected">Average</option>
                          <option value="sum">Sum</option>
                        </select>
                      </div>
                    </div>
                  </td>
                  <td>
                    <div class="form-group clearfix">
                      <label for="inputDataType" class="col-sm-5 control-label">Data type:</label>
                      <div class="col-sm-7">
                        <select id="inputDataType" name="data_type" class="form-control select2" style="width: 100%;">
                          <option value="answer" selected="selected">Answer</option>
                          <option value="formula">Formula</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group clearfix">
                      <label for="inputDataType" class="col-sm-5 control-label">Data type:</label>
                      <div class="col-sm-7">
                        <select id="inputDataType" name="data_type" class="form-control select2" style="width: 100%;">
                          <option value="user" selected="selected">Primary user</option>
                          <option value="observer">Observer</option>
                          <option value="mixed">Mixed</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group clearfix">
                      <label for="inputAggregateBy" class="col-sm-5 control-label">Aggregate by:</label>
                      <div class="col-sm-7">
                        <select id="inputAggregateBy" name="data_type" class="form-control select2" style="width: 100%;">
                          <option value="value" selected="selected">Value</option>
                          <option value="observer_category">Observer category</option>
                          <option value="answer_attributes">Answer attributes</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group clearfix">
                      <label for="inputDataAggregate" class="col-sm-5 control-label">Data aggregate:</label>
                      <div class="col-sm-7">
                        <select id="inputDataAggregate" name="data_type" class="form-control select2" style="width: 100%;">
                          <option value="average" selected="selected">Average</option>
                          <option value="sum">Sum</option>
                        </select>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                    <td colspan="3" align="right"><div class="col-md-2 pull-right"><button type="button" class="btn">Add to row</button></div></td>
                </tr>
            </table>
        </form>

        <table class="table table-bordered">
                <tr>
                  <th style="width: 20%;">Label</th>
                  <th style="width: 40%;"><label class="col-sm-5 control-label">X axis</label></th>
                  <th><label class="col-sm-5 control-label">Y axis</label></th>
                </tr>
                <tr>
                    <td>[Label]</td>
                    <td>[X axis value]</td>
                    <td>[Y axis value]</td>
                </tr>
                <tr>
                    <td>[Label]</td>
                    <td>[X axis value]</td>
                    <td>[Y axis value]</td>
                </tr>
                <tr>
                    <td>[Label]</td>
                    <td>[X axis value]</td>
                    <td>[Y axis value]</td>
                </tr>
            </table>
    </script>
@endsection
