<footer>
    <div class="intro-title">
        <div class="shape-1"></div>
        <div class="l1t-logo-container">
            <div class="shape-1"></div>
            <div class="l1t-logo l1t-logo-small">
            </div>
        </div>
    </div>
    <div class="footer-content">
        <p>
            <span class="bold">learn1thing.com</span>
            is the E-commerce store dedicated to give a wholesome &nbsp; engaging learning experience. <br/>
            The various learning tools, provide an ideal opportunity for those who seek to future-proof themselves.
        </p>

        <div class="contacts">
            <span>
                <span class="icon"><img src="{{ URL::asset('/img/document-icon.png') }}"/></span>
                beta.learn1thing.com
            </span>
            <span>
                <span class="icon"><img src="{{ URL::asset('/img/phone-icon.png') }}"/></span>
                +65 63238020
            </span>
        </div>
    </div>
</footer>