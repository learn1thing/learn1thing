<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Intro</title>
  <meta name="description" content="The Future of Learning is Here">
  <meta name="author" content="RPS">
  <!-- <link rel="stylesheet" href="css/styles.css?v=1.0"> -->

  <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
  <![endif]-->
  <style type="text/css">
    @page { margin:0; size:A4; }
    body{
      margin:0;
      padding:0;
      font-family: 'Rubik', sans-serif;
      height: 29.7cm;
      margin:auto;
      background-color: gray;
    }

    @font-face {
      font-family: 'Rubik';
      src: url({{ URL::asset('/fonts/rubik/Rubik-Light.ttf') }});
      font-weight: 300;
    }


    @font-face {
      font-family: 'Rubik';
      src: url({{ URL::asset('/fonts/rubik/Rubik-Regular.ttf') }});
      font-weight: 400;
    }

    @font-face {
      font-family: 'Rubik';
      src: url({{ URL::asset('/fonts/rubik/Rubik-Medium.ttf') }});
      font-weight: 500;
    }

    @font-face {
      font-family: 'Rubik';
      src: url({{ URL::asset('/fonts/rubik/Rubik-Bold.ttf') }});
      font-weight: 700;
    }

    @font-face {
      font-family: 'Rubik';
      src: url({{ URL::asset('/fonts/rubik/Rubik-Black.ttf') }});
      font-weight: 900;
    }

    @font-face {
      font-family: ProximaNova;
      src: url({{ URL::asset('/fonts/proxima-nova-58f4b5b9b407b.otf') }});
    }

    .wrapper{
      position: relative;
      overflow: hidden;
      height: 29.7cm;
    }

    .container{
      background-color: #ffffff;
      height: 100%;
    }

    .intro-content{
      height: 100%;
    }

    .intro-logo{
      background-image: url({{ URL::asset('/img/brand-logo.png') }});
      background-size: 100%;
      height: 60px;
      width: 150px;
      margin: 20px 25px;
      position: absolute;
      right: 0;
    }

    .intro-content header{
      display: block;
      height: 105px;
      background-color: #ffffff;
    }

    .banner{
      height: 345px;
      width: 100%;
      background-color: #000000;
      position: relative;
      background-image: url({{ URL::asset('/img/l1t-cover.png') }});
      background-position: top center;
    }

    .banner-overlay{
      background-color: rgba(0,0,0,0.3);
      position:absolute;
      height: 100%;
      width: 100%;
    }

    .intro-title{
      height: 450px;
      position: absolute;
      width: 640px;
      top: 0;
      overflow: hidden;
    }

    .shape-2-container{
      height: 345px;
      position: absolute;
      width: 100%;
      top: 0;
      overflow: hidden;
      right: -24px;
    }

    .shape-3-container{
      height: 195px;
      position: absolute;
      width: 100%;
      top: 0;
      overflow: hidden;
      right: 0px;
    }

    .shape-1 {
      border-right: 28px solid #e1bf13;
      width: 1290px;
      height: 2400px;
      position: absolute;
      background-color: #ffffff;
      -ms-transform: rotate(45deg);
      -webkit-transform: rotate(45deg);
      top: -927px;
      right: 565px;
      transform: rotate(45deg);
    }

    .shape-2 {
      border-right: 28px solid #001e61;
      width: 750px;
      height: 1890px;
      position: absolute;
      background-color: #ffffff;
      -ms-transform: rotate(45deg);
      -webkit-transform: rotate(45deg);
      top: -560px;
      right: 640px;
      transform: rotate(45deg);
    }

    .shape-3 {
      width: 750px;
      height: 1890px;
      position: absolute;
      background-color: #001e61;
      -ms-transform: rotate(45deg);
      -webkit-transform: rotate(45deg);
      top: -560px;
      right: 640px;
      transform: rotate(45deg);
    }

    .title-container{
      background-color: #001e61;
      color: #ffffff;
      position: absolute;
      font-weight: 300;
      font-size: 1.91em;
      display: inline-block;
      padding-top: 90px;
      padding-left: 50px;
    }

    .article-intro{
      font-size: 1.41em;
      padding: 30px 70px;
    }

    footer{
      position: absolute;
      bottom: 0;
      background-color: #001e61;
      height: 115px;
      width: 100%;
    }

    footer .intro-title{
      bottom: 0;
      top: initial;
      left: 0px;
      min-height: auto;
      height: 145px;
      width: 320px
    }

    .l1t-logo-container{
      height: 90px;
      position: absolute;
      overflow: hidden;
      top: 55px;
      width: 223px;
    }

    .l1t-logo-container .shape-1{
      border-right: 40px solid transparent;
      background-color: #2e2e2e;
    }

    .l1t-logo{
      background-image: url({{ URL::asset('/img/l1t-logo.png') }});
      height: 88px;
      position: absolute;
      width: 128px;
      background-size: 105px;
      background-position: center center;
      background-repeat: no-repeat;
    }

    .footer-content{
      color: #ffffff;
      text-align: right;
      width: 60%;
      padding-right: 30px;
      padding-top: 37px;
      position: absolute;
      font-size: 0.5em;
      font-weight: 300;
      right: 0;
    }

    .bold {
      font-weight: 700;
    }

    footer .contacts{
      font-size: 1.5em;
      font-weight: 400;
      margin-top: 11px;
    }

    footer .contacts > span{
      margin-left: 20px;
    }

    footer .contacts > span .icon img{
      width: 20px;
      vertical-align: middle;
      margin-right: 5px;
    }
  </style>
</head>

<body>
  <div class="container">
    <div class="wrapper">
      <div class="intro-content">
        <header>
          <div class="intro-logo">
          </div>
        </header>
        <div class="banner">
          <div class="banner-overlay">
          </div>
        </div>
        <div class="intro-title">
          <div class="shape-1"></div>
          <div class="shape-2-container">
            <div class="shape-2"></div>
          </div>
          <div class="shape-3-container">
            <div class="shape-3"></div>
          </div>
          <div class="title-container">
            360&deg; Influence Profiler
          </div>
        </div>
        <article class="article-intro">
          <p>Positive influencing skill is a key trade to have in most of our workplace. Good ideas alone would not be sufficient for you to implement things your way. Be it encouraging your direct reports to increase productivity or influencing our superiors to gain support for your projects.</p>
          <p>The 360&deg; Influence Profiler is designed to allow you to reflect on what you do on a day-to-day basis when influencing others. It helps highlight your observable strengths and blind spots, based </p>
        </article>
        <footer>
          <div class="intro-title">
            <div class="shape-1">
            </div>
            <div class="l1t-logo-container">
              <div class="shape-1">
              </div>
              <div class="l1t-logo">
              </div>
            </div>
          </div>
          <div class="footer-content">
            <p><span class="bold">learn1thing.com</span> is the E-commerce store dedicated to give a wholesome & engaging learning experience. <br/>The various learning tools, provide an ideal opportunity for those who seek to future-proof themselves.</p>

            <div class="contacts">
              <span><span class="icon"><img src="{{ URL::asset('/img/document-icon.png') }}"/></span>beta.learn1thing.com</span><span><span class="icon"><img src="{{ URL::asset('/img/phone-icon.png') }}"/></span>+65 63238022</span>
            </div>
          </div>
        </footer>
      </div>
    </div>
  </div>
  <!-- <script src="js/scripts.js"></script> -->
</body>
</html>