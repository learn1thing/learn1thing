@php ($date = strtotime($userSurvey->survey->period_start))
@php ($observer = $userSurvey->observer)
@php ($survey = $userSurvey->survey)
<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Report | {{ $survey->title }}</title>
    <meta name="description" content="The Future of Learning is Here">
    <meta name="author" content="RPS">

    <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->
    <link href="{{ asset('/css/report.css')}}" rel="stylesheet" type="text/css" />
</head>

<body>

    <!-- <div style="height: 500px; overflow: scroll;">
        <pre>
{{ print_r($answers->toArray()) }}
        </pre>
    </div> -->

    <!-- Cover -->
    <div class="container">
        <div class="wrapper">
            <div class="cover-content">
                <div class="gradient-overlay"></div>
                <div class="triangle triangle-1">
                    <div class="coloured-shape"></div>
                </div>
                <div class="date-cover">
                    <div class="rotated-box-1"></div>
                    <div class="rotated-box-2"></div>
                    <div class="rotated-box-3"></div>
                    <div class="date-content">
                        <!-- 10 <span class="month">APRIL</span>2017 -->
                        {{ date("d", $date) }}
                        <span class="month">{{ date("F", $date) }}</span>
                        {{ date("Y", $date) }}
                    </div>
                </div>
                <div class="content">
                    <div class="brand-logo"></div>
                    <div class="slogan">
                        <div class="first">THE FUTURE OF LEARNING IS HERE</div>
                        <div class="second">Learn from the comfort of your home</div>
                    </div>
                    <div class="prepared">
                        Prepared for:
                        <!-- <span class="author">Noor Fahmie Mahadi</span> -->
                        <span class="author">{{ $observer->full_name() }}</span>
                        {{ date("d-m-Y", $date) }}
                        <!-- 10-04-2017 -->
                    </div>
                    <div class="cover-badge cb-1">
                        <!-- <h1>360&deg; Influence Profiler</h1> -->
                        <h1>{{ $survey->title }}</h1>
                    </div>
                    <div class="cover-badge cb-2">
                        {!! nl2br(e($survey->description)) !!}
                        <!-- <header>Real-Time Learning in a Virtual Environment</header>
                        learn1thing.com is the E-commerce store dedicated to give a wholesome &amp; engaging learning experience. <br/>The various learning tools, provide an ideal opportunity for those who seek to future-proof themselves. -->
                    </div>
                </div>
                <div class="triangle triangle-2">
                    <div class="coloured-shape"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Cover -->

    <!-- Intro -->
    <div class="container">
        <div class="wrapper">
            <div class="intro-content">
                <header>
                    <div class="intro-logo"></div>
                </header>
                <div class="banner">
                    <div class="banner-overlay"></div>
                </div>
                <div class="intro-title">
                    <div class="shape-1"></div>
                    <div class="shape-2-container">
                        <div class="shape-2"></div>
                    </div>
                    <div class="shape-3-container">
                        <div class="shape-3"></div>
                    </div>
                    <div class="title-container">
                        {{ $survey->title }}
                        <!-- 360&deg; Influence Profiler -->
                    </div>
                </div>
                <article class="article-intro">
                    {!! nl2br(e($survey->description)) !!}
                    <!-- <p>Positive influencing skill is a key trade to have in most of our workplace. Good ideas alone would not be sufficient for you to implement things your way. Be it encouraging your direct reports to increase productivity or influencing our superiors to gain support for your projects.</p>
                    <p>The 360&deg; Influence Profiler is designed to allow you to reflect on what you do on a day-to-day basis when influencing others. It helps highlight your observable strengths and blind spots, based </p> -->
                </article>
                <footer>
                    <div class="intro-title">
                        <div class="shape-1"></div>
                        <div class="l1t-logo-container">
                            <div class="shape-1"></div>
                            <div class="l1t-logo"></div>
                        </div>
                    </div>
                    <div class="footer-content">
                        <p>
                            <span class="bold">learn1thing.com</span>
                            is the E-commerce store dedicated to give a wholesome &nbsp; engaging learning experience. <br/>
                            The various learning tools, provide an ideal opportunity for those who seek to future-proof themselves.
                        </p>

                        <div class="contacts">
                            <span>
                                <span class="icon"><img src="{{ URL::asset('/img/document-icon.png') }}"/></span>
                                beta.learn1thing.com
                            </span>
                            <span>
                                <span class="icon"><img src="{{ URL::asset('/img/phone-icon.png') }}"/></span>
                                +65 63238020
                            </span>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>
    <!-- End Intro -->

    <!-- Questions -->



    @php ($number = 0)
    @foreach ($survey->questionnaires() as $section)
        <div class="container">
            <div class="wrapper">
                <div class="questions-content">
                    @include('report.header')
                    <div class="title title-questions">
                        {{ $section->title }}
                    </div>
                    <div class="subtitle subtitle-questions">
                        {!! nl2br(e($section->description)) !!}
                    </div>
                    <article class="article-questions">
                        @php ($number = ($section->is_number_continued ? $number : 0))
                        @foreach ($section->questions as $question)
                            @php ($baseName = 'questionnaire['.$question->id.']')
                            @php ($number += 1)
                            <div class="section-question">
                                <div class="sq-1">
                                    <span>Question : </span>
                                    {!! $question->content !!}
                                </div>

                                <div class="sq-2">
                                    @if($question->answer_type == "checkboxes" || $question->answer_type == "multiple_choice_with_constraint")

                                        @if($question->answer_type == "multiple_choice_with_constraint")
                                            @php ( $settings = $question->answer_configs )
                                            @php ( $options = $settings->options )
                                            <div class="row">
                                                <div class="col-lg-1"></div>
                                                <div class="col-lg-11">
                                                    <label>Max Selected: {{ $settings->points }}</label>
                                                </div>
                                            </div>
                                        @else
                                            @php ( $options = $question->answer_configs )
                                        @endif

                                        <div class="row">
                                            <div class="col-lg-1"></div>
                                            <div class="col-md-11 col-sm-11 wrapper-answer-config" {{ $question->answer_type == "multiple_choice_with_constraint" ? "data-max-selected=" . $settings->points:"" }}>
                                                @foreach( $options as $label )
                                                    <div class="checkbox {{ $question->coordinate == 'horizontal' ? 'checkbox-inline' : '' }}">
                                                        <label>
                                                            <input name="{{ $baseName }}[]" class="{{ $question->answer_type == 'multiple_choice_with_constraint' ? 'with-constraint' : '' }}" value="{{ $label }}" type="checkbox" disabled>
                                                            {{ $label }}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    @elseif($question->answer_type == "free_text")
                                        <!-- udah ada answer -->
                                        <div class="row">
                                            <div class="col-lg-1"></div>
                                            <div class="col-md-11 col-sm-11">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <textarea name="{{ $baseName }}" class="form-control" rows="3" placeholder="Fill answer ..." disabled>{{ @$answers[$question->id] }}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    @elseif($question->answer_type == "multichoice" || $question->answer_type == "boolean" || $question->answer_type == "likert_scale")
                                        <!-- udah ada answer -->
                                        <div class="row">
                                            <div class="col-lg-1"></div>
                                            <div class="col-md-11 col-sm-11">
                                                @foreach( $question->answer_configs as $answer_config )
                                                    @if($question->answer_type == "likert_scale")
                                                        @php ($label = $answer_config->label)
                                                        @php ($value = $answer_config->value)
                                                    @else
                                                        @php ($label = $answer_config)
                                                        @php ($value = $answer_config)
                                                    @endif

                                                    <div class="radio {{ $question->coordinate == 'horizontal' ? 'radio-inline' : '' }}">
                                                        <label>
                                                            <input name="{{ $baseName }}" value="{{ $value }}" type="radio"  disabled {{ @$answers[$question->id] == $value ? 'checked' : '' }}>
                                                            {{ $label }}<br/>
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>

                                    @elseif($question->answer_type == "matching")
                                        @php ( $settings = $question->answer_configs )
                                        <div class="row">
                                            <div class="col-lg-1"></div>
                                            <div class="col-md-11 col-sm-11">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group clearfix">
                                                            <div class="col-sm-6">
                                                                <strong>Option</strong>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <strong>Value</strong>
                                                            </div>
                                                        </div>
                                                        @foreach( $settings as $setting )
                                                            <div class="form-group clearfix">
                                                                <div class="col-sm-6">
                                                                    <strong> {{ $setting->label }} </strong>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <select class="form-control" name="{{ $baseName }}{{ str_replace(' ', '_', $setting->label) }}" placeholder="" disabled>
                                                                        <option value="">Choose Option</option>
                                                                        @foreach( $settings as $data )
                                                                            <option value="{{ $data->value }}"> {{ $data->value }} </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    @elseif($question->answer_type == "ranking")
                                        @php ( $settings = $question->answer_configs )
                                        <div class="row">
                                            <div class="col-lg-1"></div>
                                            <div class="col-md-11 col-sm-11">
                                                <div class="form-group">
                                                    <div class="radio tes-rating"></div>
                                                    <input type="hidden" name="{{ $baseName }}" disabled>
                                                </div>
                                            </div>
                                        </div>

                                    @elseif($question->answer_type == "date")
                                        <!-- udah ada answer -->
                                        <div class="row">
                                            <div class="col-lg-1"></div>
                                            <div class="col-md-11 col-sm-11">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="input-group date">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" name="{{ $baseName }}" class="form-control pull-right datepicker selected-datepicker" disabled value="{{ @$answers[$question->id] }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    @elseif($question->answer_type == "time")
                                        <!-- udah ada answer -->
                                        <div class="row">
                                            <div class="col-lg-1"></div>
                                            <div class="col-md-11 col-sm-11">
                                                <div class="row">
                                                    <div class="col-md-6 bootstrap-timepicker">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                            <input type="text" name="{{ $baseName }}" class="form-control timepicker selected-timepicker" disabled value="{{ @$answers[$question->id] }}">
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    @elseif($question->answer_type == "datetime")
                                        <div class="row">
                                            <div class="col-lg-1"></div>
                                            <div class="col-md-11 col-sm-11">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="input-group date">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" name="{{ $baseName }}[date]" class="form-control pull-right datepicker selected-datepicker-datetime" disabled>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 bootstrap-timepicker">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                            <input type="text" name="{{ $baseName }}[time]" class="form-control timepicker selected-timepicker-datetime" disabled>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    @elseif($question->answer_type == "distributed_points")
                                        @php ( $settings = $question->answer_configs )
                                        <div class="row">
                                            <div class="col-lg-1"></div>
                                            <div class="col-md-11 col-sm-11">
                                                <div class="row wrapper-distributed_points">
                                                    <div class="col-md-6">
                                                        <label class="control-label label-max-point" data-max-point="{{ isset($settings->point) ? $settings->point : 0 }}">Max Point: {{ isset($settings->point) ? $settings->point : 0 }}</label>
                                                        <table class="table table-bordered">
                                                            <tr>
                                                                <th>List</th>
                                                                <th>Point</th>
                                                            </tr>
                                                            @if(isset($settings->options))
                                                                @foreach( $settings->options as $label )
                                                                    <tr>
                                                                        <td>{{ $label }}</td>
                                                                        <td><input type="number" name="{{ $baseName }}{{ str_replace(' ', '_', $label) }}" class="form-control input-distributed_points" placeholder="Point"></td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </article>
                    @include('report.footer')
                </div>
            </div>
        </div>
    @endforeach
    <!-- End Questions -->

    <!-- Observers -->
    <div class="container">
        <div class="wrapper">
            <div class="observers-content">
                @include('report.header')
                <div class="title title-observers">
                    Observers
                </div>
                <div class="post-image">
                    <img src="{{ URL::asset('/img/observers.png') }}"/>
                </div>
                <article class="article-observers">
                    <p>A 360&deg;feedback is where you get inputs from your manager, peers, direct report and any other people familiar with the your work behaviour. This instrument is used for you to gain insights on your strength and weaknesses on various competencies, enhancing personal development, self-awareness, and performance.</p>

                    <p>On completion of the profiler you will receive a report in the form of a Johari Window which helps you assess your visible strengths and areas of improvement when dealing and resolving conflicts. The Johari Window is a simple 2 x 2 matrix that compares your perception about how you deal with conflicts against how others see you, thus enhancing your self-awareness and understanding your gaps.</p>

                </article>
                @include('report.footer')
            </div>
        </div>
    </div>
    <!-- End Observers -->

    <!-- Johari -->
    <div class="container">
        <div class="wrapper">
            <div class="observers-content">
                @include('report.header')
                <div class="title title-johari">
                    How To Interpret Your Report
                </div>
                <div class="subtitle title-johari">
                    Evaluating Your Strengths And Weaknesses
                </div>
                <article class="article-johari">
                    <div class="two-column">
                        <div>
                            <img src="{{ URL::asset('/img/johari.png') }}" width="100%"/>
                        </div>
                        <div>
                            <p>
                                <span class="h3">Visible Strength</span>
                                If you rate yourself high and others rate you high, you've got a strength that is visible to everyone. Keep Up the good work.
                            </p>
                            <p>
                                <span class="h3">Blind Spot</span>
                                If you rate yourself high and others rate you low, you've got a blind spot. You need to analyze this mor deeply and then either work on the skill, or the perception of the skill.
                            </p>
                            <p>
                                <span class="h3">Unrealized Strength</span>
                                If you rate yourself low and others rate you high, you may have an unrealized strength. You need to analyze this and readjust yor assessment and/or your behaviour.
                            </p>
                            <p>
                                <span class="h3">Acknowledged Soft Spot</span>
                                If you rate yourself low and others rate you low, you have an acknowledged soft spot. This is and area on which you obviously need to work.
                            </p>
                        </div>
                    </div>
                </article>
                @include('report.footer')
            </div>
        </div>
    </div>
    <!-- End Johari -->
</body>
</html>