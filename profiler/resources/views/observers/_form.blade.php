<div class="modal fade" id="observers" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">{!! trans('app.label.add') !!}</h4>
            </div>

            {!! Form::model($data['observer'], $form) !!}
            <div class="modal-body">
                <div class="panel-body">
                    <div class="box-body">
                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-sm-2 control-label">{!! trans('app.label.email') !!}</label>

                            <div class="col-sm-10">
                                {!! Form::email('email', old('email'), ['class' => 'form-control']) !!}
                                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="first_name" class="col-sm-2 control-label">{!! trans('app.label.first_name') !!}</label>

                            <div class="col-sm-10">
                                {!! Form::text('first_name', old('first_name'), ['class' => 'form-control']) !!}
                                {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name" class="col-sm-2 control-label">{!! trans('app.label.last_name') !!}</label>

                            <div class="col-sm-10">
                                {!! Form::text('last_name', old('last_name'), ['class' => 'form-control']) !!}
                                {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('phone_number') ? ' has-error' : '' }}">
                            <label for="phone_number" class="col-sm-2 control-label">{!! trans('app.label.phone_number') !!}</label>

                            <div class="col-sm-10">
                                {!! Form::text('phone_number', old('phone_number'), ['class' => 'form-control']) !!}
                                {!! $errors->first('phone_number', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('category_id') ? ' has-error' : '' }}">
                            <label for="category_id" class="col-sm-2 control-label">{!! trans('app.label.category_id') !!}</label>

                            <div class="col-sm-10">
                                {!! Form::select('category_id', $data['categoryData'], old('category_id'), ['class' => 'form-control select2']) !!}
                                {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">{!! trans('app.label.cancel') !!}</button>
                {!! Form::submit(trans('app.label.add'), ['class'=>'btn blue']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>