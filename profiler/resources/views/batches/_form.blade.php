<div class="modal fade" id="batches" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">{!! trans('app.label.add') !!}</h4>
            </div>
            {!! Form::model($data['batch'], $form) !!}
            <div class="modal-body">
                <div class="panel-body">
                    <div class="box-body">
                        <div class="form-group {{ $errors->has('batch') ? ' has-error' : '' }}">
                            <label for="batch" class="col-sm-2 control-label">{!! trans('app.label.batch') !!}</label>

                            <div class="col-sm-10">
                                {!! Form::text('batch', old('batch'), ['class' => 'form-control']) !!}
                                {!! $errors->first('batch', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('survey_id') ? ' has-error' : '' }}">
                            <label for="survey_id" class="col-sm-2 control-label">{!! trans('app.label.survey') !!}</label>

                            <div class="col-sm-10">
                                {!! Form::select('survey_id', $data['surveyData'], old('survey_id'), ['class' => 'form-control select2', 'data-url' => RouteHelper::token('batches.observers', ['id'=>'surveyId'])]) !!}
                                {!! $errors->first('survey_id', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('observer_id') ? ' has-error' : '' }}">
                            <label for="observer_id" class="col-sm-2 control-label">{!! trans('app.label.observer') !!}</label>

                            <div class="col-sm-10">
                                {!! Form::select('observer_id', $data['observerData'], old('observer_id'), ['class' => 'form-control select2']) !!}
                                {!! $errors->first('observer_id', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">{!! trans('app.label.cancel') !!}</button>
                {!! Form::submit(trans('app.label.add'), ['class'=>'btn blue']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>