@if(count($questions) === 0)
	<div class="question callout">
		<div class="row">
			<div class="form-group clearfix">
				<label class="col-sm-2">Question Number  :</label>
				<div class="col-sm-5">
					<input type="text" class="form-control input-question-number" name="{{ $section_type }}[{{ $section_index }}][question][0][question_number]" placeholder="Enter the number of question" disabled>
				</div>
				<div class="help-block with-errors"></div>
			</div>
		</div>
		<div class="row">
			<div class="form-group question-name clearfix">
				<label class="col-sm-2">Name (<span class="text-red">*</span>) :</label>
				<div class="col-sm-5">
					<input type="text" class="form-control input-question-name" name="{{ $section_type }}[{{ $section_index }}][question][0][name]" placeholder="Enter the name of question" {{ $is_exist_answer ? 'disabled':'required' }}>
				</div>
				<div class="help-block with-errors"></div>
			</div>
		</div>
		<div class="form-group question-content">
			<label>Question (<span class="text-red">*</span>) :</label>
			<textarea id="{{ $section_type }}-section-{{ $section_index }}-question-0" name="{{ $section_type }}[{{ $section_index }}][question][0][content]" class="form-control question_contents" rows="3" placeholder="Enter ..." {{ $is_exist_answer ? 'disabled':'required' }}></textarea>
			<div class="help-block with-errors"></div>
		</div>
		<br>
		<div class="row">
			<div class="form-group clearfix">
				<label class="col-sm-2">Answer Type (<span class="text-red">*</span>) :</label>
				<div class="col-sm-5">
					<select name="{{ $section_type }}[{{ $section_index }}][question][0][answer_type]" class="answer-type form-control" {{ $is_exist_answer ? 'disabled':'required' }}>
						<option value=""></option>
						<option value="free_text">Free text</option>
						<option value="multichoice">Multiple choice</option>
						<option value="checkboxes">Checkboxes</option>
						{{-- <option value="matching">Matching</option> --}}
						{{-- <option value="likert_scale">Likert scale</option> --}}
						<option value="boolean">True or false</option>
						{{-- <option value="ranking">Ranking</option> --}}
						<option value="multiple_choice_with_constraint">Multiple choice with constraint</option>
						<option value="date">Date</option>
		        <option value="time">Time</option>
		        <option value="datetime">Date and time</option>
		        <option value="distributed_points">Distributed points</option>
					</select>
				</div>
				<div class="help-block with-errors"></div>
			</div>
		</div>
		<div class="row answer-options">

		</div>
		<br>
		<div class="box-footer">
			<div class="row">
				<div class="pull-right">
					<label>Required: </label>
					<input type="checkbox" checked data-size="small" data-toggle="toggle" data-on="" data-off="" name="{{ $section_type }}[{{ $section_index }}][question][0][is_required]" value="1" {{ $is_exist_answer ? 'disabled':'' }}>
					<button class="btn btn-default remove-question" type="button" title="Remove question"><i class="fa fa-fw fa-trash"></i></button>
				</div>
			</div>
		</div>
	</div>
@else
	@foreach($questions as $questionIndex => $question)
		@if(!in_array($question['answer_type'], array('matching', 'likert_scale', 'ranking')))
			<div class="question callout">
				<div class="row">
					<div class="form-group clearfix {{ $errors->has($section_type . '.' . $section_index . '.question.' . $questionIndex . '.question_number') ? ' has-error' : '' }}">
						<label class="col-sm-2">Question Number  :</label>
						<div class="col-sm-5">
							<input type="text" class="form-control input-question-number" name="{{ $section_type }}[{{ $section_index }}][question][{{ $questionIndex }}][question_number]" placeholder="Enter the number of question" value="{{ isset($question['question_number']) ? $question['question_number']:'' }}" {{ isset($section['automatic_numbering']) && $section['automatic_numbering'] == 'yes' || $is_exist_answer ? 'disabled':'' }}>
						</div>
						@if ($errors->has($section_type . '.' . $section_index . '.question.' . $questionIndex . '.question_number'))
				      <span class="help-block">
				        <strong>{{ $errors->first($section_type . '.' . $section_index . '.question.' . $questionIndex . '.question_number') }}</strong>
				      </span>
				    @endif
					</div>
				</div>
				<div class="row">
					<div class="form-group question-name clearfix {{ $errors->has($section_type . '.' . $section_index . '.question.' . $questionIndex . '.name') ? ' has-error' : '' }}">
						<label class="col-sm-2">Name (<span class="text-red">*</span>) :</label>
						<div class="col-sm-5">
							<input type="text" class="form-control input-question-name" name="{{ $section_type }}[{{ $section_index }}][question][{{ $questionIndex }}][name]" placeholder="Enter the name of question" value="{{ isset($question['name']) ? $question['name']:'' }}" {{ $is_exist_answer ? 'disabled':'required' }}>
						</div>
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="form-group question-content {{ $errors->has($section_type . '.' . $section_index . '.question.' . $questionIndex . '.content') ? ' has-error' : '' }}">
					<label>Question (<span class="text-red">*</span>):</label>
					<textarea id="{{ $section_type }}-section-{{ $section_index }}-question-{{ $questionIndex }}" name="{{ $section_type }}[{{ $section_index }}][question][{{ $questionIndex }}][content]" class="form-control question_contents" rows="3" placeholder="Enter ..." {{ $is_exist_answer ? 'disabled':'' }}>{{ $question['content'] }}</textarea>
					@if ($errors->has($section_type . '.' . $section_index . '.question.' . $questionIndex . '.content'))
			      <span class="help-block">
			        <strong>{{ $errors->first($section_type . '.' . $section_index . '.question.' . $questionIndex . '.content') }}</strong>
			      </span>
			    @endif
				</div>
				<br>
				<div class="row">
					<div class="form-group clearfix {{ $errors->has($section_type . '.' . $section_index . '.question.' . $questionIndex . '.answer_type') ? ' has-error' : '' }}">
						<label class="col-sm-2">Answer Type (<span class="text-red">*</span>) :</label>
						<div class="col-sm-5">
							<select name="{{ $section_type }}[{{ $section_index }}][question][{{ $questionIndex }}][answer_type]" class="answer-type form-control" {{ $is_exist_answer ? 'disabled':'' }}>
								<option value=""></option>
								<option value="free_text" {{ $question['answer_type'] == 'free_text' ? 'selected':'' }}>Free text</option>
								<option value="multichoice" {{ $question['answer_type'] == 'multichoice' ? 'selected':'' }}>Multiple choice</option>
								<option value="checkboxes" {{ $question['answer_type'] == 'checkboxes' ? 'selected':'' }}>Checkboxes</option>
								{{-- <option value="matching" {{ $question['answer_type'] == 'matching' ? 'selected':'' }}>Matching</option> --}}
								{{-- <option value="likert_scale" {{ $question['answer_type'] == 'likert_scale' ? 'selected':'' }}>Likert scale</option> --}}
								<option value="boolean" {{ $question['answer_type'] == 'boolean' ? 'selected':'' }}>True or false</option>
								{{-- <option value="ranking" {{ $question['answer_type'] == 'ranking' ? 'selected':'' }}>Ranking</option> --}}
								<option value="multiple_choice_with_constraint" {{ $question['answer_type'] == 'multiple_choice_with_constraint' ? 'selected':'' }}>Multiple choice with constraint</option>
								<option value="date" {{ $question['answer_type'] == 'date' ? 'selected':'' }}>Date</option>
				        <option value="time" {{ $question['answer_type'] == 'time' ? 'selected':'' }}>Time</option>
				        <option value="datetime" {{ $question['answer_type'] == 'datetime' ? 'selected':'' }}>Date and time</option>
				        <option value="distributed_points" {{ $question['answer_type'] == 'distributed_points' ? 'selected':'' }}>Distributed points</option>
							</select>
						</div>
						@if ($errors->has($section_type . '.' . $section_index . '.question.' . $questionIndex . '.answer_type'))
				      <span class="help-block">
				        <strong>{{ $errors->first($section_type . '.' . $section_index . '.question.' . $questionIndex . '.answer_type') }}</strong>
				      </span>
				    @endif
					</div>
				</div>
				<div class="row answer-options">
					@if($question['answer_type'] == 'free_text')
						@include('survey.forms.answer_configs.free_text', ['section_type' => $section_type, 'section_index' => $section_index, 'question_index' => $questionIndex, 'question' => $question, 'is_exist_answer' => $is_exist_answer])
					@elseif($question['answer_type'] == 'multichoice')
						@include('survey.forms.answer_configs.multichoice', ['section_type' => $section_type, 'section_index' => $section_index, 'question_index' => $questionIndex, 'question' => $question, 'is_exist_answer' => $is_exist_answer])
					@elseif($question['answer_type'] == 'checkboxes')
						@include('survey.forms.answer_configs.checkboxes', ['section_type' => $section_type, 'section_index' => $section_index, 'question_index' => $questionIndex, 'question' => $question, 'is_exist_answer' => $is_exist_answer])
					@elseif($question['answer_type'] == 'matching')
						@include('survey.forms.answer_configs.matching', ['section_type' => $section_type, 'section_index' => $section_index, 'question_index' => $questionIndex, 'question' => $question, 'is_exist_answer' => $is_exist_answer])
					@elseif($question['answer_type'] == 'likert_scale')
						@include('survey.forms.answer_configs.likert_scale', ['section_type' => $section_type, 'section_index' => $section_index, 'question_index' => $questionIndex, 'question' => $question, 'is_exist_answer' => $is_exist_answer])
					@elseif($question['answer_type'] == 'boolean')
						@include('survey.forms.answer_configs.boolean', ['section_type' => $section_type, 'section_index' => $section_index, 'question_index' => $questionIndex, 'question' => $question, 'is_exist_answer' => $is_exist_answer])
					@elseif($question['answer_type'] == 'ranking')
						@include('survey.forms.answer_configs.ranking', ['section_type' => $section_type, 'section_index' => $section_index, 'question_index' => $questionIndex, 'question' => $question, 'is_exist_answer' => $is_exist_answer])
					@elseif($question['answer_type'] == 'multiple_choice_with_constraint')
						@include('survey.forms.answer_configs.multiple_choice_with_constraint', ['section_type' => $section_type, 'section_index' => $section_index, 'question_index' => $questionIndex, 'question' => $question, 'is_exist_answer' => $is_exist_answer])
					@elseif($question['answer_type'] == 'date')
						@include('survey.forms.answer_configs.date', ['section_type' => $section_type, 'section_index' => $section_index, 'question_index' => $questionIndex, 'question' => $question, 'is_exist_answer' => $is_exist_answer])
					@elseif($question['answer_type'] == 'time')
						@include('survey.forms.answer_configs.time', ['section_type' => $section_type, 'section_index' => $section_index, 'question_index' => $questionIndex, 'question' => $question, 'is_exist_answer' => $is_exist_answer])
					@elseif($question['answer_type'] == 'datetime')
						@include('survey.forms.answer_configs.datetime', ['section_type' => $section_type, 'section_index' => $section_index, 'question_index' => $questionIndex, 'question' => $question, 'is_exist_answer' => $is_exist_answer])
					@elseif($question['answer_type'] == 'distributed_points')
						@include('survey.forms.answer_configs.distributed_points', ['section_type' => $section_type, 'section_index' => $section_index, 'question_index' => $questionIndex, 'question' => $question, 'is_exist_answer' => $is_exist_answer])
					@endif
				</div>
				<br>
				<div class="box-footer">
					<div class="row">
						<div class="pull-right">
							<label>Required: </label>
							<input type="checkbox" {{ isset($question['is_required']) && $question['is_required'] ? 'checked':'' }} data-size="small" data-toggle="toggle" data-on="" data-off="" name="{{ $section_type }}[{{ $section_index }}][question][{{ $questionIndex }}][is_required]" {{ $is_exist_answer ? 'disabled':'' }}>
							<button class="btn btn-default remove-question" type="button" title="Remove question" {{ $is_exist_answer ? 'disabled':'' }}><i class="fa fa-fw fa-trash"></i></button>
						</div>
					</div>
				</div>
			</div>
		@endif
	@endforeach
@endif