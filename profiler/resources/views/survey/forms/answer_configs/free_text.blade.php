<div class="col-sm-12">
	<div class="row">
		<div class="form-group clearfix">
			<label class="col-sm-2">Attributes: </label>
		  <div class="col-sm-5">
				<select class="form-control select2 question-attributes" multiple="multiple" data-placeholder="Select some attributes" style="width: 100%;" name="{{ $section_type }}[{{ $section_index }}][question][{{ $question_index }}][answer_attributes][]" {{ $is_exist_answer ? 'disabled':'' }}>
	        @if(old('attributes'))
             @foreach(old('attributes') as $attribute)
	      			  <option value="{{ $attribute }}" {{ isset($question['answer_attributes']) && in_array($attribute, $question['answer_attributes']) ? 'selected':'' }}>{{ $attribute }}</option>
	      		@endforeach
	      	@elseif(isset($survey['attributes']))
	      		@foreach($survey['attributes'] as $attribute)
						  <option value="{{ $attribute }}" {{ isset($question['answer_attributes']) && in_array($attribute, $question['answer_attributes']) ? 'selected':'' }}>{{ $attribute }}</option>
						@endforeach
          @else
		        @foreach($attributes as $attribute)
						  <option value="{{ $attribute->name }}" {{ isset($question['answer_attributes']) && in_array($attribute->id, $question['answer_attributes']) ? 'selected':'' }}>{{ $attribute->name }}</option>
						@endforeach
					@endif
	      </select>
      </div>
    </div>
    <div class="form-group clearfix">
			<label class="col-sm-2">Score: </label>
		  <div class="col-sm-5">
				<input type="number" placeholder="Enter score" class="form-control input-number" name="{{ $section_type }}[{{ $section_index }}][question][{{ $question_index }}][answer_scores]" value="{{ $question['answer_scores'] }}" {{ $is_exist_answer ? 'disabled':'' }}>
      </div>
    </div>
	</div>
</div>