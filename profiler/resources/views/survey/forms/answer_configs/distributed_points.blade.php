<div class="col-sm-12">
	<div class="row">
		<div class="form-group clearfix point {{ $errors->has($section_type . '.' . $section_index . '.question.' . $questionIndex . '.answer_configs.point') ? ' has-error' : '' }}">
			<label class="col-sm-2">Point on hand (<span class="text-red">*</span>) :</label>
  		<div class="col-sm-5">
    		<input type="number" class="form-control answer-configs input-number" placeholder="Point on hand" name="{{ $section_type }}[{{ $section_index }}][question][{{ $question_index }}][answer_configs][point]" value="{{ $question['answer_configs']['point'] }}" {{ $is_exist_answer ? 'disabled':'required' }}>
    	</div>
    	<div class="help-block with-errors"></div>
  	</div>
	</div>

	{{-- <label>Answer options (<span class="text-red">*</span>) :</label> --}}
	<div class="row answer-elements">
		<!-- render template here -->
		@foreach($question['answer_configs']['options'] as $answerIndex => $answerConfig)
			<div class="row">
				<div class="col-sm-4 {{ $errors->has($section_type . '.' . $section_index . '.question.' . $questionIndex . '.answer_configs.options.' . $answerIndex) ? ' has-error' : '' }}">
					<label>Option (<span class="text-red">*</span>) :</label>
					<input type="text" placeholder="Label option" class="form-control answer-configs" name="{{ $section_type }}[{{ $section_index }}][question][{{ $question_index }}][answer_configs][options][]" value="{{ $answerConfig }}" {{ $is_exist_answer ? 'disabled':'required' }}>
					<div class="help-block with-errors"></div>
				</div>
				<div class="col-sm-4">
					<label>Attributes :</label>
					<select class="form-control select2 question-attributes" multiple="multiple" data-placeholder="Select some attributes" style="width: 100%;" name="{{ $section_type }}[{{ $section_index }}][question][{{ $question_index }}][answer_attributes][{{ $answerIndex }}][]" {{ $is_exist_answer ? 'disabled':'' }}>
			    	@if(old('attributes'))
			    		@foreach(old('attributes') as $attribute)
							  <option value="{{ $attribute }}" {{ isset($question['answer_attributes'][$answerIndex]) && in_array($attribute, $question['answer_attributes'][$answerIndex]) ? 'selected':'' }}>{{ $attribute }}</option>
							@endforeach
						@elseif(isset($survey['attributes']))
		      		@foreach($survey['attributes'] as $attribute)
							  <option value="{{ $attribute }}" {{ isset($question['answer_attributes']) && in_array($attribute, $question['answer_attributes']) ? 'selected':'' }}>{{ $attribute }}</option>
							@endforeach
						@else
			        @foreach($attributes as $attribute)
							  <option value="{{ $attribute->name }}" {{ isset($question['answer_attributes'][$answerIndex]) && in_array($attribute->id, $question['answer_attributes'][$answerIndex]) ? 'selected':'' }}>{{ $attribute->name }}</option>
							@endforeach
						@endif
		      </select>
				</div>
				<div class="col-sm-3">
					<label>Score :</label>
					<input type="number" placeholder="Enter score" class="form-control input-number" name="{{ $section_type }}[{{ $section_index }}][question][{{ $question_index }}][answer_scores][]" value="{{ $question['answer_scores'][$answerIndex] }}" {{ $is_exist_answer ? 'disabled':'' }}>
				</div>
				<div class="col-sm-1">
					<label>Action</label>
					<button class="btn btn-default delete-answer-option-element" {{ $is_exist_answer ? 'disabled':'' }}><i class="fa fa-fw fa-remove"></i></button>
				</div>
			</div>
		@endforeach
	</div>
  <div class="row">
		<div class="row col-sm-12">
			<div class="form-group col-md-1 col-md-offset-11">
				<button class="btn btn-default add-answer-option-element" data-template-element-name="template-distributed_points-answer-element" {{ $is_exist_answer ? 'disabled':'' }}><i class="fa fa-fw fa-plus-square"></i></button>
			</div>
		</div>
	</div>
</div>