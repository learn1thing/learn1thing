<div class="col-sm-12">
	<div class="row">
		<div class="form-group clearfix">
			<label class="col-sm-2">Attributes: </label>
		  <div class="col-sm-5">
				<select class="form-control select2 question-attributes" multiple="multiple" data-placeholder="Select some attributes" style="width: 100%;" name="{{ $section_type }}[{{ $section_index }}][question][{{ $question_index }}][answer_attributes][]" {{ $is_exist_answer ? 'disabled':'' }}>
					@if(old('attributes'))
		        @foreach(old('attributes') as $attribute)
						  <option value="{{ $attribute }}" {{ isset($question['answer_attributes']) && in_array($attribute, $question['answer_attributes']) ? 'selected':'' }}>{{ $attribute }}</option>
						@endforeach
					@elseif(isset($survey['attributes']))
	      		@foreach($survey['attributes'] as $attribute)
						  <option value="{{ $attribute }}" {{ isset($question['answer_attributes']) && in_array($attribute, $question['answer_attributes']) ? 'selected':'' }}>{{ $attribute }}</option>
						@endforeach
					@else
						@foreach($attributes as $attribute)
						  <option value="{{ $attribute->name }}" {{ isset($question['answer_attributes']) && in_array($attribute->id, $question['answer_attributes']) ? 'selected':'' }}>{{ $attribute->name }}</option>
						@endforeach
					@endif
	      </select>
      </div>
    </div>
    <div class="form-group clearfix">
			<label class="col-sm-2">{{ trans('app.label.start_date') }} (<span class="text-red">*</span>) :</label>
			<div class="col-sm-5">
				<div class="input-group date">
					<div class="input-group-addon">
						<i class="fa fa-calendar"></i>
					</div>
					<input type="text" class="form-control start_date datepicker" placeholder="Enter the start date"  name="{{ $section_type }}[{{ $section_index }}][question][{{ $question_index }}][answer_configs][start_date]" value="{{ isset($question['answer_configs']->start_date) ? $question['answer_configs']->start_date:'' }}" required>
				</div>
			</div>
			<div class="col-sm-5 help-block with-errors"></div>
		</div>
		<div class="form-group clearfix">
			<label class="col-sm-2">{{ trans('app.label.end_date') }} (<span class="text-red">*</span>) :</label>
			<div class="col-sm-5">
				<div class="input-group date">
					<div class="input-group-addon">
						<i class="fa fa-calendar"></i>
					</div>
					<input type="text" class="form-control end_date datepicker" placeholder="Enter the end date" name="{{ $section_type }}[{{ $section_index }}][question][{{ $question_index }}][answer_configs][end_date]" value="{{ isset($question['answer_configs']->end_date) ? $question['answer_configs']->end_date:'' }}" data-date-start-date="{{ isset($question['answer_configs']->start_date) ? $question['answer_configs']->start_date:'' }}" required>
				</div>
			</div>
			<div class="col-sm-5 help-block with-errors"></div>
		</div>
    <div class="form-group clearfix">
			<label class="col-sm-2">Score: </label>
		  <div class="col-sm-5">
				<input type="number" placeholder="Enter score" class="form-control input-number" name="{{ $section_type }}[{{ $section_index }}][question][{{ $question_index }}][answer_scores]" value="{{ $question['answer_scores'] }}" {{ $is_exist_answer ? 'disabled':'' }}>
      </div>
    </div>
	</div>
</div>