<div class="row">
    <div class="col-sm-4 {{ $errors->has($section_type . '.' . $section_index . '.question.' . $questionIndex . '.answer_configs.0') ? ' has-error' : '' }}">
        <label>Label true (<span class="text-red">*</span>) :</label>
        <input type="text" class="form-control answer-configs" placeholder="Label for true value" name="{{ $section_type }}[{{ $section_index}}][question][{{ $question_index }}][answer_configs][]" value="{{ isset($question['answer_configs'][1]) ? $question['answer_configs'][0]:'' }}" {{ $is_exist_answer ? 'disabled':'required' }}>
        <div class="help-block with-errors"></div>
    </div>
    <div class="col-sm-4">
        <label>Attributes :</label>
        <select class="form-control select2 question-attributes" multiple="multiple" data-placeholder="Select some attributes" style="width: 100%;" name="{{ $section_type }}[{{ $section_index}}][question][{{ $question_index }}][answer_attributes][0][]" {{ $is_exist_answer ? 'disabled':'' }}>
            @if(old('attributes'))
                @foreach(old('attributes') as $attribute)
                      <option value="{{ $attribute }}" {{ isset($question['answer_attributes'][0]) && in_array($attribute, $question['answer_attributes'][0]) ? 'selected':'' }}>{{ $attribute }}</option>
                @endforeach
            @elseif(isset($survey['attributes']))
                @foreach($survey['attributes'] as $attribute)
                          <option value="{{ $attribute }}" {{ isset($question['answer_attributes']) && in_array($attribute, $question['answer_attributes']) ? 'selected':'' }}>{{ $attribute }}</option>
                        @endforeach
            @else
                @foreach($attributes as $attribute)
                    <option value="{{ $attribute->name }}" {{ isset($question['answer_attributes'][0]) && in_array($attribute->id, $question['answer_attributes'][0]) ? 'selected':'' }}>{{ $attribute->name }}</option>
                @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-4">
        <label>Score :</label>
        <input type="number" placeholder="Enter score" class="form-control input-number" name="{{ $section_type }}[{{ $section_index}}][question][{{ $question_index }}][answer_scores][0]" value="{{ isset($question['answer_scores'][0]) ? $question['answer_scores'][0]:'' }}" {{ $is_exist_answer ? 'disabled':'' }}>
    </div>
</div>
<div class="row">
    <div class="col-sm-4 {{ $errors->has($section_type . '.' . $section_index . '.question.' . $questionIndex . '.answer_configs.1') ? ' has-error' : '' }}">
        <label>Label false (<span class="text-red">*</span>) :</label>
        <input type="text" class="form-control answer-configs" placeholder="Label for false value" name="{{ $section_type }}[{{ $section_index}}][question][{{ $question_index }}][answer_configs][]" value="{{ isset($question['answer_configs'][1]) ? $question['answer_configs'][1]:'' }}" {{ $is_exist_answer ? 'disabled':'required' }}>
        <div class="help-block with-errors"></div>
    </div>
    <div class="col-sm-4">
        <label>Attributes :</label>
        <select class="form-control select2 question-attributes" multiple="multiple" data-placeholder="Select some attributes" style="width: 100%;" name="{{ $section_type }}[{{ $section_index}}][question][{{ $question_index }}][answer_attributes][1][]" {{ $is_exist_answer ? 'disabled':'' }}>
            @if(old('attributes'))
                @foreach(old('attributes') as $attribute)
                      <option value="{{ $attribute }}" {{ isset($question['answer_attributes'][1]) && in_array($attribute, $question['answer_attributes'][1]) ? 'selected':'' }}>{{ $attribute }}</option>
                @endforeach
            @elseif(isset($survey['attributes']))
                @foreach($survey['attributes'] as $attribute)
                          <option value="{{ $attribute }}" {{ isset($question['answer_attributes']) && in_array($attribute, $question['answer_attributes']) ? 'selected':'' }}>{{ $attribute }}</option>
                        @endforeach
            @else
                @foreach($attributes as $attribute)
                    <option value="{{ $attribute->name }}" {{ isset($question['answer_attributes'][1]) && in_array($attribute->id, $question['answer_attributes'][1]) ? 'selected':'' }}>{{ $attribute->name }}</option>
                @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-4">
        <label>Score :</label>
        <input type="number" placeholder="Enter score" class="form-control input-number" name="{{ $section_type }}[{{ $section_index}}][question][{{ $question_index }}][answer_scores][1]" value="{{ isset($question['answer_scores'][1]) ? $question['answer_scores'][1]:'' }}" {{ $is_exist_answer ? 'disabled':'' }}>
    </div>
</div>