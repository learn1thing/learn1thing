<div class="form-group clearfix {{ $errors->has($section_type . '.' . $section_index . '.question.' . $questionIndex . '.answer_configs.minimal') ? ' has-error' : '' }}">
	<label class="col-sm-2">Minimal (<span class="text-red">*</span>) :</label>
	<div class="col-sm-5">
		<input type="number" class="form-control answer_configs input-number" placeholder="Minimal value of ranking" name="{{ $section_type }}[{{ $section_index }}][question][{{ $question_index }}][answer_configs][minimal]" value="{{ $question['answer_configs']['minimal'] }}" {{ $is_exist_answer ? 'disabled':'required' }}>
	</div>
	<div class="help-block with-errors"></div>
</div>
<div class="form-group clearfix {{ $errors->has($section_type . '.' . $section_index . '.question.' . $questionIndex . '.answer_configs.interval') ? ' has-error' : '' }}">
	<label class="col-sm-2">Interval (<span class="text-red">*</span>) :</label>
	<div class="col-sm-5">
		<input type="number" class="form-control answer_configs input-number" placeholder="Interval value of ranking" name="{{ $section_type }}[{{ $section_index }}][question][{{ $question_index }}][answer_configs][interval]" value="{{ $question['answer_configs']['interval'] }}" {{ $is_exist_answer ? 'disabled':'required' }}>
	</div>
	<div class="help-block with-errors"></div>
</div>
<div class="form-group clearfix {{ $errors->has($section_type . '.' . $section_index . '.question.' . $questionIndex . '.answer_configs.maximal') ? ' has-error' : '' }}">
	<label class="col-sm-2">Maximal (<span class="text-red">*</span>) :</label>
	<div class="col-sm-5">
		<input type="number" class="form-control answer_configs input-number" placeholder="Maximal value of ranking" name="{{ $section_type }}[{{ $section_index }}][question][{{ $question_index }}][answer_configs][maximal]" value="{{ $question['answer_configs']['maximal'] }}" {{ $is_exist_answer ? 'disabled':'required' }}>
	</div>
	<div class="help-block with-errors"></div>
</div>
<div class="form-group clearfix">
	<label class="col-sm-2">Attributes: </label>
	<div class="col-sm-5">
		<select class="form-control select2 question-attributes" multiple="multiple" data-placeholder="Select some attributes" style="width: 100%;" name="{{ $section_type }}[{{ $section_index }}][question][{{ $question_index }}][answer_attributes][]" {{ $is_exist_answer ? 'disabled':'' }}>
  		@if(old('attributes'))
        @foreach(old('attributes') as $attribute)
				  <option value="{{ $attribute }}" {{ isset($question['answer_attributes']) && in_array($attribute, $question['answer_attributes']) ? 'selected':'' }}>{{ $attribute }}</option>
				@endforeach
			@elseif(isset($survey['attributes']))
    		@foreach($survey['attributes'] as $attribute)
				  <option value="{{ $attribute }}" {{ isset($question['answer_attributes']) && in_array($attribute, $question['answer_attributes']) ? 'selected':'' }}>{{ $attribute }}</option>
				@endforeach
	    @else
	      @foreach($attributes as $attribute)
				  <option value="{{ $attribute->name }}" {{ isset($question['answer_attributes']) && in_array($attribute->id, $question['answer_attributes']) ? 'selected':'' }}>{{ $attribute->name }}</option>
				@endforeach
			@endif
    </select>
	</div>
</div>
<div class="form-group clearfix">
	<label class="col-sm-2">Score: </label>
	<div class="col-sm-5">
		<input type="number" placeholder="Enter score" class="form-control input-number" name="{{ $section_type }}[{{ $section_index }}][question][{{ $question_index }}][answer_scores]" value="{{ $question['answer_scores'] }}" {{ $is_exist_answer ? 'disabled':'' }}>
	</div>
</div>