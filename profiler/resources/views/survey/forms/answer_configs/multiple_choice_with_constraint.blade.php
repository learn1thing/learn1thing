<div class="col-sm-12">
  <div class="row">
    <div class="form-group clearfix {{ $errors->has($section_type . '.' . $section_index . '.question.' . $questionIndex . '.coordinate') ? ' has-error' : '' }}">
      <label class="col-sm-2">Coordinate view (<span class="text-red">*</span>) :</label>
      <div class="col-sm-5">
        <select name="{{ $section_type }}[{{ $section_index }}][question][{{ $question_index }}][coordinate]" class="form-control" {{ $is_exist_answer ? 'disabled':'required' }}>
          <option value=""></option>
          <option value="horizontal" {{ $question['coordinate'] === 'horizontal' ? 'selected':'' }}>Horizontal</option>
          <option value="vertical" {{ $question['coordinate'] === 'vertical' ? 'selected':'' }}>Vertical</option>
        </select>
      </div>
      <div class="help-block with-errors"></div>
      {{-- @if ($errors->has($section_type . '.' . $section_index . '.question.' . $questionIndex . '.coordinate'))
        <span class="help-block">
          <strong>{{ $errors->first($section_type . '.' . $section_index . '.question.' . $questionIndex . '.coordinate') }}</strong>
        </span>
      @endif --}}
    </div>
  </div>
	<div class="row">
		<div class="form-group clearfix max-input {{ $errors->has($section_type . '.' . $section_index . '.question.' . $questionIndex . '.answer_configs.points') ? ' has-error' : '' }}">
      <label class="col-sm-2">Max Selected: </label>
  		<div class="col-sm-5">
    		<input type="number" class="form-control answer-configs input-number" placeholder="Max value of options selected" name="{{ $section_type }}[{{ $section_index }}][question][{{ $question_index }}][answer_configs][points]" value="{{ $question['answer_configs']['points'] }}" {{ $is_exist_answer ? 'disabled':'required' }}>
    	</div>
      <div class="help-block with-errors"></div>
      {{-- @if ($errors->has($section_type . '.' . $section_index . '.question.' . $questionIndex . '.answer_configs.points'))
        <span class="help-block">
          <strong>{{ $errors->first($section_type . '.' . $section_index . '.question.' . $questionIndex . '.answer_configs.points') }}</strong>
        </span>
      @endif --}}
    </div>
	</div>

	{{-- <label>Answer options (<span class="text-red">*</span>) :</label> --}}
	<div class="row answer-elements">
		<!-- render template here -->
    @foreach($question['answer_configs']['options'] as $answerIndex => $answerConfig)
      <div class="row">
        <div class="col-sm-4 {{ $errors->has($section_type . '.' . $section_index . '.question.' . $questionIndex . '.answer_configs.options.' . $answerIndex) ? ' has-error' : '' }}">
          <label>Option (<span class="text-red">*</span>) :</label>
          <div class="input-group">
            <span class="input-group-addon">
              <input type="checkbox" disabled="disabled">
            </span>
            <input type="text" placeholder="Enter the option text" class="form-control answer-configs" name="{{ $section_type }}[{{ $section_index }}][question][{{ $question_index }}][answer_configs][options][]" value="{{ $answerConfig }}" {{ $is_exist_answer ? 'disabled':'required' }}>
          </div>
          <div class="help-block with-errors"></div>
        </div>
        <div class="col-sm-4">
          <label>Attributes :</label>
          <select class="form-control select2 question-attributes" multiple="multiple" data-placeholder="Select some attributes" style="width: 100%;" name="{{ $section_type }}[{{ $section_index }}][question][{{ $question_index }}][answer_attributes][{{ $answerIndex }}][]" {{ $is_exist_answer ? 'disabled':'' }}>
            @if(old('attributes'))
              @foreach(old('attributes') as $attribute)
                <option value="{{ $attribute }}" {{ isset($question['answer_attributes'][$answerIndex]) && in_array($attribute, $question['answer_attributes'][$answerIndex]) ? 'selected':'' }}>{{ $attribute }}</option>
              @endforeach
            @elseif(isset($survey['attributes']))
              @foreach($survey['attributes'] as $attribute)
                <option value="{{ $attribute }}" {{ isset($question['answer_attributes']) && in_array($attribute, $question['answer_attributes']) ? 'selected':'' }}>{{ $attribute }}</option>
              @endforeach
            @else
              @foreach($attributes as $attribute)
                <option value="{{ $attribute->name }}" {{ isset($question['answer_attributes'][$answerIndex]) && in_array($attribute->id, $question['answer_attributes'][$answerIndex]) ? 'selected':'' }}>{{ $attribute->name }}</option>
              @endforeach
            @endif
          </select>
          </select>
        </div>
        <div class="col-sm-3">
          <label>Score :</label>
          <input type="number" placeholder="Enter score" class="form-control input-number" name="{{ $section_type }}[{{ $section_index }}][question][{{ $question_index }}][answer_scores][]" value="{{ $question['answer_scores'][$answerIndex] }}" {{ $is_exist_answer ? 'disabled':'' }}>
        </div>
        <div class="col-sm-1">
          <label>Action</label>
          <button class="btn btn-default delete-answer-option-element" {{ $is_exist_answer ? 'disabled':'' }}><i class="fa fa-fw fa-remove"></i></button>
        </div>
      </div>
    @endforeach
	</div>
  <div class="row">
    <div class="row col-sm-12">
      <div class="form-group col-md-1 col-md-offset-11">
        <button class="btn btn-default add-answer-option-element" data-template-element-name="template-multiple_choice_with_constraint-answer-element" {{ $is_exist_answer ? 'disabled':'' }}><i class="fa fa-fw fa-plus-square"></i></button>
      </div>
    </div>
  </div>
</div>