@if(count($sections) === 0)
  <div class="panel box box-primary section">
    <div class="box-header with-border">
      <h4 class="box-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#{{ $section_type }}-section-1">
          Section 1
        </a>
      </h4>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool delete-section" title="Remove"><i class="fa fa-remove"></i></button>
      </div>
    </div>
    <div id="{{ $section_type }}-section-1" class="panel-collapse collapse in">
      <div class="box-body">
        <div class="form-group clearfix">
          <label class="col-sm-2">Title (<span class="text-red">*</span>) :</label>
          <div class="col-sm-5">
            <input type="text" placeholder="Enter title" class="form-control" name="{{ $section_type }}[0][title]" required>
          </div>
          <div class="help-block with-errors"></div>
        </div>
        <div class="form-group clearfix">
          <label class="col-sm-2">Description (<span class="text-red">*</span>) :</label>
          <div class="col-sm-5">
            <textarea id="inputDescription" class="form-control" rows="3" placeholder="Enter description" name="{{ $section_type }}[0][description]" {{ $is_exist_answer ? 'disabled':'required' }}></textarea>
          </div>
          <div class="help-block with-errors"></div>
        </div>
      	<div class="form-group clearfix">
          <label class="col-sm-2">Automatic numbering (<span class="text-red">*</span>) :</label>
          <div class="col-sm-5">
            <select name="{{ $section_type }}[0][automatic_numbering]" class="form-control select-automatic-numbering" {{ $is_exist_answer ? 'disabled':'required' }}>
              <option></option>
              <option value="yes">Yes</option>
              <option value="no">No</option>
            </select>
          </div>
          <div class="help-block with-errors"></div>
        </div>
        <div class="form-group clearfix">
      		<label class="col-sm-2">Ordering type (<span class="text-red">*</span>) :</label>
      		<div class="col-sm-5">
  		      <select name="{{ $section_type }}[0][ordering_type]" class="form-control select-ordering-type" {{ $is_exist_answer ? 'disabled':'' }}>
              <option value=""></option>
  		        <option value="continue">Continue from previous section</option>
  		      	<option value="reset">Reset</option>
  		      </select>
  		    </div>
          <div class="help-block with-errors"></div>
      	</div>
      	<div class="form-group clearfix">
      		<label class="col-sm-2">Numbering type (<span class="text-red">*</span>) :</label>
      		<div class="col-sm-5">
  		      <select name="{{ $section_type }}[0][type]" class="form-control select-numbering-type" {{ $is_exist_answer ? 'disabled':'' }}>
              <option value=""></option>
  		      	<option value="arabic">Arabic</option>
  		        <option value="alphabet">Alphabet</option>
  		        <option value="roman">Roman</option>
  		      </select>
  		    </div>
          <div class="help-block with-errors"></div>
      	</div>
    		<div class="questions">
      		<!-- render questions here -->
          @include('survey.forms.question', ['section_type' => $section_type, 'section_index' => 0, 'section' => [], 'questions' => [], 'is_exist_answer' => $is_exist_answer])
      	</div>
      	<div class="{{ $is_exist_answer ? 'hide':'' }}">
      		<a href="#" class="btn btn-default pull-right add-question">Add Question</a>
      	</div>
      </div>
    </div>
  </div>
@else
  @foreach($sections as $sectionIndex => $section)
    <div class="panel box box-primary section">
      <div class="box-header with-border">
        <h4 class="box-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#{{ $section_type }}-section-{{ $sectionIndex+1 }}">
            Section {{ $sectionIndex + 1 }}
          </a>
        </h4>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool delete-section" title="Remove"><i class="fa fa-remove"></i></button>
        </div>
      </div>
      <div id="{{ $section_type }}-section-{{ $sectionIndex+1 }}" class="panel-collapse collapse in">
        <div class="box-body">
          <div class="form-group clearfix {{ $errors->has($section_type . '.' . $sectionIndex . '.title') ? ' has-error' : '' }}">
            <label class="col-sm-2">Title (<span class="text-red">*</span>) :</label>
            <div class="col-sm-5">
              <input type="text" placeholder="Enter title" class="form-control" name="{{ $section_type }}[{{ $sectionIndex }}][title]" value="{{ isset($section['title']) ? $section['title']:'' }}" {{ $is_exist_answer ? 'disabled':'required' }}>
            </div>
            <div class="help-block with-errors"></div>
            {{-- @if ($errors->has($section_type . '.' . $sectionIndex . '.title'))
              <span class="help-block">
                <strong>{{ $errors->first($section_type . '.' . $sectionIndex . '.title') }}</strong>
              </span>
            @endif --}}
          </div>
          <div class="form-group clearfix {{ $errors->has($section_type . '.' . $sectionIndex . '.description') ? ' has-error' : '' }}">
            <label class="col-sm-2">Description (<span class="text-red">*</span>) :</label>
            <div class="col-sm-5">
              <textarea id="inputDescription" class="form-control" rows="3" placeholder="Enter description" name="{{ $section_type }}[{{ $sectionIndex }}][description]" {{ $is_exist_answer ? 'disabled':'required' }}>{{ isset($section['description']) ? $section['description']:'' }}</textarea>
            </div>
            <div class="help-block with-errors"></div>
            {{-- @if ($errors->has($section_type . '.' . $sectionIndex . '.description'))
              <span class="help-block">
                <strong>{{ $errors->first($section_type . '.' . $sectionIndex . '.description') }}</strong>
              </span>
            @endif --}}
          </div>
          <div class="form-group clearfix {{ $errors->has($section_type . '.' . $sectionIndex . '.automatic_numbering') ? ' has-error' : '' }}">
            <label class="col-sm-2">Automatic numbering (<span class="text-red">*</span>) :</label>
            <div class="col-sm-5">
              <select name="{{ $section_type }}[{{ $sectionIndex }}][automatic_numbering]" class="form-control select-automatic-numbering"  {{ $is_exist_answer ? 'disabled':'required' }}>
                <option {{ isset($section['automatic_numbering']) && $section['automatic_numbering'] == '' ? 'selected':'' }} }}></option>
                <option value="yes" {{ isset($section['automatic_numbering']) && $section['automatic_numbering'] == 'yes' ? 'selected':'' }} }}>Yes</option>
                <option value="no" {{ isset($section['automatic_numbering']) && $section['automatic_numbering'] == 'no' ? 'selected':'' }} }}>No</option>
              </select>
            </div>
            <div class="help-block with-errors"></div>
            {{-- @if ($errors->has($section_type . '.' . $sectionIndex . '.automatic_numbering'))
              <span class="help-block">
                <strong>{{ $errors->first($section_type . '.' . $sectionIndex . '.automatic_numbering') }}</strong>
              </span>
            @endif --}}
          </div>
          <div class="form-group clearfix {{ $errors->has($section_type . '.' . $sectionIndex . '.ordering_type') ? ' has-error' : '' }}">
            <label class="col-sm-2">Ordering type :</label>
            <div class="col-sm-5">
              <select name="{{ $section_type }}[{{ $sectionIndex }}][ordering_type]" class="form-control select-ordering-type" {{ isset($section['automatic_numbering']) && $section['automatic_numbering'] == 'no' || $is_exist_answer ? 'disabled':'' }}>
                <option value="" {{ isset($section['ordering_type']) && $section['ordering_type'] === "" ? 'selected':'' }}></option>
                <option value="continue" {{ isset($section['ordering_type']) && $section['ordering_type'] === "continue" ? 'selected':'' }}>Continue from previous section</option>
                <option value="reset" {{ isset($section['ordering_type']) && $section['ordering_type'] === "reset" ? 'selected':'' }}>Reset</option>
              </select>
            </div>
            <div class="help-block with-errors"></div>
            {{-- @if ($errors->has($section_type . '.' . $sectionIndex . '.ordering_type'))
              <span class="help-block">
                <strong>{{ $errors->first($section_type . '.' . $sectionIndex . '.ordering_type') }}</strong>
              </span>
            @endif --}}
          </div>
          <div class="form-group clearfix {{ $errors->has($section_type . '.' . $sectionIndex . '.type') ? ' has-error' : '' }}">
            <label class="col-sm-2">Numbering type (<span class="text-red">*</span>) :</label>
            <div class="col-sm-5">
              <select name="{{ $section_type }}[{{ $sectionIndex }}][type]" class="form-control select-numbering-type" {{ isset($section['automatic_numbering']) && $section['automatic_numbering'] == 'no' || $is_exist_answer ? 'disabled':'' }}>
                <option value="" {{ isset($section['type']) && $section['type'] == '' ? 'selected':'' }}></option>
                <option value="arabic" {{ isset($section['type']) && $section['type'] == 'arabic' ? 'selected':'' }}>Arabic</option>
                <option value="alphabet" {{ isset($section['type']) && $section['type'] == 'alphabet' ? 'selected':'' }}>Alphabet</option>
                <option value="roman" {{ isset($section['type']) && $section['type'] == 'roman' ? 'selected':'' }}>Roman</option>
              </select>
            </div>
            <div class="help-block with-errors"></div>
            {{-- @if ($errors->has($section_type . '.' . $sectionIndex . '.type'))
              <span class="help-block">
                <strong>{{ $errors->first($section_type . '.' . $sectionIndex . '.type') }}</strong>
              </span>
            @endif --}}
          </div>
          <div class="questions">
            @include('survey.forms.question', ['section_type' => $section_type, 'section_index' => $sectionIndex, 'section' => $section, 'questions' => $section['question'], 'is_exist_answer' => $is_exist_answer])
          </div>
          <div class="{{ $is_exist_answer ? 'hide':'' }}">
            <a href="#" class="btn btn-default pull-right add-question">Add Question</a>
          </div>
        </div>
      </div>
    </div>
  @endforeach
@endif