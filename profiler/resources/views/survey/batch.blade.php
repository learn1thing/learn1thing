<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Data Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {!! $dataTable->table(['id' => 'datatable-batch', 'class' => 'table table-bordered table-striped', 'cellspacing'=>"0", 'width'=>"100%"]) !!}
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>

{!! $dataTable->scripts() !!}

<script type="text/javascript">
    $(document).ready(function(){
        $("table[id^='datatable-']").parent().addClass('table-responsive');
    })
</script>