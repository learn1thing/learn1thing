@extends('layouts.admin_template')

@push('plugins-css')
    {!! Html::style('vendor/AdminLTE/plugins/datatables/dataTables.bootstrap.css') !!}
@endpush

@push('plugins-js')
    {!! Html::script('vendor/AdminLTE/plugins/datatables/jquery.dataTables.min.js') !!}
    {!! Html::script('vendor/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js') !!}
@endpush

@push('custom-js')
    {!! $dataTable->scripts() !!}

    <script type="text/javascript">
        $(document).ready(function(){
            $("table[id^='datatable-']").parent().addClass('table-responsive');
        })
    </script>

    {!! Html::script('js/global.js') !!}

    <script type="text/javascript">
        $(document).on('click', '.btn-ajax', function(){
            $('.modal-body-message').addClass('hide');
            $("#batch-modal").modal();
            var dataAction = $(this).data('action');
            $.ajax({
                url: $(this).data("href")
                , beforeSend: function() {
                    $(".spinner").removeClass('hide');
                    $(".spinner-wrapper").css('z-index', 0);
                }
                , success: function(result) {
                    if (typeof result == 'string') {
                        $(".modal-body-content").html(result);
                    } else {
                        $("#datatable-"+result.position).DataTable().draw();
                        if (result.position == 'batch' || result.position == 'template') {
                            message = $('.modal-body-message');
                            message.html(result.message);
                            message.removeClass('alert-error', 'alert-success');
                            message.addClass('alert-'+result.status);
                            message.removeClass('hide');
                        }
                    }

                    if(dataAction == "set_report") {
                        // $('#dataTableBuilder').DataTable().draw();
                        $('#dataTableBuilder').DataTable().ajax.reload();
                    }
                }
                , error: function(result) {
                    alert("error");
                }
                , complete: function() {
                    $(".spinner").addClass('hide');
                    $(".spinner-wrapper").css('z-index', -1);
                }
            });
        })
    </script>
@endpush

@section('web-title', 'Survey - Survey List')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Survey</h3>
                <div class="pull-right box-tools">
                    <a href="{{ RouteHelper::token('surveys.create') }}" class="btn btn-primary btn-create">
                        {!! trans('app.label.create') !!}
                    </a>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {!! $dataTable->table(['class' => 'table table-bordered table-striped', 'cellspacing'=>"0", 'width'=>"100%"]) !!}
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
@include('layouts.delete')
@include('layouts.delete')

<div id="batch-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-error modal-body-message hide">error message</div>
                <div class="modal-body-content">
                    <p>Waiting data to load...</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="close">Cancel</button>
            </div>
        </div>
    </div>
</div>

@endsection