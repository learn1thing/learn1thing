@extends('layouts.admin_template')

@push('plugins-css')
	{!! Html::style('vendor/AdminLTE/plugins/datatables/dataTables.bootstrap.css') !!}
	{!! Html::style('vendor/AdminLTE/plugins/datepicker/datepicker3.css') !!}
	{!! Html::style('vendor/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.css') !!}
	{!! Html::style('vendor/bootstrap-toggle/css/bootstrap-toggle.css') !!}
	{!! Html::style('vendor/bootstrap-vertical-tabs/bootstrap.vertical-tabs.css') !!}
	{!! Html::style('vendor/AdminLTE/plugins/select2/select2.css') !!}
	{!! Html::style('vendor/AdminLTE/plugins/iCheck/all.css') !!}
	{!! Html::style('vendor/jquery-toast-plugin/src/jquery.toast.css') !!}
@endpush

@push('custom-css')
	<style type="text/css">
		.nav-tabs-custom {
			box-shadow: none;
		}
		.no-padding {
			padding: 0;
		}
	</style>
@endpush

@section('web-title', 'Survey - Edit Survey')

@section('content')
	<div class="row">
		<!-- form start -->
		{!! Form::open(array('url' => RouteHelper::token('surveys.update', ['id' => $survey['id']]), 'method'=>'PUT', 'id'=>'form-survey', 'class' => 'form-horizontal')) !!}
			<input type="hidden" name="is_exist_answer" value="{{ $is_exist_answer }}">
			<div class="col-md-12">
				<!-- Custom Tabs -->
	      <div class="nav-tabs-custom">
	        <ul class="nav nav-tabs">
	          <li class="active"><a href="#settings" data-toggle="tab">{{ trans('app.profiler.settings') }}</a></li>
	          <li><a href="#questions" data-toggle="tab">{{ trans('app.profiler.questions') }}</a></li>
	          <!--<li><a href="#statistics" data-toggle="tab">{{ trans('app.profiler.statistics') }}</a></li>-->
	        </ul>
	        <div class="tab-content">
	          <div class="tab-pane clearfix active" id="settings">
	            <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
								<label for="inputTitle" class="col-md-2 control-label">{{ trans('app.profiler.name') }} (<span class="text-red">*</span>) :</label>
								<div class="col-md-5">
									{{ Form::text('title', $survey['title'], array("id" => "inputTitle", "class" => "form-control", "placeholder" => trans('app.profiler.placeholder.name'), "required" => "required" )) }}
								</div>
								<div class="col-md-5 col-md-offset-2 help-block with-errors"></div>
								{{-- @if ($errors->has('title'))
									<span class="help-block">
										<strong>{{ $errors->first('title') }}</strong>
									</span>
								@endif --}}
							</div>
							<div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
								<label for="inputDescription" class="col-md-2 control-label">{{ trans('app.profiler.description') }} (<span class="text-red">*</span>) :</label>
								<div class="col-md-10">
									{{ Form::textarea('description', $survey['description'], array("id" => "inputDescription", "class" => "form-control", "rows" => "3", "placeholder" => trans('app.profiler.placeholder.description'), "required" => "required" )) }}
								</div>
								<div class="col-md-10 col-md-offset-2 help-block with-errors"></div>
								{{-- @if ($errors->has('description'))
									<span class="help-block">
										<strong>{{ $errors->first('description') }}</strong>
									</span>
								@endif --}}
							</div>
							<div class="form-group {{ $errors->has('instruction') ? ' has-error' : '' }}">
								<label for="inputInstruction" class="col-md-2 control-label">{{ trans('app.profiler.instruction') }} (<span class="text-red">*</span>) :</label>
								<div class="col-md-10">
									{{ Form::textarea('instruction', $survey['instruction'], array("id" => "inputInstruction", "class" => "form-control", "rows" => "3", "placeholder" => trans('app.profiler.placeholder.instruction'), "required" => true )) }}
								</div>
								<div class="col-md-10 col-md-offset-2 help-block with-errors"></div>
								{{-- @if ($errors->has('instruction'))
									<span class="help-block">
										<strong>{{ $errors->first('instruction') }}</strong>
									</span>
								@endif --}}
							</div>
							<div class="col-md-12 no-padding">
								<div class="form-group col-md-6 {{ $errors->has('batch_reference') ? ' has-error' : '' }}">
									<label for="inputBatchReference" class="col-md-4 control-label">{{ trans('app.profiler.batch_reference') }} (<span class="text-red">*</span>) :</label>
									<div class="col-md-8">
										{{ Form::text('batch_reference', $survey['batch_reference'], array("id" => "inputBatchReference", "class" => "form-control", "placeholder" => "Enter batch reference", "maxlength" => "10", "required" => "required" )) }}
									</div>
									<div class="col-md-8 col-md-offset-4 help-block with-errors"></div>
									{{-- @if ($errors->has('batch_reference'))
										<span class="help-block">
											<strong>{{ $errors->first('batch_reference') }}</strong>
										</span>
									@endif --}}
								</div>
								<div class="form-group col-md-6 {{ $errors->has('attributes') ? ' has-error' : '' }}">
									<label for="inputAttributes" class="col-md-4 control-label">{{ trans('app.profiler.attributes') }} :</label>
									<div class="col-md-8">
			        			<select id="inputAttributes" class="form-control input-attributes" multiple="multiple" data-placeholder="Create some attributes for tagging the questions" style="width: 100%;" name="attributes[]">
		            			@if(isset($survey['attributes']))
			            			@foreach($survey['attributes'] as $attribute)
			              			<option value="{{ $attribute }}" selected>{{ $attribute }}</option>
					    					@endforeach
				    					@else
				    						@foreach($attributes as $attribute)
					              	<option value="{{ $attribute->name }}" selected>{{ $attribute->name }}</option>
						    				@endforeach
					    				@endif
				        		</select>
									</div>
								</div>
							</div>

							{{-- <div class="col-md-12 no-padding">
								<div class="form-group col-md-6 {{ $errors->has('period_start') ? ' has-error' : '' }}">
									<label for="inputBatchReference" class="col-md-4 control-label">{{ trans('app.profiler.period_start') }} (<span class="text-red">*</span>) :</label>
									<div class="col-md-8">
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											{{ Form::text('period_start', $survey['period_start'], array("id" => "period-start", "class" => "form-control pull-right", "placeholder" => "Start Period", "required" => "required" )) }}
										</div>
									</div>
									<div class="col-md-8 col-md-offset-4 help-block with-errors"></div>
								</div>
								<div class="form-group col-md-6 {{ $errors->has('period_end') ? ' has-error' : '' }}">
									<label for="inputAttributes" class="col-md-4 control-label">{{ trans('app.profiler.period_end') }} :</label>
									<div class="col-md-8">
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											{{ Form::text('period_end', $survey['period_end'], array("id" => "period-end", "class" => "form-control pull-right", "placeholder" => "End Period", "required" => "required" )) }}
										</div>
									</div>
								</div>
							</div> --}}
							
							<div class="col-md-12 no-padding">
								<div class="form-group col-md-6 {{ $errors->has('profiler_type') ? ' has-error' : '' }}">
									<label for="inputProfilerType" class="col-md-4 control-label">{{ trans('app.profiler.profiler_type') }} (<span class="text-red">*</span>) :</label>
									<div class="col-md-8">
						        {{ Form::select('profiler_type', array('self-profiler' => 'Self profiler', '360-profiler' => '360 profiler'), $survey['profiler_type'], array('id' => 'inputProfilerType', 'class' => 'form-control', 'required' => 'required')) }}
									</div>
									<div class="col-md-8 col-md-offset-4 help-block with-errors"></div>
									{{-- @if ($errors->has('profiler_type'))
										<span class="help-block">
											<strong>{{ $errors->first('profiler_type') }}</strong>
										</span>
									@endif --}}
								</div>
								<div class="form-group col-md-6 {{ $errors->has('observer_count') ? ' has-error' : '' }}">
									<label for="inputObserverCount" class="col-md-4 control-label">{{ trans('app.profiler.observer_count') }} (<span class="text-red">*</span>) :</label>
									<?php
										$attrObserverCount = array(
											"id" => "observer_count",
											"class" => "form-control observer_input input-number",
											"placeholder" => trans('app.profiler.placeholder.observer_count'),
											"min" => "0",
											"max" => "99"
										);
										if($survey['profiler_type'] === "self-profiler") {
											$attrObserverCount['disabled'] = 'disabled';
										} else {
											$attrObserverCount['required'] = "required";
										}
									?>
									<div class="col-md-8">
										{{ Form::number('observer_count', isset($survey['observer_count']) ? $survey['observer_count']:null, $attrObserverCount) }}
									</div>
									<div class="col-md-8 col-md-offset-4 help-block with-errors"></div>
									{{-- @if ($errors->has('observer_count'))
										<span class="help-block">
											<strong>{{ $errors->first('observer_count') }}</strong>
										</span>
									@endif --}}
								</div>
							</div>
							
							<div class="form-group {{ $errors->has('category_profile_id') ? ' has-error' : '' }}">
								<label for="inputObserverCategory" class="col-md-2 control-label">{{ trans('app.profiler.observer_category') }} (<span class="text-red">*</span>) :</label>
								<div class="col-md-10">
									<?php
										$attrCategoryProfile = array('class' => 'form-control select-profiler-type observer_input', 'placeholder' => trans('app.profiler.placeholder.category_profiler'));
										if($survey['profiler_type'] == 'self-profiler') {
											$attrCategoryProfile['disabled'] = 'disabled';
										}
									?>
					        {{ Form::select('category_profile_id', $category_profiles, isset($survey['category_profile_id']) ? $survey['category_profile_id']:null, $attrCategoryProfile) }}
								</div>
								<div class="col-md-10 col-md-offset-2 help-block with-errors"></div>
								{{-- @if ($errors->has('category_profile_id'))
									<span class="help-block">
										<strong>{{ $errors->first('category_profile_id') }}</strong>
									</span>
								@endif --}}
							</div>
							<div class="form-group {{ $errors->has('observer_description') ? ' has-error' : '' }}">
								<label for="inputObserverInfo" class="col-md-2 control-label">{{ trans('app.profiler.observer_info') }} (<span class="text-red">*</span>) :</label>
								<div class="col-md-10">
									<?php
										$attrObserverDesc = ["id" => "inputObserverInfo", "class" => "form-control profiler_settings", "rows" => "3", "placeholder" => trans('app.profiler.placeholder.observer_info')];
										if($survey['profiler_type'] == 'self-profiler' || $survey['same_observer_desc'] == 1) {
											$attrObserverDesc['disabled'] = 'disabled';
										}
									?>
									{{ Form::textarea('observer_description', isset($survey['observer_description']) ? $survey['observer_description']:null, $attrObserverDesc) }}
								</div>
								<div class="col-md-10 col-md-offset-2 help-block with-errors"></div>
								{{-- @if ($errors->has('observer_description'))
									<span class="help-block">
										<strong>{{ $errors->first('observer_description') }}</strong>
									</span>
								@endif --}}
							</div>
							<div class="form-group {{ $errors->has('same_observer_desc') ? ' has-error' : '' }}">
								<label for="isSameObserverDesc" class="col-md-2 control-label">{{ trans('app.profiler.same_observer_desc') }} :</label>
								<div class="col-md-10 control-label">
									{{-- <input id="isSameObserverDesc" type="checkbox" class="icheck" name="same_observer_desc"> --}}
									<?php
										$attrSameObserverDesc = ['id' => 'isSameObserverDesc', 'class' => 'icheck'];
										if($survey['profiler_type'] === 'self-profiler') {
											$attrSameObserverDesc['disabled'] = 'disabled';
										}
									?>
									{{ Form::checkbox('same_observer_desc', null, $survey['same_observer_desc'], $attrSameObserverDesc) }}
								</div>
							</div>

							<div class="col-md-12 no-padding">
								<div class="form-group col-md-6 {{ $errors->has('type') ? ' has-error' : '' }}">
									<label for="cmp-reminder-date" class="col-md-4 control-label">{{ trans('app.profiler.cmp_reminder') }} :</label>
									<div class="col-md-2 control-label">
										{{-- <input id="toggle-cmp-reminder" type="checkbox" class="icheck" name="fixed_cmp_reminder" disabled> --}}
										<?php
											$attrCmpReminder = ['id' => 'toggle-cmp-reminder', 'class' => 'icheck'];
											// if($survey['profiler_type'] === 'self-profiler') {
											// 	$attrCmpReminder['disabled'] = 'disabled';
											// }
										?>
										{{ Form::checkbox('fixed_cmp_reminder', null, $survey['fixed_cmp_reminder'], $attrCmpReminder) }}
									</div>
									<div class="col-md-6">
											<div class="input-group date">
												<?php
													$attrReminderDate = ["id" => "cmp-reminder-date", "class" => "form-control pull-right", "placeholder" => trans("app.profiler.placeholder.input_reminder_date")];
													if($survey['fixed_cmp_reminder'] == 0) {
														$attrReminderDate['disabled'] = 'disabled';
													} else {
													$attrReminderTime['required'] = 'required';
												}
												?>
												{{ Form::text('cmp_reminder_date', $survey['cmp_reminder_date'], $attrReminderDate) }}
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
											</div>
									</div>
									<div class="col-md-6 col-md-offset-6 help-block with-errors"></div>
									{{-- @if ($errors->has('type'))
										<span class="help-block">
											<strong>{{ $errors->first('type') }}</strong>
										</span>
									@endif --}}
								</div>
								<div class="form-group col-md-6 {{ $errors->has('value') ? ' has-error' : '' }}">
									<div class="col-md-6 col-md-offset-4">
										<div class="input-group bootstrap-timepicker">
											<?php
												$attrReminderTime = ["id" => "cmp-reminder-time", "class" => "form-control pull-right", "placeholder" => trans("app.profiler.placeholder.input_reminder_time")];
												if($survey['fixed_cmp_reminder'] == 0) {
													$attrReminderTime['disabled'] = 'disabled';
												} else {
													$attrReminderTime['required'] = 'required';
												}
											?>
											{{ Form::text('cmp_reminder_time', $survey['cmp_reminder_time'], $attrReminderTime) }}
	                    <div class="input-group-addon">
	                      <i class="fa fa-clock-o"></i>
	                    </div>
	                  </div>
									</div>
									<div class="col-md-6 col-md-offset-4 help-block with-errors"></div>
									{{-- @if ($errors->has('value'))
										<span class="help-block">
											<strong>{{ $errors->first('value') }}</strong>
										</span>
									@endif --}}
								</div>
							</div>

							<div class="col-md-12 no-padding">
								<div class="form-group col-md-6 {{ $errors->has('type') ? ' has-error' : '' }}">
									<label for="inputReminderType" class="col-md-4 control-label">{{ trans('app.profiler.observer_reminder') }} :</label>
									<div class="col-md-8">
										<?php
											$attrObserverReminder = ['class' => 'form-control select-reminder-type', 'placeholder' => 'Choose Reminder Type'];
											if($survey['fixed_cmp_reminder'] == 1) {
												$attrObserverReminder['disabled'] = 'disabled';
											}
										?>
											{{ Form::select('type', array('minute' => 'Minute', 'day' => 'Day', 'week' => 'Week', 'month' => 'Month'), $survey['type'], $attrObserverReminder) }}
									</div>
									<div class="help-block with-errors"></div>
									{{-- @if ($errors->has('type'))
										<span class="help-block">
											<strong>{{ $errors->first('type') }}</strong>
										</span>
									@endif --}}
								</div>
								<div class="form-group col-md-6 {{ $errors->has('value') ? ' has-error' : '' }}">
									<?php
										$attrReminderValue = array(
											"id" => "inputReminderValue",
											"class" => "form-control input-reminder-value input-number",
											"placeholder" => "Reminder value",
											"min" => 1
										);
										if($survey['fixed_cmp_reminder'] == 1) {
											$attrReminderValue['disabled'] = 'disabled';
										}
										if($survey['type'] && $survey['fixed_cmp_reminder'] == 0) {
											$attrReminderValue['required'] = "required";
										}
									?>
									<label for="inputReminderValue" class="col-md-4 control-label">{{ trans('app.profiler.reminder_value') }} :</label>
									<div class="col-md-8">
										{{ Form::number('value', $survey['value'], $attrReminderValue) }}
									</div>
									<div class="help-block with-errors"></div>
									{{-- @if ($errors->has('value'))
										<span class="help-block">
											<strong>{{ $errors->first('value') }}</strong>
										</span>
									@endif --}}
								</div>
							</div>
							
							<div class="form-group {{ $errors->has('random_question') ? ' has-error' : '' }}">
								<label for="random-question" class="col-md-2 control-label">{{ trans('app.profiler.random_question') }} :</label>
								<div class="col-md-10 control-label">
									<input type="checkbox" id="random-question" class="icheck" name="random_question" {{ $survey['random_question'] == 1 ? 'checked':'' }}>
								</div>
							</div>
							<div class="form-group {{ $errors->has('thanks_message') ? ' has-error' : '' }}">
								<label for="inputThanksMessage" class="col-md-2 control-label">{{ trans('app.profiler.thanks_message') }} (<span class="text-red">*</span>) :</label>
								<div class="col-md-10">
									{{ Form::textarea('thanks_message', $survey['thanks_message'], array("id" => "inputThanksMessage", "class" => "form-control profiler_settings", "rows" => "3", "placeholder" => trans('app.profiler.placeholder.thanks_message'), "required" => true )) }}
								</div>
								<div class="col-md-10 col-md-offset-2 help-block with-errors"></div>
								{{-- @if ($errors->has('thanks_message'))
									<span class="help-block">
										<strong>{{ $errors->first('thanks_message') }}</strong>
									</span>
								@endif --}}
							</div>
							<div class="form-group {{ $errors->has('redirect_after_completion') ? ' has-error' : '' }}">
								<label for="redirect_after_completion_url" class="col-md-2 control-label">{{ trans('app.profiler.redirect_after_completion') }} :</label>
								<div class="col-md-1 control-label">
									{{ Form::checkbox('redirect_after_completion', null, $survey['redirect_after_completion'], ['id' => 'toggle-redirect-completion', 'class' => 'icheck']) }}
								</div>
								<div class="col-md-9">
									<?php
										$attrRedirectUrl = ["id" => "redirect_after_completion_url", "class" => "form-control", "placeholder" => trans('app.profiler.placeholder.redirect_url_input')];
										if($survey['redirect_after_completion'] == 0) {
											$attrRedirectUrl['disabled'] = 'disabled';
										} else {
											$attrRedirectUrl['required'] = 'required';
										}
									?>
									{{ Form::url('redirect_after_completion_url', $survey['redirect_after_completion_url'], $attrRedirectUrl) }}
								</div>
								<div class="col-md-9 col-md-offset-3 help-block with-errors"></div>
							</div>

							<div class="form-group {{ $errors->has('email_builder_id') ? ' has-error' : '' }}">
								<label for="inputObserverCategory" class="col-md-2 control-label">{{ trans('app.profiler.report_template') }} :</label>
								<div class="col-md-10">
									<?php
										$reportAttrs = [
											'class' => 'form-control', 
											'placeholder' => trans('app.profiler.placeholder.report_template')
										];
									?>
					        {{ Form::select('email_builder_id', $reportTemplates, isset($survey['email_builder_id']) ? $survey['email_builder_id']:null, $reportAttrs) }}
								</div>
								<div class="col-md-10 col-md-offset-2 help-block with-errors"></div>
								{{-- @if ($errors->has('email_builder_id'))
									<span class="help-block">
										<strong>{{ $errors->first('email_builder_id') }}</strong>
									</span>
								@endif --}}
							</div>
	          </div>
	          <!-- /.tab-pane -->
	          <div class="tab-pane clearfix" id="questions">
	            <div class="col-xs-2 no-padding"> <!-- required for floating -->
						    <div class="nav-tabs-custom">
							    <!-- Nav tabs -->
							    <ul class="nav nav-tabs tabs-left">
							      <li><a href="#preliminary" data-toggle="tab">Pre</a></li>
							      <li class="active"><a href="#questionnaire" data-toggle="tab">Questionnaire</a></li>
							      <li><a href="#post" data-toggle="tab">Post</a></li>
							    </ul>
						    	
						    </div>
							</div>

							<div class="col-xs-10">
								<div class="nav-tabs-custom">
							    <!-- Tab panes -->
							    <div class="tab-content">
							      <div class="tab-pane tab-question" id="preliminary" data-section-type="preliminary">
							      	<div class="form-group">
					        			<label>Use Preliminary: </label>
												<input id="toggle-preliminary" type="checkbox" {{ count(old('preliminary')) > 0 || count($survey['preliminary']) > 0 ? 'checked':'' }} data-size="small" data-toggle="toggle" data-on="" data-off="" class="toggle-optional-type" {{ $is_exist_answer ? 'disabled':'' }}>
						        	</div>
						        	<div class="row">
						        		<div class="box-group section-wrapper {{ count(old('preliminary')) > 0 || count($survey['preliminary']) > 0 ? '':'hidden' }}" id="accordion">
													<div class="sections">
														<!-- render sections here -->
														@include('survey.forms.section', ['sections' => old('preliminary') ? old('preliminary'):$survey['preliminary'], 'section_type' => 'preliminary', 'is_exist_answer' => $is_exist_answer])
													</div>
													<div class="box-footer" style="border-top: none;">
														<button class="btn btn-default pull-right add-section">
															Add Section
														</button>
													</div>
												</div>
						        	</div>
							      </div>
							      <div class="tab-pane tab-question active" id="questionnaire" data-section-type="questionnaire">
							      	<div class="row">
							      		<div class="box-group section-wrapper" id="accordion">
													<div class="sections">
														<!-- render sections here -->
														@include('survey.forms.section', ['sections' => old('questionnaire') ? old('questionnaire'):$survey['questionnaire'], 'section_type' => 'questionnaire', 'is_exist_answer' => $is_exist_answer])
													</div>
													<div class="box-footer" style="border-top: none;">
														<button class="btn btn-default pull-right add-section">
															Add Section
														</button>
													</div>
												</div>
							      	</div>
							      </div>
							      <div class="tab-pane tab-question" id="post" data-section-type="post">
						      		<div class="form-group">
					        			<label>Use Post: </label>
												<input id="toggle-post" type="checkbox" {{ count(old('post')) > 0 || count($survey['post']) > 0 ? 'checked':'' }} data-size="small" data-toggle="toggle" data-on="" data-off="" class="toggle-optional-type" {{ $is_exist_answer ? 'disabled':'' }}>
						        	</div>
						        	<div class="row">
						        		<div class="box-group section-wrapper {{ count(old('post')) > 0 || count($survey['post']) > 0 ? '':'hidden' }}" id="accordion">
													<div class="sections">
														<!-- render sections here -->
														@include('survey.forms.section', ['sections' => old('post') ? old('post'):$survey['post'], 'section_type' => 'post', 'is_exist_answer' => $is_exist_answer])
													</div>
													<div class="box-footer" style="border-top: none;">
														<button class="btn btn-default pull-right add-section">
															Add Section
														</button>
													</div>
												</div>
						        	</div>
							      </div>
							    </div>
								</div>
							</div>
	          </div>
	          <!-- /.tab-pane -->
	          <div class="tab-pane" id="statistics">
	            <div class="form-group">
								<label for="labelConsumerBought" class="col-md-3 control-label">{{ trans('app.profiler.label_consumer_bought') }} :</label>
								<div class="col-md-9">
									<span>72 {{ trans('app.profiler.persons') }}</span>
								</div>
							</div>
							<div class="form-group">
								<label for="labelCorporateBought" class="col-md-3 control-label">{{ trans('app.profiler.label_corporate_bought') }} :</label>
								<div class="col-md-9">
									<span>43 {{ trans('app.profiler.organizations') }}</span>
								</div>
							</div>
							<div class="form-group {{ $errors->has('statistic_text') ? ' has-error' : '' }}">
								<label for="inputStatisticText" class="col-md-3 control-label">{{ trans('app.profiler.statistic_text') }} :</label>
								<div class="col-md-6">
									{{ Form::textarea('statistic_text', old('statistic_text'), array("id" => "inputStatisticText", "class" => "form-control", "rows" => "3" )) }}
								</div>
							</div>
	          </div>
	          <!-- /.tab-pane -->
	        </div>
	        <!-- /.tab-content -->
	      </div>
	      <!-- nav-tabs-custom -->
			</div>
			<div class="col-md-7">
				<a href="javascript:void(0)" data-toggle="popover" class="variable-help">{{ trans('app.profiler.variable_help') }} <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a>
			</div>
			<div class="col-md-4">
				<div class="form-group col-md-10 pull-right">
          <select class="form-control" name="state" required>
            <option value="draft" {{ $survey['state'] == 'draft' ? 'selected':'' }}>{{ trans('app.profiler.save_as_draft') }}</option>
            <option value="published" {{ $survey['state'] == 'published' ? 'selected':'' }}>{{ trans('app.profiler.publish') }}</option>
            <option value="preview_as_consumer" {{ $survey['state'] == 'preview_as_consumer' ? 'selected':'' }}>{{ trans('app.profiler.preview_as_consumer') }}</option>
          </select>
        </div>
			</div>
			<div class="col-md-1">
				<button class="btn btn-primary" {{ $is_exist_answer ? "disabled":'' }} title="{{ $is_exist_answer ? "Survey couldn't update":"" }}">{{ trans('app.button.submit') }}</button>
			</div>
		{!! Form::close() !!}
	</div>
@endsection

@push('plugins-js')
	{!! Html::script('vendor/underscore/underscore.js') !!}
	{!! Html::script('vendor/AdminLTE/plugins/daterangepicker/moment.js') !!}
	{!! Html::script('vendor/AdminLTE/plugins/datepicker/bootstrap-datepicker.js') !!}
	{!! Html::script('vendor/bootstrap-toggle/js/bootstrap-toggle.js') !!}
	{!! Html::script('vendor/AdminLTE/plugins/ckeditor/ckeditor.js') !!}
	{!! Html::script('vendor/AdminLTE/plugins/select2/select2.js') !!}
	{!! Html::script('vendor/AdminLTE/plugins/iCheck/icheck.min.js') !!}
	{!! Html::script('vendor/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.js') !!}
	{!! Html::script('vendor/jquery-toast-plugin/src/jquery.toast.js') !!}
	{!! Html::script('vendor/bootstrap-validator/dist/validator.js') !!}
@endpush

@push('custom-js')

	@include('templates.surveys.section')
	@include('templates.surveys.question')
	@include('templates.surveys.answer_configs.free_text')
	@include('templates.surveys.answer_configs.multiple-choice')
	@include('templates.surveys.answer_configs.checkboxes')
	@include('templates.surveys.answer_configs.matching')
	@include('templates.surveys.answer_configs.likert-scale')
	@include('templates.surveys.answer_configs.boolean')
	@include('templates.surveys.answer_configs.ranking')
	@include('templates.surveys.answer_configs.multiple-choice-with-constraint')
	@include('templates.surveys.answer_configs.date')
	@include('templates.surveys.answer_configs.time')
	@include('templates.surveys.answer_configs.datetime')
	@include('templates.surveys.answer_configs.distributed-points')

	{!! Html::script('js/jquery.numeric.min.js') !!}
	{!! Html::script('js/survey.js') !!}
	<script type="text/javascript">
		$(document).ready(function () {
			$('#form-survey').validator();
		});
	</script>

@endpush
