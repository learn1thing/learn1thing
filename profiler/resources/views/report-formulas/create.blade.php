<div class="modal fade" id="formulas" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">{{ trans('app.label.add') }}</h4>
            </div>
            {!! Form::model($data['formula'], $form) !!}
            <input type="hidden" value="{{ route('report-formula.store') }}" id="formula-url">
            <div class="modal-body">
                <div class="row formula-wrapper" data-number="0">
                    <div class="row col-md-12">
                        <div class="form-group col-md-6 clearfix">
                            <label for="inputFormulaName" class="col-sm-3 control-label no-padding">Survey: </label>
                            <div class="col-sm-9">
                                <select name="survey_id" class="form-control">
                                    <option>Select Data Source</option>
                                    @foreach($data['profilers'] as $profiler)
                                        <option value="{{ $profiler['id'] }}">{{ $profiler['title'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-8 col-md-offset-4 help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="form-group col-md-6 clearfix">
                            <label for="inputFormulaName" class="col-sm-3 control-label no-padding">Formula name: </label>
                            <div class="col-sm-9">
                                <input name="formula[0][name]" type="text" class="form-control" id="inputFormulaName" placeholder="Enter the formula name" required>
                            </div>
                            <div class="col-md-8 col-md-offset-4 help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Data source</label>
                        <select name="formula[0][source]" class="form-control formula-input source" style="width: 100%;" required>
                          <option></option>
                          <option value="formula">Formula</option>
                          <option value="raw_data">Raw data</option>
                        </select>
                      </div>
                      <!-- /.form-group -->
                      <div class="form-group raw_data-group hidden">
                        <label>Question</label>
                        <select name="formula[0][question_id]" class="form-control formula-input question" style="width: 100%;" disabled>
                          <option></option>
                        </select>
                      </div>
                      <!-- /.form-group -->

                      <div class="form-group hidden">
                        <label>Formula</label>
                        <select name="formula[0][formula_id]" class="form-control formula-input formula" style="width: 100%;" disabled>
                          <option></option>
                        </select>
                      </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-3 raw_data-group hidden">
                      <div class="form-group">
                        <label>Profiler role</label>
                        <select name="formula[0][profiler_role]" class="form-control formula-input profiler_role" style="width: 100%;" disabled>
                          <option value="all" selected="selected">All</option>
                          <option value="observer">Observer</option>
                          <option value="user">User</option>
                        </select>
                      </div>
                      <!-- /.form-group -->
                      <div class="form-group hidden">
                        <label>Observer category</label>
                        <select name="formula[0][observer_category_id]" class="form-control formula-input observer_category" style="width: 100%;" disabled>
                          <option></option>
                        </select>
                      </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div id="user-properties" class="col-md-3 hidden">
                      <div class="form-group">
                        <label>User category</label>
                        <select name="formula[0][user_category]" class="form-control formula-input user-category" style="width: 100%;" disabled>
                          <option value="all" selected="selected">All</option>
                          <option value="department">Department</option>
                          <option value="individual">Individual</option>
                        </select>
                      </div>
                      <!-- /.form-group -->
                      <div class="form-group hidden">
                        <label>Group by</label>
                        <select name="formula[0][group_by]" class="form-control formula-input group-by" style="width: 100%;" disabled>
                          <option></option>
                        </select>
                      </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-3 raw_data-group hidden">
                      <div class="form-group">
                        <label>Question spec</label>
                        <select name="formula[0][question_spec]" class="form-control formula-input question-spec" style="width: 100%;" disabled>
                          <option value="value" selected="selected">Value</option>
                          <option value="answer_attributes">Answer attributes</option>
                          {{-- <option value="label">Label</option>
                          <option value="label_group">Label group</option> --}}
                        </select>
                      </div>
                      <!-- /.form-group -->
                      <div class="form-group">
                        <label>Function</label>
                        <select name="formula[0][function]" class="form-control formula-input function" style="width: 100%;" disabled>
                          <option value="raw_value" selected="selected">Raw value</option>
                          <option value="average">Average</option>
                          <option value="count">Count</option>
                          <option value="maximum">Maximum</option>
                          <option value="minimum">Minimum</option>
                          <option value="sum">Sum</option>
                        </select>
                      </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-12">
                      <div class="form-group col-md-2 no-padding">
                          <label>Operand</label>
                          <select id="inputOperand" name="formula[0][operand]" class="form-control formula-input operand" style="width: 100%;" disabled>
                              <option></option>
                              <option value="+">+</option>
                              <option value="-">-</option>
                              <option value="*">*</option>
                              <option value="/">/</option>
                          </select>
                      </div>
                    </div>
                    <!-- /.col -->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">{!! trans('app.label.cancel') !!}</button>
                {!! Form::submit(trans('app.label.add'), ['class'=>'btn blue']) !!}
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>
<script type="text/javascript">
  (function () {
    $(document).ready(function () {
      var surveyData = {!! json_encode($data['profilers']) !!};
      var departments = {!! json_encode($data['departments']) !!};
      var roles = {!! json_encode($data['roles']) !!};
      var selectedSurvey = {};
      var storeUrl = {!! json_encode(route('report-formula.store')) !!};

      departments.splice(0, 0, {id: '', text: 'Choose the department'});
      roles.splice(0, 0, {id: '', text: 'Choose the role'});

      setTimeout(function () {
        $('#reportformula-form').find("select.formula-input.question").select2({
          data: [],
          placeholder: "Choose the question",
          allowClear: true
        });
        $('#reportformula-form').find("select.formula-input.formula").select2({
          data: [],
          placeholder: "Choose the formula",
          allowClear: true
        });
        $('#reportformula-form').find("select.formula-input.observer_category").select2({
          data: [],
          placeholder: "Choose the category",
          allowClear: true
        });
      }, 1000);

      // formula events
      $('#reportformula-form').delegate('select[name=survey_id]', 'change', function () {
        $('#reportformula-form raw_data-group').addClass('hidden');
        var surveyId = $(this).val();
        selectedSurvey = _.find(surveyData, function (val, key) {
          return val.id == surveyId;
        });
        $('#reportformula-form').find("select.formula-input.question").empty().trigger("change");
        $('#reportformula-form').find("select.formula-input.formula").empty().trigger("change");
        $('#reportformula-form').find("select.formula-input.observer_category").empty().trigger("change");
        setTimeout(function () {
          var questions = [];
          var formulas = [];
          var categories = [];

          _.each(selectedSurvey.questions, function (val, key) {
            questions.push({id: val.id, text: val.name});
          });
          _.each(selectedSurvey.formulas, function (val, key) {
            formulas.push({id: val.id, text: val.name});
          });
          _.each(selectedSurvey.categories, function (val, key) {
            categories.push({id: val.id, text: val.name});
          });

          questions.splice(0, 0, {id: '', text: 'Choose the question'});
          formulas.splice(0, 0, {id: '', text: 'Choose the formula'});
          categories.splice(0, 0, {id: '', text: 'Choose the category'});

          $('#reportformula-form').find("select.formula-input.question").select2({
            data: questions,
            placeholder: "Choose the question",
            allowClear: true
          });
          $('#reportformula-form').find("select.formula-input.formula").select2({
            data: formulas,
            placeholder: "Choose the formula",
            allowClear: true
          });
          $('#reportformula-form').find("select.formula-input.observer_category").select2({
            data: categories,
            placeholder: "Choose the categories",
            allowClear: true
          });
        }, 1000);
      });

      $('#reportformula-form').delegate('select.formula-input.source', 'change', function () {
        var parent = $(this).parents('.formula-wrapper');
        var sourceValue = $(this).val();
        if(sourceValue == "raw_data") {
          parent.find('select.formula-input.question')
            .prop("disabled", false)
            .attr('required', 'required');
          parent.find('select.formula-input.question')
            .parents('.form-group')
            .removeClass('hidden');

          parent.find('select.formula-input.formula')
            .removeAttr('required')
            .prop("disabled", true);
          parent.find('select.formula-input.formula')
            .parents('.form-group')
            .addClass('hidden');
          
          parent.find('select.formula-input.profiler_role')
            .removeAttr('disabled') 
            .attr('required', 'required');

          parent.find('select.formula-input.question-spec')
            .removeAttr('disabled') 
            .attr('required', 'required');

          parent.find('select.formula-input.function')
            .removeAttr('disabled') 
            .attr('required', 'required');

          parent.find('.raw_data-group').removeClass('hidden');

          var functionValue = parent.find('select.formula-input.function').val();
          if(functionValue != "raw_value") {
            parent.find('select.formula-input.operand').removeAttr('disabled');
          } else {
            parent.find('select.formula-input.operand').attr('disabled', 'disabled');
          }

        } else if (sourceValue == "formula") {
          parent.find('.raw_data-group').addClass('hidden');
          parent.find('select.formula-input.formula')
            .prop("disabled", false)
            .attr('required', 'required');
          parent.find('select.formula-input.formula')
            .parents('.form-group')
            .removeClass('hidden');

          parent.find('select.formula-input.question')
            .removeAttr('required')
            .prop("disabled", true);
          parent.find('select.formula-input.question')
            .parents('.form-group')
            .addClass('hidden');

          parent.find('select.formula-input.profiler_role')
            .removeAttr('required') 
            .attr('disabled', 'disabled');

          parent.find('select.formula-input.question-spec')
            .removeAttr('required') 
            .attr('disabled', 'disabled');

          parent.find('select.formula-input.function')
            .removeAttr('required') 
            .attr('disabled', 'disabled');

          parent.find('select.formula-input.operand').removeAttr('disabled');
        } else {
          var indexWrapper = parent.data('number');
          $('.formula-wrapper').eq(parseInt(indexWrapper)+1).remove();
        }
        $('#reportformula-form').validator('update');
      });

      $('#reportformula-form').delegate('select.formula-input.operand', 'change', function () {
        var formulaWrapper = $(this).parents('.formula-wrapper');
        var parent = formulaWrapper.parent();
        var value = $(this).val();
        if (value) {
          var indexFormula = formulaWrapper.data('number');
          var isAddNewFormula = parent.find('.formula-wrapper').eq(indexFormula+1);
          if(isAddNewFormula.length == 0) {
            var form = _.template($('#element-formula-template').html());
            parent.append(form({number: indexFormula+1}));

            var questions = [];
            var formulas = [];
            var categories = [];
            _.each(selectedSurvey.questions, function (val, key) {
              questions.push({id: val.id, text: val.name});
            });
            _.each(selectedSurvey.formulas, function (val, key) {
              formulas.push({id: val.id, text: val.name});
            });
            _.each(selectedSurvey.categories, function (val, key) {
              categories.push({id: val.id, text: val.name});
            });

            questions.splice(0, 0, {id: '', text: 'Choose the question'});
            formulas.splice(0, 0, {id: '', text: 'Choose the formula'});
            categories.splice(0, 0, {id: '', text: 'Choose the category'});

            var lastWrapper = parent.find('.formula-wrapper').last();
            lastWrapper.find("select.formula-input.question").select2({
              data: questions,
              placeholder: "Choose the question",
              allowClear: true
            });
            lastWrapper.find("select.formula-input.formula").select2({
              data: formulas,
              placeholder: "Choose the formula",
              allowClear: true
            });
            lastWrapper.find("select.formula-input.observer_category").select2({
              data: categories,
              placeholder: "Choose the category",
              allowClear: true
            });
          }
        } else {
          var wrappers = parent.find('.formula-wrapper');
          var indexFormula = formulaWrapper.data('number');
          for(i = indexFormula+1; i < wrappers.length; i++) {
            if(wrappers.eq(i).length == 1) {
              wrappers.eq(i).remove();
            }
          }
        }
        $('#reportformula-form').validator('update');
      });

      $('#reportformula-form').delegate('select.formula-input.profiler_role', 'change', function () {
        var formulaWrapper = $(this).parents('.formula-wrapper');
        var roleValue = $(this).val();
        if(roleValue == "observer") {
            formulaWrapper.find('select.formula-input.observer_category')
                .prop("disabled", false)
                .attr('required', 'required');
            formulaWrapper.find('select.formula-input.observer_category')
                .parents('.form-group')
                .removeClass('hidden');

            var data = selectedSurvey.observer_categories;
            formulaWrapper.find("select.formula-input.observer_category").select2({
              data: data,
              placeholder: "Choose the observer category"
            });
            formulaWrapper.find('#user-properties')
                .addClass('hidden');
            formulaWrapper.find('#user-properties .user-category')
                .attr('disabled', 'disabled');
        } else if(roleValue == "user") {
            formulaWrapper.find('#user-properties')
                .removeClass('hidden');
            formulaWrapper.find('#user-properties .user-category')
                .removeAttr('disabled');
            formulaWrapper.find('select.formula-input.observer_category')
                .removeAttr('required')
                .prop("disabled", true);
            formulaWrapper.find('select.formula-input.observer_category')
                .parents('.form-group')
                .addClass('hidden');
        } else {
            formulaWrapper.find('select.formula-input.observer_category')
                .removeAttr('required')
                .prop("disabled", true);
            formulaWrapper.find('select.formula-input.observer_category')
                .parents('.form-group')
                .addClass('hidden');
            formulaWrapper.find('#user-properties')
                .addClass('hidden');
            formulaWrapper.find('#user-properties .user-category')
                .attr('disabled', 'disabled');
        }
        $('#reportformula-form').validator('update');
      });

      $('#reportformula-form').delegate('select.formula-input.user-category', 'change', function () {
        var formulaWrapper = $(this).parents('.formula-wrapper');
        var userCategoryValue = $(this).val();
        if(userCategoryValue == "department" || userCategoryValue == "individual") {
            formulaWrapper.find('select.formula-input.group-by')
                .prop("disabled", false)
                .attr('required', 'required');
            formulaWrapper.find('select.formula-input.group-by')
                .parent()
                .removeClass('hidden');
            var data = userCategoryValue == "department" ? departments : roles;
            var placeholderObject = userCategoryValue == "department" ? "department":"role"
            formulaWrapper.find('select.formula-input.group-by').empty().trigger("change");
            formulaWrapper.find('select.formula-input.group-by').select2({
                data: data,
                placeholder: 'Choose the ' + placeholderObject,
                allowClear: true
            });
        } else {
            formulaWrapper.find('select.formula-input.group-by')
                .prop("disabled", true)
                .removeAttr('required');
            formulaWrapper.find('select.formula-input.group-by')
                .parent()
                .addClass('hidden');
        }
        $('#reportformula-form').validator('update');
      });

      $('#reportformula-form').delegate('select.formula-input.question', 'change', function () {
        var parent = $(this).parents('.formula-wrapper');
        if(selectedSurvey.questions && $(this).val()) {
          var questionId = $(this).val();
          var question = _.find(selectedSurvey.questions, function (val, key) {
              return val.id == questionId;
          });
          if(question.answer_type == "free_text") {
            var questionSpec = parent.find('select.formula-input.question-spec');
            if(questionSpec.find('option[value=free_text]').length == 0) {
              questionSpec.append('<option value="free_text">Free Text</option>');
            }
          } else {
            var questionSpec = parent.find('select.formula-input.question-spec');
            if(questionSpec.find('option[value=free_text]').length > 0) {
              questionSpec.find('option[value=free_text]').remove();
            }
          }
        }
        $('#reportformula-form').validator('update');
      });

      $('#reportformula-form').validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) {
          
          e.preventDefault();
        } else {
          var functions = [];
          $('select.formula-input.function').each(function () {
            functions.push($(this).val());
          });

          if($.inArray("raw_value", functions) >= 0 && functions.length > 1){
            $.toast({
              heading: 'Failed',
              text: "Text cannot be combine with number",
              icon: 'error',
              position: 'top-right'
            });
          } else {
            $.ajax({
              url: storeUrl,
              method: 'POST',
              data: $(this).serialize(),
              success: function(result) {
                var data = result.data;
                $('#formulas').modal('toggle');
                $('#dataTableBuilder').DataTable().ajax.reload();
              },
              error: function(result) {
                $.toast({
                  heading: 'Failed',
                  text: result.message,
                  icon: 'error',
                  position: 'top-right'
                });
              }
            });
          }
          e.preventDefault();
        }
      });

      $('#reportformula-form').delegate('select.formula-input.question-spec', 'change', function () {
        var formulaWrapper = $(this).parents('.formula-wrapper');
        var questionSpecValue = $(this).val();
        if(questionSpecValue == "value") {
          formulaWrapper.find('select.formula-input.function')
            .prop("disabled", false);
        } else {
          formulaWrapper.find('select.formula-input.function')
            .val('raw_value')
            .prop("disabled", true);
        }
      });

      $('#reportformula-form').delegate('select.formula-input.function', 'change', function () {
        var formulaWrapper = $(this).parents('.formula-wrapper');
        var formulaNumber = formulaWrapper.data('number');
        var functionValue = $(this).val();
        if(functionValue == "raw_value") {
            formulaWrapper.find('select.formula-input.operand')
              .attr('disabled', 'disabled')
              .val('');
            $('.formula-wrapper').each(function () {
              var number = $(this).data('number');
              if(number > formulaNumber) {
                $(this).remove();
              }
            });
        } else {
            formulaWrapper.find('select.formula-input.operand')
                .removeAttr('disabled');
        }
        $('#reportformula-form').validator('update');
      });
    });
  })();
</script>

<script id="element-formula-template" type="text/template">
  <br>
  <div class="row formula-wrapper" data-number="<%= number %>">
    <div class="row col-md-12 hidden">
        <div class="form-group col-md-6 clearfix">
            <label for="inputFormulaName" class="col-sm-3 control-label no-padding">Formula name: </label>
            <div class="col-sm-9">
                <input name="formula[<%= number %>][name]" type="text" class="form-control" id="inputFormulaName" placeholder="Enter the formula name">
            </div>
            <div class="col-md-8 col-md-offset-4 help-block with-errors"></div>
        </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>Data source</label>
        <select name="formula[<%= number %>][source]" class="form-control formula-input source" style="width: 100%;" required>
          <option></option>
          <option value="formula">Formula</option>
          <option value="raw_data">Raw data</option>
        </select>
      </div>
      <!-- /.form-group -->
      <div class="form-group raw_data-group hidden">
        <label>Question</label>
        <select name="formula[<%= number %>][question_id]" class="form-control formula-input question" style="width: 100%;" disabled>
          <option></option>
        </select>
      </div>
      <!-- /.form-group -->

      <div class="form-group hidden">
        <label>Formula</label>
        <select name="formula[<%= number %>][formula_id]" class="form-control formula-input formula" style="width: 100%;" disabled>
          <option></option>
        </select>
      </div>
      <!-- /.form-group -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 raw_data-group hidden">
      <div class="form-group">
        <label>Profiler role</label>
        <select name="formula[<%= number %>][profiler_role]" class="form-control formula-input profiler_role" style="width: 100%;" disabled>
          <option value="all" selected="selected">All</option>
          <option value="observer">Observer</option>
          <option value="user">User</option>
        </select>
      </div>
      <!-- /.form-group -->
      <div class="form-group hidden">
        <label>Observer category</label>
        <select name="formula[<%= number %>][observer_category_id]" class="form-control formula-input observer_category" style="width: 100%;" disabled>
          <option></option>
        </select>
      </div>
      <!-- /.form-group -->
    </div>
    <!-- /.col -->
    <div id="user-properties" class="col-md-3 hidden">
      <div class="form-group">
        <label>User category</label>
        <select name="formula[<%= number %>][user_category]" class="form-control formula-input user-category" style="width: 100%;" disabled>
          <option value="all" selected="selected">All</option>
          <option value="department">Department</option>
          <option value="individual">Individual</option>
        </select>
      </div>
      <!-- /.form-group -->
      <div class="form-group hidden">
        <label>Group by</label>
        <select name="formula[<%= number %>][group_by]" class="form-control formula-input group-by" style="width: 100%;" disabled>
          <option></option>
        </select>
      </div>
      <!-- /.form-group -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 raw_data-group hidden">
      <div class="form-group">
        <label>Question spec</label>
        <select name="formula[<%= number %>][question_spec]" class="form-control formula-input question-spec" style="width: 100%;" disabled>
          <option value="value" selected="selected">Value</option>
          <option value="answer_attributes">Answer attributes</option>
          {{-- <option value="label">Label</option>
          <option value="label_group">Label group</option> --}}
        </select>
      </div>
      <!-- /.form-group -->
      <div class="form-group">
        <label>Function</label>
        <select name="formula[<%= number %>][function]" class="form-control formula-input function" style="width: 100%;" disabled>
          <option value="raw_value" selected="selected">Raw value</option>
          <option value="average">Average</option>
          <option value="count">Count</option>
          <option value="maximum">Maximum</option>
          <option value="minimum">Minimum</option>
          <option value="sum">Sum</option>
        </select>
      </div>
      <!-- /.form-group -->
    </div>
    <!-- /.col -->
    <div class="col-md-12">
      <div class="form-group col-md-2 no-padding">
          <label>Operand</label>
          <select id="inputOperand" name="formula[<%= number %>][operand]" class="form-control formula-input operand" style="width: 100%;" disabled>
              <option></option>
              <option value="+">+</option>
              <option value="-">-</option>
              <option value="*">*</option>
              <option value="/">/</option>
          </select>
      </div>
    </div>
    <!-- /.col -->
  </div>
</script>