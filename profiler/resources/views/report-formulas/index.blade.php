@extends('layouts.admin_template')

@push('plugins-css')
    {!! Html::style('vendor/AdminLTE/plugins/datatables/dataTables.bootstrap.css') !!}
    <!-- Select2 -->
    {!! Html::style('vendor/AdminLTE/plugins/select2/select2.min.css') !!}
    {!! Html::style('vendor/jquery-toast-plugin/src/jquery.toast.css') !!}
@endpush

@push('custom-css')
@endpush

@push('plugins-js')
    {!! Html::script('vendor/AdminLTE/plugins/datatables/jquery.dataTables.min.js') !!}
    {!! Html::script('vendor/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js') !!}
    {{-- {!! Html::script('vendor/jsvalidation/js/jsvalidation.js') !!} --}}
    {!! Html::script('vendor/underscore/underscore.js') !!}
    {!! Html::script('vendor/bootstrap-validator/dist/validator.js') !!}
    {!! Html::script('vendor/jquery-toast-plugin/src/jquery.toast.js') !!}
@endpush

@push('custom-js')
    {!! $dataTable->scripts() !!}

    <script type="text/javascript">
        $(document).ready(function(){
            $("table[id^='datatable-']").parent().addClass('table-responsive');
        })
    </script>

    <script type="text/javascript">
        function rules() { // This Rule for form | Mandatory
            
        }
    </script>

    {!! Html::script('js/global.js') !!}
    <!-- Select2 -->
    {!! Html::script('vendor/AdminLTE/plugins/select2/select2.full.min.js') !!}

    
@endpush

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Formula</h3>
                <div class="pull-right box-tools">
                    <a href="javascript:void(0)" data-table="formulas" data-url="{{ RouteHelper::token('formulas.create') }}" class="btn btn-primary btn-create">
                        {!! trans('app.label.create') !!}
                    </a>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {!! $dataTable->table(['class' => 'table table-bordered table-striped', 'cellspacing'=>"0", 'width'=>"100%"]) !!}
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>

<div id="formulas"></div>

@include('layouts.delete')
@endsection