<!-- Main Header -->
<header class="main-header">

    <a href="{!! env('HOME_URL', 'http://beta.learn1thing.com') !!}" class="logo" style="background-color: #035AA4;">
        <span class="logo-mini" style="color:white">L1T</span>
        <span class="logo-lg">
            <img src="{{url('image/logo-white.png')}}" class="img-thumbnail img-logo" alt="Logo - learn1thing" style="    max-width: 46% !important; background-color:transparent !important; border:none">
        </span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation" style="background-color: #035AA4;">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <img src="{{ asset('/image/no-avatar.png') }}" class="user-image" alt="User Image"/>
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">
                            {!! FrontendHelper::fullName() !!}
                        </span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <img src="{{ asset('/image/no-avatar.png') }}" class="img-circle" alt="User Image" />
                            <p>
                                {!! FrontendHelper::fullName() !!}
                                <small>{!! openssl_decrypt(base64_decode(strtr(session('user')['email'], '-_,', '+/=')), 'AES-128-ECB', 'l1tmiddleware') !!}</small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{!! env('DASHBOARD_URL', 'http://beta.learn1thing.com/dashboard') !!}/admin/master/user/profile" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="{!! env('DASHBOARD_URL', 'http://beta.learn1thing.com/dashboard') !!}" class="btn btn-default btn-flat">Dashboard</a>
                                <!-- <a href="{!! route('auth.logout') !!}" class="btn btn-default btn-flat">Sign out</a> -->
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>