<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> learn1thing | @yield('web-title')</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.2 -->
        <link href="{{ asset('/vendor/AdminLTE/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="https://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        @stack('plugins-css')
        <!-- Theme style -->
        <link href="{{ asset('/vendor/AdminLTE/dist/css/AdminLTE.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/vendor/AdminLTE/dist/css/skins/skin-blue.min.css')}}" rel="stylesheet" type="text/css" />

        <!-- Custom CSS -->
        <link href="{{ asset('/css/custom.css')}}" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">

        @stack('custom-css')

        <style>
            .nav-new {
                border-radius: 0;
                margin-bottom: 0;
                min-height: 75px;
                background-color: #0a215f;
            }
            li.dropdown.user {
                padding: 15px;
            }
            li.dropdown.user .dropdown-toggle, li.dropdown.user .dropdown-toggle:hover, li.dropdown.user .dropdown-toggle:focus{
                color: #fff;
                background: transparent;
            }
            .box-header .box-title {
                font-family: 'Oxigen', sans-serif;
            }
            .box-body {
                padding: 0;
            }
            .box-body blockquote {
                background: #2354a1;
                color: whitesmoke;
            }

            .footer {
                position: unset;
                bottom: 0;
                background-color: #001e61;
                height: 115px;
                width: 100%;
            }

            footer .intro-title {
                bottom: initial;
                top: initial;
                left: 0px;
                min-height: auto;
                height: 145px;
                width: 320px;
                margin-top: -30px;
            }

            .shape-1 {
                border-right: 28px solid #e1bf13;
                width: 1290px;
                height: 2400px;
                position: absolute;
                background-color: #ecf0f5;
                -ms-transform: rotate(45deg);
                -webkit-transform: rotate(45deg);
                top: -927px;
                right: 565px;
                transform: rotate(45deg);
            }

            footer .intro-title {
                bottom: initial;
                top: initial;
                left: 0px;
                min-height: auto;
                height: 145px;
                width: 320px;
                margin-top: -30px;
            }

            .intro-title{
                height: 450px;
                position: absolute;
                width: 640px;
                top: 0;
                overflow: hidden;
            }

            footer{
                position: absolute;
                bottom: 0;
                background-color: #001e61;
                height: 115px;
                width: 100%;
            }

            .l1t-logo-container{
                height: 90px;
                position: absolute;
                overflow: hidden;
                top: 55px;
                width: 223px;
            }

            .l1t-logo-container .shape-1{
                border-right: 40px solid transparent;
                background-color: #2e2e2e;
            }

            .l1t-logo{
                background-image: url('../../img/l1t-logo.png');
                height: 88px;
                position: absolute;
                width: 128px;
                background-size: 105px;
                background-position: center center;
                background-repeat: no-repeat;
            }

            .l1t-logo.l1t-logo-small{
                background-size: 80px;
            }

            .footer-content{
                color: #ffffff;
                text-align: right;
                width: 60%;
                padding-right: 30px;
                padding-top: 37px;
                position: absolute;
                font-size: 0.75em;
                font-weight: 300;
                right: 0;
            }

            .bold {
                font-weight: 700;
            }

            footer .contacts{
                font-size: 1.5em;
                font-weight: 400;
                margin-top: 11px;
            }

            footer .contacts > span{
                margin-left: 20px;
            }

            footer .contacts > span .icon img{
                width: 20px;
                vertical-align: middle;
                margin-right: 5px;
            }

        </style>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="skin-blue layout-top-nav">

        <nav class="navbar navbar-default nav-new">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <!-- <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button> -->
                    <a class="navbar-brand" href="#">
                        <img src="{{ asset('image/l1t-logo-white.png') }}" width="100px" alt="">
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                {{-- <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <!-- <li class="dropdown user">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown
                            <img src="http://www.placehold.it/20/20/" alt="">
                            <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </li> -->
                    </ul>
                </div><!-- /.navbar-collapse --> --}}
            </div><!-- /.container-fluid -->
        </nav>

        <div class="wrapper">
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <div class="container">
                    <!-- Content Header (Page header) -->
                    {{-- <section class="content-header">
                        <h1>
                            @yield('page-header')
                            <small>@yield('page-description')</small>
                        </h1>
                    </section> --}}

                    <!-- Main content -->
                    <section class="content">
                        <!-- Your Page Content Here -->
                        @yield('content')
                    </section><!-- /.content -->
                </div>
            </div><!-- /.content-wrapper -->

            {{-- <div class="footer">
                <img src="{{ asset('image/footer.png') }}" class="img-responsive" alt="">
            </div> --}}

        </div><!-- ./wrapper -->

        <footer class="footer">
            <div class="intro-title">
                <div class="shape-1">
                </div>
                <div class="l1t-logo-container">
                    <div class="shape-1">
                    </div>
                    <div class="l1t-logo l1t-logo-small">
                    </div>
                </div>
            </div>
            <div class="footer-content">
                <p><span class="bold">learn1thing.com</span> is the E-commerce store dedicated to give a wholesome & engaging learning experience. <br/>The various learning tools, provide an ideal opportunity for those who seek to future-proof themselves.</p>

                <div class="contacts">
                    <span><span class="icon"><img src="{{ URL::asset('/img/document-icon.png') }}"/></span>beta.learn1thing.com</span><span><span class="icon"><img src="{{ URL::asset('/img/phone-icon.png') }}"/></span>+65 63238020</span>
                </div>
            </div>
        </footer>
        <div class="spinner-wrapper">
            <div class="spinner hide"></div>
        </div>


        <!-- REQUIRED JS SCRIPTS -->

        <!-- jQuery 2.1.3 -->
        <script src="{{ asset ('/vendor/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>

        <!-- Bootstrap 3.3.2 JS -->
        <script src="{{ asset ('/vendor/AdminLTE/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>

        @stack('plugins-js')

        <!-- AdminLTE App -->
        <script src="{{ asset ('/vendor/AdminLTE/dist/js/app.min.js') }}" type="text/javascript"></script>

        @stack('custom-js')

        @stack('scripts')
    </body>
</html>