@if(session('success'))
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button> {!! session('success') !!}
	</div>
@endif

@if(session('error'))
	<div class="alert alert-error alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button> {!! session('error') !!}
	</div>
@endif

@if(session('warning'))
	<div class="alert alert-warning alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button> {!! session('warning') !!}
	</div>
@endif