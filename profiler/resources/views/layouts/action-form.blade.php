<div class="btn-group">
    @if (session('user')['role']['slug'])
    @endif

    @if ( isset( $show ) )
        <a href="{!! $show !!}" class="btn btn-success trigger-btn-show {{ isset( $groupBy ) ? 'hide' : '' }}" title="{!! trans('app.button.show') !!}">
            <i class="fa fa-search"></i>
        </a>
    @endif

    @if ((session('user')['role']['slug'] == 'business-partner') || (session('user')['role']['slug'] == 'super-admin'))
        @if ( isset( $edit ) )
            <a href="{!! $edit !!}" class="btn btn-info trigger-btn-edit {{ isset( $groupBy ) ? 'hide' : '' }}" title="{!! trans('app.button.update') !!}">
                <i class="fa fa-pencil-square-o"></i>
            </a>
        @endif

        @if ( isset( $editModal ) )
            <a href="javascript:void(0)" class="btn btn-info btn-edit trigger-btn-editModal {{ isset( $groupBy ) ? 'hide' : '' }}" data-table="{{ $tableName }}" data-url="{!! $editModal !!}" title="{!! trans('app.button.update') !!}">
                <i class="fa fa-pencil-square-o"></i>
            </a>
        @endif

        @if ( isset( $delete ) )
            <a href="javascript:void(0)" class="btn btn-danger btn-delete trigger-btn-delete {{ isset( $groupBy ) ? 'hide' : '' }}" data-url="{!! $delete !!}" title="{!! trans('app.button.delete') !!}">
                <i class="fa fa-trash"></i>
            </a>
        @endif

        @if ( isset( $buyer ) )
            <a href="javascript:void(0)" class="btn btn-default btn-ajax trigger-btn-buyer {{ isset( $groupBy ) ? 'hide' : '' }}" title="{!! trans('app.button.list_buyer') !!}" data-href="{!! $buyer !!}">
                <i class="fa fa-file-text-o"></i>
            </a>
        @endif

        @if ( isset( $default ) )
            @if ($data->id != $data->email_builder_id)
                <a href="javascript:void(0)" class="btn btn-primary btn-ajax" data-action="set_report" title="Set as Default" data-href="{!! $default !!}">
                    <i class="fa fa-check"></i>
                </a>
            @else
                -
            @endif
        @endif
    @endif

    @if ((session('user')['role']['slug'] == 'standard-customer') || (session('user')['role']['slug'] == 'corporate-customer'))
        @if ( isset( $observer ) )
            <a href="javascript:void(0)" class="btn btn-default btn-ajax trigger-btn-batch {{ isset( $groupBy ) ? 'hide' : '' }}" title="{!! trans('app.button.list_observer') !!}" data-href="{!! $observer !!}">
                <i class="fa fa-file-text-o"></i>
            </a>
        @endif
    @endif

    @if ( isset( $generate ) )
        <a href="{!! $generate !!}" class="btn btn-success trigger-btn-generate {{ isset( $groupBy ) ? 'hide' : '' }}" title="{!! trans('app.button.generate') !!}" target="_blank">
            <i class="fa fa-file-pdf-o"></i>
        </a>
    @endif

    <!-- addedd by rdw k5 081217-->
    @if ( isset( $duplicate ) )
        <a href="{!! $duplicate !!}" class="btn btn-warning trigger-btn-generate " title="Duplicate Data" >
            <i class="fa fa-files-o"></i>
        </a>
    @endif


    @if ( isset( $preview ) )
        <a href="{!! $preview !!}" class="btn btn-warning btn-preview trigger-btn-preview {{ isset( $groupBy ) ? 'hide' : '' }}" title="{!! trans('app.button.preview') !!}" target="_blank">
            <i class="fa fa-eye"></i>
        </a>
    @endif

    @if ( isset( $retake ) )
        <a href="javascript:void(0)" data-href="{!! $retake !!}" class="btn btn-danger btn-ajax trigger-btn-retake {{ isset( $groupBy ) ? 'hide' : '' }}" title="{!! trans('app.button.retake') !!}">
            <i class="fa fa-rotate-left"></i>
        </a>
    @endif

    @if ( isset( $groupBy ) )
        <div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                Action
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">

                @if ( isset( $show ) )
                    <li>
                        <a href="{!! $show !!}">{!! trans("app.button.show") !!}</a>
                    </li>
                @endif

                @if ((session('user')['role']['slug'] == 'business-partner') || (session('user')['role']['slug'] == 'super-admin'))
                    @if ( isset( $edit ) )
                        <li>
                            <a href="{!! $edit !!}">{!! trans("app.button.update") !!}</a>
                        </li>
                    @endif

                    @if ( isset( $editModal ) )
                        <li>
                            <a href="javascript:void(0)" class="trigger-btn-editModal" data-table="{{ $tableName }}" data-url="{!! $editModal !!}">
                                {!! trans("app.button.update") !!}
                            </a>
                        </li>
                    @endif

                    @if ( isset( $delete ) )
                        <li>
                            <a href="javascript:void(0)" class="btn-delete trigger-btn-delete" data-url="{!! $delete !!}">
                                {!! trans("app.button.delete") !!}
                            </a>
                        </li>
                    @endif

                    @if ( isset( $buyer ) )
                        <li>
                            <a href="javascript:void(0)" class="btn-ajax trigger-btn-buyer" data-href="{!! $buyer !!}">
                                {!! trans("app.button.list_buyer") !!}
                            </a>
                        </li>
                    @endif

                    @if ( isset( $reportTemplate ) )
                        <li>
                            <a href="javascript:void(0)" class="btn-ajax trigger-btn-template" data-href="{!! $reportTemplate !!}">
                                Report List
                            </a>
                        </li>
                    @endif

                    @if ( isset( $duplicateData ) )
                        <li>
                            <a href="{!! $duplicateData !!}">
                               Duplicate Data
                            </a>
                        </li>
                    @endif
                @endif

                @if ((session('user')['role']['slug'] == 'standard-customer') || (session('user')['role']['slug'] == 'corporate-customer'))
                    @if ( isset( $observer ) )
                        <li>
                            <a href="javascript:void(0)" class="btn-ajax trigger-btn-batch" data-href="{!! $observer !!}">
                                {!! trans("app.button.list_observer") !!}
                            </a>
                        </li>
                    @endif
                @endif

                @if ( isset( $generate ) )
                    <li>
                        <a href="{!! $generate !!}" target="_blank">{!! trans("app.button.generate") !!}</a>
                    </li>
                @endif

                @if ( isset( $preview ) )
                    <li>
                        <a href="{!! $preview !!}" target="_blank">{!! trans("app.button.preview") !!}</a>
                    </li>
                @endif

                @if ( isset( $retake ) )
                    <li>
                        <a href="{!! $retake !!}">{!! trans("app.button.retake") !!}</a>
                    </li>
                @endif
            </ul>
        </div>
    @endif

</div>