<!DOCTYPE html>
<html>
    <head>

	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}"> 

        <meta charset="UTF-8">
        <title> {!! config('app.name') !!} | @yield('web-title')</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.2 -->
        <link href="{{ asset('/vendor/AdminLTE/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="https://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        @stack('plugins-css')
        <!-- Theme style -->
        <link href="{{ asset('/vendor/AdminLTE/dist/css/AdminLTE.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/vendor/AdminLTE/dist/css/skins/skin-blue.min.css')}}" rel="stylesheet" type="text/css" />

        <!-- Datatables style -->
        <link href="{{ asset('/vendor/AdminLTE/plugins/datatables/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css" />

        <!-- Custom CSS -->
        <link href="{{ asset('/css/custom.css')}}" rel="stylesheet" type="text/css" />

        @stack('custom-css')

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="hold-transition skin-blue sidebar-mini">

        <div class="wrapper">
            <!-- Header -->
            @include('layouts/header')

            <!-- Sidebar -->
            @include('layouts/sidebar')

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <!-- <section class="content-header">
                    <h1>
                        @yield('page-header')
                        <small>@yield('page-description')</small>
                    </h1>
                    You can dynamically generate breadcrumbs here
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                        <li class="active">Here</li>
                    </ol>
                </section> -->

                <!-- Main content -->
                <section class="content">
                    @include('layouts.session')
                    <!-- Your Page Content Here -->
                    @yield('content')
                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->


            <!-- Footer -->
            @include('layouts/footer')

        </div><!-- ./wrapper -->
        <div class="spinner-wrapper">
            <div class="spinner hide"></div>
        </div>

        <!-- REQUIRED JS SCRIPTS -->

        <!-- jQuery 2.1.3 -->
        <script src="{{ asset ('/vendor/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>

        <!-- Bootstrap 3.3.2 JS -->
        <script src="{{ asset ('/vendor/AdminLTE/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>

        @stack('plugins-js')

        <!-- AdminLTE App -->
        <script src="{{ asset ('/vendor/AdminLTE/dist/js/app.min.js') }}" type="text/javascript"></script>

        @stack('custom-js')

        <!-- Datatables JS -->
        <script src="{{ asset('/vendor/AdminLTE/plugins/datatables/jquery.dataTables.min.js')}}" type="text/javascript"></script>
        <script src="{{ asset('/vendor/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js')}}" type="text/javascript"></script>

        @stack('scripts')
    </body>
</html>