<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <!-- <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('/image/no-avatar.png') }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{!! FrontendHelper::fullName() !!}</p>
                Status
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div> -->

        <!-- search form (Optional) -->
        <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form> -->
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">HEADER</li>
            <li>{!! FrontendHelper::link('fa-circle-o', RouteHelper::token('surveys.index'), 'Survey') !!}</li>
            <li>{!! FrontendHelper::link('fa-circle-o', RouteHelper::token('category-profile.index'), 'Category Profile') !!}</li>
            @if ((session('user')['role']['slug'] == 'business-partner') || (session('user')['role']['slug'] == 'super-admin'))
                <li>{!! FrontendHelper::link('fa-circle-o', RouteHelper::token('formulas.index'), 'Formula') !!}</li>
                <li>{!! FrontendHelper::link('fa-circle-o', RouteHelper::token('template-builder.index'), 'Report Template') !!}</li>
            @endif
            <!-- <li>{!! FrontendHelper::link('fa-circle-o', RouteHelper::token('attributes.index'), 'Attribute') !!}</li> -->
            <!-- <li>{!! FrontendHelper::link('fa-circle-o', RouteHelper::token('categories.index'), 'Category') !!}</li> -->
            <!-- <li>{!! FrontendHelper::link('fa-circle-o', RouteHelper::token('observers.index'), 'Observer') !!}</li> -->
            <!-- <li>{!! FrontendHelper::link('fa-circle-o', RouteHelper::token('batches.index'), 'Batch Reference') !!}</li> -->
            <!-- Optionally, you can add icons to the links -->
            <!-- <li><a href="#"><i class="fa fa-circle-o"></i> <span>Link</span></a></li>
            <li><a href="#"><span>Another Link</span></a></li>
            <li class="treeview">
                <a href="#"><span>Multilevel</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#">Link in level 2</a></li>
                    <li><a href="#">Link in level 2</a></li>
                </ul>
            </li> -->
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>