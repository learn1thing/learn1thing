@extends('layouts.admin_template')
 
@section('content')
<div class="row section">
	<div class="col-sm-8 col-md-offset-2">

	@foreach ($items as $item)
		{!! Form::open(array('route' => 'section.store','method'=>'POST', 'class' => 'form-horizontal')) !!}
	    <div class="box">
	      <div class="box-header with-border">
	        <h3 class="box-title">Section 1</h3>
	        <div class="box-tools pull-right">
	          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
	            <i class="fa fa-minus"></i></button>
	          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
	            <i class="fa fa-times"></i></button>
	        </div>
	      </div>
	      <div class="box-body">
	      	<div class="form-group">
            <div class="col-sm-12">
              <input type="text" class="form-control" id="title" name="title" placeholder="Form title" value="{{ $item->title }}">
            </div>
          </div>
	      	<div class="form-group">
            <div class="col-sm-12">
            	<textarea class="form-control" rows="3" name="description" placeholder="Form description" value="{{ $item->description }}">{{ $item->description }}</textarea>
            </div>
          </div>
        	<div class="questions">
        		<!-- render questions here -->
        	</div>
	      </div>
	      <div class="box-footer">
	      	<a href="#" class="btn btn-default pull-right add-question">Add Question</a>
	      	<a type="submit" class="btn btn-default add-question">Submit</a>
	      </div>
	      <!-- /.box-body -->
	    </div>
		{!! Form::close() !!}
	@endforeach
	</div>
</div>
<div class="row">
	<div class="col-sm-8 col-md-offset-2">
	    <div class="">
		<button type="button" id="add" class="btn btn-default pull-right" data-widget="add" data-toggle="tooltip" title="add">
	        <i class="fa fa-plus-square"></i>
	    </button>
	</div>
	</div>
</div>


@endsection

@push('scripts')
	
	<script id="question-template" type="text/template">
		<div class="question callout">
		  <label>Question: </label>
		  <textarea id="question" name="question" class="form-control" rows="3" placeholder="Enter ..."></textarea>
		  
		  <br>
		  
		  <div class="form-group">
		    <label class="col-sm-2">Type :</label>
		    <div class="col-sm-5">
		      <select class="answer-type form-control">
		      	<option value="multiple-choice">Multiple choice</option>
		        <option value="checkboxes">Checkboxes</option>
		        <option value="matching">Matching</option>
		        <option value="likert-scale">Likert scale</option>
		        <option value="true-or-false">True or false</option>
		        <option value="ranking">Ranking</option>
		        <option value="free-text">Free text</option>
		        <option value="multiple-choice-with-constraint">Multiple choice with constraint</option>
		        <option value="datetime">Date and/or time</option>
		        <option value="distributed-points">Distributed points</option>
		      </select>
		    </div>
		  </div>
		  <div class="row answer-options">

		  </div>
		  
		  <div class="box-footer">
		    <div class="row">
		    	<div class="col-sm-12">
		      	<div class="pull-right">
		      		<button class="btn"><i class="fa fa-fw fa-trash"></i></button>
		      	</div>
		      </div>
		    </div>
		  </div>
		</div>
	</script>

	<!-- template for multiple choice -->
	<script id="template-multiple-choice" type="text/template">
  	<div class="col-sm-12">
  		<label>Option answers: </label>
  		<div class="answer-elements">
  			<!-- render template here -->
  		</div>
    	<div class="form-group">
    		<div class="col-sm-12 action-add-element">
        	<button class="btn btn-default pull-right add-answer-option-element" data-template-element-name="template-multiple-choice-answer-element">
        		<i class="fa fa-fw fa-plus-square"></i>
        	</button>
    		</div>
      </div>
  	</div>
	</script>
	<script id="template-multiple-choice-answer-element" type="text/template">
		<div class="form-group">
			<div class="col-sm-11">
				<div class="input-group">
          <span class="input-group-addon">
            <input type="radio" disabled="disabled">
          </span>
        	<input type="text" value="Option 1" placeholder="Option 1" class="form-control">
      	</div>
			</div>
			<div class="col-sm-1">
				<button class="btn btn-default pull-right delete-answer-option-element"><i class="fa fa-fw fa-remove"></i></button>
			</div>
		</div>
	</script>
	<!-- end template for multiple choice -->

	<!-- template for checkboxes -->
	<script id="template-checkboxes" type="text/template">
		<div class="col-sm-12">
			<label>Option answers: </label>
			<div class="answer-elements">
  			<!-- render template here -->
  		</div>
			<div class="form-group">
	      <div class="col-sm-12">
	      	<button class="btn btn-default pull-right"><i class="fa fa-fw fa-plus-square"></i></button>
	  		</div>
	    </div>
		</div>
	</script>
	<script id="template-checkboxes-choice-answer-element" type="text/template">
		<div class="form-group">
			<div class="col-sm-11">
				<div class="input-group">
          <span class="input-group-addon">
            <input type="checkbox" disabled="disabled">
          </span>
        	<input type="text" value="Option 1" placeholder="Option 1" class="form-control">
      	</div>
			</div>
			<div class="col-sm-1">
				<button class="btn btn-default pull-right"><i class="fa fa-fw fa-remove"></i></button>
			</div>
		</div>
	</script>
	<!--end template for checkboxes -->

	<!-- template for matching -->
	<script id="template-matching" type="text/template">
		<div class="col-sm-12">
			<label>Option answers: </label>
			<div class="form-group">
	  		<div class="col-sm-5">
	    		<input type="text" class="form-control" id="inputEmail3" placeholder="Row 1 Column 1">
	    	</div>
	    	<div class="col-sm-5">
	    		<input type="text" class="form-control" id="inputEmail3" placeholder="Row 1 Column 2">
	    	</div>
	    	<div class="col-sm-2">
					<button class="btn btn-default pull-right"><i class="fa fa-fw fa-remove"></i></button>
				</div>
	  	</div>
	  	<div class="form-group">
	  		<div class="col-sm-5">
	    		<input type="text" class="form-control" id="inputEmail3" placeholder="Row 2 Column 1">
	    	</div>
	    	<div class="col-sm-5">
	    		<input type="text" class="form-control" id="inputEmail3" placeholder="Row 2 Column 2">
	    	</div>
	    	<div class="col-sm-2">
					<button class="btn btn-default pull-right"><i class="fa fa-fw fa-remove"></i></button>
				</div>
	  	</div>
	  	<div class="form-group">
	  		<div class="col-sm-12">
	      	<button class="btn btn-default pull-right"><i class="fa fa-fw fa-plus-square"></i></button>
	  		</div>
	    </div>
		</div>
	</script>
	<!--end template for matching -->

	<!-- template for likert scale -->
	<script id="template-likert-scale" type="text/template">
		<div class="col-sm-12">
			<label>Option answers: </label>
			<div class="form-group">
	  		<div class="col-sm-5">
	    		<input type="text" class="form-control" placeholder="Label 1">
	    	</div>
	    	<div class="col-sm-5">
	    		<input type="number" class="form-control" placeholder="Value 1">
	    	</div>
	    	<div class="col-sm-2">
					<button class="btn btn-default pull-right"><i class="fa fa-fw fa-remove"></i></button>
				</div>
	  	</div>
	  	<div class="form-group">
	  		<div class="col-sm-5">
	    		<input type="text" class="form-control" placeholder="Label 2">
	    	</div>
	    	<div class="col-sm-5">
	    		<input type="number" class="form-control" placeholder="Value 2">
	    	</div>
	    	<div class="col-sm-2">
					<button class="btn btn-default pull-right"><i class="fa fa-fw fa-remove"></i></button>
				</div>
	  	</div>
	  	<div class="form-group">
	  		<div class="col-sm-12">
	      	<button class="btn btn-default pull-right"><i class="fa fa-fw fa-plus-square"></i></button>
	  		</div>
	    </div>
		</div>
	</script>
	<!--end template for likert scale -->

	<!-- template for ranking -->
	<script id="template-ranking" type="text/template">
		<div class="col-sm-12">
			<div class="form-group">
				<label class="col-sm-2">Max Point: </label>
	  		<div class="col-sm-5">
	    		<input type="number" class="form-control" placeholder="Max value of ranking">
	    	</div>
	  	</div>
		</div>
	</script>
	<!--end template for ranking -->

	<!-- template for multiple choice with constraint -->
	<script id="template-multiple-choice-with-constraint" type="text/template">
		<div class="col-sm-12">
			<div class="form-group">
				<label class="col-sm-2">Max Selected: </label>
	  		<div class="col-sm-5">
	    		<input type="number" class="form-control" placeholder="Max value of options selected">
	    	</div>
	  	</div>

	  	<label>Option answers: </label>
			<div class="form-group">
				<div class="col-sm-11">
					<div class="input-group">
	          <span class="input-group-addon">
	            <input type="checkbox" disabled="disabled">
	          </span>
	        	<input type="text" value="Option 1" placeholder="Option 1" class="form-control">
	      	</div>
				</div>
				<div class="col-sm-1">
					<button class="btn btn-default pull-right"><i class="fa fa-fw fa-remove"></i></button>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-11">
					<div class="input-group">
	          <span class="input-group-addon">
	            <input type="checkbox" disabled="disabled">
	          </span>
	        	<input type="text" value="Option 2" placeholder="Option 2" class="form-control">
	      	</div>
				</div>
				<div class="col-sm-1">
					<button class="btn btn-default pull-right"><i class="fa fa-fw fa-remove"></i></button>
				</div>
			</div>
			<div class="form-group">
	      <div class="col-sm-12">
	      	<button class="btn btn-default pull-right"><i class="fa fa-fw fa-plus-square"></i></button>
	  		</div>
	    </div>
		</div>
	</script>
	<!--end template for multiple choice with constraint -->

	<!-- template for distributed points -->
	<script id="template-distributed-points" type="text/template">
		<div class="col-sm-12">
			<div class="form-group">
				<label class="col-sm-2">Point on hand: </label>
	  		<div class="col-sm-5">
	    		<input type="number" class="form-control" placeholder="Point on hand">
	    	</div>
	  	</div>

	  	<label>Option answers: </label>
			<div class="form-group">
				<div class="col-sm-11">
	        <input type="text" value="Option 1" placeholder="Option 1" class="form-control">
				</div>
				<div class="col-sm-1">
					<button class="btn btn-default pull-right"><i class="fa fa-fw fa-remove"></i></button>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-11">
					<input type="text" value="Option 2" placeholder="Option 2" class="form-control">
				</div>
				<div class="col-sm-1">
					<button class="btn btn-default pull-right"><i class="fa fa-fw fa-remove"></i></button>
				</div>
			</div>
			<div class="form-group">
	      <div class="col-sm-12">
	      	<button class="btn btn-default pull-right"><i class="fa fa-fw fa-plus-square"></i></button>
	  		</div>
	    </div>
		</div>
	</script>
	<!--end template for distributed points -->

	<!-- CK Editor -->
	<script src="/vendor/AdminLTE/plugins/ckeditor/ckeditor.js"></script>
	
	<script type="text/javascript">
		var template = $('#question-template').html();
		var questionCount = $('.questions .question.callout').length;
		var questionInputId = 'question-'+ (parseInt(questionCount)+1)
		var question = $('.questions').append(template);
		question.find('textarea#question').attr('id', questionInputId);
		renderCkeditor(questionInputId);

		$('.add-question').click(function (e) {
			e.preventDefault();
			var parent = $(this).parents('.box');
			var questionCount = $('.questions .question.callout').length;
			var questionInputId = 'question-'+ (parseInt(questionCount)+1);
			parent.find('.box-body .questions').append(template);
			question.find('textarea#question').attr('id', questionInputId);
			renderCkeditor(questionInputId);
		});
		
		function renderCkeditor(className) {
    	// Replace the <textarea id="editor1"> with a CKEditor
	    // instance, using default configuration.
	    CKEDITOR.replace(className, {extraPlugins: 'embed'});
    }

    $("select.answer-type")
	  .change(function () {
	  	var templateName = '#template-' + $(this).val();
	  	var template = $(templateName).html();
	  	if(template) {
	    	var answerTemplate = $(this).parents('.question').find('.answer-options').html(template);
	    	var answerElementTemplate = $(templateName + '-answer-element').html();
	    	answerTemplate.find('.answer-elements').append(answerElementTemplate);
	  	} else {
	  		$(this).parents('.question').find('.answer-options').html('');
	  	}
	  });

	  $('.answer-options').delegate('.add-answer-option-element', 'click', function (e) {
			e.preventDefault();
			var parent = $(this).parents('.col-sm-12');
			var templateElementName = $(this).data('template-element-name');
			var templateElement = $('#' + templateElementName).html();
			parent.find('.answer-elements').append(templateElement);
		});

		$('.answer-options').delegate('.delete-answer-option-element', 'click', function (e) {
			e.preventDefault();
			$(this).parents('.form-group').remove();
		});
	</script>

	<script type="text/javascript">
		$(document).ready(function () {

	        var scntDiv = $('.section');
	        var i = $(' .section').size() + 1;
	        
	        $('#add').click(function() {
	                $('<div class="col-sm-8 col-md-offset-2"><form class="form-horizontal"><div class="box"><div class="box-header with-border"><h3 class="box-title">Section ' + i + '</h3><div class="box-tools pull-right"><button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button><button type="button" class="btn btn-box-tool"  data-widget="remove" data-toggle="tooltip" title="Remove" id="remove_'+i+'"><i class="fa fa-times"></i></button></div></div><div class="box-body"><div class="form-group"><div class="col-sm-12"><input type="text" class="form-control" id="title" placeholder="Form title"></div></div><div class="form-group"><div class="col-sm-12"><textarea class="form-control" rows="3" placeholder="Form description"></textarea></div></div><div class="questions"></div></div><div class="box-footer"><a href="#" class="btn btn-default pull-right add-question">Add Question</a><a type="submit" class="btn btn-default add-question">Submit</a></div><!-- /.box-body --></div></form></div>').appendTo(scntDiv);
	                i++;
	                return false;
	        });

	        console.log(i);
	        $('#remove_'+i).click(function() {
	        	console.log('masuk'); 
                if( i > 2 ) {
                    $(this).parents('.section').remove();
                    i--;
                }
                return false;
	        });
		});
	</script>
@endpush

@push('scripts')
	
@endpush