@extends('layouts.admin_template')

@section('content')
	<a type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add</a>

	<div class="row">
 		<div class="col-xs-6 col-md-3">
	    	<a href="#" class="thumbnail">
	      		<img src="{{ asset('uploads/forms-blank.png') }}" data-toggle="modal" data-target="#myModal">
	    	</a>
  		</div>
	</div>
	<label class="checkbox-inline">
  		<input type="checkbox" checked data-toggle="toggle"> First
	</label>
	<div class="container">
	    <h2>Section</h2>
	    <p>Recents forms</p>
	</div>     
	<div class="row">
	@foreach ($items as $item)
		<div class="col-xs-6 col-md-3">
		
    		<div class="thumbnail">
      			<img src="{{ asset('uploads/lit.jpg') }}" data-toggle="modal" data-target="#myModal">
      			<div class="caption">
			        <h3>{{ $item->id }}</h3>
			        <h3>{{ $item->title }}</h3>
			        <p>{{ $item->description }}</p>
			        <p><a href="#" class="btn btn-default" role="button"><span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span></a> 
			        <a href="#" class="btn btn-default" role="button"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></p>
			    </div>
    		</div>
    	
		</div>
	@endforeach
	</div>
{!! $items->render() !!}
				


	<!-- Modal -->
@endsection

@section('content')
<div id="myModal" class="modal fade" role="dialog">
	  	<div class="modal-dialog">
	    	<!-- Modal content-->
	    	{!! Form::open(array('route' => 'section.store','method'=>'POST')) !!}
			    <div class="modal-content">
			      	<div class="modal-header">
			        	<button type="button" class="close" data-dismiss="modal">&times;</button>
			        	<h4 class="modal-title">New Section</h4>
			      	</div>
			      	<div class="modal-body">
					  	<div class="form-group">
					    	<label for="name">Name</label>
					    	<input type="name" class="form-control" id="name" name="name">
					  	</div>
					  	<div class="form-group">
					    	<label for="description">Description</label>
					    	<textarea class="form-control" rows="5" id="description" name="description"></textarea>
					  	</div>
					  	<div class="form-group">

					    	<table class="table table-condensed">
					    		<thead>
							      	<tr>
							        	<th>Question</th>
							        	<th>Type</th>
							      	</tr>
							    </thead>
						    	<tbody>
						      		<tr>
						        		<td>
					    					<input type="name" class="form-control" id="name" name="name">
					    				</td>
						        		<td>
											<div class="dropdown">
											  	<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
											    Type
											    	<span class="caret"></span>
											  	</button>
											  	<ul class="dropdown-menu" aria-labelledby="dropdownMenu1" role="menu">
											    	<li><a href="#">Checkbox</a></li>
											    	<li><a href="#">Multiples Choice</a></li>
											  	</ul>
											</div>
										</td>
										<td>
											<button type="button" class="btn btn-primary btn-sm glyphicon glyphicon-plus">Add</button>
										</td>
						      		</tr>
						    	</tbody>
						  	</table>
					  	</div>
			      	</div>
			      	<div class="modal-footer">
			      		<button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn"></i> Create
                        </button>
			        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      	</div>
			    </div>
		    {!! Form::close() !!}
	  	</div>
	</div>
@push('styles')

@endpush

@push('scripts')
<script type="text/javascript">
	console.log('abcd');
	$( document.body ).on( 'click', '.dropdown .dropdown-menu li', function( event ) {
      var $target = $( event.currentTarget );
      $target.closest( '.btn-group' )
         .find( '[data-bind="label"]' ).text( $target.text() )
            .end()
         .children( '.dropdown-toggle' ).dropdown( 'toggle' );
      return false;
   });
</script>
@endpush