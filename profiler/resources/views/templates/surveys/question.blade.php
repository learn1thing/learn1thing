<!-- template for question -->
<script id="question-template" type="text/template">
	<div class="question callout">
		<div class="row">
			<div class="form-group clearfix">
				<label class="col-sm-2">Question Number  :</label>
				<div class="col-sm-5">
					<input type="text" class="form-control input-question-number" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][question_number]" placeholder="Enter the number of question" <%= automatic_numbering == 'yes' ? 'disabled':'' %>>
				</div>
				<div class="help-block with-errors"></div>
			</div>
		</div>
		<div class="row">
			<div class="form-group question-name clearfix">
				<label class="col-sm-2">Name (<span class="text-red">*</span>) :</label>
				<div class="col-sm-5">
					<input type="text" class="form-control input-question-name" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][name]" placeholder="Enter the name of question" required>
				</div>
				<div class="help-block with-errors"></div>
			</div>
		</div>
		<div class="form-group question-content">
			<label>Question (<span class="text-red">*</span>) :</label>
			<textarea id="question" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][content]" class="form-control question_contents" rows="3" placeholder="Enter ..." required></textarea>
			<div class="help-block with-errors"></div>
		</div>
		<br>
		<div class="row">
			<div class="form-group clearfix">
				<label class="col-sm-2">Answer Type (<span class="text-red">*</span>):</label>
				<div class="col-sm-5">
					<select class="answer-type form-control" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_type]" required>
						<option value=""></option>
						<option value="free_text">Free text</option>
						<option value="multichoice">Multiple choice</option>
						<option value="checkboxes">Checkboxes</option>
						{{-- <option value="matching">Matching</option> --}}
						{{-- <option value="likert_scale">Likert scale</option> --}}
						<option value="boolean">True or false</option>
						{{-- <option value="ranking">Ranking</option> --}}
						<option value="multiple_choice_with_constraint">Multiple choice with constraint</option>
						<option value="date">Date</option>
						<option value="time">Time</option>
						<option value="datetime">Date and time</option>
						<option value="distributed_points">Distributed points</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row answer-options">

		</div>
		<br>
		<div class="box-footer">
			<div class="row">
				<div class="pull-right">
					<label>Required: </label>
					<input type="checkbox" checked data-size="small" data-toggle="toggle" data-on="" data-off="" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][is_required]" class="toggle-mandatory">
					<button type="button" class="btn btn-default remove-question" title="Remove question"><i class="fa fa-fw fa-trash"></i></button>
				</div>
			</div>
		</div>
	</div>
</script>
<!-- end template for question -->