<!-- template for section -->
<script id="section-template" type="text/template">
	<div class="panel box box-primary section">
		<div class="box-header with-border">
			<h4 class="box-title">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
					Section 1
				</a>
			</h4>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool delete-section" title="Remove"><i class="fa fa-remove"></i></button>
			</div>
		</div>
		<div id="collapseOne" class="panel-collapse collapse in">
			<div class="box-body">
				<div class="form-group clearfix">
					<label class="col-sm-2">Title (<span class="text-red">*</span>) :</label>
					<div class="col-sm-5">
						<input type="text" placeholder="Enter title" class="form-control" name="<%= survey_type %>[<%= section_index %>][title]" required>
					</div>
					<div class="help-block with-errors"></div>
				</div>
				<div class="form-group clearfix">
					<label class="col-sm-2">Description (<span class="text-red">*</span>) :</label>
					<div class="col-sm-5">
						<textarea id="inputDescription" class="form-control" rows="3" placeholder="Section description" name="<%= survey_type %>[<%= section_index %>][description]" required></textarea>
					</div>
					<div class="help-block with-errors"></div>
				</div>
				<div class="form-group clearfix">
          <label class="col-sm-2">Automatic numbering (<span class="text-red">*</span>) :</label>
          <div class="col-sm-5">
            <select name="<%= survey_type %>[<%= section_index %>][automatic_numbering]" class="form-control select-automatic-numbering" required>
              <option></option>
              <option value="yes">Yes</option>
              <option value="no">No</option>
            </select>
          </div>
          <div class="help-block with-errors"></div>
        </div>
				<div class="form-group clearfix">
					<label class="col-sm-2">Ordering type (<span class="text-red">*</span>) :</label>
					<div class="col-sm-5">
						<select name="<%= survey_type %>[<%= section_index %>][ordering_type]" class="form-control select-ordering-type">
							<option value=""></option>
							<option value="continue">Continue from previous section</option>
							<option value="reset">Reset</option>
						</select>
					</div>
					<div class="help-block with-errors"></div>
				</div>
				<div class="form-group clearfix">
					<label class="col-sm-2">Numbering type (<span class="text-red">*</span>) :</label>
					<div class="col-sm-5">
						<select name="<%= survey_type %>[<%= section_index %>][type]" class="form-control select-numbering-type">
							<option value=""></option>
							<option value="arabic">Arabic</option>
							<option value="alphabet">Alphabet</option>
							<option value="roman">Roman</option>
						</select>
					</div>
					<div class="help-block with-errors"></div>
				</div>
				<div class="questions">
					<!-- render questions here -->
				</div>
				<div class="">
					<a href="#" class="btn btn-default pull-right add-question">Add Question</a>
				</div>
			</div>
		</div>
	</div>
</script>
<!-- end template for section -->