<!-- template for boolean -->
<script id="template-boolean" type="text/template">
	<div class="row">
		<div class="col-sm-4">
			<label>Label true (<span class="text-red">*</span>) :</label>
			<input type="text" class="form-control answer-configs-label-true" placeholder="Label for true value" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_configs][]" required>
			<div class="help-block with-errors"></div>
		</div>
		<div class="col-sm-4">
			<label>Attributes :</label>
			<select class="form-control select2 question-attributes" multiple="multiple" data-placeholder="Select some attributes" style="width: 100%;" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_attributes][0][]">
			</select>
		</div>
		<div class="col-sm-4">
			<label>Score :</label>
			<input type="number" placeholder="Enter score" class="form-control input-number" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_scores][]">
		</div>
	</div>
	<div class="row">
		<div class="col-sm-4">
			<label>Label false (<span class="text-red">*</span>) :</label>
			<input type="text" class="form-control answer-configs-label-false" placeholder="Label for false value" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_configs][]" required>
			<div class="help-block with-errors"></div>
		</div>
		<div class="col-sm-4">
			<label>Attributes :</label>
			<select class="form-control select2 question-attributes" multiple="multiple" data-placeholder="Select some attributes" style="width: 100%;" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_attributes][1][]">
			</select>
		</div>
		<div class="col-sm-4">
			<label>Score :</label>
			<input type="number" placeholder="Enter score" class="form-control input-number" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_scores][]">
		</div>
	</div>
</script>
<!--end template for boolean -->