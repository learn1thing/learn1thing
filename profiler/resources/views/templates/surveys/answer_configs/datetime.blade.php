<!-- template for datetime -->
<script id="template-datetime" type="text/template">
	<div class="col-sm-12">
		<div class="row">
			<div class="form-group clearfix">
				<label class="col-sm-2">Attributes: </label>
				<div class="col-sm-5">
					<select class="form-control select2 question-attributes" multiple="multiple" data-placeholder="Select some attributes" style="width: 100%;" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_attributes][]">
					</select>
				</div>
			</div>
			<div class="form-group clearfix">
				<label class="col-sm-2">{{ trans('app.label.start_date') }} (<span class="text-red">*</span>) :</label>
				<div class="col-sm-5">
					<div class="input-group date">
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</div>
						<input type="text" class="form-control start_date datepicker" placeholder="Enter the start date" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_configs][start_date]" required>
					</div>
				</div>
				<div class="col-sm-5 help-block with-errors"></div>
			</div>
			<div class="form-group clearfix">
				<label class="col-sm-2">{{ trans('app.label.end_date') }} (<span class="text-red">*</span>) :</label>
				<div class="col-sm-5">
					<div class="input-group date">
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</div>
						<input type="text" class="form-control end_date datepicker" placeholder="Enter the end date" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_configs][end_date]" required>
					</div>
				</div>
				<div class="col-sm-5 help-block with-errors"></div>
			</div>
			<div class="form-group clearfix">
				<label class="col-sm-2">Score: </label>
				<div class="col-sm-5">
					<input type="number" placeholder="Enter score" class="form-control input-number" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_scores]">
				</div>
			</div>
		</div>
	</div>
</script>
<!-- template for end datetime -->