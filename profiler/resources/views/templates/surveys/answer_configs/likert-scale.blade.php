<!-- template for likert scale -->
<script id="template-likert_scale" type="text/template">
	<div class="col-sm-12">
		{{-- <label>Answer options (<span class="text-red">*</span>) :</label> --}}
		<div class="row answer-elements">
			<!-- render template here -->
		</div>
		<div class="row">
			<div class="row col-sm-12">
				<div class="form-group form-group col-md-1 col-md-offset-11">
					<button class="btn btn-default add-answer-option-element" data-template-element-name="template-likert_scale-answer-element"><i class="fa fa-fw fa-plus-square"></i></button>
				</div>
			</div>
		</div>
	</div>
</script>
<script id="template-likert_scale-answer-element" type="text/template">
	{{-- <div class="form-group clearfix">
		<div class="col-sm-3 likert-input">
  		<input type="text" class="form-control answer-configs" placeholder="Label" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_configs][<%= answer_index %>][label]">
  	</div>
  	<div class="col-sm-3 likert-input">
  		<input type="number" class="form-control answer-configs" placeholder="Value" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_configs][<%= answer_index %>][value]">
  	</div>
  	<div class="col-sm-3">
    	<select class="form-control select2 question-attributes" multiple="multiple" data-placeholder="Select some attributes" style="width: 100%;" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_attributes][<%= answer_index %>][]">
      </select>
		</div>
		<div class="col-sm-2">
			<input type="number" placeholder="Enter score" class="form-control" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_scores][]">
		</div>
  	<div class="col-sm-1">
			<button class="btn btn-default pull-right delete-answer-option-element"><i class="fa fa-fw fa-remove"></i></button>
		</div>
	</div> --}}
	<div class="row">
		<div class="col-sm-3 likert-input">
  		<label>Label (<span class="text-red">*</span>) :</label>
  		<input type="text" class="form-control answer-configs-label" placeholder="Label" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_configs][<%= answer_index %>][label]" required>
  		<div class="help-block with-errors"></div>
  	</div>
  	<div class="col-sm-3 likert-input">
  		<label>Value (<span class="text-red">*</span>) :</label>
  		<input type="number" class="form-control answer-configs-value input-number" placeholder="Value" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_configs][<%= answer_index %>][value]" required>
  		<div class="help-block with-errors"></div>
  	</div>
  	<div class="col-sm-3">
  		<label>Attributes :</label>
    	<select class="form-control select2 question-attributes" multiple="multiple" data-placeholder="Select some attributes" style="width: 100%;" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_attributes][<%= answer_index %>][]">
      </select>
		</div>
		<div class="col-sm-2">
			<label>Score :</label>
			<input type="number" placeholder="Enter score" class="form-control input-number" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_scores][]">
		</div>
		<div class="col-sm-1">
			<label>Action</label>
			<button class="btn btn-default delete-answer-option-element"><i class="fa fa-fw fa-remove"></i></button>
		</div>
	</div>
</script>
<!--end template for likert scale -->