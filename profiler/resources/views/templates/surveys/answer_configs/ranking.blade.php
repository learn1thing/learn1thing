<!-- template for ranking -->
<script id="template-ranking" type="text/template">
	<!-- <div class="col-sm-12"> -->
	<div class="form-group clearfix">
		<label class="col-sm-2">Minimal (<span class="text-red">*</span>) :</label>
		<div class="col-sm-5">
			<input type="number" class="form-control answer-configs-minimal input-number" placeholder="Minimal value of ranking" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_configs][minimal]" required>
		</div>
		<div class="help-block with-errors"></div>
	</div>
	<div class="form-group clearfix">
		<label class="col-sm-2">Interval (<span class="text-red">*</span>) :</label>
		<div class="col-sm-5">
			<input type="number" class="form-control answer-configs-interval input-number" placeholder="Interval value of ranking" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_configs][interval]" required>
		</div>
		<div class="help-block with-errors"></div>
	</div>
	<div class="form-group clearfix">
		<label class="col-sm-2">Maximal (<span class="text-red">*</span>) :</label>
		<div class="col-sm-5">
			<input type="number" class="form-control answer-configs-maximal input-number" placeholder="Maximal value of ranking" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_configs][maximal]" required>
		</div>
		<div class="help-block with-errors"></div>
	</div>
	<div class="form-group clearfix">
		<label class="col-sm-2">Attributes: </label>
		<div class="col-sm-5">
			<select class="form-control select2 question-attributes" multiple="multiple" data-placeholder="Select some attributes" style="width: 100%;" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_attributes][]">
			</select>
		</div>
	</div>
	<div class="form-group clearfix">
		<label class="col-sm-2">Score: </label>
		<div class="col-sm-5">
			<input type="number" placeholder="Enter score" class="form-control input-number" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_scores]">
		</div>
	</div>
	<!-- </div> -->
</script>
<!--end template for ranking -->