<!-- template for time -->
<script id="template-time" type="text/template">
	<div class="col-sm-12">
		<div class="row">
			<div class="form-group clearfix">
				<label class="col-sm-2">Attributes: </label>
				<div class="col-sm-5">
					<select class="form-control select2 question-attributes" multiple="multiple" data-placeholder="Select some attributes" style="width: 100%;" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_attributes][]">
					</select>
				</div>
			</div>
			<div class="form-group clearfix">
				<label class="col-sm-2">Score: </label>
				<div class="col-sm-5">
					<input type="number" placeholder="Enter score" class="form-control input-number" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_scores]">
				</div>
			</div>
		</div>
	</div>
</script>
<!-- template for end time -->