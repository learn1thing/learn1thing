<!-- template for distributed points -->
<script id="template-distributed_points" type="text/template">
	<div class="col-sm-12">
		<div class="row">
			<div class="form-group clearfix">
				<label class="col-sm-2">Point on hand (<span class="text-red">*</span>) :</label>
				<div class="col-sm-5">
					<input type="number" class="form-control point-on-hand input-number" placeholder="Point on hand" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_configs][point]" required>
				</div>
				<div class="help-block with-errors"></div>
			</div>
		</div>

		{{-- <label>Answer options (<span class="text-red">*</span>) :</label> --}}
		<div class="row answer-elements">
			<!-- render template here -->
		</div>
		<div class="row">
			<div class="row col-sm-12">
				<div class="form-group form-group col-md-1 col-md-offset-11">
					<button class="btn btn-default add-answer-option-element" data-template-element-name="template-distributed_points-answer-element"><i class="fa fa-fw fa-plus-square"></i></button>
				</div>
			</div>
		</div>
	</div>
</script>
<script id="template-distributed_points-answer-element" type="text/template">
	{{-- <div class="form-group clearfix">
		<div class="col-sm-4">
			<input type="text" placeholder="Label option" class="form-control" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_configs][options][]">
		</div>
		<div class="col-sm-4">
			<select class="form-control select2 question-attributes" multiple="multiple" data-placeholder="Select some attributes" style="width: 100%;" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_attributes][<%= answer_index %>][]">
			</select>
		</div>
		<div class="col-sm-3">
			<input type="number" placeholder="Enter score" class="form-control" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_scores][]">
		</div>
		<div class="col-sm-1">
			<button class="btn btn-default pull-right delete-answer-option-element"><i class="fa fa-fw fa-remove"></i></button>
		</div>
	</div> --}}
	<div class="row">
		<div class="col-sm-4">
			<label>Option (<span class="text-red">*</span>) :</label>
			<input type="text" placeholder="Enter the option text" class="form-control answer-configs" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_configs][options][]" required>
			<div class="help-block with-errors"></div>
		</div>
		<div class="col-sm-4">
			<label>Attributes :</label>
			<select class="form-control select2 question-attributes" multiple="multiple" data-placeholder="Select some attributes" style="width: 100%;" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_attributes][<%= answer_index %>][]">
			</select>
		</div>
		<div class="col-sm-3">
			<label>Score :</label>
			<input type="number" placeholder="Enter score" class="form-control input-number" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_scores][]">
		</div>
		<div class="col-sm-1">
			<label>Action</label>
			<button class="btn btn-default delete-answer-option-element"><i class="fa fa-fw fa-remove"></i></button>
		</div>
	</div>
</script>
<!--end template for distributed points -->