<!-- template for matching -->
<script id="template-matching" type="text/template">
	<div class="col-sm-12">
		{{-- <label>Answer options (<span class="text-red">*</span>) :</label> --}}
		<div class="row answer-elements">
			<!-- render template here -->
		</div>
		<div class="row">
			<div class="row col-sm-12">
				<div class="form-group col-md-1 col-md-offset-11">
					<button class="btn btn-default add-answer-option-element" data-template-element-name="template-matching-answer-element"><i class="fa fa-fw fa-plus-square"></i></button>
				</div>
			</div>
		</div>
	</div>
</script>
<script id="template-matching-answer-element" type="text/template">
	<div class="row">
		<div class="col-sm-3">
			<label>Title option (<span class="text-red">*</span>) :</label>
			<input type="text" class="form-control answer-configs-title-option" id="inputEmail3" placeholder="Title option" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_configs][<%= answer_index %>][label]" required>
			<div class="help-block with-errors"></div>
		</div>
		<div class="col-sm-3">
			<label>Title value (<span class="text-red">*</span>) :</label>
			<input type="text" class="form-control answer-configs-title-value" id="inputEmail3" placeholder="Title value" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_configs][<%= answer_index %>][value]" required>
			<div class="help-block with-errors"></div>
		</div>
		<div class="col-sm-3">
			<label>Attributes :</label>
			<select class="form-control select2 question-attributes" multiple="multiple" data-placeholder="Select some attributes" style="width: 100%;" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_attributes][<%= answer_index %>][]">
			</select>
		</div>
		<div class="col-sm-2">
			<label>Score :</label>
			<input type="number" placeholder="Enter score" class="form-control input-number" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_scores][]">
		</div>
		<div class="col-sm-1">
			<label>Action</label>
			<button class="btn btn-default delete-answer-option-element"><i class="fa fa-fw fa-remove"></i></button>
		</div>
	</div>
</script>
<!--end template for matching -->