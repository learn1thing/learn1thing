<!-- template for multiple choice -->
<script id="template-multichoice" type="text/template">
	<div class="col-sm-12">
		<div class="row">
			<div class="form-group clearfix">
				<label class="col-sm-2">Coordinate view (<span class="text-red">*</span>) :</label>
				<div class="col-sm-5">
					<select name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][coordinate]" class="form-control select-coordinate-view" required>
						<option value=""></option>
						<option value="horizontal">Horizontal</option>
						<option value="vertical">Vertical</option>
					</select>
				</div>
				<div class="help-block with-errors"></div>
			</div>
		</div>
		{{-- <label>Answer options (<span class="text-red">*</span>) :</label> --}}
		<div class="row answer-elements">
			<!-- render template here -->
		</div>
		<div class="row">
			<div class="row col-sm-12">
				<div class="form-group form-group col-md-1 col-md-offset-11">
					<button class="btn btn-default add-answer-option-element" data-template-element-name="template-multichoice-answer-element">
						<i class="fa fa-fw fa-plus-square"></i>
					</button>
				</div>
			</div>
		</div>
	</div>
</script>
<script id="template-multichoice-answer-element" type="text/template">
	{{-- <div class="form-group clearfix">
		<div class="form-group col-sm-4">
			<label>Option (<span class="text-red">*</span>) :</label>
			<div class="input-group">
				<span class="input-group-addon">
					<input type="radio" disabled="disabled">
				</span>
				<input type="text" placeholder="Enter the option text" class="form-control answer-configs" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_configs][]" required>
			</div>
		</div>
		<div class="form-group col-sm-4">
			<label>Attributes :</label>
			<select class="form-control select2 question-attributes" multiple="multiple" data-placeholder="Select some attributes" style="width: 100%;" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_attributes][<%= answer_index %>][]">
			</select>
		</div>
		<div class="form-group col-sm-3">
			<label>Score :</label>
			<input type="number" placeholder="Enter score" class="form-control" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_scores][]">
		</div>
		<div class="form-group col-sm-1">
			<label>Action</label>
			<button class="btn btn-default pull-right delete-answer-option-element"><i class="fa fa-fw fa-remove"></i></button>
		</div>
	</div> --}}
	<div class="row">
		<div class="col-sm-4">
			<label>Option (<span class="text-red">*</span>) :</label>
			<div class="input-group">
				<span class="input-group-addon">
					<input type="radio" disabled="disabled">
				</span>
				<input type="text" placeholder="Enter the option text" class="form-control answer-configs" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_configs][]" required>
			</div>
			<div class="help-block with-errors"></div>
		</div>
		<div class="col-sm-4">
			<label>Attributes :</label>
			<select class="form-control select2 question-attributes" multiple="multiple" data-placeholder="Select some attributes" style="width: 100%;" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_attributes][<%= answer_index %>][]">
			</select>
		</div>
		<div class="col-sm-3">
			<label>Score :</label>
			<input type="number" placeholder="Enter score" class="form-control input-number" name="<%= survey_type %>[<%= section_index %>][question][<%= question_index %>][answer_scores][]">
		</div>
		<div class="col-sm-1">
			<label>Action</label>
			<button class="btn btn-default delete-answer-option-element"><i class="fa fa-fw fa-remove"></i></button>
		</div>
	</div>
</script>
<!-- end template for multiple choice -->