// <script type="text/javascript">
var ckeParams = {
    extraPlugins: 'embed,justify',
    removeButtons: 'Image,Flash,Table,SpecialChar,HorizontalRule,Embed,Anchor,Source,Format,Styles,NumberedList,BulletedList,Indent,Outdent,About,Maximize,Scayt',
    on: {
        instanceReady: function( evt ) {
        $(".cke_toolbar_break").remove();
        }
    }
};

var closeButton = '<button type="button" class="close removeElement">&times;</button>';
var surveyData = '';

CKEDITOR.replace("header_text", ckeParams);
CKEDITOR.replace("footer_text", ckeParams);

//added by rdw 151217
CKEDITOR.replace("report_desc", ckeParams);

// layout
$(".element-one-column").attr('data-html', '<div class="row row-old" data-max="0" data-layout="One Column" data-edit="false"><div id="wrapper-element-one-column" class="col-md-12 element-wrapper"></div>' + closeButton + '<div class="clearfix"></div></div>');

$(".element-8-4-column").attr('data-html', '<div class="row row-old" data-max="2" data-layout="8-4 Column" data-edit="false"><div id="wrapper-element-8-4-column" class="col-md-8 element-wrapper"></div><div id="wrapper-element-8-4-column-2" class="col-md-4 element-wrapper"></div>' + closeButton + '<div class="clearfix"></div></div>');

$(".element-4-8-column").attr('data-html', '<div class="row row-old" data-max="2" data-layout="4-8 Column" data-edit="false"><div id="wrapper-element-4-8-column" class="col-md-4 element-wrapper"></div><div id="wrapper-element-4-8-column-2" class="col-md-8 element-wrapper"></div>' + closeButton + '<div class="clearfix"></div></div>');

$(".element-6x2-column").attr('data-html', '<div class="row row-old" data-max="2" data-layout="6x2 Column" data-edit="false"><div id="wrapper-element-6x2-column" class="col-md-6 element-wrapper"></div><div id="wrapper-element-6x2-column-2" class="col-md-6 element-wrapper"></div>' + closeButton + '<div class="clearfix"></div></div>');

$(".element-4x3-column").attr('data-html', '<div class="row row-old" data-max="3" data-layout="4x3 Column" data-edit="false"><div id="wrapper-element-4x3-column" class="col-md-4 element-wrapper"></div><div id="wrapper-element-4x3-column-2" class="col-md-4 element-wrapper"></div><div id="wrapper-element-4x3-column-3" class="col-md-4 element-wrapper"></div>' + closeButton + '<div class="clearfix"></div></div>');

$(".element-3x4-column").attr('data-html', '<div class="row row-old" data-max="4" data-layout="3x4 Column" data-edit="false"><div id="wrapper-element-3x4-column" class="col-md-3 element-wrapper"></div><div id="wrapper-element-3x4-column-2" class="col-md-3 element-wrapper"></div><div id="wrapper-element-3x4-column-3" class="col-md-3 element-wrapper"></div><div id="wrapper-element-3x4-column-4" class="col-md-3 element-wrapper"></div>' + closeButton + '<div class="clearfix"></div></div>');
// </script>

// <script type="text/javascript">
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].onclick = function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  }
}

$("select[name=cover_style]").imagepicker({
    hide_select: true,
    // show_label: true
})
// </script>
// <script type="text/javascript">
$('#form-component').validator();

function autoChange(elm) {
    $(elm).parent('div').next('div').find('.element-component-title-text').html($(elm).val());
    $(elm).parent('div').next('div').find('.element-component-subtitle-text').html($(elm).val());
    $(elm).parent('div').next('div').find('.element-component-text-text').html($(elm).val());
}

function generateAnswer() {
    target = $('.element-content');
    ids = $('select[name=choose_question]').val().split('-');

    html = "answer-" + surveyData.sections[ids[0]].questions[ids[1]].id;
    $(".select-for-answer").each(function(){
        html += "-" + $(this).val();
    });
    $(".replace-answer").html("<code>" + html + "</code>");
}

function resetSelector() {
    $('.element-content').removeClass('element-component-image-edit');
    $('.element-content').removeClass('element-component-title-text-edit');
    $('.element-content').removeClass('element-component-subtitle-text-edit');
    $('.element-content').removeClass('element-component-question-edit');
    $('.element-content').removeClass('element-component-answer-edit');
    $('.element-content').removeClass('element-component-attribute-edit');
    $('.element-content').removeClass('element-component-percentage-edit');
    $('.element-content').removeClass('element-component-text-text-edit');
    $('.element-content').removeClass('element-component-break-edit');
    $('.element-content').removeClass('element-component-table-edit');
    $('.element-content').removeClass('element-component-tquestion-edit');
    $('.element-content').removeClass('element-component-page-edit');
    $('.element-content').removeClass('element-component-communication-edit');
    $('.element-content').removeClass('element-component-johari-edit');
    $('.element-content').removeClass('element-formula-edit');
    $('.formula-wrapper').remove();
}

function editElement(className) {
    elementEdit = $(className);
    elementEdit.html($("input[name=append_text]").val());
    elementEdit.editable();
}

function findAttribute(id) {
    for (var i = surveyData.attributes.length - 1; i >= 0; i--) {
        if (id == surveyData.attributes[i].id) {
            return surveyData.attributes[i].name;
        }
    };
}

function defineAttribute(elm, selector) {
    target = $('.element-content');
    ids = $(elm).val().split('-');

    newQuestion = surveyData.sections[ids[0]].questions[ids[1]];
    newAttr = newQuestion.answer_attributes;

    if (selector == 'config') {
        newAttr = newQuestion.answer_attributes[ids[2]];
        attrHtml = newQuestion.answer_configs[ids[2]] + ': ';

    } else {
        attrHtml = 'Attributes: ';

    }

    for (var i = newAttr.length - 1; i >= 0; i--) {
        attrHtml += findAttribute(newAttr[i]);

        if (i > 0) {
            attrHtml += ', ';
        }
    };

    $(".replace-attribute").html("<p>" + attrHtml + "</p>");
}

function appendElement(html) {
    $('.element-content').html(html);
    $('#form-component').validator('update');

    $(".remove-element").remove();
    $("#right-copy-1tomany").children('div').each(function(index){
        $("select[name=choose_layout]").append('<option class="remove-element" value="'+index+'" data-max="'+$(this).data('max')+'">'+(index+1)+' - '+$(this).data('layout')+'</option>');
    })
    $("#myModal").modal();
}

dragula([document.getElementById('left-copy-1tomany'), document.getElementById('right-copy-1tomany')], {
    copy: function (el, source) {
        return source === document.getElementById('left-copy-1tomany');
    },
    accepts: function (el, target) {
        return target !== document.getElementById('left-copy-1tomany');
    }
})
.on('drop', function (el) {
    nextEl = $(el);
    if (nextEl.data('html') !== undefined) {
        nextEl.replaceWith(nextEl.data('html'));

    } else {
        if (nextEl.data('edit') === undefined) {
            nextEl.replaceWith('');
        }
    }
});

$(document).on('click', '.draggable-element > div > a', function(){
    parent = $(this).parent('div');
    html = '';

    if (parent.hasClass('element-component-image')) {
        html = '<div data-edit="false" class="text-center"><img src="https://bevacqua.github.io/dragula/resources/icon.svg" class="change_image" style="max-width: 100%;"/>'+closeButton+'<div class="clearfix"></div></div>';
        $('.element-content').addClass('element-component-image-edit');

    } else if (parent.hasClass('element-component-title')) {
        html = '<h1 class="text-center" data-edit="false"><a href="#" class="element-component-title-text" data-type="text" data-mode="inline">Title</a>'+closeButton+'</h1><div class="clearfix"></div>';
        $('.element-content').addClass('element-component-title-text-edit');

    } else if (parent.hasClass('element-component-subtitle')) {
        html = '<h3 class="text-center" data-edit="false"><a href="#" class="element-component-subtitle-text" data-type="text" data-mode="inline">Subtitle</a>'+closeButton+'</h3><div class="clearfix"></div>';
        $('.element-content').addClass('element-component-subtitle-text-edit');

    } else if (parent.hasClass('element-component-text')) {
        html = '<div data-edit="false"><a href="#" class="element-component-text-text" data-type="wysihtml5" data-mode="inline">Text</a>'+closeButton+'<div class="clearfix"></div></div>';
        $('.element-content').addClass('element-component-text-text-edit');

    } else if (parent.hasClass('element-component-break')) {
        html = '<div data-edit="false"><br/>'+closeButton+'<div class="clearfix"></div></div>';
        $('.element-content').addClass('element-component-break-edit');

    } else if (parent.hasClass('element-component-page')) {
        html = '<div data-edit="false"><code>[new-page]</code>'+closeButton+'<div class="clearfix"></div></div>';
        $('.element-content').addClass('element-component-page-edit');

    } else if (parent.hasClass('element-component-table')) {
        html = '<div data-edit="false"><div class="replace-table"><code>[response-table]</code>'+closeButton+'<div class="clearfix"></div></div>';
        $('.element-content').addClass('element-component-table-edit');

    } else if (parent.hasClass('element-component-tquestion')) {
        html = '<div data-edit="false"><div class="replace-tquestion"><code>[response-tquestion]</code>'+closeButton+'<div class="clearfix"></div></div>';
        $('.element-content').addClass('element-component-tquestion-edit');

    } else if (parent.hasClass('element-component-question')) {
        html = '<div data-edit="false"><div class="replace-question"><p class="text-center">question</p></div>'+closeButton+'<div class="clearfix"></div></div>';
        $('.element-content').addClass('element-component-question-edit');

    } else if (parent.hasClass('element-component-answer')) {
        html = '<div data-edit="false"><div class="replace-answer"><p class="text-center">answer</p></div>'+closeButton+'<div class="clearfix"></div></div>';
        $('.element-content').addClass('element-component-answer-edit');

    } else if (parent.hasClass('element-component-attribute')) {
        html = '<div data-edit="false"><div class="replace-attribute"><p class="text-center">attribute</p></div>'+closeButton+'<div class="clearfix"></div></div>';
        $('.element-content').addClass('element-component-attribute-edit');

    } else if (parent.hasClass('element-component-percentage')) {
        html = '<div data-edit="false"><div class="replace-percentage"><p class="text-center">percentage</p></div>'+closeButton+'<div class="clearfix"></div></div>';
        $('.element-content').addClass('element-component-percentage-edit');

    } else if (parent.hasClass('element-component-johari')) {
        html = '<div data-edit="false"><div class="replace-johari"><p class="text-center">Johari</p></div>'+closeButton+'<div class="clearfix"></div></div>';
        $('.element-content').addClass('element-component-johari-edit');

    } else if (parent.hasClass('element-formula')) {
        html = '<div data-edit="false"><div class="replace-formula"><p class="text-center">formula</p></div>'+closeButton+'<div class="clearfix"></div></div>';
        $('.element-content').addClass('element-component-formula-edit');

    } else if (parent.hasClass('element-chart')) {
        html = $('#element-chart-template').html();
        $('.element-content').addClass('element-chart-edit');

    } else if (parent.hasClass('element-component-communication')) {
        html = '<div data-edit="false"><code>[communication-style]</code>'+closeButton+'<div class="clearfix"></div></div>';
        $('.element-content').addClass('element-component-communication-edit');

    }

    if (typeof onDragElement !== 'undefined') {
        html = onDragElement(parent, html);
    }

    appendElement(html);
})

$(document).on('change', 'select.select-for-answer[name=data_aggregate], select.select-for-answer[name=aggregate_by], select.select-for-answer[name=data_type]', function(){
    generateAnswer();
})

$(document).on('change', 'select[name=answer_config]', function(){
    defineAttribute(this, 'config');
})

$(document).on('change', 'select[name=choose_layout]', function(){
    $("div.remove-element").remove();
    target = $('.element-content');
    max = $('select[name=choose_layout] option:selected').data('max');

    if (max > 1) {
        html = '<div class="form-group remove-element"><label for="choose_layout">Choose Column Number</label><select class="form-control select2" name="choose_next_layout"><option value="">Choose Column Number</option></select></div>';
        $(html).insertAfter($("select[name=choose_layout]").parent('div'));
        for (var i = 0; i < max; i++) {
            $("select[name=choose_next_layout]").append('<option class="remove-element" value="'+i+'">'+(i+1)+'</option>');
        }
    }

    if (target.hasClass('element-component-subtitle-text-edit') || target.hasClass('element-component-title-text-edit') || target.hasClass('element-component-text-text-edit')) {
        html = '<div class="form-group remove-element"><label for="choose_layout">Insert Text</label><input type="text" class="form-control" name="append_text"></div>';
        $(html).insertAfter($("select[name=choose_layout]").parent('div'));

    } else if(target.hasClass('element-component-image-edit')) {
        html = '<div class="form-group remove-element"><label for="choose_layout">Choose Image</label><input type="file" class="form-control" id="imgInp"></div>';
        $(html).insertAfter($("select[name=choose_layout]").parent('div'));

    } else if(target.hasClass('element-component-question-edit') || target.hasClass('element-component-answer-edit') || target.hasClass('element-component-attribute-edit') || target.hasClass('element-component-percentage-edit') || target.hasClass('element-component-johari-edit')) {
    // } else if(target.hasClass('element-component-question-edit') || target.hasClass('element-component-answer-edit') || target.hasClass('element-component-attribute-edit') || target.hasClass('element-component-percentage-edit') || target.hasClass('element-component-johari-edit') || target.hasClass('element-component-table-edit')) {
        options = '';

        if (surveyData != '') {
            for (var i = surveyData.sections.length - 1; i >= 0; i--) {
                options += '<option value="'+i+'">'+surveyData.sections[i].title+'</option>';
            }
        }

        html = '<div class="form-group remove-element"><label for="choose_section">Choose Section</label><select class="form-control select2" name="choose_section"><option value="-1">Choose Section</option>'+options+'</select></div>';

        $(html).insertAfter($("select[name=choose_layout]").parent('div'));

    } else if(target.hasClass('element-component-formula-edit')) {
        options = '';

        if (surveyData != '') {
            for (var i = surveyData.formulas.length - 1; i >= 0; i--) {
                options += '<option value="'+surveyData.formulas[i].id+'">'+surveyData.formulas[i].name+'</option>';
            }
        }

        html = '<div class="form-group remove-element"><label for="choose_formula">Choose formula</label><select id="choose_formula" class="form-control select2" name="choose_formula" required><option value="">Choose formula</option>'+options+'</select></div>';

        $(html).insertAfter($("select[name=choose_layout]").parent('div'));
        $('#form-component').validator('update');
    }

    if (typeof onSelectLayout !== "undefined") {
        onSelectLayout(target);
    }
})

$(document).on('change', 'select[name=choose_section]', function(){
    options = '';
    id = $(this).val();

    $("select[name=choose_question]").parent('.remove-element').remove();
    $(".select-for-answer, .select-for-attribute").parent('.remove-element').remove();

    for (var i = surveyData.sections[id].questions.length - 1; i >= 0; i--) {
        options += '<option value="'+id+'-'+i+'">'+surveyData.sections[id].questions[i].name+'</option>';
    }

    html = '<div class="form-group remove-element"><label for="choose_question">Choose Question</label><select class="form-control select2" name="choose_question"><option value="-1">Choose Question</option>'+options+'</select></div>';

    $(html).insertAfter($("select[name=choose_section]").parent('div'));
})

$(document).on('change', 'select[name=choose_question]', function(){
    target = $('.element-content');
    ids = $(this).val().split('-');

    $(".select-for-answer, .select-for-attribute").parent('.remove-element').remove();

    if (target.hasClass('element-component-question-edit')) {
        $('.replace-question').html(surveyData.sections[ids[0]].questions[ids[1]].content);

    } else if (target.hasClass('element-component-answer-edit')) {
        html = '<div class="form-group remove-element"><label for="data_type">Data Type</label><select class="form-control select2 select-for-answer" name="data_type"><option value="primary_user">Primary User</option><option value="mixed">Mixed</option><option value="observer">Observer</option></select></div>';
        html += '<div class="form-group remove-element"><label for="aggregate_by">Aggregate By</label><select class="form-control select2 select-for-answer" name="aggregate_by"><option value="value">Value</option><option value="observer_category">Observer Category</option><option value="answer_attributes">Answer Attributes</option></select></div>';
        html += '<div class="form-group remove-element"><label for="data_aggregate">Data Aggregate</label><select class="form-control select2 select-for-answer" name="data_aggregate"><option value="average">Average</option><option value="sum">Sum</option></select></div>';

        $(html).insertAfter($("select[name=choose_question]").parent('div'));
        generateAnswer();

    } else if (target.hasClass('element-component-attribute-edit')) {
        question = surveyData.sections[ids[0]].questions[ids[1]];
        options = '';

        if ( $.inArray( question.answer_type, [ 'checkboxes', 'multiple_choice_with_constraint', 'multichoice', 'boolean', 'likert_scale', 'distributed_points', 'matching' ] ) >= 0 ) {

            for (var i = question.answer_configs.length - 1; i >= 0; i--) {
                options += '<option value="'+ids[0]+'-'+ids[1]+'-'+i+'">'+question.answer_configs[i]+'</option>';
            }

            html = '<div class="form-group remove-element"><label for="answer_config">Answer</label><select class="form-control select2 select-for-attribute" name="answer_config"><option value="-1">Choose Attribute</option>'+options+'</select></div>';
        } else {
            html = '';
            defineAttribute(this, '');
        }

        $(html).insertAfter($("select[name=choose_question]").parent('div'));

    } else if (target.hasClass('element-component-percentage-edit')) {
        html = "percentage-" + surveyData.sections[ids[0]].questions[ids[1]].id;
        $(".replace-percentage").html("<code>" + html + "</code>");

    } else if (target.hasClass('element-component-johari-edit')) {
        html = "johari-" + surveyData.sections[ids[0]].questions[ids[1]].id;
        $(".replace-johari").html("<code>" + html + "</code>");

    // } else if (target.hasClass('element-component-table-edit')) {
    //     html = "table-" + surveyData.sections[ids[0]].questions[ids[1]].id;
    //     $(".replace-table").html("<code>" + html + "</code>");

    }
})

$('#form-component').delegate('select[name=choose_formula]', 'change', function () {
    var formulaValue = $(this).val();
    if(formulaValue) {
        $(this).parents('.modal-body').find(".replace-formula").html("<code>formula-" + formulaValue + "</code>");
    } else {
        $(this).parents('.modal-body').find(".replace-formula").html("<code>formula</code>");
    }
})

$(document).on('change', '#imgInp', function(){
    if (this.files[0].size > 307200) {
       alert("File is too big! Maximum 300Kb");
       this.value = "";

    } else {
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(".change_image").attr('src', e.target.result);
            }

            reader.readAsDataURL(this.files[0]);
        }
    }
});

$(document).on('change', '#fake_cover_background', function(){
    if (this.files[0].size > 30720000) {
       alert("File is too big! Maximum 300Kb");
       this.value = "";

    } else {
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $("input[name=cover_background]").val(e.target.result);
                //added by rdw k5 151217
                $("#img_cover_background").attr("src",e.target.result);
            }

            reader.readAsDataURL(this.files[0]);
        }
    }
});

//start added by rdw k5 151217
$(document).on('click', '#delete_cover_background', function(){
    $("input[name=cover_background]").val('');
    $("#fake_cover_background").val('');
    $("#img_cover_background").attr("src",'http://store.learn1thing.com/profiler/img/noImage.jpg');
});
$(document).on('change', '#fake_logo_url', function(){
    if (this.files[0].size > 307200) {
       alert("File is too big! Maximum 300Kb");
       this.value = "";

    } else {
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $("input[name=logo_url]").val(e.target.result);
                $("#img_logo_url").attr("src",e.target.result);
            }

            reader.readAsDataURL(this.files[0]);
        }
    }
});
$(document).on('click', '#delete_logo_url', function(){
    $("input[name=logo_url]").val('');
    $("#fake_logo_url").val('');
    $("#img_logo_url").attr("src",'http://store.learn1thing.com/profiler/img/noImage.jpg');
});
//end added by rdw k5 151217


// // formula events
// $('#form-component').delegate('select.formula-input.source', 'change', function () {
//     var parent = $(this).parents('.formula-wrapper');
//     var sourceValue = $(this).val();
//     if(sourceValue == "raw_data") {
//         parent.find('select.formula-input.question')
//             .removeAttr('disabled')
//             .attr('required', 'required');
//         parent.find('select.formula-input.question')
//             .parents('.form-group')
//             .removeClass('hidden');

//         parent.find('select.formula-input.formula')
//             .removeAttr('required')
//             .attr('disabled');
//         parent.find('select.formula-input.formula')
//             .parents('.form-group')
//             .addClass('hidden');

//         var questions = surveyData.questions;
//         var data = [];
//         if(questions) {
//             questions.forEach(function (val, key) {
//                 data.push({id: val.id, text: val.name});
//             });
//         }
//         parent.find("select.formula-input.question").select2({
//           data: data,
//           placeholder: "Choose the question"
//         });

//     } else {
//         parent.find('select.formula-input.formula')
//             .removeAttr('disabled')
//             .attr('required', 'required');
//         parent.find('select.formula-input.formula')
//             .parents('.form-group')
//             .removeClass('hidden');

//         parent.find('select.formula-input.question')
//             .removeAttr('required')
//             .attr('disabled');
//         parent.find('select.formula-input.question')
//             .parents('.form-group')
//             .addClass('hidden');

//         parent.find('.wrapper-answer-option')
//             .parents('.form-group')
//             .addClass('hidden');
//     }
//     $('#form-component').validator('update');
// });

// $('#form-component').delegate('select.formula-input.question', 'change', function () {
//     var parent = $(this).parents('.formula-wrapper');
//     if(surveyData.questions) {
//         var questionId = $(this).val();
//         var question = _.find(surveyData.questions, function (val, key) {
//             return val.id == questionId;
//         });
//         var answerTemplate = _.template($('#answer-'+question.answer_type).html());
//         parent.find('.wrapper-answer-option').html(answerTemplate({answer_configs: question.answer_configs, formula_index: parent.data('number')}));
//         if(question.answer_type == "ranking") {
//             var answerConfigs = question.answer_configs;
//             parent.find(".rating").rateYo({
//                 fullStar: true,
//                 numStars: (answerConfigs.maximal - answerConfigs.minimal) / answerConfigs.interval
//             })
//             .on("rateyo.set", function (e, data) {
//                 parent.find('input[type=hidden].answer').val(data.rating);
//             });
//         } else if (question.answer_type == "multiple_choice_with_constraint") {
//             parent.delegate('.with-constraint', 'change', function() {
//                 var maxSelected = parent.find('.wrapper_with_constraint').data('points');
//                 var countAll = parent.find(".with-constraint:checked").length;
//                 if (countAll == maxSelected) {
//                     parent.find('input.with-constraint:not(":checked")').attr('disabled', true);
//                 } else {
//                     parent.find('input.with-constraint:not(":checked")').attr('disabled', false);
//                 }
//             });
//         } else if (question.answer_type == "date") {
//             parent.find('input.date').datepicker({
//               format: "yyyy-mm-dd",
//                 autoclose: true,
//                 todayHighlight: true
//             });
//         } else if (question.answer_type == "time") {
//             parent.find('input.time').timepicker({
//                 showInputs: false
//             });
//         } else if (question.answer_type == "datetime") {
//             parent.find('input.date').datepicker({
//               format: "yyyy-mm-dd",
//                 autoclose: true,
//                 todayHighlight: true
//             });
//             parent.find('input.time').timepicker({
//                 showInputs: false
//             });
//         }
//     }
//     parent.find('.wrapper-answer-option')
//         .parents('.form-group')
//         .removeClass('hidden');
//     $('#form-component').validator('update');
// });


// $('#form-component').delegate('select.formula-input.profiler_role', 'change', function () {
//     var formulaWrapper = $(this).parents('.formula-wrapper');
//     var roleValue = $(this).val();
//     if(roleValue == "observer") {
//         formulaWrapper.find('select.formula-input.observer_category')
//             .removeAttr('disabled')
//             .attr('required', 'required');
//         formulaWrapper.find('select.formula-input.observer_category')
//             .parents('.form-group')
//             .removeClass('hidden');

//         var data = surveyData.observer_categories;
//         formulaWrapper.find("select.formula-input.observer_category").select2({
//           data: data,
//           placeholder: "Choose the observer category"
//         });
//         formulaWrapper.find('#user-properties')
//             .addClass('hidden');
//         formulaWrapper.find('#user-properties .user-category')
//             .attr('disabled', 'disabled');
//     } else if(roleValue == "user") {
//         formulaWrapper.find('#user-properties')
//             .removeClass('hidden');
//         formulaWrapper.find('#user-properties .user-category')
//             .removeAttr('disabled');
//         formulaWrapper.find('select.formula-input.observer_category')
//             .removeAttr('required')
//             .attr('disabled');
//         formulaWrapper.find('select.formula-input.observer_category')
//             .parents('.form-group')
//             .addClass('hidden');
//     } else {
//         formulaWrapper.find('select.formula-input.observer_category')
//             .removeAttr('required')
//             .attr('disabled');
//         formulaWrapper.find('select.formula-input.observer_category')
//             .parents('.form-group')
//             .addClass('hidden');
//         formulaWrapper.find('#user-properties')
//             .addClass('hidden');
//         formulaWrapper.find('#user-properties .user-category')
//             .attr('disabled', 'disabled');
//     }
//     $('#form-component').validator('update');
// });

// $('#form-component').delegate('select.formula-input.user-category', 'change', function () {
//     var formulaWrapper = $(this).parents('.formula-wrapper');
//     var userCategoryValue = $(this).val();
//     if(userCategoryValue == "department" || userCategoryValue == "individual") {
//         formulaWrapper.find('select.formula-input.group-by')
//             .removeAttr('disabled')
//             .attr('required', 'required');
//         formulaWrapper.find('select.formula-input.group-by')
//             .parent()
//             .removeClass('hidden');
//         var data = userCategoryValue == "department" ? departments : roles;
//         formulaWrapper.find('select.formula-input.group-by').empty().trigger("change");
//         formulaWrapper.find('select.formula-input.group-by').select2({
//             data: data,
//             placeholder: 'Choose the group by',
//             allowClear: true
//         });
//     } else {
//         formulaWrapper.find('select.formula-input.group-by')
//             .attr('disabled', 'disabled')
//             .removeAttr('required');
//         formulaWrapper.find('select.formula-input.group-by')
//             .parent()
//             .addClass('hidden');
//     }
//     $('#form-component').validator('update');
// });

// $('#form-component').delegate('select.formula-input.function', 'change', function () {
//     var formulaWrapper = $(this).parents('.formula-wrapper');
//     var functionValue = $(this).val();
//     if(functionValue == "raw_value") {
//         formulaWrapper.find('select.formula-input.operand')
//             .attr('disabled', 'disabled');
//     } else {
//         formulaWrapper.find('select.formula-input.operand')
//             .removeAttr('disabled');
//     }
//     $('#form-component').validator('update');
// });

// $('#form-component').delegate('select.formula-input.operand', 'change', function () {
//     var formulaWrapper = $(this).parents('.formula-wrapper');
//     var parent = formulaWrapper.parent();
//     var value = $(this).val();
//     if (value) {
//         var indexFormula = formulaWrapper.data('number');
//         var isAddNewFormula = parent.find('.formula-wrapper').eq(indexFormula+1);
//         if(isAddNewFormula.length == 0) {
//             var form = _.template($('#element-formula-template').html());
//             parent.append(form({number: indexFormula+1}));
//         }
//     } else {
//         var wrappers = parent.find('.formula-wrapper');
//         var indexFormula = formulaWrapper.data('number');
//         for(i = indexFormula+1; i < wrappers.length; i++) {
//             if(wrappers.eq(i).length == 1) {
//                 wrappers.eq(i).remove();
//             }
//         }
//     }
//     $('#form-component').validator('update');
// });

// // $('#form-component').delegate('.formula-input', 'change', function () {
// //     var formulaInputs = $('.formula-input');
// //     var attrs = [];
// //     formulaInputs.each(function (el, key) {
// //         var value = $(this).val();
// //         attrs.push(value);
// //     });
// //     var textCode = attrs.join('-');
// //     html = '<div data-edit="false"><div class="replace-attribute"><code>'+textCode+'</code></div>'+closeButton+'</div>';
// //     $('.element-content').html(html);
// // })

$('#form-component').validator().on('submit', function (e) {
    if (!e.isDefaultPrevented()) {
        // var state = $(this).data('state');
        // if(state == 'formula') {
        //     e.preventDefault();
        //     var data = $(this).serialize()+'&survey_id=' + surveyData.id;
        //     var arrayData = $(this).serializeArray();
        //     $.ajax({
        //         url: $(this).find("#formula-url").val()
        //         , method: 'POST'
        //         , data: data
        //         , success: function(result) {
        //             var data = result.data;
        //             columns = $("#right-copy-1tomany").children('div');
        //             selected = $("select[name=choose_layout]").val();
        //             next = $("select[name=choose_next_layout]").val();
        //             var htmlString = '<div><div class="render_formula">';
        //             data.forEach(function(value, key) {
        //                 htmlString += '<span class="formula" data-id="' + value.id + '">' + value.name + "</span>";
        //                 var operand = $('#form-component').find('select.operand').eq(key);
        //                 if(operand.length > 0 && operand.val() != "") {
        //                     htmlString += ' <span class="operand" data-operator="' + operand.val() + '">' + operand.val() + '</span> ';
        //                 }
        //             });
        //             htmlString += '</div></div>';
        //             elmCont = $(htmlString);

        //             if (next === undefined) {
        //                 next = 0;
        //             }

        //             columns.eq(selected).children('div').eq(next).append(elmCont.html());

        //             if (elmCont.hasClass('element-component-subtitle-text-edit')) {
        //                 editElement('.element-component-subtitle-text');

        //             } else if (elmCont.hasClass('element-component-title-text-edit')) {
        //                 editElement('.element-component-title-text');

        //             };

        //             resetSelector();
        //             elmCont.html('');
        //             $('#myModal').modal('hide');

        //             $('.element-component-title-text').removeClass('element-component-title-text');
        //             $('.element-component-subtitle-text').removeClass('element-component-subtitle-text');
        //             $('.element-component-image-edit').removeClass('element-component-image-edit');
        //             $('.replace-attribute').removeClass('replace-attribute');
        //             $('.replace-answer').removeClass('replace-answer');
        //             $('.replace-percentage').removeClass('replace-percentage');
        //             $('.replace-johari').removeClass('replace-johari');
        //             $('.replace-question').removeClass('replace-question');
        //             $('.change_image').removeClass('change_image');
        //             // $('#myModal').modal('toggle')
        //             console.log('yohoho', result)
        //         }
        //         , error: function(result) {
        //             console.log('error');
        //         }
        //     });
        // } else {
            columns = $("#right-copy-1tomany").children('div');
            selected = $("select[name=choose_layout]").val();
            next = $("select[name=choose_next_layout]").val();
            elmCont = $('.element-content');

            if (next === undefined) {
                next = 0;
            }

            columns.eq(selected).children('div').eq(next).append(elmCont.html());

            if (elmCont.hasClass('element-component-subtitle-text-edit')) {
                editElement('.element-component-subtitle-text');

            } else if (elmCont.hasClass('element-component-title-text-edit')) {
                editElement('.element-component-title-text');

            } else if (elmCont.hasClass('element-component-text-text-edit')) {
                editElement('.element-component-text-text');

            };

            if (typeof onSubmitStarhub !== 'undefined') {
                onSubmitStarhub(elmCont);
            }

            resetSelector();
            elmCont.html('');
            $('#myModal').modal('hide');

            $('.element-component-title-text').removeClass('element-component-title-text');
            $('.element-component-subtitle-text').removeClass('element-component-subtitle-text');
            $('.element-component-text-text').removeClass('element-component-text-text');
            $('.element-component-image-edit').removeClass('element-component-image-edit');
            $('.replace-attribute').removeClass('replace-attribute');
            $('.replace-answer').removeClass('replace-answer');
            $('.replace-percentage').removeClass('replace-percentage');
            $('.replace-johari').removeClass('replace-johari');
            $('.replace-question').removeClass('replace-question');
            $('.change_image').removeClass('change_image');
        // }
    }
    e.preventDefault();
})

$(document).on('click', '.removeElement', function(){
    parent = $(this).parent('[data-edit="false"]');
    grandParent = parent.parent('.element-content');
    if (grandParent.length == 0) {
        parent.prev('.clearfix').remove();
        parent.remove();
    }
})

$(document).on('change', 'select[name=data_source]', function(){
    if (!isEdit) {
        $("#right-copy-1tomany").html("");
    }

    thisVal = $(this).val();

    $.ajax({
        url: $(this).data("href")
        , method: 'POST'
        , data: {
            profiler_id: thisVal
        }
        , beforeSend: function() {
        }
        , success: function(result) {
            surveyData = result;
            console.log('success');

            if (thisVal == "") {
                $(".hide-composer").addClass('hide');
            } else {
                $(".hide-composer").removeClass('hide');
            }
        }
        , error: function(result) {
            console.log('error');
        }
        , complete: function() {
        }
    });
})

$(document).on('click', 'button[data-dismiss="modal"]', function(){
    resetSelector();
})

$('#myModal').on('hidden.bs.modal', function (e) {
  resetSelector();
})

$(document).on('submit', '#form-setting-template', function(){
    $("textarea[name=content]").html($("#right-copy-1tomany").html());
})

$(document).on('click', '.preview-template', function(){
    $("textarea[name=content]").html($("#right-copy-1tomany").html());

    $.ajax({
        url: $(this).data("url")
        , method: 'POST'
        , data: $("#form-setting-template").serialize()
        , beforeSend: function() {
            $('.spinner-wrapper').attr('style', 'z-index: 10000;background: white;');
            $('.spinner').removeClass('hide');
        }
        , success: function(result) {
            $('.spinner-wrapper').attr('style', '');
            $('.spinner').addClass('hide');
            var newWindow = window.open();
	    console.log(result);
            // html = result;
            html = '<iframe style="margin: 0; padding: 0; position: absolute; height: 100%; width: 100%; top: 0; left: 0; border: none;" src="'+result+'" />'
            window.open(result);
            //newWindow.document.write(html);
            //newWindow.document.closeButton();
        }
        , error: function(result) {
            $('.spinner-wrapper').attr('style', '');
            $('.spinner').addClass('hide');
            console.log('error');
            alert("Failed to preview report template.");
        }
        , complete: function() {
        }
    });
})
// </script>
