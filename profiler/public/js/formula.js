$(document).on('change', '.survey_id', function(e) {
	e.preventDefault();
	$(".sections").html("");
	if ($(this).val() != '') {
	    var surveyId = $(this).val();
	    $.ajax({
	        url: $(this).data('url').replace(':survey_id', surveyId),
	        type: "GET",
	        success: function(response) {
	        	$(".sections").html(response);
	            // select.select2('destroy');
	            // select.html('');
	            // $.each(response, function (value, text) {
	            //     select.append($("<option>").attr('value',value).text(text));
	            // });
	            // select.select2();
	        }
	    }).done(function(){
	    });

		var section = $(this).parents('.box.section');
		var detailTemplate = $(this).parents('.detail-temp').html();
		var destroy = '';
		if ($('.detail-temp').length == 1 ) {
			destroy = '<div class="col-md-3 rm"> <a href="#" class="btn btn-danger btn-sm remove-append"> <i class="fa fa-trash"></i> </a></div>';
		}
		section.find('.box-body .details').append('<div class="detail-temp row">'+detailTemplate+destroy+'</div>');
		$(this).parents('.detail-temp').find('select').attr('disabled', true);
		$(this).parents('.detail-temp').find('.rm').remove();
		$('.detail-temp').last().find('select').val('');
	}
});

$(document).on('change', '.operand', function(e) {
	e.preventDefault();
	if ($(this).val() != '') {
		var section = $(this).parents('.box.section');
		var detailTemplate = $(this).parents('.detail-temp').html();
		var destroy = '';
		if ($('.detail-temp').length == 1 ) {
			destroy = '<div class="col-md-3 rm"> <a href="#" class="btn btn-danger btn-sm remove-append"> <i class="fa fa-trash"></i> </a></div>';
		}
		section.find('.box-body .details').append('<div class="detail-temp row">'+detailTemplate+destroy+'</div>');
		$(this).parents('.detail-temp').find('select').attr('disabled', true);
		$(this).parents('.detail-temp').find('.rm').remove();
		$('.detail-temp').last().find('select').val('');
	}
});

$(document).on('click', '.remove-append', function(e) {
	e.preventDefault();
	var section = $(this).parents('.box.section');
	var detailTemplate = $(this).parents('.detail-temp').remove();
	var last = $('.detail-temp').last();
	var destroy = '';
	if ($('.detail-temp').length != 1) {
		destroy = '<div class="col-md-3 rm"> <a href="#" class="btn btn-danger btn-sm remove-append"> <i class="fa fa-trash"></i> </a></div>';
	}

	last.find('select').attr('disabled', false);
	last.last().append(destroy);
});

$('#form-formula').submit(function (e) {
	$(this).find('select').attr('disabled', false);
});