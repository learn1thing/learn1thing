function onSubmitStarhub(elmCont) {
    if (elmCont.hasClass('element-starhub-title-text-edit')) {
        editElement('.element-starhub-title-text');
        $('.element-content').removeClass('element-starhub-title-text-edit');
        $('.element-starhub-title-text').removeClass('element-starhub-title-text');

    } else if (elmCont.hasClass('element-starhub-johari-edit')) {
        $('.element-content').removeClass('element-starhub-johari-edit');
        $('.replace-starhub-johari').removeClass('replace-starhub-johari');

    }
}

function onDragElement(parent, html) {
    if (parent.hasClass('element-starhub-title')) {
        html = '<div data-edit="false" class="exclude-margin"><header><div class="title"><a href="#" class="element-starhub-title-text" data-type="text" data-selector="title" data-mode="inline">Title</a></div><div class="header-logo"></div><hr class="gray" /><hr/></header>'+closeButton+'<div class="clearfix"></div></div>';
        $('.element-content').addClass('element-starhub-title-text-edit');

    } else if (parent.hasClass('element-starhub-johari')) {
        html = '<div data-edit="false"><div class="replace-starhub-johari"><code>starhub-johari</code></div>'+closeButton+'<div class="clearfix"></div></div>';
        $('.element-content').addClass('element-starhub-johari-edit');

    }

    return html;
}

function onSelectLayout(target) {
    if (target.hasClass('element-starhub-title-text-edit')) {
        html = '<div class="form-group remove-element"><label for="choose_layout">Insert Text</label><input type="text" class="form-control" name="append_text"></div>';
        $(html).insertAfter($("select[name=choose_layout]").parent('div'));

    } else if(target.hasClass('element-starhub-johari-edit')) {
        options = '';

        if (surveyData != '') {
            for (var i = surveyData.attributes.length - 1; i >= 0; i--) {
                options += '<option value="'+surveyData.attributes[i].id+'">'+surveyData.attributes[i].name+'</option>';
            }
        }

        html = '<div class="form-group remove-element"><label for="choose_attribute">Choose Attribute</label><select class="form-control select2" name="choose_attribute"><option value="-1">Choose Attribute</option>'+options+'</select></div>';

        $(html).insertAfter($("select[name=choose_layout]").parent('div'));

    }
}

$(document).on('change', 'select[name=choose_attribute]', function(){
    html = "starhub-johari-" + $(this).val();
    $(".replace-starhub-johari").html('<code class="starhub-johari">' + html + '</code>');

})