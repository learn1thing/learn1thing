
$(document).on('click', '.actionTrigger', function() {
    if ($(this).val() != "") {
        element = $(this).parent("div").find( ".trigger-btn-" + $(this).val() ).eq(0);
        if (element) {
            if (element.attr('href') != "javascript:void(0)") {
                if (typeof element.attr("target") != "undefined") {
                    window.open(element.attr('href'), element.attr("target"));
                } else {
                    document.location.href = element.attr('href');
                }
            } else {
                element.trigger("click");
            }
        }
    }
});

$(document).on('click', '.btn-delete', function() {
    $('#destroy').attr('action', $(this).data('url'));
    $('#delete-modal').modal('show');
});

$(document).on('click', '.btn-create, .btn-edit', function() {
    var table = $(this).data('table');
    $.ajax({
        url: $(this).data('url'),
        type: "GET",
        success: function(response) {
            $('#' + table).replaceWith(response);
        }
    }).done(function(){
        $('#' + table).modal('show');
        rules();
    });
});

$(document).ready(function(){
    if($(".has-error").length >= 1) {
        $('#' + $('.btn-create').data('table')).modal('show');
    }

    $(".sidebar-menu").children('li:not(.header)').each(function(){
        if (window.location.href == $(this).children('a').attr('href')) {
            $(this).addClass('active');
        }
    })
});

$(document).on('shown.bs.modal', '.modal', function() {
    if ($.fn.select2) {
        $('.select2').select2();
    }
})