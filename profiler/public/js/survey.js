$(document).ready(function () {
	$(".question-attributes, .select-profiler-type").select2();
	$(".select-profiler-type").select2({placeholder: "Observer category profile" });

	$(".input-attributes").select2({
	  tags: true
	})
	.on("select2:select", function (e) {
		var targetValue = e.params.data.text;
		$('select.question-attributes').select2({
		  data: [targetValue]
		});
	})
	.on("select2:unselect", function (e) {
		var targetValue = e.params.data.text;
		$('select.question-attributes option[value="' + targetValue + '"]').remove();
	});

	$(".input-number").numeric({decimal: false});

	// $('#period-start').datepicker({
 //    format: "yyyy-mm-dd",
 //    autoclose: true,
 //    todayHighlight: true
 //  })
 //  .on('changeDate', function(e) {
 //  	var periodStart = $(e.target).val();
 //    // make the expiration date > the effective date
 //    if(moment(periodStart) > moment($('#period-end').val())) {
 //    	periodStart = moment(periodStart).add(1, 'days').format('YYYY-MM-DD');
 //    	$('#period-end').val(periodStart);
 //    }
 //    $('#period-end').datepicker('setStartDate', periodStart);
 //  });

	// $('#period-end').datepicker({
	// 	format: "yyyy-mm-dd",
	// 	autoclose: true,
	// 	todayHighlight: true
	// });
	
	$('.datepicker.start_date').datepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayHighlight: true
  })
  .on('changeDate', function(e) {
  	var startDate = $(e.target).val();
  	var endDateInputEl = $(this).parents('.col-sm-12').find('.datepicker.end_date');
    // make the end date > the start date
    if(moment(startDate) > moment(endDateInputEl.val())) {
    	startDate = moment(startDate).add(1, 'days').format('YYYY-MM-DD');
    	endDateInputEl.val(startDate);
    }
    endDateInputEl.datepicker('setStartDate', startDate);
  });

	$('.datepicker.end_date').datepicker({
		format: "yyyy-mm-dd",
		autoclose: true,
		todayHighlight: true
	});


	$('#cmp-reminder-date').datepicker({
		format: "yyyy-mm-dd",
		autoclose: true,
		todayHighlight: true,
		startDate: '+1d'
	});

	$("#cmp-reminder-time").timepicker({
    showInputs: false,
    defaultTime: false,
    showMeridian: false
  });


	$('.toggle-optional-type').each(function(e){
		var typeValue = $(this).prop('checked');
		if(!typeValue) {
			$(this).parents('.tab-pane.tab-question').find('.sections').html('')
		}
	});

	$('.variable-help').popover({
		title: 'Variable information',
		html: true,
		content: '<ul>' +
			'<li>' +
				'<code>[survey-title]</code> will replace by profiler name' +
			'</li>' +
			'<li>' +
				'<code>[buyer-name]</code> will replace by buyer name' +
			'</li>' +
			'<li>' +
				'<code>[observer-name]</code> will replace by observer name' +
			'</li>' +
			// '<li>' +
			// 	'<code>[current-date]</code> will replace by survey date' +
			// '</li>' +
		'</ul>'
	});

	renderCkeditorByClass('question_contents');

	function renderCkeditorByClass(className) {
		$('.question_contents, .profiler_settings').each(function () {
			var id = $(this).attr('id');
			renderCkeditor(id);
		});
	}

	function renderCkeditor(elementId) {
  	// Replace the <textarea id="<elementId>"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace(elementId, {extraPlugins: 'embed'});
	}

	$('.sections').delegate('.add-question', 'click', function (e) {
		e.preventDefault();
		var parent = $(this).parents('.tab-pane');
		var section = $(this).parents('.box.section');
		let sectionType = parent.data('section-type');
		var sectionNumber = section.index();
		var questionTemplate = _.template($('#question-template').html());
		var questionCount = $(this).parent().parent().find('.questions .question.callout').length;
		var questionInputId = sectionType + '-' + 'section-' + sectionNumber + '-question-'+ questionCount;
		var automaticNumbering = section.find('.select-automatic-numbering').val();
		var questions = section.find('.box-body .questions').append(questionTemplate({survey_type: sectionType, section_index: sectionNumber, automatic_numbering: automaticNumbering, question_index: questionCount}));
		$('.toggle-mandatory').bootstrapToggle();
		questions.find('.question.callout').last().find('textarea#question').attr('id', questionInputId);
		renderCkeditor(questionInputId);

        $('#form-survey').validator('update');
	});

	$('.section-wrapper').delegate('.add-section', 'click', function (e) {
		e.preventDefault();
		var parent = $(this).parents('.tab-pane');
		let sectionType = parent.data('section-type');
		var sectionTemplate = _.template($('#section-template').html());
		var sectionCount = $(this).parents('.section-wrapper').find('.sections .panel.box.section').length;
		var sectionId = 'section-'+ (parseInt(sectionCount)+1);
		var sections = $(this).parents('.section-wrapper').find('.sections').append(sectionTemplate({survey_type: sectionType, section_index: sectionCount}));
		var lastSection = sections.find('.box.section').last();
		lastSection.find('.box-title a').attr('href', '#' + sectionId);
		lastSection.find('.box-title a').html('Section ' + (sectionCount + 1));
		lastSection.find('.panel-collapse').attr('id', sectionId);

		var sectionNumber = lastSection.index();
		var questionTemplate = _.template($('#question-template').html());
		var questionCount = lastSection.find('.questions .question.callout').length;
		var questionInputId = sectionType + '-' + 'section-' + sectionNumber + '-question-'+ questionCount;
		var automaticNumbering = lastSection.find('.select-automatic-numbering').val();
		var questions = lastSection.find('.box-body .questions').append(questionTemplate({survey_type: sectionType, section_index: sectionNumber, automatic_numbering: automaticNumbering, question_index: questionCount}));
		$('.toggle-mandatory').bootstrapToggle();
		questions.find('.question.callout').last().find('textarea#question').attr('id', questionInputId);
		renderCkeditor(questionInputId);

		$('#form-survey').validator('update');
	});

	$('.tab-content').delegate("select.answer-type", "change", function () {
		var sectionType = $(this).parents('.tab-pane.active').data('section-type');
		var sectionIndex = $(this).parents('.panel.section').index();
		var questionIndex = $(this).parents('.question').index();
		var answerIndex = $(this).parents('.question').find('answer-options .answer-elements').length;
		var templateName = '#template-' + $(this).val();
		var template = $(templateName).html();
		if(template) {
			template = _.template(template);
			var answerTemplate = $(this).parents('.question').find('.answer-options').html(template({survey_type: sectionType, section_index: sectionIndex, question_index: questionIndex, answer_index: answerIndex}));
			var answerElementTemplate = $(templateName + '-answer-element').html();
			
			// initialize the datepicker
			answerTemplate.find('.datepicker.start_date').datepicker({
		    format: "yyyy-mm-dd",
		    autoclose: true,
		    todayHighlight: true
		  })
		  .on('changeDate', function(e) {
		  	var startDate = $(e.target).val();
		    // make the expiration date > the effective date
		    if(moment(startDate) > moment(answerTemplate.find('.datepicker.end_date').val())) {
		    	startDate = moment(startDate).add(1, 'days').format('YYYY-MM-DD');
		    	answerTemplate.find('.datepicker.end_date').val(startDate);
		    }
		    answerTemplate.find('.datepicker.end_date').datepicker('setStartDate', startDate);
		  });

			answerTemplate.find('.datepicker.end_date').datepicker({
				format: "yyyy-mm-dd",
				autoclose: true,
				todayHighlight: true
			});

			if(answerElementTemplate) {
				answerElementTemplate = _.template($(templateName + '-answer-element').html());
				answerTemplate.find('.answer-elements').append(answerElementTemplate({survey_type: sectionType, section_index: sectionIndex, question_index: questionIndex, answer_index: answerIndex}));
			}
		} else {
			$(this).parents('.question').find('.answer-options').html('');
		}
		
		$(".input-number").numeric({decimal: false});
		
		var attributes = $(".input-attributes").val();
		$(".question-attributes").select2({data: attributes});
		$('#form-survey').validator('update');
	});

	$('.section-wrapper .sections').delegate('.remove-question', 'click', function (e) {
		e.preventDefault();
		var questionWrapper = $(this).parents('.questions');
		var questionCounts = questionWrapper.find('.question.callout').length;
		if(questionCounts === 1) {
			$.toast({
			  heading: 'Failed',
			  text: 'The section have at least one question',
			  icon: 'error',
			  position: 'top-right'
			});
		} else {
			$(this).parents('.question.callout').slideUp('slow', function () {
				$(this).remove();
			});
		}
	});

	$('.sections').delegate('.add-answer-option-element', 'click', function (e) {
		e.preventDefault();
		var parent = $(this).parents('.col-sm-12');
		var sectionType = $(this).parents('.tab-pane.active').data('section-type');
		var sectionIndex = $(this).parents('.panel.section').index();
		var questionIndex = $(this).parents('.question').index();
		var answerIndex = $(this).parents('.answer-options').find('.row.answer-elements div.row').length;
		var templateElementName = $(this).data('template-element-name');
		var templateElement = _.template($('#' + templateElementName).html());
		parent.find('.answer-elements').append(templateElement({survey_type: sectionType, section_index: sectionIndex, question_index: questionIndex, answer_index: answerIndex}));
		var attributes = $(".input-attributes").val();
		$(".question-attributes").select2({data: attributes});
		$(".input-number").numeric({decimal: false});
		$('#form-survey').validator('update');
	});

	$('.section-wrapper .sections').delegate('.delete-section', 'click', function (e) {
		e.preventDefault();
		var tabPane =  $(this).parents('.tab-pane');
		var sectionParents = tabPane.find('.section-wrapper .sections');
		var sectionCounts = sectionParents.find('.panel.box.section').length;
		if(sectionCounts === 1) {
			var sectionType = tabPane.data('section-type');
			$.toast({
			  heading: 'Failed',
			  text: 'The '+ sectionType +' have at least one section',
			  icon: 'error',
			  position: 'top-right'
			});
		} else {
			$(this).parents('.panel.box.section').slideUp('slow', function () {
				$(this).remove();
			});
		}
	});

	$('.sections').delegate('.delete-answer-option-element', 'click', function (e) {
		e.preventDefault();
		$(this).parent().parent().remove();
		// $(this).parents('.row.col-md-12').remove();
	});

	// event change for optionally preliminary or post
	$('.toggle-optional-type').change(function() {
		var typeValue = $(this).prop('checked');
		if(typeValue) {
			// add section
			var parent = $(this).parents('.tab-pane.tab-question');
			let sectionType = parent.data('section-type');
			var sectionTemplate = _.template($('#section-template').html());
			var sectionCount = parent.find('.section-wrapper .sections .panel.box.section').length;
			var sectionId = 'section-'+ (parseInt(sectionCount)+1);
			var sections = parent.find('.section-wrapper .sections').append(sectionTemplate({survey_type: sectionType, section_index: sectionCount}));
			var lastSection = sections.find('.box.section').last();
			lastSection.find('.box-title a').attr('href', '#' + sectionId);
			lastSection.find('.box-title a').html('Section ' + (sectionCount + 1));
			lastSection.find('.panel-collapse').attr('id', sectionId);

			var sectionNumber = lastSection.index();
			var questionTemplate = _.template($('#question-template').html());
			var questionCount = lastSection.find('.questions .question.callout').length;
			var questionInputId = sectionType + '-' + 'section-' + sectionNumber + '-question-'+ questionCount;
			var automaticNumbering = lastSection.find('.select-automatic-numbering').val();
			var questions = lastSection.find('.box-body .questions').append(questionTemplate({survey_type: sectionType, section_index: sectionNumber, automatic_numbering: automaticNumbering, question_index: questionCount}));
			$('.toggle-mandatory').bootstrapToggle();
			questions.find('.question.callout').last().find('textarea#question').attr('id', questionInputId);
			renderCkeditor(questionInputId);
		} else {
			$(this).parents('.tab-pane.tab-question').find('.sections').html('');
		}
		$('#form-survey').validator('update');

		$(this).parents('.tab-pane.tab-question.active').find('.section-wrapper').toggleClass('hidden');
  });

  $('.sections').delegate('.select-automatic-numbering', 'change', function(){
  	var isAutomaticNumbering = $(this).val();
  	var parent = $(this).parents('.section');
  	if(isAutomaticNumbering === 'yes') {
  		parent.find('.select-ordering-type, .select-numbering-type')
  			.removeAttr('disabled')
  			.attr('required', 'required');
  		parent.find('.input-question-number')
  			.attr('disabled', true)
  			.removeAttr('required');
  	} else {
  		parent.find('.select-ordering-type, .select-numbering-type')
  			.attr('disabled', true)
  			.removeAttr('required')
  			.val('');
  		parent.find('.input-question-number')
  			.removeAttr('disabled')
  			.attr('required', 'required');
  	}
  	$('#form-survey').validator('update');
  });

  // disable observer count when is_allow_set_observer is true
  $('#form-survey').delegate('select.allow_set_observer', 'change', function (e) {
  	var value = $(this).val();
  	if(value == 'disable') {
  		$('#form-survey input#observer_count').val('');
  		$('#form-survey input#observer_count').attr('disabled', true);

      $('#form-survey input#category_count').val('');
      $('#form-survey input#category_count').attr('disabled', true);

      $('#form-survey input#observer_count').removeAttr('required');
      $('#form-survey input#category_count').removeAttr('required');
  	} else {
      $('#form-survey input#observer_count').removeAttr('disabled');

  		$('#form-survey input#category_count').removeAttr('disabled');

  		$('#form-survey input#observer_count').attr('required', 'required');
      $('#form-survey input#category_count').attr('required', 'required');
  	}
  	$('#form-survey').validator('update');
  });

  // set reminder value be required if reminder type selected
  $('.select-reminder-type').on('change', function(){
  	var typeValue = $(this).val();
  	if(typeValue.length > 0) {
  		$('.input-reminder-value').attr('required', 'required');
  	} else {
  		$('.input-reminder-value').removeAttr('required');
  	}
  	$('#form-survey').validator('update');
  });

  //iCheck for checkbox inputs
  $('input[type="checkbox"].icheck').iCheck({
    checkboxClass: 'icheckbox_minimal-blue pull-left'
  });

  // event change profiler type and disabled the observer inputs if the value is Self profiler
  $('#form-survey').delegate('#inputProfilerType', 'change', function (event) {
  	var profilerType = $(this).val();
  	var isDisableInput = profilerType == 'self-profiler' ? true:false;
  	var isDisableIcheck = profilerType == 'self-profiler' ? 'disable':'enable';
  	$(".observer_input").attr('disabled', isDisableInput);
  	$('#isSameObserverDesc').iCheck(isDisableIcheck);
  	// $('#toggle-cmp-reminder').iCheck(isDisableIcheck);

  	var sameObserverDesc = $('#isSameObserverDesc').prop('checked');
  	if(!isDisableInput && sameObserverDesc) {
  		$('textarea#inputObserverInfo').attr('disabled', 'disabled');
  		CKEDITOR.instances['inputObserverInfo'].setReadOnly(true);	
  	} else {
  		CKEDITOR.instances['inputObserverInfo'].setReadOnly(isDisableInput);
  		if(isDisableInput) {
  			$('textarea#inputObserverInfo').attr('disabled', 'disabled');
  		} else {
  			$('textarea#inputObserverInfo').removeAttr('disabled');
  		}
  	}

  	// var cmpReminder = $('#toggle-cmp-reminder').prop('checked');
  	// if(!isDisableInput && cmpReminder) {
  	// 	$('#cmp-reminder-date').attr('disabled', false);
  	// 	$('#cmp-reminder-time').attr('disabled', false);
  	// } else {
  	// 	$('#cmp-reminder-date').attr('disabled', true);
  	// 	$('#cmp-reminder-time').attr('disabled', true);
  	// }

  	if(isDisableInput) {
  		$(".observer_input").removeAttr('required');
  	} else {
  		$(".observer_input").attr('required', 'required');
  	}

  	if(profilerType === 'self-profiler') {
	  	$('.observer_input').parents('.form-group')
	  		.removeClass('has-error')
	  		.find('.help-block')
	  		.removeClass('with-errors')
	  		.html('');
  	}


  	$('#form-survey').validator('update');
  });

  // enable/disable observer info depending on the value of same observer desc checkbox 
  $('#isSameObserverDesc').on('ifChanged', function(event){
	  var isChecked = $(this).prop('checked');
	  CKEDITOR.instances['inputObserverInfo'].setReadOnly(isChecked);
	});

	// enable/disable the date input for fixed cmp reminder
	$('#toggle-cmp-reminder').on('ifChanged', function (event) {
		var isChecked = $(this).prop('checked');
		$('input#cmp-reminder-date').attr('disabled', !isChecked).attr('required', isChecked);
		$('input#cmp-reminder-time').attr('disabled', !isChecked).attr('required', isChecked);
		if(isChecked) {
			$('select.select-reminder-type')
				.attr('disabled', 'disabled');
			$('input#inputReminderValue')
				.attr('disabled', 'disabled');
		} else {
			$(this).parents('.form-group')
				.removeClass('has-error')
				.removeClass('has-danger')
				.find('.help-block').html('');
			$('input#cmp-reminder-time').parents('.form-group')
				.removeClass('has-error')
				.removeClass('has-danger')
				.find('.help-block').html('');
				
			$('select.select-reminder-type')
				.removeAttr('disabled');
			$('input#inputReminderValue')
				.removeAttr('disabled');
		}
		$('#form-survey').validator('update');
	});

	// enable/disable the url input for redirect after completion
	$('#toggle-redirect-completion').on('ifChanged', function (event) {
		var isChecked = $(this).prop('checked');
		
		if(!isChecked) {
			$('input#redirect_after_completion_url')
				.attr('disabled', 'disabled')
				.removeAttr('required');
			$(this).parents('.form-group')
				.removeClass('has-error')
				.removeClass('has-danger')
				.find('.help-block')
				.html('');
		} else {
			$('input#redirect_after_completion_url')
				.removeAttr('disabled')
				.attr('required', 'required');
		}
		$('#form-survey').validator('update');
	});

  $('#form-survey').validator().on('submit', function (e) {
  	if (e.isDefaultPrevented()) {
  		$('.question_contents').each(function(val, key){
	    	var id = $(this).attr('id');
	  		var self = $(this);
	  		var contentValue = CKEDITOR.instances[id].getData();
	  		if(contentValue.length > 0) {
	  			self.parent().removeClass('has-error');
	  			self.parent().find('.help-block.with-errors').html('');
	  		} else {
	  			isHasError = true;
	  			self.parents('.question').find('.input-question-number').focus();
	  			self.parent().addClass('has-error');
	  			var message = 'Please fill out this field.';
	  			self.parent().find('.help-block.with-errors').html(message);
	  		}
	  	});
	  	e.preventDefault();
	  	$.toast({
			  heading: 'Missing requirements',
			  text: 'Please check again form you submitted',
			  icon: 'error',
			  position: 'top-right'
			});
	  } else {
	    var isHasError = false;
	    $('.question_contents').each(function(val, key){
	    	var id = $(this).attr('id');
	  		var self = $(this);
	  		var contentValue = CKEDITOR.instances[id].getData();
	  		if(contentValue.length > 0) {
	  			self.parent().removeClass('has-error');
	  			self.parent().find('.help-block.with-errors').html('');
	  		} else {
	  			isHasError = true;
	  			self.parents('.question').find('.input-question-number').focus();
	  			self.parent().addClass('has-error');
	  			var message = 'Please fill out this field.';
	  			self.parent().find('.help-block.with-errors').html(message);
	  		}
	  	});
	  	if(isHasError) {
	  		e.preventDefault();
	  		$.toast({
				  heading: 'Missing requirements',
				  text: 'Please check again form you submitted',
				  icon: 'error',
				  position: 'top-right'
				});
	  	}
	  }
	});
});