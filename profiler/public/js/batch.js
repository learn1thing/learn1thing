
$(document).on('change', 'select[name=survey_id]', function() {
    var select = $("select[name=observer_id]");
    var surveyId = $(this).val();
    $.ajax({
        url: $(this).data('url').replace('surveyId', surveyId),
        type: "GET",
        success: function(response) {
            select.select2('destroy');
            select.html('');
            $.each(response, function (value, text) {
                select.append($("<option>").attr('value',value).text(text));
            });
            select.select2();
        }
    }).done(function(){
    });
});