<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Logs the user out and sends them to the home page
 *
 * @package    core
 * @subpackage auth
 * @copyright  1999 onwards Martin Dougiamas  http://dougiamas.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

//require_once('../config.php');

require_once(dirname(__FILE__) . '/../../config.php');

$PAGE->set_url('/auth/userkey/logout.php');
$PAGE->set_context(context_system::instance());

$sesskey = optional_param('sesskey', '__notpresent__', PARAM_RAW); // we want not null default to prevent required sesskey warning
$login   = optional_param('loginpage', 0, PARAM_BOOL);

//Add logout for manage
$isManage=optional_param('ismanage',0,PARAM_INT);
$email=optional_param('email','notfound',PARAM_TEXT);

if (!function_exists('logoutAuth')) {
   function logoutAuth()
    {
        $ch = curl_init();
        
        curl_setopt($ch,CURLOPT_URL, "https://store.learn1thing.com/dashboard/api/v1/logout");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
         'Authorization:'. $_COOKIE['token']
        ));
        //execute post
        $result = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        //close connection
        curl_close($ch);
        
        return $result;
    }
}

if (!function_exists('manageLogin')) {
    function manageLogin($email_data)
    {
        $ch = curl_init();
        
        curl_setopt($ch,CURLOPT_URL, 'https://store.learn1thing.com/dashboard/api/v1/manage/login/'.$email_data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //execute post
        $result = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        //close connection
        curl_close($ch);

        $data=json_decode($result)->data;
        
        return $data->redirect_url;
    }
}



// can be overridden by auth plugins
if ($login) {
    $redirect = get_login_url();
} else {
    $redirect = $CFG->wwwroot.'/';
}

if (!isloggedin()) {
    // no confirmation, user has already logged out
    logoutAuth();
    require_logout();
    //redirect($redirect);
    if ($isManage==1 && $email !='notfound') {
        $redirect_url=manageLogin($email);
        redirect($redirect_url);
    }else{

        //redirect($redirect);
        redirect('https://store.learn1thing.com/dashboard/login', '', 10);
    }
    
}

$authsequence = get_enabled_auth_plugins(); // auths, in sequence
foreach($authsequence as $authname) {
    $authplugin = get_auth_plugin($authname);
    $authplugin->logoutpage_hook();
}

logoutAuth();

require_logout();
//redirect($redirect);
if ($isManage==1 && $email !='notfound') {
    $redirect_url=manageLogin($email);
    redirect($redirect_url);
}else{
    header('Location: https://store.learn1thing.com/dashboard/login');
}



