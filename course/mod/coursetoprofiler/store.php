<?php

require_once("../config.php");
require_once("lib.php");
require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/gradelib.php');
require_once($CFG->libdir.'/completionlib.php');
require_once($CFG->libdir.'/plagiarismlib.php');
require_once($CFG->dirroot . '/course/modlib.php');

// $profiler_id    = required_param('profiler');
// $course_id    	= required_param('course');
// print_error(required_param('course'));
// echo required_param('course');
// var_export -- nice, one-liner
// $debug_export = var_export($course_id, true);

// var_dump
// ob_start();
// var_dump($course_id;
// $debug_dump = ob_get_clean();

// // print_r -- included for completeness, though not recommended
// $debug_printr = print_r($course_id, true);

// ob_start();
// var_dump($course_id);
// $result = ob_get_clean();
// error_log('My variable x is: ' . print_r(required_param('course'), true));
// $url = new moodle_url('mod/coursetoprofiler/store.php');
// $url->param('course', $course_id);
// $url->param('profiler', $profiler_id);
// $PAGE->set_url($url);
// $PAGE->set_context($context)
// echo $questionnaire->renderer->header();
// echo($course_id);;


// // Required files.
// require_once(__DIR__ . '/config.php');
// // Required and optional parameters.
// $id = required_param('id', PARAM_INT);
// $text = optional_param('text', '', PARAM_ALPHA);
 
// // Setting context for the page.
// $PAGE->set_context(context_system::instance());
// // URL is created and then set for the page navigation.
// $url = new moodle_url('/coursetoprofiler/store.php');
// $PAGE->set_url($url);
// // Heading, headers, page layout.
// $PAGE->set_heading(get_string('hello'));
// $PAGE->set_pagelayout('standard');
// echo $OUTPUT->header();
// // Displaying basic content.
// echo html_writer::tag('p', $text);
// // Display the footer.
// echo $OUTPUT->footer();