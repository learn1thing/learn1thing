<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The main coursetoprofiler configuration form
 *
 * It uses the standard core Moodle formslib. For more info about them, please
 * visit: http://docs.moodle.org/en/Development:lib/formslib.php
 *
 * @package    mod_coursetoprofiler
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once("../config.php");
require_once($CFG->dirroot.'/course/moodleform_mod.php');

/**
 * Module instance settings form
 *
 * @package    mod_coursetoprofiler
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_coursetoprofiler_mod_form extends moodleform_mod {

    /**
     * Defines forms elements
     */
    public function definition() {
        global $CFG, $USER, $DB, $PAGE;

        $PAGE->requires->js( new moodle_url($CFG->wwwroot . '/mod/coursetoprofiler/jquery-2.2.3.min.js'));

        $categories = [];
        $mform = $this->_form;
        
        # Get Data Course Category
        $categorySql = "SELECT id, name FROM mdl_course_categories";
        $category = $DB->get_records_sql($categorySql);

        foreach ($category as $value) {
            $categories[$value->id] = $value->name;
        }
        # End Get Data Course Category

        //var_dump($_SESSION);die();

        # Add Element        
        $mform->addElement('header', 'general', get_string('general', 'form'));
        $mform->addElement('text', 'email', 'email', ['style'=>'width:100%', 'readonly'=>'readonly', 'value'=>$USER->email]);
        // $mform->addElement('select', 'category', 'Select Category', array_merge([''=>'Select Category'], $categories), ['style'=>'width:100%']);
        // $mform->addElement('select', 'course_id', 'Select Course', [''=>'Select Course'], ['style'=>'width:100%']);
        // var_dump($_SESSION['course_profilers']); die();
        $mform->addElement('select', 'profiler', 'Select Profiler', [''=>'Select Profiler'], ['style'=>'width:100%', 'data-selected' => (array_key_exists('course_profilers', $_SESSION) && array_key_exists('update', $_REQUEST) ? $_SESSION['course_profilers']->profiler_id : '-' )]);
        $mform->addElement('select', 'coach', 'Select Coach', [], ['style'=>'width:100%', 'data-selected' => (array_key_exists('course_profilers', $_SESSION) && array_key_exists('update', $_REQUEST) ? $_SESSION['course_profilers']->coach_id : '-' )]);
        $mform->getElement('coach')->setMultiple(true);
        $mform->addElement('text', 'url', 'URL', ['readonly'=>'readonly', 'style'=>'width:100%', 'data-selected' => (array_key_exists('course_profilers', $_SESSION) && array_key_exists('update', $_REQUEST) ? $_SESSION['course_profilers']->intro : '-' )]);
        $mform->addElement('text', 'product', 'product', ['readonly'=>'readonly', 'style'=>'width:100%']);
        $mform->addElement('text', 'name', 'name', ['readonly'=>'readonly', 'style'=>'width:100%']);
        # End Add Element

        # Set Type Element
        $mform->setType('email', PARAM_TEXT);
        $mform->setType('url', PARAM_TEXT);
        $mform->setType('product', PARAM_TEXT);
        $mform->setType('name', PARAM_TEXT);
        # End Set Type Element

        # Add Rule
        // $mform->addRule('category', null, 'required', null, 'client');
        // $mform->addRule('course', null, 'required', null, 'client');
        // $mform->addRule('profiler', null, 'required', null, 'client');
        // $mform->addRule('coach', null, 'required', null, 'client');
        // $mform->addRule('url', null, 'required', null, 'client');
        // $mform->addRule('product', null, 'required', null, 'client');
        # End Add Rule
        
        // Adding the standard "intro" and "introformat" fields.
        if ($CFG->branch >= 29) {
            $this->standard_intro_elements();
        } else {
            $this->add_intro_editor();
        }

        // Add standard grading elements.
        $this->standard_grading_coursemodule_elements();

        // Add standard elements, common to all modules.
        $this->standard_coursemodule_elements();

        // Add standard buttons, common to all modules.
        $this->add_action_buttons();

        # Custom Js
        //$PAGE->requires->js( new moodle_url($CFG->wwwroot . '/mod/coursetoprofiler/jquery-2.2.3.min.js'));
        $PAGE->requires->js( new moodle_url($CFG->wwwroot . '/mod/coursetoprofiler/index.js'));
        # End Custom Js
    }
}
