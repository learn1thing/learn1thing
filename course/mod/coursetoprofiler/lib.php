<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of interface functions and constants for module coursetoprofiler
 *
 * All the core Moodle functions, neeeded to allow the module to work
 * integrated in Moodle should be placed here.
 *
 * All the coursetoprofiler specific functions, needed to implement all the module
 * logic, should go to locallib.php. This will help to save some memory when
 * Moodle is performing actions across all modules.
 *
 * @package    mod_coursetoprofiler
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Example constant, you probably want to remove this :-)
 */
define('NEWMODULE_ULTIMATE_ANSWER', 42);

/* Moodle core API */

/**
 * Returns the information on whether the module supports a feature
 *
 * See {@link plugin_supports()} for more info.
 *
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed true if the feature is supported, null if unknown
 */
function coursetoprofiler_supports($feature) {

    switch($feature) {
        case FEATURE_MOD_INTRO:
            return true;
        case FEATURE_SHOW_DESCRIPTION:
            return true;
        case FEATURE_GRADE_HAS_GRADE:
            return true;
        case FEATURE_BACKUP_MOODLE2:
            return true;
        default:
            return null;
    }
}

/**
 * Saves a new instance of the coursetoprofiler into the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @param stdClass $coursetoprofiler Submitted data from the form in mod_form.php
 * @param mod_coursetoprofiler_mod_form $mform The form instance itself (if needed)
 * @return int The id of the newly inserted coursetoprofiler record
 */
function coursetoprofiler_add_instance(stdClass $coursetoprofiler, mod_coursetoprofiler_mod_form $mform = null) {
    global $DB;

    // You may have to add extra stuff in here.

    $coursetoprofiler->name = ($coursetoprofiler->product == '') ? 'Profiler' : $coursetoprofiler->product;//'Link To Profiler';
    $coursetoprofiler->timecreated = time();
    $description=$coursetoprofiler->intro;

    $coursetoprofiler->id = $DB->insert_record('coursetoprofiler', $coursetoprofiler);

    //Change intro set url to save  Course_Profiler
    $coursetoprofiler->intro = ($coursetoprofiler->url == '') ? '-' : $coursetoprofiler->url; //$coursetoprofiler->url;
    $course_profiler_id = course_profilers_store($coursetoprofiler);
    
    $coursetoprofiler->course_profiler_id = $course_profiler_id;
    $coursetoprofiler->intro=$description;
    
    $result = $DB->update_record('coursetoprofiler', $coursetoprofiler);

    coursetoprofiler_grade_item_update($coursetoprofiler);

    return $coursetoprofiler->id;
}

/**
 * Updates an instance of the coursetoprofiler in the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @param stdClass $coursetoprofiler An object from the form in mod_form.php
 * @param mod_coursetoprofiler_mod_form $mform The form instance itself (if needed)
 * @return boolean Success/Fail
 */
function coursetoprofiler_update_instance(stdClass $coursetoprofiler, mod_coursetoprofiler_mod_form $mform = null) {
    global $DB;

    //$coursetoprofiler->into=strip_tags($coursetoprofiler->intro);
    $data = $DB->get_record('coursetoprofiler', array('id' => $coursetoprofiler->instance));

    $coursetoprofiler->name = ($coursetoprofiler->product == '') ? $data->name : $coursetoprofiler->product;//'Link To Profiler';

    $coursetoprofiler->timemodified = time();
    $coursetoprofiler->id = $coursetoprofiler->instance;
    
    $result = $DB->update_record('coursetoprofiler', $coursetoprofiler);

    coursetoprofiler_grade_item_update($coursetoprofiler);
        
    $coursetoprofiler->course_profiler_id = $data->course_profiler_id;

    course_profilers_destroy($coursetoprofiler->course_profiler_id);    

    $course_profiler_id = course_profilers_store($coursetoprofiler);
    
    $coursetoprofiler->course_profiler_id = $course_profiler_id;
    $result = $DB->update_record('coursetoprofiler', $coursetoprofiler);
    return $result;
}

/**
 * This standard function will check all instances of this module
 * and make sure there are up-to-date events created for each of them.
 * If courseid = 0, then every coursetoprofiler event in the site is checked, else
 * only coursetoprofiler events belonging to the course specified are checked.
 * This is only required if the module is generating calendar events.
 *
 * @param int $courseid Course ID
 * @return bool
 */
function coursetoprofiler_refresh_events($courseid = 0) {
    global $DB;

    if ($courseid == 0) {
        if (!$coursetoprofilers = $DB->get_records('coursetoprofiler')) {
            return true;
        }
    } else {
        if (!$coursetoprofilers = $DB->get_records('coursetoprofiler', array('course' => $courseid))) {
            return true;
        }
    }

    foreach ($coursetoprofilers as $coursetoprofiler) {
        // Create a function such as the one below to deal with updating calendar events.
        // coursetoprofiler_update_events($coursetoprofiler);
    }

    return true;
}

/**
 * Removes an instance of the coursetoprofiler from the database
 *
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @param int $id Id of the module instance
 * @return boolean Success/Failure
 */
function coursetoprofiler_delete_instance($id) {
    global $DB;
    
    
    if (! $coursetoprofiler = $DB->get_record('coursetoprofiler', array('id' => $id))) {
        return false;
    }
    course_profilers_destroy($coursetoprofiler->course_profiler_id);

    // Delete any dependent records here.

    $DB->delete_records('coursetoprofiler', array('id' => $coursetoprofiler->id));

    coursetoprofiler_grade_item_delete($coursetoprofiler);

    return true;
}

/**
 * Returns a small object with summary information about what a
 * user has done with a given particular instance of this module
 * Used for user activity reports.
 *
 * $return->time = the time they did it
 * $return->info = a short text description
 *
 * @param stdClass $course The course record
 * @param stdClass $user The user record
 * @param cm_info|stdClass $mod The course module info object or record
 * @param stdClass $coursetoprofiler The coursetoprofiler instance record
 * @return stdClass|null
 */
function coursetoprofiler_user_outline($course, $user, $mod, $coursetoprofiler) {

    $return = new stdClass();
    $return->time = 0;
    $return->info = '';
    return $return;
}

/**
 * Prints a detailed representation of what a user has done with
 * a given particular instance of this module, for user activity reports.
 *
 * It is supposed to echo directly without returning a value.
 *
 * @param stdClass $course the current course record
 * @param stdClass $user the record of the user we are generating report for
 * @param cm_info $mod course module info
 * @param stdClass $coursetoprofiler the module instance record
 */
function coursetoprofiler_user_complete($course, $user, $mod, $coursetoprofiler) {
}

/**
 * Given a course and a time, this module should find recent activity
 * that has occurred in coursetoprofiler activities and print it out.
 *
 * @param stdClass $course The course record
 * @param bool $viewfullnames Should we display full names
 * @param int $timestart Print activity since this timestamp
 * @return boolean True if anything was printed, otherwise false
 */
function coursetoprofiler_print_recent_activity($course, $viewfullnames, $timestart) {
    return false;
}

/**
 * Prepares the recent activity data
 *
 * This callback function is supposed to populate the passed array with
 * custom activity records. These records are then rendered into HTML via
 * {@link coursetoprofiler_print_recent_mod_activity()}.
 *
 * Returns void, it adds items into $activities and increases $index.
 *
 * @param array $activities sequentially indexed array of objects with added 'cmid' property
 * @param int $index the index in the $activities to use for the next record
 * @param int $timestart append activity since this time
 * @param int $courseid the id of the course we produce the report for
 * @param int $cmid course module id
 * @param int $userid check for a particular user's activity only, defaults to 0 (all users)
 * @param int $groupid check for a particular group's activity only, defaults to 0 (all groups)
 */
function coursetoprofiler_get_recent_mod_activity(&$activities, &$index, $timestart, $courseid, $cmid, $userid=0, $groupid=0) {
}

/**
 * Prints single activity item prepared by {@link coursetoprofiler_get_recent_mod_activity()}
 *
 * @param stdClass $activity activity record with added 'cmid' property
 * @param int $courseid the id of the course we produce the report for
 * @param bool $detail print detailed report
 * @param array $modnames as returned by {@link get_module_types_names()}
 * @param bool $viewfullnames display users' full names
 */
function coursetoprofiler_print_recent_mod_activity($activity, $courseid, $detail, $modnames, $viewfullnames) {
}

/**
 * Function to be run periodically according to the moodle cron
 *
 * This function searches for things that need to be done, such
 * as sending out mail, toggling flags etc ...
 *
 * Note that this has been deprecated in favour of scheduled task API.
 *
 * @return boolean
 */
function coursetoprofiler_cron () {
    return true;
}

/**
 * Returns all other caps used in the module
 *
 * For example, this could be array('moodle/site:accessallgroups') if the
 * module uses that capability.
 *
 * @return array
 */
function coursetoprofiler_get_extra_capabilities() {
    return array();
}

/* Gradebook API */

/**
 * Is a given scale used by the instance of coursetoprofiler?
 *
 * This function returns if a scale is being used by one coursetoprofiler
 * if it has support for grading and scales.
 *
 * @param int $coursetoprofilerid ID of an instance of this module
 * @param int $scaleid ID of the scale
 * @return bool true if the scale is used by the given coursetoprofiler instance
 */
function coursetoprofiler_scale_used($coursetoprofilerid, $scaleid) {
    global $DB;

    if ($scaleid and $DB->record_exists('coursetoprofiler', array('id' => $coursetoprofilerid, 'grade' => -$scaleid))) {
        return true;
    } else {
        return false;
    }
}

/**
 * Checks if scale is being used by any instance of coursetoprofiler.
 *
 * This is used to find out if scale used anywhere.
 *
 * @param int $scaleid ID of the scale
 * @return boolean true if the scale is used by any coursetoprofiler instance
 */
function coursetoprofiler_scale_used_anywhere($scaleid) {
    global $DB;

    if ($scaleid and $DB->record_exists('coursetoprofiler', array('grade' => -$scaleid))) {
        return true;
    } else {
        return false;
    }
}

/**
 * Creates or updates grade item for the given coursetoprofiler instance
 *
 * Needed by {@link grade_update_mod_grades()}.
 *
 * @param stdClass $coursetoprofiler instance object with extra cmidnumber and modname property
 * @param bool $reset reset grades in the gradebook
 * @return void
 */
function coursetoprofiler_grade_item_update(stdClass $coursetoprofiler, $reset=false) {
    global $CFG;
    require_once($CFG->libdir.'/gradelib.php');

    $item = array();
    $item['itemname'] = clean_param($coursetoprofiler->name, PARAM_NOTAGS);
    $item['gradetype'] = GRADE_TYPE_VALUE;

    if ($coursetoprofiler->grade > 0) {
        $item['gradetype'] = GRADE_TYPE_VALUE;
        $item['grademax']  = $coursetoprofiler->grade;
        $item['grademin']  = 0;
    } else if ($coursetoprofiler->grade < 0) {
        $item['gradetype'] = GRADE_TYPE_SCALE;
        $item['scaleid']   = -$coursetoprofiler->grade;
    } else {
        $item['gradetype'] = GRADE_TYPE_NONE;
    }

    if ($reset) {
        $item['reset'] = true;
    }

    grade_update('mod/coursetoprofiler', $coursetoprofiler->course, 'mod', 'coursetoprofiler',
            $coursetoprofiler->id, 0, null, $item);
}

/**
 * Delete grade item for given coursetoprofiler instance
 *
 * @param stdClass $coursetoprofiler instance object
 * @return grade_item
 */
function coursetoprofiler_grade_item_delete($coursetoprofiler) {
    global $CFG;
    require_once($CFG->libdir.'/gradelib.php');

    return grade_update('mod/coursetoprofiler', $coursetoprofiler->course, 'mod', 'coursetoprofiler',
            $coursetoprofiler->id, 0, null, array('deleted' => 1));
}

/**
 * Update coursetoprofiler grades in the gradebook
 *
 * Needed by {@link grade_update_mod_grades()}.
 *
 * @param stdClass $coursetoprofiler instance object with extra cmidnumber and modname property
 * @param int $userid update grade of specific user only, 0 means all participants
 */
function coursetoprofiler_update_grades(stdClass $coursetoprofiler, $userid = 0) {
    global $CFG, $DB;
    require_once($CFG->libdir.'/gradelib.php');

    // Populate array of grade objects indexed by userid.
    $grades = array();

    grade_update('mod/coursetoprofiler', $coursetoprofiler->course, 'mod', 'coursetoprofiler', $coursetoprofiler->id, 0, $grades);
}

/* File API */

/**
 * Returns the lists of all browsable file areas within the given module context
 *
 * The file area 'intro' for the activity introduction field is added automatically
 * by {@link file_browser::get_file_info_context_module()}
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param stdClass $context
 * @return array of [(string)filearea] => (string)description
 */
function coursetoprofiler_get_file_areas($course, $cm, $context) {
    return array();
}

/**
 * File browsing support for coursetoprofiler file areas
 *
 * @package mod_coursetoprofiler
 * @category files
 *
 * @param file_browser $browser
 * @param array $areas
 * @param stdClass $course
 * @param stdClass $cm
 * @param stdClass $context
 * @param string $filearea
 * @param int $itemid
 * @param string $filepath
 * @param string $filename
 * @return file_info instance or null if not found
 */
function coursetoprofiler_get_file_info($browser, $areas, $course, $cm, $context, $filearea, $itemid, $filepath, $filename) {
    return null;
}

/**
 * Serves the files from the coursetoprofiler file areas
 *
 * @package mod_coursetoprofiler
 * @category files
 *
 * @param stdClass $course the course object
 * @param stdClass $cm the course module object
 * @param stdClass $context the coursetoprofiler's context
 * @param string $filearea the name of the file area
 * @param array $args extra arguments (itemid, path)
 * @param bool $forcedownload whether or not force download
 * @param array $options additional options affecting the file serving
 */
function coursetoprofiler_pluginfile($course, $cm, $context, $filearea, array $args, $forcedownload, array $options=array()) {
    global $DB, $CFG;

    if ($context->contextlevel != CONTEXT_MODULE) {
        send_file_not_found();
    }

    require_login($course, true, $cm);

    send_file_not_found();
}

/* Navigation API */

/**
 * Extends the global navigation tree by adding coursetoprofiler nodes if there is a relevant content
 *
 * This can be called by an AJAX request so do not rely on $PAGE as it might not be set up properly.
 *
 * @param navigation_node $navref An object representing the navigation tree node of the coursetoprofiler module instance
 * @param stdClass $course current course record
 * @param stdClass $module current coursetoprofiler instance record
 * @param cm_info $cm course module information
 */
function coursetoprofiler_extend_navigation(navigation_node $navref, stdClass $course, stdClass $module, cm_info $cm) {
    // TODO Delete this function and its docblock, or implement it.
}

/**
 * Extends the settings navigation with the coursetoprofiler settings
 *
 * This function is called when the context for the page is a coursetoprofiler module. This is not called by AJAX
 * so it is safe to rely on the $PAGE.
 *
 * @param settings_navigation $settingsnav complete settings navigation tree
 * @param navigation_node $coursetoprofilernode coursetoprofiler administration node
 */
function coursetoprofiler_extend_settings_navigation(settings_navigation $settingsnav, navigation_node $coursetoprofilernode=null) {
    // TODO Delete this function and its docblock, or implement it.
}

/**
 * Saves a new record of the course_profilers into the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new record and return the id number
 * of the new record.
 *
 * @param stdClass $coursetoprofiler Submitted data from the form in mod_form.php
 * @param mod_coursetoprofiler_mod_form $mform The form record itself (if needed)
 * @return int The id of the newly inserted coursetoprofiler record
 */
function course_profilers_store(stdClass $coursetoprofiler) {
    global $DB;

    $sql = "SELECT id FROM mdl_enrol WHERE enrol = 'manual' AND courseid = ".$coursetoprofiler->course;

    $res = $DB->get_records_sql($sql);
    $enrolId = 0;

    foreach ($res as $key => $value) {
        $enrolId = $value->id;
    }

    if ($enrolId == 0) {
        $enrols = new stdClass;
        $enrols->enrol = 'manual';
        $enrols->courseid = $coursetoprofiler->id;
        $enrols->roleid = 5;
        $enrols->password = '7nepi!';
        $enrols->timecreated = time();
        $enrols->timemodified = time();

        $enrols->id = $DB->insert_record('enrol', $enrols);
        $enrolId = $enrols->id;
    }
    $coach = [];
    if (array_key_exists('coach', $_POST)) {
        $coach = $_POST['coach'];
    }
    $data = new stdClass;
    $data->coursetoprofiler_id = $coursetoprofiler->id;
    $data->profiler_id = ($_POST['profiler'] == '') ? 0 : explode('|', $_POST['profiler'])[0];
    $data->course_id = $coursetoprofiler->course;
    $data->coach_id = json_encode($coach);
    $data->product_name = $_POST['product'];
    $data->email = $_POST['email'];
    $data->intro = $_POST['url'];
    $data->enrol_id = $enrolId;
    $data->introformat = 0;
    $data->grade = 100;
    $data->timecreated = date('Y-m-d H:i:s');
    $data->timemodified = date('Y-m-d H:i:s');

    $data->id = $DB->insert_record('course_profilers', $data);

    return $data->id;
}

/**
 * Updates an record of the course_profilers in the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing record with new data.
 *
 * @param stdClass $coursetoprofiler An object from the form in mod_form.php
 * @param mod_coursetoprofiler_mod_form $mform The form record itself (if needed)
 * @return boolean Success/Fail
 */
function course_profilers_update(stdClass $coursetoprofiler) {
    global $DB;

    $data = new stdClass;
    $data->id = $coursetoprofiler->course_profiler_id;
    $data->coursetoprofiler_id = $coursetoprofiler->id;
    $data->profiler_id = $_POST['profiler'];
    $data->course_id = $coursetoprofiler->course;
    $data->coach_id = json_encode((array_key_exists('coach', $_POST))?$_POST['coach'] : array());
    // $data->product_name = $_POST['product_name'];
    // $data->intro = $_POST['url'];
    // $data->email = $_POST['email'];
    $data->introformat = 0;
    $data->grade = 100;
    // $data->timecreated = time();
    // $data->timemodified = time();

    $result = $DB->update_record('course_profilers', $data);

    return $result;
}

/**
 * Removes an record of the course_profilers from the database
 *
 * Given an ID of an record of this module,
 * this function will permanently delete the record
 * and any data that depends on it.
 *
 * @param int $id Id of the module record
 * @return boolean Success/Failure
 */
function course_profilers_destroy($id) {
    global $DB;

    if (! $course_profilers = $DB->get_record('course_profilers', array('id' => $id))) {
        return false;
    }

    $DB->delete_records('course_profilers', array('id' => $course_profilers->id));
    return true;
}
