<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints a particular instance of coursetoprofiler
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_coursetoprofiler
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Replace coursetoprofiler with the name of your module and remove this line.

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');

global $USER, $SESSION;

$email = strtr(base64_encode(openssl_encrypt($USER->email, 'AES-128-ECB', 'l1tprofiler')), '+/=', '-_,');
$product_id = strtr(base64_encode(openssl_encrypt($SESSION->product_id, 'AES-128-ECB', 'l1tprofiler')), '+/=', '-_,');

//debug
//var_dump($USER);die();

$id = required_param('id', PARAM_INT);    // Course Module ID.

if (! $cm = get_coursemodule_from_id('coursetoprofiler', $id)) {
    print_error('invalidcoursemodule');
}

//$cm itu isinya kalau ndak sama. course dan id profiler ada
//
//coba $cm di var_dump
if (! $course = $DB->get_record("course", array("id" => $cm->course))) {
    print_error('coursemisconf');
}

//get data
$coursetoprofiler_data = $DB->get_record("coursetoprofiler", array("id" => (int)$cm->instance));

$course_profilers=$DB->get_record("course_profilers", array("coursetoprofiler_id" => (int)$cm->instance));

require_login($course, false, $cm);
$context = $PAGE->context;
require_capability('mod/coursetoprofiler:view', $context);

echo $OUTPUT->header();
$PAGE->set_pagelayout('course');
$PAGE->requires->js( new moodle_url($CFG->wwwroot . '/mod/coursetoprofiler/jquery-2.2.3.min.js'));
echo content_profier($coursetoprofiler_data,$course_profilers,$cm,$USER->email);
$PAGE->requires->js( new moodle_url($CFG->wwwroot . '/mod/coursetoprofiler/view.js'));
echo $OUTPUT->footer();

function content_profier($coursetoprofiler,$course_profilers,$cm,$email)
{
	$tag=html_writer::tag('h2',$cm->name);
	$tag .=html_writer::tag('p',$coursetoprofiler->intro);
	$tag .=html_writer::empty_tag('input',['type'=>'hidden','name'=>'email','value'=>$email]);
	$tag .=html_writer::empty_tag('input',['type'=>'hidden','name'=>'id_course_param','value'=>$cm->course]);
	$tag .=html_writer::empty_tag('input',['type'=>'hidden','name'=>'id_profiler_param','value'=>$course_profilers->profiler_id]);
	$tag .=html_writer::tag('br');
	$tag .=html_writer::tag('a','Link To Profiler',['href'=>$course_profilers->intro,'target'=>'_blank','class'=>'btn','id'=>'plugin_btn']);
	$output=html_writer::tag('div',$tag);

	return $output;
}


