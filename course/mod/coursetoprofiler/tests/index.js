// Base URL
var baseUrl = window.location.protocol + "//" + window.location.host + "/";
// var urlMw = 'http://testing.learn1thing.com/';
var urlMw = 'http://store.learn1thing.com/';
// End Base URL

// Token
// var token = '2d85097f0466a984b30d3adcfa7b1fc5'; // LOCAL
var token = 'e4dbd4505af273e9edc357050ebdbe42'; // STORE
// End Token

(function($) {
$(document).ready(function() {
	$('#id_coach').parent().append('<span style="color:red"> * </span> <span> Use CTRL + Click For Mutiple Select</span>');
	$('input[name=email]').parent().parent().hide();
	$('input[name=product]').parent().parent().hide();
	$('input[name=name]').parent().parent().hide();
	$('input[name=name]').val('-');
	
	if ($('input[name=url]').data('selected') != '-') {
		$('input[name=url]').val($('input[name=url]').data('selected'));
	}

	getProfiler();
});
// Get Course
$(document).on('change', 'select[name=category]', function() {

	var url = baseUrl+'webservice/rest/server.php?wstoken='+token+'&wsfunction=local_get_course_dropdown_list&moodlewsrestformat=json';
	var opt = '<option value="">Select Course</option>';
	var request = {
			'req': {
				'0': {
					'email' : $('input[name=email]').val(),
				}
			}	
		};
	if ($(this).val() != '') {
		$.ajax({
			url: url,
			type: 'GET',
			dataType: 'json',
			data: request,
			success: function (response) {
				var res = $.parseJSON(response);
				if (res.data.length > 0 ) {
					jQuery.each(res.data, function(key, val) {
						opt += '<option value="'+val['id']+'">'+val['text']+'</option>';
					});
				} else {
					opt += '<option value="0000">Dummy Data</option>';
				}
				$('select[name=course_id]').html(opt);
			},
			error: function (response) {
				console.log('Error Get Data Course');
				opt += '<option value="0000">Dummy Data</option>';

			}
		});
	}
});
// End Get Course


	function getProfiler() {
		var email = $('input[name=email]').val();
		if (email == 'admin@mailinator.com') {
			email = 'admin@l1t.com';
		}
		var url = urlMw+'dashboard/api/v1/profiler/'+email;
		var opt = '<option value="">Select Profiler</option>';
		$.ajax({
			url: url,
			type: 'GET',
			dataType: 'json',
			jsonpCallback: 'callback',
			crossDomain: true,
			success: function (response) {
				if (response.data.length > 0) {
					$.each(response.data, function(key, val) {
						opt += '<option class="'+val.id+'" value="'+val.id+'|'+val.url+'|'+val.title+'">'+val.title+'</option>';
					});
				} else {
					opt += '<option value="0000">Dummy Data</option>';
				}
				$('select[name=profiler]').html(opt);
				if ($('select[name=profiler]').data('selected') != '-') {
					if ($('select[name=profiler]').data('selected') == 0 ) {
						$('select[name=profiler]').val("0000");
					} else {
						var valu = $('.'+$('select[name=profiler]').data('selected')).attr('value');
						$('select[name=profiler]').val(valu);
					}
				}
			},
			error: function (response) {
				console.log('Error Get Data Course');
				opt += '<option value="0000">Dummy Data</option>';
				$('select[name=profiler]').html(opt);
				if ($('select[name=profiler]').data('selected') != '-') {
					if ($('select[name=profiler]').data('selected') == 0 ) {
						$('select[name=profiler]').val("0000");
					} else {
						$('select[name=profiler]').val($('select[name=profiler]').data('selected'));
					}
				}
			}
		});
		getCoach(email);
	}

	function getCoach(email)
	{
		var opt = '';
		var url = urlMw+'dashboard/api/v1/coach/'+email;
		$.ajax({
			url: url,
			type: 'GET',
			dataType: 'json',
			jsonpCallback: 'callback',
			crossDomain: true,
			success: function (response) {
				if (response.data.length > 0) {
					$.each(response.data, function(key, val) {
						opt += '<option value="'+val.id_coach+'">'+val.name+'</option>';
					});
				} else {
					opt += '<option value="0000">Dummy Data</option>';
				}
				$('select[id=id_coach]').html(opt);
				if ($('select[id=id_coach]').data('selected') != []) {
					// jQuery.each(jQuery('select[id=id_coach]').data('selected'), function(key, val) {
						$('select[id=id_coach]').val($('select[id=id_coach]').data('selected'));
					// });
				}
			},
			error: function (response) {
				console.log('Error Get Data Coach');
				opt += '<option value="0000">Dummy Data</option>';
				$('select[name=coach]').html(opt);
			}
		});
	}


    $("select[name=profiler]").change(function(){
	    console.log($('select[name=profiler]').val());
	});
})(jQuery);




