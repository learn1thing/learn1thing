// Base URL
var baseUrl = window.location.protocol + "//" + window.location.host + "/";
// var urlMw = 'http://testing.learn1thing.com/';
var urlMw = 'https://store.learn1thing.com/';
// End Base URL

// Token
// var token = '2d85097f0466a984b30d3adcfa7b1fc5'; // LOCAL
var token = 'e4dbd4505af273e9edc357050ebdbe42'; // STORE
// End Token

(function($) {

	$(document).ready(function() {
		var email = $('input[name=email]').val();
		var id_course = $('input[name=id_course_param]').val();
		var id_profiler = $('input[name=id_profiler_param]').val();
		if (email == 'admin@mailinator.com') {
			email = 'admin@l1t.com';
		}
		getCustomerProfiler(email,id_course,id_profiler);
	});

	function getCustomerProfiler(email,id_course,id_profiler)
	{
		var opt = '';
		var url = urlMw+'dashboard/api/v1/profiler/customer/'+email+'?id_course='+id_course+'&id_profiler='+id_profiler;
		$.ajax({
			url: url,
			type: 'GET',
			dataType: 'json',
			jsonpCallback: 'callback',
			crossDomain: true,
			success: function (response) {
				//console.log(response.data.data[0].link);
				if(response && response.data.data[0].link && response.data.data[0].link != null){
					$('#plugin_btn').attr('href', response.data.data[0].link);
				}
				
			},
			error: function (response) {
				console.log('Error Get Data Coach');
			}
		});
	}

})(jQuery);




