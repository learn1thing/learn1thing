<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * External Web Service Template
 *
 * @package    localwstemplate
 * @copyright  2011 Moodle Pty Ltd (http://moodle.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once("../../config.php");
require_once($CFG->libdir . "/externallib.php");
// require_once("$CFG->libdir/externallib.php");

class local_course_link_profiler_external extends external_api {

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function store_data_course_link_profiler_parameters() {
        return new external_function_parameters(
            array(
                'groups' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'course_id' => new external_value(PARAM_TEXT, 'id of course'),
                            'profiler_id' => new external_value(PARAM_TEXT, 'id of profiler'),
                            'url' => new external_value(PARAM_TEXT, 'URL of profiler url'),
                            'product_name' => new external_value(PARAM_TEXT, 'URL of product name'),
                            'email' => new external_value(PARAM_TEXT, 'Email'),
                        )
                    )
                )
            )
        );
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    // public static function hello_world_parameters() {
    //     return new external_function_parameters(
    //             array('welcomemessage' => new external_value(PARAM_TEXT, 'The welcome message. By default it is "Hello world,"', VALUE_DEFAULT, 'Hello world, '))
    //     );
    // }

    /**
     * Returns welcome message
     * @return string welcome message
     */
    public static function store_data_course_link_profiler($groups) {
        global $CFG, $DB;
        require_once("$CFG->dirroot/group/lib.php");
        //Parameter validation
        $params = self::validate_parameters(self::store_data_course_link_profiler_parameters(), array('groups'=> $groups));
        $transaction = $DB->start_delegated_transaction();
        $course_id = explode(',', $params['groups'][0]['course_id']);
        $dataInsert = [];

        foreach ($course_id as $key => $value) {
            if (!empty($value)) {
                $dataInsert[$key] = array(
                    'course_id' => $value, 
                    'profiler_id' => (int)$params['groups'][0]['profiler_id'],
                    'url' => $params['groups'][0]['url'],
                    'product_name' => $params['groups'][0]['product_name'],
                    'email' => $params['groups'][0]['email']
                    );
            }
        }

        $data = array();
        foreach ($dataInsert as $group) {
            $group = (object)$group;
            
            if (!$group->profiler_id) {
                throw new invalid_parameter_exception("Invalid Proviler Id");
            }

            if ($DB->get_record('course_profilers', array('course_id'=>$group->course_id, 'profiler_id' => $group->profiler_id))) {
                throw new invalid_parameter_exception('Data with the same name already exists in the course');
            }
            $group->intro = $group->url.'|'.$group->profiler_name;
            $group->timecreated = date('Y-m-d H:i:s');
            $group->id = $DB->insert_record('course_profilers', $group, true);
            $data[] = (array)$group;
        }
 
        $transaction->allow_commit();
        $result['meta']['code'] = '200';
        $result['meta']['message'] = 'success';
        $result['data'] = $data;
        return json_encode($result);
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function store_data_course_link_profiler_returns() {
        return new external_value(PARAM_TEXT, 'Success Insert');
    }

    /**
     * Mark the function as deprecated.
     * @return bool
     */
    public static function store_data_course_link_profiler_is_deprecated() {
        return true;
    }

}
