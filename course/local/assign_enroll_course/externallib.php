<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * URL Hit
 * http://course.dev/webservice/rest/server.php?wstoken=wstoken&wsfunction=local_assign_enroll_course_data&moodlewsrestformat=json
 *
 * Param    url[0][type]
 *          url[0][email]
 * Method   POST
 */

/**
 * External Web Service Template
 *
 * @package    localwstemplate
 * @copyright  2011 Moodle Pty Ltd (http://moodle.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once("../../config.php");
require_once($CFG->libdir . "/externallib.php");

class local_assign_enroll_course_external extends external_api {

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function assign_enroll_course_parameters() {
        return new external_function_parameters(
            array(
                'url' => new external_multiple_structure(
                    new external_single_structure([
                        'type' => new external_value(PARAM_TEXT, 'type'),
                        'course_id' => new external_value(PARAM_TEXT, 'course_id'),
                        'shortname' => new external_value(PARAM_TEXT, 'shortname'),
                        'enrol_id' => new external_value(PARAM_TEXT, 'enrol_id'),
                        'email' => new external_value(PARAM_TEXT, 'email'),
                        'timestart' => new external_value(PARAM_TEXT, 'timestart'),
                        'timeend' => new external_value(PARAM_TEXT, 'timeend'),
                    ])
                )
            )
        );
    }

    /**
     * Returns welcome message
     * @return string welcome message
     */
    public static function assign_enroll_course($url) {
        global $CFG, $DB;
        require_once("$CFG->dirroot/group/lib.php");
        //Parameter validation
        $params =  self::validate_parameters(self::assign_enroll_course_parameters(), 
            array('url' => $url)); 
	 
	$data = array();
        # Get User
        $user = $DB->get_record('user', array('email'=>$params['url'][0]['email']));
        # Chect Type Enrollment
        if ($params['url'][0]['type'] == 'enrol-coach-course') {
            $sql = "SELECT id FROM mdl_enrol
               WHERE enrol = 'manual' AND courseid = ".(int)$params['url'][0]['course_id'];
            $e = $DB->get_records_sql($sql);
            foreach ($e as $key => $value) {
                $params['url'][0]['enrol_id'] = $value->id;
            }
        }
        # Get Data Enrol
        $sql = "SELECT enrolstartdate, enrolenddate FROM mdl_enrol
           WHERE id = ".(int)$params['url'][0]['enrol_id'];
        $courses = $DB->get_records_sql($sql);
        $erl = [];
        foreach ($courses as $value) {
            $erl[] = (array)$value;
        }
        # Set Data
        if ($user) {
            $enrols = new stdClass;
            $enrols->status = 0;
            $enrols->enrolid = (int)$params['url'][0]['enrol_id'];
            $enrols->userid = $user->id;
            if ($params['url'][0]['timestart'] != '-') {
                $enrols->timestart = (int)$params['url'][0]['timestart'];
            } else {
                if (count($erl)>0 && (int)$erl[0]['enrolstartdate'] != 0) {
                    $enrols->timestart = (int)$erl[0]['enrolstartdate'];
                } else {
                    $enrols->timestart = time();
                }
            }
            if ($params['url'][0]['timeend'] != '-') {
                $enrols->timeend = (int)$params['url'][0]['timeend'];
            } else {
                if (count($erl)>0) {
                    $enrols->timeend = (int)$erl[0]['enrolenddate'];
                } else {
                    $enrols->timeend = (int)$params['url'][0]['timeend'];
                }
            }
            // Code Ngadadak
            $enrols->timestart = time();
            $enrols->timeend = 0;
            // End Code Ngadadak
            $enrols->modifierid = 2;
            $enrols->timecreated = time();
            $enrols->timemodified = time();

	
	    //added by puw k5 on 11-01-2018

	    $sql_context="SELECT id FROM `mdl_context` WHERE instanceid=".$params['url'][0]['course_id']." and contextlevel=50";

	    $mdl_context_query = $DB->get_records_sql($sql_context);
	    $mdl_context_id=0;

	    foreach ($mdl_context_query as $key => $value) {
    		$mdl_context_id = $value->id;
	    }

	    	
 
	    //data User
	    $user_role_id=0;
	    foreach ($user as $key => $value) {
    		$user_role_id = $value->id;
	    }
	
	    $check_role_asign=$DB->record_exists('role_assignments',array(
		'roleid'=>'5',
		'contextid'=>$mdl_context_id,
		'userid'=>$user->id
	    ));
 



	    if($check_role_asign){
		
	    	//$sql_test="INSERT INTO mdl_role_assignments(roleid,contextid,userid,timemodified,modifierid,component,itemid,sortorder) VALUES(5,".$mdl_context_id.",".$user->id.",".time().",2,2,0,0)";
	    	//$DB->execute($sql_test);
	    }else{
		$sql_test="INSERT INTO mdl_role_assignments(roleid,contextid,userid,timemodified,modifierid,component,itemid,sortorder) VALUES(5,".$mdl_context_id.",".$user->id.",".time().",2,2,0,0)";
	    	$DB->execute($sql_test);

	    }
	
            # End Set Data
            $check = $DB->get_record('user_enrolments', array(
                    'userid'    => $user->id,
                    'enrolid'   => (int)$params['url'][0]['enrol_id']
                )
            );
            // var_dump($check->id); die();
            if (!$check) {
                $enrols->id = $DB->insert_record('user_enrolments', $enrols);
		
		
		//insert role_assignments
		//$sql_role_="INSERT INTO mdl_role_assignments(roleid,contextid,userid,timemodified,modifierid,component,itemid,sortorder) VALUES(5,".$mdl_context_id.",".$user_role_id.",1515753823,2,0,0,0)";
		//$DB->execute($sql_role_);

		//$sql_test="INSERT INTO mdl_role_assignments(roleid,contextid,userid,timemodified,modifierid,component,itemid,sortorder) VALUES(5,3,9999,1515753823,2,2,0,0)";
		//$DB->execute($sql_test);
  
		              	
		$data[] = array('user_enrolments_id' => $enrols->id,'context_id'=>$params);
                $result['meta']['code'] = '200';
                $result['meta']['message'] = 'success';
            } else {
                if ($enrols->timestart < $check->timestart || $check->timeend < $enrols->timestart) {
                    $sql = "UPDATE mdl_user_enrolments SET timestart = ".$enrols->timestart." 
                        WHERE id = ".$check->id;
                    $DB->execute($sql);
                }

                // if ((int)$check->timestart < time()) {
                //     $sql = "UPDATE mdl_user_enrolments SET timestart = ".$enrols->timestart." 
                //         WHERE id = ".$check->id;
                //     $DB->execute($sql);
                // }


		//$sql_role="insert into mdl_role_assignments(roleid,contextid,userid,timemodified,modifierid,component,itemid,sortorder) values(5,".$mdl_context_id.",".$user->id.",".time().",".2.",".$user->id.",2,2)";
		
		//$DB->execute($sql_role);


                if ($enrols->timeend > $check->timeend || $enrols->timeend == 0) {
                    $sql = "UPDATE mdl_user_enrolments SET timeend = ".$enrols->timeend." 
                        WHERE id = ".$check->id;
                    $DB->execute($sql);
                }
                // $DB->delete_records('user_enrolments', array('id' => $check->id));
                // $enrols->id = $DB->insert_record('user_enrolments', $enrols);
                // $data[] = array('user_enrolments_id' => $enrols->id);	
                
		
		//insert role_assignments
		//$sql_role_1="INSERT INTO mdl_role_assignments(roleid,contextid,userid,timemodified,modifierid,component,itemid,sortorder) VALUES(5,".$mdl_context_id.",".$user_role_id.",1515753823,2,0,0,0)";
		//$DB->execute($sql_role_1);


		//$sql_test="INSERT INTO mdl_role_assignments(roleid,contextid,userid,timemodified,modifierid,component,itemid,sortorder) VALUES(5,3,9999,1515753823,2,2,0,0)";
		//$DB->execute($sql_test);



		$data[] = array('user_enrolments_id' => $check->id,'context_id'=>$params);
                $result['meta']['code'] = '200';
                $result['meta']['message'] = 'data already exists';
            }
            $result['data'] = $data;
        } else {
            $data[] = array();
            $result['meta']['code'] = '400';
            $result['meta']['message'] = 'failed, data not completed';
            $result['data'] = $data;
        }
       
        return json_encode($result);
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function assign_enroll_course_returns() {
        return new external_value(PARAM_TEXT, 'Success Get URL');
    }

    /**
     * Mark the function as deprecated.
     * @return bool
     */
    public static function assign_enroll_course_is_deprecated() {
        return true;
    }

}
