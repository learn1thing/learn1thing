<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * URL Hit
 * http://course.dev/webservice/rest/server.php?wstoken=wstoken&wsfunction=local_get_course_list_dropdown_list&moodlewsrestformat=json
 *
 * Param    req[0][type]
 * Method   POST
 */

/**
 * External Web Service Template
 *
 * @package    localwstemplate
 * @copyright  2011 Moodle Pty Ltd (http://moodle.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once("../../config.php");
require_once($CFG->libdir . "/externallib.php");
// require_once("$CFG->libdir/externallib.php");

class local_get_course_list_dropdown_external extends external_api {

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function get_course_list_dropdown_parameters() {
        return new external_function_parameters(
            array(
                'req' => new external_multiple_structure(
                    new external_single_structure([
                        'email' => new external_value(PARAM_TEXT, 'email'),
                    ])
                )
            )
        );
    }

    /**
     * Returns Data Course
     * @return array Data Course
     */
    public static function get_course_list_dropdown($req) {
        // unset($req);
        global $CFG, $DB;
        require_once("$CFG->dirroot/group/lib.php");
        $params =  self::validate_parameters(self::get_course_list_dropdown_parameters(), array('req'=> $req));
        $data = array();
        # Get Data Course
        $sql = "SELECT r.shortname, u.username,u.email, c.id AS course_id, c.fullname AS coursename
                FROM mdl_role r
                JOIN mdl_role_assignments ra ON ra.roleid = r.id
                JOIN mdl_context ctx ON ctx.id = ra.contextid AND ctx.contextlevel = 50
                JOIN mdl_course c ON c.id = ctx.instanceid
                JOIN mdl_user u ON u.id = ra.userid
                WHERE u.email = '".$req[0]['email']."'";
        $courses = $DB->get_records_sql($sql);

        foreach ($courses as $value) {
            $data[] = array(
                    'id' => $value->course_id,
                    'text' => $value->coursename,
                );
        }

        if (count($data) == 0) {
            $data[] = array(
                    'id' => 000,
                    'text' => 'Dummy Data',
                );
        }
 
        $result['meta']['code'] = '200';
        $result['meta']['message'] = 'success';
        $result['data'] = $data;
        return json_encode($result);
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function get_course_list_dropdown_returns() {
        return new external_value(PARAM_TEXT, 'Success Get Data Course');
    }

    /**
     * Mark the function as deprecated.
     * @return bool
     */
    public static function get_course_list_dropdown_is_deprecated() {
        return true;
    }

}
