<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Web service local plugin template external functions and service definitions.
 *
 * @package    localwstemplate
 * @copyright  2011 Jerome Mouneyrac
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// We defined the web service functions to install.
$functions = array(
        'local_get_course_list' => array(
                'classname'   => 'local_get_course_external',
                'methodname'  => 'get_course',
                'classpath'   => 'local/get_course/externallib.php',
                'description' => 'Get Data Course',
                'type'        => 'read',
                'services'    => 'get_course',
                'shortname'    => 'get_course'
        )
);

// We define the services to install as pre-build services. A pre-build service is not editable by administrator.
$services = array(
        'get_course' => array(
                'functions' => array ('local_get_course_list'),
                'restrictedusers' => 0,
                // 'requiredcapability' => '',
                'enabled'=>1,
        )
);
