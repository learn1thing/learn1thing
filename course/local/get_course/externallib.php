<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * URL Hit
 * http://course.dev/webservice/rest/server.php?wstoken=wstoken&wsfunction=local_get_course_list&moodlewsrestformat=json
 *
 * Param    req[0][type]
 * Method   POST
 */

/**
 * External Web Service Template
 *
 * @package    localwstemplate
 * @copyright  2011 Moodle Pty Ltd (http://moodle.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once("../../config.php");
require_once($CFG->libdir . "/externallib.php");
// require_once("$CFG->libdir/externallib.php");

class local_get_course_external extends external_api {

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function get_course_parameters() {
        return new external_function_parameters(
            array(
                'req' => new external_multiple_structure(
                    new external_single_structure([
                        'type' => new external_value(PARAM_TEXT, 'type'),
                        'email' => new external_value(PARAM_TEXT, 'email'),
                    ])
                )
            )
        );
    }

    /**
     * Returns Data Course
     * @return array Data Course
     */
    public static function get_course($req) {
        // unset($req);
        global $CFG, $DB;
        require_once("$CFG->dirroot/group/lib.php");
        $params =  self::validate_parameters(self::get_course_parameters(), array('req'=> $req));
        $data = array();
        # Get Data Course
        $sql = "SELECT c.id, c.fullname
            FROM mdl_course c
            INNER JOIN mdl_course_profilers cp
            WHERE cp.email = '".$params['req'][0]['email']."'
            ORDER BY c.id ASC";
        $courses = $DB->get_records_sql($sql);

        foreach ($courses as $value) {
            $data[] = (array)$value;
        }
 
        $result['meta']['code'] = '200';
        $result['meta']['message'] = 'success';
        $result['data'] = $data;
        return json_encode($result);
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function get_course_returns() {
        return new external_value(PARAM_TEXT, 'Success Get Data Course');
    }

    /**
     * Mark the function as deprecated.
     * @return bool
     */
    public static function get_course_is_deprecated() {
        return true;
    }

}
