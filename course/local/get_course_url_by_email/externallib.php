<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * URL Hit
 * http://course.dev/webservice/rest/server.php?wstoken=wstoken&wsfunction=local_get_course_url_by_email_data&moodlewsrestformat=json
 *
 * Param    url[0][type]
 *          url[0][email]
 * Method   POST
 */

/**
 * External Web Service Template
 *
 * @package    localwstemplate
 * @copyright  2011 Moodle Pty Ltd (http://moodle.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once("../../config.php");
require_once($CFG->libdir . "/externallib.php");

class local_get_course_url_by_email_external extends external_api {

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function get_course_url_by_email_parameters() {
        return new external_function_parameters(
            array(
                'url' => new external_multiple_structure(
                    new external_single_structure([
                        'type' => new external_value(PARAM_TEXT, 'type'),
                        'email' => new external_value(PARAM_TEXT, 'email'),
                    ])
                )
            )
        );
    }

    /**
     * Returns welcome message
     * @return string welcome message
     */
    public static function get_course_url_by_email($url) {
        global $CFG, $DB;
        require_once("$CFG->dirroot/group/lib.php");
        //Parameter validation
        $params =  self::validate_parameters(self::get_course_url_by_email_parameters(), 
            array('url' => $url));
        $collect = array();
        $data = array();
        $idCourse = array();
        # Get Data Course

        $roleSql = "SELECT r.shortname FROM mdl_role r
                JOIN mdl_role_assignments ra ON ra.roleid = r.id 
                JOIN mdl_user u ON u.id = ra.userid
                WHERE u.email = '".$params['url'][0]['email']."' LIMIT 1";
        $roles = $DB->get_records_sql($roleSql);

        $role = '';
        foreach ($roles as $key => $value) {
            $role = $value->shortname;
            break;
        }
        
        if ($role == '') {
            $role = 'manager';
        }

        // $sql = "SELECT CASE WHEN u.email IS NULL THEN '-' ELSE u.email END AS email,
        //             CASE WHEN c.id IS NULL THEN 0 ELSE c.id END AS course_id,
        //             CASE WHEN c.fullname IS NULL THEN '-' ELSE c.fullname END AS course_name,
        //             CASE WHEN c.shortname IS NULL THEN 0 ELSE c.shortname END AS shortname,
        //             CASE WHEN cp.profiler_id IS NULL THEN '-' ELSE cp.profiler_id END AS profiler_id,
        //             CASE WHEN coach_id IS NULL THEN 0 ELSE coach_id END AS coach_id,
        //             CASE WHEN cp.intro IS NULL THEN '-' ELSE cp.intro END AS url,
        //             CASE WHEN cp.enrol_id IS NULL THEN 0 ELSE cp.enrol_id END AS enrol_id,
        //             CASE WHEN product_name IS NULL THEN '-' ELSE product_name END AS profiler_name
        //         FROM mdl_role r
        //         JOIN mdl_role_assignments ra ON ra.roleid = r.id
        //         JOIN mdl_context ctx ON ctx.id = ra.contextid
        //         JOIN mdl_course c ON c.id = ctx.instanceid
        //         JOIN mdl_user u ON u.id = ra.userid
        //         LEFT JOIN mdl_course_profilers cp ON u.email = cp.email WHERE r.shortname = '".$role."'";

        // if (strtolower($params['url'][0]['email']) != 'all') {
        //     $sql = $sql." AND u.email = '".$params['url'][0]['email']."'";
        // }
/*
        # Without Filtering
        $sql = "SELECT CASE WHEN cp.email IS NULL THEN '-' ELSE cp.email END AS email,
                    CASE WHEN c.id IS NULL THEN 0 ELSE c.id END AS course_id,
                    CASE WHEN c.fullname IS NULL THEN '-' ELSE c.fullname END AS course_name,
                    CASE WHEN c.shortname IS NULL THEN 0 ELSE c.shortname END AS shortname,
                    CASE WHEN cp.profiler_id IS NULL THEN '-' ELSE cp.profiler_id END AS profiler_id,
                    CASE WHEN coach_id IS NULL THEN 0 ELSE coach_id END AS coach_id,
                    CASE WHEN cp.intro IS NULL THEN '-' ELSE cp.intro END AS url,
                    CASE WHEN cp.enrol_id IS NULL THEN 0 ELSE cp.enrol_id END AS enrol_id,
                    CASE WHEN product_name IS NULL THEN '-' ELSE product_name END AS profiler_name
                FROM mdl_course c 
                LEFT JOIN mdl_course_profilers cp ON c.id = cp.course_id";

        # With Filtering
*/
        $sql = "SELECT CASE WHEN re.email IS NULL THEN '-' ELSE re.email END AS email,
                    CASE WHEN c.id IS NULL THEN 0 ELSE c.id END AS course_id,
                    CASE WHEN c.fullname IS NULL THEN '-' ELSE c.fullname END AS course_name,
                    CASE WHEN c.shortname IS NULL THEN 0 ELSE c.shortname END AS shortname,
                    CASE WHEN cp.profiler_id IS NULL THEN 0 ELSE cp.profiler_id END AS profiler_id,
                    CASE WHEN coach_id IS NULL THEN 0 ELSE coach_id END AS coach_id,
                    CASE WHEN cp.intro IS NULL THEN '-' ELSE cp.intro END AS url,
                    CASE WHEN cp.enrol_id IS NULL THEN 0 ELSE cp.enrol_id END AS enrol_id,
                    CASE WHEN product_name IS NULL THEN '-' ELSE product_name END AS profiler_name
                FROM mdl_course c 
                LEFT JOIN mdl_relation_course_users re ON c.id = re.course_id
                LEFT JOIN mdl_course_profilers cp ON c.id = cp.course_id
                WHERE re.email = '".$params['url'][0]['email']."'
                and cp.id is not null
                ORDER BY cp.id DESC";

        $courses = $DB->get_recordset_sql($sql);
        foreach ($courses as $key => $value) {
            $value->course_url = $CFG->wwwroot.'/course/view.php?name='.$value->shortname;
            $collect[] = (array) $value;
        }

        foreach ($collect as $key => $value) {
            if (!in_array($value['course_id'], $idCourse)) {
                $data[] = $value;
            }  
            $idCourse[] = $value['course_id'];
        }
        
        $result['meta']['code'] = '200';
        $result['meta']['message'] = 'success';
        $result['data'] = $data;
        return json_encode($result);
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function get_course_url_by_email_returns() {
        return new external_value(PARAM_TEXT, 'Success Get URL');
    }

    /**
     * Mark the function as deprecated.
     * @return bool
     */
    public static function get_course_url_by_email_is_deprecated() {
        return true;
    }

}
