<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * URL Hit
 * http://course.dev/webservice/rest/server.php?wstoken=wstoken&wsfunction=local_assign_unenroll_course_data&moodlewsrestformat=json
 *
 * Param    url[0][type]
 *          url[0][email]
 * Method   POST
 */

/**
 * External Web Service Template
 *
 * @package    localwstemplate
 * @copyright  2011 Moodle Pty Ltd (http://moodle.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once("../../config.php");
require_once($CFG->libdir . "/externallib.php");

class local_assign_unenroll_course_external extends external_api {

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function assign_unenroll_course_parameters() {
        return new external_function_parameters(
            array(
                'url' => new external_multiple_structure(
                    new external_single_structure([
                        'type' => new external_value(PARAM_TEXT, 'type'),
                        'course_id' => new external_value(PARAM_TEXT, 'course_id'),
                        'shortname' => new external_value(PARAM_TEXT, 'shortname'),
                        'enrol_id' => new external_value(PARAM_TEXT, 'enrol_id'),
                        'email' => new external_value(PARAM_TEXT, 'email'),
                        'user_enrolments_id' => new external_value(PARAM_TEXT, 'user_enrolments_id'),
                    ])
                )
            )
        );
    }

    /**
     * Returns welcome message
     * @return string welcome message
     */
    public static function assign_unenroll_course($url) {
        global $CFG, $DB;
        require_once("$CFG->dirroot/group/lib.php");
        //Parameter validation
        $params =  self::validate_parameters(self::assign_unenroll_course_parameters(), 
            array('url' => $url));
        $data = array();
        
        if (! $course_profilers = $DB->get_record('user_enrolments', array('id' => (int)$params['url'][0]['user_enrolments_id']))) {
            $data[] = array();
            $result['meta']['code'] = '400';
            $result['meta']['message'] = 'failed, data not found';
            $result['data'] = $data;
        } else {
            $DB->delete_records('user_enrolments', array('id' => (int)$params['url'][0]['user_enrolments_id']));
            $data[] = array();
            $result['meta']['code'] = '200';
            $result['meta']['message'] = 'success';
            $result['data'] = $data;
        }
        
        return json_encode($result);
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function assign_unenroll_course_returns() {
        return new external_value(PARAM_TEXT, 'Success Get URL');
    }

    /**
     * Mark the function as deprecated.
     * @return bool
     */
    public static function assign_unenroll_course_is_deprecated() {
        return true;
    }

}
