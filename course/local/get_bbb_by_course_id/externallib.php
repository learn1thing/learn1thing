<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * URL Hit
 * http://course.dev/webservice/rest/server.php?wstoken=wstoken&wsfunction=local_get_bbb_by_course_id_data&moodlewsrestformat=json
 *
 * Param    url[0][type]
 *          url[0][email]
 * Method   POST
 */

/**
 * External Web Service Template
 *
 * @package    localwstemplate
 * @copyright  2011 Moodle Pty Ltd (http://moodle.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once("../../config.php");
require_once($CFG->libdir . "/externallib.php");

class local_get_bbb_by_course_id_external extends external_api {

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function get_bbb_by_course_id_parameters() {
        return new external_function_parameters(
            array(
                'url' => new external_multiple_structure(
                    new external_single_structure([
                        'courseid' => new external_value(PARAM_TEXT, 'courseid'),
                    ])
                )
            )
        );
    }

    /**
     * Returns welcome message
     * @return string welcome message
     */
    public static function get_bbb_by_course_id($url) {
        global $CFG, $DB;
        require_once("$CFG->dirroot/group/lib.php");
        //Parameter validation
        $params =  self::validate_parameters(self::get_bbb_by_course_id_parameters(), 
            array('url' => $url));
        
        $sql = "SELECT *
                FROM mdl_bigbluebuttonbn 
                WHERE course = ".$params['url'][0]['courseid'];
        $data = array();
        $courses = $DB->get_recordset_sql($sql);

        foreach ($courses as $key => $value) {
            $value->course_url = $CFG->wwwroot.'/course/view.php?name='.$value->shortname;
            $data[] = (array) $value;
        }
        
        $result['meta']['code'] = '200';
        $result['meta']['message'] = 'success';
        $result['data'] = $data;
        return json_encode($result);

        return json_encode($result);
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function get_bbb_by_course_id_returns() {
        return new external_value(PARAM_TEXT, 'Success Get URL');
    }

    /**
     * Mark the function as deprecated.
     * @return bool
     */
    public static function get_bbb_by_course_id_is_deprecated() {
        return true;
    }

}
