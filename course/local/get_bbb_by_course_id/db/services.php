<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Web service local plugin template external functions and service definitions.
 *
 * @package    localwstemplate
 * @copyright  2011 Jerome Mouneyrac
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// We defined the web service functions to install.
$functions = array(
        'local_get_bbb_by_course_id_data' => array(
                'classname'   => 'local_get_bbb_by_course_id_external',
                'methodname'  => 'get_bbb_by_course_id',
                'classpath'   => 'local/get_bbb_by_course_id/externallib.php',
                'description' => 'Get BBB By Course Id',
                'type'        => 'Write',
                'services'    => 'get_bbb_by_course_id',
                'shortname'    => 'get_bbb_by_course_id',
        )
);

// We define the services to install as pre-build services. A pre-build service is not editable by administrator.
$services = array(
        'get_bbb_by_course_id' => array(
                'functions' => array ('local_get_bbb_by_course_id_data'),
                'restrictedusers' => 0,
                // 'requiredcapability' => '',
                'enabled'=>1,
        )
);
