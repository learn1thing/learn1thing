<?php
// HTTP
define('HTTP_SERVER', 'https://store.learn1thing.com/store/');

// HTTPS
define('HTTPS_SERVER', 'https://store.learn1thing.com/store/');

// DIR
define('DIR_APPLICATION', '/var/www/html/store/catalog/');
define('DIR_SYSTEM', '/var/www/html/store/system/');
define('DIR_IMAGE', '/var/www/html/store/image/');
define('DIR_LANGUAGE', '/var/www/html/store/catalog/language/');
define('DIR_TEMPLATE', '/var/www/html/store/catalog/view/theme/');
define('DIR_CONFIG', '/var/www/html/store/system/config/');
define('DIR_CACHE', '/var/www/html/store/system/storage/cache/');
define('DIR_DOWNLOAD', '/var/www/html/store/system/storage/download/');
define('DIR_LOGS', '/var/www/html/store/system/storage/logs/');
define('DIR_MODIFICATION', '/var/www/html/store/system/storage/modification/');
define('DIR_UPLOAD', '/var/www/html/store/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'secret');
define('DB_DATABASE', 'learn1thing_store');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

