<?php
namespace Cart;
class Cart {
	private $data = array();

	public function __construct($registry) {
		$this->config = $registry->get('config');
		$this->customer = $registry->get('customer');
		$this->session = $registry->get('session');
		$this->db = $registry->get('db');
		$this->tax = $registry->get('tax');
		$this->weight = $registry->get('weight');

		// Remove all the expired carts with no customer ID
		$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE customer_id = '0' AND date_added < DATE_SUB(NOW(), INTERVAL 1 HOUR)");

		if ($this->customer->getId()) {
			// We want to change the session ID on all the old items in the customers cart
			$this->db->query("UPDATE " . DB_PREFIX . "cart SET session_id = '" . $this->db->escape($this->session->getId()) . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");

			// Once the customer is logged in we want to update the customer ID on all items he has
			$cart_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE customer_id = '0' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");

			foreach ($cart_query->rows as $cart) {
				$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE cart_id = '" . (int)$cart['cart_id'] . "'");

				// The advantage of using $this->add is that it will check if the products already exist and increaser the quantity if necessary.
				
				$this->add($cart['product_id'], $cart['quantity'], json_decode($cart['option']), $cart['recurring_id'], $cart['subscription_id']);
			
			}
		}
	}

	public function getProducts() {
		$product_data = array();

		$cart_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");

		foreach ($cart_query->rows as $cart) {
			$stock = true;

			$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_store p2s LEFT JOIN " . DB_PREFIX . "product p ON (p2s.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND p2s.product_id = '" . (int)$cart['product_id'] . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.date_available <= NOW() AND p.status = '1'");

			if ($product_query->num_rows && ($cart['quantity'] > 0)) {
				$option_price = 0;
				$option_points = 0;
				$option_weight = 0;

				$option_data = array();

				foreach (json_decode($cart['option']) as $product_option_id => $value) {
					$option_query = $this->db->query("SELECT po.product_option_id, po.option_id, od.name, o.type FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_option_id = '" . (int)$product_option_id . "' AND po.product_id = '" . (int)$cart['product_id'] . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");

					if ($option_query->num_rows) {
						if ($option_query->row['type'] == 'select' || $option_query->row['type'] == 'radio' || $option_query->row['type'] == 'image') {
							$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$value . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

							if ($option_value_query->num_rows) {

				$option_subscription = array();
				if (strpos(strtolower($option_query->row['name']), 'monthly subscription') !== false) {
					$duration = 0;
					$frequency = '';
					$cycle2 = 0;
					$pos = strpos($option_value_query->row['name'], " months");
					if ($pos !== false) {
						$cycle2 = trim(substr($option_value_query->row['name'], 0, $pos));
					}
					$pos1 = strpos(strtolower($option_value_query->row['name']), "twice monthly");
					$pos2 = strpos(strtolower($option_value_query->row['name']), "once monthly");
					if ($pos1 !== false) {
						$frequency = "semi_month";
						$duration = $cycle2 * 2;
					} elseif ($pos2 !== false) {
						$frequency = "month";
						$duration = $cycle2;
					}
					$option_subscription[] = array(
						'single_sale'	=> 1,
						'duration'		=> $duration,
						'frequency'		=> $frequency,
						'cycle'			=> 1
					);
				}
			
								if ($option_value_query->row['price_prefix'] == '+') {
									$option_price += $option_value_query->row['price'];
								} elseif ($option_value_query->row['price_prefix'] == '-') {
									$option_price -= $option_value_query->row['price'];
								}

								if ($option_value_query->row['points_prefix'] == '+') {
									$option_points += $option_value_query->row['points'];
								} elseif ($option_value_query->row['points_prefix'] == '-') {
									$option_points -= $option_value_query->row['points'];
								}

								if ($option_value_query->row['weight_prefix'] == '+') {
									$option_weight += $option_value_query->row['weight'];
								} elseif ($option_value_query->row['weight_prefix'] == '-') {
									$option_weight -= $option_value_query->row['weight'];
								}

								if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $cart['quantity']))) {
									$stock = false;
								}

								$option_data[] = array(
									'product_option_id'       => $product_option_id,
									'product_option_value_id' => $value,
									'option_id'               => $option_query->row['option_id'],
									'option_value_id'         => $option_value_query->row['option_value_id'],
									'name'                    => $option_query->row['name'],
									'value'                   => $option_value_query->row['name'],
									'type'                    => $option_query->row['type'],
									'quantity'                => $option_value_query->row['quantity'],
									'subtract'                => $option_value_query->row['subtract'],
									'price'                   => $option_value_query->row['price'],
									'price_prefix'            => $option_value_query->row['price_prefix'],
									'points'                  => $option_value_query->row['points'],
									'points_prefix'           => $option_value_query->row['points_prefix'],
									'weight'                  => $option_value_query->row['weight'],
									'weight_prefix'           => $option_value_query->row['weight_prefix']
								);
							}
						} elseif ($option_query->row['type'] == 'checkbox' && is_array($value)) {
							foreach ($value as $product_option_value_id) {
								$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$product_option_value_id . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

								if ($option_value_query->num_rows) {
									if ($option_value_query->row['price_prefix'] == '+') {
										$option_price += $option_value_query->row['price'];
									} elseif ($option_value_query->row['price_prefix'] == '-') {
										$option_price -= $option_value_query->row['price'];
									}

									if ($option_value_query->row['points_prefix'] == '+') {
										$option_points += $option_value_query->row['points'];
									} elseif ($option_value_query->row['points_prefix'] == '-') {
										$option_points -= $option_value_query->row['points'];
									}

									if ($option_value_query->row['weight_prefix'] == '+') {
										$option_weight += $option_value_query->row['weight'];
									} elseif ($option_value_query->row['weight_prefix'] == '-') {
										$option_weight -= $option_value_query->row['weight'];
									}

									if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $cart['quantity']))) {
										$stock = false;
									}

									$option_data[] = array(
										'product_option_id'       => $product_option_id,
										'product_option_value_id' => $product_option_value_id,
										'option_id'               => $option_query->row['option_id'],
										'option_value_id'         => $option_value_query->row['option_value_id'],
										'name'                    => $option_query->row['name'],
										'value'                   => $option_value_query->row['name'],
										'type'                    => $option_query->row['type'],
										'quantity'                => $option_value_query->row['quantity'],
										'subtract'                => $option_value_query->row['subtract'],
										'price'                   => $option_value_query->row['price'],
										'price_prefix'            => $option_value_query->row['price_prefix'],
										'points'                  => $option_value_query->row['points'],
										'points_prefix'           => $option_value_query->row['points_prefix'],
										'weight'                  => $option_value_query->row['weight'],
										'weight_prefix'           => $option_value_query->row['weight_prefix']
									);
								}
							}
						} elseif ($option_query->row['type'] == 'text' || $option_query->row['type'] == 'textarea' || $option_query->row['type'] == 'file' || $option_query->row['type'] == 'date' || $option_query->row['type'] == 'datetime' || $option_query->row['type'] == 'time') {
							$option_data[] = array(
								'product_option_id'       => $product_option_id,
								'product_option_value_id' => '',
								'option_id'               => $option_query->row['option_id'],
								'option_value_id'         => '',
								'name'                    => $option_query->row['name'],
								'value'                   => $value,
								'type'                    => $option_query->row['type'],
								'quantity'                => '',
								'subtract'                => '',
								'price'                   => '',
								'price_prefix'            => '',
								'points'                  => '',
								'points_prefix'           => '',
								'weight'                  => '',
								'weight_prefix'           => ''
							);
						}
					}
				}

				$price = $product_query->row['price'];

				// Product Discounts
				$discount_quantity = 0;

				foreach ($cart_query->rows as $cart_2) {
					if ($cart_2['product_id'] == $cart['product_id']) {
						$discount_quantity += $cart_2['quantity'];
					}
				}

				$product_discount_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$cart['product_id'] . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND quantity <= '" . (int)$discount_quantity . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity DESC, priority ASC, price ASC LIMIT 1");

				if ($product_discount_query->num_rows) {
					$price = $product_discount_query->row['price'];
				}

				// Product Specials
				$product_special_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$cart['product_id'] . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY priority ASC, price ASC LIMIT 1");

				if ($product_special_query->num_rows) {
					$price = $product_special_query->row['price'];
				}

				// Reward Points
				$product_reward_query = $this->db->query("SELECT points FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$cart['product_id'] . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "'");

				if ($product_reward_query->num_rows) {
					$reward = $product_reward_query->row['points'];
				} else {
					$reward = 0;
				}

				// Downloads
				$download_data = array();

				$download_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_download p2d LEFT JOIN " . DB_PREFIX . "download d ON (p2d.download_id = d.download_id) LEFT JOIN " . DB_PREFIX . "download_description dd ON (d.download_id = dd.download_id) WHERE p2d.product_id = '" . (int)$cart['product_id'] . "' AND dd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

				foreach ($download_query->rows as $download) {
					$download_data[] = array(
						'download_id' => $download['download_id'],
						'name'        => $download['name'],

				'subscription'		=> $download['subscription'],
				'subscription_id'	=> $cart['subscription_id'],
			
						'filename'    => $download['filename'],
						'mask'        => $download['mask']
					);
				}

				// Stock
				if (!$product_query->row['quantity'] || ($product_query->row['quantity'] < $cart['quantity'])) {
					$stock = false;
				}

				$subscription = false;
				$subscription_frequency = 0;
				$subscription_price = 0;
				$subscription_cycle = 0;
				$subscription_discount = 0;
				$subscription_discount_type = 0;
				$subscription_duration = 0;
				$subscription_specific_day = '';
				$subscription_status = 0;
				$subscription_trial = 0;
				$subscription_trial_only = 0;
				$subscription_trial_status = 0;
				$subscription_trial_price = 0;
				$subscription_trial_cycle = 0;
				$subscription_trial_duration = 0;
				$subscription_trial_frequency = 0;
				$include_taxes = 0;
				$include_shipping = 0;
				$days_diff = 0;
				if ($cart['subscription_id']) {
					$subscription_info = $this->db->query("SELECT s.*, p2s.price FROM " . DB_PREFIX . "subscriptions s LEFT JOIN " . DB_PREFIX . "product_to_subscription p2s ON (s.subscription_id = p2s.subscription_id) WHERE p2s.product_id = '" . (int)$product_query->row['product_id'] . "' AND s.subscription_id = '" . (int)$cart['subscription_id'] . "' AND (s.status = '1' or (s.trial = '1' AND s.trial_status = '1'))");
					if ($subscription_info) {
						$subscription = true;
						if (!isset($this->session->data['renewal'])) {
							$subscription_trial = $subscription_info->row['trial'];
							$subscription_trial_only = $subscription_info->row['trial_only'];
							$subscription_trial_status = $subscription_info->row['trial_status'];
							$subscription_trial_price = $subscription_info->row['trial_price'];
							$subscription_trial_cycle = $subscription_info->row['trial_cycle'];
							$subscription_trial_frequency = $subscription_info->row['trial_frequency'];
							$subscription_trial_duration = $subscription_info->row['trial_duration'];
						}
						if ($subscription_info->row['price'] > 0) {
							$subscription_price = $subscription_info->row['price'];
						} else {
							if ($product_special_query->num_rows) {
								$subscription_price = $product_special_query->row['price'];
							} elseif ($product_discount_query->num_rows) {
								$subscription_price = $product_discount_query->row['price'];
							} else {
								$subscription_price = $product_query->row['price'];
							}
							if (is_array($option_price)) {
								ksort($option_price);
								foreach ($option_price as $operations) {
									foreach ($operations as $operation => $value) {
										if ($operation == '=') {
											if ($subscription_price != 0) {
												$subscription_price = $value;
											} else {
												$subscription_price += $value;
											}
										} elseif ($operation == '+') {
											$subscription_price += $value;
										} elseif ($operation == '-') {
											$subscription_price -= $value;
										} elseif ($operation == '*') {
											$subscription_price = $subscription_price * $value;
										}
									}
								}
							} else {
								$subscription_price += $option_price;
							}
						}
						$subscription_cycle = $subscription_info->row['cycle'];
						$subscription_frequency = $subscription_info->row['frequency'];
						$subscription_duration = $subscription_info->row['duration'];
						$subscription_specific_day = $subscription_info->row['specific_day'];
						$subscription_status = $subscription_info->row['status'];
						$subscription_discount = $subscription_info->row['discount'];
						$subscription_discount_type = $subscription_info->row['discount_type'];
						$include_taxes = $subscription_info->row['include_taxes'];
						$include_shipping = $subscription_info->row['include_shipping'];
						if ($subscription_trial) {
							$price = $subscription_trial_price;
						}
						if ($subscription_discount) {
							$discount_amt = 0;
							if ($subscription_discount_type == "Percent") {
								$discount_amt = number_format(($subscription_price * $subscription_discount) / 100, 2);
								$subscription_price -= $discount_amt;
							} else {
								$discount_amt = $subscription_discount;
								$subscription_price -= $discount_amt;
							}
							if (!$subscription_trial) {
								$price = $subscription_price;
							}
						}
						$start_date = time();
						if (isset($subscription_info->row['subscription_specific_day']) && $subscription_info->row['subscription_specific_day']) {
							if (strtolower(date('l', time())) != $subscription_info->row['subscription_specific_day']) {
								for ($z = 1; $z <= 7; $z++) {
									$start_date += 86400;
									if (strtolower(date('l', time())) == $subscription_info->row['subscription_specific_day']) {
										if ($z < 7) {
											$start_date += (86400 * (7 - $z));
										}
										break;
									}
								}
								$days_diff = floor(($start_date - time()) / 86400);
							}
						} elseif (isset($this->session->data['choose_payment_date'][$cart['cart_id']])) {
							$day = $this->session->data['choose_payment_date'][$cart['cart_id']];
							$month = date('n', time());
							$year = date('Y', time());
							$start_date = strtotime($year . '-' . $month . '-' . $day);
							if ($start_date < time()) {
								if ($month == 12) {
									$month = 1;
									$year = date('Y', strtotime('+1 year'));
								} else {
									$month++;
								}
								$start_date = time();
								$real_start_date = strtotime($year . '-' . $month . '-' . $day);
								$days_diff = floor((($real_start_date - $start_date) / 86400) + 1);
							}
						}
					}
				} elseif (isset($option_subscription) && $option_subscription) {
					foreach ($option_subscription as $opt) {
						$subscription = true;
						$subscription_price = $price + $option_price;
						$subscription_cycle = $opt['cycle'];
						$subscription_frequency = $opt['frequency'];
						$subscription_duration = $opt['duration'];
						$subscription_status = 1;
					}
				}
				if ($subscription) {
					$recurring = array(
						'recurring_id'    => $cart['subscription_id'],
						'name'            => '',
						'frequency'       => $subscription_frequency,
						'price'           => $subscription_price,
						'cycle'           => $subscription_cycle,
						'duration'        => $subscription_duration,
						'trial'           => $subscription_trial_status,
						'trial_frequency' => $subscription_trial_frequency,
						'trial_only'	  => $subscription_trial_only,
						'trial_price'     => $subscription_trial_price,
						'trial_cycle'     => $subscription_trial_cycle,
						'trial_duration'  => $subscription_trial_duration
					);
				} else {
					$recurring = false;
				}
				$pro_rated = 0;
				$pro_rated_amount = 0;
				if (!$subscription_trial) {
					if ($subscription_price && !$days_diff) {
						$price = $subscription_price;
					} elseif ($subscription_price && $days_diff) {
						$days_in_month = date('t', time());
						$pro_rated_amount = $subscription_price / $days_in_month;
						$price = $pro_rated_amount * $days_diff;
						$pro_rated = 1;
					} else {
						if (!isset($price)) {
							$price = 0;
						}
						if (is_array($option_price)) {
							ksort($option_price);
							foreach ($option_price as $operations) {
								foreach ($operations as $operation => $value) {
									if ($operation == '=') {
										if ($price != 0) {
											$price = $value;
										} else {
											$price += $value;
										}
									} elseif ($operation == '+') {
										$price += $value;
									} elseif ($operation == '-') {
										$price -= $value;
									} elseif ($operation == '*') {
										$price = $price * $value;
									}
								}
							}
						} else {
							$price += $option_price;
						}
					}
				}
			

				$recurring_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "recurring r LEFT JOIN " . DB_PREFIX . "product_recurring pr ON (r.recurring_id = pr.recurring_id) LEFT JOIN " . DB_PREFIX . "recurring_description rd ON (r.recurring_id = rd.recurring_id) WHERE r.recurring_id = '" . (int)$cart['recurring_id'] . "' AND pr.product_id = '" . (int)$cart['product_id'] . "' AND rd.language_id = " . (int)$this->config->get('config_language_id') . " AND r.status = 1 AND pr.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "'");

				
			

















				$product_data[] = array(
					'cart_id'         => $cart['cart_id'],
					'product_id'      => $product_query->row['product_id'],
					'name'            => $product_query->row['name'],
					'model'           => $product_query->row['model'],
					'shipping'        => $product_query->row['shipping'],
					'image'           => $product_query->row['image'],
					'option'          => $option_data,
					'download'        => $download_data,
					'quantity'        => $cart['quantity'],
					'minimum'         => $product_query->row['minimum'],
					'subtract'        => $product_query->row['subtract'],
					'stock'           => $stock,
					'price'           
				=> $price,
			
					'total'           
				=> $price * $cart['quantity'],
			
					'reward'          => $reward * $cart['quantity'],
					'points'          => ($product_query->row['points'] ? ($product_query->row['points'] + $option_points) * $cart['quantity'] : 0),
					'tax_class_id'    => $product_query->row['tax_class_id'],
					'weight'          => ($product_query->row['weight'] + $option_weight) * $cart['quantity'],
					'weight_class_id' => $product_query->row['weight_class_id'],
					'length'          => $product_query->row['length'],
					'width'           => $product_query->row['width'],
					'height'          => $product_query->row['height'],

				'single_sale'					=> $product_query->row['single_sale'],
				'pro_rated'						=> $pro_rated,
				'choose_payment_date'			=> (isset($this->session->data['choose_payment_date'][$cart['cart_id']]) ? $this->session->data['choose_payment_date'][$cart['cart_id']] : ''),
				'subscription'					=> $subscription,
				'subscription_id'				=> $cart['subscription_id'],
				'subscription_trial'			=> $subscription_trial,
				'subscription_trial_only'		=> $subscription_trial_only,
				'subscription_trial_status'		=> $subscription_trial_status,
				'subscription_trial_cycle'		=> $subscription_trial_cycle,
				'subscription_trial_frequency'	=> $subscription_trial_frequency,
				'subscription_trial_duration'	=> $subscription_trial_duration,
				'subscription_trial_price'		=> $subscription_trial_price,
				'subscription_price'			=> $subscription_price,
				'subscription_cycle'			=> $subscription_cycle,
				'subscription_frequency'		=> $subscription_frequency,
				'subscription_duration'			=> $subscription_duration,
				'subscription_specific_day'		=> $subscription_specific_day,
				'subscription_status'			=> $subscription_status,
				'subscription_discount'			=> $subscription_discount,
				'subscription_discount_type'	=> $subscription_discount_type,
				'include_taxes'					=> ($this->config->get('product_subscribe_add_taxes') ? $this->config->get('product_subscribe_add_taxes') : $include_taxes),
				'include_shipping'				=> ($this->config->get('product_subscribe_add_shipping') ? $this->config->get('product_subscribe_add_shipping') : $include_shipping),
			
					'length_class_id' => $product_query->row['length_class_id'],
					'recurring'       => $recurring
				);
			} else {
				$this->remove($cart['cart_id']);
			}
		}

		return $product_data;
	}

	
				public function add($product_id, $quantity = 1, $option = array(), $recurring_id = 0, $subscription_id = 0, $choose_payment_date = '') {
			
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "' AND product_id = '" . (int)$product_id . "' AND recurring_id = '" . (int)$recurring_id . "' AND `option` = '" . $this->db->escape(json_encode($option)) . "'");


				$cart_id = $this->db->getLastId();
				if ($choose_payment_date) {
					$this->session->data['choose_payment_date'][$cart_id] = $choose_payment_date;
				}
			
		if (!$query->row['total']) {
			
				$this->db->query("INSERT " . DB_PREFIX . "cart SET customer_id = '" . (int)$this->customer->getId() . "', session_id = '" . $this->db->escape($this->session->getId()) . "', product_id = '" . (int)$product_id . "', recurring_id = '" . (int)$recurring_id . "', subscription_id = '" . (int)$subscription_id . "', `option` = '" . $this->db->escape(json_encode($option)) . "', quantity = '" . (int)$quantity . "', date_added = NOW()");
			
		} else {
			
				$this->db->query("UPDATE " . DB_PREFIX . "cart SET quantity = (quantity + " . (int)$quantity . ") WHERE customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "' AND product_id = '" . (int)$product_id . "' AND recurring_id = '" . (int)$recurring_id . "' AND subscription_id = '" . (int)$subscription_id . "' AND `option` = '" . $this->db->escape(json_encode($option)) . "'");
			
		}
	}

	public function update($cart_id, $quantity) {
		$this->db->query("UPDATE " . DB_PREFIX . "cart SET quantity = '" . (int)$quantity . "' WHERE cart_id = '" . (int)$cart_id . "' AND customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
	}

	public function remove($cart_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE cart_id = '" . (int)$cart_id . "' AND customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
	}

	public function clear() {
		$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
	}

	public function getRecurringProducts() {
		$product_data = array();

		foreach ($this->getProducts() as $value) {
			if ($value['recurring']) {
				$product_data[] = $value;
			}
		}

		return $product_data;
	}

	public function getWeight() {
		$weight = 0;

		foreach ($this->getProducts() as $product) {
			if ($product['shipping']) {
				$weight += $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id'));
			}
		}

		return $weight;
	}

	public function getSubTotal() {
		$total = 0;

		foreach ($this->getProducts() as $product) {
			$total += $product['total'];
		}

		return $total;
	}

	public function getTaxes() {
		$tax_data = array();

		foreach ($this->getProducts() as $product) {
			if ($product['tax_class_id']) {
				$tax_rates = $this->tax->getRates($product['price'], $product['tax_class_id']);

				foreach ($tax_rates as $tax_rate) {
					if (!isset($tax_data[$tax_rate['tax_rate_id']])) {
						$tax_data[$tax_rate['tax_rate_id']] = ($tax_rate['amount'] * $product['quantity']);
					} else {
						$tax_data[$tax_rate['tax_rate_id']] += ($tax_rate['amount'] * $product['quantity']);
					}
				}
			}
		}

		return $tax_data;
	}

	public function getTotal() {
		$total = 0;

		foreach ($this->getProducts() as $product) {
			$total += $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'];
		}

		return $total;
	}

	public function countProducts() {
		$product_total = 0;

		$products = $this->getProducts();

		foreach ($products as $product) {
			$product_total += $product['quantity'];
		}

		return $product_total;
	}

	public function hasProducts() {
		return count($this->getProducts());
	}

	public function hasRecurringProducts() {
		return count($this->getRecurringProducts());
	}

	public function hasStock() {
		foreach ($this->getProducts() as $product) {
			if (!$product['stock']) {
				return false;
			}
		}

		return true;
	}

	public function hasShipping() {
		foreach ($this->getProducts() as $product) {
			if ($product['shipping']) {
				return true;
			}
		}

		return false;
	}

	public function hasDownload() {
		foreach ($this->getProducts() as $product) {
			if ($product['download']) {
				return true;
			}
		}

		return false;
	}
}
