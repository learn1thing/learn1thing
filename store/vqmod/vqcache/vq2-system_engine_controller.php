<?php
abstract class Controller {
	protected $registry;

	public function __construct($registry) {
		$this->registry = $registry;
	}

	public function __get($key) {
		return $this->registry->get($key);
	}


				protected function hasAction($child, $args = array()) {
					$action = new Action($child, $args);
					if (file_exists($action->getFile())) {
						require_once(\VQMod::modCheck($action->getFile()));
						$class = $action->getClass();
						$controller = new $class($this->registry);
						if (method_exists($controller, $action->getMethod())) {
							return true;
						} else {
							return false;
						}
					} else {
						return false;				
					}		
				}
			
	public function __set($key, $value) {
		$this->registry->set($key, $value);
	}
}