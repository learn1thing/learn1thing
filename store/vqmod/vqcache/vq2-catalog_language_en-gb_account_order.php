<?php
// Heading
$_['heading_title']         = 'Order History';

$_['text_invoice']                            = 'Invoice';
$_['text_email']                              = 'E-Mail:';
$_['text_telephone']                          = 'Telephone:';
$_['text_fax']                                = 'Fax:';
$_['text_website']                            = 'Web Site:';
$_['text_to']                                 = 'Invoice Address';
$_['text_ship_to']                            = 'Delivery Address';
$_['button_invoice_print']          		  = 'Print Invoice';
			

// Text

				$_['text_subscription']				= 'This order contains subscription products';
				$_['text_sub_heading']				= 'Subscriptions';
				$_['text_confirm_cancel']			= 'If you cancel this subscription, you will not be able to renew it from here.  You will have to purchase the subscription again.  Are you sure you want to cancel this subscription?';
				$_['text_confirm_pause']			= 'Are you sure you want to suspend this subscription?';
				$_['text_confirm_restart']			= 'Are you sure you want to reactivate this subscription?';
				$_['text_cancel_success']			= 'You have successfully cancelled your subscription';
				$_['text_pause_success']			= 'You have successfully suspended your subscription';
				$_['text_restart_success']			= 'You have successfully reactivated your subscription';
				$_['text_cancel_failed']			= 'The subscription could not be cancelled.  Please contact the website administrator for assistance.';
				$_['text_pause_failed']				= 'The subscription could not be suspended.  Please contact the website administrator for assistance.';
				$_['text_restart_failed']			= 'The subscription could not be reactivated.  Please contact the website administrator for assistance.';
				$_['text_cancel_subscription']		= 'Subscription Cancellation';
				$_['text_pause_subscription']		= 'Subscription Suspended';
				$_['text_restart_subscription']		= 'Subscription Reactivated';
				$_['text_cancel_message']			= 'Subscription ID %s has been cancelled.';
				$_['text_pause_message']			= 'Subscription ID %s has been suspended.';
				$_['text_restart_message']			= 'Subscription ID %s has been reactivated.';
				$_['text_cancel_client']			= 'You have successfully cancelled your subscription for the %s.  If you have any questions or would like to renew this subscription, please <a href="mailto:%s">contact us</a>';
				$_['text_pause_client']				= 'You have successfully suspended your subscription for the %s.  If you have any questions, please <a href="mailto:%s">contact us</a>';
				$_['text_restart_client']			= 'You have successfully reactivated your subscription for the %s.  If you have any questions, please <a href="mailto:%s">contact us</a>';
				$_['text_expired']					= 'Expired';
				$_['text_trial']					= 'Trial Period:';
				$_['text_renew']					= 'Renew';
				$_['text_change']					= 'Change Plan';
				$_['text_renewal_period']			= 'Renewal Options';
				$_['text_savings']					= 'You Save';
				$_['text_cancel']					= 'Cancel';
				$_['text_pause']					= 'Suspend';
				$_['text_restart']					= 'Reactivate';
				$_['column_trial']					= 'Trial Ends';
				$_['column_terms']					= 'Current Term';
				$_['column_term_amount']			= 'Term Cost';
				$_['column_renewal_amount']			= 'Renewal Cost';
				$_['column_end_date']				= 'Expires';
				$_['column_next_due']				= 'Next Due';
				$_['column_action']					= 'Actions';
				$_['text_renew_discount_duration']	= '%s %s(s) @ %s for %s payment(s)';
				$_['text_renew_discount']			= '%s %s(s) @ %s until cancelled';
				$_['text_renew_duration']			= '%s %s(s) @ %s for %s payment(s)';
				$_['text_renew']					= '%s %s(s) @ %s until cancelled';
			
$_['text_account']          = 'Account';
$_['text_order']            = 'Order Information';
$_['text_order_detail']     = 'Order Details';
$_['text_invoice_no']       = 'Invoice No.:';
$_['text_order_id']         = 'Order ID:';
$_['text_date_added']       = 'Date Added:';
$_['text_shipping_address'] = 'Shipping Address';
$_['text_shipping_method']  = 'Shipping Method:';
$_['text_payment_address']  = 'Payment Address';
$_['text_payment_method']   = 'Payment Method:';
$_['text_comment']          = 'Order Comments';
$_['text_history']          = 'Order History';
$_['text_success']          = 'Success: You have added <a href="%s">%s</a> to your <a href="%s">shopping cart</a>!';
$_['text_empty']            = 'You have not made any previous orders!';
$_['text_error']            = 'The order you requested could not be found!';

// Column
$_['column_order_id']       = 'Order ID';
$_['column_customer']       = 'Customer';
$_['column_product']        = 'No. of Products';
$_['column_name']           = 'Product Name';
$_['column_model']          = 'Model';
$_['column_quantity']       = 'Quantity';
$_['column_price']          = 'Price';
$_['column_total']          = 'Total';
$_['column_action']         = 'Action';
$_['column_date_added']     = 'Date Added';
$_['column_status']         = 'Status';
$_['column_comment']        = 'Comment';

// Error
$_['error_reorder']         = '%s is not currently available to be reordered.';