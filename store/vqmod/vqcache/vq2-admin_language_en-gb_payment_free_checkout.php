<?php
// Heading
$_['heading_title']		 = 'Free Checkout';

// Text

				$_['entry_product_subscribe']		= 'Use with Product Subscription';
				$_['entry_recurring_cancel_status']	= 'Allow Customer to Cancel';
				$_['entry_recurring_pause_status']	= 'Allow Customer to Suspend';
				$_['text_yes']						= 'Yes';
				$_['text_no'] 						= 'No';
			
$_['text_payment']		 = 'Payment';
$_['text_success']		 = 'Success: You have modified Free Checkout payment module!';
$_['text_edit']          = 'Edit Free Checkout';

// Entry
$_['entry_order_status'] = 'Order Status';
$_['entry_status']		 = 'Status';
$_['entry_sort_order']	 = 'Sort Order';

// Error
$_['error_permission']	  = 'Warning: You do not have permission to modify payment Free Checkout!';