<?php
class ControllerAccountOrder extends Controller {
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('account/order');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);
		
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/order', $url, true)
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_empty'] = $this->language->get('text_empty');

		$data['column_order_id'] = $this->language->get('column_order_id');

				$data['text_subscription'] = $this->language->get('text_subscription');
			
		$data['column_customer'] = $this->language->get('column_customer');
		$data['column_product'] = $this->language->get('column_product');
		$data['column_total'] = $this->language->get('column_total');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_date_added'] = $this->language->get('column_date_added');

		$data['button_view'] = $this->language->get('button_view');
		$data['button_continue'] = $this->language->get('button_continue');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['orders'] = array();

		$this->load->model('account/order');

		$order_total = $this->model_account_order->getTotalOrders();

		$results = $this->model_account_order->getOrders(($page - 1) * 10, 10);

		foreach ($results as $result) {

				$subscriptions = $this->model_account_order->getSubscriptionCount($result['order_id']);
			
			$product_total = $this->model_account_order->getTotalOrderProductsByOrderId($result['order_id']);
			$voucher_total = $this->model_account_order->getTotalOrderVouchersByOrderId($result['order_id']);

			$data['orders'][] = array(
				'order_id'   => $result['order_id'],

				'subscription'	 => $subscriptions,
			
				'name'       => $result['firstname'] . ' ' . $result['lastname'],
				'status'     => $result['status'],
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'products'   => ($product_total + $voucher_total),
				'total'      => $this->currency->format($result['total'], $result['currency_code'], $result['currency_value']),
				'view'       => $this->url->link('account/order/info', 'order_id=' . $result['order_id'], true),
			);
		}

		$pagination = new Pagination();
		$pagination->total = $order_total;
		$pagination->page = $page;
		$pagination->limit = 10;
		$pagination->url = $this->url->link('account/order', 'page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($order_total - 10)) ? $order_total : ((($page - 1) * 10) + 10), $order_total, ceil($order_total / 10));

		$data['continue'] = $this->url->link('account/account', '', true);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

//		$this->response->setOutput($this->load->view('account/order_list', $data));
		header("Location: https://store.learn1thing.com/dashboard/admin/order/order-history");
	}


				public function cancel() {
					$this->load->language('account/order');
					$this->load->model('account/order');
					$order_subscription_id = $this->request->post['order_subscription_id'];
					$results = $this->model_account_order->cancelSubscription($order_subscription_id);
					$order_id = 0;
					$recurring_id = 0;
					$product_id = 0;
					$product_name = '';
					$order_info = array();
					$result = '';
					foreach ($results as $result) {
						$order_id = $result['order_id'];
						$recurring_id = $result['subscription_id'];
						$product_id = $result['product_id'];
						$product_name = $this->model_account_order->getProductName($result['product_id']);
						$order_info = $this->model_account_order->getOrder($order_id);
					}
					$profile_query = $this->db->query("SELECT order_recurring_id, reference FROM `" . DB_PREFIX . "order_recurring` WHERE order_id = '" . (int)$order_id . "' AND recurring_id = '" . (int)$recurring_id . "' AND product_id = '" . (int)$product_id . "'");
					if ($profile_query->row['reference']) {
						if ($order_info['payment_code'] == 'pp_express') {
							$this->load->model('payment/pp_express');
							$result = $this->model_payment_pp_express->recurringCancel($profile_query->row['reference']);
						} elseif ($order_info['payment_code'] == 'authorizenet_aim_arb') {
							$this->load->model('payment/authorizenet_aim_arb');
							$result = $this->model_payment_authorizenet_aim_arb->recurringCancel($profile_query->row['order_recurring_id']);
						}
					} else {
						$result = 9999;
					}
					if ((is_array($result) && $result['ACK'] == 'Success') || $result == 9999) {
						$this->db->query("UPDATE `" . DB_PREFIX . "order_subscriptions` SET active = '0', next_due = '0' WHERE order_subscription_id = '" . (int)$order_subscription_id . "'");
						$subject = $this->language->get('text_cancel_subscription');
						$message = sprintf($this->language->get('text_cancel_message'), $order_subscription_id);
						$client_message = sprintf($this->language->get('text_cancel_client'), $product_name, $this->config->get('config_email'));
						// Store email
						$mail = new Mail();
						$mail->protocol = $this->config->get('config_mail_protocol');
						$mail->parameter = $this->config->get('config_mail_parameter');
						$mail->smtp_hostname = $this->config->get('config_mail_smtp_host');
						$mail->smtp_username = $this->config->get('config_mail_smtp_username');
						$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
						$mail->smtp_port = $this->config->get('config_mail_smtp_port');
						$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
			
						$mail->setTo($this->config->get('config_email'));
						$mail->setFrom($this->config->get('config_email'));
						$mail->setReplyTo($order_info['email']);
						$mail->setSender($order_info['store_name']);
						$mail->setSubject($subject);
						$mail->setHtml($message);
						$mail->send();
						// Client email
						$mail->setTo($order_info['email']);
						$mail->setHtml($client_message);
						$mail->send();
						$success = $this->language->get('text_cancel_success');
					} else {
						$success = $this->language->get('text_cancel_failed');
					}
					$json = array(
						'message'	=> $success,
						'href'		=> str_replace("&amp;", "&", $this->url->link('account/order/info', 'order_id=' . $order_id, true))
					);
					echo json_encode($json);
				}
				public function pause() {
					$this->load->language('account/order');
					$this->load->model('account/order');
					$order_subscription_id = $this->request->post['order_subscription_id'];
					$results = $this->model_account_order->cancelSubscription($order_subscription_id);
					$order_id = 0;
					$recurring_id = 0;
					$product_id = 0;
					$product_name = '';
					$order_info = array();
					$result = '';
					foreach ($results as $result) {
						$order_id = $result['order_id'];
						$recurring_id = $result['subscription_id'];
						$product_id = $result['product_id'];
						$product_name = $this->model_account_order->getProductName($result['product_id']);
						$order_info = $this->model_account_order->getOrder($order_id);
					}
					$profile_query = $this->db->query("SELECT order_recurring_id, reference, status FROM " . DB_PREFIX . "order_recurring WHERE order_id = '" . (int)$order_id . "' AND recurring_id = '" . (int)$recurring_id . "' AND product_id = '" . (int)$product_id . "'");
					if ($profile_query->num_rows && $profile_query->row['reference']) {
						if ($order_info['payment_code'] == 'pp_express') {
							$this->load->model('payment/pp_express');
							$result = $this->model_payment_pp_express->recurringPause($profile_query->row['reference']);
						} elseif ($order_info['payment_code'] == 'authorizenet_aim_arb') {
							$result = 9999;
						}
					} else {
						$result = 9999;
					}
					if ((is_array($result) && $result['ACK'] == 'Success') || $result == 9999) {
						$this->db->query("UPDATE " . DB_PREFIX . "order_subscriptions SET active = '0' WHERE order_subscription_id = '" . (int)$order_subscription_id . "'");
						$this->db->query("INSERT INTO `" . DB_PREFIX . "order_recurring_transaction` SET `order_recurring_id` = '" . (int)$profile_query->row['order_recurring_id'] . "', `date_added` = NOW(), `type` = '6'");
						$this->db->query("UPDATE `" . DB_PREFIX . "order_recurring` SET status = 3 WHERE order_recurring_id = '" . $profile_query->row['order_recurring_id'] . "' LIMIT 1");
						$subject = $this->language->get('text_pause_subscription');
						$message = sprintf($this->language->get('text_pause_message'), $order_subscription_id);
						$client_message = sprintf($this->language->get('text_pause_client'), $product_name, $this->config->get('config_email'));
						// Store email
						$mail = new Mail();
						$mail->protocol = $this->config->get('config_mail_protocol');
						$mail->parameter = $this->config->get('config_mail_parameter');
						$mail->smtp_hostname = $this->config->get('config_mail_smtp_host');
						$mail->smtp_username = $this->config->get('config_mail_smtp_username');
						$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
						$mail->smtp_port = $this->config->get('config_mail_smtp_port');
						$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
			
						$mail->setTo($this->config->get('config_email'));
						$mail->setFrom($this->config->get('config_email'));
						$mail->setReplyTo($order_info['email']);
						$mail->setSender($order_info['store_name']);
						$mail->setSubject($subject);
						$mail->setHtml($message);
						$mail->send();
						// Client email
						$mail->setTo($order_info['email']);
						$mail->setHtml($client_message);
						$mail->send();
						$success = $this->language->get('text_pause_success');
					} else {
						$success = $this->language->get('text_pause_failed');
					}
					$json = array(
						'message'	=> $success,
						'href'		=> str_replace("&amp;", "&", $this->url->link('account/order/info', 'order_id=' . $order_id, true))
					);
					echo json_encode($json);
				}
				public function restart() {
					$this->load->language('account/order');
					$this->load->model('account/order');
					$order_subscription_id = $this->request->post['order_subscription_id'];
					$results = $this->model_account_order->cancelSubscription($order_subscription_id);
					$order_id = 0;
					$recurring_id = 0;
					$product_id = 0;
					$product_name = '';
					$order_info = array();
					$result = '';
					foreach ($results as $result) {
						$order_id = $result['order_id'];
						$recurring_id = $result['subscription_id'];
						$product_id = $result['product_id'];
						$product_name = $this->model_account_order->getProductName($result['product_id']);
						$order_info = $this->model_account_order->getOrder($order_id);
					}
					$profile_query = $this->db->query("SELECT order_recurring_id, reference, status FROM " . DB_PREFIX . "order_recurring WHERE order_id = '" . (int)$order_id . "' AND recurring_id = '" . (int)$recurring_id . "' AND product_id = '" . (int)$product_id . "'");
					if ($profile_query->num_rows && $profile_query->row['reference']) {
						if ($order_info['payment_code'] == 'pp_express') {
							$this->load->model('payment/pp_express');
							$result = $this->model_payment_pp_express->recurringRestart($profile_query->row['reference']);
						} elseif ($order_info['payment_method'] == 'authorizenet_aim_arb') {
							$result = 9999;
						}
					} else {
						$result = 9999;
					}
					if ((is_array($result) && $result['ACK'] == 'Success') || $result == 9999) {
						$next_due_query = $this->db->query("SELECT next_due FROM `" . DB_PREFIX . "order_subscriptions` WHERE order_subscription_id = '" . (int)$order_subscription_id . "'");
						if ($next_due_query->row['next_due'] < time()) {
							$next_due = time();
						} else {
							$next_due = $next_due_query->row['next_due'];
						}
						$this->db->query("UPDATE " . DB_PREFIX . "order_subscriptions SET active = '1', next_due = '" . (int)$next_due . "' WHERE order_subscription_id = '" . (int)$order_subscription_id . "'");
						$this->db->query("INSERT INTO `" . DB_PREFIX . "order_recurring_transaction` SET `order_recurring_id` = '" . (int)$profile_query->row['order_recurring_id'] . "', `date_added` = NOW(), `type` = '0'");
						$this->db->query("UPDATE `" . DB_PREFIX . "order_recurring` SET status = 2 WHERE order_recurring_id = '" . $profile_query->row['order_recurring_id'] . "' LIMIT 1");
						$subject = $this->language->get('text_restart_subscription');
						$message = sprintf($this->language->get('text_restart_message'), $order_subscription_id);
						$client_message = sprintf($this->language->get('text_restart_client'), $product_name, $this->config->get('config_email'));
						// Store email
						$mail = new Mail();
						$mail->protocol = $this->config->get('config_mail_protocol');
						$mail->parameter = $this->config->get('config_mail_parameter');
						$mail->smtp_hostname = $this->config->get('config_mail_smtp_host');
						$mail->smtp_username = $this->config->get('config_mail_smtp_username');
						$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
						$mail->smtp_port = $this->config->get('config_mail_smtp_port');
						$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
			
						$mail->setTo($this->config->get('config_email'));
						$mail->setFrom($this->config->get('config_email'));
						$mail->setReplyTo($order_info['email']);
						$mail->setSender($order_info['store_name']);
						$mail->setSubject($subject);
						$mail->setHtml($message);
						$mail->send();
						// Client email
						$mail->setTo($order_info['email']);
						$mail->setHtml($client_message);
						$mail->send();
						$success = $this->language->get('text_restart_success');
					} else {
						$success = $this->language->get('text_restart_failed');
					}
					$json = array(
						'message'	=> $success,
						'href'		=> str_replace("&amp;", "&", $this->url->link('account/order/info', 'order_id=' . $order_id, true))
					);
					echo json_encode($json);
				}
			
	public function info() {
		$this->load->language('account/order');
		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}
		header("Location: https://store.learn1thing.com/dashboard/admin/order/order-history/detail/".$order_id);
		
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order/info', 'order_id=' . $order_id, true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->model('account/order');


				$data['order_id'] = $order_id;
			
		$order_info = $this->model_account_order->getOrder($order_id);

		if ($order_info) {
			$this->document->setTitle($this->language->get('text_order'));

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', true)
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('account/order', $url, true)
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_order'),
				'href' => $this->url->link('account/order/info', 'order_id=' . $this->request->get['order_id'] . $url, true)
			);

			$data['heading_title'] = $this->language->get('text_order');

			$data['text_order_detail'] = $this->language->get('text_order_detail');
			$data['text_invoice_no'] = $this->language->get('text_invoice_no');
			$data['text_order_id'] = $this->language->get('text_order_id');
			$data['text_date_added'] = $this->language->get('text_date_added');
			$data['text_shipping_method'] = $this->language->get('text_shipping_method');
			$data['text_shipping_address'] = $this->language->get('text_shipping_address');
			$data['text_payment_method'] = $this->language->get('text_payment_method');
			$data['text_payment_address'] = $this->language->get('text_payment_address');
			$data['text_history'] = $this->language->get('text_history');
			$data['text_comment'] = $this->language->get('text_comment');
			$data['text_no_results'] = $this->language->get('text_no_results');


				$data['text_trial'] = $this->language->get('text_trial');
				$data['text_renew'] = $this->language->get('text_renew');
				$data['text_change'] = $this->language->get('text_change');
				$data['text_renewal_period'] = $this->language->get('text_renewal_period');
				$data['text_savings'] = $this->language->get('text_savings');
				$data['text_sub_heading'] = $this->language->get('text_sub_heading');
				$data['text_confirm_cancel'] = $this->language->get('text_confirm_cancel');
				$data['text_confirm_pause'] = $this->language->get('text_confirm_pause');
				$data['text_confirm_restart'] = $this->language->get('text_confirm_restart');
				$data['text_cancel'] = $this->language->get('text_cancel');
				$data['text_pause'] = $this->language->get('text_pause');
				$data['text_restart'] = $this->language->get('text_restart');
				$data['text_day'] = $this->language->get('text_day');
				$data['text_week'] = $this->language->get('text_week');
				$data['text_semi_month'] = $this->language->get('text_semi_month');
				$data['text_month'] = $this->language->get('text_month');
				$data['text_year'] = $this->language->get('text_year');
				$data['column_trial'] = $this->language->get('column_trial');
				$data['column_term_amount'] = $this->language->get('column_term_amount');
				$data['column_renewal_amount'] = $this->language->get('column_renewal_amount');
				$data['column_end_date'] = $this->language->get('column_end_date');
				$data['column_next_due'] = $this->language->get('column_next_due');
				$data['column_action'] = $this->language->get('column_action');
				$data['column_terms'] = $this->language->get('column_terms');
			
			$data['column_name'] = $this->language->get('column_name');
			$data['column_model'] = $this->language->get('column_model');
			$data['column_quantity'] = $this->language->get('column_quantity');
			$data['column_price'] = $this->language->get('column_price');
			$data['column_total'] = $this->language->get('column_total');
			$data['column_action'] = $this->language->get('column_action');
			$data['column_date_added'] = $this->language->get('column_date_added');
			$data['column_status'] = $this->language->get('column_status');
			$data['column_comment'] = $this->language->get('column_comment');

			$data['button_reorder'] = $this->language->get('button_reorder');
			$data['button_return'] = $this->language->get('button_return');
			$data['button_continue'] = $this->language->get('button_continue');

			if (isset($this->session->data['error'])) {
				$data['error_warning'] = $this->session->data['error'];

				unset($this->session->data['error']);
			} else {
				$data['error_warning'] = '';
			}

			if (isset($this->session->data['success'])) {
				$data['success'] = $this->session->data['success'];

				unset($this->session->data['success']);
			} else {
				$data['success'] = '';
			}

			if ($order_info['invoice_no']) {
				$data['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
			} else {
				$data['invoice_no'] = '';
			}

			$data['order_id'] = $this->request->get['order_id'];
			$data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));

			if ($order_info['payment_address_format']) {
				$format = $order_info['payment_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['payment_firstname'],
				'lastname'  => $order_info['payment_lastname'],
				'company'   => $order_info['payment_company'],
				'address_1' => $order_info['payment_address_1'],
				'address_2' => $order_info['payment_address_2'],
				'city'      => $order_info['payment_city'],
				'postcode'  => $order_info['payment_postcode'],
				'zone'      => $order_info['payment_zone'],
				'zone_code' => $order_info['payment_zone_code'],
				'country'   => $order_info['payment_country']
			);

			$data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			$data['payment_method'] = $order_info['payment_method'];

			if ($order_info['shipping_address_format']) {
				$format = $order_info['shipping_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['shipping_firstname'],
				'lastname'  => $order_info['shipping_lastname'],
				'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country']
			);

			$data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			$data['shipping_method'] = $order_info['shipping_method'];

			$this->load->model('catalog/product');
			$this->load->model('tool/upload');

			// Products
			$data['products'] = array();

			$products = $this->model_account_order->getOrderProducts($this->request->get['order_id']);
			//$data['survey'] = array();

			get_courses($this->request->get['order_id'], $this->session->data['code']);
			$link_survey = checkSurveyLink($this->request->get['order_id']);
			$data['survey'] = $link_survey['items'];
			foreach ($products as $product) {
				$option_data = array();;
				$options = $this->model_account_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);

				foreach ($options as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}

				$product_info = $this->model_catalog_product->getProduct($product['product_id']);

				if ($product_info) {
					$reorder = $this->url->link('account/order/reorder', 'order_id=' . $order_id . '&order_product_id=' . $product['order_product_id'], true);
				} else {
					$reorder = '';
				}

				$data['products'][] = array(
					'name'     => $product['name'],
					'model'    => $product['model'],
					'option'   => $option_data,
					'quantity' => $product['quantity'],
					'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
					'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
					'reorder'  => $reorder,
					'return'   => $this->url->link('account/return/add', 'order_id=' . $order_info['order_id'] . '&product_id=' . $product['product_id'], true)
				);
			}

			// Voucher
			$data['vouchers'] = array();

			$vouchers = $this->model_account_order->getOrderVouchers($this->request->get['order_id']);

			foreach ($vouchers as $voucher) {
				$data['vouchers'][] = array(
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
				);
			}

			// Totals
			$data['totals'] = array();

			$totals = $this->model_account_order->getOrderTotals($this->request->get['order_id']);

			foreach ($totals as $total) {
				$data['totals'][] = array(
					'title' => $total['title'],
					'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
				);
			}

			$data['comment'] = nl2br($order_info['comment']);

			// History

				$data['subscriptions'] = array();
				$results = $this->model_account_order->getSubscriptions($this->request->get['order_id']);
				if ($results) {
					foreach ($results as $result) {
						$subscription_info = $this->model_account_order->getSubscription($result['product_id'], $result['subscription_id']);
						$product_name = $this->model_account_order->getProductName($result['product_id']);
						$trial_end_date = '';
						$end_date = '';
						$next_due = '';
						$active = 0;
						$active_trial = 0;
						$expiring = 0;
						$expired = 0;
						$start_date = date($this->language->get('date_format_short'), $result['start_date']);
						if ($result['trial']) {
							if ($result['trial_end_date'] > time()) {
								$active_trial = 1;
							}
							$trial_end_date = date($this->language->get('date_format_short'), $result['trial_end_date']);
						}
						if ($result['active']) {
							$active = 1;
							if ($result['end_date'] > 0) {
								$end_date = date($this->language->get('date_format_short'), $result['end_date']);
								if (time() > $result['end_date'] - (86400 * ($subscription_info['reminder_period'] * $subscription_info['reminder_count']))) {
									$expiring = 1;
								}
							}
						} else {
							$expired = 1;
							$end_date = $this->language->get('text_expired');
						}
						if ($result['next_due'] > 0) {
							$next_due = date($this->language->get('date_format_short'), $result['next_due']);
						}
						$pause = 0;
						$restart = 0;
						$cancel = 0;
						$status_id_query = $this->db->query("SELECT `status` FROM `" . DB_PREFIX . "order_recurring` WHERE order_id = '" . (int)$result['order_id'] . "' AND recurring_id = '" . (int)$result['subscription_id'] . "' AND product_id = '" . (int)$result['product_id'] . "'");
						if ($status_id_query->num_rows) {
							if (($status_id_query->row['status'] == 1 || $status_id_query->row['status'] == 2) && $this->config->get($order_info['payment_code'] . '_recurring_cancel_status')) {
								$cancel = 1;
							}
							if ($status_id_query->row['status'] == 2 && $this->config->get($order_info['payment_code'] . '_recurring_pause_status')) {
								$pause = 1;
							}
							if ($status_id_query->row['status'] == 3 && $this->config->get($order_info['payment_code'] . '_recurring_pause_status')) {
								$restart = 1;
							}
						}
						$renewal_options = array();
						$product_subscriptions = $this->model_account_order->getProductSubscriptions($result['product_id']);
						foreach ($product_subscriptions as $product_subscription) {
							if ($this->customer->isLogged()) {
								$customer_group_id = $this->customer->getGroupId();
							} else {
								$customer_group_id = $this->config->get('config_customer_group_id');
							}
							if (in_array($customer_group_id, unserialize($product_subscription['allowed_customer_groups']))) {
								$terms = '';
								if ($product_subscription['price'] > 0) {
									$cost = $product_subscription['price'];
								} else {
									$cost = $this->model_account_order->getProductPrice($result['product_id']);
								}
								$tax_class_id = $this->model_account_order->getProductTaxClass($result['product_id']);
								if ($product_subscription['discount']) {
									$discount_amt = 0;
									if ($product_subscription['discount_type'] == "Percent") {
										$cost -= ($product_subscription['price'] * $product_subscription['discount']) / 100;
									 } else {
										$cost -= $product_subscription['discount'];
									}
									if ($this->config->get('product_subscribe_add_taxes') || $product_subscription['include_taxes']) {
										$cost = $this->currency->format($this->tax->calculate($cost, $tax_class_id, 1), $this->config->get('config_currency'));
									} else {
										$cost = $this->currency->format($cost, $this->config->get('config_currency'));
									}
									if ($product_subscription['duration']) {
										$terms = sprintf($this->language->get('text_renew_discount_duration'), $product_subscription['cycle'], $this->language->get('text_' . $product_subscription['frequency']), $cost, $product_subscription['duration']);
									} else {
										$terms = sprintf($this->language->get('text_renew_discount'), $product_subscription['cycle'], $this->language->get('text_' . $product_subscription['frequency']), $cost);
									}
								} else {
									if ($this->config->get('product_subscribe_add_taxes') || $product_subscription['include_taxes']) {
										$cost = $this->currency->format($this->tax->calculate($cost, $tax_class_id, 1), $this->config->get('config_currency'));
									} else {
										$cost = $this->currency->format($cost, $this->config->get('config_currency'));
									}
									if ($product_subscription['duration']) {
										$terms = sprintf($this->language->get('text_renew_duration'), $product_subscription['cycle'], $this->language->get('text_' . $product_subscription['frequency']), $cost, $product_subscription['duration']);
									} else {
										$terms = sprintf($this->language->get('text_renew'), $product_subscription['cycle'], $this->language->get('text_' . $product_subscription['frequency']), $cost);
									}
								}
								$renewal_options[] = array(
									'subscription_id'		=> $product_subscription['subscription_id'],
									'terms'					=> $terms,
									'cost'					=> $cost
								);
							}
						}
						$change_href = $this->url->link('product/product', '&order_id=' . $this->request->get['order_id'] . '&product_id=' . $result['product_id'] . '&order_subscription_id=' . $result['order_subscription_id'], true);
						$data['subscriptions'][] = array(
							'order_subscription_id'		=> $result['order_subscription_id'],
							'subscription_id'			=> $result['subscription_id'],
							'product_id'				=> $result['product_id'],
							'quantity'					=> 1,
							'name'						=> $product_name,
							'amount'					=> $this->currency->format($result['payment_amount'], $order_info['currency_code'], $order_info['currency_value']),
							'renewal_options'			=> $renewal_options,
							'change_href'				=> $change_href,
							'trial_end_date'			=> $trial_end_date,
							'start_date'				=> $start_date,
							'end_date'					=> $end_date,
							'next_due'					=> $next_due,
							'recurring'					=> $result['recurring'],
							'terms'						=> $result['terms'],
							'active'					=> $active,
							'pause'						=> $pause,
							'restart'					=> $restart,
							'cancel'					=> $cancel,
							'active_trial'				=> $active_trial,
							'expiring'					=> $expiring,
							'expired'					=> $expired
						);
					}
				}
			
			$data['histories'] = array();

			$results = $this->model_account_order->getOrderHistories($this->request->get['order_id']);

			foreach ($results as $result) {
				$data['histories'][] = array(
					'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					'status'     => $result['status'],
					'comment'    => $result['notify'] ? nl2br($result['comment']) : ''
				);
			}

			$data['continue'] = $this->url->link('account/order', '', true);

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('account/order_info', $data));
		} else {
			$this->document->setTitle($this->language->get('text_order'));

			$data['heading_title'] = $this->language->get('text_order');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', true)
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('account/order', '', true)
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_order'),
				'href' => $this->url->link('account/order/info', 'order_id=' . $order_id, true)
			);

			$data['continue'] = $this->url->link('account/order', '', true);

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}

	public function reorder() {
		$this->load->language('account/order');

		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}

		$this->load->model('account/order');


				$data['order_id'] = $order_id;
			
		$order_info = $this->model_account_order->getOrder($order_id);

		if ($order_info) {
			if (isset($this->request->get['order_product_id'])) {
				$order_product_id = $this->request->get['order_product_id'];
			} else {
				$order_product_id = 0;
			}

			$order_product_info = $this->model_account_order->getOrderProduct($order_id, $order_product_id);

			if ($order_product_info) {
				$this->load->model('catalog/product');

				$product_info = $this->model_catalog_product->getProduct($order_product_info['product_id']);

				if ($product_info) {
					$option_data = array();

					$order_options = $this->model_account_order->getOrderOptions($order_product_info['order_id'], $order_product_id);

					foreach ($order_options as $order_option) {
						if ($order_option['type'] == 'select' || $order_option['type'] == 'radio' || $order_option['type'] == 'image') {
							$option_data[$order_option['product_option_id']] = $order_option['product_option_value_id'];
						} elseif ($order_option['type'] == 'checkbox') {
							$option_data[$order_option['product_option_id']][] = $order_option['product_option_value_id'];
						} elseif ($order_option['type'] == 'text' || $order_option['type'] == 'textarea' || $order_option['type'] == 'date' || $order_option['type'] == 'datetime' || $order_option['type'] == 'time') {
							$option_data[$order_option['product_option_id']] = $order_option['value'];
						} elseif ($order_option['type'] == 'file') {
							$option_data[$order_option['product_option_id']] = $this->encryption->encrypt($order_option['value']);
						}
					}

					$this->cart->add($order_product_info['product_id'], $order_product_info['quantity'], $option_data);

					$this->session->data['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $product_info['product_id']), $product_info['name'], $this->url->link('checkout/cart'));

					unset($this->session->data['shipping_method']);
					unset($this->session->data['shipping_methods']);
					unset($this->session->data['payment_method']);
					unset($this->session->data['payment_methods']);
				} else {
					$this->session->data['error'] = sprintf($this->language->get('error_reorder'), $order_product_info['name']);
				}
			}
		}

		$this->response->redirect($this->url->link('account/order/info', 'order_id=' . $order_id));
	}
}
