<?php
// Heading
$_['heading_title']					= 'Cash On Delivery';

// Text

				$_['entry_product_subscribe']		= 'Use with Product Subscription';
				$_['entry_recurring_cancel_status']	= 'Allow Customer to Cancel';
				$_['entry_recurring_pause_status']	= 'Allow Customer to Suspend';
				$_['text_yes']						= 'Yes';
				$_['text_no'] 						= 'No';
			
$_['text_payment']					= 'Payment';
$_['text_success']					= 'Success: You have modified Cash On Delivery payment module!';
$_['text_edit']                     = 'Edit Cash On Delivery';

// Entry
$_['entry_total']					= 'Total';
$_['entry_order_status']			= 'Order Status';
$_['entry_geo_zone']				= 'Geo Zone';
$_['entry_status']					= 'Status';
$_['entry_sort_order']				= 'Sort Order';

// Help
$_['help_total']					= 'The checkout total the order must reach before this payment method becomes active.';

// Error
$_['error_permission']				= 'Warning: You do not have permission to modify payment Cash On Delivery!';