<?php
// Locale
$_['code']                  = 'en';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'd/m/Y';
$_['date_format_long']      = 'l dS F Y';
$_['time_format']           = 'h:i:s A';
$_['datetime_format']       = 'd/m/Y H:i:s';
$_['decimal_point']         = '.';
$_['thousand_point']        = ',';

// Text

				$_['text_day']							= 'day';
				$_['text_week']							= 'week';
				$_['text_semi_month']					= 'semi_month';
				$_['text_month']						= 'month';
				$_['text_year']							= 'year';
				$_['text_monday']						= 'Monday';
				$_['text_tuesday']						= 'Tuesday';
				$_['text_wednesday']					= 'Wednesday';
				$_['text_thursday']						= 'Thursday';
				$_['text_friday']						= 'Friday';
				$_['text_saturday']						= 'Saturday';
				$_['text_sunday']						= 'Sunday';
				$_['text_subscription_trial']			= 'Trial for %s %s(s) @ %s';
				$_['text_subscription_trial_only']		= 'Trial for %s %s(s) @ %s. This is a TRIAL ONLY!';
				$_['text_subscription_duration']		= 'Every %s %s(s) @ %s for %s payment(s)';
				$_['text_subscription_duration_sd']		= 'Every %s %s(s) on %s @ %s for %s payment(s)';
				$_['text_subscription_cancel']			= 'Every %s %s(s) @ %s until cancelled';
				$_['text_subscription_cancel_sd']		= 'Every %s %s(s) on %s @ %s until cancelled';
				$_['text_subscription_item']			= '<span class="required">Subscription Product: </span>';
			
$_['text_home']             = '<i class="fa fa-home"></i>';
$_['text_yes']              = 'Yes';
$_['text_no']               = 'No';
$_['text_none']             = ' --- None --- ';
$_['text_select']           = ' --- Please Select --- ';
$_['text_all_zones']        = 'All Zones';
$_['text_pagination']       = 'Showing %d to %d of %d (%d Pages)';
$_['text_loading']          = 'Loading...';
$_['text_no_results']       = 'No results!';

// Buttons
$_['button_address_add']    = 'Add Address';
$_['button_back']           = 'Back';
$_['button_continue']       = 'Continue';
$_['button_cart']           = 'Add to Cart';
$_['button_cancel']         = 'Cancel';
$_['button_compare']        = 'Compare this Product';
$_['button_wishlist']       = 'Add to Wish List';
$_['button_checkout']       = 'Checkout';
$_['button_confirm']        = 'Confirm Order';
$_['button_coupon']         = 'Apply Coupon';
$_['button_delete']         = 'Delete';
$_['button_download']       = 'Download';
$_['button_edit']           = 'Edit';
$_['button_filter']         = 'Refine Search';
$_['button_new_address']    = 'New Address';
$_['button_change_address'] = 'Change Address';
$_['button_reviews']        = 'Reviews';
$_['button_write']          = 'Write Review';
$_['button_login']          = 'Login';
$_['button_update']         = 'Update';
$_['button_remove']         = 'Remove';
$_['button_reorder']        = 'Reorder';
$_['button_return']         = 'Return';
$_['button_shopping']       = 'Continue Shopping';
$_['button_search']         = 'Search';
$_['button_shipping']       = 'Apply Shipping';
$_['button_submit']         = 'Submit';
$_['button_guest']          = 'Guest Checkout';
$_['button_view']           = 'View';
$_['button_voucher']        = 'Apply Gift Certificate';
$_['button_upload']         = 'Upload File';
$_['button_reward']         = 'Apply Points';
$_['button_quote']          = 'Get Quotes';
$_['button_list']           = 'List';
$_['button_grid']           = 'Grid';
$_['button_map']            = 'View Google Map';

// Error
$_['error_exception']       = 'Error Code(%s): %s in %s on line %s';
$_['error_upload_1']        = 'Warning: The uploaded file exceeds the upload_max_filesize directive in php.ini!';
$_['error_upload_2']        = 'Warning: The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form!';
$_['error_upload_3']        = 'Warning: The uploaded file was only partially uploaded!';
$_['error_upload_4']        = 'Warning: No file was uploaded!';
$_['error_upload_6']        = 'Warning: Missing a temporary folder!';
$_['error_upload_7']        = 'Warning: Failed to write file to disk!';
$_['error_upload_8']        = 'Warning: File upload stopped by extension!';
$_['error_upload_999']      = 'Warning: No error code available!';
$_['error_curl']            = 'CURL: Error Code(%s): %s';

$_['button_credit_advance'] = 'Apply Credit';
			