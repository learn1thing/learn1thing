<?php echo $header; ?>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <h1 class="heading-title"><?php echo $heading_title; ?></h1>
      <?php echo $content_top; ?>
      <table class="table table-bordered table-hover list">
        <thead>
          <tr>
            <td class="text-left" colspan="2"><?php echo $text_order_detail; ?></td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="text-left" style="width: 50%;"><?php if ($invoice_no) { ?>
              <b><?php echo $text_invoice_no; ?></b> <?php echo $invoice_no; ?><br />
              <?php } ?>
              <b><?php echo $text_order_id; ?></b> #<?php echo $order_id; ?><br />
              <b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?></td>
            <td class="text-left"><?php if ($payment_method) { ?>
              <b><?php echo $text_payment_method; ?></b> <?php echo $payment_method; ?><br />
              <?php } ?>
              <?php if ($shipping_method) { ?>
              <b><?php echo $text_shipping_method; ?></b> <?php echo $shipping_method; ?>
              <?php } ?></td>
          </tr>
        </tbody>
      </table>
      <table class="table table-bordered table-hover list">
        <thead>
          <tr>
            <td class="text-left" style="width: 50%;"><?php echo $text_payment_address; ?></td>
            <?php if ($shipping_address) { ?>
            <td class="text-left"><?php echo $text_shipping_address; ?></td>
            <?php } ?>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="text-left"><?php echo $payment_address; ?></td>
            <?php if ($shipping_address) { ?>
            <td class="text-left"><?php echo $shipping_address; ?></td>
            <?php } ?>
          </tr>
        </tbody>
      </table>
      <div class="table-responsive">
        <table class="table table-bordered table-hover list">
          <thead>
            <tr>
              <td class="text-left"><?php echo $column_name; ?></td>
              <td class="text-left"><?php echo $column_model; ?></td>
              <td class="text-right"><?php echo $column_quantity; ?></td>
              <td class="text-right"><?php echo $column_price; ?></td>
              <td class="text-right"><?php echo $column_total; ?></td>
              <?php if ($products) { ?>
              <td style="width: 20px;"></td>
              <?php } ?>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($products as $product) { ?>
            <tr>
              <td class="text-left"><?php echo $product['name']; ?>
                <?php foreach ($product['option'] as $option) { ?>
                <br />
                &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                <?php } ?></td>
              <td class="text-left"><?php echo $product['model']; ?></td>
              <td class="text-right"><?php echo $product['quantity']; ?></td>
              <td class="text-right"><?php echo $product['price']; ?></td>
              <td class="text-right"><?php echo $product['total']; ?></td>
              <td class="text-right" style="white-space: nowrap;"><?php if ($product['reorder']) { ?>
                <a href="<?php echo $product['reorder']; ?>" data-toggle="tooltip" title="<?php echo $button_reorder; ?>" class="btn btn-primary"><i class="fa fa-shopping-cart"></i></a>
                <?php } ?>
                <a href="<?php echo $product['return']; ?>" data-toggle="tooltip" title="<?php echo $button_return; ?>" class="btn btn-danger"><i class="fa fa-reply"></i></a></td>
            </tr>
            <?php } ?>
            <?php foreach ($vouchers as $voucher) { ?>
            <tr>
              <td class="text-left"><?php echo $voucher['description']; ?></td>
              <td class="text-left"></td>
              <td class="text-right">1</td>
              <td class="text-right"><?php echo $voucher['amount']; ?></td>
              <td class="text-right"><?php echo $voucher['amount']; ?></td>
              <?php if ($products) { ?>
              <td></td>
              <?php } ?>
            </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <?php foreach ($totals as $total) { ?>
            <tr>
              <td colspan="3"></td>
              <td class="text-right"><b><?php echo $total['title']; ?></b></td>
              <td class="text-right"><?php echo $total['text']; ?></td>
              <?php if ($products) { ?>
              <td></td>
              <?php } ?>
            </tr>
            <?php } ?>
          </tfoot>
        </table>
      </div>

				<?php if ($subscriptions) { ?>
				<h3><?php echo $text_sub_heading; ?></h3>
				<table class="table table-bordered table-hover list">
					<thead>
						<thbody>
							<tr>
								<td class="text-left"><?php echo $column_name; ?></td>
								<td class="text-left"><?php echo $column_trial; ?></td>
								<td class="text-left"><?php echo $column_terms; ?></td>
								<td class="text-left"><?php echo $column_end_date; ?></td>
								<td class="text-left"><?php echo $column_next_due; ?></td>
								<td class="text-center"><?php echo $column_term_amount; ?></td>
								<td class="text-center"><?php echo $column_action; ?></td>
							</tr>
						</thbody>
					</thead>
						<tbody>
							<?php foreach ($subscriptions as $subscription) { ?>
								<tr>
									<td class="text-left"><?php echo $subscription['name']; ?></td>
									<td class="text-left">
										<?php if ($subscription['active_trial']) { ?>
											<?php echo $subscription['trial_end_date']; ?>
										<?php } ?>
									</td>
									<td class="text-left"><?php echo $subscription['terms']; ?></td>
									<td class="text-left"><?php echo $subscription['end_date']; ?></td>
									<td class="text-left"><?php echo $subscription['next_due']; ?></td>
									<td class="text-center">
										<?php if (!$subscription['expiring'] && $subscription['active']) { ?>
											<?php echo $subscription['amount']; ?>
										<?php } ?>
									</td>
									<td class="text-center">
										<?php if ($subscription['expiring'] && !empty($subscription['renewal_options'])) { ?>
											<form id="renewal">
												<input type="hidden" name="product_id" id="product_id" value="<?php echo $subscription['product_id']; ?>" />
												<input type="hidden" name="quantity" id="quantity" value="<?php echo $subscription['quantity']; ?>" />
												<input type="hidden" name="order_subscription_id" id="order_subscription_id" value="<?php echo $subscription['order_subscription_id']; ?>" />
												<select id="renew" name="renew">
													<option value="" selected="selected"><?php echo $text_renewal_period; ?></option>
													<?php foreach ($subscription['renewal_options'] as $renewal_option) { ?>
														<option value="<?php echo $renewal_option['subscription_id']; ?>"><?php echo $renewal_option['terms']; ?></option>
													<?php } ?>
												</select>
											</form><br /><br />
										<?php } ?>
										[ <a href="<?php echo $subscription['change_href']; ?>" style="margin-left: 3px;"><?php echo $text_change; ?></a> ]
										<?php if ($subscription['active'] && $subscription['cancel']) { ?>
											[ <a class="cancel" style="margin-left: 3px;" title="<?php echo $subscription['order_subscription_id']; ?>"><?php echo $text_cancel; ?></a> ]
										<?php } ?>
										<?php if ($subscription['pause']) { ?>
											[ <a class="pause" style="margin-left: 3px;" title="<?php echo $subscription['order_subscription_id']; ?>"><?php echo $text_pause; ?></a> ]
										<?php } ?>
										<?php if ($subscription['restart']) { ?>
											[ <a class="restart" style="margin-left: 3px;" title="<?php echo $subscription['order_subscription_id']; ?>"><?php echo $text_restart; ?></a> ]
										<?php } ?>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				<?php } ?>
			
      <?php if ($comment) { ?>
      <table class="table table-bordered table-hover list">
        <thead>
          <tr>
            <td class="text-left"><?php echo $text_comment; ?></td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="text-left"><?php echo $comment; ?></td>
          </tr>
        </tbody>
      </table>
      <?php } ?>

      <?php if ($invoice_print) { ?>
            <div class="buttons clearfix">
                 <div class="pull-right"><a href="<?php echo $invoice; ?>" target="_blank" class="btn btn-info"><i class="fa fa-print"></i> <?php echo $button_invoice_print; ?></a></div>
            </div>
      <?php } ?>
			
      <?php if ($histories) { ?>
      <h3><?php echo $text_history; ?></h3>
      <div class="table-responsive">
        <table class="table table-bordered table-hover list">
          <thead>
          <tr>
            <td class="text-left"><?php echo $column_date_added; ?></td>
            <td class="text-left"><?php echo $column_status; ?></td>
            <td class="text-left"><?php echo $column_comment; ?></td>
          </tr>
          </thead>
          <tbody>
          <?php foreach ($histories as $history) { ?>
          <tr>
            <td class="text-left"><?php echo $history['date_added']; ?></td>
            <td class="text-left"><?php echo $history['status']; ?></td>
            <td class="text-left"><?php echo $history['comment']; ?></td>
          </tr>
          <?php } ?>
          </tbody>
        </table>
      </div>
      <?php } ?>
      <div class="table-responsive">
      <?php 
        if( count($survey) > 0 ) {
          foreach( $survey as $link ) {
            if($link['profiler_url'] != ""){
            ?>

            <p style="margin-top: 0px; margin-bottom: 20px;">Profiler:</p>
            <p style="margin-top: 0px; margin-bottom: 20px;"><a href="<?php echo $link['profiler_url']; ?>"><?php echo $link['profiler_url']; ?></a></p>
            <?php } if( count($link['course_url']) > 0 ) { 
            foreach($link['course_url'] as $course) {
            ?>
            <p style="margin-top: 0px; margin-bottom: 20px;">Course:</p>
            <p style="margin-top: 0px; margin-bottom: 20px;"><a href="<?php echo $course ?>"><?php echo $course ?></a></p>
            <?php
            }
            }
          }
        }
      ?>
      </div>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary button"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    </div>
</div>

				<script type="text/javascript">
					$(document).ready(function() {
						$('.cancel').on('click', function() {
							if (confirm('<?php echo $text_confirm_cancel; ?>')) {
								var order_subscription_id = $(this).attr('title');
								$.ajax({
									url: 'index.php?route=account/order/cancel',
									type: 'POST',
									dataType: 'json',
									data: 'order_subscription_id=' + order_subscription_id,
									success: function(json) {
										alert(json.message);
										location.href = json.href;
									},
									error: function(xhr,j,i) {
										alert(i);
									}
								});
							} else {
								return false;
							}
						});
						$('.pause').on('click', function() {
							if (confirm('<?php echo $text_confirm_pause; ?>')) {
								var order_subscription_id = $(this).attr('title');
								$.ajax({
									url: 'index.php?route=account/order/pause',
									type: 'POST',
									dataType: 'json',
									data: 'order_subscription_id=' + order_subscription_id,
									success: function(json) {
										alert(json.message);
										location.href = json.href;
									},
									error: function(xhr,j,i) {
										alert(i);
									}
								});
							} else {
								return false;
							}
						});
						$('.restart').on('click', function() {
							if (confirm('<?php echo $text_confirm_restart; ?>')) {
								var order_subscription_id = $(this).attr('title');
								$.ajax({
									url: 'index.php?route=account/order/restart',
									type: 'POST',
									dataType: 'json',
									data: 'order_subscription_id=' + order_subscription_id,
									success: function(json) {
										alert(json.message);
										location.href = json.href;
									},
									error: function(xhr,j,i) {
										alert(i);
									}
								});
							} else {
								return false;
							}
						});
						$('#renew').on('change', function() {
							if ($('#renew').val() != '') {
								<?php if (version_compare(VERSION, '1.5.1.3.1', '>')) { ?>
									var url = 'index.php?route=checkout/cart/add';
								<?php } else { ?>
									var url = 'index.php?route=checkout/cart/update';
								<?php } ?>
								$.ajax({
									url: url,
									type: 'post',
									data: 'product_id=' + $('#product_id').val() + '&quantity=' + $('#quantity').val() + '&renewal_subscription_id=' + $('#renew').val() + '&order_subscription_id=' + $('#order_subscription_id').val(),
									dataType: 'json',
									success: function(json) {
										$('.success, .warning, .attention, .information, .error').remove();
										if (json['redirect']) {
											location = json['redirect'];
										}
										if (json['success']) {
											$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
											$('.success').fadeIn('slow');
											$('#cart-total').html(json['total']);
											$('html, body').animate({ scrollTop: 0 }, 'slow');
										}
									}
								});
							} else {
								return false;
							}
						});
					});
				</script>
			
<?php echo $footer; ?>