<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/project/bounty/L1T-opencart/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/project/bounty/L1T-opencart/');

// DIR
define('DIR_APPLICATION', '/srv/www/project/bounty/L1T-opencart/catalog/');
define('DIR_SYSTEM', '/srv/www/project/bounty/L1T-opencart/system/');
define('DIR_IMAGE', '/srv/www/project/bounty/L1T-opencart/image/');
define('DIR_LANGUAGE', '/srv/www/project/bounty/L1T-opencart/catalog/language/');
define('DIR_TEMPLATE', '/srv/www/project/bounty/L1T-opencart/catalog/view/theme/');
define('DIR_CONFIG', '/srv/www/project/bounty/L1T-opencart/system/config/');
define('DIR_CACHE', '/srv/www/project/bounty/L1T-opencart/system/storage/cache/');
define('DIR_DOWNLOAD', '/srv/www/project/bounty/L1T-opencart/system/storage/download/');
define('DIR_LOGS', '/srv/www/project/bounty/L1T-opencart/system/storage/logs/');
define('DIR_MODIFICATION', '/srv/www/project/bounty/L1T-opencart/system/storage/modification/');
define('DIR_UPLOAD', '/srv/www/project/bounty/L1T-opencart/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_DATABASE', 'db_bounty_oc');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
