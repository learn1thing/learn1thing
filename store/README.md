# learn1thing
the suicide project

## engine
  - Opencart 2.2.0.0

## Setup

- Config
  - copy config config-local.php with config.php and edit path with your project file 
  - copy admin/config.php with admin/config.php and edit path with your project path
  - db use in folder db/master.sql

- credential
  - Admin
    - username  : admin
    - password  : admin123

## Requirements

These are minimum requirements needed so that OpenCart can be installed and work properly.
- Web Server (Apache suggested)
- PHP (at least 5.3)
- Curl enabled
- Database (MySQLi suggested)


## Note
  - always check user group presmission after instal/copy module
  - always do modification clear button after instal/copy module
