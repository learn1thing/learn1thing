<?php
abstract class RestController extends Controller {

    public function checkPlugin() {
        $this->config->set('config_error_display', 0);

        $this->response->addHeader('Content-Type: application/json; charset=utf-8');

        $json = array("success"=>false);

        /*check rest api is enabled*/
        if (!$this->config->get('rest_api_status')) {
            $json["error"] = 'API is disabled. Enable it!';
        }


        $headers = apache_request_headers();

        $key = "";

        if(isset($headers['X-Oc-Merchant-Id'])){
            $key = $headers['X-Oc-Merchant-Id'];
        }else if(isset($headers['X-OC-MERCHANT-ID'])) {
            $key = $headers['X-OC-MERCHANT-ID'];
        }

        /*validate api security key*/
        if ($this->config->get('rest_api_key') && ($key != $this->config->get('rest_api_key'))) {
            $json["error"] = 'Invalid secret key';
        }

        if(isset($json["error"])){
            echo(json_encode($json));
            exit;
        }

        $currency = "";

        if(isset($headers['X-Oc-Currency'])){
            $currency = $headers['X-Oc-Currency'];
        } else if(isset($headers['X-OC-CURRENCY'])){
            $currency = $headers['X-OC-CURRENCY'];
        }

        if (!empty($currency)) {

            if(strpos(VERSION, '2.2.') === false && strpos(VERSION, '2.3.') === false) {
                $this->currency->set($currency);
            }

            $this->currency->setRestCurrencyCode($currency);

        } else {
            if(strpos(VERSION, '2.2.') === false && strpos(VERSION, '2.3.') === false) {
                $this->currency->setRestCurrencyCode($this->currency->getCode());
            } else {
                $this->currency->setRestCurrencyCode($this->session->data['currency']);
            }
        }

        if(isset($headers['X-Oc-Store-Id'])){
            $this->config->set('config_store_id', $headers['X-Oc-Store-Id']);
        } else if(isset($headers['X-OC-STORE-ID'])){
            $this->config->set('config_store_id', $headers['X-OC-STORE-ID']);
        }

        //set language
        $osc_lang = "";
        if(isset($headers['X-Oc-Merchant-Language'])){
            $osc_lang = $headers['X-Oc-Merchant-Language'];
        }else if(isset($headers['X-OC-MERCHANT-LANGUAGE'])){
            $osc_lang = $headers['X-OC-MERCHANT-LANGUAGE'];
        }

        if($osc_lang != ""){

            $languages = array();
            $this->load->model('localisation/language');
            $all = $this->model_localisation_language->getLanguages();

            foreach ($all as $result) {
                $languages[$result['code']] = $result;
            }

            if(isset($languages[$osc_lang])){
                $this->session->data['language'] = $osc_lang;
                $this->config->set('config_language', $osc_lang);
                $this->config->set('config_language_id', $languages[$osc_lang]['language_id']);

                if(isset($languages[$osc_lang]['directory']) && !empty($languages[$osc_lang]['directory'])){
                    $directory = $languages[$osc_lang]['directory'];
                } else {
                    $directory = $languages[$osc_lang]['code'];
                }

                $language = new \Language($directory);
                $language->load($directory);
                $this->registry->set('language', $language);
            }

        }

        if(isset($headers['X-Oc-Image-Dimension'])){
            $d = $headers['X-Oc-Image-Dimension'];
            $d = explode('x', $d);
            $this->config->set('config_rest_api_image_width', $d[0]);
            $this->config->set('config_rest_api_image_height', $d[1]);
        } else if(isset($headers['X-OC-IMAGE-DIMENSION'])){
            $d = $headers['X-OC-IMAGE-DIMENSION'];
            $d = explode('x', $d);
            $this->config->set('config_rest_api_image_width', $d[0]);
            $this->config->set('config_rest_api_image_height', $d[1]);
        } else {
            $this->config->set('config_rest_api_image_width', 500);
            $this->config->set('config_rest_api_image_height', 500);
        }
    }

    public function sendResponse($json) {
        if ($this->debugIt) {
            echo '<pre>';
            print_r($json);
            echo '</pre>';
        } else {
            $this->response->addHeader('Content-Type: application/json; charset=utf-8');
            $this->response->setOutput(json_encode($json));
        }
    }

    //update user session
    function update_session($osc_session) {
        if(session_id() != $osc_session){
            // Close the current session
            session_write_close();
            session_id($osc_session);
            session_start();
            $this->session->data = $_SESSION;
        }
    }

    public function returnDeprecated(){
        $json['success'] = false;
        $json['error'] = "This service has been removed for security reasons.Please contact us for more information.";
        echo(json_encode($json));
        exit;
    }

}
if( !function_exists('apache_request_headers') ) {
    function apache_request_headers() {
        $arh = array();
        $rx_http = '/\AHTTP_/';

        foreach($_SERVER as $key => $val) {
            if( preg_match($rx_http, $key) ) {
                $arh_key = preg_replace($rx_http, '', $key);
                $rx_matches = array();
                // do some nasty string manipulations to restore the original letter case
                // this should work in most cases
                $rx_matches = explode('_', $arh_key);

                if( count($rx_matches) > 0 and strlen($arh_key) > 2 ) {
                    foreach($rx_matches as $ak_key => $ak_val) {
                        $rx_matches[$ak_key] = ucfirst($ak_val);
                    }

                    $arh_key = implode('-', $rx_matches);
                }

                $arh[$arh_key] = $val;
            }
        }

        return( $arh );
    }

}
