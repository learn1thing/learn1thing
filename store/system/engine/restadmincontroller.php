<?php
abstract class RestAdminController extends Controller {

    public function checkPlugin() {

        if(!$this->ipValidation()){
            header("Content-Type: application/json;charset=utf-8");
            echo(json_encode(array('status'=>false, "error"=>"Permission denied.")));
            exit;
        }

        $this->config->set('config_error_display', 0);

        $this->response->addHeader('Content-Type: application/json; charset=utf-8');

        $json = array("success"=>false);

        /*check rest api is enabled*/
        if (!$this->config->get('restadmin_status')) {
            $json["error"] = 'Rest Admin API is disabled. Enable it!';
        }

        $headers = apache_request_headers();

        $key = "";

        if(isset($headers['X-Oc-Restadmin-Id'])){
            $key = $headers['X-Oc-Restadmin-Id'];
        }else if(isset($headers['X-OC-RESTADMIN-ID'])) {
            $key = $headers['X-OC-RESTADMIN-ID'];
        }

        /*validate api security key*/
        if ($this->config->get('restadmin_key') && ($key != $this->config->get('restadmin_key'))) {
            $json["error"] = 'Invalid secret key';
        }

        $currency = "";

        if(isset($headers['X-Oc-Currency'])){
            $currency = $headers['X-Oc-Currency'];
        } else if(isset($headers['X-OC-CURRENCY'])){
            $currency = $headers['X-OC-CURRENCY'];
        }

        if (!empty($currency)) {
            $this->currency->set($currency);
        }

        if(isset($headers['X-Oc-Store-Id'])){
            $this->config->set('config_store_id', $headers['X-Oc-Store-Id']);
        } else if(isset($headers['X-OC-STORE-ID'])){
            $this->config->set('config_store_id', $headers['X-OC-STORE-ID']);
        }

        //set language
        $osc_lang = "";
        if(isset($headers['X-Oc-Merchant-Language'])){
            $osc_lang = $headers['X-Oc-Merchant-Language'];
        }else if(isset($headers['X-OC-MERCHANT-LANGUAGE'])){
            $osc_lang = $headers['X-OC-MERCHANT-LANGUAGE'];
        }

        if($osc_lang != ""){
            $this->session->data['language'] = $osc_lang;
            $this->config->set('config_language', $osc_lang);
            $languages = array();
            $this->load->model('localisation/language');
            $all = $this->model_localisation_language->getLanguages();

            foreach ($all as $result) {
                $languages[$result['code']] = $result;
            }
            $this->config->set('config_language_id', $languages[$osc_lang]['language_id']);

            if(isset($languages[$osc_lang]['directory']) && !empty($languages[$osc_lang]['directory'])){
                $directory = $languages[$osc_lang]['directory'];
            } else {
                $directory = $languages[$osc_lang]['code'];
            }

            $language = new \Language($directory);
            $language->load($directory);
            $this->registry->set('language', $language);
        }

        if(isset($json["error"])){
            header("Content-Type: application/json;charset=utf-8");
            echo(json_encode($json));
            exit;
        }else {
            $this->response->setOutput(json_encode($json));
        }
    }

    public function sendResponse($json) {
        $this->response->addHeader('Content-Type: application/json; charset=utf-8');
        $this->response->setOutput(json_encode($json));
    }

    private function ipValidation(){
        if(!empty($this->config->get('restadmin_allowed_ip'))){
            $ips = explode(",", $this->config->get('restadmin_allowed_ip'));

            $ips =  array_map(
                function($ip) { return trim($ip); },
                $ips
            );

            if(!in_array($_SERVER['REMOTE_ADDR'], $ips)
                || (isset($_SERVER["HTTP_X_FORWARDED_FOR"]) && !in_array($_SERVER["HTTP_X_FORWARDED_FOR"], $ips))) {
                return false;
            } else {
                return true;
            }
        }
        return true;
    }
}
if( !function_exists('apache_request_headers') ) {
    function apache_request_headers() {
        $arh = array();
        $rx_http = '/\AHTTP_/';

        foreach($_SERVER as $key => $val) {
            if( preg_match($rx_http, $key) ) {
                $arh_key = preg_replace($rx_http, '', $key);
                $rx_matches = array();
                // do some nasty string manipulations to restore the original letter case
                // this should work in most cases
                $rx_matches = explode('_', $arh_key);

                if( count($rx_matches) > 0 and strlen($arh_key) > 2 ) {
                    foreach($rx_matches as $ak_key => $ak_val) {
                        $rx_matches[$ak_key] = ucfirst($ak_val);
                    }

                    $arh_key = implode('-', $rx_matches);
                }

                $arh[$arh_key] = $val;
            }
        }

        return( $arh );
    }

}