<?php
class currency_tools_pro extends AddistModel
{
    public $info = array(
        'hash'              =>  '9f898471534230e985e7a8563e662da3',
        'version'           =>  '0.5.1.9.6',
        'link'              =>  'http://addist.ru/index.php?route=product/product&product_id=2',
        'trial'             =>  '0',
        'domain'            =>  '',
        'release'           =>  'stable-2.2+',
    );
    
    public $fields = array(
        'currency_product'      =>  '',
        'currency_store'        =>  '',
        'country_detector'      =>  'geoiplookup',
        'session_lifetime'      =>  86400,
        'restriction'           =>  false,
        'currency_secondary'    =>  '',
        'currency_payment'      =>  '',
        'template'              =>  '{main}',
        'use_core'              =>  false,
        
        'autorefresh'           =>  false,
        'hours'                 =>  array('00'),
        'last_refresh'          =>  0,
        'provider'              =>  'fixer',
        
        'payment_methods'       =>  array(),
        'shipping_methods'      =>  array(),
        'countries'             =>  array(),
        'executed'              =>  '',
        'cron_running'          =>  false,
        'auto_price'            =>  false,
        
        'widget_status'         =>  false,
        'widget_currency'       =>  'USD',
        'widget_tag'            =>  'div',
        'widget_container'      =>  'currency-widget',
        'widget_selector'       =>  'div#menu ul:first, nav#menu div.collapse:first',
        'widget_method'         =>  'prependTo',
        'widget_style'          =>  '#currency-widget {display: inline-block; padding-top: 10px; margin-right: 12px; float: right; color: #fff;}',
        'widget_template'       =>  '{main} = {store}',
    );
    
    public $mods = array('vqmod'=>array(),);
    
    public $files = array('system/library/currency_tools.php', 'system/library/currency_tools_helper.php', 'system/addist/extension/currency_tools_pro/install.php', 'system/addist/extension/currency_tools_pro/vqmod/2.x/currency_tools.xml', 'system/addist/extension/currency_tools_pro/currency.json', 'system/currency_provider/keb.php', 'system/currency_provider/fixer.php', 'system/currency_provider/okua.php', 'system/currency_provider/bnm.php', 'system/currency_provider/finmarket.php', 'system/currency_provider/tcmb.php', 'system/currency_provider/yahoo.php', 'system/currency_provider/bnr.php', 'system/currency_provider/apilayer.php', 'system/currency_provider/mnb.php', 'admin/view/template/module/currency_tools_export.tpl', 'admin/view/template/module/currency_tools_convert.tpl', 'admin/view/template/module/currency_tools_import.tpl', 'admin/view/template/module/currency_tools.tpl', 'admin/view/template/module/currency_tools_form.tpl', 'admin/view/template/currency_provider/finmarket.tpl', 'admin/view/template/currency_provider/mnb.tpl', 'admin/view/template/currency_provider/apilayer.tpl', 'admin/view/template/currency_provider/bnm.tpl', 'admin/view/template/currency_provider/yahoo.tpl', 'admin/view/template/currency_provider/fixer.tpl', 'admin/view/template/currency_provider/okua.tpl', 'admin/view/template/currency_provider/keb.tpl', 'admin/view/template/currency_provider/tcmb.tpl', 'admin/view/template/currency_provider/bnr.tpl', 'admin/ct_cron.php', 'admin/model/module/currency_tools.php', 'admin/language/en-gb/module/currency_tools.php', 'admin/language/en-gb/currency_provider/keb.php', 'admin/language/en-gb/currency_provider/fixer.php', 'admin/language/en-gb/currency_provider/okua.php', 'admin/language/en-gb/currency_provider/bnm.php', 'admin/language/en-gb/currency_provider/finmarket.php', 'admin/language/en-gb/currency_provider/tcmb.php', 'admin/language/en-gb/currency_provider/yahoo.php', 'admin/language/en-gb/currency_provider/bnr.php', 'admin/language/en-gb/currency_provider/apilayer.php', 'admin/language/en-gb/currency_provider/mnb.php', 'admin/language/ru-ru/module/currency_tools.php', 'admin/language/ru-ru/currency_provider/bmd.php', 'admin/language/ru-ru/currency_provider/keb.php', 'admin/language/ru-ru/currency_provider/fixer.php', 'admin/language/ru-ru/currency_provider/okua.php', 'admin/language/ru-ru/currency_provider/finmarket.php', 'admin/language/ru-ru/currency_provider/tcmb.php', 'admin/language/ru-ru/currency_provider/yahoo.php', 'admin/language/ru-ru/currency_provider/bnr.php', 'admin/language/ru-ru/currency_provider/apilayer.php', 'admin/language/ru-ru/currency_provider/mnb.php', 'admin/controller/module/currency_tools.php', 'admin/controller/currency_provider/keb.php', 'admin/controller/currency_provider/fixer.php', 'admin/controller/currency_provider/okua.php', 'admin/controller/currency_provider/bnm.php', 'admin/controller/currency_provider/finmarket.php', 'admin/controller/currency_provider/tcmb.php', 'admin/controller/currency_provider/yahoo.php', 'admin/controller/currency_provider/bnr.php', 'admin/controller/currency_provider/apilayer.php', 'admin/controller/currency_provider/mnb.php', 'catalog/view/theme/default/template/module/currency_tools_widget.tpl', 'catalog/controller/module/currency_tools_widget.php');
    
    public function install()
    {
        //set cron stopped
        $this->config->save('currency_tools','cron_running',false);
        
        //currency.correction
        $query = $this->db->query("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "currency' AND COLUMN_NAME = 'correction'");
        if (!$query->num_rows)
        {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "currency` ADD `correction` DECIMAL(10,4) NOT NULL");
        }
        
        //currency.round_value
        $query = $this->db->query("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "currency' AND COLUMN_NAME = 'round_value'");
        if (!$query->num_rows)
        {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "currency` ADD `round_value` INT(2) NOT NULL DEFAULT '2'");
        }
        
        //currency.margin
        $query = $this->db->query("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "currency' AND COLUMN_NAME = 'margin'");
        if (!$query->num_rows)
        {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "currency` ADD `margin` DECIMAL(6,2) NOT NULL");
        }
        
        //currency.overheads
        $query = $this->db->query("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "currency' AND COLUMN_NAME = 'overheads'");
        if (!$query->num_rows)
        {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "currency` ADD `overheads` DECIMAL(10,4) NOT NULL");
        }
        
        //currency.deviation
        $query = $this->db->query("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "currency' AND COLUMN_NAME = 'deviation'");
        if (!$query->num_rows)
        {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "currency` ADD `deviation` DECIMAL(10,4) NOT NULL");
        }
        
        //currency.autorefresh
        $query = $this->db->query("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "currency' AND COLUMN_NAME = 'autorefresh'");
        if (!$query->num_rows)
        {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "currency` ADD `autorefresh` TINYINT(1) NOT NULL DEFAULT '1'");
        }
        
        //product.currency
        $query = $this->db->query("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "product' AND COLUMN_NAME = 'currency'");
        if (!$query->num_rows)
        {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "product` ADD `currency` CHAR(3) NOT NULL AFTER `price`");
        }
        
        //product.margin
        $query = $this->db->query("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "product' AND COLUMN_NAME = 'margin'");
        if (!$query->num_rows)
        {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "product` ADD `margin` DECIMAL(6,2) NOT NULL AFTER `price`");
        }
        
        //product.overheads
        $query = $this->db->query("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "product' AND COLUMN_NAME = 'overheads'");
        if (!$query->num_rows)
        {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "product` ADD `overheads` DECIMAL(10,4) NOT NULL AFTER `price`");
        }
        
        //product.overheads_currency
        $query = $this->db->query("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "product' AND COLUMN_NAME = 'overheads_currency'");
        if (!$query->num_rows)
        {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "product` ADD `overheads_currency` CHAR(3) NOT NULL AFTER `price`");
        }
        
        //product.refresh
        $query = $this->db->query("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "product' AND COLUMN_NAME = 'refresh'");
        if (!$query->num_rows)
        {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "product` ADD `refresh` TINYINT(1) NOT NULL DEFAULT '1'");
        }
        
        //manufacturer.currency
        $query = $this->db->query("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "manufacturer' AND COLUMN_NAME = 'currency'");
        if (!$query->num_rows)
        {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "manufacturer` ADD `currency` CHAR(3) NOT NULL AFTER `image`");
        }
        
        //manufacturer.margin
        $query = $this->db->query("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "manufacturer' AND COLUMN_NAME = 'margin'");
        if (!$query->num_rows)
        {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "manufacturer` ADD `margin` DECIMAL(6,2) NOT NULL AFTER `image`");
        }
        
        //manufacturer.overheads
        $query = $this->db->query("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "manufacturer' AND COLUMN_NAME = 'overheads'");
        if (!$query->num_rows)
        {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "manufacturer` ADD `overheads` DECIMAL(10,4) NOT NULL AFTER `image`");
        }
        
        //manufacturer.overheads_currency
        $query = $this->db->query("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "manufacturer' AND COLUMN_NAME = 'overheads_currency'");
        if (!$query->num_rows)
        {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "manufacturer` ADD `overheads_currency` CHAR(3) NOT NULL AFTER `image`");
        }
        
        //manufacturer.custom_rates
        $query = $this->db->query("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "manufacturer' AND COLUMN_NAME = 'custom_rates'");
        if (!$query->num_rows)
        {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "manufacturer` ADD `custom_rates` TINYINT(1) NOT NULL AFTER `currency`");
        }
        
        //currency_to_manufacturer table
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "currency_to_manufacturer` (`manufacturer_id` int(11) NOT NULL,  `currency` char(3) NOT NULL,  `value` decimal(15,8) NOT NULL,  PRIMARY KEY (`manufacturer_id`,`currency`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        
        //currency-to-country
        $countries = $this->config->get('currency_tools_countries');
        $json = json_decode(file_get_contents(dirname(__FILE__).'/currency.json'),true);
        foreach($json as $country_code=>$currency_code)
        {
            if (empty($countries[$country_code]) && $this->currency->has($currency_code))
            {
                $countries[$country_code] = $currency_code;
            }
        }
        $this->config->save('currency_tools','countries',$countries,0);
        
        //default product currency
        if (!$this->config->get('currency_tools_currency_product'))
        {
            $this->config->save('currency_tools','currency_product',$this->config->get('config_currency'),0);
        }
        
        //insert price fields
        require_once(DIR_SYSTEM.'library/currency_tools_helper.php');
        $helper = new CurrencyToolsHelper($this->registry);
        $helper->addPriceFields();
        $helper->setRefresh();
        
        //db triggers
        if ($this->hasTriggerAccess())
        {
            //drop db triggers
            $this->db->query("DROP TRIGGER IF EXISTS upd_product;");
            $this->db->query("DROP TRIGGER IF EXISTS ins_product_discount;");
            $this->db->query("DROP TRIGGER IF EXISTS ins_product_special;");
            $this->db->query("DROP TRIGGER IF EXISTS ins_product_option_value;");
            
            $this->db->query("CREATE TRIGGER upd_product BEFORE UPDATE ON " . DB_PREFIX . "product FOR EACH ROW
            BEGIN
            	IF (OLD.currency != NEW.currency || OLD.price != NEW.price) THEN
            		SET NEW.refresh = '1';
            	END IF;
            END;");
            
            $this->db->query("CREATE TRIGGER ins_product_discount BEFORE INSERT ON " . DB_PREFIX . "product_discount FOR EACH ROW
            BEGIN
            	UPDATE " . DB_PREFIX . "product op SET op.refresh = '1' WHERE op.product_id = NEW.product_id;
            END;");
            
            $this->db->query("CREATE TRIGGER ins_product_special BEFORE INSERT ON " . DB_PREFIX . "product_special FOR EACH ROW
            BEGIN
            	UPDATE " . DB_PREFIX . "product op SET op.refresh = '1' WHERE op.product_id = NEW.product_id;
            END;");
            
            $this->db->query("CREATE TRIGGER ins_product_option_value BEFORE INSERT ON " . DB_PREFIX . "product_option_value FOR EACH ROW
            BEGIN
            	UPDATE " . DB_PREFIX . "product op SET op.refresh = '1' WHERE op.product_id = NEW.product_id;
            END;");
        }
    }
    
    public function hasTriggerAccess()
    {
        $query = $this->dbQuery("SELECT VERSION() as mysql_version");
        $mysql_info = explode('-',$query->row['mysql_version']);
        $mysql_v = $mysql_info[0];
        
        if (version_compare($mysql_v,'5.0.2','<'))
        {
            return false;
        }
        elseif (version_compare($mysql_v,'5.1.6','>='))
        {
            return true;
        }
        
        if ($this->validate_ip(DB_HOSTNAME))
        {
            $sql = "SELECT * FROM INFORMATION_SCHEMA.SCHEMA_PRIVILEGES WHERE (GRANTEE LIKE '\'".DB_USERNAME."''@''%\'' OR GRANTEE LIKE '\'".DB_USERNAME."''@''".DB_HOSTNAME."\'' OR GRANTEE LIKE '\'".DB_USERNAME."''@''".gethostbyaddr(DB_HOSTNAME)."\'') AND (TABLE_SCHEMA = '".DB_DATABASE."' OR TABLE_SCHEMA = '".str_replace('_','\_',DB_DATABASE)."') AND (PRIVILEGE_TYPE = 'TRIGGER')";
        }
        else
        {
            $sql = "SELECT * FROM INFORMATION_SCHEMA.SCHEMA_PRIVILEGES WHERE (GRANTEE LIKE '\'".DB_USERNAME."''@''%\'' OR GRANTEE LIKE '\'".DB_USERNAME."''@''".DB_HOSTNAME."\'' OR GRANTEE LIKE '\'".DB_USERNAME."''@''".gethostbyname(DB_HOSTNAME)."\'') AND (TABLE_SCHEMA = '".DB_DATABASE."' OR TABLE_SCHEMA = '".str_replace('_','\_',DB_DATABASE)."') AND (PRIVILEGE_TYPE = 'TRIGGER')";
        }
        
        $query = $this->dbQuery($sql);
        if (DB_USERNAME == 'root' || $query->num_rows)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function validate_ip($host)
    {
        $parts = explode('.',$host);
        if (count($parts) != 4)
        {
            return false;
        }
        else
        {
            foreach($parts as $part)
            {
                if (!is_int($part))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
?>