<?php
#encode-me[ioncube]#
class AddistObject extends Controller
{
    protected $data = array('status'=>false);
    protected $path;
    protected $group;
    protected $code;
    protected $model;
    protected $info = array();
    protected $params = array();
    protected $settings = array();
    protected $url;
    protected $user_group_id;
    
	public function __get($key)
    {
        $object = $this->registry->get('addist_'.$key);
        if (!empty($object))
        {
            return $object;
        }
        else
        {
            return $this->registry->get($key);
        }
	}
    
    public function __construct($registry)
    {
        parent::__construct($registry);
        
        //startup libs
        if (version_compare(VERSION,'2.2.0','>='))
        {
            $currency = $this->registry->get('currency');
            if (empty($currency)) $this->registry->set('currency', new Cart\Currency($this->registry));
            $user = $this->registry->get('user');
            if (empty($user)) $this->registry->set('user', new Cart\User($this->registry));
        }
        
        //url object
        if (IS_ADMIN)
        {
            $this->url = new AddistUrl(HTTP_SERVER, $this->config->get('config_secure') || $this->config->get('config_ssl') ? HTTPS_SERVER : HTTP_SERVER);
        }
        else
        {
            $this->url = $registry->get('url');
        }
        
        if (!empty($_POST) && empty($this->request->post))
        {
            $this->request->post = $this->request->clean($_POST);
        }
        
        if (isset($this->request->post['addist_installer']['debug']))
        {
            $this->config->save('addist_installer','debug',$this->request->post['addist_installer']['debug']);
        }
        
        // Checking debug mode
        if (isset($this->request->get['debug']) || $this->config->get('addist_installer_debug'))
        {
            error_reporting(E_ALL);
            ini_set('display_errors','on');
        }
        
        //loading language
        $this->load_language('common/addist');
        
        //detecting current domain and host
        if (!defined('ADDIST_DOMAIN'))
        {
            $parts = parse_url(HTTP_SERVER);
            $domain = preg_replace('#^www.(.*)#','${1}',$parts['host']);
            define('ADDIST_DOMAIN',$domain);
        }
        
        //checking if extension
        if (empty($this->path))
        {
            return;
        }
        
        //parsing params
        list($this->group,$this->code,$this->model) = explode('/',$this->path);
        
        //loading extension
        $this->info = (array)$this->extension->load($this->path)->info;
        
        //loading meta params
        $this->params = array_merge($this->params,(array)$this->config->get($this->info['hash'].'_extension'));
        
        if (IS_ADMIN)
        {
            //check permission
            if (!empty($this->request->post[$this->code]) && !$this->validate())
            {
                unset($this->request->post[$this->code]);
                addMessage('error',$this->language->get('error_permission'));
            }
        
            if (isset($this->request->post[$this->code]['status']))
            {
                //uninstall mods on disable
                if ($this->config->get($this->code.'_status') && empty($this->request->post[$this->code]['status']))
                {
                    $this->extension->uninstall_mods($this->path);
                }
                
                //install mods on enable
                if (!$this->config->get($this->code.'_status') && !empty($this->request->post[$this->code]['status']))
                {
                    $this->extension->install_mods($this->path);
                }
            }
            elseif ($this->config->get($this->code.'_status') && !$this->extension->check_mods($this->path))
            {
                $this->extension->install_mods($this->path);
            }
            elseif (!$this->config->get($this->code.'_status') && $this->extension->check_mods($this->path))
            {
                $this->extension->uninstall_mods($this->path);
            }
            
            //updating system settings
            if (isset($this->request->post['config']))
            {
                $this->config->saveGroup('config',$this->request->post['config']);
            }
        
            //update meta params
            if (isset($this->request->post[$this->code]['extension']))
            {
                $this->params = array_merge($this->params,$this->request->post[$this->code]['extension']);
                $this->save_params();
                unset($this->request->post[$this->code]['extension']);
            }
            
            //updating settings
            if (isset($this->request->post[$this->code]))
            {
                $this->config->saveGroup($this->code,$this->request->post[$this->code]);
            }
        }
        
        //loading fields
        foreach($this->info['fields'] as $key=>$value)
        {
            $this->data[$key] = $value;
            $this->settings[$key] = $value;
        }
    
        //loading db settings
        foreach($this->config->filter($this->code) as $key=>$value)
        {
            $this->data[$key] = $value;
            $this->settings[$key] = $value;
        }
        
        //init extension object
        $this->extension->init($this->path);
    }
    
    public function save_params()
    {
        $this->config->save($this->info['hash'],'extension',$this->params);
    }
    
    public function redirect($url, $status = 302)
    {
        if (OC_VERSION == '2.x')
        {
            $this->response->redirect($url, $status);
        }
        else
        {
            parent::redirect($url, $status);
        }
    }
    
    public function load_view($template_file, $data = array())
    {
		if (OC_VERSION == '2.x')
        {
            if (!IS_ADMIN && version_compare(VERSION,'2.2','>='))
            {
                $parts = explode('/',$template_file);
        		$template_file = implode('/',array_slice($parts,2,count($parts)-2));
            }
            return $this->load->view($template_file,$data);
        }
        else
        {
    		if (is_file(DIR_TEMPLATE . $template_file))
            {
                extract($data);
                ob_start();
                require(DIR_TEMPLATE . $template_file);
                $output = ob_get_contents();
                ob_end_clean();
                return $output;
    		}
            else
            {
    			trigger_error('Error: Could not load template ' . DIR_TEMPLATE . $template_file . '!');
    			exit();				
    		}
        }
    }
    
    public function load_controller($route, $args = array())
    {
        if (OC_VERSION == '2.x')
        {
            return $this->load->controller($route, $args);
        }
        else
        {
            $action = new Action($route, $args);
            $file = class_exists('VQMod') ? VQMod::modCheck($action->getFile()) : $action->getFile();
            if (file_exists($file))
            {
                $class = $action->getClass();
                if (!class_exists($class))
                {
                    require_once($file);
                }
                $controller = new $class($this->registry);                                
    			if (is_callable(array($controller, $action->getMethod()), false))
                {
    				$result = call_user_func(array($controller, $action->getMethod()), $args);
                    if (!empty($controller->output))
                    {
                        return $controller->output;
                    }                    
                    else
                    {
                        return $result;
                    }
    			}
                else
                {                    
    				return $this->getChild($route, $args);
    			}
            }
            else
            {
                trigger_error('Error: Could not load controller ' . $route . '!');
                exit();					
            }
        }
    }
    
    public function load_model($filename)
    {
        //fix models
        if ($filename == 'setting/extension')
        {
            $filename = (version_compare(VERSION,'2.0') >= 0) ? 'extension/extension' : $filename;
        }
        if ($filename == 'sale/customer_group')
        {
            $filename = (version_compare(VERSION,'2.1') >= 0) ? 'customer/customer_group' : $filename;
        }
        
        //load
        $this->load->model($filename);
        
        //fix
        if ($filename == 'extension/extension')
        {
            $this->model_setting_extension = $this->model_extension_extension;
        }
        if ($filename == 'customer/customer_group')
        {
            $this->model_sale_customer_group = $this->model_customer_customer_group;
        }
    }
    
    public function load_language($filename)
    {
        $language = (array)$this->language->load($filename);
        $this->data = array_merge($this->data,$language);
    }
    
	public function validate()
    {
        if (IS_ADMIN && $this->registry->get('user') && $this->user->isLogged() && $this->user->hasPermission('modify', $this->group.'/'.$this->code))
        {
            return true;
        }
        elseif (!IS_ADMIN && (!$this->registry->get('user') || !$this->user->isLogged()))
        {
            return true;
        }
        else
        {
            return false;
        }
	}
    
    public function getLanguages()
    {
        $this->load_model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        foreach($languages as &$language)
        {
            if (version_compare(VERSION,'2.2.0','>='))
            {
                $language['image'] = 'language/'.$language['code'].'/'.$language['code'].'.png';
            }
            else
            {
                $language['image'] = '../image/flags/'.$language['image'];
            }
        }
        return $languages;
    }
    
    public function dbQuery($sql)
    {
        return $this->db->query($sql, true);
    }
    
    public function activated()
    {
        return true;
    }
}
?>