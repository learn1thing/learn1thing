<?php
#encode-me[string-],encode-me[ioncube]#
class AddistController extends AddistObject
{
    public function __construct($registry)
    {
        parent::__construct($registry);
    }
    
    public function update_extension()
    {
        $data = array();
        $data['trial'] = !empty($this->params['expires']);
        $extension = $this->extension->load($this->path);
        $data['release'] = !empty($extension->info['release']) ? $extension->info['release'] : 'stable';
        $this->installer->download($this->path,$data);
        $this->redirect($this->url->link($this->group.'/'.$this->code, 'token=' . $this->session->data['token'], 'SSL'));
    }        
    
    public function install()
    {
        $this->extension->install($this->path);
    }
    
    public function uninstall()
    {
        $this->extension->uninstall($this->path);
    }
    
    public function reinstall()
    {
        $this->extension->install($this->path);
        addMessage('success',$this->language->get('addist_success_repair'),true);
        $this->redirect($this->url->link($this->group.'/'.$this->code, 'token=' . $this->session->data['token'], 'SSL'));
    }
    
    public function install_mods()
    {
        $this->extension->install_mods($this->path);
        if (OC_VERSION == '2.x')
        {
            $this->extension->refresh_mods();
        }
        $this->redirect($this->url->link($this->group.'/'.$this->code, 'token=' . $this->session->data['token'], 'SSL'));
    }
    
    public function self_remove()
    {
        $this->extension->remove($this->path);
        addMessage('success',$this->language->get('addist_success_mods'));
        $this->redirect($this->url->link('extension/'.$this->group, 'token=' . $this->session->data['token'], 'SSL'));
    }
    
    public function backup_settings()
    {
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=".$this->model.'_v'.$this->params['version']."_backup.txt");
        header("Pragma: no-cache");
        header ("Expires: 0");
        exit(serialize($this->extension->backup($this->path)));
    }
    
    public function restore_settings()
    {
        if (isset($_FILES['file']))
        {
            $this->extension->restore($this->path,$_FILES['file']['tmp_name']);
            addMessage('success',$this->language->get('addist_success_restore'));
        }
    }
    
    public function setOutput()
    {
        //messages
        $this->data['success'] = $this->message->get('success');
		$this->data['error'] = $this->message->get('error');
        
        //breadcrumbs
        if (IS_ADMIN && empty($this->data['breadcrumbs']) && $this->user->isLogged())
        {
    		$this->data['breadcrumbs'] = array();
    
    		$this->data['breadcrumbs'][] = array(
    			'text'      => $this->language->get('text_home'),
    			'href'      => $this->url->link(OC_VERSION == '2.x' ? 'common/dashboard' : 'common/home', 'token=' . $this->session->data['token'], 'SSL'),
    			'separator' => false
    		);
    
    		$this->data['breadcrumbs'][] = array(
    			'text'      => $this->language->get('text_'.$this->group),
    			'href'      => $this->url->link('extension/'.$this->group, 'token=' . $this->session->data['token'], 'SSL'),
    			'separator' => ' :: '
    		);
    
    		$this->data['breadcrumbs'][] = array(
    			'text'      => $this->language->get('heading_title'),
    			'href'      => $this->url->link($this->group.'/'.$this->code, 'token=' . $this->session->data['token'], 'SSL'),
    			'separator' => ' :: '
    		);
        }
        
        //stores
        $this->data['stores'] = getStores();
        $this->data['store_id'] = STORE_ID;
        
        if (IS_ADMIN)
        {
            //default data
            if (empty($this->data['action']))
    		{
                $this->data['action'] = $this->url->link($this->group.'/'.$this->code, 'token=' . $this->session->data['token'], 'SSL');
    		}
            if (empty($this->data['cancel']))
            {
                $this->data['cancel'] = $this->url->link('extension/'.$this->group, 'token=' . $this->session->data['token'], 'SSL');
            }
    		
            $this->data['token'] = $this->session->data['token'];
            
            //bootstrap for oc 1.x
            if (OC_VERSION != '2.x')
            {
                $this->document->addScript('view/assets/js/bootstrap.min.js');
                $this->document->addStyle('view/assets/css/bootstrap.min.css');
                
                //font awesome
                $this->document->addStyle('view/assets/css/font-awesome.min.css');
            }
            
            //fix bootstrap
            $this->document->addStyle('view/assets/css/bootstrap.fix.css');
            
            //multi-select
            $this->document->addScript('view/assets/js/bootstrap-multiselect.js');
            $this->document->addStyle('view/assets/css/bootstrap-multiselect.css');
            
            //adding addist assets
            $this->document->addScript('view/assets/js/addist.js');
            $this->document->addStyle('view/assets/css/addist.css');
            
            //columns
            if (OC_VERSION == '2.x')
            {
                $this->data['header'] = $this->load_controller('common/header');
                $this->data['column_left'] = $this->load_controller('common/column_left');
                $this->data['footer'] = $this->load_controller('common/footer');
            }
            else
            {
                $this->data['header'] = $this->load_controller('common/header');
                $this->data['column_left'] = '';
                $this->data['footer'] = $this->load_controller('common/footer');
            }
            
            //addist tab
            if (get_class($this) != 'AddistInstaller')
            {
                if ($this->registry->get('user') && $this->user->hasPermission('modify', $this->group.'/'.$this->code))
                {
                    $this->data['addist_tab'] = $this->load_controller('addist/tab',array('group'=>$this->group,'code'=>$this->code,'model'=>$this->model));
                }
                else
                {
                    $this->data['addist_tab'] = '';
                }
            }
            
            //replacing jquery
            $html = $this->load_view($this->template, $this->data);
                
            if (OC_VERSION != '2.x')
            {
                $html = preg_replace(
                    array(
                        "#<script(.*)jquery(.*)></script>#",
                        "#<script(.*)jquery(.*)ui(.*)></script>#",
                    ),
                    array(
                        '<script type="text/javascript" src="view/assets/js/jquery-2.1.1.min.js"></script><script type="text/javascript" src="view/assets/js/jquery-migrate-1.2.1.min.js"></script>',
                        '<script type="text/javascript" src="view/assets/js/jquery-ui.min.js"></script>',
                    ),
                    $html,1
                );
            }
        }
        else
        {
            if (OC_VERSION == '2.x')
            {
                $html = $this->load_view($this->template, $this->data);
            }
            else
            {
                $html = parent::render();
            }
        }
        
        $this->response->setOutput($html);
        return $html;
    }
    
    public function redirect($url, $status = 302)
    {
        if (version_compare(VERSION,'2.3.0','>='))
        {
            global $ext_paths;
            $parts = explode('/',$url);
            if (in_array($parts[0],$ext_paths))
            {
                $url = 'extension/'.$url;
            }
        }
        
        if (OC_VERSION == '2.x')
        {
            $this->response->redirect($url, $status);
        }
        else
        {
            parent::redirect($url, $status);
        }
    }
}
?>