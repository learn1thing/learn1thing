<?php
#encode-me[string-],encode-me[ioncube]#
class AddistExtension extends AddistClass
{
	protected $registry;
    protected $server = 'http://api1.addist.ru/,http://api2.addist.ru/,http://api.addist.ru/,http://u-coder.ru/';
    protected $current_server;
    
    public function init($path)
    {
        list($group,$code,$model) = explode('/',$path);
        $info = $this->load($path)->info;
        $params = $this->config->get($info['hash'].'_extension');
        
        if ((empty($params['version']) || $params['version'] != $info['version']) && !empty($info['version']) && IS_ADMIN)
        {
            $this->install($path);
            addMessage('success',$this->language->get('addist_success_update'),true);
        }
    }
    
    public function load($path)
    {
        global $languages;
        list($group,$code,$model) = explode('/',$path);
        $file = DIR_SYSTEM.'addist/extension/'.$model.'/install.php';
        if (file_exists($file))
        {
            require_once($file);
            $extension = new $model($this->registry);
            
            $extension->info['fields'] = $extension->fields;
            $extension->info['status'] = $this->config->get($code.'_status');
            
            if (IS_ADMIN)
            {
                $langfile = DIR_SYSTEM.'../'.basename(DIR_APPLICATION).'/language/'.$languages[$this->config->get('config_language')]['directory'].'/'.$group.'/'.$code.'.php';
            }
            else
            {
                $langfile = DIR_SYSTEM.'../admin/language/'.$languages[$this->config->get('config_language')]['directory'].'/'.$group.'/'.$code.'.php';
            }
            
            if (!file_exists($langfile))
            {
                $langfile = str_replace('/'.$languages[$this->config->get('config_language')]['directory'].'/','/'.$GLOBALS['fnames']['english'].'/',$langfile);
            }
            
            if (file_exists($langfile))
            {
                include(realpath($langfile));
                if (isset($_['heading_title']))
                {
                    $extension->info['name'] = $_['heading_title'];
                }
                else
                {
                    $extension->info['name'] = '';
                }
            }
            elseif (IS_ADMIN)
            {
                $extension->info['name'] = '';
            }
            
            return $extension;
        }
        else
        {
            return false;
        }
    }
    
    public function install($path)
    {
        list($group,$code,$model) = explode('/',$path);
        $extension = $this->load($path);
        
        //set permission
        $this->load->model('user/user');
        $user = $this->model_user_user->getUser($this->user->getId());
        
        $this->load->model('user/user_group');
        $this->model_user_user_group->addPermission($user['user_group_id'],'access','addist/activation');
        $this->model_user_user_group->addPermission($user['user_group_id'],'access','addist/error');
        $this->model_user_user_group->addPermission($user['user_group_id'],'access','addist/tab');
        $this->model_user_user_group->addPermission($user['user_group_id'],'modify','addist/activation');
        $this->model_user_user_group->addPermission($user['user_group_id'],'modify','addist/error');
        $this->model_user_user_group->addPermission($user['user_group_id'],'modify','addist/tab');
        $this->model_user_user_group->addPermission($user['user_group_id'],'access',$group.'/'.$code);
        $this->model_user_user_group->addPermission($user['user_group_id'],'modify',$group.'/'.$code);
        if (version_compare(VERSION,'2.3.0','>='))
        {
            $this->model_user_user_group->addPermission($user['user_group_id'],'access','extension/'.$group.'/'.$code);
            $this->model_user_user_group->addPermission($user['user_group_id'],'modify','extension/'.$group.'/'.$code);
        }
        
        //adding fields
        if (!empty($extension->fields))
        {
            foreach($extension->fields as $key=>$value)
            {
                if (!$this->config->has($code.'_'.$key))
                {
                    $this->config->save($code,$key,$value,0);
                }
            }
        }
        
        //chmod
        foreach($extension->files as $file)
        {
            if (is_file(DIR_SYSTEM.'../'.$file) && is_writable(DIR_SYSTEM.'../'.$file))
            {
                exec('chmod 0777 '.realpath(DIR_SYSTEM.'../'.$file));
            }
        }
        
        if (!$this->config->has($code.'_status'))
        {
            $this->config->save($code,'status',false,0);
        }
        
        //save params
        $info = $this->load($path)->info;
        $params = (array)$this->config->get($info['hash'].'_extension');
        $params['version'] = $info['version'];
        $this->config->save($info['hash'],'extension',$params);
        
        //installing mod
        if (method_exists($extension,'install'))
        {
            $extension->install();
        }
        
        //add to db
        $key = OC_VERSION == '1.4.9.x' ? 'key' : 'code';
        $this->dbQuery("DELETE FROM `".DB_PREFIX."extension` WHERE `type` = '$group' AND `$key` = '$code'");
        $query = $this->dbQuery("INSERT INTO `".DB_PREFIX."extension` SET `type` = '$group', `$key` = '$code'");
        
        //success message
        addMessage('success',sprintf($this->language->get('addist_text_installed'),$extension->info['name']));
    }
    
    public function install_mods($path,$status = true)
    {
        list($group,$code,$model) = explode('/',$path);
        
        $error = false;
        
        //remove old ocmods
        if (OC_VERSION == '2.x')
        {
            $query = $this->db->query("SELECT * FROM `".DB_PREFIX."modification` WHERE `name` = 'addist' OR `code` = 'addist' OR `name` = '$model' OR `code` = '$model'");
            if ($query->num_rows)
            {
                $this->db->query("DELETE FROM `".DB_PREFIX."modification` WHERE `name` = 'addist' OR `code` = 'addist' OR `name` = '$model' OR `code` = '$model'");
            }
        }
        
        //remove old ocmod files
        if (is_dir(DIR_ADDIST.'extension/'.$model.'/ocmod'))
        {
            $files = glob(DIR_ADDIST.'extension/'.$model.'/ocmod/*.ocmod.xml');
            if ($files)
            {
                foreach($files as $file)
                {
                    @unlink($file);
                }
            }
            rmdir(DIR_ADDIST.'extension/'.$model.'/ocmod');
        }
        if (is_dir(DIR_ADDIST.'install/ocmod'))
        {
            $files = glob(DIR_ADDIST.'install/ocmod/*.ocmod.xml');
            if ($files)
            {
                foreach($files as $file)
                {
                    @unlink($file);
                }
            }
            rmdir(DIR_ADDIST.'install/ocmod');
        }
        
        //remove old vqmod files
        if (is_dir(DIR_ADDIST.'extension/'.$model.'/vqmod'))
        {
            $files = glob(DIR_ADDIST.'extension/'.$model.'/vqmod/*.xml');
            if ($files)
            {
                foreach($files as $file)
                {
                    @unlink($file);
                }
            }
        }
        
        //install core mod file
        if (is_file(DIR_ADDIST.'install/vqmod/'.OC_VERSION.'/addist.xml') && !$this->install_vqmod(DIR_ADDIST.'install/vqmod/'.OC_VERSION.'/addist.xml'))
        {
            $error = true;
        }
        
        //install extensions mods
        $files = $this->get_mods($path);
        if ($status && $files && !$error)
        {
            foreach($files as $file)
            {
                if (!$this->install_vqmod($file))
                {
                    $error = true;
                }
            }
        }
        
        //clear vqmod cache
        if (is_dir(DIR_SYSTEM.'../vqmod'))
        {
            if (version_compare(VERSION,'2.2.0','>='))
            {
                @unlink(DIR_SYSTEM.'../vqmod/xml/addist.xml');
                
                if ($files)
                {
                    foreach($files as $file)
                    {
                        @unlink(DIR_SYSTEM.'../vqmod/xml/'.basename($file));
                    }
                }
            }
            
            if (check_vqmod() > 0)
            {
                $files = glob(DIR_SYSTEM.'../vqmod/vqcache/*');
                if ($files)
                {
                    foreach($files as $file)
                    {
                        @unlink($file);
                    }
                }
            }
            
            if (is_file(DIR_SYSTEM.'../vqmod/checked.cache'))
            {
                @unlink(DIR_SYSTEM.'../vqmod/checked.cache');
            }
            
            if (is_file(DIR_SYSTEM.'../vqmod/mods.cache'))
            {
                @unlink(DIR_SYSTEM.'../vqmod/mods.cache');
            }
        }
        
        //refresh mods
        if (OC_VERSION == '2.x' && !$error && (check_vqmod() != 1 || version_compare(VERSION,'2.2.0','>=')))
        {
            if (!$this->refresh_mods())
            {
                addMessage('error',sprintf($this->language->get('addist_error_mods_refresh'),$this->url->link('extension/modification/refresh', 'token=' . $this->session->data['token'], 'SSL')));
            }
        }
        
        //success message
        if ($error)
        {
            addMessage('error',$this->language->get('addist_error_mods'));
        }
        else
        {
            addMessage('success',$this->language->get('addist_success_mods'));
        }
    }
    
    public function check_mods($path)
    {
        if (check_vqmod() > 0 && version_compare(VERSION,'2.2.0','<'))
        {
            $files = $this->get_mods($path);
            foreach($files as $file)
            {
                if (!is_file(DIR_SYSTEM.'../vqmod/xml/'.basename($file)))
                {
                    return false;
                }
            }
        }
        return true;
    }
    
    public function get_mods($path)
    {
        list($group,$code,$model) = explode('/',$path);
        
        $files = array();
        if (is_dir(DIR_ADDIST.'extension/'.$model.'/vqmod'))
        {
            if (OC_VERSION == '2.x')
            {
                if (is_dir(DIR_ADDIST.'extension/'.$model.'/vqmod/2.x'))
                {
                    $files = glob(DIR_ADDIST.'extension/'.$model.'/vqmod/2.x/*.xml');
                }
            }
            else
            {
                if (is_dir(DIR_ADDIST.'extension/'.$model.'/vqmod/1.x'))
                {
                    $files = glob(DIR_ADDIST.'extension/'.$model.'/vqmod/1.x/*.xml');
                }
            }
            
            if (is_dir(DIR_ADDIST.'extension/'.$model.'/vqmod/'.OC_VERSION))
            {
                $files = glob(DIR_ADDIST.'extension/'.$model.'/vqmod/'.OC_VERSION.'/*.xml');
            }
            
            if (is_dir(DIR_ADDIST.'extension/'.$model.'/vqmod/'.VERSION))
            {
                $files = glob(DIR_ADDIST.'extension/'.$model.'/vqmod/'.VERSION.'/*.xml');
            }
            
            if (is_dir(DIR_ADDIST.'extension/'.$model.'/vqmod/custom'))
            {
                $files = glob(DIR_ADDIST.'extension/'.$model.'/vqmod/custom/*.xml');
            }
        }
        return $files;
    }
    
    public function refresh_mods()
    {
        $maintenance = $this->config->get('config_maintenance');
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_URL, HTTPS_SERVER . 'index.php?route=extension/modification/refresh&token='.$this->session->data['token']);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, ini_get('open_basedir') ? 0: 1);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/4.0");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        
        if (isset($this->request->cookie[session_name()]))
        {
            curl_setopt($curl, CURLOPT_COOKIE, session_name().'='.$this->request->cookie[session_name()].'; path=/');
        }
        
        session_write_close();
        $result = curl_exec($curl);
        
        if (curl_errno($curl))
        {
            $status = false;
        }
        else
        {
            $status = true;
        }
        
        curl_close($curl);
        session_start();
        
        $this->config->save('config','maintenance',$maintenance);
        
        return $status;
    }
    
    public function install_vqmod($file)
    {
        if (check_vqmod() > 0 && version_compare(VERSION,'2.2.0','<'))
        {
            $xmlstring = file_get_contents($file);
            $xmlstring = str_replace('"admin/','"'.basename(DIR_APPLICATION).'/',$xmlstring);
            file_put_contents(DIR_SYSTEM.'../vqmod/xml/'.basename($file),$xmlstring);
            if (file_exists(DIR_SYSTEM.'../vqmod/xml/'.basename($file)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            if (is_file(DIR_SYSTEM.'../vqmod/xml/'.basename($file)))
            {
                @unlink(DIR_SYSTEM.'../vqmod/xml/'.basename($file));
            }
            $this->install_ocmod($this->vqmod_to_ocmod($file),basename($file,'.xml'));
            return true;
        }
    }
    
    public function vqmod_to_ocmod($file)
    {
    	$dom = new DOMDocument('1.0', 'UTF-8');
    	$dom->preserveWhiteSpace = false;
    	$dom->loadXml(file_get_contents($file));
        
        $delimiter = version_compare(VERSION,'2.1.0','>') ? '|' : ',';
        
        /*$ocmod = '<?xml version="1.0" encoding="UTF-8"?>'."\n";*/
        $ocmod = '<modification>'."\n";
        $ocmod .= "\t".'<name>'.$dom->getElementsByTagName('id')->item(0)->textContent.'</name>'."\n";
        $ocmod .= "\t".'<code>'.basename($file,'.xml').'</code>'."\n";
        $ocmod .= "\t".'<version>'.$dom->getElementsByTagName('version')->item(0)->textContent.'</version>'."\n";
        $ocmod .= "\t".'<author>addist.ru</author>'."\n\n";
        $ocmod .= "\t".'<link>http://addist.ru/</link>'."\n";
        
        $files = $dom->getElementsByTagName('modification')->item(0)->getElementsByTagName('file');
        foreach($files as $file)
        {
            $paths_filtered = array();
            $paths = explode(',',$file->getAttribute('name'));
            foreach($paths as $path)
            {
                if (substr_count($path,'*') || is_file(DIR_SYSTEM.'../'.trim($path)))
                {
                    $paths_filtered[] = $path;
                }
            }
            
            if (version_compare(VERSION,'2.0.1','>='))
            {
                $paths_array = array(implode($delimiter,$paths_filtered));
            }
            else
            {
                $paths_array = $paths_filtered;
            }
            
            foreach($paths_array as $paths_item)
            {
                $ocmod .= "\t".'<file path="'.$paths_item.'">'."\n";
                
                $operations = $file->getElementsByTagName('operation');
                foreach($operations as $operation)
                {
                    $ocmod .= "\t\t".'<operation error="skip">'."\n";
                    
                    $ignoreif = $operation->getElementsByTagName('ignoreif')->item(0);
                    $search = $operation->getElementsByTagName('search')->item(0);
                    $add = $operation->getElementsByTagName('add')->item(0);
                    
                    if ($ignoreif)
                    {
                        $ocmod .= "\t\t\t".'<ignoreif>'.$ignoreif->textContent.'</ignoreif>'."\n";
                    }
                    
                    if ($search && $add)
                    {
                        $ocmod .= "\t\t\t".'<search'.($search->getAttribute('regex') == 'true' ? ' regex="true"' : '').'><![CDATA['.$search->textContent.']]></search>'."\n";
                        $ocmod .= "\t\t\t".'<add position="'.$search->getAttribute('position').'"><![CDATA['.$add->textContent.']]></add>'."\n";
                    }
                    
                    $ocmod .= "\t\t".'</operation>'."\n";
                }
                
                $ocmod .= "\t".'</file>'."\n\n";
            }
        }
        $ocmod = substr($ocmod,0,strlen($ocmod)-1);
        $ocmod .= '</modification>';
        return $ocmod;
    }
    
    public function install_ocmod($xmlstring,$code)
    {
        $xml = new SimpleXMLElement($xmlstring);
        
        if (version_compare(VERSION,'2.0.0.0','>'))
        {
            $query_check = $this->dbQuery("SELECT * FROM " . DB_PREFIX . "modification WHERE `code` = '" . trim($code) . "'");
            if ($query_check->num_rows)
            {
                $this->dbQuery("UPDATE " . DB_PREFIX . "modification SET `xml` = '".$this->db->escape($xmlstring)."', `name` = '".trim($xml->name)."', `version` = '".trim($xml->version)."', `author` = '".trim($xml->author)."', `link` = '".trim($xml->link)."', `status` = '1' WHERE `code` = '".trim($code)."'");
            }
            else
            {
                $this->dbQuery("INSERT IGNORE INTO " . DB_PREFIX . "modification SET `xml` = '".$this->db->escape($xmlstring)."', `name` = '".trim($xml->name)."', `version` = '".trim($xml->version)."', `code` = '".trim($code)."', `author` = '".trim($xml->author)."', `link` = '".trim($xml->link)."', `status` = '1', `date_added` = NOW()");
            }
        }
        else
        {
            $query_check = $this->dbQuery("SELECT * FROM " . DB_PREFIX . "modification WHERE `name` = '".trim($code)."'");
            if ($query_check->num_rows)
            {
                $this->dbQuery("UPDATE " . DB_PREFIX . "modification SET `code` = '".$this->db->escape($xmlstring)."', `name` = '".trim($code)."', `version` = '".trim($xml->version)."', `author` = '".trim($xml->author)."', `link` = '".trim($xml->link)."', `status` = '1' WHERE `name` = '".trim($code)."'");
            }
            else
            {
                $this->dbQuery("INSERT IGNORE INTO " . DB_PREFIX . "modification SET `code` = '".$this->db->escape($xmlstring)."', `name` = '".trim($code)."', `version` = '".trim($xml->version)."', `author` = '".trim($xml->author)."', `link` = '".trim($xml->link)."', `status` = '1', `date_added` = NOW()");
            }
        }
    }
    
    public function uninstall($path)
    {
        list($group,$code,$model) = explode('/',$path);
        $extension = $this->load($path);
        
        //uninstall extension
        if (method_exists($extension,'uninstall'))
        {
            $extension->uninstall();
        }
        
        //uninstalling mods
        $this->uninstall_mods($path);
        
        //removing configs
        $this->config->remove($code);
        
        //removing extension params
        $this->config->remove($extension->info['hash']);
        
        //remove from db
        $key = OC_VERSION == '1.4.9.x' ? 'key' : 'code';
        $this->dbQuery("DELETE FROM " . DB_PREFIX . "extension WHERE `type` = '$group' AND `$key` = '$model'");
        
        //success message
        addMessage('success',sprintf($this->language->get('addist_text_uninstalled'),$extension->info['name']),true);
    }
    
    public function uninstall_mods($path)
    {
        $refresh = false;
        if (OC_VERSION =='2.x')
        {
            $query = $this->db->query("SELECT * FROM `".DB_PREFIX."modification` WHERE `name` = 'addist' OR `code` = 'addist'");
            if ($query->num_rows)
            {
                $refresh = true;
            }
        }
        
        $files = $this->get_mods($path);
        if ($files)
        {
            foreach($files as $file)
            {
                if (is_file(DIR_SYSTEM.'../vqmod/xml/'.basename($file)))
                {
                    @unlink(DIR_SYSTEM.'../vqmod/xml/'.basename($file));
                }
                if (OC_VERSION =='2.x' && check_vqmod() != 1)
                {
                    $query = $this->db->query("SELECT * FROM `".DB_PREFIX."modification` WHERE `name` = '".basename($file,'.xml')."' OR `code` = '".basename($file,'.xml')."'");
                    if ($query->num_rows)
                    {
                        $this->db->query("DELETE FROM `".DB_PREFIX."modification` WHERE `name` = '".basename($file,'.xml')."' OR `code` = '".basename($file,'.xml')."'");
                        $refresh = true;
                    }
                }
            }
        }
        
        if (OC_VERSION =='2.x' && $refresh)
        {
            $this->refresh_mods();
        }
    }
    
    public function remove($path)
    {
        $this->uninstall($path);
        $extension = $this->load($path);
        foreach($extension->files as $file)
        {
            if (file_exists(DIR_SYSTEM.'../'.$file))
            {
                unlink(DIR_SYSTEM.'../'.$file);
                $subfiles = glob(dirname($file).'/*');
                if (!count($subfiles))
                {
                    if (is_dir(dirname($file)))
                    {
                        rmdir(dirname($file));
                    }
                }
            }
        }
    }
    
    public function getAPI()
    {
        $servers = explode(',',$this->server);
        foreach($servers as $server)
        {
            $server = trim($server);
            if ($server)
            {
                $this->current_server = $server;
                $url_params = parse_url($server);
                $host = $url_params['host'];
                if ($this->checkHost($host))
                {
                    return $server;
                }
            }
        }
    }
    
    public function checkHost($host)
    {
        if (gethostbyname($host) != $host)
        {
            $catalog = defined('HTTP_CATALOG') ? HTTP_CATALOG : HTTP_SERVER;
            if ($this->fileGet($this->current_server.'index.php?catalog='.$catalog) == 'error')//.'&token='.$this->session->data['token']
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    
	public function fileGet($url, $data = array(), $user_agent = 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0', $cookiefile = null)
    {
		if (extension_loaded('curl'))
        {
        	$curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, ini_get('open_basedir') ? 0: 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_AUTOREFERER, true);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 3);
            curl_setopt($curl, CURLOPT_TIMEOUT, 120);
            if ($cookiefile)
            {
                curl_setopt($curl, CURLOPT_COOKIEFILE, $cookiefile);
                curl_setopt($curl, CURLOPT_COOKIEJAR, $cookiefile);
            }
            curl_setopt($curl, CURLOPT_USERAGENT, $user_agent);
            if ($data)
            {
                if (is_array($data) || is_object($data))
                {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
                }
                elseif (is_string($data))
                {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                }
            }
        	$contents = curl_exec($curl);
            
        	curl_close($curl);
            
            return $contents;
        }
        else
        {
            addMessage('error',$this->language->get('addist_error_curl'));
            return false;
        }
	}
    
    public function apiRequest($route,$get=array(),$post=array(),$json=true)
    {
        $api = $this->getAPI();
        if ($api && isset($this->session->data['token']))
        {
            $catalog = defined('HTTP_CATALOG') ? HTTP_CATALOG : HTTP_SERVER;
            $this->config->save('addist','token',$this->session->data['token']);
            $url = $api.'index.php?route='.$route.'&catalog='.$catalog.'&token='.$this->session->data['token'].(function_exists('ioncube_loader_version') ? '&ioncube_version='.ioncube_loader_version() : '').'&'.http_build_query($get);
            $response = $this->fileGet($url,$post);
            
            if ($json)
            {
                return json_decode($response,true);
            }
            else
            {
                return $response;
            }
        }
        else
        {
            return false;
        }
    }
    
    public function backup($path)
    {
        list($group,$code,$model) = explode('/',$path);
        $extension = $this->load($path);
        
        if (method_exists($extension,'backup'))
        {
            return $extension->backup();
        }
        else
        {
            return $this->config->filter($code);
        }
    }
    
    public function restore($path,$file)
    {
        list($group,$code,$model) = explode('/',$path);
        $extension = $this->load($path);
        
        $data =  unserialize(file_get_contents($file));
        if (method_exists($extension,'restore'))
        {
            return $extension->restore($data);
        }
        else
        {
            $this->config->saveGroup($code,$data);
        }
    }
    
    public function zipFiles($folder,$dest)
    {
        // Get real path for our folder
        $rootPath = str_replace('\\','/',realpath($folder));
        
        // Initialize archive object
        $zip = new ZipArchive;
        $zip->open($dest, ZipArchive::CREATE);
        
        // Create recursive directory iterator
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($rootPath),
            RecursiveIteratorIterator::LEAVES_ONLY
        );
        
        foreach ($files as $name => $file) {
            // Get real path for current file
            $filePath = str_replace('\\','/',$file->getRealPath());
            
            // Add current file to archive
            if ($filePath != realpath($folder) && is_file($filePath))
            {
                $zip->addFile($filePath,str_replace($rootPath.'/','',$filePath));
            }
        }
        
        // Zip archive will be created only after closing object
        $zip->close();
    }
    
    public function getRealFile($file)
    {
        if (version_compare(VERSION,'2.0','>='))
        {
            $file = str_replace(str_replace('\\','/',realpath(DIR_MODIFICATION)).'/','',$file);
        }
        $file = str_replace(str_replace('\\','/',realpath(DIR_SYSTEM.'../')).'/','',$file);
        $file = preg_replace('#^vqmod/vqcache/vq2-(.*)#','${1}',$file);
        $file = preg_replace('#system_(storage_)?modification_#','',$file);
        
        $parts = explode('_',$file);
        
        $file = '';
        $path = '';
        $delemiter = '';
        foreach($parts as $part)
        {
            $path = $file.$delemiter.$part;
            if (file_exists(DIR_SYSTEM.'../'.$path))
            {
                $delemiter = '/';
                $file = $path;
                continue;
            }
            else
            {
                $delemiter = '_';
                $file = $path;
                continue;
            }
        }
        
        return $file;
    }
}
?>