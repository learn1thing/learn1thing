<?php 

/**
 * Get Product By Product ID
 * @author thetaramolor@gmail.com
 * @param  product_id <int>
 * @return string
 */
function get_product_detail( $id )
{
	$ch = curl_init();

	curl_setopt($ch,CURLOPT_URL, "https://store.learn1thing.com/dashboard/api/v1/product/".$id);
		//curl_setopt($ch,CURLOPT_HTTPHEADER, $parameters);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json'
		));
	    //execute post
	$result = curl_exec($ch);
	$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	    //close connection
	curl_close($ch);
	$response = json_decode($result);
	return $response;
}

function get_product( $email ) 
{
	$ch = curl_init();
	$parameters = array(
		'email' => $email,
//		'type' => 'online'
		);

	curl_setopt($ch,CURLOPT_URL, "https://store.learn1thing.com/dashboard/api/v1/product");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	curl_setopt($ch,CURLOPT_POSTFIELDS, $parameters);

	    //execute post
	$result = curl_exec($ch);

	    //close connection
	curl_close($ch);
//	print_r(json_decode($result)->data->items);
	return $result;
}

function decrypt_password( $email )
{
	$decode = openssl_decrypt(base64_decode(strtr($email, '-_,', '+/=')), 'AES-128-ECB', 'l1tmiddleware');

	return $decode;
}

function checkSurveyLink( $order_id )
{
	$ch = curl_init();
	$parameters = array(
		'user_id' => 15,
		'order_id' => $order_id,
		//'buysurvey' => true,
		);

	curl_setopt($ch,CURLOPT_URL, "https://store.learn1thing.com/dashboard/api/v1/getOrderHistoryStore");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	curl_setopt($ch,CURLOPT_POSTFIELDS, $parameters);
	
	    //execute post
	$result = curl_exec($ch);
	
	    //close connection
	curl_close($ch);
	
	$response = json_decode($result);
//	print_r($response); 
	$products = array();
	foreach ($response->data->products as $product) {
		$result = array(
			'product_id' => $product->product_id,
			'profiler_url' => $product->profiler_url,
			'course_url'  => $product->course_url,
			'bbb_link' => $product->bbb_link
			);

		array_push($products, $result);
	}


	$data = array(
		'order_id' => $response->data->info->order_id,
		'customer_id' => $response->data->info->customer_id,
		'items' => $products
		);

	return $data;
}

function uniq_code() 
{
	$generate = substr(md5(uniqid(mt_rand(), true)) , 0, 8);

	return $generate;
}

function createorderhistory( $parameters )
{
	//var_dump($parameters);die();
    //$get_courses = get_courses($parameters['order_code'], $parameters['code']);

        

	$ch = curl_init();
//	$param = json_decode($parameters);	
	$test = array(
		'order_code' => $parameters['order_code'],
		'customer_id' => $parameters['customer_id'],
		//'products' => json_encode($get_courses->data->products)
		'products' => json_encode($parameters['products'])
	);
//	print_r($test);

	curl_setopt($ch,CURLOPT_URL, "https://store.learn1thing.com/dashboard/api/v1/createorderhistory");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	curl_setopt($ch,CURLOPT_POSTFIELDS, $test);
	
	    //execute post
	$result = curl_exec($ch);
	
	    //close connection
	curl_close($ch);
	
//	$response = json_decode($result);
//	$param = json_decode($parameters);
//	print_r($param);
//	get_courses($parameters['order_code'], $parameters['code']);
//	return $param;	
	return $result;
}

function comment( $code )
{
	$ch = curl_init();

	curl_setopt($ch,CURLOPT_URL, "https://store.learn1thing.com/dashboard/api/v1/log/order-schedule/".$code);
		//curl_setopt($ch,CURLOPT_HTTPHEADER, $parameters);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json'
		));
	    //execute post
	$result = curl_exec($ch);
	$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	    //close connection
	curl_close($ch);
	$response = json_decode($result);
	$result = array();
	// foreach ($response->data as $coach) {
	// 	$courses = array(
	// 		'product_id' => $coach->product_id,
	// 		'details' => json_decode($coach->details)
	// 		);
	// 	array_push( $result, $courses );
	// }

	return $result;
}

function get_courses( $order_id, $code )
{
	// print_r('order_id nya='.$order_id);
	// print_r('order_code nya='.$code);
	$ch = curl_init();
	$parameters = array(
		'user_id' => 15,
		'order_id' => $order_id,
		'buysurvey' => true,
		'code' => $code
		);
//	print_r($order_id);
	curl_setopt($ch,CURLOPT_URL, "https://store.learn1thing.com/dashboard/api/v1/getOrderHistoryStore");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	curl_setopt($ch,CURLOPT_POSTFIELDS, $parameters);
	
	    //execute post
	$result = curl_exec($ch);
	
	    //close connection
	curl_close($ch);
	
	$response = json_decode($result);
	//print_r($response);
	$products = array();
	$link     = array();

	if (is_object($response)) {
		
		if ($response->status->code == 200) {

			foreach ($response->data->products as $product) {
				$result = array(
					'product_id' => $product->product_id,
					'courses'    => $product->courses,
					'coachs'    => isset($product->coaches) ? $product->coaches : []
					);
				array_push($products, $result);

				$link_course = array(
					'course_url' => $product->course_url
					);
				array_push($link, $link_course);
			}

			$data = array(
				'order_id' => $response->data->info->order_id,
				'customer_id' => $response->data->info->customer_id,
				'items' => $products
				);

			foreach ($data['items'] as $item) { 
				foreach ($item['courses'] as $course) {
					if (!empty($item['coachs']) && count($item['coachs']) > 0) {
						foreach ($item['coachs'] as $i => $coach) {
							foreach ($coach->details as $detail) {
								$enroll = array(
								'course_id' => $course->course_id,
								'enrol_id'  => $course->enrol_id,
								'shortname' => $course->shortname,
								'email'     => $coach->email,
								'email_user' => $response->data->info->email,
								'product_id' => $item['product_id'],
								'timestart' => $detail->timestart,
								'timeend' 	 => $detail->timeend
								);
						set_enroll( $enroll );
							}
						}
					} else {
						$enroll = array(
	                                        'course_id' => $course->course_id,
	                                        'enrol_id'  => $course->enrol_id,
	                                        'shortname' => $course->shortname,
	                                        'email'     => decrypt_password( $_COOKIE['email'] ),
	                                        'email_user' => $response->data->info->email,
	                                        'product_id' => $item['product_id'],
	                                        'timestart' => '-',
	                                        'timeend'       => '-'
	                                        );
	                    set_enroll( $enroll );
					}
				} //end foreach course
			}//end foreach item
		} //end if respone 200

	}

	return $response;
}

function set_enroll( $request )
{
	$ch = curl_init();
	$parameters = array(
		'course_id' => $request['course_id'],
		'enrol_id'  => $request['enrol_id'],
		'shortname' => $request['shortname'],
		'email'     => $request['email'],
		'product_id'=> $request['product_id'],
		'email_user' => $request['email_user'],
		'timestart' => $request['timestart'],
		'timeend'  => $request['timeend']
		);
//	print_r( $parameters ); exit;
	curl_setopt($ch,CURLOPT_URL, "https://store.learn1thing.com/dashboard/api/v1/enrollUser");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	curl_setopt($ch,CURLOPT_POSTFIELDS, $parameters);
	    //execute post
	$result = curl_exec($ch);
	$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	    //close connection
	curl_close($ch);
	$response = json_decode($result);
//print_r($response); exit;	
	return $response;
}

function payment_scheduler( $code, $order_id )
{
	$ch = curl_init();
	$parameters = array(
		'code' => $code,
		'order_id' => $order_id
		);

	curl_setopt($ch,CURLOPT_URL, "https://store.learn1thing.com/dashboard/api/v1/log/order-schedule/payment");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	curl_setopt($ch,CURLOPT_POSTFIELDS, $parameters);
	
	    //execute post
	$result = curl_exec($ch);
	
	    //close connection
	curl_close($ch);
	
	$response = json_decode($result);

	return $response;
}

function sendMail( $request )
{
	$ch = curl_init();
	$parameters = array(
		'email' => $request['email'],
		'order_id' => $request['order_id'],
		'name_user' => $request['name_user'],
		'links' 	=> $request['links']
		);
	
	curl_setopt($ch,CURLOPT_URL, "https://store.learn1thing.com/dashboard/api/v1/coach/mail");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	curl_setopt($ch,CURLOPT_POSTFIELDS, $parameters);
	
	    //execute post
	$result = curl_exec($ch);
	
	    //close connection
	curl_close($ch);
	
	$response = json_decode($result);

	return $response;
}

function reset_password( $request )
{
	$ch = curl_init();
	$parameters = array(
		'new_password' => $request['new_password'],
		'password_confirmation' => $request['password_confirmation'],
		'customer_id' => $request['customer_id']
		);
	
	curl_setopt($ch,CURLOPT_URL, "https://store.learn1thing.com/dashboard/api/v1/reset-old-password");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	curl_setopt($ch,CURLOPT_POSTFIELDS, $parameters);
	
	    //execute post
	$result = curl_exec($ch);
	
	    //close connection
	curl_close($ch);
	
	$response = json_decode($result);

	return $response;
}

function enrolment( $request )
{
	$ch = curl_init();
	$parameters = array(
		'id' => $request['id'],
		'user_enrolments_id' => $request['user_enrolments_id']
		);

	curl_setopt($ch,CURLOPT_URL, "https://store.learn1thing.com/dashboard/api/v1/log/order-schedule/user/enrolment");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	curl_setopt($ch,CURLOPT_POSTFIELDS, $parameters);
	    //execute post
	$result = curl_exec($ch);
	$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	    //close connection
	curl_close($ch);
	$response = json_decode($result);
	
	return $response;
}

function storelink( $request )
{
	$ch = curl_init();
	$parameters = array(
		'order_id' => $request['order_id'],
		'link' => $request['link'],
		'product_id' => $request['product_id'],
		);

	curl_setopt($ch,CURLOPT_URL, "https://store.learn1thing.com/dashboard/api/v1/storelink");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	curl_setopt($ch,CURLOPT_POSTFIELDS, $parameters);
	    //execute post
	$result = curl_exec($ch);
	$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	    //close connection
	curl_close($ch);
	$response = json_decode($result);
	
	return $response;
}
