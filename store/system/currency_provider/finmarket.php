<?php
class cp_finmarket extends AddistClass
{
    public $countries = array('10148'=>'RUB','10134'=>'UAH','10120'=>'BYR','10123'=>'KZT');
    
    public function getCurrency()
    {
        $config = $this->config->filter('finmarket');
        return isset($config['country_id']) ? $this->countries[$config['country_id']] : 'RUB';
    }
    
    public function getRates()
    {
        $rates = array();
        
        $config = $this->config->filter('finmarket');
        $country_id = isset($config['country_id']) ? $config['country_id'] : '10148';
        $time_offset = isset($config['time_offset']) ? $config['time_offset'] : '86400';
        
        $time = time()+$time_offset;
        $content = fileGet('http://www.finmarket.ru/currency/rates/?id='.$country_id.'&bd='.(int)date("d",$time).'&bm='.(int)date("m",$time).'&by='.(int)date("Y",$time));
        if ($content)
        {
            libxml_use_internal_errors(true);
            $doc = new DOMDocument();
            $doc->loadHTML($content);
            
            $xpath = new DOMXPath($doc);
            $rows = $xpath->query('//table[@class="karramba"]/tbody/tr');
            if ($rows)
            {
                foreach($rows as $row)
                {
                    $code = $xpath->query('td',$row)->item(0)->nodeValue;
                    $rate = (float)str_replace(',','.',$xpath->query('td',$row)->item(3)->nodeValue)/(float)preg_replace("/[^0-9+]/",'',$xpath->query('td',$row)->item(2)->nodeValue);
                    $rates[$code] = $rate;
                }
            }
        }
        
        return $rates;
    }
}
?>