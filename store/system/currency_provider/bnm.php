<?php
class cp_bnm extends AddistClass
{
    public function getCurrency()
    {
        $config = $this->config->filter('bnr');
        return isset($config['currency']) ? $config['currency'] : 'MDL';
    }
    
    public function getRates()
    {
        $rates = array();
        $content = file_get_contents('http://bnm.md/ru/export-official-exchange-rates?date='.date('d.m.Y'));
        if ($content)
        {
        	$lines = explode("\n", trim($content));
            for($i=3;$i<(count($lines)-4);$i++)
            {
                $parts = explode(';',$lines[$i]);
        		$code = $parts[2];
        		$value = (float)str_replace(',','.',$parts[4])/$parts[3];
                if ($value) $rates[$code] = 1/$value;
            }
        }
        return $rates;
    }
}
?>