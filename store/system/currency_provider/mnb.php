<?php
class cp_mnb extends AddistClass
{
    public function getCurrency()
    {
        $config = $this->config->filter('mnb');
        return isset($config['currency']) ? $config['currency'] : 'HUF';
    }
    
    public function getRates()
    {
        $rates = array();
        
        $content = fileGet('https://www.mnb.hu/en/arfolyamok');
        
        libxml_use_internal_errors(true);
        $doc = new DOMDocument();
        $doc->loadHTML($content);
        
        $xpath = new DOMXPath($doc);
        $rows = $xpath->query('//table[@class="datatable"]/tbody/tr');
        
        if ($rows)
        {
            foreach($rows as $row)
            {
                $node1 = $xpath->query('td',$row)->item(0);
                $node2 = $xpath->query('td',$row)->item(3);
                $rates[$node1->nodeValue] = (float)$node2->nodeValue;
            }
        }
        
        return $rates;
    }
}
?>