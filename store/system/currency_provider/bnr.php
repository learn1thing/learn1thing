<?php
class cp_bnr extends AddistClass
{
    public function getCurrency()
    {
        $config = $this->config->filter('bnr');
        return isset($config['currency']) ? $config['currency'] : 'RON';
    }
    
    public function getRates()
    {
        $rates = array();
        
		$content = fileGet('http://www.bnr.ro/nbrfxrates.xml');
        $xml = simplexml_load_string($content);
        if ($xml)
        {
            foreach($xml->Body->Cube->children() as $rate)
            {
                $code = (string)$rate->attributes()->currency;
                $rates[$code] = (float)$rate;
            }
        }
        
        return $rates;
    }
}
?>