<?php
class cp_fixer extends AddistClass
{
    public function getCurrency()
    {
        $config = $this->config->filter('fixer');
        return isset($config['currency']) ? $config['currency'] : $this->config->get('config_currency');
    }
    
    public function getRates()
    {
        $rates = array();
        
		$json = json_decode(fileGet('http://api.fixer.io/latest?base='.$this->getCurrency()),true);
        
        if ($json && !empty($json['rates']))
        {
            foreach($json['rates'] as $code=>$rate)
            {
                $rates[$code] = 1/$rate;
            }
        }
        
        return $rates;
    }
}
?>