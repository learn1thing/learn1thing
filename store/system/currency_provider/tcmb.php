<?php
class cp_tcmb extends AddistClass
{
    public function getCurrency()
    {
        return 'TRY';
    }
    
    public function getRates()
    {
        $rates = array();
        
		$content = fileGet('http://www.tcmb.gov.tr/kurlar/today.xml');
        if ($content)
        {
            $xml = simplexml_load_string($content);
            foreach ($xml->Currency as $currency)
            {
                $code = trim($currency['CurrencyCode']);
                $rate = (float)trim($currency->ForexSelling)*(int)trim($currency->Unit);
                if ($rate)
                {
                    $rates[$code] = $rate;
                }
            }
        }
        
        return $rates;
    }
}
?>