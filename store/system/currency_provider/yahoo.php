<?php
class cp_yahoo extends AddistClass
{
    public function getCurrency()
    {
        $config = $this->config->filter('yahoo');
        return isset($config['currency']) ? $config['currency'] : 'USD';
    }
    
    public function getRates()
    {
        $rates = array();
        
        $data = array();
        
        $config = $this->config->filter('yahoo');
        $currency = isset($config['currency']) ? $config['currency'] : 'USD';
        
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "currency");
        
		foreach ($query->rows as $result)
        {
            if ($result['code'] != $currency)
			{
                $data[] = $currency . $result['code'] . '=X';
			}
		}
        
		$content = fileGet('http://download.finance.yahoo.com/d/quotes.csv?s=' . implode(',', $data) . '&f=sl1&e=.csv');
        
        if ($content)
        {
    		$lines = explode("\n", trim($content));
            if ($lines)
            {
        		foreach ($lines as $line)
                {
        			$code = substr($line, 4, 3);
        			$value = substr($line, 11, 6);
                    if ($value) $rates[$code] = 1/$value;
        		}
            }
        }
        
        return $rates;
    }
}
?>