<?php
class cp_okua extends AddistClass
{
    public function getCurrency()
    {
        $config = $this->config->filter('okua');
        return isset($config['currency']) ? $config['currency'] : 'UAH';
    }
    
    public function getRates()
    {
        $rates = array();
        
        $json = json_decode(fileGet('https://obmenka.kharkov.ua/rate/list'),true);
        foreach($json as $currency)
        {
            if ($currency['direction']['currencyTo']['name'] == 'UAH')
            {
                $code = $currency['direction']['currencyFrom']['name'];
                $rate = $currency['amountWholesaleTo'];
                $rates[$code] = (float)$rate;
            }
        }
        
        return $rates;
    }
}
?>