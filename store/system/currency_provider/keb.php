<?php
class cp_keb extends AddistClass
{
    public function getCurrency()
    {
        $config = $this->config->filter('keb');
        return isset($config['currency']) ? $config['currency'] : $this->config->get('config_currency');
    }
    
    public function getRates()
    {
        $rates = array();
        
        $content = fileGet('http://fx.keb.co.kr/FER1101C.web?schID=pib&mID=PER1101C&_frame=no&LOCALE_CODE=EN#');
        
        libxml_use_internal_errors(true);
        $doc = new DOMDocument();
        $doc->loadHTML($content);
        
        $xpath = new DOMXPath($doc);
        $rows = $xpath->query('//div[@id="gridPosition"]/table/tr');
        if ($rows)
        {
            foreach($rows as $row)
            {
                $node = $xpath->query('td',$row)->item(0);
                if ($node)
                {
                    $parts = explode(' ',$node->nodeValue);
                    $quote = isset($parts[1]) ? $parts[1] : 1;
                    $rate = $quote / (float)$xpath->query('td',$row)->item(11)->nodeValue;
                    $rates[$parts[0]] = (float)$rate;
                }
            }
        }
        
        return $rates;
    }
}
?>