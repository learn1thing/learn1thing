<?php
class cp_apilayer extends AddistClass
{
    public function getCurrency()
    {
        $config = $this->config->filter('apilayer');
        return isset($config['currency']) ? $config['currency'] : $this->config->get('config_currency');
    }
    
    public function getRates()
    {
        $rates = array();
        
		$json = json_decode(fileGet('http://www.apilayer.net/api/live?access_key='.$this->config->get('apilayer_access_key').'&format=1&source='.$this->getCurrency()),true);
        if ($json)
        {
            if (!empty($json['quotes']))
            {
                foreach($json['quotes'] as $code=>$rate)
                {
                    $rates[substr($code,3,3)] = 1/$rate;
                }
            }
            elseif (!empty($json['error']))
            {
                addMessage('error',$json['error']['info']);
            }
        }
        
        return $rates;
    }
}
?>