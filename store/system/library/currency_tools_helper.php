<?php
#encode-me[string]#
class CurrencyToolsHelper extends AddistClass
{
    public $currencies, $manufacturer_currencies;
    
    public function __construct($registry)
    {
        parent::__construct($registry);
        
        //currencies
        $this->currencies = array();
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "currency");
        foreach ($query->rows as $currency)
        {
            $this->currencies[$currency['code']] = $currency['value'];
        }
        unset($query);
        
        //manufacturer currencies
        $this->manufacturer_currencies = array();
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "manufacturer WHERE custom_rates = '1'");
        foreach ($query->rows as $manufacturer)
        {
            $this->manufacturer_currencies[$manufacturer['manufacturer_id']] = $this->currencies;
            
            $sub_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "currency_to_manufacturer WHERE manufacturer_id = '".(int)$manufacturer['manufacturer_id']."'");
            foreach ($sub_query->rows as $manufacturer_currency)
            {
                $this->manufacturer_currencies[$manufacturer_currency['manufacturer_id']][$manufacturer_currency['currency']] = $manufacturer_currency['value'];
            }
        }
        unset($query);
    }
    
    public function cron()
    {
        //check current cron proccess
        if ($this->config->get('currency_tools_cron_running'))
        {
            die("Another cron task is already running...\n");
        }
        
        //set cron running
        $this->config->save('currency_tools','cron_running',true);
        
        //ex rate refresh
        $this->refreshRates();
        
        //price refresh
        $this->refreshPrice();
        
        //set cron stopped
        $this->config->save('currency_tools','cron_running',false);
        
        die("Cron task successfully executed...\n");
    }
    
    public function refreshRates()
    {
        if ($this->config->get('currency_tools_autorefresh'))
        {
            $hours = $this->config->get('currency_tools_hours');
            $last_refresh = $this->config->get('currency_tools_last_refresh');
            foreach($hours as $hour)
            {
                if (time() > strtotime(date("Y-m-d $hour:00:00")) && $last_refresh < strtotime(date("Y-m-d $hour:i:s")) && $hour < date('H'))
                {
                    $this->refresh(true);
                    break;
                }
            }
        }
    }
    
    public function refresh($cron = false)
    {
        //language
        if (!$cron && IS_ADMIN)
        {
            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "language` WHERE language_id = '".$this->config->get('config_language_id')."'");
            $language = new Language($query->row['directory']);
            $language->load('module/currency_tools');
        }
        
        //refresh
        $provider = $this->config->get('currency_tools_provider');
        if ($provider)
        {
            if (!file_exists(DIR_SYSTEM.'currency_provider/'.$provider.'.php'))
            {
                if (!$cron && IS_ADMIN)
                {
                    addMessage('error',$language->get('error_provider'));
                }
                return false;
            }
            
            $class_name = 'cp_'.$provider;
            
            require_once(DIR_SYSTEM.'currency_provider/'.$provider.'.php');
            
            if (!class_exists($class_name))
            {
                if (!$cron && IS_ADMIN)
                {
                    addMessage('error',$language->get('error_provider'));
                }
                return false;
            }
            
            $currency_admin = $this->config->get('config_currency');
            
            $class = new $class_name($this->registry);
            $currency_provider = $class->getCurrency();
            $rates = $class->getRates();
            
            if ($rates)
            {
                $rate = (isset($rates[$currency_admin]) ? $rates[$currency_admin] : 1);
                foreach($rates as $code=>$value)
                {
                    $rates[$code] = $rate/$value;
                }
                $rates[$currency_provider] = $rate;
                
                foreach($rates as $code=>$value)
                {
                    $currency_info = $this->getCurrency($code);
                    if (!empty($currency_info['autorefresh']))
                    {
                        $this->db->query("UPDATE `".DB_PREFIX."currency` SET `value` = '".$value."', `date_modified` = '".date("Y-m-d H:i:s")."' WHERE `code` = '".$code."'");
                    }
                }
                
                if (!$cron && IS_ADMIN)
                {
                    addMessage('success',$language->get('text_refreshed'));
                }
            }
            else
            {
                if (!$cron && IS_ADMIN)
                {
                    addMessage('error',$language->get('error_refresh'));
                }
                return false;
            }
            
            if ($cron)
            {
                echo "Currency rates successfully refreshed...\n";
            }
            
            //set refresh time
            $this->config->save('currency_tools','last_refresh',time());
            
            //set refresh
            $this->setRefresh();
            
            return true;
        }
    }
    
    public function autoRefreshPrice()
    {
        $query = $this->db->query("SELECT `product_id` FROM " . DB_PREFIX . "product WHERE `refresh` = '1'");
        if ($query->num_rows < 20000)
        {
            $this->refreshPrice();
        }
    }
    
    public function refreshPrice($product_id = null)
    {
        //currency data
        $currencies = array();
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "currency WHERE status = '1'");
        foreach ($query->rows as $currency)
        {
            $currencies[$currency['code']] = $currency;
        }
        unset($query);
        
        //product_discount.price update
        $query = $this->db->query("SELECT pd.product_discount_id, p.product_id, p.manufacturer_id, pd.price, p.margin, p.overheads, IF(p.`currency` != '', p.`currency`, IF(m.`currency` != '', m.`currency`, '".$this->config->get('currency_tools_currency_product')."')) AS currency, IF(p.`overheads_currency` != '', p.`overheads_currency`, IF(p.`currency` != '', p.`currency`, IF(m.`currency` != '', m.`currency`, '".$this->config->get('currency_tools_currency_product')."'))) AS overheads_currency, (SELECT m.custom_rates FROM " . DB_PREFIX . "manufacturer m WHERE m.manufacturer_id = p.manufacturer_id) AS custom_rates, (SELECT m.margin FROM " . DB_PREFIX . "manufacturer m WHERE m.manufacturer_id = p.manufacturer_id) AS m_margin, (SELECT m.currency FROM " . DB_PREFIX . "manufacturer m WHERE m.manufacturer_id = p.manufacturer_id) AS m_currency, (SELECT m.overheads FROM " . DB_PREFIX . "manufacturer m WHERE m.manufacturer_id = p.manufacturer_id) AS m_overheads, (SELECT m.overheads_currency FROM " . DB_PREFIX . "manufacturer m WHERE m.manufacturer_id = p.manufacturer_id) AS m_overheads_currency FROM " . DB_PREFIX . "product_discount pd LEFT JOIN " . DB_PREFIX . "product p ON p.product_id = pd.product_id LEFT JOIN " . DB_PREFIX . "manufacturer m ON m.manufacturer_id = p.manufacturer_id WHERE 1".($product_id ? " AND p.product_id = '$product_id'" : " AND p.refresh = '1' AND p.status = '1'"));
        foreach ($query->rows as $product)
        {
            $rates = $product['custom_rates'] ? $this->manufacturer_currencies[$product['manufacturer_id']] : $this->currencies;
            foreach($currencies as $currency=>$currency_data)
            {
                $price = $this->convert($product['price']*(($product['margin']+100)/100),$product['currency'],$this->config->get('config_currency'),$rates,$currency)+$this->convert($product['overheads'],$product['overheads_currency'],$this->config->get('config_currency'),$rates,$currency);
                //if ($price > 0)
                {
                    $product_price = $price + $price*($currency_data['margin']/100);
                    $product_price += $this->convert($currency_data['overheads'],$currency,$this->config->get('config_currency'),$rates);
                    
                    $product_price = $product_price + $product_price*($product['m_margin']/100);
                    $product_price += $this->convert($product['m_overheads'],$product['m_overheads_currency'] ? $product['m_overheads_currency'] : ($product['currency'] ? $product['currency'] : $this->config->get('config_currency')),$this->config->get('config_currency'),$rates);
                    
                    $this->db->query("UPDATE " . DB_PREFIX . "product_discount SET price_".strtolower($currency)." = '$product_price' WHERE product_discount_id = '".$product['product_discount_id']."'");
                }
            }
            unset($rates);
        }
        unset($query);
        
        //product_special.price update
        $query = $this->db->query("SELECT pd.product_special_id, p.product_id, p.manufacturer_id, pd.price, p.margin, p.overheads, IF(p.`currency` != '', p.`currency`, IF(m.`currency` != '', m.`currency`, '".$this->config->get('currency_tools_currency_product')."')) AS currency, IF(p.`overheads_currency` != '', p.`overheads_currency`, IF(p.`currency` != '', p.`currency`, IF(m.`currency` != '', m.`currency`, '".$this->config->get('currency_tools_currency_product')."'))) AS overheads_currency, (SELECT m.custom_rates FROM " . DB_PREFIX . "manufacturer m WHERE m.manufacturer_id = p.manufacturer_id) AS custom_rates, (SELECT m.margin FROM " . DB_PREFIX . "manufacturer m WHERE m.manufacturer_id = p.manufacturer_id) AS m_margin, (SELECT m.currency FROM " . DB_PREFIX . "manufacturer m WHERE m.manufacturer_id = p.manufacturer_id) AS m_currency, (SELECT m.overheads FROM " . DB_PREFIX . "manufacturer m WHERE m.manufacturer_id = p.manufacturer_id) AS m_overheads, (SELECT m.overheads_currency FROM " . DB_PREFIX . "manufacturer m WHERE m.manufacturer_id = p.manufacturer_id) AS m_overheads_currency FROM " . DB_PREFIX . "product_special pd LEFT JOIN " . DB_PREFIX . "product p ON p.product_id = pd.product_id LEFT JOIN " . DB_PREFIX . "manufacturer m ON m.manufacturer_id = p.manufacturer_id WHERE 1".($product_id ? " AND p.product_id = '$product_id'" : " AND p.refresh = '1' AND p.status = '1'"));
        foreach ($query->rows as $product)
        {
            $rates = $product['custom_rates'] ? $this->manufacturer_currencies[$product['manufacturer_id']] : $this->currencies;
            foreach($currencies as $currency=>$currency_data)
            {
                $price = $this->convert($product['price']*(($product['margin']+100)/100),$product['currency'],$this->config->get('config_currency'),$rates,$currency)+$this->convert($product['overheads'],$product['overheads_currency'],$this->config->get('config_currency'),$rates,$currency);
                //if ($price > 0)
                {
                    $product_price = $price + $price*($currency_data['margin']/100);
                    $product_price += $this->convert($currency_data['overheads'],$currency,$this->config->get('config_currency'),$rates);
                    
                    $product_price = $product_price + $product_price*($product['m_margin']/100);
                    $product_price += $this->convert($product['m_overheads'],$product['m_overheads_currency'] ? $product['m_overheads_currency'] : ($product['currency'] ? $product['currency'] : $this->config->get('config_currency')),$this->config->get('config_currency'),$rates);
                    
                    $this->db->query("UPDATE " . DB_PREFIX . "product_special SET price_".strtolower($currency)." = '$product_price' WHERE product_special_id = '".$product['product_special_id']."'");
                }
            }
            unset($rates);
        }
        unset($query);
        
        //product_option_value.price update
        $query = $this->db->query("SELECT pd.product_option_value_id, p.product_id, p.manufacturer_id, pd.price, p.margin, p.overheads, IF(p.`currency` != '', p.`currency`, IF(m.`currency` != '', m.`currency`, '".$this->config->get('currency_tools_currency_product')."')) AS currency, IF(p.`overheads_currency` != '', p.`overheads_currency`, IF(p.`currency` != '', p.`currency`, IF(m.`currency` != '', m.`currency`, '".$this->config->get('currency_tools_currency_product')."'))) AS overheads_currency, (SELECT m.custom_rates FROM " . DB_PREFIX . "manufacturer m WHERE m.manufacturer_id = p.manufacturer_id) AS custom_rates, (SELECT m.margin FROM " . DB_PREFIX . "manufacturer m WHERE m.manufacturer_id = p.manufacturer_id) AS m_margin, (SELECT m.currency FROM " . DB_PREFIX . "manufacturer m WHERE m.manufacturer_id = p.manufacturer_id) AS m_currency, (SELECT m.overheads FROM " . DB_PREFIX . "manufacturer m WHERE m.manufacturer_id = p.manufacturer_id) AS m_overheads, (SELECT m.overheads_currency FROM " . DB_PREFIX . "manufacturer m WHERE m.manufacturer_id = p.manufacturer_id) AS m_overheads_currency FROM " . DB_PREFIX . "product_option_value pd LEFT JOIN " . DB_PREFIX . "product p ON p.product_id = pd.product_id LEFT JOIN " . DB_PREFIX . "manufacturer m ON m.manufacturer_id = p.manufacturer_id WHERE 1".($product_id ? " AND p.product_id = '$product_id'" : " AND p.refresh = '1' AND p.status = '1'"));
        foreach ($query->rows as $product)
        {
            $rates = $product['custom_rates'] ? $this->manufacturer_currencies[$product['manufacturer_id']] : $this->currencies;
            foreach($currencies as $currency=>$currency_data)
            {
                $price = $this->convert($product['price']*(($product['margin']+100)/100),$product['currency'],$this->config->get('config_currency'),$rates,$currency);
                //if ($price > 0)
                {
                    $product_price = $price + $price*($currency_data['margin']/100);
                    if ($product_price > 0)
                    {
                        $this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET price_".strtolower($currency)." = '$product_price' WHERE product_option_value_id = '".$product['product_option_value_id']."'");
                    }
                }
            }
            unset($rates);
        }
        unset($query);
        
        //product.price update
        $query = $this->db->query("SELECT p.product_id, p.manufacturer_id, p.price, p.margin, p.overheads, IF(p.`currency` != '', p.`currency`, IF(m.`currency` != '', m.`currency`, '".$this->config->get('currency_tools_currency_product')."')) AS currency, IF(p.`overheads_currency` != '', p.`overheads_currency`, IF(p.`currency` != '', p.`currency`, IF(m.`currency` != '', m.`currency`, '".$this->config->get('currency_tools_currency_product')."'))) AS overheads_currency, (SELECT m.custom_rates FROM " . DB_PREFIX . "manufacturer m WHERE m.manufacturer_id = p.manufacturer_id) AS custom_rates, (SELECT m.margin FROM " . DB_PREFIX . "manufacturer m WHERE m.manufacturer_id = p.manufacturer_id) AS m_margin, (SELECT m.currency FROM " . DB_PREFIX . "manufacturer m WHERE m.manufacturer_id = p.manufacturer_id) AS m_currency, (SELECT m.overheads FROM " . DB_PREFIX . "manufacturer m WHERE m.manufacturer_id = p.manufacturer_id) AS m_overheads, (SELECT m.overheads_currency FROM " . DB_PREFIX . "manufacturer m WHERE m.manufacturer_id = p.manufacturer_id) AS m_overheads_currency FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "manufacturer m ON m.manufacturer_id = p.manufacturer_id WHERE 1".($product_id ? " AND p.product_id = '$product_id'" : " AND p.refresh = '1' AND p.status = '1'"));
        foreach ($query->rows as $product)
        {
            $rates = $product['custom_rates'] ? $this->manufacturer_currencies[$product['manufacturer_id']] : $this->currencies;
            foreach($currencies as $currency=>$currency_data)
            {
                $price = $this->convert($product['price']*(($product['margin']+100)/100),$product['currency'],$this->config->get('config_currency'),$rates,$currency)+$this->convert($product['overheads'],$product['overheads_currency'],$this->config->get('config_currency'),$rates,$currency);
                //if ($price > 0)
                {
                    $product_price = $price + $price*($currency_data['margin']/100);
                    $product_price += $this->convert($currency_data['overheads'],$currency,$this->config->get('config_currency'),$rates);
                    
                    $product_price = $product_price + $product_price*($product['m_margin']/100);
                    $product_price += $this->convert($product['m_overheads'],$product['m_overheads_currency'] ? $product['m_overheads_currency'] : ($product['currency'] ? $product['currency'] : $this->config->get('config_currency')),$this->config->get('config_currency'),$rates);
                    
                    $this->db->query("UPDATE " . DB_PREFIX . "product SET price_".strtolower($currency)." = '$product_price', refresh = '0' WHERE product_id = '".$product['product_id']."'");
                }
            }
            unset($rates);
        }
        unset($query);
        
        $this->config->save('currency_tools','executed',time());
    }
    
    public function convert($amount, $from_currency, $to_currency, $rates, $currency = '')
    {
        $from_currency = strtoupper($from_currency);
        $to_currency = strtoupper($to_currency);
        $currency = strtoupper($currency);
        
    	if (isset($rates[$from_currency]))
        {
    		$from_value = $rates[$from_currency];
    	}
    	elseif (isset($this->currencies[$from_currency]))
        {
    		$from_value = $this->currencies[$from_currency];
    	}
        else
        {
    		$from_value = 1;
    	}
    
    	if (isset($rates[$to_currency]))
        {
    		$to_value = $rates[$to_currency];
    	}
    	elseif (isset($this->currencies[$to_currency]))
        {
    		$from_value = $this->currencies[$to_currency];
    	}
        else
        {
    		$to_value = 1;
    	}
    
    	return $amount * ($to_value / $from_value) * (($currency && isset($this->currencies[$currency])) ? $rates[$currency]/$this->currencies[$currency] : 1);
    }
    
    public function addPriceFields()
    {
        $query = $this->db->query("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . DB_DATABASE . "' AND TABLE_NAME REGEXP '^" . DB_PREFIX . "(.*)$' AND COLUMN_NAME = 'price'");
        if ($query->num_rows)
        {
            foreach($query->rows as $row)
            {
                foreach($this->currencies as $currency=>$currency_value)
                {
                    $subquery = $this->db->query("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . DB_DATABASE . "' AND TABLE_NAME = '" . $row['TABLE_NAME'] . "' AND COLUMN_NAME = 'price_".strtolower($currency)."'");
                    if (!$subquery->num_rows && in_array($row['TABLE_NAME'],array(DB_PREFIX.'product',DB_PREFIX.'product_discount',DB_PREFIX.'product_special',DB_PREFIX.'product_option_value')))
                    {
                        //insert field
                        $this->db->query("ALTER TABLE `" . $row['TABLE_NAME'] . "` ADD `price_".strtolower($currency)."` DECIMAL(20,12) NOT NULL AFTER `price`");
                        
                        //set refresh
                        $this->setRefresh();
                    }
                }
            }
        }
    }
    
    public function setRefresh($manufacturer_id = 0)
    {
        $this->db->query("UPDATE ".DB_PREFIX."product SET refresh = 1".($manufacturer_id ? " WHERE manufacturer_id = '".(int)$manufacturer_id."'" : ""));
    }
    
    public function getCurrency($currency)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "currency WHERE `code` = '$currency'");
        if ($query->num_rows)
        {
            return $query->row;
        }
        else
        {
            return array();
        }
    }
}
?>