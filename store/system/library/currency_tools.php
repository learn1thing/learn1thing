<?php
#encode-me[ioncube],encode-me[function]#
if (!class_exists('AddistObject')) require_once(DIR_SYSTEM.'addist/startup.php');
class CurrencyTools extends AddistModel
{
    protected $path = 'module/currency_tools/currency_tools_pro';
    
	private $helper;
    private $currency, $currencies = array();
    
	public function __construct($registry)
    {
        parent::__construct($registry);
        
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "currency");

		foreach ($query->rows as $result)
        {
			$this->currencies[strtoupper($result['code'])] = array(
				'currency_id'       => $result['currency_id'],
				'title'             => $result['title'],
				'symbol_left'       => $result['symbol_left'],
				'symbol_right'      => $result['symbol_right'],
				'decimal_place'     => $result['decimal_place'],
                'round_value'       => $result['round_value'],
                'value'             => $result['value'],
                'correction'        => $result['correction'],
                'margin'            => $result['margin'],
                'overheads'         => $result['overheads'],
                'deviation'         => $result['deviation'],
                'autorefresh'       => $result['autorefresh'],
			);
		}
        
        //detect
        $this->detect();
        
        if (!IS_ADMIN || (!empty($this->request->get['route']) && $this->request->get['route'] == 'module/currency_tools'))
        {
            //auto refresh prices
            require_once(DIR_SYSTEM.'library/currency_tools_helper.php');
            $this->helper = new CurrencyToolsHelper($this->registry);
            $this->helper->autoRefreshPrice();
        }
	}
    
    public function refresh()
    {
        //load helper
        require_once(DIR_SYSTEM.'library/currency_tools_helper.php');
        $helper = new CurrencyToolsHelper($this->registry);
        $helper->refresh();
    }
    
	public function detect()
    {
        if (IS_ADMIN)
        {
            $currency = $this->config->get('config_currency');
        }
        else
        {
            if ($this->config->get('currency_tools_restriction') && $this->config->get('currency_tools_currency_store') != 'AUTO' && $this->has($this->config->get('currency_tools_currency_store')))
            {
                $currency = $this->config->get('currency_tools_currency_store');
            }
    		elseif (!isset($this->session->data['currency']) || !isset($this->session->data['currency_changed']) || ((int)$this->session->data['currency_changed'] + (int)$this->config->get('currency_tools_session_lifetime')) < time())
            {
                if (isset($this->request->get['currency']) && $this->has($this->request->get['currency']))
                {
                    $currency = $this->request->get['currency'];
                }
                
                if (empty($currency) && $this->config->get('currency_tools_currency_store') == 'AUTO' && !isset($this->request->get['bot']))
                {
                    $currency = $this->detectByIp();
                }
                
                if (empty($currency) && $this->config->get('currency_tools_currency_store'))
                {
                    $currency = $this->config->get('currency_tools_currency_store');
                }
                
                if (empty($currency) || !$this->has($currency))
                {
                    $currency = $this->config->get('config_currency');
                }
                
                $this->session->data['currency'] = $currency;
                $this->session->data['currency_changed'] = time();
            }
            elseif ($this->has($this->session->data['currency']))
            {
                $currency = $this->session->data['currency'];
            }
        }
        
        if (empty($currency) || !$this->has($currency))
        {
            $currency = $this->config->get('config_currency');
        }
        
        $this->currency = $currency;
	}
    
    
    public function detectByIp()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_REAL_IP']))
        {
            $ip = $_SERVER['HTTP_X_REAL_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        
        $country_code = $this->getCountryCode($ip);
        if ($country_code)
        {
            $countries = $this->config->get('currency_tools_countries');
            if ($countries && !empty($countries[$country_code]))
            {
                return $countries[$country_code];
            }
        }
    }
    
    public function getCountryCode($ip)
    {
        $json = json_decode($this->extension->fileGet('http://geoip.nekudo.com/api/'.$ip),true);
        $country_code = !empty($json['country']['code']) ? $json['country']['code'] : '';
        
        if (empty($country_code) || $this->config->get('currency_tools_country_detector','geoiplookup') == 'geoiplookup')
        {
            $xml = simplexml_load_string($this->extension->fileGet('http://api.geoiplookup.net/?query='.$ip));
            $country_code = !empty($xml) ? $xml->result->countrycode : '';
        }
        
        if (empty($country_code) || $this->config->get('currency_tools_country_detector','ip_api') == 'ip_api')
        {
            $json = json_decode($this->extension->fileGet('http://ip-api.com/json/'.$ip),true);
            $country_code = !empty($json['countryCode']) ? $json['countryCode'] : '';
        }
        
        return $country_code;
    }
    
	public function set($currency)
    {
        if (!$this->config->get('currency_tools_restriction'))
        {
            $this->session->data['currency'] = $currency;
        }
	}
    
    public function getInputCurrencyByCaller($caller)
    {
        $file = $this->extension->getRealFile($caller[0]['file']);
        
        if (basename($file) == 'object.php')
        {
            $file = $this->extension->getRealFile($caller[1]['file']);
        }
        
        if ($file)
        {
            $shipping_code = '';
            
            if (substr_count($file,'model/shipping/'))
            {
                $shipping_code = basename($file,'.php');
            }
            elseif ($file == 'catalog/model/total/shipping.php' && !empty($this->session->data['shipping_method']))
            {
                $parts = explode('.',$this->session->data['shipping_method']['code']);
                $shipping_code = $parts[0];
            }
            
            if ($shipping_code)
            {
                $shipping_methods = $this->config->get('currency_tools_shipping_methods');
                if (!empty($shipping_methods[$shipping_code]))
                {
                    return $shipping_methods[$shipping_code];
                }
            }
        }
    }
    
    public function getOutputCurrencyByCaller($caller)
    {
        if (empty($caller[0]['file']))
        {
            return '';
        }
        
        $files = array(
            'payment'   =>  array(
                'system/library/simple/simplecheckout.php',
                'catalog/controller/common/cart.php',
                'catalog/controller/module/cart.php',
                'catalog/controller/checkout',
                'catalog/model/payment',
                'catalog/model/shipping',
                'catalog/model/total',
                'catalog/controller/quickcheckout',
                'catalog/controller/checkout/simplecheckout',
            ),
        );
        
        $file = $this->extension->getRealFile($caller[0]['file']);
        
        if (basename($file) == 'object.php')
        {
            $file = $this->extension->getRealFile($caller[1]['file']);
        }
        
        if ($file)
        {
            if ($file == 'catalog/controller/checkout/confirm.php')
            {
                return $this->config->get('config_currency');
            }
            if (in_array($file,$files['payment']) || in_array(dirname($file),$files['payment']))
            {
                return $this->getCode('payment');
            }
            elseif (substr_count($file,'controller/payment/'))
            {
                $payment_code = basename($file,'.php');
                $payment_methods = $this->config->get('currency_tools_payment_methods');
                if (!empty($payment_methods[$payment_code]))
                {
                    return $payment_methods[$payment_code];
                }
            }
        }
    }

	public function format($amount, $currency_output = '', $rate = '', $format = true, $use_template = true)
    {
        //check input currency
        if (!IS_ADMIN && $caller_currency = $this->getInputCurrencyByCaller(debug_backtrace()))
        {
            $currency_input = $caller_currency;
        }
        
        if (empty($currency_input) || !$this->has($currency_input))
        {
            $currency_input = $this->config->get('config_currency');
        }
        
        //check output currency
        if (!IS_ADMIN && !$rate && $caller_currency = $this->getOutputCurrencyByCaller(debug_backtrace()))
        {
            $currency_output = $caller_currency;
            $use_template = false;
        }
        
        if (empty($currency_output) || !$this->has($currency_output))
        {
            $currency_output = $this->currency;
        }
        
        //string params
		$symbol_left   = $this->currencies[$currency_output]['symbol_left'];
		$symbol_right  = $this->currencies[$currency_output]['symbol_right'];
		$decimal_place = $this->currencies[$currency_output]['decimal_place'];
        $round_value   = (int)$this->currencies[$currency_output]['round_value'];
        
        //language params
        $decimal_point = $format ? $this->language->get('decimal_point') : '.';
		$thousand_point = $format ? $this->language->get('thousand_point') : '';
        
        //convert
        if ($rate)
        {
            $value = (float)$amount*(float)$rate;
        }
        else
        {
            $value = (float)$this->convert($amount, $currency_input, $currency_output);
        }
        
        //format
        if (!IS_ADMIN)
        {
            if ($value > 0)
            {
                if (strlen((int)$value) > $round_value)
                {
                    $value = (float)round($value, $round_value);
                }
                
                $value += $this->currencies[$currency_output]['deviation'];
            }
        }
        
        //format to string
        if ($this->config->get('currency_tools_use_core'))
        {
            $string = $this->currency_core->format($value,$currency_output,1,true);
        }
        else
        {
    		$string = '';
    
    		if ($symbol_left && $format)
            {
    			$string .= $symbol_left;
    		}
    
    		$string .= number_format($value, (int)$decimal_place, $decimal_point, $thousand_point);
    
    		if ($symbol_right && $format)
            {
    			$string .= $symbol_right;
    		}
        }
        
        if (!IS_ADMIN && $format && $use_template)
        {
            $data = array();
            $data['{main}'] = $string;
            $data['{secondary}'] = $this->config->get('currency_tools_currency_secondary') ? ($this->config->get('currency_tools_use_core') ? $this->currency_core->format($amount, $this->config->get('currency_tools_currency_secondary'), $this->currencies[$this->config->get('currency_tools_currency_secondary')]['value'], true) : $this->format($amount, $this->config->get('currency_tools_currency_secondary'), $this->currencies[$this->config->get('currency_tools_currency_secondary')]['value'], true, false)) : '';
            $data['{payment}'] = $this->config->get('currency_tools_use_core') ? $this->currency_core->format($amount, $this->getCode('payment'), $this->currencies[$this->getCode('payment')]['value'], true) : $this->format($amount, $this->getCode('payment'), $this->currencies[$this->getCode('payment')]['value'], true, false);
            $data['#n'] = '<br/>';
            
            foreach($this->currencies as $code=>$item)
            {
                $data["[$code]"] = $this->config->get('currency_tools_use_core') ? $this->currency_core->format($amount, $code, $this->currencies[$code]['value'], true) : $this->format($amount, $code, $this->currencies[$code]['value'], true, false);
            }
            
            $string = str_replace(array_keys($data), array_values($data), html_entity_decode($this->config->get('currency_tools_template')));
            $string = trim(strip_tags($string, '<br><span>'));
        }

		return $string;
	}

	public function convert($amount, $from_currency, $to_currency)
    {
		if ($this->has($from_currency))
        {
			$from_value = (float)$this->currencies[$from_currency]['value'];
		}
        else
        {
			$from_value = 1;
		}

		if ($this->has($to_currency))
        {
			$to_value = (float)$this->currencies[$to_currency]['value'];
		}
        else
        {
			$to_value = 1;
		}

		return (float)$amount * ($to_value / $from_value);
	}

	public function getId($currency = '')
    {
		if (!$currency)
        {
			return $this->currencies[$this->currency]['currency_id'];
		}
        elseif ($currency && $this->has($currency))
        {
			return $this->currencies[$currency]['currency_id'];
		}
        else
        {
			return 0;
		}
	}

	public function getSymbolLeft($currency = '')
    {
		if (!$currency)
        {
			return $this->currencies[$this->currency]['symbol_left'];
		}
        elseif ($currency && $this->has($currency))
        {
			return $this->currencies[$currency]['symbol_left'];
		}
        else
        {
			return '';
		}
	}

	public function getSymbolRight($currency = '')
    {
		if (!$currency)
        {
			return $this->currencies[$this->currency]['symbol_right'];
		}
        elseif ($currency && $this->has($currency))
        {
			return $this->currencies[$currency]['symbol_right'];
		}
        else
        {
			return '';
		}
	}

	public function getDecimalPlace($currency = '')
    {
		if (!$currency)
        {
			return $this->currencies[$this->currency]['decimal_place'];
		}
        elseif ($currency && $this->has($currency))
        {
			return $this->currencies[$currency]['decimal_place'];
		}
        else
        {
			return 0;
		}
	}

	public function getCode($type = '')
    {
		if (IS_ADMIN)
        {
            $currency = $this->currency;
        }
        else
        {
            if ($type && $type != 'store')
            {
                if ($type == 'payment')
                {
                    $payment_methods = $this->config->get('currency_tools_payment_methods');
                    if (!empty($payment_methods) && is_array($payment_methods) && !empty($this->session->data['payment_method']))
                    {
                        if (is_array($this->session->data['payment_method']) && isset($payment_methods[$this->session->data['payment_method']['code']]))
                        {
                            $currency =  $payment_methods[$this->session->data['payment_method']['code']];
                        }
                        elseif (!is_array($this->session->data['payment_method']) && isset($payment_methods[$this->session->data['payment_method']]))
                        {
                            $currency = $payment_methods[$this->session->data['payment_method']];
                        }
                    }
                    
                    if (empty($currency) || !$this->has($currency))
                    {
                        if ($this->config->get('currency_tools_currency_payment'))
                        {
                            $currency = $this->config->get('currency_tools_currency_payment');
                        }
                        else
                        {
                            $currency = $this->getCode('store');
                        }
                    }
                }
            }
            elseif ($caller_currency = $this->getOutputCurrencyByCaller(debug_backtrace()))
            {
                $currency = $caller_currency;
            }
            else
            {
                $currency = $this->currency;
            }
        }
        
        if (empty($currency) || !$this->has($currency))
        {
            $currency = $this->config->get('config_currency');
        }
        
        return $currency;
	}
    
	public function getValue($currency = '')
    {
		if (!$currency)
        {
			return $this->currencies[$this->currency]['value'];
		}
        elseif ($currency && $this->has($currency))
        {
			return $this->currencies[$currency]['value'];
		}
        else
        {
			return 0;
		}
	}

	public function has($currency)
    {
		return isset($this->currencies[$currency]);
	}
    
    public function getCurrency($currency)
    {
        if ($this->has($currency))
        {
            return $this->currencies[$currency];
        }
    }
    
    public function getManufacturerRates($manufacturer_id)
    {
        $rates = array();
        
        foreach($this->currencies as $code=>$currency)
        {
            $rates[$code] = $currency['value'];
        }
        
        $query = $this->db->query("SELECT mc.* FROM " . DB_PREFIX . "currency_to_manufacturer mc LEFT JOIN " . DB_PREFIX . "manufacturer m ON m.manufacturer_id = mc.manufacturer_id WHERE m.manufacturer_id = '".(int)$manufacturer_id."' AND m.custom_rates = '1'");
        if ($query->num_rows)
        {
            foreach($query->rows as $row)
            {
                if ($row['value'])
                {
                    $rates[$row['currency']] = $row['currency'] == $this->getCode('admin') ? 1 : $row['value'];
                }
            }
        }
        
        return $rates;
    }
    
	public function getCurrencies()
    {
		return $this->currencies;
	}
    
    public function getProductCurrency($product_id)
    {
        $query = $this->db->query("SELECT IF(p.`currency` != '', p.`currency`, IF(m.`currency` != '', m.`currency`, '".$this->config->get('currency_tools_currency_product')."')) AS currency FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "manufacturer m ON m.manufacturer_id = p.manufacturer_id WHERE p.product_id = '$product_id' LIMIT 1");
        return $query->row['currency'];
    }
    
    public function getDbQuery($sql)
    {
        $test = trim(preg_replace('/\s\s+/', ' ', $sql));
        
        if (!IS_ADMIN && preg_match("#^SELECT#",$test))
        {
            //check sql pattern
            if (preg_match("# (FROM|JOIN) `?(".DB_PREFIX."product|".DB_PREFIX."product_discount|".DB_PREFIX."product_special|".DB_PREFIX."product_option_value)`? #",$test))
            {
                $sql = preg_replace("#\bprice\b#",'price_'.strtolower($this->currency),$sql);
            }
        }
        
        return $sql;
    }
    
    public function getDbResult($result)
    {
        if (is_object($result) && $result->num_rows && isset($result->row['price_'.strtolower($this->currency)]))
        {
            $result->row['price'] = (float)$result->row['price_'.strtolower($this->currency)] > 0 ? (float)$result->row['price_'.strtolower($this->currency)] : (isset($result->row['price']) ? (float)$result->row['price'] : 0);
                        
            foreach($result->rows as &$row)
            {
                $row['price'] = (float)$row['price_'.strtolower($this->currency)] > 0 ? (float)$row['price_'.strtolower($this->currency)] : (isset($row['price']) ? (float)$row['price'] : 0);
            }
        }
        return $result;
    }
}
?>