<?php

function sso_login() {

	// header("Cache-Control: no-cache, must-revalidate");
	// header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	// header("Content-Type: application/xml; charset=utf-8");
	
	$data = '';
	if (isset($_COOKIE['token']) && isset($_COOKIE['password'])) {
		$getProfile = getProfileMiddleware( $_COOKIE['token'] );
		$data  = json_decode($getProfile);

		if (!empty($data->status->code) && $data->status->code == 401) {
			$data = "logout";
		}
	}

	return $data;
}

function getProfileMiddleware( $token )
{
	$ch = curl_init();

	curl_setopt($ch,CURLOPT_URL, "https://store.learn1thing.com/dashboard/api/v1/profile");
		//curl_setopt($ch,CURLOPT_HTTPHEADER, $parameters);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Authorization:'. $token
		));
	    //execute post
	$result = curl_exec($ch);
	$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	    //close connection
	curl_close($ch);

	return $result;
}

