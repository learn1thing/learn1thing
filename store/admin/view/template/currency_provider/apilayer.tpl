<div class="form-group">
    <label class="col-sm-2 control-label" for="apilayer-currency"><?php echo $entry_currency; ?></label>
    <div class="col-sm-3">
        <select name="apilayer[currency]" id="apilayer-currency" class="form-control">
            <?php foreach ($currencies as $code=>$item) { ?>
            <option value="<?php echo $code; ?>"<?php if ($code == $currency) { ?> selected="selected"<?php } ?>><?php echo $code; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label" for="apilayer-access_key"><?php echo $entry_access_key; ?></label>
    <div class="col-sm-3">
        <input name="apilayer[access_key]" id="apilayer-access_key" value="<?php echo $access_key; ?>" class="form-control" />
    </div>
</div>