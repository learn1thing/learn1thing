<div class="form-group">
    <label class="col-sm-2 control-label" for="keb-currency"><?php echo $entry_currency; ?></label>
    <div class="col-sm-3">
        <select name="keb[currency]" id="keb-currency" class="form-control">
            <?php foreach ($currencies as $code=>$item) { ?>
            <option value="<?php echo $code; ?>"<?php if ($code == $currency) { ?> selected="selected"<?php } ?>><?php echo $code; ?></option>
            <?php } ?>
        </select>
    </div>
</div>