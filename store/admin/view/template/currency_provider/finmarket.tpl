<div class="form-group">
    <label class="col-sm-2 control-label" for="finmarket-country_id"><?php echo $entry_country; ?></label>
    <div class="col-sm-3">
        <select name="finmarket[country_id]" id="finmarket-country_id" class="form-control">
            <?php foreach ($countries as $item) { ?>
            <option value="<?php echo $item['id']; ?>"<?php if ($item['id'] == $country_id) { ?> selected="selected"<?php } ?>><?php echo $item['name']; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label" for="finmarket-time_offset"><?php echo $entry_time_offset; ?></label>
    <div class="col-sm-3">
        <select name="finmarket[time_offset]" id="finmarket-time_offset" class="form-control">
            <option value="0"<?php if ($time_offset == 0) { ?> selected="selected"<?php } ?>><?php echo $text_today; ?></option>
            <option value="86400"<?php if ($time_offset == 86400) { ?> selected="selected"<?php } ?>><?php echo $text_tomorrow; ?></option>
        </select>
    </div>
</div>