<div class="form-group">
    <label class="col-sm-2 control-label" for="tcmb-column"><?php echo $entry_column; ?></label>
    <div class="col-sm-3">
        <select name="tcmb[column]" id="tcmb-column" class="form-control">
            <option value="ForexBuying"<?php if ($column == 'ForexBuying') { ?> selected="selected"<?php } ?>><?php echo $text_buying; ?></option>
            <option value="ForexSelling"<?php if ($column == 'ForexSelling') { ?> selected="selected"<?php } ?>><?php echo $text_selling; ?></option>
        </select>
    </div>
</div>