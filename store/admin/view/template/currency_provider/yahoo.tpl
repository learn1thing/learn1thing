<div class="form-group">
    <label class="col-sm-2 control-label" for="yahoo-currency"><?php echo $entry_currency; ?></label>
    <div class="col-sm-3">
        <select name="yahoo[currency]" id="yahoo-currency" class="form-control">
            <?php foreach ($currencies as $item) { ?>
            <option value="<?php echo $item['code']; ?>"<?php if ($item['code'] == $currency) { ?> selected="selected"<?php } ?>><?php echo $item['title']; ?></option>
            <?php } ?>
        </select>
    </div>
</div>