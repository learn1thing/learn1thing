<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-subscriptions" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
	  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-subscriptions" class="form-horizontal">
        <div class="table-responsive">
			<form onsubmit="$('select[name=\'specific_day\']').prop('disabled', false);" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
				<table class="table table-bordered table-hover">
					<tr>
						<td colspan="2">
							<?php echo $text_trial_help; ?>
						</td>
					</tr>
					<tr>
						<td><?php echo $entry_trial; ?></td>
						<td>
							<?php if ($trial) { ?>
								<input type="checkbox" name="trial" value="1" checked="checked" />
							<?php } else { ?>
								<input type="checkbox" name="trial" value="1" />
							<?php } ?>
						</td>
					</tr>
					<tr>
						<td><?php echo $entry_trial_only; ?></td>
						<td>
							<?php if ($trial_only) { ?>
								<input type="checkbox" name="trial_only" value="1" checked="checked" />
							<?php } else { ?>
								<input type="checkbox" name="trial_only" value="1" />
							<?php } ?>
						</td>
					</tr>
					<tr>
						<td><?php echo $entry_trial_duration; ?></td>
						<td><input type="text" name="trial_duration" value="<?php echo $trial_duration; ?>" size="3" /></td>
					</tr>
					<tr>
						<td><?php echo $entry_trial_cycle; ?></td>
						<td><input type="text" name="trial_cycle" value="<?php echo $trial_cycle; ?>" size="3" /></td>
					</tr>
					<tr>
						<td><?php echo $entry_trial_frequency; ?></td>
						<td>
							<select name="trial_frequency">
								<?php foreach ($frequencies as $key => $value) { ?>
									<?php if ($trial_frequency == $key) { ?>
										<option value="<?php echo $key ?>" selected="selected"><?php echo $value; ?></option>
									<?php } else { ?>
										<option value="<?php echo $key ?>"><?php echo $value; ?></option>
									<?php } ?>
								<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
						<td><?php echo $entry_trial_price; ?></td>
						<td><input type="text" name="trial_price" value="<?php echo $trial_price; ?>" size="6" /></td>
					</tr>
					<tr>
						<td><?php echo $entry_trial_status; ?></td>
						<td>
							<select name="trial_status">
								<?php if ($trial_status) { ?>
									<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
									<option value="0"><?php echo $text_disabled; ?></option>
								<?php } else { ?>
									<option value="1"><?php echo $text_enabled; ?></option>
									<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<?php echo $text_recurring_help; ?>
						</td>
					</tr>
					<tr>
						<td><?php echo $entry_cycle; ?></td>
						<td><input type="text" name="cycle" value="<?php echo $cycle; ?>" size="3" /></td>
					</tr>
					<tr>
						<td><?php echo $entry_frequency; ?></td>
						<td>
							<select name="frequency">
								<?php foreach ($frequencies as $key => $value) { ?>
									<?php if ($frequency == $key) { ?>
										<option value="<?php echo $key ?>" selected="selected"><?php echo $value; ?></option>
									<?php } else { ?>
										<option value="<?php echo $key ?>"><?php echo $value; ?></option>
									<?php } ?>
								<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
						<td><?php echo $entry_duration; ?></td>
						<td><input type="text" name="duration" value="<?php echo $duration; ?>" size="3" /></td>
					</tr>
					<tr>
						<td data-toggle="tooltip" title="Leave blank to start subscription from today or choose a day your customer will be billed on every cycle for the duration of the subscription. Only available if you choose week for the frequency!"><?php echo $entry_specific_day; ?></td>
						<td>
							<select name="specific_day" style="margin-right: 10px;">
								<option value="" selected="selected"></option>
								<?php foreach ($days as $day) { ?>
									<?php if ($day['value'] == $specific_day) { ?>
										<option value="<?php echo $day['value']; ?>" selected="selected"><?php echo $day['name']; ?></option>
									<?php } else { ?>
										<option value="<?php echo $day['value']; ?>"><?php echo $day['name']; ?></option>
									<?php } ?>
								<?php } ?>
							</select><?php echo $text_sd_only_available; ?>
						</td>
					</tr>
					<tr>
						<td><?php echo $entry_discount; ?></td>
						<td>
							<input type="text" name="discount" value="<?php echo $discount; ?>" size="3" />&nbsp;&nbsp;
							<select name="discount_type">
								<?php if ($discount_type == "Percent") { ?>
									<option value="Percent" selected="selected"><?php echo $text_percent; ?></option>
									<option value="Amount"><?php echo $text_amount; ?></option>
								<?php } else { ?>
									<option value="Percent"><?php echo $text_percent; ?></option>
									<option value="Amount" selected="selected"><?php echo $text_amount; ?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
						<td><?php echo $entry_grace_period; ?></td>
						<td><input type="text" name="grace_period" value="<?php echo $grace_period; ?>" size="3" /></td>
					</tr>
					<tr>
						<td><?php echo $entry_reminder_count; ?></td>
						<td><input type="text" name="reminder_count" value="<?php echo $reminder_count; ?>" size="3" /></td>
					</tr>
					<tr>
						<td><?php echo $entry_reminder_period; ?></td>
						<td><input type="text" name="reminder_period" value="<?php echo $reminder_period; ?>" size="3" /></td>
					</tr>
					<tr>
						<td><?php echo $entry_reinstatement_fee; ?></td>
						<td><input type="text" name="reinstatement_fee" value="<?php echo $reinstatement_fee; ?>" size="3" /></td>
					</tr>
					<tr>
						<td><?php echo $entry_reinstatement_period; ?></td>
						<td><input type="text" name="reinstatement_period" value="<?php echo $reinstatement_period; ?>" size="3" /></td>
					</tr>
					<tr>
						<td><?php echo $entry_include_totals; ?></td>
						<td>
							<?php if ($include_taxes) { ?>
								<input type="checkbox" name="include_taxes" value="1" checked="checked" />
							<?php } else { ?>
								<input type="checkbox" name="include_taxes" value="1" />
							<?php } ?><?php echo $text_taxes; ?>
							<?php if ($include_shipping) { ?>
								<input style="margin-left: 10px;" type="checkbox" name="include_shipping" value="1" checked="checked" />
							<?php } else { ?>
								<input style="margin-left: 10px;" type="checkbox" name="include_shipping" value="1" />
							<?php } ?><?php echo $text_shipping; ?>
						</td>
					</tr>
					<tr>
						<td><?php echo $entry_customer_groups; ?></td>
						<td>
							<?php foreach ($customer_groups as $customer_group) { ?>
								<?php if (!empty($allowed_customer_groups)) { ?>
									<?php if (in_array($customer_group['customer_group_id'], $allowed_customer_groups)) { ?>
										<input style="margin-right: 5px; vertical-align: middle;" type="checkbox" name="allowed_customer_groups[]" value="<?php echo $customer_group['customer_group_id']; ?>" checked="checked" /><?php echo $customer_group['name']; ?>
									<?php } else { ?>
										<input style="margin-right: 5px; vertical-align: middle;" type="checkbox" name="allowed_customer_groups[]" value="<?php echo $customer_group['customer_group_id']; ?>" /><?php echo $customer_group['name']; ?>
									<?php } ?>
								<?php } else { ?>
									<input style="margin-right: 5px; vertical-align: middle;" type="checkbox" name="allowed_customer_groups[]" value="<?php echo $customer_group['customer_group_id']; ?>" checked="checked" /><?php echo $customer_group['name']; ?>
								<?php } ?>
								<br />
							<?php } ?>
						</td>
					</tr>
					<tr>
						<td><?php echo $entry_status; ?></td>
						<td>
							<select name="status">
								<?php if ($status) { ?>
									<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
									<option value="0"><?php echo $text_disabled; ?></option>
								<?php } else { ?>
									<option value="1"><?php echo $text_enabled; ?></option>
									<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
						<td><?php echo $entry_sort_order; ?></td>
						<td><input type="text" name="sort_order" value="<?php echo $sort_order; ?>" size="3" /></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		if ($('select[name=\'frequency\']').val() == 'week') {
			$('select[name=\'specific_day\']').prop('disabled', false);
		} else {
			$('select[name=\'specific_day\']').prop('disabled', true);
			$('select[name=\'specific_day\']').val('');
		}
		$('select[name=\'frequency\']').on('change', function() {
			if ($('select[name=\'frequency\']').val() == 'week') {
				$('select[name=\'specific_day\']').prop('disabled', false);
			} else {
				$('select[name=\'specific_day\']').prop('disabled', true);
				$('select[name=\'specific_day\']').val('');
			}
		});
	z});
</script>
<?php echo $footer; ?>