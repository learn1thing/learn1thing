<div class="modal fade" id="converter-modal" tabindex="-1" role="dialog" aria-labelledby="converter-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo $text_close; ?>"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="converter-modal-label"><?php echo $heading; ?></h4>
            </div>
            <div id="modal-body" class="modal-body">
                <?php if (!empty($error)){ ?>
                <div id="message">
                    <?php foreach($error as $item) { ?>
                    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i><?php echo $item; ?></div>
                    <?php } ?>
                </div>
                <?php } else { ?>
                <form action="<?php echo $action; ?>" method="post" id="form-converter" onsubmit="return checkForm();" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="input-convert-currency"><?php echo $entry_currency_new; ?></label>
                        <div class="col-sm-9">
                            <select name="convert[currency]" id="input-convert-currency" class="form-control"> 
                                <?php foreach ($currencies as $currency) { ?>
                                <option value="<?php echo $currency['code']; ?>"><?php echo $currency['title']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="input-convert-skip"><?php echo $entry_currency_skip; ?></label>
                        <div class="col-sm-9">
                            <select name="convert[skip][]" multiple="true" id="input-convert-skip" class="form-control">
                                <?php foreach ($currencies as $currency) { ?>
                                <option value="'<?php echo $currency['code']; ?>'"><?php echo $currency['title']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="input-convert-criteria"><?php echo $entry_criteria; ?></label>
                        <div class="col-sm-9">
                            <select name="convert[criteria]" id="input-convert-criteria" class="form-control">
                                <option value="1"><?php echo $text_all; ?></option>
                                <option value="2"><?php echo $text_by_brand; ?></option>
                                <option value="3"><?php echo $text_by_category; ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="convert-brand" style="display: none;">
                        <label class="control-label col-sm-3"><?php echo $entry_brand; ?></label>
                        <div class="col-sm-9">
                            <div class="well well-sm" style="height: 150px; overflow: auto;">
                                <table class="" style="width: 100%;height: 150px; overflow: scroll;">
                                    <tbody>
                                        <?php foreach ($brands as $i=>$brand) { ?>
                                        <tr>
                                            <td>
                                                <label for="convert_brand_<?php echo $brand['manufacturer_id']; ?>"><input type="checkbox" name="convert[brand][<?php echo $i; ?>][manufacturer_id]" id="convert_brand_<?php echo $brand['manufacturer_id']; ?>" value="<?php echo $brand['manufacturer_id']; ?>" /><?php echo $brand['name']; ?></label>
                                            </td>
                                            <td class="text-right" style="width: 150px;">
                                                <select name="convert[brand][<?php echo $i; ?>][currency]">
                                                    <option value=""><?php echo $text_default; ?></option>
                                                    <?php foreach ($currencies as $currency) { ?>
                                                    <option value="<?php echo $currency['code']; ?>"<?php if ($currency['code'] == $brand['currency']) { ?> selected=""<?php } ?>><?php echo $currency['title']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <a onclick="$(this).parent().find(':checkbox').prop('checked', true);" class="btn btn-default"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').prop('checked', false);" class="btn btn-default"><?php echo $text_unselect_all; ?></a>
                        </div>
                    </div>
                    <div class="form-group" id="convert-category" style="display: none;">
                        <label class="control-label col-sm-3"><?php echo $entry_category; ?></label>
                        <div class="col-sm-9">
                            <div class="well well-sm" style="height: 150px; overflow: auto;">
                                <table class="" style="width: 100%;height: 150px; overflow: scroll;">
                                    <tbody>
                                        <?php foreach ($categories as $i=>$category) { ?>
                                        <tr>
                                            <td>
                                                <label for="convert_category_<?php echo $category['category_id']; ?>"><input type="checkbox" name="convert[category][<?php echo $i; ?>][category_id]" id="convert_category_<?php echo $category['category_id']; ?>" value="<?php echo $category['category_id']; ?>" /><?php echo $category['name']; ?></label>
                                            </td>
                                            <td class="text-right" style="width: 150px;">
                                                <select name="convert[category][<?php echo $i; ?>][currency]">
                                                    <option value=""><?php echo $text_default; ?></option>
                                                    <?php foreach ($currencies as $currency) { ?>
                                                    <option value="<?php echo $currency['code']; ?>"><?php echo $currency['title']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <a onclick="$(this).parent().find(':checkbox').prop('checked', true);" class="btn btn-default"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').prop('checked', false);" class="btn btn-default"><?php echo $text_unselect_all; ?></a>
                        </div>
                    </div>
                </form>
                <?php } ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i>  <?php echo $text_close; ?></button>
                <?php if (empty($error)){ ?>
                <button type="submit" class="btn btn-primary" form="form-converter"><i class="fa fa-exchange"></i> <?php echo $button_convert; ?></button>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    function reposition()
    {
        var modal = $(this),
        dialog = modal.find('.modal-dialog');
        modal.css('display', 'block');
        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
    }
    
    $('.modal').on('show.bs.modal', reposition);
    
    $(window).on('resize', function() {
        $('.modal:visible').each(reposition);
    });
    
    $('#input-convert-criteria').change(function(){
        $('#convert-brand,#convert-category').hide();
        if ($('#input-convert-criteria').val() == '1')
        {
            $('#convert-brand,#convert-category').find(':checkbox').prop('checked', false);
        }
        else if ($('#input-convert-criteria').val() == '2')
        {
            $('#convert-brand').show();
            $('#convert-category').find(':checkbox').prop('checked', false);
        }
        else if ($('#input-convert-criteria').val() == '3')
        {
            $('#convert-category').show();
            $('#convert-brand').find(':checkbox').prop('checked', false);
        }
        
        $('.modal:visible').each(reposition);
    });
});

function checkForm()
{
    if (confirm('<?php echo $text_confirm; ?>'))
    {
        return true;
    }
    else
    {
        return false;
    }
}
</script>