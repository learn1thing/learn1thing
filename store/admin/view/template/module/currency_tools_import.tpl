<div class="modal fade" id="importer-modal" tabindex="-1" role="dialog" aria-labelledby="importer-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo $text_close; ?>"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="importer-modal-label"><?php echo $heading; ?></h4>
            </div>
            <div id="modal-body" class="modal-body">
                <?php if (!empty($error)){ ?>
                <div id="message">
                    <?php foreach($error as $item) { ?>
                    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i><?php echo $item; ?></div>
                    <?php } ?>
                </div>
                <?php } else { ?>
                <form action="<?php echo $action; ?>" method="post" id="form-importer" onsubmit="return checkForm();" class="form-horizontal" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="input-import-file"><?php echo $entry_file; ?></label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <input type="file" name="import[file]" id="input-import-file" onchange="fileSelected()" style="display: none;" />
                                <input type="text" readonly="true" id="selected-file" class="form-control" />
                                <span class="input-group-btn">
                                    <button type="button" onclick="$('#input-import-file').click()" class="btn btn-danger"><?php echo $text_select_file; ?></button>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
                <?php } ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i>  <?php echo $text_close; ?></button>
                <?php if (empty($error)){ ?>
                <button type="submit" class="btn btn-primary" form="form-importer"><i class="fa fa-download"></i> <?php echo $button_import; ?></button>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    function reposition()
    {
        var modal = $(this),
        dialog = modal.find('.modal-dialog');
        modal.css('display', 'block');
        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
    }
    
    $('.modal').on('show.bs.modal', reposition);
    
    $(window).on('resize', function() {
        $('.modal:visible').each(reposition);
    });
});

function getExtension(filename)
{
    var parts = filename.split('.');
    return parts[parts.length - 1];
}

function checkForm()
{
    if ($('#input-import-file').val() == '')
    {
        alert('<?php echo $error_file_empty; ?>');
        return false;
    }
    
    if (confirm('<?php echo $text_confirm; ?>'))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function fileSelected()
{
    if (getExtension($('#input-import-file').val()) != 'xls')
    {
        $('#input-import-file').val('');
        alert('<?php echo $error_file_type; ?>');
    }
    else
    {
        $('#selected-file').val($('#input-import-file').val());
    }
}
</script>