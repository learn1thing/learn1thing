<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
    <div class="container-fluid">
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
        </ul>
    </div>
    <div class="container-fluid">
        
        <div id="message"<?php if (!$success && !$error){ ?> style="display: none;"<?php } ?>>
            <?php foreach($success as $item) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i><?php echo $item; ?> <button type="button" class="close" data-dismiss="alert">×</button></div>
            <?php } ?>
            <?php foreach($error as $item) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i><?php echo $item; ?> <button type="button" class="close" data-dismiss="alert">×</button></div>
            <?php } ?>
        </div>
        
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
                <div class="pull-right btn-group">
                    <a onclick="$('#form').submit()" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i> <?php echo $button_save; ?></a>
                    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" id="form" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="input-name"><?php echo $entry_name; ?></label>
                        <div class="col-sm-4">
                            <input type="text" id="input-name" value="<?php echo $title; ?>(<?php echo $code; ?>)" readonly="" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="input-value"><?php echo $entry_value; ?></label>
                        <div class="col-sm-4"><input type="text" name="value" id="input-value" value="<?php echo number_format($value,8,'.',''); ?>" oninput="calculateRate(true)" class="form-control" /></div>
                        <div class="col-sm-6">
                            <span class="help"><?php echo sprintf($help_value,$currency_admin); ?></span>
                        </div>
                    </div>
                    <?php if ($code != $currency_admin){ ?>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="input-reversed_value"><?php echo $entry_reversed_value; ?></label>
                        <div class="col-sm-4"><input type="text" name="reversed_value" id="input-reversed_value" value="<?php echo number_format($reversed_value,8,'.',''); ?>" oninput="calculateRate(false)" class="form-control" /></div>
                        <div class="col-sm-6">
                            <span class="help"><?php echo $help_reversed_value; ?></span>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="input-correction"><?php echo $entry_correction; ?></label>
                        <div class="col-sm-4"><input type="text" name="correction" id="input-correction" value="<?php echo $correction; ?>" class="form-control" /></div>
                        <div class="col-sm-6">
                            <span class="help"><?php echo $help_correction; ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="input-margin"><?php echo $entry_margin; ?></label>
                        <div class="col-sm-4"><input type="text" name="margin" id="input-margin" value="<?php echo $margin; ?>" class="form-control" /></div>
                        <div class="col-sm-6">
                            <span class="help"><?php echo $help_margin; ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="input-overheads"><?php echo $entry_overheads; ?></label>
                        <div class="col-sm-4"><input type="text" name="overheads" id="input-overheads" value="<?php echo $overheads; ?>" class="form-control" /></div>
                        <div class="col-sm-6">
                            <span class="help"><?php echo $help_overheads; ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="input-deviation"><?php echo $entry_deviation; ?></label>
                        <div class="col-sm-4"><input type="text" name="deviation" id="input-deviation" value="<?php echo !empty($deviation) ? $deviation : '0'; ?>" class="form-control" /></div>
                        <div class="col-sm-6">
                            <span class="help"><?php echo $help_deviation; ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="input-round_value"><?php echo $entry_round_value; ?></label>
                        <div class="col-sm-4"><input type="text" name="round_value" id="input-round_value" value="<?php echo (int)$round_value; ?>" class="form-control" /></div>
                        <div class="col-sm-6">
                            <span class="help"><?php echo $help_round_value; ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="input-decimal_place"><?php echo $entry_decimal_place; ?></label>
                        <div class="col-sm-4"><input type="text" name="decimal_place" id="input-decimal_place" value="<?php echo (int)$decimal_place; ?>" class="form-control" /></div>
                        <div class="col-sm-6">
                            <span class="help"><?php echo $help_decimal_place; ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="input-autorefresh"><?php echo $entry_autorefresh; ?></label>
                        <div class="col-sm-4">
                            <select name="autorefresh" id="input-autorefresh" class="form-control radio">
                                <option value="0"<?php if (!$autorefresh) { ?> selected="selected"<?php } ?>><?php echo $text_disabled; ?></option>
                                <option value="1"<?php if ($autorefresh) { ?> selected="selected"<?php } ?>><?php echo $text_enabled; ?></option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="panel-heading"><h4 class="panel-title"><?php echo $heading_preview; ?></h4></div><br />
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="input-amount"><?php echo $entry_input; ?></label>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <input type="text" id="input-amount" name="amount" value="100.00" class="form-control" />
                                <div class="input-group-btn">
                                    <select id="input-currency_id" name="currency_id" class="form-control" style="width: 80px;">
                                        <?php foreach ($currencies as $item) { ?>
                                        <option value="<?php echo $item['currency_id']; ?>"<?php if ($item['code'] == $code) { ?> selected="selected"<?php } ?>><?php echo $item['code']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-info" onclick="preview()"><?php echo $button_preview; ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="input-preview"><?php echo $entry_preview; ?></label>
                        <div class="col-sm-4"><input type="text" id="input-preview" value="<?php echo $preview; ?>" readonly="" class="form-control" /></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function calculateRate(reverse)
{
	if (reverse)
    {
        var value = 1/$('#input-value').val();
        value = value.toFixed(8);
        $('#input-reversed_value').val(value);
    }
    else
    {
        var value = 1/$('#input-reversed_value').val();
        value = value.toFixed(8);
        $('#input-value').val(value);
    }
}

function preview()
{
	$.ajax({
		url: 'index.php?route=module/currency_tools/preview&token=<?php echo $token; ?>&currency_id=<?php echo $currency_id; ?>',
		type: 'post',
        dataType: 'html',
        data: $('#form').serialize(),
        beforeSend: function() {
        	
		},
        complete: function() {
			
        },
		success: function(html) {
			$('#input-preview').val(html);
		}
	});
}
</script>
<?php echo $footer; ?>