<div class="modal fade" id="exporter-modal" tabindex="-1" role="dialog" aria-labelledby="exporter-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo $text_close; ?>"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exporter-modal-label"><?php echo $heading; ?></h4>
            </div>
            <div id="modal-body" class="modal-body">
                <?php if (!empty($error)){ ?>
                <div id="message">
                    <?php foreach($error as $item) { ?>
                    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i><?php echo $item; ?></div>
                    <?php } ?>
                </div>
                <?php } else { ?>
                <form action="<?php echo $action; ?>" method="post" id="form-exporter" onsubmit="return checkForm();" class="form-horizontal" target="_blank">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="input-export-criteria"><?php echo $entry_criteria; ?></label>
                        <div class="col-sm-9">
                            <select name="export[criteria]" id="input-export-criteria" class="form-control">
                                <option value="1"><?php echo $text_all; ?></option>
                                <option value="2"><?php echo $text_by_brand; ?></option>
                                <option value="3"><?php echo $text_by_category; ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="export-brand" style="display: none;">
                        <label class="control-label col-sm-3"><?php echo $entry_brand; ?></label>
                        <div class="col-sm-9">
                            <div class="well well-sm" style="height: 150px; overflow: auto;">
                                <table class="" style="width: 100%;height: 150px; overflow: scroll;">
                                    <tbody>
                                        <?php foreach ($brands as $brand) { ?>
                                        <tr>
                                            <td>
                                                <label for="export_brand_<?php echo $brand['manufacturer_id']; ?>"><input type="checkbox" name="export[brand][]" id="export_brand_<?php echo $brand['manufacturer_id']; ?>" value="<?php echo $brand['manufacturer_id']; ?>" /><?php echo $brand['name']; ?></label>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <a onclick="$(this).parent().find(':checkbox').prop('checked', true);" class="btn btn-default"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').prop('checked', false);" class="btn btn-default"><?php echo $text_unselect_all; ?></a>
                        </div>
                    </div>
                    <div class="form-group" id="export-category" style="display: none;">
                        <label class="control-label col-sm-3"><?php echo $entry_category; ?></label>
                        <div class="col-sm-9">
                            <div class="well well-sm" style="height: 150px; overflow: auto;">
                                <table class="" style="width: 100%;height: 150px; overflow: scroll;">
                                    <tbody>
                                        <?php foreach ($categories as $category) { ?>
                                        <tr>
                                            <td>
                                                <label for="export_category_<?php echo $category['category_id']; ?>"><input type="checkbox" name="export[category][]" id="export_category_<?php echo $category['category_id']; ?>" value="<?php echo $category['category_id']; ?>" /><?php echo $category['name']; ?></label>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <a onclick="$(this).parent().find(':checkbox').prop('checked', true);" class="btn btn-default"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').prop('checked', false);" class="btn btn-default"><?php echo $text_unselect_all; ?></a>
                        </div>
                    </div>
                </form>
                <?php } ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i>  <?php echo $text_close; ?></button>
                <?php if (empty($error)){ ?>
                <button type="submit" class="btn btn-primary" form="form-exporter"><i class="fa fa-download"></i> <?php echo $button_export; ?></button>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    function reposition()
    {
        var modal = $(this),
        dialog = modal.find('.modal-dialog');
        modal.css('display', 'block');
        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
    }
    
    $('.modal').on('show.bs.modal', reposition);
    
    $(window).on('resize', function() {
        $('.modal:visible').each(reposition);
    });
    
    $('#input-export-criteria').change(function(){
        $('#export-brand,#export-category').hide();
        if ($('#input-export-criteria').val() == '1')
        {
            $('#export-brand,#export-category').find(':checkbox').prop('checked', false);
        }
        else if ($('#input-export-criteria').val() == '2')
        {
            $('#export-brand').show();
            $('#export-category').find(':checkbox').prop('checked', false);
        }
        else if ($('#input-export-criteria').val() == '3')
        {
            $('#export-category').show();
            $('#export-brand').find(':checkbox').prop('checked', false);
        }
        
        $('.modal:visible').each(reposition);
    });
});

function checkForm()
{
    if (confirm('<?php echo $text_confirm; ?>'))
    {
        return true;
    }
    else
    {
        return false;
    }
}
</script>