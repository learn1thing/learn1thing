<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
    <div class="container-fluid">
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
        </ul>
    </div>
    <div class="container-fluid">
        
        <div id="message"<?php if (!$success && !$error){ ?> style="display: none;"<?php } ?>>
            <?php foreach($success as $item) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i><?php echo $item; ?> <button type="button" class="close" data-dismiss="alert">×</button></div>
            <?php } ?>
            <?php foreach($error as $item) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i><?php echo $item; ?> <button type="button" class="close" data-dismiss="alert">×</button></div>
            <?php } ?>
        </div>
        
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
                <div class="pull-right btn-group" role="group">
                    <a onclick="jQuery('#form').submit()" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i> <?php echo $button_save; ?></a>
                    <a href="<?php echo $refresh; ?>" data-toggle="tooltip" title="<?php echo $button_refresh; ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> <?php echo $button_refresh; ?></a>
                    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-general" data-toggle="tab"><i class="fa fa-power-off"></i> <?php echo $tab_general; ?></a></li>
                        <li><a href="#tab-currency" data-toggle="tab"><i class="fa fa-list"></i> <?php echo $tab_currency; ?></a></li>
                        <li><a href="#tab-widget" data-toggle="tab"><i class="fa fa-puzzle-piece"></i> <?php echo $tab_widget; ?></a></li>
                        <li><a href="#tab-tools" data-toggle="tab"><i class="fa fa-cogs"></i> <?php echo $tab_tools; ?></a></li>
                        <li><a href="#tab-addist" data-toggle="tab"><i class="fa fa-wrench"></i> <?php echo $tab_addist; ?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-general">
                            <div class="panel-heading"><h4 class="panel-title"><?php echo $heading_general; ?></h4></div><br />
                            <?php if (STORE_ID == 0){ ?>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="input-status"><?php echo $entry_status; ?></label>
                                <div class="col-sm-3">
                                    <select name="currency_tools[status]" id="input-status" class="form-control radio">
                                        <option value="0"<?php if (!$status) { ?> selected="selected"<?php } ?>><?php echo $text_disabled; ?></option>
                                        <option value="1"<?php if ($status) { ?> selected="selected"<?php } ?>><?php echo $text_enabled; ?></option>
                                    </select>
                                </div>
                                <div class="col-sm-7">
                                    <span class="help"><?php echo $help_status; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="input-currency_admin"><?php echo $entry_currency_admin; ?></label>
                                <div class="col-sm-3">
                                    <select name="config[currency]" id="input-currency_admin" class="form-control">
                                        <?php foreach ($currencies as $item) { ?>
                                        <option value="<?php echo $item['code']; ?>"<?php if ($item['code'] == $currency_admin) { ?> selected="selected"<?php } ?>><?php echo $item['code']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-7">
                                    <span class="help"><?php echo $help_currency_admin; ?></span>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="input-currency_product"><?php echo $entry_currency_product; ?></label>
                                <div class="col-sm-3">
                                    <select name="currency_tools[currency_product]" id="input-currency_product" class="form-control">
                                        <?php foreach ($currencies as $item) { ?>
                                        <option value="<?php echo $item['code']; ?>"<?php if ($item['code'] == $currency_product) { ?> selected="selected"<?php } ?>><?php echo $item['code']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-7">
                                    <span class="help"><?php echo $help_currency_product; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="input-currency_store"><?php echo $entry_currency_store; ?></label>
                                <div class="col-sm-3">
                                    <select name="currency_tools[currency_store]" id="input-currency_store" class="form-control">
                                        <option value=""><?php echo $text_default; ?></option>
                                        <option value="AUTO"<?php if ($currency_store == 'AUTO') { ?> selected="selected"<?php } ?>><?php echo $text_autodetect; ?></option>
                                        <?php foreach ($currencies as $item) { ?>
                                        <option value="<?php echo $item['code']; ?>"<?php if ($item['code'] == $currency_store) { ?> selected="selected"<?php } ?>><?php echo $item['code']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-7">
                                    <span class="help"><?php echo $help_currency_store; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="input-country_detector"><?php echo $entry_country_detector; ?></label>
                                <div class="col-sm-3">
                                    <select name="currency_tools[country_detector]" id="input-country_detector" class="form-control">
                                        <option value="nekudo"<?php if ($country_detector == 'nekudo') { ?> selected="selected"<?php } ?>>geoip.nekudo.com</option>
                                        <option value="geoiplookup"<?php if ($country_detector == 'geoiplookup') { ?> selected="selected"<?php } ?>>geoiplookup.net</option>
                                        <option value="ip_api"<?php if ($country_detector == 'ip_api') { ?> selected="selected"<?php } ?>>ip-api.com</option>
                                    </select>
                                </div>
                                <div class="col-sm-7">
                                    <span class="help"><?php echo $help_country_detector; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="input-session_lifetime"><?php echo $entry_session_lifetime; ?></label>
                                <div class="col-sm-3">
                                    <div class="input-group">
                                        <input name="currency_tools[session_lifetime]" id="input-session_lifetime" value="<?php echo (int)$session_lifetime; ?>" class="form-control" />
                                        <div class="input-group-addon">sec.</div>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <span class="help"><?php echo $help_session_lifetime; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="input-restriction"><?php echo $entry_restriction; ?></label>
                                <div class="col-sm-3">
                                    <select name="currency_tools[restriction]" id="input-restriction" class="form-control radio">
                                        <option value="0"<?php if (!$restriction) { ?> selected="selected"<?php } ?>><?php echo $text_disabled; ?></option>
                                        <option value="1"<?php if ($restriction) { ?> selected="selected"<?php } ?>><?php echo $text_enabled; ?></option>
                                    </select>
                                </div>
                                <div class="col-sm-7">
                                    <span class="help"><?php echo $help_restriction; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="input-currency_secondary"><?php echo $entry_currency_secondary; ?></label>
                                <div class="col-sm-3">
                                    <select name="currency_tools[currency_secondary]" id="input-currency_secondary" class="form-control">
                                        <option value=""><?php echo $text_none; ?></option>
                                        <?php foreach ($currencies as $item) { ?>
                                        <option value="<?php echo $item['code']; ?>"<?php if ($item['code'] == $currency_secondary) { ?> selected="selected"<?php } ?>><?php echo $item['code']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-7">
                                    <span class="help"><?php echo $help_currency_secondary; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="input-currency_payment"><?php echo $entry_currency_payment; ?></label>
                                <div class="col-sm-3">
                                    <select name="currency_tools[currency_payment]" id="input-currency_payment" class="form-control">
                                        <option value=""><?php echo $text_default; ?></option>
                                        <?php foreach ($currencies as $item) { ?>
                                        <option value="<?php echo $item['code']; ?>"<?php if ($item['code'] == $currency_payment) { ?> selected="selected"<?php } ?>><?php echo $item['code']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-7">
                                    <span class="help"><?php echo $help_currency_payment; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="input-template"><?php echo $entry_template; ?></label>
                                <div class="col-sm-3">
                                    <textarea name="currency_tools[template]" id="input-template" class="form-control" rows="6"><?php echo $template; ?></textarea>
                                </div>
                                <div class="col-sm-7">
                                    <span class="help"><?php echo $help_template; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="input-use_core"><?php echo $entry_use_core; ?></label>
                                <div class="col-sm-3">
                                    <select name="currency_tools[use_core]" id="input-use_core" class="form-control radio">
                                        <option value="0"<?php if (!$use_core) { ?> selected="selected"<?php } ?>><?php echo $text_no; ?></option>
                                        <option value="1"<?php if ($use_core) { ?> selected="selected"<?php } ?>><?php echo $text_yes; ?></option>
                                    </select>
                                </div>
                                <div class="col-sm-7">
                                    <span class="help"><?php echo $help_use_core; ?></span>
                                </div>
                            </div>
                            
                            <?php if (STORE_ID == 0){ ?>
                            <div class="panel-heading"><h4 class="panel-title"><?php echo $heading_refresh; ?></h4></div><br />
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="input-autorefresh"><?php echo $entry_autorefresh; ?></label>
                                <div class="col-sm-3">
                                    <select name="currency_tools[autorefresh]" id="input-autorefresh" class="form-control radio">
                                        <option value="0"<?php if (!$autorefresh) { ?> selected="selected"<?php } ?>><?php echo $text_disabled; ?></option>
                                        <option value="1"<?php if ($autorefresh) { ?> selected="selected"<?php } ?>><?php echo $text_enabled; ?></option>
                                    </select>
                                </div>
                                <div class="col-sm-7">
                                    <span class="help"><?php echo $help_autorefresh; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="input-hours"><?php echo $entry_hours; ?></label>
                                <div class="col-sm-3">
                                    <select name="currency_tools[hours][]" id="input-hours" class="form-control" multiple="true">
                                        <?php foreach($day_hours as $i=>$hour) { ?>
                                        <option value="<?php echo $hour; ?>" <?php if (in_array($hour,$hours)) echo 'selected="true"'; ?>><?php echo $hour.':00'; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-7">
                                    <span class="help"><?php echo $help_hours; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="input-provider"><?php echo $entry_provider; ?></label>
                                <div class="col-sm-3">
                                    <select name="currency_tools[provider]" id="input-provider" class="form-control">
                                        <?php foreach ($providers as $code=>$item) { ?>
                                        <option value="<?php echo $code; ?>"<?php if ($code == $provider) { ?> selected="selected"<?php } ?>><?php echo $item['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-7">
                                    <span class="help"><?php echo $help_provider; ?></span>
                                </div>
                            </div>
                            <div id="provider-form">
                                <?php echo $provider_form;  ?>
                            </div>
                            
                            <div class="panel-heading"><h4 class="panel-title"><?php echo $heading_cron; ?></h4></div><br />
                            <div class="form-group">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <span class="help"><?php echo $help_cron; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="input-cron"><?php echo $entry_cron; ?></label>
                                <div class="col-sm-6">
                                    <input id="input-cron" value="<?php echo $cron; ?>" class="form-control" readonly="" onclick="this.select();" />
                                </div>
                                <label class="control-label col-sm-2" for="input-executed"><?php echo $entry_executed; ?></label>
                                <div class="col-sm-2">
                                    <input id="input-executed" value="<?php echo $executed; ?>" class="form-control" readonly="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="input-progress"><?php echo $entry_progress; ?></label>
                                <div class="col-sm-2">
                                    <div class="input-group">
                                        <input id="input-progress" value="<?php echo $progress; ?>" class="form-control" readonly="" />
                                        <div class="input-group-addon">%</div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading-payment_methods">
                                            <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-payment_methods" aria-expanded="false" aria-controls="collapse-payment_methods"><?php echo $heading_payment; ?></a></h4>
                                        </div>
                                        <div id="collapse-payment_methods" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-payment_methods">
                                            <div class="panel-body">
                                                <span class="help"><?php echo $help_payment_methods; ?></span>
                                                <?php foreach(array_chunk($payment_methods,2) as $items){ ?>
                                                <div class="form-group">
                                                    <?php foreach($items as $payment_method) { ?>
                                                    <label class="col-sm-4 control-label" for="input-payment_method-<?php echo $payment_method['code']; ?>"><?php echo $payment_method['name']; ?></label>
                                                    <div class="col-sm-2">
                                                        <select name="currency_tools[payment_methods][<?php echo $payment_method['code']; ?>]" id="input-payment_method-<?php echo $payment_method['code']; ?>" class="form-control">
                                                            <option value="">-</option>
                                                            <?php foreach ($currencies as $item) { ?>
                                                            <option value="<?php echo $item['code']; ?>"<?php if ($item['code'] == $payment_method['currency']) { ?> selected="selected"<?php } ?>><?php echo $item['code']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
        
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading-shipping_methods">
                                            <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-shipping_methods" aria-expanded="false" aria-controls="collapse-shipping_methods"><?php echo $heading_shipping; ?></a></h4>
                                        </div>
                                        <div id="collapse-shipping_methods" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-shipping_methods">
                                            <div class="panel-body">
                                                <span class="help"><?php echo $help_shipping_methods; ?></span>
                                                <?php foreach(array_chunk($shipping_methods,2) as $items){ ?>
                                                <div class="form-group">
                                                    <?php foreach($items as $shipping_method) { ?>
                                                    <label class="col-sm-4 control-label" for="input-shipping_method-<?php echo $shipping_method['code']; ?>"><?php echo $shipping_method['name']; ?></label>
                                                    <div class="col-sm-2">
                                                        <select name="currency_tools[shipping_methods][<?php echo $shipping_method['code']; ?>]" id="input-shipping_method-<?php echo $shipping_method['code']; ?>" class="form-control">
                                                            <option value="">-</option>
                                                            <?php foreach ($currencies as $item) { ?>
                                                            <option value="<?php echo $item['code']; ?>"<?php if ($item['code'] == $shipping_method['currency']) { ?> selected="selected"<?php } ?>><?php echo $item['code']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading-countries">
                                            <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-countries" aria-expanded="false" aria-controls="collapse-countries"><?php echo $heading_country; ?></a></h4>
                                        </div>
                                        <div id="collapse-countries" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-countries">
                                            <div class="panel-body">
                                                <span class="help"><?php echo $help_countries; ?></span>
                                                <?php foreach(array_chunk($countries,4) as $items){ ?>
                                                <div class="form-group">
                                                    <?php foreach($items as $country) { ?>
                                                    <label class="control-label col-sm-2" for="input-payment_method-<?php echo $country['code']; ?>"><?php echo $country['name']; ?></label>
                                                    <div class="col-sm-1">
                                                        <select name="currency_tools[countries][<?php echo $country['code']; ?>]" id="input-payment_method-<?php echo $country['code']; ?>" class="form-control">
                                                            <option value="">-</option>
                                                            <?php foreach ($currencies as $item) { ?>
                                                            <option value="<?php echo $item['code']; ?>"<?php if ($item['code'] == $country['currency']) { ?> selected="selected"<?php } ?>><?php echo $item['code']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-currency">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <td class="text-left" style="width:200px;"><?php echo $entry_name; ?></td>
                                            <td class="text-left" style="width:30px;"><?php echo $entry_code; ?></td>
                                            <td class="text-right"><?php echo $entry_value; ?></td>
                                            <td class="text-right"><?php echo $entry_round_value; ?></td>
                                            <td class="text-right"><?php echo $entry_decimal_place; ?></td>
                                            <td class="text-right"><?php echo $entry_correction; ?></td>
                                            <td class="text-right"><?php echo $entry_margin; ?></td>
                                            <td class="text-right"><?php echo $entry_overheads; ?></td>
                                            <td class="text-left"><?php echo $entry_autorefresh; ?></td>
                                            <td class="text-center" style="width:100px;"><?php echo $entry_action; ?></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if ($currencies) { ?>
                                        <?php foreach ($currencies as $currency) { ?>
                                        <tr>
                                            <td class="text-left"><?php echo $currency['title']; ?></td>
                                            <td class="text-left"><?php echo $currency['code']; ?></td>
                                            <td class="text-right"><?php echo $currency['value']; ?></td>
                                            <td class="text-right"><?php echo (int)$currency['round_value']; ?></td>
                                            <td class="text-right"><?php echo (int)$currency['decimal_place']; ?></td>
                                            <td class="text-right"><?php echo (float)$currency['correction']; ?></td>
                                            <td class="text-right"><?php echo (float)$currency['margin']; ?></td>
                                            <td class="text-right"><?php echo (float)$currency['overheads']; ?></td>
                                            <td class="text-left"><?php echo $currency['autorefresh'] ? $text_enabled : $text_disabled; ?></td>
                                            <td class="text-center"><a href="<?php echo $currency['href']; ?>" data-toggle="tooltip" title="<?php echo $text_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                                        </tr>
                                        <?php } ?>
                                        <?php } else { ?>
                                        <tr>
                                            <td class="text-center" colspan="11"><?php echo $text_no_results; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-widget">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="input-widget_status"><?php echo $entry_status; ?></label>
                                <div class="col-sm-4">
                                    <select name="currency_tools[widget_status]" id="input-widget_status" class="form-control radio">
                                        <option value="0"<?php if (!$widget_status) { ?> selected="selected"<?php } ?>><?php echo $text_disabled; ?></option>
                                        <option value="1"<?php if ($widget_status) { ?> selected="selected"<?php } ?>><?php echo $text_enabled; ?></option>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <span class="help"><?php echo $help_widget_status; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="input-widget_currency"><?php echo $entry_widget_currency; ?></label>
                                <div class="col-sm-4">
                                    <select name="currency_tools[widget_currency]" id="input-widget_currency" class="form-control"> 
                                        <?php foreach ($currencies as $currency) { ?>
                                        <option value="<?php echo $currency['code']; ?>"<?php if ($currency['code'] == $widget_currency) { ?> selected="selected"<?php } ?>><?php echo $currency['title']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <span class="help"><?php echo $help_widget_currency; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="input-widget_tag"><?php echo $entry_widget_tag; ?></label>
                                <div class="col-sm-4"><input type="text" name="currency_tools[widget_tag]" id="input-widget_tag" value="<?php echo $widget_tag; ?>" class="form-control" /></div>
                                <div class="col-sm-6">
                                    <span class="help"><?php echo $help_widget_tag; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="input-widget_container"><?php echo $entry_widget_container; ?></label>
                                <div class="col-sm-4"><input type="text" name="currency_tools[widget_container]" id="input-widget_container" value="<?php echo $widget_container; ?>" class="form-control" /></div>
                                <div class="col-sm-6">
                                    <span class="help"><?php echo $help_widget_container; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="input-widget_selector"><?php echo $entry_widget_selector; ?></label>
                                <div class="col-sm-4"><input type="text" name="currency_tools[widget_selector]" id="input-widget_selector" value="<?php echo $widget_selector; ?>" class="form-control" /></div>
                                <div class="col-sm-6">
                                    <span class="help"><?php echo $help_widget_selector; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="input-widget_method"><?php echo $entry_widget_method; ?></label>
                                <div class="col-sm-4">
                                    <select name="currency_tools[widget_method]" id="input-widget_method" class="form-control">
                                        <option value="appendTo"<?php if ($widget_method == 'appendTo') { ?> selected="selected"<?php } ?>>appendTo(...)</option>
                                        <option value="prependTo"<?php if ($widget_method == 'prependTo') { ?> selected="selected"<?php } ?>>prependTo(...)</option>
                                        <option value="insertBefore"<?php if ($widget_method == 'insertBefore') { ?> selected="selected"<?php } ?>>insertBefore(...)</option>
                                        <option value="insertAfter"<?php if ($widget_method == 'insertAfter') { ?> selected="selected"<?php } ?>>insertAfter(...)</option>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <span class="help"><?php echo $help_widget_method; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="input-widget_style"><?php echo $entry_widget_style; ?></label>
                                <div class="col-sm-4"><textarea name="currency_tools[widget_style]" id="input-widget_style" class="form-control"><?php echo $widget_style; ?></textarea></div>
                                <div class="col-sm-6"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2"><?php echo $entry_widget_template; ?></label>
                                <div class="col-sm-4">
                                    <ul class="nav nav-tabs">
                                    <?php $i = 0; foreach ($languages as $language) { $i++; ?>
                                        <li class="<?php if ($i==1) echo 'active'; ?>"><a href="#tab-language-<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                    <?php } ?>
                                    </ul>
                                    <div class="tab-content">
                                        <?php $i = 0; foreach ($languages as $language) { $i++; ?>
                                        <div class="tab-pane <?php if ($i==1) echo 'active'; ?>" id="tab-language-<?php echo $language['language_id']; ?>">
                                            <textarea name="currency_tools[widget_template][<?php echo $language['code']; ?>]" class="form-control"><?php echo isset($widget_template[$language['code']]) ? $widget_template[$language['code']] : '{main} = {store}'; ?></textarea>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <span class="help"><?php echo $help_widget_template; ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-tools">
                            <div class="col-sm-4">
                                <div class="" style="padding: 30px 10px; border: 1px solid #ddd; border-radius: 5px; min-height: 220px;">
                                    <h2 class="text-center"><i class="fa fa-exchange"></i> <?php echo $text_converter; ?></h2>
                                    <p class="text-center"><span class="help"><?php echo $help_convert; ?></span></p>
                                    <p class="text-center"><a class="btn btn-primary" onclick="openConverter()"><?php echo $button_open; ?></a></p>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="" style="padding: 30px 10px; border: 1px solid #ddd; border-radius: 5px; min-height: 220px;">
                                    <h2 class="text-center"><i class="fa fa-download"></i> <?php echo $text_exporter; ?></h2>
                                    <p class="text-center"><span class="help"><?php echo $help_export; ?></span></p>
                                    <p class="text-center"><a class="btn btn-primary" onclick="openExporter()"><?php echo $button_open; ?></a></p>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="" style="padding: 30px 10px; border: 1px solid #ddd; border-radius: 5px; min-height: 220px;">
                                    <h2 class="text-center"><i class="fa fa-upload"></i> <?php echo $text_importer; ?></h2>
                                    <p class="text-center"><span class="help"><?php echo $help_import; ?></span></p>
                                    <p class="text-center"><a class="btn btn-primary" onclick="openImporter()"><?php echo $button_open; ?></a></p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-addist"><?php echo $addist_tab; ?></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="modal-container"></div>
<script type="text/javascript">
$(document).ready(function(){
    $('#input-provider').change(function(){
        changeProvider();
    });
});

function changeProvider()
{
	$.ajax({
		url: 'index.php?route=module/currency_tools/provider&token=<?php echo $token; ?>&code=' +  $('#input-provider').val(),
		dataType: 'html',
        beforeSend: function() {
        	$('#provider-form').find('input, select, textarea').prop('disabled',true);
		},
        complete: function() {
			$('#provider-form').find('input, select, textarea').prop('disabled',false);
        },
		success: function(html) {
			$('#provider-form').html(html);
		}
	});
}
</script>

<script type="text/javascript">
function openConverter()
{
    $('#modal-container').load('index.php?route=module/currency_tools/converter&token=<?php echo $token; ?>','',function(){
        $('#converter-modal').modal({
            show: true,
        });
    });
}

function openExporter()
{
    $('#modal-container').load('index.php?route=module/currency_tools/exporter&token=<?php echo $token; ?>','',function(){
        $('#exporter-modal').modal({
            show: true,
        });
    });
}

function openImporter()
{
    $('#modal-container').load('index.php?route=module/currency_tools/importer&token=<?php echo $token; ?>','',function(){
        $('#importer-modal').modal({
            show: true,
        });
    });
}
</script>
<?php echo $footer; ?>