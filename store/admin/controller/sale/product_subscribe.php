<?php 
class ControllerSaleProductSubscribe extends Controller {
	private $error = array(); 
	 
	public function index() { 
		$this->load->language('sale/product_subscribe');
		
		$this->load->model('sale/product_subscribe');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_records'] = $this->language->get('text_no_records');
		$data['text_color_code'] = $this->language->get('text_color_code');
		$data['text_delete_confirm'] = $this->language->get('text_delete_confirm');
		$data['text_delete_multiple_confirm'] = $this->language->get('text_delete_multiple_confirm');
		$data['text_until_cancelled'] = $this->language->get('text_until_cancelled');
		$data['text_update_cost_help'] = $this->language->get('text_update_cost_help');
		$data['text_view'] = $this->language->get('text_view');
		$data['text_confirm'] = $this->language->get('text_confirm');
		
		$data['column_order_id'] = $this->language->get('column_order_id');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_customer'] = $this->language->get('column_customer');
		$data['column_product_name'] = $this->language->get('column_product_name');
		$data['column_trial_end'] = $this->language->get('column_trial_end');
		$data['column_terms'] = $this->language->get('column_terms');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_start_date'] = $this->language->get('column_start_date');
		$data['column_end_date'] = $this->language->get('column_end_date');
		$data['column_amount'] = $this->language->get('column_amount');
		$data['column_next_due'] = $this->language->get('column_next_due');
		$data['column_last_paid'] = $this->language->get('column_last_paid');
		$data['column_active'] = $this->language->get('column_active');
		$data['column_action'] = $this->language->get('column_action');
		
		$data['button_view'] = $this->language->get('button_view');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_cancel'] = $this->language->get('button_cancel');
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		$url = '';
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		if (isset($this->session->data['error'])) {
			$data['error_warning'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} elseif (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('sale/product_subscribe', 'token=' . $this->session->data['token'] . $url, true)
		);
  		
		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();
		
		$data['add'] = $this->url->link('sale/product_subscribe/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['cancel'] = $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true);	
		
		$filter_data = array(
			'start'           => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'           => $this->config->get('config_limit_admin')
		);
		
		$data['subscriptions'] = array();
		
		$results = $this->model_sale_product_subscribe->getOrderSubscriptions($filter_data);
		
		if ($results) {
			$this->load->model('sale/order');
			
			$this->load->model('catalog/product');
			
			foreach ($results as $result) {
				$subscription_info = $this->model_sale_product_subscribe->getSubscription($result['subscription_id']);
				
				$order_info = $this->model_sale_order->getOrder($result['order_id']);
				
				$order_recurring_id = $this->model_sale_product_subscribe->getOrderRecurringId($result['order_id'], $result['product_id']);
				
				$product_info = $this->model_catalog_product->getProduct($result['product_id']);
				
				$profile = $this->model_sale_product_subscribe->getProfile($order_recurring_id);
				
				$active = $result['active'];
				
				$trial_end_date = '';
				
				$end_date = '';
				
				$next_due = '';
				
				$last_paid = '';
				
				$grace_period = 0;
				
				$expiring = 0;
				
				if ($result['trial']) {
					$trial_end_date = date($this->language->get('date_format_short'), $result['trial_end_date']);
				}
				
				if (!$subscription_info['trial_only']) {
					if ($result['end_date'] > 0) {
						if (isset($subscription_info['grace_period']) && $subscription_info['grace_period']) {
							$grace_period = 86400 * $subscription_info['grace_period'];
						}
						
						$expires = $result['end_date'] + $grace_period;
						
						if (time() > $result['end_date'] + $grace_period) {
							$active = 0;
							$this->db->query("UPDATE " . DB_PREFIX . "order_subscriptions SET active = 0 WHERE order_subscription_id = '" . (int)$result['order_subscription_id'] . "'");
						}
						
						if (isset($subscription_info['reminder_count']) && $subscription_info['reminder_count']) {
							if ($expires - time() < (86400 * ($subscription_info['reminder_count'] * $subscription_info['reminder_period']) + $grace_period)) {
								$expiring = 1;
							}
						}
						
						$end_date = date($this->language->get('date_format_short'), $result['end_date']);
					}
					
					if ($result['start_date']) {
						$start_date = date($this->language->get('date_format_short'), $result['start_date']);
					} else {
						$start_date = date($this->language->get('date_format_short'), $result['trial_start_date']);
					}
					
					if ($result['next_due'] < time()) {
						$overdue = 1;
					} else {
						$overdue = 0;
					}
					
					if ($result['next_due']) {
						$next_due = date($this->language->get('date_format_short'), $result['next_due']);
					}
					
					if ($result['last_paid']) {
						$last_paid = date($this->language->get('date_format_short'), $result['last_paid']);
					}
					
					$terms = $result['terms'];
					
					$amount = $this->currency->format($result['payment_amount'], $order_info['currency_code'], $order_info['currency_value'], $this->config->get('config_currency'), false);
					
				} else {
					
					$start_date = date($this->language->get('date_format_short'), $result['trial_start_date']);
					
					$end_date = $trial_end_date;
					
					$next_due = "N/A";
					
					$overdue = 0;
					
					$last_paid = date($this->language->get('date_format_short'), $result['trial_start_date']);
					
					$terms = '';
					
					$amount = "N/A";
				
				}
				
				$href = $this->url->link('sale/product_subscribe/info', 'token=' . $this->session->data['token'] . '&order_recurring_id=' . $order_recurring_id . '&order_subscription_id=' . $result['order_subscription_id'], true);
				
				$data['subscriptions'][] = array(
					'order_subscription_id'	=> $result['order_subscription_id'],
					'order_recurring_id'	=> $order_recurring_id,
					'subscription_id'		=> $result['subscription_id'],
					'product_id'			=> $result['product_id'],
					'status'				=> isset($profile['status']) ? $profile['status'] : '',
					'order_id'				=> $result['order_id'],
					'name'					=> $product_info['name'],
					'trial_end'				=> $trial_end_date,
					'terms'					=> $terms,
					'customer'				=> $order_info['firstname'] . ' ' . $order_info['lastname'],
					'quantity'				=> 1,
					'start_date'			=> $start_date,
					'end_date'				=> $end_date,
					'href'					=> $href,
					'expiring'				=> $expiring,
					'overdue'				=> $overdue,
					'recurring'				=> $result['recurring'],
					'amount'				=> $amount,
					'next_due'				=> $next_due,
					'last_paid'				=> $last_paid,
					'selected'      		=> isset($this->request->post['selected']) && in_array($result['order_id'], $this->request->post['selected']),
					'delete'				=> $this->url->link('sale/product_subscribe/singleDelete', 'token=' . $this->session->data['token'] . '&order_subscription_id=' . $result['order_subscription_id'], true),
					'active'				=> $active
				);
			}
		}
		
		$subscription_total = $this->model_sale_product_subscribe->countOrderSubscriptions($filter_data);

		$pagination = new Pagination();
		$pagination->total = $subscription_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('sale/product_subscribe', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($subscription_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($subscription_total - $this->config->get('config_limit_admin'))) ? $subscription_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $subscription_total, ceil($subscription_total / $this->config->get('config_limit_admin')));
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('sale/product_subscribe_list', $data));
	}
	
	public function info() {
		$this->load->model('sale/product_subscribe');
		
		$this->load->model('sale/order');
		
		$this->load->model('catalog/product');
		
		$this->load->model('sale/recurring');
		
		$this->language->load('sale/product_subscribe');
		
		$this->language->load('sale/recurring');
		
		if (isset($this->request->get['order_recurring_id'])) {
			$order_recurring_id = $this->request->get['order_recurring_id'];
		} else {
			$order_recurring_id = 0;
		}
		
		$order_recurring_info = $this->model_sale_recurring->getRecurring($order_recurring_id);

		if ($order_recurring_info) {
			$this->load->language('sale/recurring');
		
			$this->document->setTitle($this->language->get('heading_title'));

			$data['heading_title'] = $this->language->get('heading_title');

			$data['text_recurring_detail'] = $this->language->get('text_recurring_detail');
			$data['text_order_detail'] = $this->language->get('text_order_detail');
			$data['text_product_detail'] = $this->language->get('text_product_detail');
			$data['text_transaction'] = $this->language->get('text_transaction');
			$data['text_order_recurring_id'] = $this->language->get('text_order_recurring_id');
			$data['text_reference'] = $this->language->get('text_reference');
			$data['text_recurring_name'] = $this->language->get('text_recurring_name');
			$data['text_recurring_description'] = $this->language->get('text_recurring_description');
			$data['text_recurring_status'] = $this->language->get('text_recurring_status');			
			$data['text_payment_method'] = $this->language->get('text_payment_method');
			$data['text_order_id'] = $this->language->get('text_order_id');
			$data['text_customer'] = $this->language->get('text_customer');
			$data['text_email'] = $this->language->get('text_email');
			$data['text_order_status'] = $this->language->get('text_order_status');
			$data['text_date_added'] = $this->language->get('text_date_added');
			$data['text_product'] = $this->language->get('text_product');
			$data['text_quantity'] = $this->language->get('text_quantity');
			$data['text_no_results'] = $this->language->get('text_no_results');
			
			$data['column_date_added'] = $this->language->get('column_date_added');
			$data['column_amount'] = $this->language->get('column_amount');
			$data['column_type'] = $this->language->get('column_type');

			$data['button_cancel'] = $this->language->get('button_cancel');

			$data['token'] = $this->request->get['token'];
			
			$url = '';

			if (isset($this->request->get['filter_order_recurring_id'])) {
				$url .= '&filter_order_recurring_id=' . $this->request->get['filter_order_recurring_id'];
			}

			if (isset($this->request->get['filter_order_id'])) {
				$url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
			}

			if (isset($this->request->get['filter_reference'])) {
				$url .= '&filter_reference=' . $this->request->get['filter_reference'];
			}

			if (isset($this->request->get['filter_customer'])) {
				$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('sale/recurring', 'token=' . $this->session->data['token'] . $url, true)
			);

			$data['cancel'] = $this->url->link('sale/product_subscribe', 'token=' . $this->session->data['token'] . $url, true);
			
			// Recurring
			$data['order_recurring_id'] = $order_recurring_info['order_recurring_id'];
			$data['reference'] = $order_recurring_info['reference'];
			$data['recurring_name'] = $order_recurring_info['recurring_name'];
			
			if ($order_recurring_info['recurring_id']) {
				$data['recurring'] = $this->url->link('catalog/recurring/edit', 'token=' . $this->session->data['token'] . '&recurring_id=' . $order_recurring_info['recurring_id'], true);
			} else {
				$data['recurring'] = '';
			}			
			
			$data['recurring_description'] = $order_recurring_info['recurring_description'];
			
			if ($order_recurring_info['status']) {
				$data['recurring_status']= $this->language->get('text_status_' . $order_recurring_info['status']);
			} else {
				$data['recurring_status'] = '';
			}
			
			$this->load->model('sale/order');

			$order_info = $this->model_sale_order->getOrder($order_recurring_info['order_id']);
			
			$data['payment_method'] = $order_info['payment_method'];
			
			// Order
			$data['order_id'] = $order_info['order_id'];
			$data['order'] = $this->url->link('sale/order/info', 'token=' . $this->session->data['token'] . '&order_id=' . $order_info['order_id'], true);
			$data['firstname'] = $order_info['firstname'];
			$data['lastname'] = $order_info['lastname'];
			
			if ($order_info['customer_id']) {
				$data['customer'] = $this->url->link('customer/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $order_info['customer_id'], true);
			} else {
				$data['customer'] = '';
			}
		
			$data['email'] = $order_info['email'];
			$data['order_status'] = $order_info['order_status'];
			$data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));
			
			// Product
			$data['product'] = $order_recurring_info['product_name'];
			$data['quantity'] = $order_recurring_info['product_quantity'];

			// Transactions
			$data['transactions'] = array();
			
			$transactions = $this->model_sale_recurring->getRecurringTransactions($order_recurring_info['order_recurring_id']);

			foreach ($transactions as $transaction) {
				$data['transactions'][] = array(
					'date_added' => $transaction['date_added'],
					'type'       => $transaction['type'],
					'amount'     => $this->currency->format($transaction['amount'], $this->config->get('config_currency'), $order_info['currency_code'], $order_info['currency_value'])
				);
			}

			$data['buttons'] = $this->load->controller('payment/' . $order_info['payment_code'] . '/recurringButtons');

			$data['header'] = $this->load->controller('common/header');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['footer'] = $this->load->controller('common/footer');

			$this->response->setOutput($this->load->view('sale/recurring_info', $data));
		} else {
			return new Action('error/not_found');
		}
	}

	public function updateCost() {
		$this->load->language('sale/product_subscribe');
		$this->load->model('sale/product_subscribe');
		
		$parts = explode('_', $this->request->post['form_data']);
		
		$order_subscription_id = $parts[0];
		$order_recurring_id = $parts[1];
		
		$subscription_id = $this->model_sale_product_subscribe->getSubscriptionId($order_subscription_id);
		$subscription_info = $this->model_sale_product_subscribe->getSubscription($subscription_id);
		
		if ($subscription_info['duration'] > 0) {
			$terms = sprintf($this->language->get('text_subscription_duration'), $subscription_info['cycle'], $this->language->get('text_' . $subscription_info['frequency']), $this->currency->format($this->request->post['cost'], $this->config->get('config_currency'), $subscription_info['duration']));
		} else {
			$terms = sprintf($this->language->get('text_subscription_cancel'), $subscription_info['cycle'], $this->language->get('text_' . $subscription_info['frequency']), $this->currency->format($this->request->post['cost'], $this->config->get('config_currency'), $subscription_info['duration']));
		}
		$results = $this->model_sale_product_subscribe->getProfile($order_recurring_id);
		if (isset($results['reference']) && $results['reference'] && ($results['status_id'] == 1 || $results['status_id'] == 2)) {
			if (file_exists(DIR_APPLICATION . 'model/payment/pp_express.php')) {
				$this->load->model('payment/pp_express');
				$result = $this->model_payment_pp_express->recurringUpdate($results['reference'], $this->request->post['cost'], $terms);
				if (isset($result['PROFILEID']) && $result['ACK'] == "Success") {
					$this->model_sale_product_subscribe->updateCost($order_subscription_id, $this->request->post['cost'], $terms);
					$this->db->query("UPDATE " . DB_PREFIX . "order_recurring SET recurring_description = '" . $this->db->escape($terms) . "', recurring_price = '" . (float)$this->request->post['cost'] . "' WHERE order_recurring_id = '" . (int)$order_recurring_id . "' LIMIT 1");
					$this->session->data['success'] = sprintf($this->language->get('text_update_cost_success'), $order_subscription_id);
				} else {
					if (isset($result['L_LONGMESSAGE0'])) {
						$error_message = $result['L_LONGMESSAGE0'];
					} else {
						$error_message = "not specified";
					}
					$this->session->data['error'] = sprintf($this->language->get('error_pp_express'), $error_message);
				}
			} else {
				$this->session->data['error'] = $this->language->get('error_pp_express_install');
			}
		} else {
			$this->model_sale_product_subscribe->updateCost($order_subscription_id, $this->request->post['cost'], $terms);
			$this->session->data['success'] = sprintf($this->language->get('text_update_cost_success'), $order_subscription_id);
		}
		$url = str_replace("&amp;", "&", $this->url->link('sale/product_subscribe', 'token=' . $this->session->data['token'], 'SSL'));
		echo json_encode($url);
	}

	public function pauseSubscription() {
		$this->load->language('sale/product_subscribe');
		
		$this->load->model('sale/product_subscribe');
		
		$order_recurring_id = $this->request->get['order_recurring_id'];
		
		$order_subscription_id = $this->request->get['order_subscription_id'];
		
		$results = $this->model_sale_product_subscribe->getProfile($order_recurring_id);
		
		if ($results['status_id'] == 2) {
			$this->db->query("UPDATE `" . DB_PREFIX . "order_recurring` SET `status` = 3 WHERE `order_recurring_id` = '" . (int)$order_recurring_id . "' LIMIT 1");
			$this->db->query("UPDATE `" . DB_PREFIX . "order_subscriptions` SET active = '0' WHERE order_subscription_id = '" . (int)$order_subscription_id . "'");
			$this->session->data['success'] = $this->language->get('text_pause_success');
		} else {
			$this->session->data['error'] = $this->language->get('error_pausing');
		}
		
		$this->response->redirect($this->url->link('sale/product_subscribe/info', 'order_recurring_id=' . $order_recurring_id . '&order_subscription_id=' . $order_subscription_id . '&token=' . $this->request->get['token'], true));
	}

	public function restartSubscription() {
		$this->load->language('sale/product_subscribe');
		
		$this->load->model('sale/product_subscribe');
		
		$order_recurring_id = $this->request->get['order_recurring_id'];
		
		$order_subscription_id = $this->request->get['order_subscription_id'];
		
		$results = $this->model_sale_product_subscribe->getProfile($order_recurring_id);
		
		if ($results['status_id'] == 3) {
			$next_due_query = $this->db->query("SELECT next_due FROM `" . DB_PREFIX . "order_subscriptions` WHERE order_subscription_id = '" . (int)$order_subscription_id . "'");
			

			if ($next_due_query->row['next_due'] < time()) {
				$next_due = time();
			} else {
				$next_due = $next_due_query->row['next_due'];
			}
			
			$this->db->query("INSERT INTO `" . DB_PREFIX . "order_recurring_transaction` SET `order_recurring_id` = '" . (int)$order_recurring_id . "', `date_added` = NOW(), `type` = '0'");
			
			$this->db->query("UPDATE `" . DB_PREFIX . "order_recurring` SET `status` = 2 WHERE `order_recurring_id` = '" . (int)$order_recurring_id . "' LIMIT 1");
			
			$this->db->query("UPDATE " . DB_PREFIX . "order_subscriptions SET next_due = '" . (int)$next_due . "', active = '1' WHERE order_subscription_id = '" . (int)
			
			$order_subscription_id . "'");
			
			$this->session->data['success'] = $this->language->get('text_restart_success');
		}
		$this->response->redirect($this->url->link('sale/product_subscribe/info', 'order_recurring_id=' . $order_recurring_id . '&order_subscription_id=' . $order_subscription_id . '&token=' . $this->request->get['token'], true));
	}

	public function cancelSubscription() {
		$this->load->language('sale/product_subscribe');
		
		$this->load->model('sale/product_subscribe');
		
		$order_recurring_id = $this->request->get['order_recurring_id'];
		
		$order_subscription_id = $this->request->get['order_subscription_id'];
		
		$results = $this->model_sale_product_subscribe->getProfile($order_recurring_id);
		
		if ($results['status_id'] == 1 || $results['status_id'] == 2) {
			$this->db->query("INSERT INTO `" . DB_PREFIX . "order_recurring_transaction` SET `order_recurring_id` = '" . (int)$order_recurring_id . "', `date_added` = NOW(), `type` = '5'");
			
			$this->db->query("UPDATE `" . DB_PREFIX . "order_recurring` SET `status` = 4 WHERE `order_recurring_id` = '" . (int)$order_recurring_id . "' LIMIT 1");
			
			$this->db->query("UPDATE " . DB_PREFIX . "order_subscriptions SET next_due = '0', active = '0' WHERE order_subscription_id = '" . (int)$order_subscription_id . "'");
			
			$this->session->data['success'] = $this->language->get('text_cancelsub_success');
		}
		
		$this->response->redirect($this->url->link('sale/product_subscribe/info', 'order_recurring_id=' . $order_recurring_id . '&order_subscription_id=' . $order_subscription_id . '&token=' . $this->request->get['token'], true));
	}

	public function updateStartDate() {
		$this->load->language('sale/product_subscribe');
		
		$this->load->model('sale/product_subscribe');
		
		$this->model_sale_product_subscribe->updateStartDate($this->request->post);
		
		$url = str_replace("&amp;", "&", $this->url->link('sale/product_subscribe', 'token=' . $this->session->data['token'], true));
		
		$this->session->data['success'] = sprintf($this->language->get('text_update_start_success'), $this->request->post['order_subscription_id']);
		
		echo json_encode($url);
	
	}
	
	public function updateEndDate() {
		$this->load->language('sale/product_subscribe');
		
		$this->load->model('sale/product_subscribe');
		
		$this->model_sale_product_subscribe->updateEndDate($this->request->post);
		
		$url = str_replace("&amp;", "&", $this->url->link('sale/product_subscribe', 'token=' . $this->session->data['token'], true));
		
		$this->session->data['success'] = sprintf($this->language->get('text_update_end_success'), $this->request->post['order_subscription_id']);
		
		echo json_encode($url);
	
	}
	
	public function updateNextDue() {
		$this->load->language('sale/product_subscribe');
		
		$this->load->model('sale/product_subscribe');
		
		$this->model_sale_product_subscribe->updateNextDue($this->request->post);
		
		$url = str_replace("&amp;", "&", $this->url->link('sale/product_subscribe', 'token=' . $this->session->data['token'], true));
		
		$this->session->data['success'] = sprintf($this->language->get('text_update_next_success'), $this->request->post['order_subscription_id']);
		
		echo json_encode($url);
	
	}
	
	public function updateLastPaid() {
		$this->load->language('sale/product_subscribe');
		
		$this->load->model('sale/product_subscribe');
		
		$this->model_sale_product_subscribe->updateLastPaid($this->request->post);
		
		$url = str_replace("&amp;", "&", $this->url->link('sale/product_subscribe', 'token=' . $this->session->data['token'], true));
		
		$this->session->data['success'] = sprintf($this->language->get('text_update_last_success'), $this->request->post['order_subscription_id']);
		
		echo json_encode($url);
	
	}
	
	public function singleDelete() {
		$this->load->language('sale/product_subscribe');
		
		$this->load->model('sale/product_subscribe');
		
		$this->model_sale_product_subscribe->deleteSubscription($this->request->get['order_subscription_id']);
		/*$url = str_replace("&amp;", "&", $this->url->link('sale/product_subscribe', 'token=' . $this->session->data['token'], true));*/
		$this->session->data['success'] = $this->language->get('text_delete_success');
		/*echo json_encode($url);*/
		$this->response->redirect($this->url->link('sale/product_subscribe', 'token=' . $this->session->data['token'], true));
	
	}
	
	public function activate() {
		$this->load->language('sale/product_subscribe');
		
		$this->load->model('sale/product_subscribe');
		
		$this->model_sale_product_subscribe->activate($this->request->post['order_subscription_id']);
		
		$url = str_replace("&amp;", "&", $this->url->link('sale/product_subscribe', 'token=' . $this->session->data['token'], true));
		
		$this->session->data['success'] = $this->language->get('text_activate_success');
		
		echo json_encode($url);
	}

}
