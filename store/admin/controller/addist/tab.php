<?php
#encode-me[string],encode-me[ioncube]#
class ControllerAddistTab extends AddistController
{   
	public function index($data = array())
    {
        //texts
        $data = array_merge($this->data, $data);
        
        $data['token'] = $this->session->data['token'];
        
        $data['action'] = $this->url->link($data['group'].'/'.$data['code'], 'token=' . $this->session->data['token'], 'SSL', true);
        $data['reinstall'] = $this->url->link($data['group'].'/'.$data['code'].'/reinstall', 'token=' . $this->session->data['token'], 'SSL');
        $data['install_mods'] = $this->url->link($data['group'].'/'.$data['code'].'/install_mods', 'token=' . $this->session->data['token'], 'SSL');
        $data['remove'] = $this->url->link($data['group'].'/'.$data['code'].'/self_remove', 'token=' . $this->session->data['token'], 'SSL');
        $data['restore'] = $this->url->link($data['group'].'/'.$data['code'].'/restore_settings', 'token=' . $this->session->data['token'], 'SSL');
        $data['backup'] = $this->url->link($data['group'].'/'.$data['code'].'/backup_settings', 'token=' . $this->session->data['token'], 'SSL');
        
        //stores
        $data['stores'] = getStores();
        $data['store_id'] = STORE_ID;
        
        //checking params
        $info = $this->extension->load($data['group'].'/'.$data['code'].'/'.$data['model'])->info;
        $params = $this->config->get($info['hash'].'_extension');
        $data['debug'] = $this->config->get('addist_installer_debug');
        
        //checking permission
        $data['access'] = $this->user->hasPermission('modify', $data['group'].'/'.$data['code']) ? true : false;
        
        return $this->load_view('addist/tab.tpl',$data);
	}
}
?>