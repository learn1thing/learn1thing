<?php
class ControllerCurrencyProviderKEB extends AddistController
{
    protected $data = array(
        'currency'     => 'USD',
        'currencies'   => array(),
    );
    
    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->load_language('currency_provider/keb');
        $this->load_model('localisation/currency');
    }
    
    public function getInfo()
    {
        $info = array();
        $info['name'] = $this->language->get('text_name');
        return $info;
    }
    
	public function index()
    {
        $data = array_merge($this->data,$this->config->filter('keb'));
        
        if (empty($data['currencies']))
        {
            $content = $this->extension->fileGet('http://fx.keb.co.kr/FER1101C.web?schID=pib&mID=PER1101C&_frame=no&LOCALE_CODE=EN#');
            
            libxml_use_internal_errors(true);
            $doc = new DOMDocument();
            $doc->loadHTML($content);
            
            $xpath = new DOMXPath($doc);
            $rows = $xpath->query('//div[@id="gridPosition"]/table/tr');
            if ($rows)
            {
                foreach($rows as $row)
                {
                    $node = $xpath->query('td',$row)->item(0);
                    if ($node)
                    {
                        $parts = explode(' ',$node->nodeValue);
                        $quote = isset($parts[1]) ? $parts[1] : 1;
                        $rate = $quote / (float)$xpath->query('td',$row)->item(11)->nodeValue;
                        $data['currencies'][$parts[0]] = (float)$rate;
                    }
                }
            }
            
            $this->config->save('keb','currencies',$data['currencies']);
        }
        
		return $this->load_view('currency_provider/keb.tpl',$data);
	}
}
?>