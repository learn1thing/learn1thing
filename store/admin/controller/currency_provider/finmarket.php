<?php
class ControllerCurrencyProviderFinmarket extends AddistController
{
    protected $data = array(
        'country_id'     => '10148',
        'time_offset'    => '86400',
    );
    
    public $countries = array('10148'=>'RUB','10134'=>'UAH','10120'=>'BYR','10123'=>'KZT');
    
    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->load_language('currency_provider/finmarket');
    }
    
    public function getInfo()
    {
        $info = array();
        $info['name'] = $this->language->get('text_name');
        return $info;
    }
    
	public function index()
    {
        $data = array_merge($this->data,$this->config->filter('finmarket'));
        $data['countries'] = array();
        foreach($this->countries as $id=>$currency)
        {
            $data['countries'][] = array('id'=>$id,'currency'=>$currency,'name'=>$this->language->get('text_country_'.$id));
        }
		return $this->load_view('currency_provider/finmarket.tpl',$data);
	}
}
?>