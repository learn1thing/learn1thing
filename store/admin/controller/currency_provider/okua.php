<?php
class ControllerCurrencyProviderOKUA extends AddistController
{
    protected $data = array(
        'currency'     => 'UAH',
    );
    
    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->load_language('currency_provider/okua');
        $this->load_model('localisation/currency');
    }
    
    public function getInfo()
    {
        $info = array();
        $info['name'] = $this->language->get('text_name');
        return $info;
    }
    
	public function index()
    {
        $data = array_merge($this->data,$this->config->filter('okua'));
        $data['currencies'] = $this->model_module_currency_tools->getCurrencies();
		return $this->load_view('currency_provider/okua.tpl',$data);
	}
}
?>