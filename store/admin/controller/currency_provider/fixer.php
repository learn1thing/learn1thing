<?php
class ControllerCurrencyProviderFixer extends AddistController
{
    protected $data = array(
        'currency'     => 'USD',
    );
    
    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->load_language('currency_provider/fixer');
        $this->load_model('localisation/currency');
    }
    
    public function getInfo()
    {
        $info = array();
        $info['name'] = $this->language->get('text_name');
        return $info;
    }
    
	public function index()
    {
        $data = array_merge($this->data,$this->config->filter('fixer'));
        $data['currencies'] = array();
        
		$json = json_decode($this->extension->fileGet('http://api.fixer.io/latest?base=USD'),true);
        
        if ($json && !empty($json['rates']))
        {
            $data['currencies'] = $json['rates'];
        }
        
        $data['currencies']['USD'] = 1;
        
		return $this->load_view('currency_provider/fixer.tpl',$data);
	}
}
?>