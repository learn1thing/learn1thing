<?php
class ControllerCurrencyProviderTcmb extends AddistController
{
    protected $data = array(
        'column'         => 'ForexSelling',
    );
    
    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->load_language('currency_provider/tcmb');
    }
    
    public function getInfo()
    {
        $info = array();
        $info['name'] = $this->language->get('text_name');
        return $info;
    }
    
	public function index()
    {
        $data = array_merge($this->data,$this->config->filter('tcmb'));
		return $this->load_view('currency_provider/tcmb.tpl',$data);
	}
}
?>