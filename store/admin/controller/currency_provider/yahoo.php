<?php
class ControllerCurrencyProviderYahoo extends AddistController
{
    protected $data = array(
        'currency'     => 'USD',
    );
    
    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->load_language('currency_provider/yahoo');
        $this->load_model('localisation/currency');
    }
    
    public function getInfo()
    {
        $info = array();
        $info['name'] = $this->language->get('text_name');
        return $info;
    }
    
	public function index()
    {
        $data = array_merge($this->data,$this->config->filter('yahoo'));
        $data['currencies'] = $this->model_module_currency_tools->getCurrencies();
		return $this->load_view('currency_provider/yahoo.tpl',$data);
	}
}
?>