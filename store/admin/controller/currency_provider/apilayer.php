<?php
class ControllerCurrencyProviderApilayer extends AddistController
{
    protected $data = array(
        'currency'     => 'USD',
        'access_key'   => '',
    );
    
    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->load_language('currency_provider/apilayer');
        $this->load_model('localisation/currency');
    }
    
    public function getInfo()
    {
        $info = array();
        $info['name'] = $this->language->get('text_name');
        return $info;
    }
    
	public function index()
    {
        $data = array_merge($this->data,$this->config->filter('apilayer'));
        $data['currencies'] = array();
        $data['currencies']['USD'] = 1;
        
		$json = json_decode($this->extension->fileGet('http://www.apilayer.net/api/live?access_key=81255e904c1768b36c86fe489e16e8a7&format=1&source=USD'),true);
        if ($json && !empty($json['quotes']))
        {
            foreach($json['quotes'] as $code=>$rate)
            {
                $data['currencies'][substr($code,3,3)] = $rate;
            }
        }
        
		return $this->load_view('currency_provider/apilayer.tpl',$data);
	}
}
?>