<?php
class ControllerCurrencyProviderBNM extends AddistController
{
    protected $data = array(
        'currency'     => 'MDL',
    );
    
    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->load_language('currency_provider/bnm');
        $this->load_model('localisation/currency');
    }
    
    public function getInfo()
    {
        $info = array();
        $info['name'] = $this->language->get('text_name');
        return $info;
    }
    
	public function index()
    {
        $data = array_merge($this->data,$this->config->filter('bnm'));
        $data['currencies'] = $this->model_module_currency_tools->getCurrencies();
		return $this->load_view('currency_provider/bnm.tpl',$data);
	}
}
?>