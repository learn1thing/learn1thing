<?php

class ControllerCatalogSubscriptions extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/subscriptions');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/subscriptions');
		$this->getList();
	}

  	public function add() {
		$this->load->language('catalog/subscriptions');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/subscriptions');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_subscriptions->addSubscription($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/subscriptions', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

  	public function update() {
    	$this->language->load('catalog/subscriptions');
    	$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/subscriptions');
    	if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_subscriptions->editSubscription($this->request->get['subscription_id'], $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
			$this->response->redirect($this->url->link('catalog/subscriptions', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
    	$this->getForm();
  	}

  	public function delete() {
    	$this->language->load('catalog/subscriptions');
    	$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/subscriptions');
		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $subscription_id) {
				$this->model_catalog_subscriptions->deleteSubscription($subscription_id);
	  		}
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
			$this->response->redirect($this->url->link('catalog/subscriptions', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
    	$this->getList();
  	}

	protected function getList() {
  		$data['breadcrumbs'] = array();
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/subscriptions', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

		$data['add'] = $this->url->link('catalog/subscriptions/add', 'token=' . $this->session->data['token'], 'SSL');
		$data['delete'] = $this->url->link('catalog/subscriptions/delete', 'token=' . $this->session->data['token'], 'SSL');

		$data['subscriptions'] = array();

		$results = $this->model_catalog_subscriptions->getSubscriptions();

		foreach ($results as $result) {
			$action = array();
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/subscriptions/update', 'token=' . $this->session->data['token'] . '&subscription_id=' . $result['subscription_id'], 'SSL')
			);

			$trial_terms = '';
			$trial_status = '';

			if ($result['trial_cycle'] && $result['trial_frequency']) {
				$trial_terms = $result['trial_cycle'] . ' ' . $result['trial_frequency'] . '(s) @ ' . $this->currency->format($result['trial_price'], $this->config->get('config_currency'), $this->session->data['currency']);
				if ($result['trial_status'] == 1) {
					$trial_status = $this->language->get('text_enabled');
				} else {
					$trial_status = $this->language->get('text_disabled');
				}
			}
			
			if ($result['duration']) {
				$subscription_terms = $this->language->get('text_every') . ' ' . $result['cycle'] . ' ' . $result['frequency'] . '(s) ' . $this->language->get('text_for') . ' ' . $result['duration'] . ' payment(s)';
			} elseif (!$result['trial_only']) {
				$subscription_terms = $this->language->get('text_every') . ' ' . $result['cycle'] . ' ' . $result['frequency'] . '(s) ' . $this->language->get('text_until_cancel');
			} else {
				$subscription_terms = $this->language->get('text_trial_only');
			}
			
			$group_name = "";
			foreach (unserialize($result['allowed_customer_groups']) as $customer_group) {
				$customer_group_name = $this->model_catalog_subscriptions->getCustomerGroupName($customer_group);
				if ($group_name) {
					$group_name .= "\n" . $customer_group_name;
				} else {
					$group_name = $customer_group_name;
				}
			}
			
      		$data['subscriptions'][] = array(
				'subscription_id'	=> $result['subscription_id'],
				'trial'				=> $result['trial'],
				'trial_status'		=> $trial_status,
				'trial_terms'		=> $trial_terms,
				'subscription'		=> $subscription_terms,
				'specific_day'		=> (isset($result['specific_day']) ? (!empty($result['specific_day']) ? $this->language->get('text_' . $result['specific_day']) : '') : ''),
				'discount'			=> $result['discount'],
				'discount_type'		=> $result['discount_type'],
				'customer_groups'	=> nl2br($group_name),
				'status'			=> $result['status'],
				'selected'			=> isset($this->request->post['selected']) && in_array($result['subscription_id'], $this->request->post['selected']),
				'action'			=> $action
			);
    	}

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_list'] = $this->language->get('text_list');

		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_subscription_id'] = $this->language->get('column_subscription_id');
		$data['column_trial_status'] = $this->language->get('column_trial_status');
		$data['column_trial_terms'] = $this->language->get('column_trial_terms');
		$data['column_subscription'] = $this->language->get('column_subscription');
		$data['column_specific_day'] = $this->language->get('column_specific_day');
		$data['column_discount'] = $this->language->get('column_discount');
		$data['column_customer_groups'] = $this->language->get('column_customer_groups');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_delete'] = $this->language->get('button_delete');

 		$data['token'] = $this->session->data['token'];

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/subscriptions_list.tpl', $data));
	}

  	protected function getForm() {
    	$data['heading_title'] = $this->language->get('heading_title');
		$data['text_form'] = $this->language->get('text_form');
    	$data['text_enabled'] = $this->language->get('text_enabled');
    	$data['text_disabled'] = $this->language->get('text_disabled');
    	$data['text_none'] = $this->language->get('text_none');
    	$data['text_yes'] = $this->language->get('text_yes');
    	$data['text_no'] = $this->language->get('text_no');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');
		$data['text_trial_help'] = $this->language->get('text_trial_help');
		$data['text_recurring_help'] = $this->language->get('text_recurring_help');
		$data['text_taxes'] = $this->language->get('text_taxes');
		$data['text_shipping'] = $this->language->get('text_shipping');
		$data['text_sd_only_available'] = $this->language->get('text_sd_only_available');
		$data['entry_trial'] = $this->language->get('entry_trial');
		$data['entry_trial_only'] = $this->language->get('entry_trial_only');
		$data['entry_trial_frequency'] = $this->language->get('entry_trial_frequency');
		$data['entry_trial_cycle'] = $this->language->get('entry_trial_cycle');
		$data['entry_trial_duration'] = $this->language->get('entry_trial_duration');
		$data['entry_trial_price'] = $this->language->get('entry_trial_price');
    	$data['entry_trial_status'] = $this->language->get('entry_trial_status');
		$data['entry_frequency'] = $this->language->get('entry_frequency');
		$data['entry_cycle'] = $this->language->get('entry_cycle');
		$data['entry_duration'] = $this->language->get('entry_duration');
		$data['entry_specific_day'] = $this->language->get('entry_specific_day');
		$data['entry_discount'] = $this->language->get('entry_discount');
		$data['entry_discount_type'] = $this->language->get('entry_discount_type');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_reinstatement_fee'] = $this->language->get('entry_reinstatement_fee');
		$data['entry_reinstatement_period'] = $this->language->get('entry_reinstatement_period');
		$data['entry_reminder_count'] = $this->language->get('entry_reminder_count');
    	$data['entry_reminder_period'] = $this->language->get('entry_reminder_period');
    	$data['entry_grace_period'] = $this->language->get('entry_grace_period');
    	$data['entry_customer_groups'] = $this->language->get('entry_customer_groups');
		$data['entry_include_totals'] = $this->language->get('entry_include_totals');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
    	$data['button_save'] = $this->language->get('button_save');
    	$data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

  		$data['breadcrumbs'] = array();
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
   		);
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/subscriptions', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

		if (!isset($this->request->get['subscription_id'])) {
			$data['action'] = $this->url->link('catalog/subscriptions/add', 'token=' . $this->session->data['token'], 'SSL');
		} else {
			$data['action'] = $this->url->link('catalog/subscriptions/update', 'token=' . $this->session->data['token'] . '&subscription_id=' . $this->request->get['subscription_id'], 'SSL');
		}

		$data['cancel'] = $this->url->link('catalog/subscriptions', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->get['subscription_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      		$subscription_info = $this->model_catalog_subscriptions->getSubscription($this->request->get['subscription_id']);
    	}

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->post['trial'])) {
      		$data['trial'] = $this->request->post['trial'];
    	} elseif (!empty($subscription_info)) {
			$data['trial'] = $subscription_info['trial'];
		} else {
      		$data['trial'] = 0;
    	}

		if (isset($this->request->post['trial_only'])) {
      		$data['trial_only'] = $this->request->post['trial_only'];
    	} elseif (!empty($subscription_info)) {
			$data['trial_only'] = $subscription_info['trial_only'];
		} else {
      		$data['trial_only'] = 0;
    	}

		if (isset($this->request->post['trial_frequency'])) {
      		$data['trial_frequency'] = $this->request->post['trial_frequency'];
    	} elseif (!empty($subscription_info)) {
			$data['trial_frequency'] = $subscription_info['trial_frequency'];
		} else {
      		$data['trial_frequency'] = '';
    	}

		if (isset($this->request->post['trial_duration'])) {
      		$data['trial_duration'] = ($this->request->post['trial_duration']) ? $this->request->post['trial_duration'] : 1;
    	} elseif (!empty($subscription_info)) {
			$data['trial_duration'] = ($subscription_info['trial_duration']) ? $subscription_info['trial_duration'] : 1;
		} else {
      		$data['trial_duration'] = 1;
    	}

		if (isset($this->request->post['trial_cycle'])) {
      		$data['trial_cycle'] = $this->request->post['trial_cycle'];
    	} elseif (!empty($subscription_info)) {
			$data['trial_cycle'] = $subscription_info['trial_cycle'];
		} else {
      		$data['trial_cycle'] = '';
    	}

		if (isset($this->request->post['trial_price'])) {
      		$data['trial_price'] = $this->request->post['trial_price'];
    	} elseif (!empty($subscription_info)) {
			$data['trial_price'] = $subscription_info['trial_price'];
		} else {
      		$data['trial_price'] = '';
    	}

		if (isset($this->request->post['trial_status'])) {
      		$data['trial_status'] = $this->request->post['trial_status'];
    	} elseif (!empty($subscription_info)) {
			$data['trial_status'] = $subscription_info['trial_status'];
		} else {
      		$data['trial_status'] = 0;
    	}

		if (isset($this->request->post['frequency'])) {
      		$data['frequency'] = $this->request->post['frequency'];
    	} elseif (!empty($subscription_info)) {
			$data['frequency'] = $subscription_info['frequency'];
		} else {
      		$data['frequency'] = '';
    	}

		if (isset($this->request->post['duration'])) {
			$data['duration'] = $this->request->post['duration'];
		} elseif (!empty($subscription_info)) {
			$data['duration'] = $subscription_info['duration'];
		} else {
			$data['duration'] = '';
		}

		if (isset($this->request->post['cycle'])) {
			$data['cycle'] = $this->request->post['cycle'];
		} elseif (!empty($subscription_info)) {
			$data['cycle'] = $subscription_info['cycle'];
		} else {
			$data['cycle'] = '';
		}

		if (isset($this->request->post['specific_day'])) {
			$data['specific_day'] = $this->request->post['specific_day'];
		} elseif (!empty($subscription_info)) {
			$data['specific_day'] = $subscription_info['specific_day'];
		} else {
			$data['specific_day'] = '';
		}

		$data['days'][] = array(
			'value'	=> 'monday',
			'name'	=> $this->language->get('text_monday')
		);
		$data['days'][] = array(
			'value'	=> 'tuesday',
			'name'	=> $this->language->get('text_tuesday')
		);
		$data['days'][] = array(
			'value'	=> 'wednesday',
			'name'	=> $this->language->get('text_wednesday')
		);
		$data['days'][] = array(
			'value'	=> 'thursday',
			'name'	=> $this->language->get('text_thursday')
		);
		$data['days'][] = array(
			'value'	=> 'friday',
			'name'	=> $this->language->get('text_friday')
		);
		$data['days'][] = array(
			'value'	=> 'saturday',
			'name'	=> $this->language->get('text_saturday')
		);
		$data['days'][] = array(
			'value'	=> 'sunday',
			'name'	=> $this->language->get('text_sunday')
		);

		if (isset($this->request->post['discount'])) {
			$data['discount'] = $this->request->post['discount'];
		} elseif (!empty($subscription_info)) {
			$data['discount'] = $subscription_info['discount'];
		} else {
			$data['discount'] = '';
		}

		if (isset($this->request->post['discount_type'])) {
			$data['discount_type'] = $this->request->post['discount_type'];
		} elseif (!empty($subscription_info)) {
			$data['discount_type'] = $subscription_info['discount_type'];
		} else {
			$data['discount_type'] = 'Percent';
		}

		if (isset($this->request->post['include_taxes'])) {
			$data['include_taxes'] = $this->request->post['include_taxes'];
		} elseif (!empty($subscription_info)) {
			$data['include_taxes'] = $subscription_info['include_taxes'];
		} else {
			$data['include_taxes'] = 0;
		}

		if (isset($this->request->post['include_shipping'])) {
			$data['include_shipping'] = $this->request->post['include_shipping'];
		} elseif (!empty($subscription_info)) {
			$data['include_shipping'] = $subscription_info['include_shipping'];
		} else {
			$data['include_shipping'] = 0;
		}

    	if (isset($this->request->post['status'])) {
      		$data['status'] = $this->request->post['status'];
    	} elseif (!empty($subscription_info)) {
      		$data['status'] = $subscription_info['status'];
    	} else {
			$data['status'] = 1;
		}

    	if (isset($this->request->post['reinstatement_fee'])) {
      		$data['reinstatement_fee'] = $this->request->post['reinstatement_fee'];
    	} elseif (!empty($subscription_info)) {
			$data['reinstatement_fee'] = $subscription_info['reinstatement_fee'];
		} else {
      		$data['reinstatement_fee'] = '';
    	}

    	if (isset($this->request->post['reinstatement_period'])) {
      		$data['reinstatement_period'] = $this->request->post['reinstatement_period'];
    	} elseif (!empty($subscription_info)) {
      		$data['reinstatement_period'] = $subscription_info['reinstatement_period'];
    	} else {
			$data['reinstatement_period'] = '';
		}

		if (isset($this->request->post['reminder_count'])) {
      		$data['reminder_count'] = $this->request->post['reminder_count'];
    	} elseif (!empty($subscription_info)) {
      		$data['reminder_count'] = $subscription_info['reminder_count'];
    	} else {
			$data['reminder_count'] = '';
		}

		if (isset($this->request->post['reminder_period'])) {
      		$data['reminder_period'] = $this->request->post['reminder_period'];
    	} elseif (!empty($subscription_info)) {
      		$data['reminder_period'] = $subscription_info['reminder_period'];
    	} else {
			$data['reminder_period'] = '';
		}

		if (isset($this->request->post['grace_period'])) {
      		$data['grace_period'] = $this->request->post['grace_period'];
    	} elseif (!empty($subscription_info)) {
      		$data['grace_period'] = $subscription_info['grace_period'];
    	} else {
			$data['grace_period'] = '';
		}

		if (isset($this->request->post['sort_order'])) {
			$data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($subscription_info)) {
			$data['sort_order'] = $subscription_info['sort_order'];
		} else {
			$data['sort_order'] = '';
		}

		$data['frequencies'] = $this->model_catalog_subscriptions->getFrequencies();

		$this->load->model('customer/customer_group');
		$data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups();

		if (isset($this->request->post['allowed_customer_groups'])) {
			$data['allowed_customer_groups'] = $this->request->post['allowed_customer_groups'];
		} elseif (!empty($subscription_info)) {
			$data['allowed_customer_groups'] = $subscription_info['allowed_customer_groups'];
		} else {
			$data['allowed_customer_groups'] = array();
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/subscriptions_form.tpl', $data));
  	}

  	
	protected function validateForm() {
    	if (!$this->user->hasPermission('modify', 'catalog/subscriptions')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

    	if (!$this->error) {
			return true;
    	} else {
      		return false;
    	}
  	}

  	protected function validateDelete() {
    	if (!$this->user->hasPermission('modify', 'catalog/subscriptions')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	}
		
		$this->load->model('catalog/subscriptions');

		if (!$this->error) {
	  		return true;
		} else {
	  		return false;
		}
  	}

}

?>