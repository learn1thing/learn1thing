<?php

class ControllerModuleProductSubscribe extends Controller {

	public function index() {
		$this->language->load('module/product_subscribe');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/setting');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('product_subscribe', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], true));
		}
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['entry_add_shipping'] = $this->language->get('entry_add_shipping');
		$data['entry_add_taxes'] = $this->language->get('entry_add_taxes');		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

  		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/product_subscribe', 'token=' . $this->session->data['token'], true)
		);

		$data['action'] = $this->url->link('module/product_subscribe', 'token=' . $this->session->data['token'], true);
		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], true);	
		
		if (isset($this->request->post['product_subscribe_add_shipping'])) {
			$data['product_subscribe_add_shipping'] = $this->request->post['product_subscribe_add_shipping'];
		} else {
			$data['product_subscribe_add_shipping'] = $this->config->get('product_subscribe_add_shipping');
		}
		
		if (isset($this->request->post['product_subscribe_add_taxes'])) {
			$data['product_subscribe_add_taxes'] = $this->request->post['product_subscribe_add_taxes'];
		} else {
			$data['product_subscribe_add_taxes'] = $this->config->get('product_subscribe_add_taxes');
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/product_subscribe', $data));
	}

	public function install() {
		$this->load->model('catalog/subscriptions');
		$this->model_catalog_subscriptions->install();
		return;
	}

	public function uninstall() {
		$this->load->model('catalog/subscriptions');
		$this->model_catalog_subscriptions->uninstall();
		return;
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/product_subscribe')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}

}

?>