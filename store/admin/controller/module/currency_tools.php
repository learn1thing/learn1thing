<?php
#encode-me[]#
if (!class_exists('AddistObject')) require_once(DIR_SYSTEM.'addist/startup.php');
class ControllerModuleCurrencyTools extends AddistController
{
    protected $path = 'module/currency_tools/currency_tools_pro';
    
    public function __construct($registry)
    {
        parent::__construct($registry);
        
        $this->load_model('module/currency_tools');
        $this->load_model('catalog/category');
        $this->load_model('catalog/manufacturer');
        $this->load_model('localisation/language');
        
        $this->load_language('module/currency_tools');
    }
    
    public function index()
    {
        if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate())
        {
            if (isset($this->request->post[$this->config->get('currency_tools_provider')]))
            {
                foreach ($this->request->post[$this->config->get('currency_tools_provider')] as $key=>$value)
                {
                    $this->config->save($this->config->get('currency_tools_provider'),$key,$value);
                }
            }
            
            //insert price fields
            if (!class_exists('CurrencyToolsHelper'))
            {
                require_once(DIR_SYSTEM.'library/currency_tools_helper.php');
            }
            $helper = new CurrencyToolsHelper($this->registry);
            $helper->addPriceFields();
            
            addMessage('success',$this->language->get('text_success'));
    		$this->redirect($this->url->link('module/currency_tools', 'token=' . $this->session->data['token'], 'SSL'));
        }
        
        if ($this->config->get('currency_tools_status'))
        {
            //check auto-detection security
            if ($this->config->get('currency_tools_currency_store') == 'AUTO' && (int)$this->config->get('currency_tools_session_lifetime') < 86400)
            {
                addMessage('error',$this->language->get('error_session_lifetime'));
            }
            
            //check cron execution
            if ($this->config->get('currency_tools_executed') == '')
            {
                addMessage('error',$this->language->get('error_cron'));
            }
            elseif ((int)$this->config->get('currency_tools_executed') < (time() - 3600))
            {
                addMessage('error',$this->language->get('error_cron_time'));
            }
            
            //check currency codes
            $error_codes = array();
            $currency_codes = json_decode(file_get_contents(DIR_ADDIST.'extension/currency_tools_pro/currency.json'),true);
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "currency");
            foreach($query->rows as $row)
            {
                if (!in_array($row['code'],$currency_codes))
                {
                    $error_codes[] = $row['title'].'('.$row['code'].')';
                }
            }
            if ($error_codes)
            {
                addMessage('error',sprintf($this->language->get('error_currency_code'),implode(', ',$error_codes)));
            }
        }
        
		$this->document->setTitle($this->language->get('heading_title'));
        $this->data['heading_title'] = $this->language->get('heading_title');
        
        //variables
        $this->data['currencies'] = $this->model_module_currency_tools->getCurrencies();
        $this->data['payment_methods'] = $this->model_module_currency_tools->getExtensions('payment');
        $this->data['shipping_methods'] = $this->model_module_currency_tools->getExtensions('shipping');
        $this->data['countries'] = $this->model_module_currency_tools->getCountries();
        $this->data['languages'] = $this->getLanguages();
        
        $this->data['day_hours'] = array();
        for($h=0;$h<24;$h++)
        {
            $this->data['day_hours'][] = sprintf("%02d", $h);
        }
        
        $this->data['providers'] = array();
        $providers = glob(DIR_SYSTEM.'currency_provider/*.php');
        foreach($providers as $provider)
        {
            require_once($provider);
            $code = substr(basename($provider),-strlen(basename($provider)),-4);
            if (class_exists('cp_'.$code))
            {
                $this->data['providers'][$code] = $this->load_controller('currency_provider/'.$code.'/getinfo');
            }
        }
        
        //config currency
        $this->data['currency_admin'] = $this->config->get('config_currency');
        
        //provider_form form
        if ($this->config->get('currency_tools_provider'))
        {
            $this->data['provider_form'] = $this->load_controller('currency_provider/'.$this->config->get('currency_tools_provider'));
        }
        else
        {
            $this->data['provider_form'] = '';
        }
        
        $this->data['cron'] = 'php -q '.realpath(DIR_APPLICATION.'ct_cron.php');
        $this->data['executed'] = $this->settings['executed'] ? date('Y-m-d H:i:s',$this->settings['executed']) : '';
        $this->data['progress'] = $this->model_module_currency_tools->getCronProgress();
        
        $this->data['refresh'] = $this->url->link('module/currency_tools/refresh', 'token=' . $this->session->data['token'], 'SSL');
        
		$this->template = 'module/currency_tools.tpl';
        
        $this->setOutput();
    }
    
    public function edit()
    {
        if ($this->request->server['REQUEST_METHOD'] == 'POST' && !empty($this->request->get['currency_id']) && $this->validate())
        {
            if ($this->model_module_currency_tools->saveCurrency($this->request->get['currency_id'],$this->request->post))
            {
                addMessage('success',$this->language->get('text_saved'));
        		$this->redirect($this->url->link('module/currency_tools', 'token=' . $this->session->data['token'], 'SSL'));
            }
        }
        
        //title
		$this->document->setTitle($this->language->get('heading_edit'));
        $this->data['heading_title'] = $this->language->get('heading_edit');
        
        //variables
        $this->data['currencies'] = $this->model_module_currency_tools->getCurrencies();
        
        //data
        $this->data['currency_admin'] = $this->config->get('config_currency');
        $this->data['reversed'] = $this->config->get('currency_tools_reversed');
        
        $this->load_model('localisation/currency');
        $currency_info = $this->model_localisation_currency->getCurrency($this->request->get['currency_id']);
        
        $this->data['currency_id'] = $currency_info['currency_id'];
        $this->data['title'] = $currency_info['title'];
        $this->data['code'] = $currency_info['code'];
        $this->data['value'] = (float)$currency_info['value'];
        $this->data['reversed_value'] = (float)(1/$currency_info['value']);
        
		if (isset($this->request->post['correction'])) {
			$this->data['correction'] = $this->request->post['correction'];
		} else {
			$this->data['correction'] = $currency_info['correction'];
		}
        
		if (isset($this->request->post['margin'])) {
			$this->data['margin'] = $this->request->post['margin'];
		} else {
			$this->data['margin'] = $currency_info['margin'];
		}
        
		if (isset($this->request->post['overheads'])) {
			$this->data['overheads'] = $this->request->post['overheads'];
		} else {
			$this->data['overheads'] = $currency_info['overheads'];
		}
        
		if (isset($this->request->post['deviation'])) {
			$this->data['deviation'] = $this->request->post['deviation'];
		} else {
			$this->data['deviation'] = $currency_info['deviation'];
		}
        
		if (isset($this->request->post['round_value'])) {
			$this->data['round_value'] = $this->request->post['round_value'];
		} else {
			$this->data['round_value'] = $currency_info['round_value'];
		}
        
		if (isset($this->request->post['decimal_place'])) {
			$this->data['decimal_place'] = $this->request->post['decimal_place'];
		} else {
			$this->data['decimal_place'] = $currency_info['decimal_place'];
		}
        
		if (isset($this->request->post['autorefresh'])) {
			$this->data['autorefresh'] = $this->request->post['autorefresh'];
		} else {
			$this->data['autorefresh'] = $currency_info['autorefresh'];
		}
        
        //preview
        $params = array();
        $params['amount'] = 100;
        $params['currency_input'] = $currency_info['code'];
        $params['currency_output'] = $currency_info['code'];
        $params['to_value'] = $currency_info['value'];
        $params['from_value'] = $currency_info['value'];
        $params['round_value'] = $currency_info['round_value'];
        $params['margin'] = $currency_info['margin'];
        $params['decimal_place'] = $currency_info['decimal_place'];
        $params['overheads'] = $currency_info['overheads'];
        $params['deviation'] = $currency_info['deviation'];
        $params['symbol_left'] = $currency_info['symbol_left'];
        $params['symbol_right'] = $currency_info['symbol_right'];
        $this->data['preview'] = $this->model_module_currency_tools->format($params);
        
        //actions
        $this->data['action'] = $this->url->link('module/currency_tools/edit', 'currency_id='.$currency_info['currency_id'].'&token=' . $this->session->data['token'], 'SSL');
        $this->data['cancel'] = $this->url->link('module/currency_tools', 'token=' . $this->session->data['token'], 'SSL');
        
		$this->template = 'module/currency_tools_form.tpl';
        
		$this->setOutput();
    }
    
    public function refresh()
    {
        //load helper
        require_once(DIR_SYSTEM.'library/currency_tools_helper.php');
        $helper = new CurrencyToolsHelper($this->registry);
        $helper->refresh();
        
        $this->redirect($this->url->link('module/currency_tools', 'token=' . $this->session->data['token'], 'SSL'));
    }
    
    public function provider()
    {
        $this->response->setOutput($this->load_controller('currency_provider/'.$this->request->get['code']));
    }
    
    public function preview()
    {
        $this->load_model('localisation/currency');
        
        $currency_from = $this->model_localisation_currency->getCurrency($this->request->post['currency_id']);
        $currency_to = $this->model_localisation_currency->getCurrency($this->request->get['currency_id']);
        
        $params = array();
        $params['amount'] = $this->request->post['amount'];
        $params['currency_input'] = $currency_from['code'];
        $params['currency_output'] = $currency_to['code'];
        $params['to_value'] = $this->request->post['value'];
        $params['from_value'] = $currency_from['value'];
        $params['deviation'] = $this->request->post['deviation'];
        $params['round_value'] = $this->request->post['round_value'];
        $params['decimal_place'] = $this->request->post['decimal_place'];
        $params['margin'] = $this->request->post['margin'];
        $params['overheads'] = $this->request->post['overheads'];
        $params['symbol_left'] = $currency_to['symbol_left'];
        $params['symbol_right'] = $currency_to['symbol_right'];
        
        $preview = $this->model_module_currency_tools->format($params);
        $this->response->setOutput($preview);
    }
    
    public function converter()
    {
        if ($this->request->server['REQUEST_METHOD'] == 'POST')
        {
            if (!parent::validate())
            {
                addMessage('error',$this->language->get('error_permission'));
            }
            elseif (!empty($this->request->post['convert']))
            {
                $criteria = $this->request->post['convert']['criteria'];
                $data = array();
                if ($criteria == 2 && !empty($this->request->post['convert']['brand']))
                {
                    foreach($this->request->post['convert']['brand'] as $brand)
                    {
                        if (!empty($brand['manufacturer_id']))
                        {
                            $data[$brand['manufacturer_id']] = $brand['currency'] ? $brand['currency'] : $this->request->post['convert']['currency'];
                        }
                    }
                }
                elseif ($criteria == 3 && !empty($this->request->post['convert']['category']))
                {
                    foreach($this->request->post['convert']['category'] as $category)
                    {
                        if (!empty($category['category_id']))
                        {
                            $data[$category['category_id']] = $category['currency'] ? $category['currency'] : $this->request->post['convert']['currency'];
                        }
                    }
                }
                if (!empty($this->request->post['convert']['skip']))
                {
                    $skip = $this->request->post['convert']['skip'];
                }
                else
                {
                    $skip = array();
                }
                
                if ($criteria == 1 || $data)
                {
                    if ($this->model_module_currency_tools->setProductCurrencies($this->request->post['convert']['currency'],$skip,$criteria,$data))
                    {
                        addMessage('success',$this->language->get('text_converted'));
                    }
                    else
                    {
                        addMessage('error',$this->language->get('error_convert'));
                    }
                }
                else
                {
                    addMessage('error',$this->language->get('error_no_product'));
                }
            }
            
            $this->redirect($this->url->link('module/currency_tools', 'token=' . $this->session->data['token'], 'SSL'));
        }
        else
        {
            //data
            $this->data['heading'] = $this->language->get('heading_converter');
    		$this->data['categories'] = $this->model_catalog_category->getCategories(0);
            $this->data['brands'] = $this->model_catalog_manufacturer->getManufacturers();
            $this->data['currencies'] = $this->model_module_currency_tools->getCurrencies();
            
            $this->data['action'] = $this->url->link('module/currency_tools/converter', 'token=' . $this->session->data['token'], 'SSL');
            
            exit($this->load_view('module/currency_tools_convert.tpl',$this->data));
        }
    }
    
    public function exporter()
    {
        if ($this->request->server['REQUEST_METHOD'] == 'POST')
        {
            if (!parent::validate())
            {
                addMessage('error',$this->language->get('error_permission'));
            }
            elseif (!is_dir(DIR_SYSTEM.'addist/temp') || !is_writable(DIR_SYSTEM.'addist/temp'))
            {
                addMessage('error',sprintf($this->language->get('error_temp_error'),DIR_SYSTEM.'addist/temp'));
            }
            elseif (!empty($this->request->post['export']))
            {
                if ($this->request->post['export']['criteria'] == 1 || ($this->request->post['export']['criteria'] == 2 && !empty($this->request->post['export']['brand'])) || ($this->request->post['export']['criteria'] == 3 && !empty($this->request->post['export']['category'])))
                {
                    $file = $this->model_module_currency_tools->export($this->request->post['export']);
                    if ($file)
                    {
                        header("Content-type: application/vnd.ms-excel");
                        header("Content-Disposition: attachment; filename=".basename($file));
                        header("Pragma: no-cache");
                        header ("Expires: 0");
                        exit(file_get_contents($file));
                    }
                    else
                    {
                        addMessage('error',$this->language->get('error_export'));
                    }
                }
                else
                {
                    addMessage('error',$this->language->get('error_no_product'));
                }
            }
            
            $this->redirect($this->url->link('module/currency_tools', 'token=' . $this->session->data['token'], 'SSL'));
        }
        else
        {
            //check PHPExcel
            if (!is_file(DIR_SYSTEM.'library/PHPExcel.php') || !is_dir(DIR_SYSTEM.'library/PHPExcel'))
            {
                $this->data['error'][] = $this->language->get('error_phpexcel');
            }
            
            //data
            $this->data['heading'] = $this->language->get('heading_exporter');
    		$this->data['categories'] = $this->model_catalog_category->getCategories(0);
            $this->data['brands'] = $this->model_catalog_manufacturer->getManufacturers();
            
            $this->data['action'] = $this->url->link('module/currency_tools/exporter', 'token=' . $this->session->data['token'], 'SSL');
            
            exit($this->load_view('module/currency_tools_export.tpl',$this->data));
        }
    }
    
    public function importer()
    {
        if ($this->request->server['REQUEST_METHOD'] == 'POST')
        {
            if (!parent::validate())
            {
                addMessage('error',$this->language->get('error_permission'));
            }
            elseif (!empty($this->request->files['import']))
            {
                if ($this->model_module_currency_tools->import($this->request->files['import']))
                {
                    addMessage('success',$this->language->get('text_success_import'));
                }
            }
            
            $this->redirect($this->url->link('module/currency_tools', 'token=' . $this->session->data['token'], 'SSL'));
        }
        else
        {
            //check PHPExcel
            if (!is_file(DIR_SYSTEM.'library/PHPExcel.php') || !is_dir(DIR_SYSTEM.'library/PHPExcel'))
            {
                $this->data['error'][] = $this->language->get('error_phpexcel');
            }
            
            //data
            $this->data['heading'] = $this->language->get('heading_importer');
            
            $this->data['action'] = $this->url->link('module/currency_tools/importer', 'token=' . $this->session->data['token'], 'SSL');
            
            exit($this->load_view('module/currency_tools_import.tpl',$this->data));
        }
    }
}
?>