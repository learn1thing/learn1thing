<?php
#encode-me[]#
if (!class_exists('AddistObject')) require_once(DIR_SYSTEM.'addist/startup.php');
class ModelModuleCurrencyTools extends AddistModel
{
    protected $path = 'module/currency_tools/currency_tools_pro';
    
	public function getCurrencies()
    {
        $result = array();
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "currency ORDER BY `code` ASC");
        foreach($query->rows as $row)
        {
            $result[$row['code']] = $row;
            $result[$row['code']]['href'] = $this->url->link('module/currency_tools/edit', 'currency_id='.$row['currency_id'].'&token=' . $this->session->data['token'], 'SSL');
        }
		return $result;
	}
    
	public function saveCurrency($currency_id,$data)
    {
        if ($data)
        {
            $currencies = $this->getCurrencies();
            $this->db->query("UPDATE " . DB_PREFIX . "currency SET `value` = '" . (float)$data['value'] . "', `round_value` = '" . (int)$data['round_value'] . "', `decimal_place` = '" . (int)$data['decimal_place'] . "', `correction` = '" . (float)$data['correction'] . "', `margin` = '" . (float)$data['margin'] . "', `overheads` = '" . (float)$data['overheads'] . "', `deviation` = '" . (float)$data['deviation'] . "', `autorefresh` = '" . (int)$data['autorefresh'] . "' WHERE currency_id = '" . (int)$currency_id . "'");
            
            require_once(DIR_SYSTEM.'library/currency_tools_helper.php');
            $helper = new CurrencyToolsHelper($this->registry);
            $helper->setRefresh();
            
            return true;
        }
        else
        {
            return false;
        }
	}
    
    public function setProductCurrency($currency,$where)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p WHERE p.`currency` != '$currency' $where");
        if ($query->num_rows)
        {
            foreach($query->rows as $row)
            {
                $row['currency'] = empty($row['currency']) ? $this->currency->getProductCurrency($row['product_id']) : $row['currency'];
                
                $this->db->query("UPDATE " . DB_PREFIX . "product SET `price` = ".$this->currency->convert($row['price'], $row['currency'], $currency).", `currency` = '$currency', `refresh` = '1' WHERE `product_id` = '".$row['product_id']."'");
                
                //special
                $sub_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_special WHERE `product_id` = '".$row['product_id']."'");
                if ($sub_query->num_rows)
                {
                    foreach($sub_query->rows as $sub_row)
                    {
                        $this->db->query("UPDATE " . DB_PREFIX . "product_special SET `price` = ".$this->currency->convert($sub_row['price'], $row['currency'], $currency)." WHERE `product_id` = '".$row['product_id']."' AND `product_special_id` = '".$sub_row['product_special_id']."'");
                    }
                }
                
                //discount
                $sub_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_discount WHERE `product_id` = '".$row['product_id']."'");
                if ($sub_query->num_rows)
                {
                    foreach($sub_query->rows as $sub_row)
                    {
                        $this->db->query("UPDATE " . DB_PREFIX . "product_discount SET `price` = ".$this->currency->convert($sub_row['price'], $row['currency'], $currency)." WHERE `product_id` = '".$row['product_id']."' AND `product_discount_id` = '".$sub_row['product_discount_id']."'");
                    }
                }
                
                //option_value
                $sub_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value WHERE `product_id` = '".$row['product_id']."'");
                if ($sub_query->num_rows)
                {
                    foreach($sub_query->rows as $sub_row)
                    {
                        $this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET `price` = ".$this->currency->convert($sub_row['price'], $row['currency'], $currency)." WHERE `product_id` = '".$row['product_id']."' AND `product_option_value_id` = '".$sub_row['product_option_value_id']."'");
                    }
                }
            }
        }
    }
    
	public function setProductCurrencies($currency,$skip,$criteria,$data)
    {
        if ($criteria==1)
        {
            $where = $skip ? " AND p.currency NOT IN (".implode(',',$skip).")" : '';
            $this->setProductCurrency($currency,$where);
        }
        elseif ($criteria==2 && $data)
        {
            foreach($data as $brand_id=>$brand_currency)
            {
                $this->db->query("UPDATE " . DB_PREFIX . "manufacturer SET `currency` = '".$brand_currency."' WHERE manufacturer_id = '".$brand_id."'");
                $where = " AND p.manufacturer_id = '$brand_id'".($skip ? " AND p.currency NOT IN (".implode(',',$skip).")" : '');
                $this->setProductCurrency($brand_currency,$where);
            }
            $where = ($data ? " AND p.manufacturer_id IN (".implode(',',array_keys($data)).")" : '').($skip ? " AND p.currency NOT IN (".implode(',',$skip).")" : '');
        }
        elseif ($criteria==3 && $data)
        {
            foreach($data as $category_id=>$category_currency)
            {
                $where = " AND (SELECT COUNT(*) FROM `".DB_PREFIX."product_to_category` p2c WHERE p2c.product_id = p.product_id AND p2c.category_id IN = '$category_id') > 0".($skip ? " AND p.currency NOT IN (".implode(',',$skip).")" : '');
                $this->setProductCurrency($category_currency,$where);
            }
        }
        
        return true;
	}
    
    public function export($data)
    {
        $sql = "SELECT p.product_id, p.manufacturer_id, p.model, p.price, p.margin, p.overheads, p.currency, p.overheads_currency FROM ".DB_PREFIX."product p LEFT JOIN ".DB_PREFIX."manufacturer m ON m.manufacturer_id = p.manufacturer_id WHERE 1";
        
        if ($data['criteria'] == 2 && $data)
        {
            $sql .= " AND p.manufacturer_id IN (".implode(',',array_values($data['brand'])).")";
        }
        elseif ($data['criteria'] == 3 && $data)
        {
            $sql .= " AND (SELECT COUNT(*) FROM ".DB_PREFIX."product_to_category p2c WHERE p2c.product_id = p.product_id AND p2c.category_id IN (".implode(',',array_values($data['category'])).")) > 0";
        }
        
        $query = $this->db->query($sql);
        
        if ($query->num_rows)
        {
            require_once(DIR_SYSTEM.'library/PHPExcel.php');
            
            $filename = DIR_ADDIST.'temp/ctp_export_products.xls';
            
            $xls = new PHPExcel();
            $xls->setActiveSheetIndex(0);
            
            $sheet = $xls->getActiveSheet();
            $sheet->setTitle($this->language->get('text_export'));
            
            //headers
            foreach(array_keys($query->row) as $i=>$column)
            {
                $sheet->setCellValueByColumnAndRow(1+$i,2,strtoupper($column));
                
                $colname = PHPExcel_Cell::stringFromColumnIndex(1+$i);
                $sheet->getColumnDimension($colname)->setAutoSize(true);
                
                $sheet->getStyle($colname)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $sheet->getStyle($colname)->getFill()->getStartColor()->setRGB('EEEEEE');
            }
            
            //values
            foreach($query->rows as $line=>$row)
            {
                foreach(array_values($row) as $column=>$value)
                {
                    $sheet->setCellValueByColumnAndRow(1+$column,3+$line,$value);
                }
            }
            
            $writer = new PHPExcel_Writer_Excel5($xls);
            $writer->save($filename);
            
            return $filename;
        }
        else
        {
            return false;
        }
    }
    
    public function import($file)
    {
        $columns = array();
        
        require_once(DIR_SYSTEM.'library/PHPExcel.php');
        
        $filename = DIR_ADDIST.'temp/ctp_import_products.xls';
        
        if (is_file($filename))
        {
            unlink($filename);
        }
        
        if (is_file($filename))
        {
            return false;
        }
        
        $parts = explode('.',$file['name']['file']);
        if (end($parts) == 'xls')
        {
            move_uploaded_file($file['tmp_name']['file'], $filename);
        }
        else
        {
            addMessage('error',$this->language->get('error_file_type'));
            return false;
        }
        
        $xls = PHPExcel_IOFactory::load(realpath($filename));
        
        $xls->setActiveSheetIndex(0);
        
        $sheet = $xls->getActiveSheet();
        
        $rowIterator = $sheet->getRowIterator();
        foreach ($rowIterator as $i=>$row)
        {
            if ($i > 1)
            {
                $item = array();
                
                $cellIterator = $row->getCellIterator();
                
                foreach ($cellIterator as $col_index=>$cell)
                {
                    if ($i == 2)
                    {
                        $columns[] = strtolower($cell->getCalculatedValue());
                    }
                    else
                    {
                        $item[] = "`".$columns[$col_index-1]."` = '".$cell->getCalculatedValue()."'";
                    }
                }
                
                if ($i > 2)
                {
                    $this->db->query("UPDATE ".DB_PREFIX."product SET ".implode(', ',$item).", `refresh` = '1' WHERE ".$item[0]);
                }
            }
        }
        
        return true;
    }
    
	public function getExtensions($type = 'payment')
    {
        $result = array();
        
        //language
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "language` WHERE language_id = '".$this->config->get('config_language_id')."'");
        $language = new Language($query->row['directory']);
        
        //extension model
        $this->load_model('setting/extension');
        
        $methods = $this->config->get('currency_tools_'.$type.'_methods');
        
        $files = glob(DIR_APPLICATION.'controller/'.$type.'/*.php');
        foreach($files as $file)
        {
            $file = basename($file);
            $code = substr($file,0,-4);
            
            if (!$this->config->get($code.'_status')) continue;
            
            $language->load($type.'/'.$code);
            $result[] = array(
                'code'          =>      $code,
                'name'          =>      trim(strip_tags($language->get('heading_title'))),
                'currency'      =>      isset($methods[$code]) ? $methods[$code] : '',
            );
        }
        
        return $result;
	}
    
	public function getCountries()
    {
        $result = array();
        
        $countries = $this->config->get('currency_tools_countries');
        
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "country ORDER BY `name` ASC");
        foreach($query->rows as $row)
        {
            $result[] = array(
                'code'          =>      $row['iso_code_2'],
                'name'          =>      $row['name'],
                'currency'      =>      isset($countries[$row['iso_code_2']]) ? $countries[$row['iso_code_2']] : '',
            );
        }
        
		return $result;
	}
    
    public function getCronProgress()
    {
        $query = $this->db->query("SELECT COUNT(*) AS `complete`, (SELECT COUNT(*) FROM ".DB_PREFIX."product WHERE `status` = '1' AND `price` > 0) AS `total` FROM ".DB_PREFIX."product WHERE `refresh` = '0' AND `status` = '1' AND `price` > 0");
        return number_format((100*(int)$query->row['complete'])/(int)$query->row['total'],2);
    }
    
	public function format($params)
    {
        //language params
        $decimal_point = $this->language->get('decimal_point');
		$thousand_point = $this->language->get('thousand_point');
        
        //convert
		$value = $params['amount'] * ($params['to_value'] / $params['from_value']);
        
        $value += $value * ($params['margin']/100);
        
        $value += $params['overheads'];
        
        if (strlen((int)$value) > $params['round_value'])
        {
            $value = (float)round($value, $params['round_value']);
        }
        
        $value += $params['deviation'];
        
        //format to string
		$string = '';

		if ($params['symbol_left'])
        {
			$string .= $params['symbol_left'];
		}

		$string .= number_format($value, (int)$params['decimal_place'], $decimal_point, $thousand_point);

		if ($params['symbol_right'])
        {
			$string .= $params['symbol_right'];
		}
        
		return $string;
	}
}
?>