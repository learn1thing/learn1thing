<?php
/*------------------------------------------------------------------------
# Recommended Products
# ------------------------------------------------------------------------
# The Krotek
# Copyright (C) 2011-2016 The Krotek. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://thekrotek.com
# Support: support@thekrotek.com
-------------------------------------------------------------------------*/

class ModelModuleRecommended extends Model
{
	public function checkTables()
	{
		$query = $this->db->query("SHOW TABLES LIKE '".DB_PREFIX."product_recommended'");
		
		if (!$query->num_rows) {
			$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."product_recommended` (
				`product_id` INT(11) NOT NULL,
				`recommended` VARCHAR(255) NOT NULL,
				`module_id` int(11) NOT NULL,
				PRIMARY KEY (`product_id`, `recommended`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");
		}
	}
		
	public function getRecommendedProducts($product_id)
	{
		$query = $this->db->query("SELECT * FROM ".DB_PREFIX."product_recommended WHERE product_id = '".(int)$product_id."'");
		$recommended = $query->row;

		return $recommended;
	}
	
	public function getRecommendedModules()
	{
		$query = $this->db->query("SELECT module_id, name FROM ".DB_PREFIX."module WHERE code = 'recommended'");
		$modules = $query->rows;

		return $modules;
	}
	
	public function autocompleteProducts($data)
	{
		$query = $this->db->query("SELECT p.product_id, pd.name FROM ".DB_PREFIX."product AS p LEFT JOIN ".DB_PREFIX."product_description AS pd ON pd.product_id = p.product_id WHERE pd.name LIKE '%".$data['keyword']."%' AND pd.language_id = '".(int)$this->config->get('config_language_id')."' ORDER BY pd.name ASC LIMIT ".$data['start'].", ".$data['limit']);
		$products = $query->rows;

		return $products;
	}	
}

?>