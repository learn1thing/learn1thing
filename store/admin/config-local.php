<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/L1T-opencart/admin/');
define('HTTP_CATALOG', 'http://localhost/L1T-opencart/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/L1T-opencart/admin/');
define('HTTPS_CATALOG', 'http://localhost/L1T-opencart/');

// DIR
define('DIR_APPLICATION', 'E:\xampp\htdocs\L1T-opencart\admin/');
define('DIR_SYSTEM', 'E:\xampp\htdocs\L1T-opencart\system/');
define('DIR_IMAGE', 'E:\xampp\htdocs\L1T-opencart\image/');
define('DIR_LANGUAGE', 'E:\xampp\htdocs\L1T-opencart\admin/language/');
define('DIR_TEMPLATE', 'E:\xampp\htdocs\L1T-opencart\admin/view/template/');
define('DIR_CONFIG', 'E:\xampp\htdocs\L1T-opencart\system/config/');
define('DIR_CACHE', 'E:\xampp\htdocs\L1T-opencart\system/storage/cache/');
define('DIR_DOWNLOAD', 'E:\xampp\htdocs\L1T-opencart\system/storage/download/');
define('DIR_LOGS', 'E:\xampp\htdocs\L1T-opencart\system/storage/logs/');
define('DIR_MODIFICATION', 'E:\xampp\htdocs\L1T-opencart\system/storage/modification/');
define('DIR_UPLOAD', 'E:\xampp\htdocs\L1T-opencart\system/storage/upload/');
define('DIR_CATALOG', 'E:\xampp\htdocs\L1T-opencart\catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'rps_opencart');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
