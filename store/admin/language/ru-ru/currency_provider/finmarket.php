<?php
// Text
$_['text_name']                     = 'Банки СНГ';

// Entry
$_['entry_country']                 = 'Банк';
$_['entry_time_offset']             = 'Дата актуальности';

// Text
$_['text_country_10148']            = 'Центральный Банк РФ';
$_['text_country_10134']            = 'Нацбанк Украины';
$_['text_country_10120']            = 'Нацбанк Беларуси';
$_['text_country_10123']            = 'Нацбанк Казахстана';
$_['text_today']                    = 'Сегодня';
$_['text_tomorrow']                 = 'Завтра';
?>