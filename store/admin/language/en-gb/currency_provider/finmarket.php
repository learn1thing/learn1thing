<?php
// Text
$_['text_name']                     = 'Central banks of CIS';

// Entry
$_['entry_country']                 = 'Bank';
$_['entry_time_offset']             = 'Relevance point';

// Text
$_['text_country_10148']            = 'Central Bank of Russian Federation';
$_['text_country_10134']            = 'National Bank of Ukraine';
$_['text_country_10120']            = 'National Bank of Belarus';
$_['text_country_10123']            = 'National Bank of Kazakhstan';
$_['text_today']                    = 'Today';
$_['text_tomorrow']                 = 'Tomorrow';
?>