<?php
// Heading
$_['heading_title']    = 'Advance Store Credit';

// Text
$_['text_total']       = 'Order Totals';
$_['text_success']     = 'Success: You have modified advance store credit total!';
$_['text_edit']        = 'Edit Advance Store Credit Total';

// Entry
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify advance store credit total!';
