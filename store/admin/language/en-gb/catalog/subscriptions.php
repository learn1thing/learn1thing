<?php

// Heading
$_['heading_title']					= 'Subscriptions';
$_['text_list']					    = 'Subscription Profiles';

// Text
$_['text_edit']						= 'Edit';
$_['text_form']						= 'Subscriptions Form';
$_['text_enabled']					= 'Enabled';
$_['text_disabled']					= 'Disabled';
$_['text_no_results']				= 'No subscription profiles have been setup!';
$_['text_yes']						= 'Yes';
$_['text_no']						= 'No';
$_['text_none']						= 'None';
$_['text_percent']					= 'Percent';
$_['text_amount']					= 'Amount';
$_['text_success']					= 'Success: You have modified subscriptions!';
$_['text_day']						= 'day';
$_['text_week']						= 'week';
$_['text_semi_month']				= 'semi-month';
$_['text_month']					= 'month';
$_['text_year']						= 'year';
$_['text_trial_help']				= '<font color="blue">You can offer a trial period for your subscription.  At the end of the trial period, the selected subscription will take over unless the customer cancels during the trial period.</font>';
$_['text_recurring_help']			= '<font color="blue">Recurring amounts are calculated by the frequency and cycles.<br />If you use a frequency of "month" and a cycle of "1", then the user will be billed every 1 months.<br />The duration is the number of times the user will make a payment, set this to 0 if you want payments until they are cancelled.</font>';
$_['text_every']					= 'Every';
$_['text_for']						= 'for';
$_['text_until_cancel']				= 'until cancelled';
$_['text_payments']					= 'payments';
$_['text_taxes']					= 'Taxes';
$_['text_shipping']					= 'Shipping';
$_['text_monday']					= 'Monday';
$_['text_tuesday']					= 'Tuesday';
$_['text_wednesday']				= 'Wednesday';
$_['text_thursday']					= 'Thursday';
$_['text_friday']					= 'Friday';
$_['text_saturday']					= 'Saturday';
$_['text_sunday']					= 'Sunday';
$_['text_sd_only_available']		= '</br><font color="red"><b>Specific Day is only available for weekly subscriptions!</b></font>';
$_['text_trial_only']				= 'This is a trial only!';

// Column
$_['column_subscription_id']		= 'ID';
$_['column_recurring']				= 'Recurring';
$_['column_trial']					= 'Trial';
$_['column_trial_terms']			= 'Trial Terms';
$_['column_trial_status']			= 'Trial Status';
$_['column_subscription']			= 'Subscription Terms';
$_['column_specific_day']			= 'Specific Day Due';
$_['column_discount']				= 'Subscription Discount';
$_['column_customer_groups']		= 'Customer Groups';
$_['column_status']					= 'Status';
$_['column_action']					= 'Action';

// Button
$_['button_insert']					= 'Insert';
$_['button_delete']					= 'Delete';
$_['button_save']					= 'Save';
$_['button_cancel']					= 'Cancel';

// Entry
$_['entry_trial']					= 'Offer Trial';
$_['entry_trial_only']				= 'Trial Only<span class="help">If checked, there will be no subscription renewal after the trial period.  This will be a trial ONLY!</span>';
$_['entry_trial_frequency']			= 'Trial Frequency';
$_['entry_trial_cycle']				= 'Trial Cycle';
$_['entry_trial_duration']			= 'Trial Duration';
$_['entry_trial_price']				= 'Trial Cost';
$_['entry_trial_status']			= 'Trial Status';
$_['entry_frequency']				= 'Frequency';
$_['entry_cycle']					= 'Cycle';
$_['entry_duration']				= 'Duration';
$_['entry_subscription']			= 'Subscription';
$_['entry_specific_day']			= 'Specific Day </br><span class="help"><font color="blue">Leave blank to start subscription from today or choose a day your customer will be billed on every cycle for the duration of the subscription.</br> Only available if you choose "week" for the frequency!</font></span>';
$_['entry_discount']				= 'Discount';
$_['entry_discount_type']			= 'Discount Type';
$_['entry_include_totals']			= 'Include Tax, Shipping:</br><span class="help"><font color="blue">Include taxes and/or shipping charges in the recurring payment</font></span>';
$_['entry_status']					= 'Status';
$_['entry_reinstatement_fee']		= 'Reinstatement Fee<br /><span class="help"><font color="blue">Fee to charge to reinstate an expired or cancelled subscription</font></span>';
$_['entry_reinstatement_period']	= 'Reinstatement Period<br /><span class="help"><font color="blue"># of days before an expired or cancelled subscription cannot be reinstated</font></span>';
$_['entry_reminder_count']			= 'Reminder Count<br /><span class="help"><font color="blue"># of times to send reminder emails</font></span>';
$_['entry_reminder_period']			= 'Reminder Period<br /><span class="help"><font color="blue"># of days to wait between reminder emails</font></span>';
$_['entry_grace_period']			= 'Grace Period<br /><span class="help"><font color="blue"># of days past due before becoming delinquent</font></span>';
$_['entry_customer_groups']			= 'Allowed Customer Groups';
$_['entry_sort_order']				= 'Sort Order';

// Error
$_['error_warning']					= 'Warning: Please check the form carefully for errors!';
$_['error_permission']				= 'Warning: You do not have permission to modify subscriptions!';

?>