<?php
// Heading
$_['heading_title']             =   'Multi Currency PRO v0.5.1.9.6(stable-2.2+) [by addist.ru]';
$_['heading_converter']         =   'Multi Currency PRO v0.5.1.9.6(stable-2.2+) - product currency converter';
$_['heading_exporter']          =   'Multi Currency PRO v0.5.1.9.6(stable-2.2+) - product exporter';
$_['heading_importer']          =   'Multi Currency PRO v0.5.1.9.6(stable-2.2+) - product importer';
$_['heading_edit']              =   'Multi Currency PRO v0.5.1.9.6(stable-2.2+) - edit currency';
$_['heading_general']           =   'General settings';
$_['heading_refresh']           =   'Currency rates updater';
$_['heading_cron']              =   'Cron options';
$_['heading_payment']           =   'Payment methods - click to collapse';
$_['heading_shipping']          =   'Shipping methods (source currency) - click to collapse';
$_['heading_country']           =   'Currency by countries - click to collapse';
$_['heading_preview']           =   'Preview';

// Tab
$_['tab_currency']              =   'Currencies';
$_['tab_widget']                =   'Widget';
$_['tab_tools']                 =   'Tools';

// Text
$_['text_module']               =   'Modules';
$_['text_success']              =   'Setting successfully modified!';
$_['text_saved']                =   'Currency successfully saved!';
$_['text_edit']                 =   'Edit';
$_['text_autodetect']           =   'Detect automatically';
$_['text_refreshed']            =   'Currency rates has been successfully refreshed!';
$_['text_by_category']          =   'By category';
$_['text_by_brand']             =   'By manufacturer';
$_['text_all']                  =   'All';
$_['text_select_file']          =   'Select file';
$_['text_converter']            =   'Product currencies converter';
$_['text_export']               =   'Exported products';
$_['text_exporter']             =   'Export products to Excel(.xls)';
$_['text_importer']             =   'Import products from Excel(.xls)';
$_['text_converted']            =   'Product currencies are converted!';
$_['text_success_import']       =   'Product are successfully imported!';
$_['text_confirm']              =   'Are you sure to continue?';

// Entry
$_['entry_status']              =   'Status';
$_['entry_action']              =   'Action';
$_['entry_currency_admin']      =   'Backend currency';
$_['entry_currency_product']    =   'Product currency';
$_['entry_currency_store']      =   'Store currency';
$_['entry_restriction']         =   'Restriction';
$_['entry_session_lifetime']    =   'Session lifetime';
$_['entry_country_detector']    =   'Country detection service';
$_['entry_currency_secondary']  =   'Secondary currency';
$_['entry_currency_payment']    =   'Payment currency';
$_['entry_template']            =   'Template';
$_['entry_use_core']            =   'Built-in formatter';
$_['entry_hours']               =   'Hours';
$_['entry_provider']            =   'Source';
$_['entry_cron']                =   'Cron command';
$_['entry_executed']            =   'Last executed';
$_['entry_progress']            =   'Progress';
$_['entry_name']                =   'Name';
$_['entry_code']                =   'Code';
$_['entry_value']               =   'Value';
$_['entry_reversed_value']      =   'Reversed value';
$_['entry_round_value']         =   'Round value(+/-)';
$_['entry_decimal_place']       =   'Decimal place';
$_['entry_correction']          =   'Correction(+/-)';
$_['entry_margin']              =   'Margin(%)';
$_['entry_overheads']           =   'Overheads(+/-)';
$_['entry_deviation']           =   'Deviation(+/-)';
$_['entry_autorefresh']         =   'Auto-refresh';
$_['entry_input']               =   'Input value';
$_['entry_preview']             =   'Preview';
$_['entry_widget_currency']     =   'Main currency';
$_['entry_widget_tag']          =   'Container tag';
$_['entry_widget_container']    =   'Container ID';
$_['entry_widget_selector']     =   'Selector';
$_['entry_widget_method']       =   'jQuery method';
$_['entry_widget_style']        =   'CSS style';
$_['entry_widget_template']     =   'Template';
$_['entry_currency_new']        =   'New currency';
$_['entry_currency_skip']       =   'Skip products with currency';
$_['entry_criteria']            =   'Filter';
$_['entry_category']            =   'Categories';
$_['entry_brand']               =   'Manufacturers';
$_['entry_file']                =   'File';

// Button
$_['button_refresh']            =   'Refresh rates';
$_['button_preview']            =   'Preview';
$_['button_open']               =   'Open';
$_['button_convert']            =   'Convert';
$_['button_export']             =   'Export';
$_['button_import']             =   'Import';

// Help
$_['help_status']               =   'When you enable/disable it, the extension\'s modifications will be automatically installed/uninstalled. If it does not work even it is installed, you must check the extension\'s modifications.';
$_['help_currency_admin']       =   'Source currency that will be used for all items by default. This currency\'s value must be equal to "1.00".';
$_['help_currency_product']     =   'Default product base currency that is used as base currency for products which the base currency is not assigned for.';
$_['help_currency_store']       =   'The currency in which prices are displayed on the store for visitor. By default, it inherits the backend currency\'s value. It\'s value can be also automatically detected by country. If you use autodetection option, you must set currency for countries(see the block below).';
$_['help_country_detector']     =   'Country detection API service, which is used for automatic currency detection.';
$_['help_session_lifetime']     =   'The store currency lifetime in seconds. The value is stored in the visitor\'s session during this time. If you use auto-detection feature, it is recommended to use value >= 86400 sec.';
$_['help_restriction']          =   'This option restricts visitors to change the currency on the store and hides currency selector.';
$_['help_currency_secondary']   =   'Auxilary currency that will be used for comparing prices using store currency.<br/>For example: $100.00 (<strong>€ 90.00</strong>)';
$_['help_currency_payment']     =   'Checkout/payment currency. By default, it inherits "Store currency" value.<br/>Also, you can specify different currencies for each payment method(see "Payment methods" block).';
$_['help_template']             =   'Currency template for displaying prices on the store. Accepted variables:<span class="kbd"><kbd>{main}</kbd> - current store currency,</span><span class="kbd"><kbd>{secondary}</kbd> - secondary currency,</span><span class="kbd"><kbd>{payment}</kbd> - current payment currency,</span><span class="kbd"><kbd>[ISO 4217 CODE]</kbd> - separate currency,</span><span class="kbd"><kbd>#n</kbd> - new line.</span>';
$_['help_use_core']             =   'Prices are formatted through the built-in library((system/library/currency.php).';
$_['help_autorefresh']          =   'If you enable this option, the currency rates will be refreshed regularly.';
$_['help_hours']                =   'If "Auto-refresh" option is enabled, the currency rates will be automatically refreshed on selected hours. You can select multiple hours by holding "CTRL" button.';
$_['help_provider']             =   'You can select the provider, which you get currency rates from. If you can\'t find the provider that you want to load from, you can contact us by email <mark><a href="mailto:dev@addist.ru">dev@addist.ru</a></mark> and we will add your provider for free.';
$_['help_payment_methods']      =   'You can select different currencies for each payment method. When customer selects a payment method on the checkout page, the checkout payment currency will be changed to the corresponding currency and the customer will pay the order in this currency.';
$_['help_shipping_methods']     =   'You can select different "source" currencies for each shipping method. It means, that you must specify shipping costs in the corresponding currency. When you select a shipping method, the shipping amount will be converted from corresponding currency to payment currency.';
$_['help_cron']                 =   'You must copy the line below and add cron entry with "every minute" interval. It is important for currency rates automatically refreshing and price caching.';
$_['help_countries']            =   'You can select different currencies for each country. If "Auto-detection" option is enabled, visitors currency will be changed to the currency, that specified for his country.';
$_['help_value']                =   'The currency\'s rate that must be equal to 1 %s.';
$_['help_reversed_value']       =   'The currency\'s reversed rate. Its value = 1/its rate.';
$_['help_correction']           =   'This value will be added to the currency\'s rate as deviation.';
$_['help_margin']               =   'The percentage value, that will be added to the product prices on frontend as percentage.';
$_['help_overheads']            =   'The fixed value, that will be added to the product prices on frontend.';
$_['help_deviation']            =   'The fixed value, that will be added to the prices on frontend that is used for creating beautiful prices.';
$_['help_round_value']          =   'This value is used for rounding prices. Use negative value to round to left and positive to right.';
$_['help_decimal_place']        =   'Length of values after point.';
$_['help_widget_status']        =   'The widget is an informer the shows currency rates on frontend.';
$_['help_widget_currency']      =   'The main currency for comparision that is equal to 1.';
$_['help_widget_tag']           =   'Container\'s HTML tag, that contain widget\'s container.';
$_['help_widget_container']     =   'The ID of the container that contain the widget.';
$_['help_widget_selector']      =   'jQuery selector for connection the widget\'s container.';
$_['help_widget_method']        =   'jQuery method for inserting widget.';
$_['help_widget_template']      =   'Widget\'s template for displaying rates on store. Accepted variables:<span class="kbd"><kbd>{main}</kbd> - main widget currency,</span><span class="kbd"><kbd>{store}</kbd> - current store currency,</span><span class="kbd"><kbd>{secondary}</kbd> - secondary currency,</span><span class="kbd"><kbd>{payment}</kbd> - current payment currency,</span><span class="kbd"><kbd>[ISO 4217 CODE]</kbd> - separate currency.</span>';
$_['help_convert']              =   'Currency converter helps you to convert product currencies with one click.';
$_['help_export']               =   'This tool helps you to export products to Excel(.xls) with price, currency, margin and overheads values.';
$_['help_import']               =   'This tool helps you to import products from Excel(.xls) with price and currency values.';

// Error
$_['error_permission']          =   'Warning: You do not have permission to modify this module!';
$_['error_session_lifetime']    =   'Warning: It is recommended to use bigger value(for example: 86400 sec.) for "Session life" option!';
$_['error_provider']            =   'Error with currency rates provider.';
$_['error_refresh']             =   'An error occured while updating currency rates.';
$_['error_cron']                =   'The cron task is not executed yet. It is very important for correct functioning. If you have added it recently, please, wait a little time.';
$_['error_cron_time']           =   'The cron task is executed last time a long time ago. Please, check it.';
$_['error_no_product']          =   'There are no products by selected criteria.';
$_['error_phpexcel']            =   'The PHPEcel library is not found, please download it from <a href="http://addist.ru/upload/phpexcel.zip" target="_blank">here</a> and extract to your sites root folder!';
$_['error_convert']             =   'An error occured while converting product currencies.';
$_['error_export']              =   'An error occured while exporting products.';
$_['error_file_empty']          =   'File is not selected!';
$_['error_file_type']           =   'Invalid file type!';
$_['error_currency_code']       =   'There are some currencies with wrong codes: %s';
$_['error_temp_error']          =   'The temp folder %s does not exist or has not write access.';
?>