<?php

// Heading
$_['heading_title']       	= '<font color="2755bb"><b>Product Subscriptions</b></font>';

// Text
$_['text_edit']				= 'Product Subscriptions Settings';
$_['text_module']      		= 'Modules';
$_['text_yes']				= 'Yes';
$_['text_no']				= 'No';
$_['text_success']			= 'You have successfully modified module product subscribe';

// Entry
$_['entry_add_shipping']	= 'Add Shipping:';
$_['entry_add_taxes']		= 'Add Taxes:';

// Error
$_['error_permission']    	= 'Warning: You do not have permission to modify module product subscription!';

?>