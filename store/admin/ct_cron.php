<?php
ob_start();

//error_reporting
error_reporting(E_ALL);
ini_set('display_errors','on');

//configuration
if (is_file(dirname(__FILE__).'/config.php'))
{
	require_once('config.php');
}

//install
if (!defined('DIR_APPLICATION'))
{
	die('NOT INSTALLED!');
}

//constants
define('STORE_ID',0);
define('IS_ADMIN',true);

//oc version
define('VERSION',defined('DIR_MODIFICATION') ? '2.0' : '1.5.5');
define('OC_VERSION',defined('DIR_MODIFICATION') ? '2.x' : '1.5.x');

//load addist helper
require_once(DIR_SYSTEM . 'addist/helper.php');

//libraries
require_once(DIR_SYSTEM . 'engine/registry.php');
require_once(DIR_SYSTEM . 'engine/model.php');
require_once(DIR_SYSTEM . 'engine/action.php');
require_once(DIR_SYSTEM . 'engine/controller.php');
require_once(DIR_SYSTEM . 'engine/event.php');
require_once(DIR_SYSTEM . 'engine/front.php');
require_once(DIR_SYSTEM . 'engine/loader.php');
require_once(DIR_SYSTEM . 'engine/proxy.php');

//addist libraries
require_once(DIR_SYSTEM . 'addist/engine/class.php');
require_once(DIR_SYSTEM . 'addist/library/config.php');

//startup
function library($class)
{
	$file = DIR_SYSTEM . 'library/' . str_replace('\\', '/', strtolower($class)) . '.php';

	if (is_file($file))
    {
		include_once($file);
		return true;
	}
    else
    {
		return false;
	}
}

spl_autoload_register('library');
spl_autoload_extensions('.php');

//registry
$registry = new Registry();

//database
$db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, defined('DB_PORT') ? DB_PORT : '3306');
$registry->set('db',$db);

//config
$config = new Config();
$registry->set('config',$config);

//settings
$query = $db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '0'");
foreach ($query->rows as $setting)
{
	if (!$setting['serialized'])
    {
		$config->set($setting['key'], $setting['value']);
	}
    else
    {
		$config->set($setting['key'], json_decode($setting['value'], true));
	}
}

//addist config
$config = new AddistConfig($registry);
$registry->set('config',$config);

//request
$request = new Request();
$registry->set('request',$request);
$request->get['currency'] = $config->get('config_currency');
$request->cookie['currency'] = $config->get('config_currency');

//currency
$currency = new Cart\Currency($registry);
$registry->set('currency',$currency);

//shutdown function
function shutdown()
{
    global $registry;
    $registry->get('config')->save('currency_tools','cron_running',false);
    $result = ob_get_contents();
    ob_clean();
    if ($registry->get('config')->get('addist_installer_debug'))
    {
        file_put_contents(DIR_LOGS.'cron.log',$result,FILE_APPEND);
        echo $result;
    }
}

//register shutdown
register_shutdown_function('shutdown');

//execute cron
require_once(DIR_SYSTEM.'library/currency_tools_helper.php');
$helper = new CurrencyToolsHelper($registry);
$helper->cron();
?>