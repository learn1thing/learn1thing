<?php
class ModelTotalCreditAdvance extends Model {
	public function getTotal($total) {
		if ($this->config->get('credit_advance_status') && isset($this->session->data['credit_advance'])) {
			$this->load->language('total/credit_advance');

            $balance = $this->customer->getBalance();

			if (($this->session->data['credit_advance'] <= $balance) && (float)$this->session->data['credit_advance']) {
				if ($this->session->data['credit_advance'] > $total['total']) {
					$credit = $total['total'];
				} else {
					$credit = $this->session->data['credit_advance'];
				}

				if ($credit > 0) {
				   $total['totals'][] = array(
                        'code'       => 'credit_advance',
					    'title'      => $this->language->get('text_credit_advance'),
					    'value'      => -$credit,
					    'sort_order' => $this->config->get('credit_advance_sort_order')
				    );

				    $total['total'] -= $credit;
                }
			}
		}
	}

	public function confirm($order_info, $order_total) {
		$this->load->language('total/credit_advance');

		$transaction_total = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$order_info['customer_id'] . "'");

		if ($transaction_total->row['total'] >= -$order_total['value']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer_transaction SET customer_id = '" . (int)$order_info['customer_id'] . "', order_id = '" . (int)$order_info['order_id'] . "', description = '" . $this->db->escape(sprintf($this->language->get('text_order_id'), (int)$order_info['order_id'])) . "', amount = '" . (float)$order_total['value'] . "', date_added = NOW()");
		} else {
			return $this->config->get('config_fraud_status_id');
		}
	}

	public function unconfirm($order_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_transaction WHERE order_id = '" . (int)$order_id . "'");
	}
}
