<?php
/*------------------------------------------------------------------------
# Recommended Products
# ------------------------------------------------------------------------
# The Krotek
# Copyright (C) 2011-2016 The Krotek. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://thekrotek.com
# Support: support@thekrotek.com
-------------------------------------------------------------------------*/
	
class ModelModuleRecommended extends Model
{	
	public function getRecommendedProducts($product_id)
	{
		$recommended = array();
		
		$query = $this->db->query("SELECT recommended FROM ".DB_PREFIX."product_recommended pr LEFT JOIN ".DB_PREFIX."product p ON (p.product_id IN (pr.recommended)) LEFT JOIN ".DB_PREFIX."product_to_store p2s ON (p.product_id = p2s.product_id) WHERE pr.product_id = '".(int)$product_id."' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '".(int)$this->config->get('config_store_id')."'");
		
		if ($query->num_rows) {
			$this->load->model('catalog/product');
			
			$recommended_products = explode(',', $query->row['recommended']);
				
			foreach ($recommended_products as $recommended_id) {
				$recommended[$recommended_id] = $this->model_catalog_product->getProduct($recommended_id);
			}
		}

		return $recommended;
	}
	
	public function getRecommendedModule($product_id)
	{
		$query = $this->db->query("SELECT m.setting FROM ".DB_PREFIX."module AS m LEFT JOIN ".DB_PREFIX."product_recommended AS pr ON pr.module_id = m.module_id WHERE m.code = 'recommended' AND pr.product_id = '".(int)$product_id."'");
		$module = $query->row;

		if (!empty($module['setting'])) {
			if (@json_decode($module['setting']) !== NULL) $settings = json_decode($module['setting'], true);
			else $settings = unserialize($module['setting']);
		} else {
			$settings = array();
		}

		return $settings;
	}
}

?>