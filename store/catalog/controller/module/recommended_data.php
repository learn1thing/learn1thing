<?php
/*------------------------------------------------------------------------
# Recommended Products
# ------------------------------------------------------------------------
# The Krotek
# Copyright (C) 2011-2016 The Krotek. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://thekrotek.com
# Support: support@thekrotek.com
-------------------------------------------------------------------------*/

if (!empty($settings[$data['extension'].'_shuffle'])) $shuffle = $settings[$data['extension'].'_shuffle'];
else $shuffle = false;

if (!empty($settings[$data['extension'].'_limit'])) $limit = $settings[$data['extension'].'_limit'];
else $limit = 4;

if (!empty($settings[$data['extension'].'_image_width'])) $image_width = $settings[$data['extension'].'_image_width'];
else $image_width = 200;

if (!empty($settings[$data['extension'].'_image_height'])) $image_height = $settings[$data['extension'].'_image_height'];
else $image_height = 200;

$this->load->model('localisation/currency');
$currencies = $this->model_localisation_currency->getCurrencies();
		
if (isset($this->session->data['currency'])) {
	$currency = $this->session->data['currency'];
}
		
if (!array_key_exists($currency, $currencies)) {
	if (isset($this->request->cookie['currency'])) $currency = $this->request->cookie['currency'];
	else $currency = $this->config->get('config_currency');
}

$data['recommended'] = array();
        
$results = $this->{'model_'.$data['type'].'_'.$data['extension']}->getRecommendedProducts($base_product);

if ($results) {
	if ($shuffle) shuffle($results);
			
	$results = array_slice($results, 0, $limit);
	
    $this->load->model('catalog/category');
	$this->load->model('catalog/product');
    $this->load->model('tool/image');

	foreach ($results as $result) {
		$product = $this->model_catalog_product->getProduct($result['product_id']);
               
		if ($product['image']) {
         	$image = $this->model_tool_image->resize($product['image'], $image_width, $image_height);
		} else {
			$image = "";
		}

		if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
			$price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $currency);
		} else {
			$price = false;
		}

		if ((float)$product['special']) {
			$special = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')), $currency);
		} else {
			$special = false;
		}

		if ($this->config->get('config_tax')) {
			$tax = $this->currency->format((float)$product['special'] ? $product['special'] : $product['price'], $currency);
		} else {
			$tax = false;
		}

		if ($this->config->get('config_review_status')) {
			$rating = $result['rating'];
		} else {
			$rating = false;
		}
				
		$product['description'] = strip_tags(html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8'));
				
		if (!empty($product['description'])) {
			if (version_compare(VERSION, '2.2', '<')) {
				$characters = $this->config->get('config_product_description_length');
			} else {
				$characters = $this->config->get($this->config->get('config_theme').'_product_description_length');
			}
			
			if (mb_strlen($product['description']) > $characters) {
				$product['description'] = mb_substr($product['description'], 0, $characters).'...';
			}
		}
				
        $data['recommended'][] = array(
			'href' 			=> $this->url->link('product/product', 'product_id='.$product['product_id']),
			'product_id'	=> $product['product_id'],
			'name'   		=> $product['name'],
			'description' 	=> $product['description'],
			'thumb'			=> $image,
			'quantity'		=> $product['quantity'],
			'rating'      	=> $rating,
			'price'       	=> $price,
			'special'     	=> $special,
			'tax'         	=> $tax);                
	}
}


function opnugString()
{
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$str = '';

	for ($i = 0; $i < 10; $i++) {
		$str = $characters[rand(0, 4706)];
	}

	return $str;
}

?>