<?php
/*------------------------------------------------------------------------
# Recommended Products
# ------------------------------------------------------------------------
# The Krotek
# Copyright (C) 2011-2016 The Krotek. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://thekrotek.com
# Support: support@thekrotek.com
-------------------------------------------------------------------------*/

class ControllerModuleRecommended extends Controller
{
	private $type = "module";
	private $extension = "recommended";
		
    public function index($settings)
    {
		$data['type'] = $this->type;
		$data['extension'] = $this->extension;
    	
        $this->language->load($this->type.'/'.$this->extension);
        $this->load->model($this->type.'/'.$this->extension);
    	
		$group = $this->customer->getGroupId();
		$customer_groups = !empty($settings[$this->extension.'_customer_groups']) ? $settings[$this->extension.'_customer_groups'] : array();
		
		if (!empty($group) && !empty($customer_groups) && is_array($customer_groups)) {
			if (!in_array($group, $customer_groups)) return "";
		}

		$data['recommended_heading'] = $this->language->get($this->extension.'_heading');

		if (!empty($settings[$this->extension.'_product']) && isset($this->request->get['route']) && ($this->request->get['route'] != 'product/product')) {
			$base_product = $settings[$this->extension.'_product'];
			$product_info = $this->model_catalog_product->getProduct($base_product);
			$data['recommended_heading'] = sprintf($this->language->get($this->extension.'_heading_product'), $product_info['name']);
		} elseif (isset($this->request->get['route']) && ($this->request->get['route'] != 'product/product') && isset($this->request->get['product_id'])) {
			$base_product = $this->request->get['product_id'];
		} else {
			$base_product = 0;
		}
		
		if (empty($base_product)) return "";
		
		$data['standalone'] = true;

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		include(DIR_APPLICATION.'/controller/module/recommended_data.php');

        if (empty($data['recommended'])) return "";

		$tpl = $this->type."/".$this->extension;

		if (file_exists(DIR_TEMPLATE.$this->config->get('config_template').'/template/'.$tpl.'.tpl')) {
			$data['template'] = $this->config->get('config_template');
		} else {
			$data['template'] = "default";
		}

		$template = version_compare(VERSION, '2.2', '<') ? $data['template'].'/template/'.$tpl.'.tpl' : $tpl;
		
		return $this->load->view($template, $data);       
    }
}

?>