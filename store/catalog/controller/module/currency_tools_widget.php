<?php
#encode-me[]#
if (!class_exists('AddistObject')) require_once(DIR_SYSTEM.'addist/startup.php');
class ControllerModuleCurrencyToolsWidget extends AddistController
{
    protected $path = 'module/currency_tools/currency_tools_pro';
    
    public function __construct($registery)
    {
        parent::__construct($registery);
    }
    
    public function index()
    {
        if (!$this->config->get('currency_tools_widget_status')) return false;
        
        //data
        $this->data['style'] = $this->config->get('currency_tools_widget_style');
        $this->data['tag'] = $this->config->get('currency_tools_widget_tag');
        $this->data['container'] = $this->config->get('currency_tools_widget_container');
        $this->data['selector'] = $this->config->get('currency_tools_widget_selector');
        $this->data['method'] = $this->config->get('currency_tools_widget_method');
        
        //container
        $language_code = $this->session->data['language'];
        $templates = $this->config->get('currency_tools_widget_template');
        if (!empty($templates[$language_code]))
        {
            $template = $templates[$language_code];
        }
        elseif (count($templates))
        {
            $template = current($templates);
        }
        else
        {
            $template = '{main} = {store}';
        }
        
        //main vars
        $replaces['{main}'] = $this->format($this->config->get('currency_tools_widget_currency'));
        $replaces['{store}'] = $this->format($this->currency->getCode('store'));
        $replaces['{payment}'] = $this->format($this->currency->getCode('payment'));
        
        //[currency]
        preg_match_all('#\[(\w+)\]#',$template,$matches);
        if ($matches)
        {
            foreach($matches[0] as $i=>$match)
            {
                if ($this->currency->has($matches[1][$i]))
                {
                    $replaces[$match] = $this->format($matches[1][$i]);
                }
            }
        }
        
        $this->data['content'] = str_replace(array_keys($replaces),$replaces,$template);
        
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/currency_tools_widget.tpl'))
        {
            $this->template = $this->config->get('config_template') . '/template/module/currency_tools_widget.tpl';
        }
        else
        {
            $this->template = 'default/template/module/currency_tools_widget.tpl';
        }
        
        return $this->setOutput();
    }
    
    public function format($currency)
    {
        $decimal_point = $this->language->get('decimal_point');  
        $thousand_point = $this->language->get('thousand_point');
        
        $value = $this->currency->convert(1, $this->config->get('currency_tools_widget_currency'), $currency, false, false);
        
        $value = number_format($value, (int)$this->currency->getDecimalPlace($currency), $decimal_point, $thousand_point);
        $result = $this->currency->getSymbolLeft($currency).$value.$this->currency->getSymbolRight($currency);
        
        return $result;
    }
}
?>