<?php
class ControllerTotalCreditAdvance extends Controller {
	public function index() {
        $balance = $this->customer->getBalance();

		$price_total = 0;

		foreach ($this->cart->getProducts() as $product) {
			if ($product['total']) {
				$price_total += $product['total'];
			}
		}

		if (($balance > 0) && $price_total && $this->config->get('credit_advance_status')) {
			$this->load->language('total/credit_advance');

			$data['heading_title'] = sprintf($this->language->get('heading_title'), $this->currency->format($balance, $this->session->data['currency']));

			$data['text_loading'] = $this->language->get('text_loading');

			$data['entry_credit_advance'] = sprintf($this->language->get('entry_credit_advance'), $this->currency->format($price_total, $this->session->data['currency']));

			$data['button_credit_advance'] = $this->language->get('button_credit_advance');

			if (isset($this->session->data['credit_advance'])) {
				$data['credit_advance'] = $this->session->data['credit_advance'];
			} else {
				$data['credit_advance'] = '';
			}

			return $this->load->view('total/credit_advance', $data);
		}
	}

	public function credit_advance() {
		$this->load->language('total/credit_advance');

		$json = array();

        $balance = $this->customer->getBalance();

		$price_total = 0;

		foreach ($this->cart->getProducts() as $product) {
			if ($product['total']) {
				$price_total += $product['total'];
			}
		}

		if (empty($this->request->post['credit_advance'])) {
			$json['error'] = $this->language->get('error_credit_advance');
		}

		if ($this->request->post['credit_advance'] > $balance) {
			$json['error'] = sprintf($this->language->get('error_balance'), $this->request->post['credit_advance']);
		}

		if ($this->request->post['credit_advance'] > $price_total) {
			$json['error'] = sprintf($this->language->get('error_maximum'), $price_total);
		}

		if (!$json) {
			$this->session->data['credit_advance'] = abs($this->request->post['credit_advance']);

			$this->session->data['success'] = $this->language->get('text_success');

			if (isset($this->request->post['redirect'])) {
				$json['redirect'] = $this->url->link($this->request->post['redirect']);
			} else {
				$json['redirect'] = $this->url->link('checkout/cart');	
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
