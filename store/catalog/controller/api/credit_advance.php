<?php
class ControllerApiCreditAdvance extends Controller {
	public function index() {
		$this->load->language('api/credit_advance');

		// Delete past credit_advance in case there is an error
		unset($this->session->data['credit_advance']);

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			$balance = $this->customer->getBalance();

			$price_total = 0;

			foreach ($this->cart->getProducts() as $product) {
				if ($product['total']) {
					$price_total += $product['total'];
				}
			}

			if (empty($this->request->post['credit_advance'])) {
				$json['error'] = $this->language->get('error_credit_advance');
			}

			if ($this->request->post['credit_advance'] > $balance) {
				$json['error'] = sprintf($this->language->get('error_balance'), $this->request->post['credit_advance']);
			}

			if ($this->request->post['credit_advance'] > $price_total) {
				$json['error'] = sprintf($this->language->get('error_maximum'), $price_total);
			}

			if (!$json) {
				$this->session->data['credit_advance'] = abs($this->request->post['credit_advance']);

				$json['success'] = $this->language->get('text_success');
			}
		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function maximum() {
		$this->load->language('api/credit_advance');

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			$json['maximum'] = 0;

			foreach ($this->cart->getProducts() as $product) {
				if ($product['total']) {
					$json['maximum'] += $product['total'];
				}
			}
		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function available() {
		$this->load->language('api/credit_advance');

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			$json['total'] = $this->customer->getBalance();
		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
