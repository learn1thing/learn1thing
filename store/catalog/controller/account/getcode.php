<?php
class ControllerAccountGetcode extends Controller {
	private $error = array();

	public function index() {
		$code = '';
		if (!empty($this->session->data['code'])) {
			$code = $this->session->data['code'];
		} else {
			$this->session->data['code'] = uniq_code();
			$code = $this->session->data['code'];	
		}

		echo $code;
	}
}