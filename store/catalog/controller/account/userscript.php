<?php
class ControllerAccountUserscript extends Controller {
	private $error = array();

	public function index() {
		require_once(dirname(dirname(dirname(dirname(__FILE__)))) . '/sso.php');
		$sso_login = sso_login();
		if ($this->customer->isLogged()) {
			//$this->response->redirect($this->url->link('account/account', '', true));
		}
		/*
			$this->event->trigger('pre.customer.login');
			// Trigger customer post login event
			$this->event->trigger('post.customer.login');
			*/
		$this->document->setTitle($this->language->get('heading_title'));
		



		if ($sso_login == 'logout') {
			$this->customer->logout();
		}

		if(!empty($sso_login) && !empty($sso_login->data->email)) {
				
			//if($sso_login->data->customer_id!=)

			$refreshpage = false;
			if($sso_login->data->customer_id!=$this->session->data[ 'customer_id' ]){
					$refreshpage = true;
			}

			$this->customer->logout();
			$this->customer->login(decrypt_password($sso_login->data->email), '', true);


			$this->session->data[ 'customer_id' ] = $sso_login->data->customer_id;
			$this->session->data[ 'token_middleware' ] = $sso_login->data->token;



			/*print_r($this->session);
			print_r($this->customer->getId());*/

			$this->event->trigger('pre.customer.login');

			unset($this->session->data['guest']);

			
			$this->load->model('account/address');

			//if ($this->config->get('config_tax_customer') == 'payment') {
				$this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());

			//}

			//if ($this->config->get('config_tax_customer') == 'shipping') {
				$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
			//}
//print_r($this->session);
			// Wishlist
			if (isset($this->session->data['wishlist']) && is_array($this->session->data['wishlist'])) {
				$this->load->model('account/wishlist');

				foreach ($this->session->data['wishlist'] as $key => $product_id) {
					$this->model_account_wishlist->addWishlist($product_id);

					unset($this->session->data['wishlist'][$key]);
				}
			}

			// Add to activity log
			$this->load->model('account/activity');

			$activity_data = array(
				'customer_id' => $this->customer->getId(),
				'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
			);

			$this->model_account_activity->addActivity('login', $activity_data);


			$this->event->trigger('post.customer.login');

				//var_dump($refreshpage);die();

			if ($refreshpage){
				header('Location: '.HTTPS_SERVER);
			}
			//print_r($this->session->data);die();

		}
	}
}