<?php
class ControllerInformationGuide extends Controller {
	private $error = array();

	public function index() {
		$filename = 'user-guide.pdf';
		header('Content-disposition: attachment; filename='.$filename);
		header('Content-type: application/pdf');
		readfile($filename);
	}
}