<?php
class ControllerCommonHome extends Controller {
	public function index() {

		$this->load->model('account/customer');

		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink(HTTP_SERVER, 'canonical');
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		// if (empty($this->session->data[ 'token_middleware' ])) {
		
		// 	if (isset($_COOKIE['token']) && isset($_COOKIE['password'])) {
		// 		$getProfile = $this->getProfileMiddleware( $_COOKIE['token'] );
		// 		$decodeAPI  = json_decode($getProfile);

		// 		$this->customer->login($decodeAPI->data->email, $_COOKIE['password']);
		// 		$this->session->data[ 'token_middleware' ] = $_COOKIE['token'];
		// 	}
		// }

		$this->response->setOutput($this->load->view('common/home', $data));
	}

	public function getProfileMiddleware( $token )
	{
		$ch = curl_init();
		
		curl_setopt($ch,CURLOPT_URL, "https://store.learn1thing.com/dashboard/api/v1/profile");
		//curl_setopt($ch,CURLOPT_HTTPHEADER, $parameters);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		 'Authorization:'. $token
		));
	    //execute post
	    $result = curl_exec($ch);
	    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	 	
	    //close connection
	    curl_close($ch);
	 	
	    return $result;
	}
}
