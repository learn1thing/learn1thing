<?php
/**
 * shipping_method_admin.php
 *
 * Shipping method management
 *
 * @author     Makai Lajos
 * @copyright  2015
 * @license    License.txt
 * @version    1.0
 * @link       http://opencart-api.com/product/opencart-rest-admin-api/
 * @see        http://webshop.opencart-api.com/rest-admin-api/
 */
require_once(DIR_SYSTEM . 'engine/restadmincontroller.php');

class ControllerRestShippingMethodAdmin extends RestAdminController {

    /*
    * Get shipping methods
    */
    public function listShippingMethods() {

        $json = array('success' => false);

        $this->load->model('extension/extension');

        $results = $this->model_extension_extension->getExtensions('shipping');

        if (count($results) == 0 || empty($results)) {
            $json['error'] = "No shipping method found";
        } else {
            $json['success'] = true;
            $json['data'] = $results;
        }

        $this->sendResponse($json);
    }

    /*
    * SHIPPING METHOD FUNCTIONS
    * index.php?route=rest/shipping_method_admin/shippingmethods
    */
    public function shippingmethods() {

        $this->checkPlugin();

        if ( $_SERVER['REQUEST_METHOD'] === 'GET' ){
            $this->listShippingMethods();
        }
    }
}