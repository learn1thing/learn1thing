<?php
/**
 * payment_method_admin.php
 *
 * Payment method management
 *
 * @author     Makai Lajos
 * @copyright  2015
 * @license    License.txt
 * @version    1.0
 * @link       http://opencart-api.com/product/opencart-rest-admin-api/
 * @see        http://webshop.opencart-api.com/rest-admin-api/
 */
require_once(DIR_SYSTEM . 'engine/restadmincontroller.php');

class ControllerRestPaymentMethodAdmin extends RestAdminController {

    /*
    * Get payment methods
    */
    public function listPaymentMethods() {

        $json = array('success' => false);

        $this->load->model('extension/extension');

        $results = $this->model_extension_extension->getExtensions('payment');

        if (count($results) == 0 || empty($results)) {
            $json['error'] = "No payment method found";
        } else {
            $json['success'] = true;
            $json['data'] = $results;
        }

        $this->sendResponse($json);
    }

    /*
    * PAYMENT METHOD FUNCTIONS
    * index.php?route=rest/payment_method_admin/paymentmethods
    */
    public function paymentmethods() {

        $this->checkPlugin();

        if ( $_SERVER['REQUEST_METHOD'] === 'GET' ){
            $this->listPaymentMethods();
        }
    }
}