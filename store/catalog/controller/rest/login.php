<?php  

/**
 * login.php
 *
 * Login management
 *
 * @author     Makai Lajos
 * @copyright  2015
 * @license    License.txt
 * @version    2.0
 * @link       http://opencart-api.com/product/opencart-restful-api-pro-v2-0/
 * @see        http://newapi2.opencart-api.com/demo/
 */
require_once(DIR_SYSTEM . 'engine/restcontroller.php');

class ControllerRestLogin extends RestController {

	const FACEBOOK_USER_INFORMATION_URL = 'https://graph.facebook.com/me?fields=email,name';
	const GOOGLE_USER_INFORMATION_URL = 'https://www.googleapis.com/oauth2/v3/tokeninfo?access_token=';

	/*
	* Login user
	*/
	public function login() {

		$this->checkPlugin();

		$json = array('success' => true);

		if ( $_SERVER['REQUEST_METHOD'] === 'POST' ){

			$requestjson = file_get_contents('php://input');

			$requestjson = json_decode($requestjson, true);

			$post		 = $this->request->post;

			$this->language->load('checkout/checkout');
			$this->load->model('account/customer');

			if ($this->customer->isLogged()) {
				$json['error']		= "User already is logged";
				$json['success']	= false;
			}

			if ($json['success']) {
				if (!$this->customer->login($post['email'], $post['password'],true)) {
					$json['error']['warning'] = $this->language->get('error_login');
					$json['success']	= false;
				}

				

				$customer_info = $this->model_account_customer->getCustomerByEmail($post['email']);

				if ($customer_info && !$customer_info['approved']) {
					$json['error']['warning'] = $this->language->get('error_approved');
					$json['success']	= false;
				}
			}

			if ($json['success']) {
				unset($this->session->data['guest']);

				// Default Addresses
				$this->load->model('account/address');

				if ($this->config->get('config_tax_customer') == 'payment') {
					$this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
				}

				if ($this->config->get('config_tax_customer') == 'shipping') {
					$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
				}

				// Add to activity log
				$this->load->model('account/activity');

				$activity_data = array(
					'customer_id' => $this->customer->getId(),
					'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
				);

				$this->model_account_activity->addActivity('login', $activity_data);

				unset($customer_info['password']);
				unset($customer_info['token']);
				unset($customer_info['salt']);

				$customer_info["session"] = session_id();

				// Custom Fields
				$this->load->model('account/custom_field');

				$customer_info['custom_fields'] = $this->model_account_custom_field->getCustomFields($this->config->get('config_customer_group_id'));

				if(strpos(VERSION, '2.1.') === false && strpos(VERSION, '2.2.') === false){
					$customer_info['account_custom_field'] = unserialize($customer_info['custom_field']);
				} else {
					$customer_info['account_custom_field'] = json_decode($customer_info['custom_field'], true);
				}

				unset($customer_info['custom_field']);

				$json['data'] = $customer_info;
			}

		}else {
			$json["error"]		= "Only POST request method allowed";
			$json["success"]	= false;
		}

		$this->sendResponse($json);
	}

	/*
	* Login user
	*/
	public function sociallogin() {

		$this->checkPlugin();

		$json = array('success' => true);
		$customer_info = array();
		$firstname = "";
		$lastname = "";
		if ( $_SERVER['REQUEST_METHOD'] === 'POST' ){

			$input = file_get_contents('php://input');

			$post = json_decode($input, true);

			$this->language->load('checkout/checkout');

			if ($this->customer->isLogged()) {
				$json['error']		= "User already is logged";
				$json['success']	= false;
			}

			if ($json['success']) {
				$this->load->model('account/customer');

				$customer_info = $this->model_account_customer->getCustomerByEmail($post['email']);

				if(!isset($post['provider']) || ($post['provider'] != 'facebook' && $post['provider'] != 'google')) {
					$json['error']		= "Invalid social provider";
					$json['success']	= false;
				}

				if(isset($post['provider']) && ($post['provider'] == 'facebook' || $post['provider'] == 'google')) {
					$social = $this->requestUserDataFromProvider($post['provider'], $post['access_token']);

					if (empty($social) || $social['email'] != $post['email']) {
						$json['error'] = "Social email and posted email mismatch";
						$json['success'] = false;
					} else {
						if(isset($social['name'])){
							$exploded = explode(' ', $social['name']);
							$firstname = array_shift($exploded);
							$lastname= implode(' ', $exploded);
						}
					}
				}

				if($json['success']) {
					//if email does not exist, register as a new customer
					if (!$customer_info) {
						$data['email'] = $post['email'];
						$data['firstname'] = $firstname;
						$data['lastname'] = $lastname;
						$data['telephone'] = "";
						$data['address_1'] = "";
						$data['city'] = "";
						$data['postcode'] = "";
						$data['country'] = "";
						$data['zone_id'] = "";
						$data['fax'] = "";
						$data['company'] = "";
						$data['address_2'] = "";
						$data['country_id'] = "";
						$data['approved'] = 1;
						$data['password'] = md5(microtime());

						$this->model_account_customer->addCustomer($data);
						$customer_info = $this->model_account_customer->getCustomerByEmail($post['email']);
					}

					if ($customer_info) {
						if (!$this->customer->login($post['email'], "", true)) {
							$json['error']['warning'] = $this->language->get('error_login');
							$json['success'] = false;
						}
					}

					if ($customer_info && !$customer_info['approved']) {
						$json['error']['warning'] = $this->language->get('error_approved');
						$json['success'] = false;
					}
				}
			}

			if ($json['success'] && !empty($customer_info)) {
				unset($this->session->data['guest']);

				// Default Addresses
				$this->load->model('account/address');

				if ($this->config->get('config_tax_customer') == 'payment') {
					$this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
				}

				if ($this->config->get('config_tax_customer') == 'shipping') {
					$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
				}

				// Add to activity log
				$this->load->model('account/activity');

				$activity_data = array(
					'customer_id' => $this->customer->getId(),
					'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
				);

				$this->model_account_activity->addActivity('login', $activity_data);

				unset($customer_info['password']);
				unset($customer_info['token']);
				unset($customer_info['salt']);

				$customer_info["session"] = session_id();

				// Custom Fields
				$this->load->model('account/custom_field');

				$customer_info['custom_fields'] = $this->model_account_custom_field->getCustomFields($this->config->get('config_customer_group_id'));

				if(strpos(VERSION, '2.1.') === false && strpos(VERSION, '2.2.') === false){
					$customer_info['account_custom_field'] = unserialize($customer_info['custom_field']);
				} else {
					$customer_info['account_custom_field'] = json_decode($customer_info['custom_field'], true);
				}

				unset($customer_info['custom_field']);

				$json['data'] = $customer_info;
			}

		}else {
			$json["error"]		= "Only POST request method allowed";
			$json["success"]	= false;
		}

		$this->sendResponse($json);
	}

	private function requestUserDataFromProvider($provider, $access_token) {
		$ch = curl_init();
		if($provider == 'facebook'){
			curl_setopt($ch, CURLOPT_URL, static::FACEBOOK_USER_INFORMATION_URL);
			$headers = array("Authorization: Bearer " . $access_token);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		} elseif ($provider == 'google') {
			curl_setopt($ch, CURLOPT_URL, static::GOOGLE_USER_INFORMATION_URL.$access_token);
		}
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);

		// execute the CURL request.
		$data = curl_exec($ch);
		if(!curl_errno($ch)){
			curl_close($ch);
			return json_decode($data, true);
		} else {
			curl_close($ch);
			return NULL;
		}
	}
}