<?php
/**
 * store_admin.php
 *
 * Store management
 *
 * @author     Makai Lajos
 * @copyright  2015
 * @license    License.txt
 * @version    1.0
 * @link       http://opencart-api.com/product/opencart-rest-admin-api/
 * @see        http://webshop.opencart-api.com/rest-admin-api/
 */
require_once(DIR_SYSTEM . 'engine/restadmincontroller.php');

class ControllerRestStoreAdmin extends RestAdminController {

    /*
    * Get stores
    */
    public function listStore() {

        $json = array('success' => false);

        $this->load->language('restapi/store');
        $this->load->model('rest/restadmin');

        $data['stores'] = array();

        $data['stores'][] = array(
            'store_id' => 0,
            'name'     => $this->config->get('config_name'),
            'url'      => (strpos(VERSION, '2.2.') === false) ? HTTP_CATALOG : HTTP_SERVER
        );

        $store_total = $this->model_rest_restadmin->getTotalStores();

        $results = $this->model_rest_restadmin->getStores();

        foreach ($results as $result) {
            $data['stores'][] = array(
                'store_id' => $result['store_id'],
                'name'     => $result['name'],
                'url'      => $result['url']
            );
        }


        $json['success'] = true;
        $json['data'] = $data['stores'];

        $this->sendResponse($json);
    }

    /*
    * Get store
    */
    public function getStore($id) {

        $json = array('success' => true);

        $this->load->model('rest/restadmin');
        $store_info = $this->model_rest_restadmin->getSetting('config', $id);
        if(!empty($store_info)){
            $json["data"] = $store_info;
        } else {
            $json['error'] = 'Store does not exist.';
            $json["success"] = false;
        }

        $this->sendResponse($json);
    }

    /*
    * Copy store
    */
    public function copyStore($id, $post=array()) {

        $json = array('success' => true);

        $this->load->model('rest/restadmin');
        $store_info = $this->model_rest_restadmin->getSetting('config', $id);
        if(!empty($store_info)){
            if(!empty($post)){
                $this->convertData($store_info);
                $store_info = array_merge($store_info, $post);
            }

            $store_id = $this->model_rest_restadmin->addStore($store_info);
            $this->model_rest_restadmin->editSetting('config', $store_info, $store_id);
            $json["data"] = $store_id;
        } else {
            $json['error'] = 'Store does not exist.';
            $json["success"] = false;
        }

        $this->sendResponse($json);
    }

    /*
    * Edit store
    */
    public function editStore($id, $post) {

        $json = array('success' => true);

        $this->load->model('rest/restadmin');
        $store_info = $this->model_rest_restadmin->getSetting('config', $id);
        if(!empty($store_info)){

            if(!empty($post)){
                $this->convertData($store_info);
                $store_info = array_merge($store_info, $post);
            }

            if($id != 0){
                $this->model_rest_restadmin->editStore($id, $store_info);
            }

            $this->model_rest_restadmin->editSetting('config', $store_info, $id);

        } else {
            $json['error'] = 'Store does not exist.';
            $json["success"] = false;
        }

        $this->sendResponse($json);
    }

    private function convertData(&$store_info){
        if(strpos(VERSION, '2.1.') === false && strpos(VERSION, '2.0.3.1') === false  && strpos(VERSION, '2.2.') === false){
            $store_info['open'] = $store_info['config_open'];
            $store_info['comment'] = $store_info['config_comment'];
        }
    }

    /*
    * STORE FUNCTIONS
    * index.php?route=rest/store_admin/store
    */
    public function store() {

        $this->checkPlugin();

        if ( $_SERVER['REQUEST_METHOD'] === 'GET' ){
            if (isset($this->request->get['id']) && ctype_digit($this->request->get['id'])) {
                $this->getStore($this->request->get['id']);
            }else {
                $this->listStore();
            }

        } else if ( $_SERVER['REQUEST_METHOD'] === 'POST' ){
            $requestjson = file_get_contents('php://input');

            $requestjson = json_decode($requestjson, true);
            if (isset($this->request->get['id']) && ctype_digit($this->request->get['id'])) {
                $this->copyStore($this->request->get['id'], $requestjson);
            }else {
                $this->sendResponse(array('success' => false));
            }
        } else if ( $_SERVER['REQUEST_METHOD'] === 'PUT' ){
            $requestjson = file_get_contents('php://input');
            $requestjson = json_decode($requestjson, true);

            if (isset($this->request->get['id']) && ctype_digit($this->request->get['id'])
                && !empty($requestjson)) {
                $this->editStore($this->request->get['id'], $requestjson);
            }else {
                $this->sendResponse(array('success' => false));
            }
        }
    }
}