<?php
$_['addist_error_header']          = 'Some serious problems have been detected:';
$_['addist_error_footer']          = 'For removing this message, please, solve the problems or deactivate modifications of above extensions(vqmod/ocmod).';

$_['addist_error_vqmod']           = 'The extension requires "vQmod" to be installed(min version: 2.4). If you need help, please, contact us by email dev@addist.ru !';
$_['addist_error_vqmod_version']   = 'Please, update your "vQmod" extension to newer one(min version: 2.4). If you need help, please, contact us by email dev@addist.ru !';
?>