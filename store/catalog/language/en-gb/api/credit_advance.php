<?php
// Text
$_['text_success']         = 'Success: Your store credit has been applied!';

// Error
$_['error_permission']     = 'Warning: You do not have permission to access the API!';
$_['error_credit_advance'] = 'Warning: Please enter the amount of store credit to use!';
$_['error_balance']        = 'Warning: You don\'t have %s store credit!';
$_['error_maximum']        = 'Warning: The maximum store credit that can be applied is %s!';
