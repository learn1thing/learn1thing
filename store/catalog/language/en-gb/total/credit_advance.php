<?php
// Heading
$_['heading_title']         = 'Use Store Credit (Available %s)';

// Text
$_['text_credit_advance']   = 'Store Credit';
$_['text_order_id']         = 'Order ID: #%s';
$_['text_success']          = 'Success: Your store credit has been applied!';

// Entry
$_['entry_credit_advance']  = 'Store credit to use (Max %s)';

// Error
$_['error_credit_advance']  = 'Warning: Please enter the amount of store credit to use!';
$_['error_balance']         = 'Warning: You don\'t have %s store credit!';
$_['error_maximum']         = 'Warning: The maximum store credit that can be applied is %s!';
