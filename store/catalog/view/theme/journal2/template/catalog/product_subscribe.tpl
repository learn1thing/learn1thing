<?php echo $header; ?>
<div class="container">
	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	</ul>
	<?php if ($attention) { ?>
		<div class="alert alert-info">
			<i class="fa fa-info-circle"></i> <?php echo $attention; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
	<?php } ?>
	<?php if ($success) { ?>
		<div class="alert alert-success">
			<i class="fa fa-check-circle"></i> <?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
	<?php } ?>
	<?php if ($error_warning) { ?>
		<div class="alert alert-danger">
			<i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
	<?php } ?>
	<div class="row">
		<?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
			<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
			<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
			<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		<div id="content" class="<?php echo $class; ?>">
			<?php echo $content_top; ?>
			<h1><?php echo $heading_title; ?></h1>
			<div class="table-responsive">
				<table class="table table-bordered">
					<thead>
						<tr>
							<td class="text-left"><?php echo $column_name; ?></td>
							<td class="text-left"><?php echo $column_model; ?></td>
							<td class="text-left"><?php echo $column_description; ?></td>
							<td class="text-center"><?php echo $column_effective_date; ?></td>
							<td class="text-right"><?php echo $column_price; ?></td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="text-left"><?php echo $name; ?></td>
							<td class="text-left"><?php echo $model; ?></td>
							<td class="text-left"><?php echo $description; ?></td>
							<td class="text-center"><?php echo $effective_date; ?></td>
							<td class="text-right"><?php echo $price; ?></td>
						</tr>
					</tbody>
					<tfoot>
						<?php foreach ($totals as $total) { ?>
							<tr>
								<td class="text-right" colspan="4"><?php echo $total['title']; ?></td>
								<td class="text-right"><?php echo $total['text']; ?></td>
							</tr>
						<?php } ?>
					</tfoot>
				</table>
			</div>
			<div class="buttons">
				<div class="pull-right"><a href="<?php echo $checkout; ?>" class="btn btn-primary"><?php echo $button_checkout; ?></a></div>
			</div>
		</div>
	</div>
	<?php echo $content_bottom; ?>
	<?php echo $column_right; ?>
</div>
<?php echo $footer; ?>