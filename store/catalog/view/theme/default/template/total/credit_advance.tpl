<div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title"><a href="#collapse-credit-advance" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"><?php echo $heading_title; ?> <i class="fa fa-caret-down"></i></a></h4>
  </div>
  <div id="collapse-credit-advance" class="panel-collapse collapse">
    <div class="panel-body">
      <label class="col-sm-2 control-label" for="input-credit-advance"><?php echo $entry_credit_advance; ?></label>
      <div class="input-group">
        <input type="text" name="credit_advance" value="<?php echo $credit_advance; ?>" placeholder="<?php echo $entry_credit_advance; ?>" id="input-credit-advance" class="form-control" />
        <span class="input-group-btn">
        <input type="submit" value="<?php echo $button_credit_advance; ?>" id="button-credit-advance" data-loading-text="<?php echo $text_loading; ?>"  class="btn btn-primary" />
        </span></div>
      <script type="text/javascript"><!--
$('#button-credit-advance').on('click', function() {
	$.ajax({
		url: 'index.php?route=total/credit_advance/credit_advance',
		type: 'post',
		data: 'credit_advance=' + encodeURIComponent($('input[name=\'credit_advance\']').val()),
		dataType: 'json',
		beforeSend: function() {
			$('#button-credit-advance').button('loading');
		},
		complete: function() {
			$('#button-credit-advance').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('.breadcrumb').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('html, body').animate({ scrollTop: 0 }, 'slow');
			}

			if (json['redirect']) {
				location = json['redirect'];
			}
		}
	});
});
//--></script>
    </div>
  </div>
</div>
