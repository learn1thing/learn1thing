<?php
require __DIR__. '/vendor/autoload.php';

require_once('lib/curl.php');

use GuzzleHttp\Client;

// $client = new Client(['defaults' => ['verify' => false]]);

// $res=$client->request('GET', 'https://store.learn1thing.com/course/auth/userkey/logout.php');

// print_r($res);

$client = new Client();

$cert_file='/var/www/html/cacert.pem';

 // create curl resource 
$ch = curl_init(); 

// set url 
curl_setopt($ch, CURLOPT_URL, "https://store.learn1thing.com/course/login/logout.php?sesskey=esmD7ngjG6"); 

//return the transfer as a string 
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
curl_setopt($ch, CURLOPT_CAINFO, $cert_file);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);

// $output contains the output string 
$output = curl_exec($ch); 

// close curl resource to free up system resources 
curl_close($ch);     

print_r($output);
