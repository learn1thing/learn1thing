<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Log Keeper
        \MathiasGrimm\LaravelLogKeeper\Commands\LogKeeper::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('queue:work --tries=2')
//             ->everyMinute();
->everyFiveMinutes();
             // ->withoutOverlapping();

        $schedule->command('queue:flush')
            ->hourly();
        $schedule->command('queue:restart')
            ->hourly();

        $schedule->command('laravel-log-keeper')->daily();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
