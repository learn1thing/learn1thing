<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoachCourse extends Model
{
    protected $table = 'coach_courses';

    /**
     * The attributes that are mass assignable.
     * @author ali
     * @var array
     */
    protected $fillable = [
        'name', 'user_id','description', 'created_at', 'updated_at'
    ];

    public function scheduleRelated()
    {
        return $this->belongsTo(CoachSchedule::class);
    }

}
