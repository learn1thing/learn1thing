<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use GuzzleHttp\Client;

class Workflow extends Model
{
    protected $table = 'workflow';

    protected $fillable = [
    'product_category_id',
    'product_id',
    'action',
    'action_template',
    'recipient_role',
    'recipient_user_id',
    'recipient_user',
    'recipient_department',
    'offset',
    'event_trigger',
    ];

    /**
     * Datatables
     *
     * @return   array
     */
    
    public static function datatables()
    {
        return static::select( 
            'product_category_id',
            'product_id',
            'action',
            'action_template',
            'recipient_role',
            'recipient_user_id',
            'recipient_user',
            'recipient_department',
            'offset',
            'event_trigger',
            'id',
            'created_at'
            );
    }

    /**
     * Gets the category.
     *
     * @return    array
     */
    public static function getCategory()
    {
        $client = new Client();
        $categories = $client->request('GET', env('OC_BASE_URL').'/index.php?route=rest/category_admin/category', [
            'headers' => [
            'X-Oc-Restadmin-Id' => '12345',
            ]
            ]);
        $categories = json_decode($categories->getBody());
        $data = [];
        foreach ($categories->data->categories as $category) {
            $result = array(
                'id' => $category->category_id,
                'name' => $category->name
                ); 
            array_push($data, $result);
        }

        return collect( $data );

    }

    public static function getProduct( $id )
    {
        $client = new Client();
        $getproduct = $client->request('GET', env('OC_BASE_URL').'/index.php?route=rest/product_admin/products&category='.$id, [
            'headers' => [
            'X-Oc-Restadmin-Id' => '12345',
            ]
            ]);
        $products = json_decode($getproduct->getBody());
        $data = [];
        foreach ($products->data as $product) {
            $result = array(
                'id' => $product->id,
                'name' => $product->product_description->{1}->name
                );
            array_push($data, $result);
        }

        return json_encode($data);
    }

    public static function getAction()
    {
        $data = array(
            array( 'id' => 1, 'name' => 'All' ),
            array( 'id' => 2, 'name' => 'Email' ),
            array( 'id' => 3, 'name' => 'SMS' ),
            );
        
        
        return collect( $data );
    }

    public static function getRecipient()
    {
        $data = array(
            array( 'id' => 'business-partner', 'name' => 'Partner' ),
            array( 'id' => 'corporate-customer', 'name' => 'Corporate' ),
            array( 'id' => 'standard-customer', 'name' => 'Consumer' ),
            );

        return collect( $data );
    }

    public static function getUser( $slug )
    {
        $user = User::join('role_users','users.id','=','role_users.user_id')
        ->join('roles','role_users.role_id','=','roles.id')
        ->join('activations','activations.user_id','=','users.id')
        ->select('users.id','users.first_name','users.last_name','activations.completed','users.email','users.created_at','users.related_id','users.related_type')
        ->where('roles.slug','=',$slug)->get();
        
        $data = [];
        foreach ($user as $value) {
            $result = array(
                'id' => $value->id,
                'name' => $value->first_name.' '.$value->last_name
                );
            array_push($data, $result);
        }

        return $data;
    }

    public static function getUserById( $id )
    {
        $user = User::join('role_users','users.id','=','role_users.user_id')
        ->join('roles','role_users.role_id','=','roles.id')
        ->join('activations','activations.user_id','=','users.id')
        ->select('users.id','users.first_name','users.last_name','activations.completed','users.email','users.created_at','users.related_id','users.related_type')
        ->where('users.id','=',$id)->get();
        
        $data = [];
        foreach ($user as $value) {
            $result = array(
                'id' => $value->id,
                'name' => $value->first_name.' '.$value->last_name
                );
            array_push($data, $result);
        }

        return $data;
    }

    /**
     * Event Trigger
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public static function eventTrigger()
    {

        $data = array(
            //Middleware
            array( 'slug' => 'changePassword', 'name' => 'After change password' ),
            array( 'slug' => 'updateProfile', 'name' => 'After Update Profile' ),
            array( 'slug' => 'storeBulk', 'name' => 'After Bulk User' ),
            array( 'slug' => remove_space('bulk product'), 'name' => 'After bulk product' ),
            array( 'slug' => remove_space('create user'), 'name' => 'After create user' ),
            array( 'slug' => remove_space('change user role'), 'name' => 'After change user role' ),
            array( 'slug' => remove_space('request product'), 'name' => 'After request product' ),
            array( 'slug' => remove_space('bulk order notif Admin'), 'name' => 'After bulk order notif Admin' ),
            );

        return $data;
    }

    public static function getUserAccess()
    {
        $data = array(
            array( 'id' => 1, 'name' => 'User Department' ),
            array( 'id' => 2, 'name' => 'User Role' )
            );

        return collect( $data );
    }

    public static function getSchedule()
    {
        $data = array(
            array( 'slug' => 'daily', 'name' => 'Daily' ),
            array( 'slug' => 'week', 'name' => 'Week' ),
            array( 'slug' => 'month', 'name' => 'Month' )
            );

        return collect( $data );
    }

    // Relation
    
    /**
     * { function_description }
     *
     * @return     <type>  ( description_of_the_return_value )
     */

    public function product()
    {
        return $this->belongsTo( Product::class, 'product_id', 'product_code');
    }

    public function recipient()
    {
        return $this->belongsTo( User::class, 'recipient_user_id', 'id' );
    }

    public function email_template()
    {
        return $this->belongsTo( EmailTemplate::class, 'action_template', 'id' );
    }
    
}
