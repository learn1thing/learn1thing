<?php

namespace App\Models;

use App\Models\CoachCourse;
use Illuminate\Database\Eloquent\Model;

class CoachSchedule extends Model
{
    protected $table = 'coach_schedules';

    /**
     * The attributes that are mass assignable.
     * @author ali
     * @var array
     */
    protected $fillable = [
        'id', 'user_id', 'course_id', 'coach_id', 'schedule_date', 'start_time', 'end_time', 'location', 'comments', 'choice', 'devine_slots', 'break_slots', 'overlap_slots', 'multiple_allow', 'display_appointment', 'email_reminder', 'status', 'schedule_id','start_date','end_date',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function courseRelated()
    {
        return $this->hasOne(CoachCourse::class,'id','course_id');
    }

    public function coach()
    {
        return $this->belongsTo('App\Models\Coach','coach_id');
    }

    public function coach_schedule_order()
    {
        return $this->hasOne('App\Models\CoachScheduleOrder','coach_schedule_id');
    }

    public function schedule()
    {
        return $this->belongsTo('App\Models\Schedule','schedule_id');
    }

    public function saveData($request)
    {
        $schedule_date = date("Y-m-d", strtotime($request->start));
        $start_time = date("H:i:s", strtotime($request->start));
        $end_time = date("H:i:s", strtotime($request->end));
        $start_date = date("Y-m-d", strtotime($request->start));
        $end_date = date("Y-m-d", strtotime($request->end));

        $dataInput = [
                        'coach_id'   => $request->coach_id,
                        'start_date' => $start_date,
                        'schedule_date' => $start_date,
                        'end_date'   => $end_date,
                        'start_time' => $start_time,
                        'end_time'   => $end_time,
                        'schedule_id'=> $request->schedule_id
                    ];

        $schedule = CoachSchedule::where($dataInput)->first();
        if (!$schedule) {
            $schedule = new CoachSchedule();
            $schedule->coach_id         = $request->coach_id;
            $schedule->start_date       = $start_date;
            $schedule->schedule_date    = $start_date;
            $schedule->end_date         = $end_date;
            $schedule->start_time       = $start_time;
            $schedule->end_time         = $end_time;
            $schedule->schedule_id      = $request->schedule_id;
            $schedule->save();
        } else {
            $schedule->delete();
        }

        if ($schedule) {
            return true;
        } else {
            return false;
        }
    }
}
