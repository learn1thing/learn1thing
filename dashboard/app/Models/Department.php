<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'departments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'user_id', 'parent_id', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function users()
    {
        return $this->hasMany('App\Models\User','department_id','id');
    }

    public function myDepartmentsDatatable($user_id=null)
    {
        \DB::statement(\DB::raw('set @row_number:=0'));
        $query =  $this->leftJoin('users','users.id','=','departments.user_id')
            ->select([
                \DB::raw('(@row_number:=@row_number + 1) as number'),
                'departments.id',
                'departments.name',
                'departments.description',
                'departments.user_id',
                'users.email as owner',
                'departments.parent_id as department_parent',
                'departments.created_at',
                'departments.updated_at'
            ]);
        if ($user_id) {
            if (user_info('parent_id')) {
                $query = $query
                    ->where(function($query) {
                        $query->where('user_id',user_info('id'))
                            ->orWhere('user_id',user_info('parent_id'));
                    });
            } else {
                $childUser = User::where('parent_id',user_info('id'))->pluck('id')->toArray();
                $query = $query
                    ->where(function($query) use($childUser) {
                        $query->where('user_id',user_info('id'))
                            ->orWhereIn('user_id',$childUser);
                    });
                    
            }
        }

        return $query;
    }

    public function getDepartmentByUserId($user_id)
    {
        $results = $this->where('user_id',$user_id);
        if (user_info('parent_id')) {
            $results = $results->orWhere('user_id',user_info('parent_id'));
        }
        return $results;
    }

    public function getDepartment()
    {
        if (user_info('parent_id')) {
            $departments = $this
                ->where(function($query) {
                    $query->where('user_id',user_info('id'))
                        ->orWhere('user_id',user_info('parent_id'));
                });
        } else {
            $childUser = User::where('parent_id',user_info('id'))->pluck('id')->toArray();
            $departments = $this
                ->where(function($query) use($childUser) {
                    $query->where('user_id',user_info('id'))
                        ->orWhereIn('user_id',$childUser);
                });
                
        }

        return $departments;
    }

    public function childs()
    {
        return $this->hasMany(Department::class,'parent_id','id');
    }

    public function parent()
    {
        return $this->belongsTo(Department::class,'parent_id');
    }

    public static function boot()
    {
        parent::boot();

        Department::deleting(function($department) {
            if ($department->childs->count() > 0) {
                foreach ($department->childs as $child) {
                    $child->delete();
                }
            }
        });
    }

    public static function getDataDepartment()
    {
        $parents = static::select( 'id', 'name', 'parent_id')->get();
        $html = '';

        foreach ($parents as $parent) {
            $class = $parent->parent_id == null ? 'class="optionGroup"' : '';

            $html .= '<option value="'. $parent->id .'" '. $class .' >'. $parent->name .'</option>';    
        }

        return $html;
    }
}
