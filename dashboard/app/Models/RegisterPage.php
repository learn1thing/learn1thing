<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegisterPage extends Model
{
    protected $table = 'register_pages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'image', 'created_at', 'updated_at'
    ];
}
