<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;

class Product extends Model
{
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'product_code', 'name', 'image', 'model', 'price', 'quantity', 'status','type', 'time_limit', 'created_at', 'updated_at', 'mini_store_status', 'session','credit', 'min_quota'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function productSets()
    {
        return $this->hasMany('App\Models\ProductSet','product_id','product_code');
    }

    public function findProduct($productCode)
    {
        return $this->where('product_code',$productCode)->first();
    }

    public function orders()
    {
        return $this->hasMany('App\Models\OrderDetail','product_code','product_code');
    }

    public function productQuota()
    {
        return $this->hasOne('App\Models\ProductQuota','product_code','product_code');
    }

    public static function boot()
    {
        parent::boot();

        Product::deleting(function($product) {
            $client = new Client();
            $response = $client->request('delete', env('OC_BASE_URL').'/index.php?route=rest/product_admin/products&id='.$product->product_code, [
                'headers' => [
                    'X-Oc-Restadmin-Id' => '12345',
                ],
            ]);

            if ($product->image) {
                delete_file($product->image);
            }

            if (count($product->productSets) > 0) {
                foreach ($product->productSets as $productSet) {
                    $productSet->delete();
                }
            }

            if (count($product->orders)) {
                foreach ($product->orders as $order) {
                    $order->delete();
                }
            }
        });
    }

    public function getProducts($type=null,$email=null)
    {
        $results = $this->join('users','users.id','=','products.user_id')
            ->where('products.status', 1)
            ->select('products.product_code as product_id','products.model','products.type');

        if ($type) {
            $results = $results->where('type',$type);
        }

        if ($email) {
            $results = $results->where('users.email',$email);
        }

        return $results->get();
    }

    public function getProductsForStore($type=null,$email=null)
    {
        $results = $this->join('users','users.id','=','products.user_id')
            ->where('products.status', 1);

        if ($type) {
            $results = $results->where('type',$type);
        }

        if ($email) {
            $results = $results->where('users.email',$email);
        }

        return $results;
    }
}
