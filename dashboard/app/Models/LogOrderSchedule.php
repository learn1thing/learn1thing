<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\CoachSchedule;
use App\Models\LogOrderScheduleTemp;

class LogOrderSchedule extends Model
{
	protected $table = 'log_order_schedules';

	public function saveData($param)
	{
		$dataSchedule = CoachSchedule::whereIn('id',$param['schedule_id'])->get();

		if (count($dataSchedule) > 0) {
			foreach ($dataSchedule as $key => $value) {
				$details['start'] = $value->start_date.' '.$value->start_time;
				$details['end'] = $value->end_date.' '.$value->end_time;
				$details['coach_id'] = $value->coach_id;
				$details['coach_schedule_id'] = $value->id;
				
				$logTemp = new LogOrderScheduleTemp();
				$logTemp->coach_schedule_id = $value->id;
				$logTemp->save();

				$resSchedule[] = $details;
			}
		}
		$log = LogOrderSchedule::where('code',$param['code'])
								->where('product_id',$param['product_id'])
								->first();
		if (!$log) {
			$log = new LogOrderSchedule();
			$log->course_id 		= $param['course_id'];
			$log->code 				= $param['code'];
			$log->product_id 		= $param['product_id'];
			$log->details 			= json_encode($resSchedule);
		} else {
			$log->course_id 		= $param['course_id'];
			$log->code 				= $param['code'];
			$log->product_id 		= $param['product_id'];
			$log->details 			= json_encode($resSchedule);
		}

		if ($log->save()) {
			return $log;
		} else {
			return false;
		}
	}
}
