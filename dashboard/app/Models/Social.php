<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    protected $table = "social_logins";

    /**
     * The attributes that are mass assignable.
     * @author ali
     * @var array
     */

    protected $fillable = [
        'provider', 'social_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
