<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderLink extends Model
{
    protected $table = 'order_links';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_code', 'link','product_code', 'created_at', 'updated_at'
    ];
}
