<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogOrderScheduleTemp extends Model
{
	protected $table = 'log_order_schedule_temps';

    protected $fillable = [ 'coach_schedule_id' ];
}
