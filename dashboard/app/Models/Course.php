<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
	protected $connection = 'mysql2';
    protected $table = 'mdl_course';

    public static function getCourseByid($id)
    {
    	$data = Course::where('id',$id)->get()->take(1);
    	if ($data) {
    		return $data;
    	} else {
    		return array();
    	}
    }

}
