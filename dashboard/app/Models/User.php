<?php

namespace App\Models;

use App\Models\CoachSchedule;
use App\Models\Social;
use App\Models\CreditHistory;
use Cartalyst\Sentinel\Users\EloquentUser as Model;
// use Illuminate\Database\Eloquent\Model;
use File;
// use Image;
use Config;
use Intervention\Image\ImageManagerStatic as Image;
use App\Classes\CurlRequest;
use App\Traits\WorkflowTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;
    
    use WorkflowTrait;

    
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'permission', 'first_name', 'last_name', 'related_id', 'related_type','parent_id','phone','fax','company','address_1','address_2','city','post_code','country','region','subscribe','image','customer_id','department_id','payment_method_id','credit','deleted_at','is_change_payment_method'
    ];

    public function related()
    {
        return $this->morphTo();
    }

    // ali
    public function socialRelated()
    {
        return $this->hasMany(Social::class);
    }

    public function scheduleRelated()
    {
        return $this->hasMany(CoachSchedule::class,'user_id')->where('actived',1);;
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product','user_id');
    }

    public function groups()
    {
        return $this->hasMany('App\Models\BusinessPartnerGroup','created_by','id');
    }

    public function groupCorporates()
    {
        return $this->hasMany('App\Models\CorporateGroup','created_by','id');
    }

    public function departments()
    {
        return $this->hasMany('App\Models\Department','user_id','id');
    }

    public function department()
    {
        return $this->belongsTo('App\Models\Department','department_id');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order','user_id','id');
    }

    public function orderHistories()
    {
        return $this->hasMany('App\Models\OrderHistory','customer_id','customer_id');
    }

    public function downlines()
    {
        return $this->hasMany('App\Models\User','parent_id','id');
    }

    public function upline()
    {
        return $this->belongsTo('App\Models\User','parent_id');
    }

    // public function coaches()
    // {
    //     return $this->hasMany('App\Models\Coach','user_id');
    // }

    public function coache()
    {
        return $this->hasOne('App\Models\Coach','user_id');
    }

    public function productQuotas()
    {
        return $this->hasMany('App\Models\ProductQuota','user_id');
    }

    public function generateEmail($nickname)
    {
        $noGenerate = (int)date('ym').(int)'1';
        $email = $nickname.$noGenerate.'@mailinator.com';
        
        $tempUserEmail = $email;
        $statusUserEmail = 'false';
        do {
            $checkUser = User::where(['email'=>$tempUserEmail])->first();
            if(count($checkUser) > 0) {
                $noGenerate +=1;
                
                $tempUserEmail = '';
                $tempUserEmail = $nickname.$noGenerate.'@mailinator.com';
                $statusUserEmail = 'false';
            } else {
                $statusUserEmail = 'true';
            }
        } while ($statusUserEmail != 'true');
        return $tempUserEmail;
    }

    public function updateUser($id,$param,$currentUser)
    {
        $findUser = User::find($id);
        $dataRelated = '';
        if ($findUser) {

            $findUser->first_name = $param['first_name'];
            $findUser->last_name = $param['last_name'];
            // $findUser->email = $param['email'];
            $findUser->subscribe = $param['subscribe'];
            $findUser->address_1 = $param['address_1'];
            $findUser->address_2 = $param['address_2'];
            $findUser->city = $param['city'];
            $findUser->post_code = $param['post_code'];
            $findUser->country = $param['country'];
            $findUser->region = $param['region'];

            if ($findUser->related_type == 'App\Models\BusinessPartner') {
                $dataRelated = BusinessPartner::firstOrNew(array('id' => $findUser->related_id));
                $dataRelated->name = $param['company_name'];
                $dataRelated->address = $param['company_address'];
                $dataRelated->no_regis = $param['company_register_no'];                
                $dataRelated->description = $param['description'];                
                // $dataRelated->logo = $param['logo'];                
                $dataRelated->save();

                if (array_key_exists('logo', $param)) {
                    $oldLogo = $dataRelated->logo;
                    $pathDest = Config::get('services.public_path.logo.pattern_path');

                    if (!empty($logo)) {
                        delete_image($pathDest.'/'.$oldImage, $type = 'public');
                    }

                    $logo = upload_image($param['logo'],$pathDest)['original'];
                    $dataRelated->logo = $logo;
                    $dataRelated->save();
                }

                if (array_key_exists('banner', $param)) {
                    $oldLogo = $dataRelated->banner;
                    $pathDest = Config::get('services.public_path.banner.pattern_path');

                    if (!empty($banner)) {
                        delete_image($pathDest.'/'.$oldImage, $type = 'public');
                    }

                    $banner = upload_image($param['banner'],$pathDest)['original'];
                    $dataRelated->banner = $banner;
                    $dataRelated->save();
                }
            
            }

            if ($findUser->related_type == 'App\Models\CorporateCustomer') {
                $dataRelated = CorporateCustomer::firstOrNew(array('id' => $findUser->related_id));
                $dataRelated->no_regis = $param['company_register_no'];                
                $dataRelated->name = $param['company_name'];
                $dataRelated->address = $param['company_address'];
                $dataRelated->description = $param['description'];
                if (array_key_exists('logo', $param)) {
                    $oldLogo = $dataRelated->logo;
                    $pathDest = Config::get('services.public_path.logo.pattern_path');

                    if (!empty($logo)) {
                        delete_image($pathDest.'/'.$oldImage, $type = 'public');
                    }

                    $logo = upload_image($param['logo'],$pathDest)['original'];
                    $dataRelated->logo = $logo;
                    $dataRelated->save();
                }
                $dataRelated->save();
            }

            if ($findUser->related_type == 'App\Models\StandardCustomer') {
                $dataRelated = CorporateCustomer::firstOrNew(array('id' => $findUser->related_id));
                $dataRelated->name = $param['first_name'];
                $dataRelated->address = $param['address_1'];
            }

            if (user_info()->inRole('corporate-customer')) {
                
                if ($findUser->payment_method_id != $param['payment_method']) {
                    if (@user_info('payment_method')->name) {
                        if (!user_info('is_change_payment_method')) {
                            $findUser->is_change_payment_method = $param['is_change_payment_method'];
                        } else {
                            
                        }
                        //Added by puw on 15-01-2018
                        $findUser->payment_method_id = $param['payment_method'];
                    } else {
                        $findUser->credit = 0;
                        $findUser->payment_method_id = $param['payment_method'];
                    }
                }
                
            }

            $findUser->save();

            if(array_key_exists('image', $param)) {
                $pathDest = Config::get('services.public_path.image.pattern_path');
                $oldImage = $findUser->image;
                
                if (!empty($oldImage)) {
                    delete_image($pathDest.'/'.$oldImage, $type = 'public');
                }
                $image = upload_image($param['image'],$pathDest)['original'];
                $findUser->update(['image'=>$image]);

            }

            if($findUser->save()){
                return $findUser;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function updateOcUser($request)
    {
        $url = env('OC_BASE_URL').'/index.php?route=rest/account/account';
        $methode = 'POST';
        $body = [
            'firstname' => $request->first_name,
            'lastname' => (empty($request->last_name) ? 'Last name' : $request->last_name),
            'email' => $request->email,
            'telephone' => (empty($request->telephone) ? '123' : $request->telephone),
            'fax' => (empty($request->fax) ? '123' : $request->fax),
            'address_1' => (empty($request->address_1) ? 'Address_1' : $request->address_1),
            'address_2' => (empty($request->address_2) ? 'Address_2' : $request->address_2),
            'city' => (empty($request->city) ? 'City' : $request->city),
            'postcode' => (empty($request->post_code) ? '123' : $request->post_code),
            'country_id' => (empty($request->country) ? '1' : $request->country),
            'zone_id' => (empty($request->region) ? '1' : $request->region),
            'company' => (empty($request->company_name) ? 'Company Name' : $request->company_name),
        ];

        $responses = CurlRequest::sendRequest($methode, $url,null,json_encode($body));
        if (property_exists($responses, "success") && empty($responses->success)) {
            $return = array('status'=>'error','message'=>'Update account has been failed');
        } else {
            $return = array('status'=>'success','message'=>'Update account has been successfully');
        }
        return $return;
    }

    public function linkAccounts()
    {
        return $this->hasMany('App\Models\LogRole','username','email');
    }

    public static function boot()
    {
        parent::boot();

        User::deleting(function($user) {
            if (@$user->related_type != 'App\Models\BusinessPartnerGroup' && @$user->related_type != 'App\Models\CorporateGroup') {
                if ($user->related) {
                    $user->related->delete();
                }
            }

            if (count($user->socialRelated) > 0) {
                foreach ($user->socialRelated as $socialRelated) {
                    $socialRelated->delete();
                }
            }

            if (count($user->products) > 0) {
                foreach ($user->products as $product) {
                    $product->delete();
                }
            }

            if (count($user->downlines) > 0) {
                foreach ($user->downlines as $downline) {
                    $downline->forceDelete();
                }
            }

            if (count($user->log_roles) > 0) {
                foreach ($user->log_roles as $log_role) {
                    $log_role->delete();
                }
            }

            if (count($user->groups) > 0) {
                foreach ($user->groups as $group) {
                    $group->delete();
                }
            }

            if (count($user->groupCorporates) > 0) {
                foreach ($user->groupCorporates as $group) {
                    $group->delete();
                }
            }

            if (count($user->departments) > 0) {
                foreach ($user->departments as $department) {
                    $department->delete();
                }
            }

            if (count($user->productQuotas) > 0) {
                foreach ($user->productQuotas as $productQuota) {
                    $productQuota->delete();
                }
            }

            if (count($user->linkAccounts) > 0) {
                foreach ($user->linkAccounts as $linkAccount) {
                    $linkAccount->delete();
                }
            }

            if ($user->coache) {
                $user->coache->delete();
            }

            // if (count($user->orderHistories) > 0) {
            //     foreach ($user->orderHistories as $orderHistory) {
            //         $orderHistory->delete();
            //     }
            // }

            // delete user Moodle & OC
            $modelUser = new User;
            $userMoodle = $modelUser->getUserInfoMoodle($user->email);
            if (@$userMoodle[0]) {
                $data['userids'][0] = $userMoodle[0]->id;
                $deleteUserMoodle = CurlRequest::requestMoodle('POST', 'core_user_delete_users', $data,env('MOODLE_TOKEN_DELETE_USER'));
                info('delete user moodle: '.json_encode($deleteUserMoodle));
            }
            
            // delete user OC
            if (@$user->customer_id) {
                $deleteUserOC = CurlRequest::sendRequest("DELETE", env('OC_BASE_URL').'/index.php?route=rest/customer_admin/customers',null,json_encode(['customers' => [$user->customer_id]]));
                info('delete user oc: '.json_encode($deleteUserOC));

                // delete order history OC
                // $getUserOrders = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/order_admin/userorders&user='.$user->customer_id);
                // if ($getUserOrders->success) {
                //     foreach ($getUserOrders->data as $order) {
                //         info('delete order: '.json_encode($order->order_id));
                //         $deleteOrderHistoryOC = CurlRequest::sendRequest("DELETE", env('OC_BASE_URL').'/index.php?route=rest/order_admin/orders&id='.$order->order_id,null,null);
                //         info('delete order status: '.json_encode($deleteOrderHistoryOC));
                //     }
                // }
            }

            // delete activations, role_users, persistence, throttle
            \DB::table('activations')->where('user_id',$user->id)->delete();
            \DB::table('role_users')->where('user_id',$user->id)->delete();
            \DB::table('persistences')->where('user_id',$user->id)->delete();
            \DB::table('throttle')->where('user_id',$user->id)->delete();
        });
    }

    public function getUserInfoMoodle($email)
    {
        $data['values'][0] = $email;
        $data['field'] = 'email';
        $token = env('MOODLE_TOKEN_GET_USER');

        $responses = CurlRequest::requestMoodle('POST', 'core_user_get_users_by_field', $data,$token);

        return $responses;
    }

    public function updateUserMoodle($user,$moodleUserId,$type='update-profile')
    {
        if ($type == 'update-profile') {
            $data['users'][0] =[
                    'id'=>$moodleUserId,
                    'username'=>$user->email,
                    'firstname'=>$user->first_name,
                    'lastname'=>(@$user->last_name) ? $user->last_name : '',
                    'email'=>$user->email,
                    'auth'=>'manual',
                    'idnumber'=>'string',
                    'lang'=>'en',
                    'calendartype'=>'string',
                    'theme'=>'',
                    'timezone'=>'99',
                    'mailformat'=>'1',
                    'description'=>'description',
                    'city'=>'city',
                    'country'=>'country',
                    'firstnamephonetic'=>'1wertg',
                    'lastnamephonetic'=>'lastnamephonetic',
                    'middlename'=>$user->first_name,
                    'alternatename'=>$user->first_name,
                    'preferences'=>array(array('type'=>'1wertg','value'=>'1wertg')),
                    'customfields'=>array(array('type'=>'1wertg','value'=>'1wertg')),
                  ];
        } else {
            $data['users'][0] =[
                    'id'=>$moodleUserId,
                    'firstname'=> @$user->first_name,
                    'lastname'=> (@$user->last_name) ? $user->last_name : '',
                    'password'=>$user->new_password,
                    'auth'=>'manual',
                    'idnumber'=>'string',
                    'lang'=>'en',
                    'calendartype'=>'string',
                    'theme'=>'',
                    'timezone'=>'99',
                    'mailformat'=>'1',
                    'description'=>'description',
                    'city'=>'city',
                    'country'=>'country',
                    'firstnamephonetic'=>'1wertg',
                    'lastnamephonetic'=>'lastnamephonetic',
                    'middlename'=>$user->first_name,
                    'alternatename'=>$user->first_name,
                    'preferences'=>array(array('type'=>'1wertg','value'=>'1wertg')),
                    'customfields'=>array(array('type'=>'1wertg','value'=>'1wertg')),
                  ];
        }
                  
        $token = env('MOODLE_TOKEN_UPDATE_USER');
        $responses = CurlRequest::requestMoodle('POST', 'core_user_update_users', $data,$token);

        return $responses;
    }

    public function updateUserOC($request,$type='bulk')
    {
        if ($request->customer_id) {
            if ($type != 'bulk') {
                $body = json_encode([
                    "firstname" => $request->first_name,
                    "lastname" => (@$request->last_name) ? $request->last_name : '',
                    "password" => $request->password,
                    "confirm" => $request->password,
                    "newsletter" => @$request->subscribe,
                    "address" => [
                        [
                            "firstname" => $request->first_name,
                            "lastname" => (@$request->last_name) ? $request->last_name : '',
                            "address_1" => (@$request->address_1) ? $request->address_1 : '',
                            "address_2" => (@$request->address_2) ? $request->address_2 : '',
                            "city" => $request->city,
                            "country_id" => $request->country,
                            "zone_id" => $request->region,
                            "postcode" => $request->post_code,
                            "default" => "1"
                        ]
                    ]
                ]);
            } else {
                $body = json_encode([
                    "firstname" => $request->first_name,
                    "lastname" => (@$request->last_name) ? $request->last_name : '',
                    "password" => $request->password,
                    "confirm" => $request->password,
                    "telephone" => $request->phone,
                    "address" => []
                ]);
            }
            $responses = CurlRequest::sendRequest('PUT', env('OC_BASE_URL').'/index.php?route=rest/customer_admin/customers&id='.$request->customer_id,null,$body);
            if (@$responses->success ) {
                return true;
            }
        }

        return false;
    }

    public function log_roles()
    {
        return $this->hasMany( LogRole::class, 'user_id', 'id' );
    }

    public function paymentMethod()
    {
        return $this->belongsTo('App\Models\PaymentMethod');
    }

    public function getUsersByRole($role)
    {
        return $this->join('role_users','role_users.user_id','=','users.id')
            ->join('roles','roles.id','=','role_users.role_id')
            ->select('users.id','email','first_name','last_name')
            ->where('roles.slug',$role)
            ->where(function($query) {
                $query->where('users.first_name','!=',null)
                    ->orWhere('users.first_name','!=','');
            });
    }

    public function creditHistories()
    {
        return $this->hasMany('App\Models\CreditHistory','user_id','id');
    }

    public function invoices()
    {
        return $this->hasMany('App\Models\Invoice','user_id','id');
    }

    public function bulkLogs()
    {
        return $this->hasMany('App\Models\BulkLog');
    }
}
