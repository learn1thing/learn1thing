<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditHistory extends Model
{
    protected $table = 'credit_histories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id'
        , 'credit'
        , 'mutation'
        , 'activity'
        , 'created_at'
        , 'updated_at'
        , 'total_payment' // decimal (default:0)
        , 'day_expired_credit' // integer (default:0)
        , 'status' // pending or approved (default:pending)
        , 'type_created' // admin or corporate (default:admin)
        , 'date_approved'
    ];

    protected $hidden = [
      'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public static function boot()
    {
        parent::boot();

        CreditHistory::created(function($creditHistory) {
            $topUp = \Request::input('mutation');
            $user = $creditHistory->user;
            /*
            if ($user && $creditHistory->type_created == 'admin') {
                $lastCredit = $user->credit;
                //update credit user
                $user->update(['credit' => $topUp+$lastCredit]);
                info('topup success');
                info('update-credit: '.$user->credit);
            }
            */
        });
    }
}
