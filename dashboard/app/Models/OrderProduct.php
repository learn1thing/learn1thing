<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $table = 'order_products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_detail_id', 'product_code', 'link', 'status', 'created_at', 'updated_at','user_id','bbb_link'
    ];

    public function orderDetail()
    {
        return $this->belongsTo('App\Models\OrderDetail','order_detail_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_code','product_code');
    }
}
