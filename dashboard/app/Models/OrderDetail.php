<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = 'order_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','order_id', 'product_code', 'quantity', 'status', 'created_at', 'updated_at'
    ];

    public function head()
    {
        return $this->belongsTo('App\Models\Order','id','order_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_code','product_code');
    }

    public function orderProducts()
    {
        return $this->hasMany('App\Models\OrderProduct','order_detail_id');
    }
}
