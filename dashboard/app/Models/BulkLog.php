<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BulkLog extends Model
{
    protected $table = 'bulk_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'data', 'description', 'status', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function getLogRealTime($userId)
    {
        $log = $this->select('id','user_id', 'data', 'description')
            ->where('user_id',$userId)
            ->where('status',false)
            ->orderBy('id','DESC')
            ->first();

        return $log;
    }
}
