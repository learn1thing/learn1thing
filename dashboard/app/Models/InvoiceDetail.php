<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model
{
    protected $table = 'invoice_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'invoice_id', 'product_code', 'quantity', 'price', 'total', 'status', 'created_at', 'updated_at'
    ];

    protected $hidden = [
      'created_at', 'updated_at'
    ];

    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoice','invoice_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_code','product_code');
    }
}
