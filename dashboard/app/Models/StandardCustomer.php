<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StandardCustomer extends Model
{
    protected $table = 'standard_customers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'address', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->morphOne('App\Models\User','related');
    }
}
