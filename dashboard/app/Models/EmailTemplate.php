<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class EmailTemplate extends Model
{
	use Sluggable;

    protected $table = 'email_templates';

    protected $fillable = [ 
    'title', 
    'slug', 
    'subject', 
    'message' 
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public static function snippets()
    {
        $data = array(
            '{{$bp_name}}',
            '{{$corp_name}}',
            '{{$user_name}}',
            '{{$product}}',
            '{{$email}}',
            '{{$product_name}}',
            '{{$product_category}}',
            '{{$course_name}}',
            '{{$course_link}}',
            '{{$coach_name}}',
            '{{$coach_email}}'
            );

        return $data;
    }
}
