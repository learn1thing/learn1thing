<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAssign extends Model
{
    protected $table = 'product_assigns';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'business_partner_id', 'corporate_id', 'product_id','created_at', 'updated_at'
    ];
}
