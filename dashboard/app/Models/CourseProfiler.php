<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Coach;
use DB;

class CourseProfiler extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'mdl_course_profilers';


    public static function get_course_profiler( $course_id )
    {
    	$profiler = CourseProfiler::where('course_id',$course_id);
    	return $profiler;
    }

    public function getCoachByCourseId($id)
    {
        $coach_ids = [];
        $dataCoachs = array();
        
        $courseProfile = $this::where('course_id',$id)->get();
        if (count($courseProfile)) {
            foreach ($courseProfile as $key => $value) {
            	$decodeId = json_decode($value->coach_id);
                if (is_array($decodeId))  {
                    foreach ($decodeId as $coach_id) {
                        array_push($coach_ids, $coach_id);
                    }
                }   
            }
            if (count( $coach_ids) > 0) {
            	$coach_ids = array_unique($coach_ids);

            	$dataCoachs = Coach::whereIn('user_id',$coach_ids)->get();
            }

            return $dataCoachs;

        } else {
            return array();
        }
    }

}
