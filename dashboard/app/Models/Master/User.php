<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Users\EloquentUser as SentinelUser;

class User extends SentinelUser
{
    /**
     * User Relation
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function related()
    {
      return $this->morphTo();
    }
}
