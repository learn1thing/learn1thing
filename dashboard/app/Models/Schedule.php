<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Models\CoachSchedule;
use App\Models\CoachScheduleOrder;

class Schedule extends Model
{
    protected $table = "schedules";

    /**
     * The attributes that are mass assignable.
     * @author ali
     * @var array
     */

    protected $fillable = [
        'schedule_date', 'start_time', 'end_time','start_date','end_date','interval'
    ];

    public function days()
    {
        return $this->hasMany('App\Models\ScheduleDay','schedule_id');
    }

    public function saveSchedule($request,$user)
    {
        $dateRanges = date_range(Carbon::parse($request->start_date), Carbon::parse($request->end_date));
        $days = $request->day;
        $hourStart = date("H",strtotime($request->start_time));
        $hourEnd = date("H",strtotime($request->end_time));
        $start_time = date("H:i:s", strtotime($request->start_time));
        $end_time = date("H:i:s", strtotime($request->end_time));
        $resDataSchedule = [];

        $times = array();

        array_push($times, $start_time);
        $maxTime = $start_time;

        while ( $maxTime < $end_time) {
            
            $timeStamp = strtotime($maxTime)+60*$request->interval;
            $new_time = date("H:i:s",$timeStamp);

            array_push($times, $new_time);

            $maxTime = $new_time;
        }

        // $times = range($hourStart,$hourEnd,$request->interval);

        $schedule = new Schedule();
        $schedule->schedule_date    = date("Y-m-d",strtotime($request->start_date));
        $schedule->start_time       = date("H:i",strtotime($request->start_time));
        $schedule->end_time         = date("H:i",strtotime($request->end_time));
        $schedule->start_date       = date("Y-m-d",strtotime($request->start_date));
        $schedule->end_date         = date("Y-m-d",strtotime($request->end_date));
        $schedule->interval         = $request->interval;
        // $schedule->course_id        = $request->course;
        $schedule->business_partner_id = $user->related->id;
        // $schedule->course_name      = $request->course_name;
        // $schedule->type             = $request->type;
        // $schedule->details          = json_encode($resDataSchedule);
        $schedule->save();

        if ($dateRanges) {
            foreach ($days as $key => $value) {
                foreach ($dateRanges as $range) {
                    $convRange = date("Y-m-d", strtotime($range));
                    // if day in date is equl with day selected from front end
                    if ( strtolower(date("l",strtotime($convRange))) == strtolower($value) ) {
                        foreach ($times as $key => $time) {
                            if (!empty($times[$key+1])) {
                                $dataSchedule['time'] = $time;
                                $dataSchedule['schedule_id'] = $schedule->id;
                                $dataSchedule['date'] = $convRange;
                                $dataSchedule['date_time'] = $convRange.' '.$time;
                                $dataSchedule['start'] = $convRange.' '.$time;
                                $dataSchedule['end'] = $convRange.' '.$times[$key+1];
                                $dataSchedule['status'] = 'available';

                                $resDataSchedule[] = $dataSchedule;
                            }
                            
                        }
                    }
                }
            }    
        }

        $schedule->details = json_encode($resDataSchedule);
        $schedule->save();

        foreach ($request->day as $key => $value) {
            $day = new ScheduleDay();
            $day->day = $value;
            $day->schedule_id = $schedule->id;
            $day->save();
        }

        if ($schedule->id) {
            return true;
        } else {
            return false;
        }
    }

    public function setEvent($eventStart, $eventEnd, $type = 'available', $id=0, $interval=0, $eventName=null,$key=0, $count=0)
    {
        $mark = ($type == 'ordered') ? ' mark-'.date('Y-m-d',strtotime($eventStart)) : '' ;
        if ($type == 'available') {
            
            $color = '#3a87ad';
            $typeClass = 'available';
            $type = 'available';

        } else if ($type == 'available-expired') {

            $color = '#474f54';
            $typeClass = 'expired';
            $type = 'passed';

        } else if ($type == 'available-book') {

            $color = '#ff851b';
            $typeClass = 'booking';
            $type = 'available'; //override $type to available
        
        } else if ($type == 'ordered') {

            $color = '#ff851b';
            $typeClass = 'ordered';
            $type = 'available'; //override $type to available

        } else if ($type == 'ordered-expired') {

            $color = '#474f54';
            $typeClass = 'expired';
            $type = 'ordered';

        } else if ($type == 'available-book-expired') {

            $color = '#474f54';
            $typeClass = 'expired';
            $type = 'passed';

        } else if ($type == 'locked') {
            
            $typeClass = 'locked';
            $color = '#f50202';
            $type = 'booked';

        } else if ($type == 'locked-expired') {

            $typeClass = 'expired';
            $color = '#474f54';
            $type = 'booked';
        
        } else {
            $color = '#ff851b';
            $typeClass = $type;
        }

        $event = \Calendar::event(
                strtoupper($type), //title
                false, //full day
                $eventStart, //start time (you can also use Carbon instead of DateTime)
                $eventEnd, //endtime
                $id,
                [
                    'className' => 'sc-'.$typeClass. ' key-' . $key. ' tempClass'. ' date-'.date('d-m-Y',strtotime($eventStart)). ' c-'.$count . $mark, // available or booking
                    'dataInterval' => $interval,
                    'backgroundColor' => $color,
                ]
            );

        return $event;
        
    }

    public function setEventCoach($eventStart, $eventEnd, $type = 'available', $id=0, $interval=0, $eventName=null,$key=0, $count=0)
    {
        $mark = ($type == 'ordered') ? ' mark-'.date('H-i-s',strtotime($eventStart)) : '' ;
        if ($type == 'available') {
            
            $color = '#3a87ad';
            $typeClass = 'available';
            $type = 'available';

        } else if ($type == 'available-expired') {

            $color = '#474f54';
            $typeClass = 'expired';
            $type = 'passed';

        } else if ($type == 'available-book') {

            $color = '#ff851b';
            $typeClass = 'booking';
            $type = 'available for booking'; //override $type to available
        
        } else if ($type == 'ordered') {

            $color = '#ff851b';
            $typeClass = 'ordered';
            $type = 'available'; //override $type to available

        } else if ($type == 'ordered-expired') {

            $color = '#474f54';
            $typeClass = 'expired';
            $type = 'ordered';

        } else if ($type == 'available-book-expired') {

            $color = '#474f54';
            $typeClass = 'expired';
            $type = 'passed';

        } else if ($type == 'locked') {
            
            $typeClass = 'locked';
            $color = '#f50202';
            $type = 'locked';

        } else if ($type == 'locked-expired') {

            $typeClass = 'expired';
            $color = '#474f54';
            $type = 'locked';
        
        } else {
            $color = '#ff851b';
            $typeClass = $type;
        }

        $event = \Calendar::event(
                ucwords($type), //title
                false, //full day
                $eventStart, //start time (you can also use Carbon instead of DateTime)
                $eventEnd, //endtime
                $id,
                [
                    'className' => 'sc-'.$typeClass. ' key-' . $key. ' tempClass'. ' date-str-'.strtotime($eventStart),
                    'dataInterval' => $interval,
                    'backgroundColor' => $color,
                ]
            );

        return $event;
        
    }

    public function deleteById($id)
    {
        $schedule = Schedule::find($id);
        if ($schedule) {
            $schedule->delete();

            $schedule->days()->delete();

            return true;

        } else {
            return false;
        }
    }

    public function deleteScheduleByCoachId($id)
    {
        $data = CoachSchedule::where('coach_id',$id)->get();
        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                $shceduleOrder = CoachScheduleOrder::where('coach_schedule_id',$value->id)->delete();
                $value->delete();
            }

            return true;

        } else  {
            return false;
        }

    }
}
