<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductQuota extends Model
{
    protected $table = 'product_quotas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'product_code', 'min_quota', 'sold_quota', 'sold_quota_num', 'is_alert', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function product()
    {
        return $this->hasOne('App\Models\Product','product_code','product_code');
    }
}
