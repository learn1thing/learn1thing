<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'status', 'type', 'created_at', 'updated_at','payment_method_id'
    ];

    public function details()
    {
        return $this->hasMany('App\Models\OrderDetail','order_id','id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    public function myOrderDatatable($user_id=null)
    {
        \DB::statement(\DB::raw('set @row_number:=0'));
        $query = $this->join('users','users.id','=','orders.user_id')
            ->join('payment_methods','payment_methods.id','=','orders.payment_method_id')
            ->select([\DB::raw('(@row_number:=@row_number + 1) as number'),'orders.id as order_id','users.email',
                'orders.status','orders.created_at','orders.updated_at','payment_methods.name as payment_method'
            ]);

        if (@user_info('role')->slug == 'business-partner') {
            return $query->leftJoin('order_details','order_details.order_id','=','orders.id')
                ->leftJoin('products','products.product_code','=','order_details.product_code')
                ->where('products.user_id',user_info('id'))
                ->addSelect('order_details.status as status_approval')
                ->groupBy('orders.id','users.email','orders.status','orders.created_at','orders.updated_at','order_details.status','payment_methods.name');
        }

        if ($user_id) {
            $query = $query->where('orders.user_id',$user_id);
        }

        return $query;
    }

    public function detailOrder($id)
    {
        $query = $this->join('users','users.id','=','orders.user_id')
            ->join('order_details','order_details.order_id','=','orders.id')
            ->join('products','products.product_code','=','order_details.product_code')
            ->leftJoin('payment_methods','payment_methods.id','=','orders.payment_method_id')
            ->select(\DB::raw('sum(order_details.quantity) as quantity'),'products.name','products.model','order_details.price','products.credit',
                    'order_details.status','orders.created_at','orders.updated_at',\DB::raw('max(payment_methods.name) as payment_method'))
            ->groupBy('products.name','products.model','order_details.price','products.credit','order_details.status','orders.created_at','orders.updated_at')
            ->where('orders.id',$id);
        if (user_info()->inRole('business-partner')) {
            $query = $query->where('products.user_id',user_info('id'));
        }

        return $query->get();
    }

    public function getOrderDetailPending($id)
    {
        return $this->whereStatus(1)->whereId($id)->first();
    }

    public static function boot()
    {
        parent::boot();

        Order::deleting(function($order) {
            if (count($order->details) > 0) {
                foreach ($order->details as $detailOrder) {
                    $detailOrder->delete();
                }
            }
        });
    }

    public function paymentMethod()
    {
        return $this->belongsTo('App\Models\PaymentMethod','payment_method_id');
    }

    public static function scopeProductLeftToBeAssigned($query)
    {
        $results = $query->join('order_details', 'orders.id', '=', 'order_details.order_id')
            ->join('order_products', 'order_details.id', '=', 'order_products.order_detail_id')
            ->join('products', 'order_products.product_code', '=', 'products.product_code')
            ->where('orders.user_id', user_info('id'))
            ->where('order_details.status',2);
    }
}
