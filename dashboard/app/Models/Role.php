<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Cartalyst\Sentinel\Roles\EloquentRole as Model;

class Role extends Model
{
    use Sluggable;
    
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public $fillable = [
        'name', 'slug', 'permissions'
    ];

    public function hasAccess($name)
    {
        return array_has($this->permissions, $name) ? 'checked' : '';
    }

    public function role_users()
    {
        return $this->hasMany('App\Models\RoleUser');
    }

    public function restrictDelete()
    {
        return (bool) $this->role_users()->first();
    }

    public function findBySlug($slug)
    {
        return $this->where('slug',$slug)->first();
    }

    public function group()
    {
        return $this->hasOne('App\Models\BusinessPartnerGroup','business_partner_group_id');
    }

    public function customerGroup()
    {
        return $this->hasOne('App\Models\CustomerGroup','corporate_group_id');
    }
}