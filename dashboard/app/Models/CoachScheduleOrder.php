<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Coach;
use App\Models\LogOrderScheduleTemp;
use GuzzleHttp\Client;
use App\Classes\CurlRequest;

class CoachScheduleOrder extends Model
{
	protected $table = 'coach_schedule_orders';

	public function coach()
    {
        return $this->belongsTo('App\Models\Coach','coach_id');
    }

    public function coach_schedule()
    {
    	return $this->belongsTo('App\Models\CoachSchedule','coach_schedule_id');
    }

	public function saveScheduleOrder($param)
	{
		$schedule = new CoachScheduleOrder();
		$schedule->coach_schedule_id = $param->coach_schedule_id;
		$schedule->order_id = (!empty($param->order_id)?$param->order_id:0);
		$schedule->order_product_id = (!empty($param->order_product_id)?$param->order_product_id:0);
		$schedule->start_date = $param->start;
		$schedule->end_date = $param->end;
		$schedule->coach_id = $param->coach_id;
		$schedule->product_id = $param->product_id;
		$schedule->start_time = date("H:i:s", strtotime($param->start));
		$schedule->end_time = date("H:i:s", strtotime($param->end));
		
		$logOrderScheduleTemp = LogOrderScheduleTemp::where('coach_schedule_id',$param->coach_schedule_id)->get();
		if (count($logOrderScheduleTemp)>0) {
			$logOrderScheduleTemp->delete();	
		}

		if ($schedule->save()) {
			return $schedule;
		} else {
			return false;
		}
	}

	// Group by coach id
	public function getScheduleByIds($id)
	{
		$resData = array();
		$fixData = array();

		$schedule = CoachScheduleOrder::whereIn('id',$id)
			->orderBy('coach_id','ASC')
			->orderBy('coach_schedule_id','ASC')
			->get();
		
		$coach_ids = array();

		if (count($schedule) > 0) {
			foreach ($schedule as $key => $value) {
				if (!in_array($value->coach_id, $coach_ids)) {
					array_push($coach_ids, $value->coach_id);
				}
			}

			foreach ($coach_ids as $coach_id) {
				$resDataDet='';
				$resData['details'] = '';

				$resData['coach_id'] = $coach_id;

				$coach = Coach::find($coach_id);

				$resData['email'] = @$coach->user_coach->email;
				
				$dataCourse = array(
	                'moodlewsrestformat'    => 'json',
	                'field'                 => 'id',
	                'value'                 => $coach->course_id,
	            );

				$course = CurlRequest::requestMoodle('POST', 'core_course_get_courses_by_field', $dataCourse, env('TOKEN_GET_COURSE'));

				$resData['url_course'] = env('BASE_URL_MOODLE').'/course/view.php?name='.$course->courses[0]->shortname;

				foreach ($schedule as $key => $value) {
					if ($value->coach_id == $coach_id) {
						$resDataDet['id'] = $value->id;
						$resDataDet['start_date'] = $value->start_date;
						$resDataDet['end_date'] = $value->end_date;
						$resDataDet['product_id'] = $value->product_id;
						$resDataDet['start_time'] = $value->start_time;
						$resDataDet['end_time'] = $value->end_time;
						$resDataDet['timestart'] = strtotime($value->start_date.' '.$value->start_time);
						$resDataDet['timeend'] = strtotime($value->end_date.' '.$value->end_time);
				
						$resData['details'][] = $resDataDet;
					}
					
				}
				if (!empty($resData['details'])) {
					sort($resData['details']);
				}

				$fixData[] = $resData;			
			}
		}

		return $fixData;

	}

	public function getCoachScheduleOrderByEmail($email,$order_id)
	{
		$user = User::where('email',$email)->first();
		if ($user) {
			if ($user->coache) {
				$resData = array();
				$fixData = array();

				$schedule = CoachScheduleOrder::where('coach_id',$user->coache->id)
					->where('order_id',$order_id)
					->orderBy('coach_id','ASC')
					->orderBy('coach_schedule_id','ASC')
					->get();
				$coach_ids = array();

				if (count($schedule) > 0) {
					foreach ($schedule as $key => $value) {
						if (!in_array($value->coach_id, $coach_ids)) {
							array_push($coach_ids, $value->coach_id);
						}
					}

					foreach ($coach_ids as $coach_id) {
						$resDataDet='';
						$resData['details'] = '';

						$resData['coach_id'] = $coach_id;

						$coach = Coach::find($coach_id);

						$resData['email'] = @$coach->user_coach->email;

						foreach ($schedule as $key => $value) {
							if ($value->coach_id == $coach_id) {
								$resDataDet['start_date'] = $value->start_date;
								$resDataDet['end_date'] = $value->end_date;
								$resDataDet['product_id'] = $value->product_id;
								$resDataDet['start_time'] = $value->start_time;
								$resDataDet['end_time'] = $value->end_time;
								
								$resData['details'][] = $resDataDet;
							}
							
						}

						if (!empty($resData['details'])) {
							sort($resData['details']);
						}

						$fixData[] = $resData;			
					}
				}
				return $fixData;
			}
		} else {
			return array();
		}
	}

	public function updateData($param)
	{
		$dataSchedule = CoachSchedule::whereIn('id',$param['schedule_id'])->get();
		$ids = array();
		//delete current schedule order
		if (@$param['type_order'] == 'corporate') {
			self::where('order_product_id',$param['order_product_id'])
				->where('product_id',$param['product_id'])->delete();
		} else {
			self::where('order_id',$param['order_id'])
				->where('product_id',$param['product_id'])->delete();	
		}

		if (count($dataSchedule) > 0) {
			foreach ($dataSchedule as $key => $value) {

				$value->coach_schedule_id = $value->id;
				if (@$param['type_order'] == 'corporate') {
					$value->order_product_id = $param['order_product_id'];
				} else {
					$value->order_id = $param['order_id'];	
				}
				
				$value->start = $value->start_date;
				$value->end = $value->end_date;
				$value->product_id = $param['product_id'];
				$value->start = $value->start_date.' '.$value->start_time;
				$value->end = $value->end_date.' '.$value->end_time;

				$saveSchedule = $this->saveScheduleOrder($value); 

				array_push($ids, $saveSchedule->id);                           
			}
		}
		
		if ($saveSchedule) {
			$data['ids'] = $ids;
			$data['schedule'] = $saveSchedule;
			return $data;
		} else {
			return false;
		}
	}

	public function getCoachScheduleOrderByCoachId($coach_id,$order_id)
	{
		$coach = Coach::find($coach_id);
		if ($coach) {
			$resData = array();
			$fixData = array();

			$schedule = CoachScheduleOrder::where('coach_id',$coach->id)
				->where('order_id',$order_id)
				->orderBy('coach_id','ASC')
				->orderBy('coach_schedule_id','ASC')
				->get();
			$coach_ids = array();

			if (count($schedule) > 0) {
				foreach ($schedule as $key => $value) {
					if (!in_array($value->coach_id, $coach_ids)) {
						array_push($coach_ids, $value->coach_id);
					}
				}

				foreach ($coach_ids as $coach_id) {
					$resDataDet='';
					$resData['details'] = '';

					$resData['coach_id'] = $coach_id;

					$coach = Coach::find($coach_id);

					$resData['email'] = @$coach->user_coach->email;

					foreach ($schedule as $key => $value) {
						if ($value->coach_id == $coach_id) {
							$resDataDet['start_date'] = $value->start_date;
							$resDataDet['end_date'] = $value->end_date;
							$resDataDet['product_id'] = $value->product_id;
							$resDataDet['start_time'] = $value->start_time;
							$resDataDet['end_time'] = $value->end_time;
							
							$resData['details'][] = $resDataDet;
						}
						
					}
					if (!empty($resData['details'])) {
						sort($resData['details']);
					}
					$fixData[] = $resData;			
				}
			}
			return $fixData;
		} else {
			return array();
		}
	}
	
}
