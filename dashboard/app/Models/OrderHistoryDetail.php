<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderHistoryDetail extends Model
{
    protected $table = 'order_history_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','order_history_id', 'product_id', 'link_profiler', 'link_course', 'created_at', 'updated_at'
    ];

    public function orderHistory()
    {
        return $this->belongsTo('App\Models\OrderHistory','order_history_id');
    }
}
