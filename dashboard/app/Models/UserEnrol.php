<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserEnrol extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'mdl_user_enrolments';

    public function getUserEnrol($userName=null, $enrolId=null)
    {
        if (@$userName && @$enrolId) {
            $userEnrol = $this->join('mdl_user','mdl_user.id','=','mdl_user_enrolments.userid')
                ->select('mdl_user_enrolments.timestart','mdl_user_enrolments.timeend')
                ->where([
                    'mdl_user_enrolments.enrolid' => $enrolId,
                    'mdl_user.username' => $userName
                ])->first();

            return $userEnrol;
        }

        return false;
    }

    public static function enrolUser($query, $username, $enrolId)
    {
        return $query->join('mdl_user','mdl_user.id','=','mdl_user_enrolments.userid')
                ->where([
                    'mdl_user_enrolments.enrolid' => $enrolId,
                    'mdl_user.username' => $userName
                ]);
    }
}
