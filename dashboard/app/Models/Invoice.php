<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'total_payment', 'status', 'month', 'year', 'created_at', 'updated_at'
    ];

    protected $hidden = [
      'created_at', 'updated_at'
    ];

    public function details()
    {
        return $this->hasMany('App\Models\InvoiceDetail','invoice_id','id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function getInvoiceByDate($month,$year,$userId)
    {
        return $this->join('invoice_details','invoice_details.invoice_id','=','invoices.id')
            ->leftJoin('products','products.product_code','=','invoice_details.product_code')
            ->select('invoice_details.product_code','products.name as product_name', \DB::raw('sum(invoice_details.quantity) as quantity'),
                \DB::raw('sum(invoice_details.price) as price'), \DB::raw('sum(invoice_details.total) as total'),
                \DB::raw('max(invoice_details.status) as status'))
            ->where('month',$month)
            ->where('year',$year)
            ->where('invoices.user_id',$userId)
            ->groupBy('invoice_details.product_code','products.name')
            ->get();
    }

    public function getInvoice($start_date,$end_date,$userId)
    {
        return $this->join('invoice_details','invoice_details.invoice_id','=','invoices.id')
            ->leftJoin('products','products.product_code','=','invoice_details.product_code')
            ->select('invoice_details.invoice_id','invoice_details.product_code','products.name as product_name', \DB::raw('sum(invoice_details.quantity) as quantity'),
                \DB::raw('avg(invoice_details.price) as price'), \DB::raw('sum(invoice_details.total) as total'),
                \DB::raw('max(invoice_details.status) as status'),'invoices.created_at as date')
            ->whereBetween('invoices.created_at',[date('Y-m-d',strtotime($start_date)).' 00:00:01',date('Y-m-d',strtotime($end_date)).' 23:59:59'])
            ->where('invoices.user_id',$userId)
            ->groupBy('invoice_details.invoice_id','invoice_details.product_code','products.name','invoices.created_at')
            ->get();
    }

    public static function scopeInvoicesByUser($query, $userId)
    {
        return $query->join('invoice_details', 'invoices.id', '=', 'invoice_details.invoice_id')
            ->where('invoices.user_id', $userId)
            ->where('invoices.status',1);
    }
}
