<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScheduleDay extends Model
{
	protected $table = "schedule_days";
    
    protected $fillable = [
        'schedule_id', 'day'
    ];

    public function schedule()
    {
    	return $this->belongsTo('App\Models\Schedule','schedule_id');
    }
}
