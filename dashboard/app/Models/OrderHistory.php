<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderHistory extends Model
{
    protected $table = 'order_histories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','order_code', 'customer_id','status','total','products','date_added','date_modified', 'created_at', 'updated_at'
    ];

    public function details()
    {
        return $this->hasMany('App\Models\OrderHistoryDetail','order_history_id');
    }

    public static function boot()
    {
        parent::boot();

        OrderHistory::deleting(function($orderhistory) {
            foreach ($orderhistory->details as $history) {
                $history->delete();
            }
        });
    }
}
