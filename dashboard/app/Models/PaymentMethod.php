<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    protected $table = 'payment_methods';

    protected $fillable = [
        'name', 'created_at', 'updated_at'
    ];

    public function users()
    {
        return $this->hasMany('App\Models\User','payment_method_id','id');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order','payment_method_id','id');
    }
}
