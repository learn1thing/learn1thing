<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogRole extends Model
{
    protected $table = 'log_roles';

    protected $fillable = [ 'user_id', 'role_id', 'default', 'status', 'active', 'username', 'password' ];

    /**
     * Belongs To User
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function user()
    {
    	return $this->belongsTo( User::class );
    }

    /**
     * { function_description }
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function role()
    {
    	return $this->belongsTo( Role::class );
    }

    public function scopeActive( $query, $user_id )
    {
    	return $query->where( 'active', 1 )->where( 'user_id', $user_id )->first();
    }

    public function checkUser($userId, $email)
    {
        return $this->where('user_id',$userId)->where('username',$email)->first();
    }
}
