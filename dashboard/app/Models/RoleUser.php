<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    protected $table = "role_users";

    /**
     * The attributes that are mass assignable.
     * @author ali
     * @var array
     */

    protected $fillable = [
        'user_id', 'role_id'
    ];

  public function user()
  {
      return $this->hasOne('App\Models\User', 'id', 'user_id');
  }

}
