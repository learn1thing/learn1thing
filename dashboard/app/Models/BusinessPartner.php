<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessPartner extends Model
{
    protected $table = 'business_partners';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'address', 'no_regis', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->morphOne('App\Models\User','related');
    }

    public function coaches()
    {
        return $this->hasMany('App\Models\Coach','business_partner_id');
    }
}
