<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CorporateGroup extends Model
{
    protected $table = 'corporate_groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description','created_by', 'created_at', 'updated_at'
    ];

    public function role()
    {
        return $this->hasOne('App\Models\Role','corporate_group_id');
    }

    public function users()
    {
        return $this->morphMany('App\Models\User','related');
    }

    public function getGroups()
    {
        $result = [];
        if (user_info('parent_id')) {
            $result = $this->where(function($query) {
                $query->where('created_by',user_info('id'))
                    ->orWhere('created_by',user_info('parent_id'));
            });
        } else {
            $childs = user_info()->downlines()->pluck('id')->toArray();
            $result = $this->where(function($query) use($childs) {
                $query->where('created_by',user_info('id'))
                    ->orWhereIn('created_by',$childs);
            });
        }
        return $result;
    }

    public static function boot()
    {
        parent::boot();

        CorporateGroup::deleting(function($corp) {
            foreach ($corp->users as $user) {
                $user->forceDelete();
            }

            if ($corp->role) {
                $corp->role->delete();
            }
        });
    }
}
