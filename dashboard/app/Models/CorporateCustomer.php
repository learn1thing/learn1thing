<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CorporateCustomer extends Model
{
    protected $table = 'corporate_customers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'address', 'no_regis', 'created_at', 'updated_at','logo','description','banner'
    ];

    protected $hidden = [
      'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->morphOne('App\Models\User','related');
    }
}
