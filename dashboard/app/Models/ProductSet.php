<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSet extends Model
{
    protected $table = 'product_sets';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_code','product_id');
    }

    public function course_profiles()
    {
        return $this->belongsTo('App\Models\CourseProfiler','course_id','course_id');
    }

    // type = course/profiler
    public function orderCountByType($type, $productIds = array())
    {
        $result = ProductSet::whereIn('product_id', $productIds);
        if($type === "course") {
            $result = $result->whereNotNull('course_id');
        } else if($type === "profiler") {
            $result = $result->whereNotNull('profiler_id');
        }
        return $result->count();
    }
}
