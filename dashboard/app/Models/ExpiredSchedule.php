<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExpiredSchedule extends Model
{
	protected $table = 'expired_schedules';
	protected $fillable = ['expired','business_partner_id'];
}
