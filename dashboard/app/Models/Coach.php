<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Config;

class Coach extends Model
{
    protected $table = 'coaches';

    /**
     * The attributes that are mass assignable.
     * @author ali
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'description', 'photo', 'business_partner_id', 'course_id', 'user_id'
    ];

    public function coach_schedules()
    {
    	return $this->hasMany('App\Models\CoachSchedule','coach_id');
    }

    public function user_coach()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function business_partner()
    {
        return $this->belongsTo('App\Models\BusinessPartner','business_partner_id');
    }

    public function coach_schedule_orders()
    {
        return $this->hasMany('App\Models\CoachScheduleOrder','coach_id');
    }

    public function updateCoach($id,$param)
    {
        $coach = Coach::find($id);
        if ($coach) {
            $coach->name = $param['name'];
            $coach->description = $param['description'];
            $coach->course_id = $param['course_id'];

            if(array_key_exists('photo', $param)) {   
                $pathDest = Config::get('services.public_path.image.pattern_path');
                $image = upload_image($param['photo'],$pathDest)['original'];
                $coach->photo = $image;
            }
            
            $coach->save();

            return true;
        
        } else {
            return false;
        }
    }

    public static function boot()
    {
        parent::boot();

        Coach::deleting(function($coach) {
            if (count($coach->coach_schedules) > 0) {
                foreach ($coach->coach_schedules as $coachSchedule) {
                    $coachSchedule->delete();
                }
            }

            // if (count($coach->coach_schedule_orders) > 0) {
            //     foreach ($coach->coach_schedule_orders as $coachScheduleOrder) {
            //         $coachScheduleOrder->delete();
            //     }
            // }
        });
    }
}
