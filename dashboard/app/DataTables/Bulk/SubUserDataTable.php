<?php

namespace App\DataTables\Bulk;

use App\Models\User;
use App\Models\BusinessParterGroup;
use Yajra\Datatables\Services\DataTable;

class SubUserDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
         return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function($subuser){
                $data = [
                    "edit_url" => route('admin.business-partner.bulk.edit', $subuser->id),
                    "delete_url" => route('admin.business-partner.bulk.destroy', $subuser->id),
                ];
                return view('partials.action-button')->with($data);
            })
            ->editColumn('created_at', function($data) {
                return date('d M Y', strtotime($data->created_at));
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        if (user_info()->hasAccess('all')) {
            $query = $this->queryType('general'); 
        } else {

            if (user_info('parent_id')) {
                if (user_info()->upline) {
                    if (user_info()->upline->inRole('business-partner')) {
                        $query = $this->queryType('business-partner');
                    } elseif (user_info()->upline->inRole('corporate-customer')) {
                        $query = $this->queryType('corporate-customer');
                    } else {
                        $query = $this->queryType('general');
                    }
                } else {
                    $query = $this->queryType('general');
                }

                $query = $query->where(function($query) {
                    $query->where('users.parent_id',user_info('id'))
                        ->orWhere('users.parent_id',user_info('parent_id'));
                });
            } else {
                if (user_info('role')->slug == 'business-partner') {
                    $query = $this->queryType('business-partner');
                } elseif (user_info('role')->slug == 'corporate-customer') {
                    $query = $this->queryType('corporate-customer');
                } else {
                    $query = $this->queryType('general');
                }

                $childs = user_info()->downlines()->pluck('id')->toArray();
                $query = $query->where(function($query) use($childs) {
                    $query->where('users.parent_id',user_info('id'))
                        ->orWhereIn('users.parent_id',$childs);
                });
            }

        }

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        if (user_info('role')->slug == 'business-partner') {
            return [
                'first_name' => ['name'=>'users.first_name', 'data' => 'first_name', 'title'=>'First Name', 'id' => 'last_name'],
                'last_name' => ['name'=>'users.last_name', 'data' => 'last_name', 'title'=>'Last Name', 'id' => 'first_name'],
                'email' => ['name'=>'users.email', 'data' => 'email', 'title'=>'Email', 'id' => 'email'],
                'role' => ['name'=>'business_partner_groups.name', 'data' => 'name', 'title'=>'Role', 'id' => 'role'],
                'department_name' => ['name'=>'departments.name', 'data' => 'department_name', 'title'=>'Department', 'id' => 'department'],
                'created_at' => ['name'=>'users.created_at', 'data' => 'created_at', 'title'=>'Created At', 'id' => 'created_at'],
            ];
        } else {
            return [
                'first_name' => ['name'=>'users.first_name', 'data' => 'first_name', 'title'=>'First Name', 'id' => 'last_name'],
                'last_name' => ['name'=>'users.last_name', 'data' => 'last_name', 'title'=>'Last Name', 'id' => 'first_name'],
                'email' => ['name'=>'users.email', 'data' => 'email', 'title'=>'Email', 'id' => 'email'],
                'role' => ['name'=>'corporate_groups.name', 'data' => 'name', 'title'=>'Role', 'id' => 'role'],
                'department_name' => ['name'=>'departments.name', 'data' => 'department_name', 'title'=>'Department', 'id' => 'department'],
                'created_at' => ['name'=>'users.created_at', 'data' => 'created_at', 'title'=>'Created At', 'id' => 'created_at'],
            ];
        }
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'bulk/subusers_' . time();
    }

    protected function queryType($type)
    {
        if ($type == 'business-partner') {
            return User::join('business_partner_groups','users.related_id','=','business_partner_groups.id')
                ->leftJoin('departments','users.department_id','=','departments.id')
                ->select(['users.id','users.first_name','users.last_name','users.email','business_partner_groups.name','users.created_at','departments.name as department_name']);
        } elseif ($type == 'corporate-customer') {
            return User::join('corporate_groups','users.related_id','=','corporate_groups.id')
                ->leftJoin('departments','users.department_id','=','departments.id')
                ->select(['users.id','users.first_name','users.last_name','users.email','corporate_groups.name','users.created_at','departments.name as department_name']);
        } else {
            return User::join('business_partner_groups','users.related_id','=','business_partner_groups.id')
                ->leftJoin('departments','users.department_id','=','departments.id')
                ->select('users.id','users.first_name','users.last_name','users.email','business_partner_groups.name','users.created_at','departments.name as department_name');
        }
    }
}
