<?php

namespace App\DataTables\Order;

use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderProduct;
use App\Models\Product;
use Yajra\Datatables\Services\DataTable;
use Illuminate\Support\Collection;
use Sentinel;

class ProductDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->of($this->query())
            ->addColumn('user', function($data){
                $orderProduct = OrderProduct::find($data['order_detail_id']);
                if ($data['status'] == 'Completed' && @$orderProduct->status == 0) {
                    $userInfo = Sentinel::findById($data['user_id']);
                    $userGroups = $userInfo->groupCorporates;
                    $html = '<select class="form-control"><option value="">Select user</option>';
                    $tempEmail = [];
                    foreach ($userGroups as $group) {
                        if ($group->users->count() > 0) {
                            foreach ($group->users as $user) {
                                if (!in_array($user->email, $tempEmail)) {
                                    $html .= '<option value="'.$user->email.'">'.$user->first_name.' '.$user->last_name.'</option>';
                                    $tempEmail[] = $user->email;
                                }
                            }
                        }
                    }
                    $html .= '</select>';
                    return $html;
                } else {
                    return '-';
                }
            })
            ->addColumn('action', function($data){
                if ($data['status'] == 'Completed') {
                    $orderProduct = OrderProduct::find($data['order_detail_id']);
                    if ($orderProduct && $orderProduct->status == 0) {
                        return '<button class="btn btn-info btn-xs send-product" data-product-id="'.$data['product_id'].'" data-order-detail-id="'.$data['order_detail_id'].'">Send</button>';
                    } else {
                        return '-';
                    }
                } else {
                    return '-';
                }
            })
            ->editColumn('created_at', function($data) {
                return date('d M Y', strtotime($data['created_at']));
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $order = new Order;
        $result = [];
        $query = new Collection;
        $orders = $order->where('user_id',user_info('id'))->get();
        $x= 0;
        if (count($orders) > 0) {
            foreach ($orders as $key => $valOrder) {
                $orderDetails = $valOrder->details;
                if ($valOrder->status == 1) {
                    $status = 'Pending';
                } elseif ($valOrder->status == 2) {
                    $status = 'Completed';
                } else {
                    $status = 'Rejected';
                }
                if (count($orderDetails) > 0) {
                    foreach ($orderDetails as $detail) {
                        if ($detail->orderProducts->count() > 0 && @$detail->product->name) {
                            foreach ($detail->orderProducts as $value) {
                                $query->push([
                                    'order_detail_id' => $value->id,
                                    'email' => '',
                                    'user_id' => $valOrder->user_id,
                                    'product_name' => @$detail->product->name,
                                    'product_id' => @$detail->product->product_code,
                                    'model' => @$detail->product->model,
                                    'price' => @$detail->product->price,
                                    'status' => $status,
                                    'created_at' => date('Y-m-d H:i:s',strtotime($valOrder->created_at))
                                ]);
                            }
                        }
                    }
                }
            }
        }
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get Builder Parameter.
     * 
     * @return array
     */
    public function getBuilderParameters()
    {
        return [
            'order'   => [[0, 'asc']],
            'dom' => 'Bfrtip'
        ];
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'product_name' => ['name'=>'product_name', 'data' => 'product_name', 'title'=>'Product Name', 'id' => 'product_name'],
            'model' => ['name'=>'model', 'data' => 'model', 'title'=>'Model', 'id' => 'model'],
            'price' => ['name'=>'price', 'data' => 'price', 'title'=>'Price', 'id' => 'price'],
            'status' => ['name'=>'status', 'data' => 'status', 'title'=>'Status', 'id' => 'status'],
            'created_at' => ['name'=>'created_at', 'data' => 'created_at', 'title'=>'Created At', 'id' => 'created_at'],
            'user' => [
                'orderable' => false,
                'searchable' => false,
                'title' => 'User',
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'order/products_' . time();
    }
}
