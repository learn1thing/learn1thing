<?php

namespace App\DataTables\Order;

use Yajra\Datatables\Services\DataTable;
use App\Classes\CurlRequest;
use App\Models\Product;

class OrderHistoryDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            // ->addColumn('action', 'path.to.action.view')
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        // $products = $this->product->where('user_id',user_info('id'))->pluck('product_code')->toArray();
        // if (count($products) > 0) {
            $responses = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/order_admin/listorderswithdetails');
            // echo "<pre>";
            // print_r($responses); die();
            $result = [];
            $x = 0;
            if ($responses->success) {
                foreach ($responses->data as $key => $value) {
                    foreach($value->products as $product) {
                        $y = 0;
                        // if (in_array($product->product_id, $products)) {
                        $total = 0;
                        if ($product->product_id == 62 || $product->product_id == 78) {
                            $result[$x]['product'] = $product;
                            $total += $product->total;
                            $y++;
                        }
                    }

                    if ($y > 0) {
                        unset($value->products);
                        $result[$x]['info'] = $value;
                        $result[$x]['total_order'] = $total;
                        $x++;
                    }
                }
            }

            return ;
        // }

        return $this->applyScopes($result);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            // add your columns
            'created_at',
            'updated_at',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'order/orderhistories_' . time();
    }
}
