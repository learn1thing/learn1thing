<?php

namespace App\DataTables\Order;

use App\Models\Order;
use Yajra\Datatables\Services\DataTable;

class OrderDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('status', function($data) {
                $status = $this->getStatusApp($data);
                if ($status == 1) {
                    return 'Pending';
                } elseif ($status == 2) {
                    return 'Approved';
                } else {
                    return 'Rejected';
                }
            })
            ->addColumn('action', function($data){
                $detail_url = route('admin.order.detail', $data->order_id);
                $status = $this->getStatusApp($data);
                if ($status == 1) {
                    if (user_info()->inRole('super-admin') || user_info()->inRole('business-partner')) {
                        $approve_url = route('admin.order.approve', $data->order_id);
                        $decline_url = route('admin.order.decline', $data->order_id);
                        return view('partials.action-button')->with(compact('approve_url','decline_url','detail_url'));
                    } else {
                        // $edit_url = route('admin.order.edit', $data->order_id);
                        // $delete_url = route('admin.order.destroy', $data->order_id);
                        return view('partials.action-button')->with(compact('detail_url'));
                    }
                } else {
                    return view('partials.action-button')->with(compact('detail_url'));
                }
            })
            // ->addColumn('learning_provider',function($data){
            //     return 'ODE Consulting';
            // })
            ->editColumn('created_at', function($data) {
                return date('d M Y', strtotime($data->created_at));
            })
            ->editColumn('updated_at', function($data) {
                return date('d M Y', strtotime($data->created_at));
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $order = new Order;
        $user_id = null;
        if (!user_info()->inRole('super-admin')) {
            $user_id = user_info('id');
        }

        $query = $order->myOrderDatatable($user_id);

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get Builder Parameter.
     * 
     * @return array
     */
    public function getBuilderParameters()
    {
        return [
            'order'   => [[3, 'desc']],
            'dom' => 'Bfrtip'
        ];
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            // 'number' => [
            //     'orderable' => false,
            //     'searchable' => false,
            //     'title' => 'No',
            //     'width' => '10px',
            // ],

            'Customer' => [
                'id'    =>  'customer',
                'name' => 'users.email',
                'title' => 'Customer',
                'data' => 'email',
            ],
            // 'learning_provider'=>[
            //     'id'=>'learning_provider',
            //     'name'=>'learning_provider',
            //     'title'=>'Learning Provider',
            //     'data'=>'learning_provider',
            // ],
            'status'  =>  [
                'id'    =>  'status',
                'title' =>  'Status',
                'name'  =>  'orders.status',
                'data'  =>  'status',
            ],
            'payment_method' => ['id' => 'payment_method',
                    'name'=>'payment_methods.name', 
                    'data' => 'payment_method', 
                    'title'=>'Payment Method', 
                ],
            'created_at' => ['id' => 'created_at',
                    'name'=>'orders.created_at', 
                    'data' => 'created_at', 
                    'title'=>'Purchased On', 
                    ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'order/orders_' . time();
    }

    protected function getStatusApp($data)
    {
        if (user_info()->inRole('business-partner')) {
            $status = $data->status_approval;
        } else {
            $status = $data->status;
        }

        return $status;
    }
}
