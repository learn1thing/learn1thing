<?php

namespace App\DataTables\Schedule;

use App\User;
use Yajra\Datatables\Services\DataTable;
use App\Models\CoachSchedule;

class ScheduleDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function($user){
                // $data = [
                //     "custom_show" => [
                //                         'show_url' => route('coach.schedule.show', $user->id),
                //                         'extra_caption' => " View Schedule"
                //                     ]
                //         ];
                // return view('partials.action-button')->with($data);
            })
            ->editColumn('schedule_date', function($data) {
                return date('d M Y', strtotime($data->schedule_date));
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        // if (user_info()->hasAccess('all')) {
        //     $arrayWhere = ['roles.name' => 'coach'];
        // } else {
        //     $arrayWhere = ['roles.name' => 'coach' , 'users.parent_id' => user_info('id')];
        // }

        $query = CoachSchedule::query();
        // ->join('roles','role_users.role_id','=','roles.id')
        // ->select('users.id','users.first_name','users.last_name')
        // ->where($arrayWhere);



        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id', 
            'user_id', 
            'course_id', 
            'schedule_date', 
            'start_time', 
            'end_time',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'schedule/scheduledatatables_' . time();
    }
}
