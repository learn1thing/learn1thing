<?php

namespace App\DataTables\Schedule;

use App\User;
use App\Models\ExpiredSchedule;
use App\Models\BusinessPartner;
use Yajra\Datatables\Services\DataTable;

class ExpiredScheduleDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function($coach){
                $data = [
                    "delete_url" => route('master.schedule.expired.destroy', $coach->id),
                    "edit_url" => route('master.schedule.expired.edit', $coach->id),
                ];
                return view('partials.action-button')->with($data);
            })
            ->editColumn('name', function($data){
                if ($data->business_partner_id == 0) {
                    return 'Admin Default';
                } else {
                    return $data->name;
                }
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        if (user_info()->inRole('business-partner')) {
            $userid = user_info()->related->id;
            $query = ExpiredSchedule::leftjoin('business_partners','business_partners.id','=','expired_schedules.business_partner_id')->where('expired_schedules.business_partner_id', $userid)
            ->select('expired_schedules.id','expired_schedules.expired','expired_schedules.business_partner_id','expired_schedules.created_at','business_partners.name');            
        } else if (user_info()->inRole('super-admin')) {
            $query = ExpiredSchedule::leftjoin('business_partners','business_partners.id','=','expired_schedules.business_partner_id')
                ->where('expired_schedules.business_partner_id', '>=',0)
                ->select('expired_schedules.id','expired_schedules.expired','expired_schedules.business_partner_id','expired_schedules.created_at','business_partners.name');
        }

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => ['name'=>'expired_schedules.id', 'data' => 'id', 'title'=>'Id', 'id' => 'id'],

            'expired' => ['name'=>'expired_schedules.expired', 'data' => 'expired', 'title'=>'Expired (Hours)', 'id' => 'expired'],
            
            'name' => ['name'=>'business_partners.name', 'data' => 'name', 'title'=>'Name Business Partner', 'id' => 'name'],
            
            'created_at' => ['name'=>'expired_schedules.created_at', 'data' => 'created_at', 'title'=>'Created At', 'id' => 'created_at'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'schedule/expiredscheduledatatables_' . time();
    }
}
