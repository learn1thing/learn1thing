<?php

namespace App\DataTables\Coach;

use App\Models\User;
use Sentinel;
use Yajra\Datatables\Services\DataTable;
use Illuminate\Support\Collection;

class CoachListDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->of($this->query())
            ->editColumn('created_at', function($data) {
                return date('d M Y', strtotime($data['created_at']));
            })
            // ->addColumn('action', 'path.to.action.view')
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = new Collection;

        $users = User::select('id','email','first_name','last_name','related_id','related_type')
            ->where('related_type', 'App\Models\BusinessPartner')->get();

        if (count($users) > 0) {
            foreach ($users as $user) {
                if (count($user->groups) > 0) {
                    foreach ($user->groups as $group) {
                        if ($group->role->hasAccess('manage-coach')) {
                            $groupUsers = $group->users;
                            if (count($groupUsers) > 0) {
                                foreach ($groupUsers as $userVal) {
                                    $query->push([
                                        'owner' => $user->email,
                                        'first_name' => $userVal->first_name,
                                        'last_name' => $userVal->last_name,
                                        'email' => $userVal->email,
                                        'department' => @$userVal->department->name,
                                        'created_at' => date('Y-m-d H:i:s',strtotime($userVal->created_at))
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
        }
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    // ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'owner' => ['name'=>'owner', 'data' => 'owner', 'title'=>'Owner', 'id' => 'owner'],
            'first_name' => ['name'=>'first_name', 'data' => 'first_name', 'title'=>'First Name', 'id' => 'first_name'],
            'last_name' => ['name'=>'last_name', 'data' => 'last_name', 'title'=>'Last Name', 'id' => 'last_name'],
            'email' => ['name'=>'email', 'data' => 'email', 'title'=>'Email', 'id' => 'email'],
            'department' => ['name'=>'department', 'data' => 'department', 'title'=>'Department', 'id' => 'department'],
            'created_at' => ['name'=>'created_at', 'data' => 'created_at', 'title'=>'Created At', 'id' => 'created_at'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'coach/coachlists_' . time();
    }
}
