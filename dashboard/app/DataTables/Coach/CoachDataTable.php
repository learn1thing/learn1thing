<?php

namespace App\DataTables\Coach;

use App\User;
use Yajra\Datatables\Services\DataTable;
use App\Models\Coach;
use Sentinel;

class CoachDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function($coach){
                $data = [
                    "delete_url" => route('coach.schedule.delete', $coach->id),
                    "show_url" => route('coach.schedule.show', $coach->id),
                    "edit_url" => route('coach.schedule.edit', $coach->id),
                    "reset_url" => route('coach.schedule.reset', $coach->id),
                ];
                return view('partials.action-button')->with($data);
            })
            ->editColumn('description',function($data) {
                return str_limit($data->description, 150);
            })
            ->editColumn('created_at', function($data) {
                return date('d M Y', strtotime($data->created_at));
            })
            ->editColumn('updated_at', function($data) {
                return date('d M Y', strtotime($data->created_at));
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $currentUser = Sentinel::getUser();
        if ($currentUser->inRole('business-partner')) {
            $userid = $currentUser->related->id;
            $query = Coach::where('business_partner_id', $userid);            
        } else if ($currentUser->inRole('super-admin')) {
            $query = Coach::where('business_partner_id', '>', 0);
        } else {
            $query = Coach::where('user_id',$currentUser->id);
        }

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '120px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'name',
            'description',
            'created_at',
            'updated_at',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'coach/coachdatatables_' . time();
    }
}
