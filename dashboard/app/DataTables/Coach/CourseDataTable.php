<?php

namespace App\DataTables\Coach;

use App\Models\CoachCourse;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Services\DataTable;

class CourseDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function($course){
                $data = [
                    "edit_url" => route('coach.course.edit', $course->id),
                    "delete_url" => route('coach.course.destroy', $course->id),
                ];
                return view('partials.action-button')->with($data);
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        if (user_info()->hasAccess('all')) {
            $query = CoachCourse::query();
        } else {
            $query = CoachCourse::where('user_id',user_info('id'));
        }

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            // 'rownum',
            'id',
            'name',
            'description',
            'created_at',
            'updated_at',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'coach\coursedatatables_' . time();
    }
}
