<?php

namespace App\DataTables\GroupCorporate;

use App\User;
use App\Models\CorporateGroup;
use Yajra\Datatables\Services\DataTable;

class GroupCorporateDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function($group){
                $data = [
                    "edit_url" => route('admin.corporate-customer.store.group-user.edit', $group->id),
                    "delete_url" => route('admin.corporate-customer.store.group-user.destroy', $group->id),
                    "permission_url" => route('admin.corporate-customer.store.group-user.manage', @$group->role->id),
                ];
                return view('partials.action-button')->with($data);
            })
            ->editColumn('created_at', function($data) {
                return date('d M Y', strtotime($data->created_at));
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        if (user_info()->hasAccess('all')) {
            $query = CorporateGroup::join('users','users.id','=','corporate_groups.created_by')
                ->select('corporate_groups.id','corporate_groups.name','corporate_groups.created_at'
                    ,\DB::raw("concat(users.first_name, ' ', users.last_name) as owner"),'users.first_name','users.email');
        } else {
            $query = CorporateGroup::join('users','users.id','=','corporate_groups.created_by')
                ->select('corporate_groups.id','corporate_groups.name','corporate_groups.created_at'
                    ,\DB::raw("concat(users.first_name, ' ', users.last_name) as owner"),'users.first_name','users.email')
                ->where('created_by',user_info('id'));
        }

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'name' => ['name'=>'corporate_groups.name', 'data' => 'name', 'title'=>'Role Name', 'id' => 'name'],
            'first_name' => ['name'=>'users.first_name', 'data' => 'owner', 'title'=>'Owner', 'id' => 'owner'],
            'email' => ['name'=>'users.email', 'data' => 'email', 'title'=>'Email', 'id' => 'email'],
            'created_at' => ['name'=>'corporate_groups.created_at', 'data' => 'created_at', 'title'=>'Created At', 'id' => 'created_at'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'groupcorporate/groupcorporatedatatables_' . time();
    }
}
