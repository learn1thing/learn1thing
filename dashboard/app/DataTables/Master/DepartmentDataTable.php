<?php

namespace App\DataTables\Master;

use App\Models\BusinessPartner;
use App\Models\CorporateCustomer;
use App\Models\Department;
use Yajra\Datatables\Services\DataTable;

class DepartmentDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('department_parent', function($data) {
                return (@$data->department_parent) ? Department::find($data->department_parent)->name : '-';
            })
            ->addColumn('action', function($data){
                $edit_url = route('admin.department.edit', $data->id);
                $delete_url = route('admin.department.destroy', $data->id);
                return view('partials.action-button')->with(compact('delete_url','edit_url'));
            })
            ->editColumn('created_at', function($data) {
                return date('d M Y', strtotime($data->created_at));
            })
            ->editColumn('updated_at', function($data) {
                return date('d M Y', strtotime($data->created_at));
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $department = new Department;
        if (user_info()->inRole('super-admin')) {
            $query = $department->myDepartmentsDatatable();
        } else {
            $query = $department->myDepartmentsDatatable(user_info('id'));
        }

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get Builder Parameter.
     * 
     * @return array
     */
    public function getBuilderParameters()
    {
        return [
            'order'   => [[0, 'asc']],
            'dom' => 'Bfrtip'
        ];
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'number' => [
                'orderable' => false,
                'searchable' => false,
                'title' => 'No',
                'width' => '10px',
            ],
            'owner' => [
                'id'    =>  'owner',
                'name' => 'users.email',
                'title' => 'Owner',
                'data' => 'owner',
            ],
            'department_parent'  =>  [
                'orderable' => false,
                'searchable' => false,
                'id'    =>  'parent_department',
                'title' =>  'Parent Department',
                'name'  =>  'departments.parent_id',
                'data'  =>  'department_parent',
            ],
            'name'  =>  [
                'id'    =>  'name',
                'title' =>  'Department Name',
                'name'  =>  'departments.name',
                'data'  =>  'name',
            ],
            'created_at' => ['id' => 'created_at',
                    'name'=>'departments.created_at', 
                    'data' => 'created_at', 
                    'title'=>'Created At', 
                    ],
            'updated_at' => ['id' => 'updated_at',
                    'name'=>'departments.updated_at', 
                    'data' => 'updated_at', 
                    'title'=>'Updated At', 
                ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'master/departments_' . time();
    }
}
