<?php

namespace App\DataTables\Master;

use App\Models\Product;
use App\Models\User;
use Yajra\Datatables\Services\DataTable;
use App\Classes\CurlRequest;

class ProductDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('status',function($product){
                return ($product->status == '1') ? 'Enabled' : 'Disabled';
            })
            ->editColumn('image',function($product){
                return '<img src='.get_file($product->image,'thumbnail').' style="width:50px;height:50px;">';
            })
            ->addColumn('action', function($product){
                $delete_url = route('admin.business-partner.destroy-product', $product->product_code);
                $edit_url = route('admin.business-partner.edit-product', $product->product_code);
                $restart_url = route('admin.business-partner.restart-url-product', $product->product_code);

                $restartAlert = false;
                $responses = CurlRequest::sendRequest('GET', env('OC_BASE_URL').'/index.php?route=rest/product_admin/products&id='.$product->product_code);
                if ($responses->success) {
                    if (count($responses->data->category) > 0) {
                        $state = false;
                        foreach ($responses->data->category as $category) {
                            if (!$state) {
                                if (strpos(strtolower(@$category->name), 'virtual') !== false) {
                                    $restartAlert = true;
                                }
                            }
                        }
                    }
                }

                if ($restartAlert) {
                    return view('partials.action-button')->with(compact('delete_url','edit_url','restart_url'));
                } else {
                    return view('partials.action-button')->with(compact('delete_url','edit_url'));
                }
            })
            ->editColumn('created_at', function($data) {
                return date('d M Y', strtotime($data->created_at));
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Product::join('users','users.id','=','products.user_id')
            ->select('users.related_id as business_partner_id','users.id as user_id','products.product_code'
                ,\DB::raw("concat(users.first_name,' ',users.last_name) as owner"),'users.email','products.name as product_name',
                'users.parent_id','products.image','products.model','products.price','products.quantity','products.status',
                'products.created_at');
            
        if (user_info()->hasAccess('manage-product') && (user_info('role')->slug == 'business-partner' || user_info()->upline->inRole('business-partner'))) {
            if (user_info('parent_id')) {
                $query = $query->where(function($query) {
                    $query->where('users.id',user_info('id'))
                        ->orWhere('users.id',user_info('parent_id'));
                });
            } else {
                $query = $query->where(function($query) {
                    $query->where('users.id',user_info('id'))
                        ->orWhere('users.parent_id',user_info('id'));
                });
            }
        } else {
            $query = $query->where('users.id',user_info('id'));
        }
        
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '50px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'image' => ['name'=>'products.image', 'data' => 'image', 'title'=>'Image', 'id' => 'image'],
            'email' => ['name'=>'users.email', 'data' => 'email', 'title'=>'Email', 'id' => 'email'],
            'owner' => ['name'=>'users.first_name', 'data' => 'owner', 'title'=>'Owner', 'id' => 'owner'],
            'product_name' => ['name'=>'products.name', 'data' => 'product_name', 'title'=>'Product Name', 'id' => 'product_name'],
            'model' => ['name'=>'products.name', 'data' => 'model', 'title'=>'Model', 'id' => 'model'],
            'price' => ['name'=>'products.name', 'data' => 'price', 'title'=>'Price', 'id' => 'price'],
            'quantity' => ['name'=>'products.name', 'data' => 'quantity', 'title'=>'Quantity', 'id' => 'quantity'],
            'status' => ['name'=>'products.name', 'data' => 'status', 'title'=>'Show At Store', 'id' => 'status'],
            'created_at' => ['name'=>'users.created_at', 'data' => 'created_at', 'title'=>'Created At', 'id' => 'created_at'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'master/products_' . time();
    }
}
