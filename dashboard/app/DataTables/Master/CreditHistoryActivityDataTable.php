<?php

namespace App\DataTables\Master;

use App\Models\User;
use App\Models\CreditHistory;
use Yajra\Datatables\Services\DataTable;

class CreditHistoryActivityDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('credit',function($data) {
                return number_format($data->credit);
            })
            ->editColumn('mutation',function($data) {
                return number_format($data->mutation);
            })
            ->editColumn('created_at',function($data) {
                return date('Y-m-d H:i:s',strtotime($data->created_at));
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = CreditHistory::join('users','users.id','=','credit_histories.user_id')
            ->select(['users.first_name','users.last_name','credit_histories.mutation','credit_histories.credit','credit_histories.activity'
                ,'credit_histories.created_at']);
        if (!user_info()->hasAccess('all')) {
            $query = $query->where('credit_histories.user_id',user_info('id'));
        } else {
            $query = $query->where('credit_histories.activity','Top Up');
        }

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get Builder Parameter.
     * 
     * @return array
     */
    public function getBuilderParameters()
    {
        return [
            'order'   => [[0, 'asc']],
            'dom' => 'Bfrtip'
        ];
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'created_at' => [
                'orderable' => false,
                'id'    =>  'created_at',
                'name' => 'credit_histories.created_at',
                'title' => 'Date',
                'data' => 'created_at',
            ],
            'mutation'  =>  [
                'orderable' => false,
                'id'    =>  'mutation',
                'title' =>  'Mutation',
                'name'  =>  'credit_histories.mutation',
                'data'  =>  'mutation',
            ],
            'credit' => [
                'orderable' => false,
                'id' => 'credit',
                'name'=>'credit_histories.credit', 
                'data' => 'credit', 
                'title'=>'Credit', 
            ],
            'activity'  =>  [
                'orderable' => false,
                'id'    =>  'activity',
                'title' =>  'Activity',
                'name'  =>  'credit_histories.activity',
                'data'  =>  'activity',
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'master/credithistories_' . time();
    }
}
