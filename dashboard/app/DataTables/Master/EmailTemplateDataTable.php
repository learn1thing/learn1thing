<?php

namespace App\DataTables\Master;

use App\Models\EmailTemplate;
use Yajra\Datatables\Services\DataTable;

class EmailTemplateDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function($data) {
                $edit_url = route('admin.emailtemplate.edit', $data->id);
                $delete_url = route('admin.emailtemplate.destroy', $data->id);
                return view('partials.action-button')->with(compact('delete_url','edit_url'));
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = EmailTemplate::select('id', 'title', 'slug', 'subject', 'message', 'created_at');

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'title',
            // add your columns
            'subject',
            'created_at',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'master/emailtemplatedatatables_' . time();
    }
}
