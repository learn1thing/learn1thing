<?php

namespace App\DataTables\Master;

use App\Models\User;
use Yajra\Datatables\Services\DataTable;

class UserDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('completed', function($user){
                if ($user->completed) {
                    return 'Active';
                } else {
                    return 'Not Active';
                }
            })
            ->addColumn('action', function($user){
                $accept_url = route('admin.user.active', $user->id);
                return view('partials.action-button')->with(compact('accept_url','user'));
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = User::join('role_users','role_users.user_id','=','users.id')
            ->join('roles','roles.id','=','role_users.role_id')
            ->join('activations','activations.user_id','=','users.id')
            ->select('users.id','users.first_name','users.last_name','users.created_at','roles.name as role_name','activations.completed');

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters(array_merge($this->getBuilderParameters(), ["order" => [[ 3, 'desc' ]]]));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => ['name'=>'users.id', 'data' => 'id', 'title'=>'Id', 'id' => 'id'],
            'first_name' => ['name'=>'users.first_name', 'data' => 'first_name', 'title'=>'First Name', 'id' => 'first_name'],
            'last_name' => ['name'=>'users.last_name', 'data' => 'last_name', 'title'=>'First Name', 'id' => 'last_name'],
            'role_name' => ['name'=>'roles.name', 'data' => 'role_name', 'title'=>'Role', 'id' => 'role_name'],
            'completed' => ['name'=>'activations.completed', 'data' => 'completed', 'title'=>'Active', 'id' => 'completed'],
            'created_at' => ['name'=>'users.created_at', 'data' => 'created_at', 'title'=>'Created At', 'id' => 'created_at'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'master/user/users_' . time();
    }
}
