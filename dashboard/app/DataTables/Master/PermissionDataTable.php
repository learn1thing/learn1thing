<?php

namespace App\DataTables\Master;

use App\Models\Role;
use DB;
use Yajra\Datatables\Services\DataTable;

class PermissionDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function($role) {
                $permission_url = route('admin.permission.manage',['slug' => $role->slug]);
                return view('partials.action-button')->with(compact('permission_url'));
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Role::select('id','name','slug')
            ->whereIn('slug',['business-partner','corporate-customer','standard-customer']);

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters())
                    ->parameters(['bFilter' => false,'bInfo'=>false,'bPaginate'=>false]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'name',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'master/permissions_' . time();
    }
}
