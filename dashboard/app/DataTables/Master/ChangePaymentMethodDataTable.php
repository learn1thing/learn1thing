<?php

namespace App\DataTables\Master;

use App\Models\User;
use Yajra\Datatables\Services\DataTable;

class ChangePaymentMethodDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('current_payment', function($user){
                return $user->paymentMethod->name;
            })
            ->addColumn('request_to', function($user){
                return $this->payment_method($user);
            })
            ->addColumn('action', function($user){
                $approve_url = route('admin.change-payment-method.approve', $user->id);
                return view('partials.action-button')->with(compact('approve_url'));
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = User::join('role_users','role_users.user_id','=','users.id')
            ->join('roles','roles.id','=','role_users.role_id')
            ->join('activations','activations.user_id','=','users.id')
            ->select('users.id','users.email','users.first_name','users.last_name','users.created_at','roles.name as role_name','activations.completed','users.payment_method_id')
            ->where('is_change_payment_method',true);

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'first_name' => ['name'=>'users.first_name', 'data' => 'first_name', 'title'=>'First Name', 'id' => 'first_name'],
            'last_name' => ['name'=>'users.last_name', 'data' => 'last_name', 'title'=>'Last Name', 'id' => 'last_name'],
            'email' => ['name'=>'users.email', 'data' => 'email', 'title'=>'Email', 'id' => 'email'],
            'current_payment' => ['title'=>'Current Payment', 'searchable' => false, 'orderable' => false],
            'request_to' => ['title'=>'Request To', 'searchable' => false, 'orderable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'master/changepaymentmethods_' . time();
    }

    protected function payment_method($user)
    {
        if (strpos(strtolower($user->paymentMethod->name), 'credit') !== false) {
            return 'Pay As You Go';
        }

        return 'Credit Based System';
    }
}
