<?php

namespace App\DataTables\Master;

use App\User;
use Yajra\Datatables\Services\DataTable;
use App\Models\OrderProduct;

class RequestProductDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('status', function($data) {
                return (($data->status == 1) ? 'Approved' : 'Request') ;
            })
            ->addColumn('action', function($data) {
                if ($data->status == 1) {
                    $show_url = route('admin.corporate-customer.request-product-list.detail',$data->id);
                    return view('partials.action-button')->with(compact('show_url'));
                } else {
                    return '-';
                }
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = OrderProduct::join('products', 'order_products.product_code', '=', 'products.product_code')
                    ->join('order_details', 'order_products.order_detail_id', '=', 'order_details.id')
                    ->join('orders', 'order_details.order_id', '=', 'orders.id')
                    ->join('users', 'order_products.user_id', '=', 'users.id')
                    ->select('order_products.id','users.email','order_products.user_id','products.name','order_products.product_code',
                        \DB::raw("DATE_FORMAT(order_products.updated_at, '%d-%b-%Y') as date"),
                        'order_products.status')
                    ->where('order_products.user_id', user_info('id'))
                    ->where('order_products.user_id', '!=', "")
                    ->where(function ($query) {
                        $query->where('order_products.status', '=', 2)
                            ->orWhere('order_products.status', '=', 1);
                    });

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'updated_at' => ['name'=>'order_products.updated_at', 'data' => 'date', 'title'=>'Date', 'id' => 'date'],          
            'name' => ['name'=>'products.name', 'data' => 'name', 'title'=>'Product', 'id' => 'name'],
            'status' => ['name'=>'users.status', 'data' => 'status', 'title'=>'Status', 'id' => 'status'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'master/requestproducts_' . time();
    }
}
