<?php

namespace App\DataTables\Master;

use App\User;
use App\Models\Product;
use App\Models\ProductAssign;
use Yajra\Datatables\Services\DataTable;

class ProductAssignDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function($product){
                $data = [
                    // "edit_url" => route('admin.business-partner.product.set.edit', $product->id),
                    "delete_url" => route('admin.business-partner.product.assign-destroy', $product->id),
                ];
                return view('partials.action-button')->with($data);
            })
            ->editColumn('created_at', function($data) {
                return date('d M Y', strtotime($data->created_at));
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = ProductAssign::join('products','product_assigns.product_id','=','products.product_code')
            ->join('users','product_assigns.corporate_id','=','users.id')
            ->select(\DB::raw("concat(users.first_name,' ',users.last_name) as corporate"),'users.id as user_id','products.name as product_name','product_assigns.id','product_assigns.created_at')
            ->where('products.user_id',user_info('id'));

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'corporate' => ['name'=>'users.first_name', 'data' => 'corporate', 'title'=>'Corporate', 'id' => 'corporate'],
            'product_name' => ['name'=>'products.name', 'data' => 'product_name', 'title'=>'Product Name', 'id' => 'product_name'],
            'created_at' => ['name'=>'product_assigns.created_at', 'data' => 'created_at', 'title'=>'Assigned At', 'id' => 'created_at'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'master/productassigndatatables_' . time();
    }
}
