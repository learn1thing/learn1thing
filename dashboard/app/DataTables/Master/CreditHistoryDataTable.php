<?php

namespace App\DataTables\Master;

use App\Models\User;
use App\Models\CreditHistory;
use Yajra\Datatables\Services\DataTable;

class CreditHistoryDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('mutation',function($data) {
                return number_format($data->mutation);
            })
            ->editColumn('created_at',function($data) {
                return date('Y-m-d H:i:s',strtotime($data->created_at));
            })
            ->editColumn('status',function($data) {
                if ($data->status == 'pending') {
                    return '<span class="label label-warning">'.$data->status.'</span>';
                } else if ($data->status == 'rejected') {
                    return '<span class="label label-danger">'.$data->status.'</span>';
                } else if ($data->status == 'approved') {
                    return '<span class="label label-success">'.$data->status.'</span>';
                }
            })
            ->addColumn('action', function($data){
                $approved_url = route('admin.credit.approved').'?type=approved';

                $rejected_url = route('admin.credit.approved').'?type=rejected';
                $id = $data->id;
                if ($data->status == 'pending') {
                    return view('partials.action-button')->with(compact('approved_url','rejected_url','id'));
                }

            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = CreditHistory::join('users','users.id','=','credit_histories.user_id')
            ->select(['credit_histories.id','users.first_name','users.last_name','credit_histories.mutation','credit_histories.created_at', 'credit_histories.total_payment', 'credit_histories.status','credit_histories.date_approved','credit_histories.day_expired_credit'])
            ->where('credit_histories.activity','Top Up');
        if (!user_info()->hasAccess('all')) {
            $query = $query->where('credit_histories.user_id',user_info('id'));
        }

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '60px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'created_at' => [
                'id'    =>  'created_at',
                'name' => 'credit_histories.created_at',
                'title' => 'Date',
                'data' => 'created_at',
            ],
            'first_name'  =>  [
                'id'    =>  'first_name',
                'title' =>  'First Name',
                'name'  =>  'users.first_name',
                'data'  =>  'first_name',
            ],
            'last_name'  =>  [
                'id'    =>  'last_name',
                'title' =>  'Last Name',
                'name'  =>  'users.last_name',
                'data'  =>  'last_name',
            ],
            'credit' => ['id' => 'credit',
                'name'=>'credit_histories.mutation', 
                'data' => 'mutation', 
                'title'=>'Credit', 
            ],
            'total_payment' => ['id' => 'total_payment',
                'name'=>'credit_histories.total_payment', 
                'data' => 'total_payment', 
                'title'=>'Total Payment', 
            ],
            'status' => ['id' => 'status',
                'name'=>'credit_histories.status', 
                'data' => 'status', 
                'title'=>'Status', 
            ],
            'date_approved' => ['id' => 'date_approved',
                'name'=>'credit_histories.date_approved', 
                'data' => 'date_approved', 
                'title'=>'Date Approved', 
            ],
            'day_expired_credit' => ['id' => 'day_expired_credit',
                'name'=>'credit_histories.day_expired_credit', 
                'data' => 'day_expired_credit', 
                'title'=>'Days', 
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'master/credithistories_' . time();
    }
}
