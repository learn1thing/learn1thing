<?php

namespace App\DataTables\Master;

use App\Models\StandardCustomer;
use Yajra\Datatables\Services\DataTable;
use App\Models\User;

class StandardCustomerDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('completed', function($user){
                if ($user->completed) {
                    return 'Active';
                } else {
                    return 'Not Active';
                }
            })
            ->addColumn('action', function($user){
                $accept_url = route('admin.standard-customer.active', $user->related_id);
                $edit_url = route('admin.standard-customer.edit', $user->related_id);
                // $deactive_url = route('admin.standard-customer.deactive', $user->related_id);
                $delete_url = route('admin.standard-customer.destroy', $user->related_id);
                return view('partials.action-button')->with(compact('accept_url','user','edit_url','delete_url'));
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        // $query = StandardCustomer::join('users','users.related_id','=','standard_customers.id')
        //     ->join('activations','activations.user_id','=','users.id')
        //     ->select('standard_customers.id','standard_customers.name','standard_customers.address','activations.completed','standard_customers.created_at')
        //     ->where('users.related_type','App\Models\StandardCustomer');

        // return $this->applyScopes($query);

        $query = User::join('role_users','users.id','=','role_users.user_id')
            ->join('roles','role_users.role_id','=','roles.id')
            ->join('activations','activations.user_id','=','users.id')
            ->select('users.id','users.first_name','users.last_name','activations.completed','users.email','users.created_at','users.related_id')
            ->where('roles.slug','=','standard-customer');

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters(array_merge($this->getBuilderParameters(), ["order" => [[ 3, 'desc' ]]]));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        // return [
        //     'id' => ['name'=>'standard_customers.id', 'data' => 'id', 'title'=>'Id', 'id' => 'id'],
        //     'name' => ['name'=>'standard_customers.name', 'data' => 'name', 'title'=>'Name', 'id' => 'name'],
        //     'completed' => ['name'=>'activations.completed', 'data' => 'completed', 'title'=>'Active', 'id' => 'completed'],
        //     'created_at' => ['name'=>'standard_customers.created_at', 'data' => 'created_at', 'title'=>'Created At', 'id' => 'created_at'],
        // ];

        return [
            'first_name' => ['name'=>'users.first_name', 'data' => 'first_name', 'title'=>'First Name', 'id' => 'first_name'],
            'last_name' => ['name'=>'users.last_name', 'data' => 'last_name', 'title'=>'Last Name', 'id' => 'last_name'],
            'email' => ['name'=>'users.email', 'data' => 'email', 'title'=>'Email', 'id' => 'email'],
            'created_at' => ['name'=>'users.created_at', 'data' => 'created_at', 'title'=>'Created At', 'id' => 'created_at'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'master/standardcustomers_' . time();
    }
}
