<?php

namespace App\DataTables\Master;

use App\User;
use Yajra\Datatables\Services\DataTable;
use App\Models\OrderProduct;

class RequestedProductDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function($product){
                if ($product->status == 1) {
                    return '<span class="label label-success">Approved</span>';
                // } elseif ($product->status == 2) {
                } else {
                    // $approve_url = route('admin.corporate-customer.requested-product.approve', ['product_code' => $product->product_code, 'user_id' => $product->user_id]);
                    $approve_url = route('admin.order.send-product-order', ['product_code' => $product->product_code, 'user_id' => $product->user_id]);
                    $decline_url = route('admin.corporate-customer.requested-product.decline', ['product_code' => $product->product_code, 'user_id' => $product->user_id]);
                    return view('partials.action-button')->with(compact('approve_url','decline_url'));
                }
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = OrderProduct::join('products', 'order_products.product_code', '=', 'products.product_code')
                    ->join('order_details', 'order_products.order_detail_id', '=', 'order_details.id')
                    ->join('orders', 'order_details.order_id', '=', 'orders.id')
                    ->join('users', 'order_products.user_id', '=', 'users.id')
                    ->select('users.email','order_products.user_id','products.name','order_products.product_code',\DB::raw("DATE_FORMAT(order_products.updated_at, '%d-%b-%Y') as date"), 'order_products.status')
                    ->where('orders.user_id', user_info('id'))
                    ->where('order_products.user_id', '!=', "")
                    ->where('order_products.status', '!=', 0);

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'updated_at' => ['name'=>'order_products.updated_at', 'data' => 'date', 'title'=>'Date', 'id' => 'date'],          
            'name' => ['name'=>'products.name', 'data' => 'name', 'title'=>'Product', 'id' => 'name'],
            'email' => ['name'=>'users.email', 'data' => 'email', 'title'=>'Requester', 'id' => 'email'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'master/requestedproductdatatables_' . time();
    }
}
