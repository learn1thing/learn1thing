<?php

namespace App\DataTables\Master;

use App\User;
use Yajra\Datatables\Services\DataTable;
use App\Models\Schedule;
use Sentinel;

class MasterScheduleDatatable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function($coach){
                $data = [
                    "delete_url" => route('master.schedule.destroy', $coach->id),
                ];
                return view('partials.action-button')->with($data);
            })
            ->editColumn('start_date', function($data) {
                return date('d M Y', strtotime($data->start_date));
            })
            ->editColumn('end_date', function($data) {
                return date('d M Y', strtotime($data->end_date));
            })
            ->editColumn('interval', function($data) {
                return $data->interval.' '.'Minutes';
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $currentUser = Sentinel::getUser();
        $query = Schedule::where('business_partner_id', 0);
        if ($currentUser->inRole('business-partner')) {
            $userid = $currentUser->related->id;
            $query = Schedule::where('business_partner_id', $userid);            
        } else if ($currentUser->inRole('super-admin')) {
            $query = Schedule::where('business_partner_id', '>', 0);
        }
        
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            // add your columns
            'start_date',
            'end_date',
            'start_time',
            'end_time',
            'interval'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'master/masterscheduledatatables_' . time();
    }
}
