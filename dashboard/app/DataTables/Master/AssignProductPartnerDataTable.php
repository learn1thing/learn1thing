<?php

namespace App\DataTables\Master;

use App\Models\ProductAssign;
use Yajra\Datatables\Services\DataTable;

class AssignProductPartnerDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function($data){
                $data = [
                    "edit_url" => route('admin.assign-product.edit', $data->product_code),
                    "delete_url" => route('admin.assign-product.delete', $data->product_code),
                ];
                return view('partials.action-button')->with($data);
            })
            ->editColumn('partner', function($data) {
                $explode = explode(',', $data->partner);
                return implode('<br> ', $explode);
            })
            ->editColumn('updated_at', function($data) {
                return date('d F Y H:i:s', strtotime($data->updated_at));
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = ProductAssign::join('users','users.related_id','=','product_assigns.business_partner_id')
            ->join('products','products.product_code','=','product_assigns.product_id')
            ->select(\DB::raw("group_concat(users.email) as partner"), 'products.name as product_name',
                'products.product_code', 'product_assigns.updated_at')
            ->where('users.related_type','App\Models\BusinessPartner')
            ->where('product_assigns.corporate_id',0)
            ->groupBy('products.name','product_assigns.updated_at','products.product_code');

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'product_name' => ['name'=>'products.name', 'data' => 'product_name', 'title'=>'Product Name', 'id' => 'product_name'],
            'partner' => ['name'=>'users.first_name', 'data' => 'partner', 'title'=>'Partner', 'id' => 'partner'],
            'updated_at' => ['name'=>'product_assigns.updated_at', 'data' => 'updated_at', 'title'=>'Date', 'id' => 'updated_at'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'master/assignproductpartners_' . time();
    }
}
