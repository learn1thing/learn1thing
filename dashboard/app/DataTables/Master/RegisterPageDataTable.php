<?php

namespace App\DataTables\Master;

use App\Models\RegisterPage;
use Yajra\Datatables\Services\DataTable;

class RegisterPageDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('image', function($data) {
                $image = $data->image;
                return '<img src="'.env('APP_URL').'/'.$image.'" style="max-width:100px;max-height:100px" />';
            })
            ->addColumn('action', function($data){
                $edit_url = route('admin.register-page.edit', $data->id);
                return view('partials.action-button')->with(compact('edit_url'));
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        return $this->applyScopes(RegisterPage::query());
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get Builder Parameter.
     * 
     * @return array
     */
    public function getBuilderParameters()
    {
        return [
            'order'   => [[0, 'asc']],
            'dom' => 'Bfrtip'
        ];
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'image'  =>  [
                'id'    =>  'image',
                'title' =>  'Image',
                'name'  =>  'image',
                'data'  =>  'image',
            ],
            'title' => [
                'id'    =>  'title',
                'name' => 'title',
                'title' => 'Title',
                'data' => 'title',
            ],
            'description'  =>  [
                'orderable' => false,
                'searchable' => false,
                'id'    =>  'description',
                'title' =>  'Description',
                'name'  =>  'description',
                'data'  =>  'description',
            ],
            'created_at' => ['id' => 'created_at',
                    'name'=>'created_at', 
                    'data' => 'created_at', 
                    'title'=>'Created At', 
                    ],
            'updated_at' => ['id' => 'updated_at',
                    'name'=>'updated_at', 
                    'data' => 'updated_at', 
                    'title'=>'Updated At', 
                ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'master/departments_' . time();
    }
}
