<?php

namespace App\DataTables\Master;

use App\Models\User;
use App\Models\Invoice;
use App\Models\InvoiceDetail;
use Yajra\Datatables\Services\DataTable;

class InvoiceDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('status', function($data) {
                if ($data->status == 1) {
                    return 'Pending';
                } elseif ($data->status == 2) {
                    return 'Approved';
                } else {
                    return 'Rejected';
                }
            })
            ->editColumn('total_payment',function($data) {
                return number_format($data->total_payment);
            })
            ->addColumn('action', function($data){
                return '<a href="'.route('admin.invoice.detail',['month' => $data->month, 'year' => $data->year]).'"><i class="fa fa-eye"></i></a>';
                // $detail_url = route('admin.order.detail', $data->order_id);
                // $status = $this->getStatusApp($data);
                // if ($status == 1) {
                //     if (user_info()->inRole('super-admin') || user_info()->inRole('business-partner')) {
                //         $approve_url = route('admin.order.approve', $data->order_id);
                //         $decline_url = route('admin.order.decline', $data->order_id);
                //         return view('partials.action-button')->with(compact('approve_url','decline_url','detail_url'));
                //     } else {
                //         // $edit_url = route('admin.order.edit', $data->order_id);
                //         // $delete_url = route('admin.order.destroy', $data->order_id);
                //         return view('partials.action-button')->with(compact('detail_url'));
                //     }
                // } else {
                //     return view('partials.action-button')->with(compact('detail_url'));
                // }
            })
            ->editColumn('month', function($data) {
                return $this->convertDateToText($data->month - 1).' '.$data->year;
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Invoice::select(['month','year',\DB::raw('sum(total_payment) as total_payment'),'status'])
            ->groupBy('month','year','status');
        if (user_info()->inRole('corporate-customer')) {
            $query = $query->where('user_id',user_info('id'));
        }

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get Builder Parameter.
     * 
     * @return array
     */
    public function getBuilderParameters()
    {
        return [
            'order'   => [[0, 'desc']],
            'dom' => 'Bfrtip'
        ];
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'month' => [
                'id'    =>  'month',
                'name' => 'month',
                'title' => 'Month',
                'data' => 'month',
            ],
            'total_payment'  =>  [
                'id'    =>  'total_payment',
                'title' =>  'Total Payment',
                'name'  =>  'total_payment',
                'data'  =>  'total_payment',
            ],
            'status' => ['id' => 'status',
                    'name'=>'status', 
                    'data' => 'status', 
                    'title'=>'status Payment', 
                ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'master/invoices_' . time();
    }

    protected function convertDateToText($date)
    {
        $arrMonth = ['January','February','March','April','May','June','July','August','September','October','November','December'];
        return $arrMonth[$date];
    }
}
