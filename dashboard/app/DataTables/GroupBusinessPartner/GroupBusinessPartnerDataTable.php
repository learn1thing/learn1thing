<?php

namespace App\DataTables\GroupBusinessPartner;

use App\Models\BusinessPartnerGroup;
use Yajra\Datatables\Services\DataTable;

class GroupBusinessPartnerDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('name', function($group) {
                if (strtolower($group->name) == 'coach') {
                    return '<p>
                                <span data-toggle="tooltip" title="Role Coach is automatically created by system that is connected to scheduler as default." class="c-fld-tooltip-hint__cue">'.$group->name.' <i class="fa fa-info-circle"></i></span>
                            </p>';
                } else {
                    return $group->name;
                }
            })
            ->addColumn('action', function($group){
                $data = [
                    "edit_url" => route('admin.business-partner.store.group-user.edit', $group->id),
                    "delete_url" => route('admin.business-partner.store.group-user.destroy', $group->id),
                    "permission_url" => route('admin.business-partner.store.group-user.manage', @$group->role->id),
                ];
                if (strtolower($group->name) == 'coach') {
                    $data = [
                        "permission_url" => route('admin.business-partner.store.group-user.manage', @$group->role->id),
                    ];
                }
                    return view('partials.action-button')->with($data);
            })
            ->editColumn('created_at', function($data) {
                return date('d M Y', strtotime($data->created_at));
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        if (user_info()->hasAccess('all')) {
            $query = BusinessPartnerGroup::join('users','users.id','=','business_partner_groups.created_by')
                ->select('business_partner_groups.id','business_partner_groups.name','business_partner_groups.created_at'
                    ,\DB::raw("concat(users.first_name, ' ', users.last_name) as owner"),'users.first_name','users.email');
        } else {
            $query = BusinessPartnerGroup::join('users','users.id','=','business_partner_groups.created_by')
                ->select('business_partner_groups.id','business_partner_groups.name','business_partner_groups.created_at'
                    ,\DB::raw("concat(users.first_name, ' ', users.last_name) as owner"),'users.first_name','users.email');
            if (user_info('parent_id')) {
                $query = $query->where(function($query) {
                    $query->where('created_by',user_info('id'))
                        ->orWhere('created_by',user_info('parent_id'));
                });
            } else {
                $childs = user_info()->downlines()->pluck('id')->toArray();
                $query = $query->where(function($query) use($childs) {
                    $query->where('created_by',user_info('id'))
                        ->orWhereIn('created_by',$childs);
                });
            }
        }

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'name' => ['name'=>'business_partner_groups.name', 'data' => 'name', 'title'=>'Role Name', 'id' => 'name'],
            'first_name' => ['name'=>'users.first_name', 'data' => 'owner', 'title'=>'Owner', 'id' => 'owner'],
            'email' => ['name'=>'users.email', 'data' => 'email', 'title'=>'Email', 'id' => 'email'],
            'created_at' => ['name'=>'business_partner_groups.created_at', 'data' => 'created_at', 'title'=>'Created At', 'id' => 'created_at'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'groupbusinesspartner/groupbusinesspartners_' . time();
    }
}
