<?php

namespace App\DataTables\ProductSet;

use App\User;
use Yajra\Datatables\Services\DataTable;
use App\Models\ProductSet;

class ProductSetDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function($product){
                $data = [
                    // "edit_url" => route('admin.business-partner.product.set.edit', $product->id),
                    "delete_url" => route('admin.business-partner.product.set.destroy', $product->id),
                ];
                return view('partials.action-button')->with($data);
            })
            ->editColumn('created_at', function($data) {
                return date('d M Y', strtotime($data->created_at));
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = ProductSet::where('created_by',user_info('id'));

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            // 'id',
            // add your columns
            'product_name',
            'course_name',
            // 'profiler_name',
            'created_at',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'productset/productsetdatatables_' . time();
    }
}
