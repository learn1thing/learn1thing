<?php

namespace App\Http\Requests\Master\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class StandardCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $req = Request::all();
        $email = 'required|email|unique:users,email';
        $password = 'required|min:8';
        if(!empty(@$req['user_id'])) {
            $id = $req['user_id'];
            $email = 'required|email|unique:users,email,'.$id;
            $password = '';
        }
        return [
            'first_name' => 'required|between:1,32',
            'last_name' => 'required|between:1,32',
            'email' => $email,
            'password' => $password,
            'address_1' => 'required|between:3,128',
            'address_2' => 'between:3,128',
            'city' => 'required|between:2,128',
            'post_code' => 'required|between:2,10',
            'country' => 'required',
            // 'region' => 'required',
        ];
    }
}
