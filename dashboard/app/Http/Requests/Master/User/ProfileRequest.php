<?php

namespace App\Http\Requests\Master\User;

use Illuminate\Foundation\Http\FormRequest;
// use App\Http\Requests\Request;
use Illuminate\Http\Request;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $req = Request::all();
        if(isset($req['id'])) {
            $id = $req['id'];
            return [
                'first_name' => 'required|between:1,32',
                'last_name' => 'required|between:1,32',
                // 'email' => 'required|email|unique:users,email'.($id?",$id" : ''),
                
                'address_1' => 'required|between:3,128',
                'address_2' => 'between:3,128',
                'city' => 'required|between:2,128',
                'post_code' => 'required|between:2,10',
                'country' => 'required',
                'image' => 'mimes:jpg,jpeg,png,gif|max:2000',
                'logo' => 'mimes:jpg,jpeg,png,gif|max:2000',

                'company_name' => 'required_if:customer_group_id,=,4|required_if:customer_group_id,=,2',
                'company_address' => 'required_if:customer_group_id,=,4|required_if:customer_group_id,=,2',
                'company_register_no' => 'required_if:customer_group_id,=,4|required_if:customer_group_id,=,2'
            ];
        } else {
            return [
                'first_name' => 'required|between:1,32',
                'last_name' => 'required|between:1,32',
                // 'email' => 'required|email|unique:users,email',
                
                'address_1' => 'required|between:3,128',
                'address_2' => 'between:3,128',
                'city' => 'required|between:2,128',
                'post_code' => 'required|between:2,10',
                'country' => 'required',
                'image' => 'mimes:jpg,jpeg,png,gif|max:2000',
                'logo' => 'mimes:jpg,jpeg,png,gif|max:2000',

                'company_name' => 'required_if:customer_group_id,=,4|required_if:customer_group_id,=,2',
                'company_address' => 'required_if:customer_group_id,=,4|required_if:customer_group_id,=,2',
                'company_register_no' => 'required_if:customer_group_id,=,4|required_if:customer_group_id,=,2'
            ];
        }

            
    }

    public function messages()
    {
        return [
            'company_name.required_if' => 'The company name field is required',
            'company_address.required_if' => 'The company address field is required',
            'company_register_no.required_if' => 'The company register field is required'
        ];
    }
}
