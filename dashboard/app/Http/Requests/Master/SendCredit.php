<?php

namespace App\Http\Requests\Master;

use Illuminate\Foundation\Http\FormRequest;

class SendCredit extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (user_info()->related_type == 'App\Models\CorporateCustomer') {
            return [
                'mutation' => 'required|numeric',
                // 'corporate' => 'required',
                'total_payment' => 'required|numeric'
            ];
        } else {
            return [
                'mutation' => 'required|numeric',
                'corporate' => 'required',
            ];    
        }
    }
}
