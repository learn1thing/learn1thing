<?php

namespace App\Http\Requests\Master;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class DepartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $req = Request::all();
        $name = 'required|uniqueDepartment:departments,name';
        if(!empty(@$req['id'])) {
            $id = $req['id'];
            $name = 'required|uniqueDepartment:departments,name,'.$id;
        }

        return [
            'name'  => $name
        ];
    }
}
