<?php

namespace App\Http\Requests\Api\v1;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use App\Classes\ApiResponse;

class DepartmentUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'department_id' => 'required|exists:departments,id',
            'customer_ids' => 'required|min:1'
        ];
    }

    public function response(array $errors)
    {
        $data = new ApiResponse([
          'success' => false,
          'message' => trans('auth.unprocessable_entity'),
          'code' => 422
        ], $errors);
        return new JsonResponse($data, 422);
    }
}
