<?php

namespace App\Http\Requests\ExpiredSchedule;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ExpiredScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $req = Request::all();
        $bpId = 'required|unique:expired_schedules,business_partner_id';
        if(!empty(@$req['id'])) {
            $id = $req['id'];
            $bpId = 'required|unique:expired_schedules,business_partner_id,'.$id;
        }

        return [
            'business_partner_id' => $bpId,
            'expired' => 'required',
        ];
    }
}
