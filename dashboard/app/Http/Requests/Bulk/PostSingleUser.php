<?php

namespace App\Http\Requests\Bulk;

use Illuminate\Foundation\Http\FormRequest;

class PostSingleUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $array = ['admin.business-partner.bulk.edit', 'admin.business-partner.bulk.update'];
        if (in_array($this->route()->getName(), $array)) {
            return [
                'first_name' => 'required',
                'email' => 'required|email|unique:users,email',
                'related_id' => 'required',
                'country' => 'required'
            ];
        } else {
            return [
                'first_name' => 'required',
                'email' => 'required|email|unique:users,email',
                'related_id' => 'required',
                // 'department_id' => 'required',
                'password' => 'required|min:8',
                'country' => 'required'
            ];
        }
    }

    public function response(array $errors){
        return \Redirect::back()->withErrors($errors)->withInput();
    }
}
