<?php

namespace App\Http\Requests\BusinessPartner;

use Illuminate\Foundation\Http\FormRequest;

class BusinessPartnerGroup extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|iunique:business_partner_groups,name'
        ];
    }

    public function response(array $errors){
        return \Redirect::back()->withErrors($errors)->withInput();
    }
}
