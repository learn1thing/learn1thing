<?php

namespace App\Http\Requests\Schedule;

use Illuminate\Foundation\Http\FormRequest;
use App\Classes\ApiResponse;
use Illuminate\Http\JsonResponse;

class ScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'course_id' => 'required',
            'schedule_date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'location' => 'required',
            'comments' => 'required',
            'choice' => 'required',
            'devine_slots' => 'required',
            'break_slots' => 'required',
            'overlap_slots' => 'required',
            'multiple_allow' => 'required',
            'display_appointment' => 'required',
            'email_reminder' => 'required',
            'status' => 'required',
            'actived' => 'required',
        ];
    }

    public function response(array $errors)
    {
        $data = new ApiResponse([
          'success' => false,
          'message' => trans('auth.unprocessable_entity'),
          'code' => 422
        ], $errors);
        return new JsonResponse($data, 422);
    }
}
