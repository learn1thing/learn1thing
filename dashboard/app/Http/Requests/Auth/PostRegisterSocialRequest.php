<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class PostRegisterSocialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'terms' => 'required',
            'company_name' => 'required_if:customer_group_id,=,4|required_if:customer_group_id,=,2',
            'company_address' => 'required_if:customer_group_id,=,4|required_if:customer_group_id,=,2',
            'company_register_no' => 'required_if:customer_group_id,=,4|required_if:customer_group_id,=,2'
        ];
    }

    public function messages()
    {
        return [
            'company_name.required_if' => 'The company name field is required',
            'company_address.required_if' => 'The company address field is required',
            'company_register_no.required_if' => 'The company register field is required'
        ];
    }
}
