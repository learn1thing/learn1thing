<?php

namespace App\Http\Requests\Auth;

use Illuminate\Http\JsonResponse;
use Illuminate\Foundation\Http\FormRequest;
use App\Classes\ApiResponse;
class PostLoginReqeust extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required',
            'remember' => 'in:0,1'
        ];
    }

    public function response(array $errors)
    {
        $data = new ApiResponse([
          'success' => false,
          'message' => trans('auth.unprocessable_entity'),
          'code' => 422
        ], $errors);
        return new JsonResponse($data, 422);
    }
}
