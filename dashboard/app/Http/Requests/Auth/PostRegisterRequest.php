<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use App\Classes\ApiResponse;

class PostRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

            return [
                'first_name' => 'required|between:1,32',
                'last_name' => 'required|between:1,32',
                'email' => 'required|email|unique:users,email',
                
                //'address_1' => 'required|between:3,128',
                //'address_2' => 'between:3,128',
                'city' => 'required|between:2,128',
                'post_code' => 'required|between:2,10',
                'country' => 'required',
              //  'region' => 'required',

                'password' => 'required|min: 8',
                'password_confirm' => 'required|min: 8|same:password',

                'terms' => 'required',

                'company_name' => 'required_if:customer_group_id,=,4|required_if:customer_group_id,=,2',
                'company_address' => 'required_if:customer_group_id,=,4|required_if:customer_group_id,=,2',
                'company_register_no' => 'required_if:customer_group_id,=,4|required_if:customer_group_id,=,2',
                'payment_method_id' => 'required_if:customer_group_id,=,4',
            ];
    }

    public function messages()
    {
        return [
            'company_name.required_if' => 'The company name field is required',
            'company_address.required_if' => 'The company address field is required',
            'company_register_no.required_if' => 'The company register field is required',
            'payment_method_id.required_if' => 'The payment method field is required',
        ];
    }

    public function response(array $errors)
    {
        $data = new ApiResponse([
          'success' => false,
          'message' => trans('auth.unprocessable_entity'),
          'code' => 422
        ], $errors);
        return new JsonResponse($data, 422);
    }
}
