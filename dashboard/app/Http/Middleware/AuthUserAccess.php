<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use GuzzleHttp\Client;

class AuthUserAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $this->getTokenFromReqeust($request);
        $user = Sentinel::findByPersistenceCode($token);
        if (!$user) {

          if (cache()->has($token)) {
            $response = [
              'status' => 'error',
              'error' => cache()->get($token)
            ];
            cache()->forget($token);
          } else {
            $response = [
              'status' => 'error',
              'error' => session()->get('message') ? session()->get('message') : 'You don\'t have credentials.'
            ];
          }
          if ($request->header('content-type') == 'application/json') {
              return response()->json($response, 401);
          } else {
              session()->flash('alert-class', 'alert-danger');
              session()->flash('message', $response['error']);
              return redirect()->route('login');
          }
        } else {
          $persistences = $user->persistences->first();
          if ($user->hasAccess('all')) {
            return $next($request);
          }

          # Code By Nova
          if ($user->parent_id) {
              $user = Sentinel::findById($user->parent_id);
              if ($user->parent_id) {
                  $user = Sentinel::findById($user->parent_id);
              }
          }
          # End Code
          
          if (@$request->header('is_survey')) {
            if (!$user->inRole('business-partner')) {
              return response()->json(['status' => 'error', 'message' => 'Unauthorized'], 401);
            }
          }

          if (@$request->header('is_moodle')) {
            if (!$user->inRole('business-partner')) {
              return response()->json(['status' => 'error', 'message' => 'Unauthorized'], 401);
            }
          }
        }

        return $next($request);
    }
    /**
     * Update Ip if ip has been changed
     * @param  \Caralyst\Sentine\Persistences\EloquentPersistence $persistences
     * @param  string $ipAddress
     * @return void
     */
    public function updateIp($persistences, $ipAddress)
    {
      if ($persistences->ip_address != $ipAddress) {
        $persistences->ip_address = $ipAddress;
        $persistences->save();
      }
    }

    /**
     * Get token from request
     * @param  \Illuminate\Http\Request $request
     * @return string
     */
    public function getTokenFromReqeust($request)
    {
      $token = '';
      if ($request->get('token')) {
        $token = $request->get('token');
      } else if ($request->header('Authorization')) {
        $token = $request->header('Authorization');
      } else if (session()->has('cartalyst_sentinel') && Sentinel::findByPersistenceCode(session()->get('cartalyst_sentinel'))) {
        $token = session()->get('cartalyst_sentinel');
      } else if ($request->cookie('token') && Sentinel::findByPersistenceCode($request->cookie('token'))) {
        $token = $request->cookie('token');
        session()->put('cartalyst_sentinel', $token);
      }

      return $token;
    }
}
