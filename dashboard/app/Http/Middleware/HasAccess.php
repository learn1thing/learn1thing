<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;

class HasAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission, $permission1 = null, $permission2 = null)
    {
        $access = [$permission,$permission1,$permission2];
        if (Sentinel::hasAccess('all')) {
          return $next($request);
        }

        if (! Sentinel::hasAnyAccess($access)) {
           if ($request->ajax() || $request->wantsJson()) {
               return response('Unauthorized.', 403);
           }

           return response()->view('errors.401');
       }

       return $next($request);
    }
}
