<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\StreamedResponse;
use App\Models\BulkLog;

class HomeController extends Controller
{
    public function getCountryDetail($id)
    {
        $client = new Client();
        $get_country_detail = $client->request('GET', env('OC_BASE_URL').'/index.php?route=feed/rest_api/countries&id='.$id, [
        'headers' => [
          'X-Oc-Merchant-Id' => 12345,
          'id' => $id,
        ]
      ]);
      $country_detail = json_decode($get_country_detail->getBody())->data;
      return response()->json(['status' => true,'data' => $country_detail->zone],200);
    }

    public function checkBulkLog(Request $request)
    {
        if (@$request->id) {
          $log = new BulkLog;
          $log->where('id',$request->id)->update(['status' => true]);
          return 'true';
        }

        $response = new StreamedResponse(function()
        {
            $log = new BulkLog;
            if ( user_info() ) {
                $get_log = $log->getLogRealTime(user_info('id'));
                echo "data: " . json_encode($get_log). "\n\n";
                flush();
            } else {
                echo "data: no notification\n\n";
                flush();
            }
        });

        $response->headers->set('Content-Type', 'text/event-stream');
        $response->headers->set('Cache-Control', 'no-cache');

        return $response;
    }
}
