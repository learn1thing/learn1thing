<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Admin\Master\OrderController;
use App\Models\Department;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\ProductSet;
use App\Models\InvoiceDetail;
use App\Models\OrderProduct;
use App\Models\User;
use App\Models\Invoice;
use App\Models\Order;
use GuzzleHttp\Client;
use App\Classes\CurlRequest;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->orderHistory = new OrderController;
        $this->user = new \App\Models\User;
        $this->order = new Order;
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $orderHistory = $this->orderHistory->orderHistory($request,true);
            $business = $this->user->where('related_type','App\Models\BusinessPartner')->count();
            $corporates = $this->user->where('related_type','App\Models\CorporateCustomer')->count();
            $standards = $this->user->where('related_type','App\Models\StandardCustomer')->count();
            return json_encode([
                'status' => true,
                'order' => number_format(count($orderHistory)),
                'business' => number_format($business),
                'corporates' => number_format($corporates),
                'standards' => number_format($standards),
            ]);
        }

        return view('dashboard_static');
    }

    /**
     * [request for get analityc data]
     * @author Rangga <ranggapl@windowslive.com>
     * @return [type]           [description]
     */
    public function analytic(Request $request)
    {
        $result = array();
        try {
            // super-admin, standard-customer, corporate-customer, business-partner
            $userId = user_info('id');
            if(user_info()->inRole('standard-customer')) {

                // $orderHistory = $this->orderHistory->orderHistory($request,true);

                // get order by status
                $responses = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/order_admin/userorders&user='.user_info('customer_id'));
                $orderPending = 0;
                $completeOrders = 0;
                $orderCourse = 0;
                $orderProfiler = 0;
                $productBuyPerCategory = [];
                $date_added = [];
                $dataExpiredProd = [];
                $productIds = array();

                if ($responses->success) {
                    foreach ($responses->data as $key => $order) {
                        if($order->status == "Completed" || $order->status == "Complete") {
                            $completeOrders += intval($order->total);
                        }
                    }
                }

                // get order by type(course & profiler)
                $orderHistory = $this->orderHistory->orderHistory($request,true);
                if($orderHistory) {
                    foreach($orderHistory as $order) {
                        foreach($order['products'] as $product) {
                            array_push($productIds, $product->product_id);
                        }
                    }
                    $productSet = new ProductSet;
                    $orderCourse = $productSet->orderCountByType('course', $productIds);
                    $orderProfiler = $productSet->orderCountByType('profiler', $productIds);
                }

                // get category
                $categories = [];
                $dataCategories = [];
                $responses = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/category_admin/category');
                if ($responses->success) {
                    foreach ($responses->data->categories as $key => $value) {
                        $categories[$key]['id'] = $value->category_id;
                        $categories[$key]['name'] = $value->name;
                        $dataCategories[] = $value->name;
                    }
                }

                foreach($orderHistory as $order) {
                    if(isset($order['products'])) {
                        foreach($order['products'] as $product) {
                            $productIds[] = $product->product_id;
                            $dataExpired = date('Y-m-d', strtotime($order['info']->date_added));

                            $dataExpiredName['prod_name'] = $product->name;
                            $dataExpiredName['prod_date'] = date('d-m-Y', strtotime($dataExpired. ' + 30 days'));

                            array_push($dataExpiredProd, $dataExpiredName);
                        }
                    }                        
                

                    if(isset($order['info'])) {
                        if (date('Y-m', strtotime($order['info']->date_added)) == date('Y-m')) {
                            $totalOrder = (int)$order['total_order'];
                            if (!in_array(date('Y-m-d', strtotime($order['info']->date_added)), $date_added)) {
                                $date_added[] = date('Y-m-d', strtotime($order['info']->date_added));
                                $purchaseMonthly[] = $totalOrder;
                            } else {
                                $key = array_search(date('Y-m-d', strtotime($order['info']->date_added)), $date_added);
                                $purchaseMonthly[$key] += $totalOrder;
                            }
                        }

                    }
                }

                foreach ($categories as $key => $category) {
                    $x = 0 ;
                    $productDetail = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/product_admin/products&category='.$category['id']);
                    if (@$productDetail->success) {
                        $productBuyPerCategory[$key]['name'] = $category['name'];
                        foreach ($productDetail->data as $dataProduct) {
                            if (in_array($dataProduct->id, $productIds)) {
                                $x++;
                            }
                        }
                        $productBuyPerCategory[$key]['y'] = $x;
                    } else {
                        $productBuyPerCategory[$key] = ['name' => $category['name'], 'y' => $x];
                    }
                }


                // Product Bought
                $productBoughts = [];

                foreach ($dataExpiredProd as $key => $value) {
                    
                    if(count($productBoughts) < 10){
                        $dataEx['name'] = $value['prod_name'];
                        $dataEx['expired'] = $value['prod_date'];

                        array_push($productBoughts, $dataEx);    
                    }
                     
           
                }

                $htmlProductBought = '<tr>
                                <td colspan="5" align="center">No Available</td>
                            </tr>';
                if (count($productBoughts) > 0 ) {
                    $htmlProductBought = '';
                    foreach ($productBoughts as $key => $course) {
                        // print_r($course);die();
                        $htmlProductBought .= '<tr>
                                <td>'.$course['name'].'</td>
                                <td>'.$course['expired'].'</td>
                            </tr>';
                    }
                }

                $htmlProductStart = '<tr>
                                <td colspan="5" align="center">No Available</td>
                            </tr>';
                if (count($productBoughts) > 0 ) {
                    $htmlProductStart = '';
                    foreach ($productBoughts as $key => $course) {
                        // print_r($course);die();
                        $htmlProductStart .= '<tr>
                                <td>'.$course['name'].'</td>
                            </tr>';
                    }
                }

                //Recomended Products
                $client = new Client();
                $get_recomended = $client->request('GET', env('OC_BASE_URL').'/index.php?route=feed/rest_api/bestsellers&limit=10', [
                    'headers' => [
                      'X-Oc-Merchant-Id' => 12345,
                    ]
                  ]);
                $rec = json_decode($get_recomended->getBody())->data;

                // print_r($rec->products);die();
                $htmlProductRec = '<tr>
                                <td colspan="5" align="center">No Available</td>
                            </tr>';
                if (count($rec) > 0 ) {
                    $htmlProductRec = '';
                    foreach ($rec->products as $key => $product) {
                        $htmlProductRec .= '<tr>
                                <td>'.$product->name.'</td>
                            </tr>';
                    }
                }

                $result = array(
                    'status' => true,
                    //edited by k5 5/12/17
                    'order_completed' => '$'.number_format($completeOrders,0,'.',','),
                    //'order_completed' => '$'.number_format($completeOrders,2,',','.'),
                    // 'order_pending' => $orderPending,
                    'order_course' => $orderCourse,
                    'order_profiler' => $orderProfiler,
                    'product_buy_per_category' => $productBuyPerCategory,
                    'product_expired' => $orderCourse,
                    'html_product_bought' => $htmlProductBought,
                    'html_product_start' => $htmlProductStart,
                    'html_product_rec' => $htmlProductRec,
                    'recomended' => count($rec->products),
                );
            } else if(user_info()->inRole('corporate-customer')) {
                $productIds = [];
                $completeOrders = 0;
                $productBuyPerCategory = [];
                $purchaseMonthly = [];
                $date_added = [];
                
                // get orderHistory
                $orderHistory = $this->orderHistory->orderHistory($request,true);

                // print_r($orderHistory);die();
                // get order by status
                $responses = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/order_admin/userorders&user='.user_info('customer_id'));

                $productCounts = OrderDetail::join('orders', 'orders.id', '=', 'order_details.order_id')
                    ->where('orders.type', 'bulk')
                    ->where('orders.user_id',user_info('id'))
                    ->whereRaw('order_details.created_at + INTERVAL 30 DAY < NOW()')
                    ->selectRaw('sum(order_details.quantity) as total_product')
                    ->first();

                if ($responses->success) {
                    foreach ($responses->data as $key => $order) {
                        if($order->status == "Completed" || $order->status == "Complete") {
                            $completeOrders += intval($order->total);
                        }
                    }
                }

                // $userCounts = User::where('related_type', 'App\Models\CorporateGroup')
                    // ->join('corporate_groups', 'users.related_id', '=', 'corporate_groups.id')
                    // ->where('corporate_groups.created_by', $userId)
                    // ->count();


                // get category
                $categories = [];
                $dataCategories = [];
                $responses = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/category_admin/category');
                if ($responses->success) {
                    foreach ($responses->data->categories as $key => $value) {
                        $categories[$key]['id'] = $value->category_id;
                        $categories[$key]['name'] = $value->name;
                        $dataCategories[] = $value->name;
                    }
                }

                $arrVar = [];
                foreach($orderHistory as $order) {
                    // if(isset($order['products'])) {
                        foreach($order['products'] as $product) {
                            if (in_array($product->product_id, $productIds)) {
                                $arrVar[$product->product_id] += $product->quantity;
                            } else {
                                array_push($productIds, $product->product_id);
                                $arrVar[$product->product_id] = $product->quantity;
                            }
                        }
                    // }

                    if(isset($order['info'])) {
                        if (date('Y-m', strtotime($order['info']->date_added)) == date('Y-m')) {
                            $totalOrder = (int)$order['total_order'];
                            if (!in_array(date('Y-m-d', strtotime($order['info']->date_added)), $date_added)) {
                                $date_added[] = date('Y-m-d', strtotime($order['info']->date_added));
                                $purchaseMonthly[] = $totalOrder;
                            } else {
                                $key = array_search(date('Y-m-d', strtotime($order['info']->date_added)), $date_added);
                                $purchaseMonthly[$key] += $totalOrder;
                            }
                        }
                    }
                }

                foreach ($categories as $key => $category) {
                    $x = 0 ;
                    $productDetail = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/product_admin/products&category='.$category['id']);
                    if (@$productDetail->success) {
                        $productBuyPerCategory[$key]['name'] = $category['name'];
                        foreach ($productDetail->data as $dataProduct) {
                            if (in_array($dataProduct->id, $productIds)) {
                                $x += $arrVar[$dataProduct->id];
                            }
                        }
                        $productBuyPerCategory[$key]['y'] = $x;
                    } else {
                        $productBuyPerCategory[$key] = ['name' => $category['name'], 'y' => $x];
                    }
                }

                // $departmentCounts = Department::where('user_id', $userId)->count();

                // Product left to be assigned
                $productLeftToBeAssigned = Order::productLeftToBeAssigned()->select(\DB::raw('SUM(CASE WHEN order_products.status = 0 THEN 1 ELSE 0 END) product_left'))->first();
                $productLeftToBeAssignedGets = Order::productLeftToBeAssigned()->select('products.name',
                        \DB::raw('SUM(CASE WHEN order_products.status = 0 THEN 1 ELSE 0 END) unassigned'),
                        \DB::raw('SUM(CASE WHEN order_products.status = 2 THEN 1 ELSE 0 END) assigned'),
                        \DB::raw('SUM(CASE WHEN order_products.status = 1 THEN 1 ELSE 0 END) complete')
                    )
                    ->groupBy('products.name')
                    ->get();
                $htmlProductLeftToBeAssigned = '<tr>
                                <td colspan="5" align="center">No Available</td>
                            </tr>';
                if (count($productLeftToBeAssignedGets) > 0) {
                    $htmlProductLeftToBeAssigned = '';
                    foreach ($productLeftToBeAssignedGets as $key => $assigned) {
                        $htmlProductLeftToBeAssigned .= '<tr>
                                <td>'.($key+1).'</td>
                                <td>'.$assigned->name.'</td>
                                <td class="text-center">'.$assigned->assigned.'</td>
                                <td class="text-center">'.$assigned->unassigned.'</td>
                                <td class="text-center">'.$assigned->complete.'</td>
                            </tr>';
                    }
                }


                // Course Bought
                $courseBoughts = [];

                foreach ($productIds as $key => $value) {
                   $dataCourse = ProductSet::join('order_products','product_sets.product_id', '=', 'order_products.product_code')
                   ->select(\DB::raw('DATE(order_products.created_at) + INTERVAL 30 DAY as expired'),'product_sets.course_name')
                   ->where('product_sets.product_id', $value)->first();
                   if($dataCourse){
                        $dataEx['name'] = $dataCourse->course_name;
                        $dataEx['expired'] = $dataCourse->expired;

                        array_push($courseBoughts, $dataEx);    
                   }    
           
                }

                // print_r($courseBoughts);die();

                $htmlCourseBought = '<tr>
                                <td colspan="5" align="center">No Available</td>
                            </tr>';
                if (count($courseBoughts) > 0 ) {
                    $htmlCourseBought = '';
                    foreach ($courseBoughts as $key => $course) {
                        // print_r($course);die();
                        $htmlCourseBought .= '<tr>
                                <td>'.$course['name'].'</td>
                                <td>'.$course['expired'].'</td>
                            </tr>';
                    }
                }

                $payment_summary = 0;
                if (strtolower(@user_info('payment_method')->name) == 'credit based system') {
                    $payment_summary = '<i class="fa fa-money"></i> '.number_format(user_info('credit'));
                } elseif (strtolower(@user_info('payment_method')->name) == 'pay as you go') {
                    $invoice = Invoice::invoicesByUser(user_info('id'))->select(\DB::raw('SUM(invoices.total_payment) as total_payment'))->first();
                    
                    //$payment_summary = '$'.number_format(@$invoice->total_payment,2,',','.');
                    //edited by k5 5/12/17
                    $payment_summary = '$'.number_format(@$invoice->total_payment,0,'.',',');
                }

                $result = array(
                    'status' => true,
                    //edited by k5 5/12/17
                    'order_completed' => '$'.number_format($completeOrders,0,'.',','),
                    //'order_completed' => '$'.number_format($completeOrders,2,',','.'),
                    'product_counts' => count($courseBoughts),
                    // 'user_counts' => $userCounts,
                    // 'department_counts' => $departmentCounts,
                    'categories' => $dataCategories,
                    'product_buy_per_category' => $productBuyPerCategory,
                    'date_added' => array_reverse($date_added),
                    'purchase_monthly' => array_reverse($purchaseMonthly),
                    'product_left' => number_format(@$productLeftToBeAssigned->product_left),
                    'html_product_left' => $htmlProductLeftToBeAssigned,
                    'html_course_bought' => $htmlCourseBought,
                    'payment_summary' => $payment_summary
                );
            } else if(user_info()->inRole('business-partner')) {
                $productsBP = Product::where('user_id', $userId)->pluck('product_code')->toArray();
                $productCounts = Product::where('user_id', $userId)->count();
                $orderHistory = $this->orderHistory->orderHistory($request,true);
                $productSoldCounts = 0;
                $totalMoney = 0;
                $customerIds = array();
                $tempCusIds = array();
                $incomeDaily = [];
                $timeIncomeDaily = [];
                $date_added = [];
                $salesPerCategory = [];
                $productIds = [];
                $customerCountries = [];
                $tempCusCountries = [];
                $customerB2bB2c = [];
                $productName = [];
                $topFiveProduct = [];
                $top_five_products = [];
                $countListActionRequired = 0;

                // breakdown customer by country
                if (@$request->type == 'country') {
                    if($orderHistory) {
                        foreach($orderHistory as $num => $order) {
                            if(isset($order['info'])) {
                                // consumer by country
                                if (!in_array($order['info']->customer_id, $tempCusIds)) {
                                    $tempCusIds[$num] = $order['info']->customer_id;
                                    if (in_array($order['info']->payment_country, $tempCusCountries)) {
                                        $key = array_search($order['info']->payment_country, $tempCusCountries);
                                        $customerCountries[$key]['y'] += 1;
                                    } else {
                                        $tempCusCountries[$num] = $order['info']->payment_country;
                                        $customerCountries[$num]['name'] = $order['info']->payment_country;
                                        $customerCountries[$num]['y'] = 1;
                                    }
                                }
                            }
                        }
                        $result = array(
                            'status' => true,
                            'data' => array_values($customerCountries)
                        );

                        return response()->json($result, 200);
                    }
                }

                // breakdown customer by b2b/b2c
                if (@$request->type == 'b2b_b2c') {
                    if($orderHistory) {
                        $b2b = 0;
                        $b2c = 0;
                        $tempB2b = [];
                        $tempB2c = [];
                        $customerB2bB2c = [
                            [
                                'name' => 'Customer B2B',
                                'y' => 0,
                            ],
                            [
                                'name' => 'Customer B2C',
                                'y' => 0,
                            ]
                        ];

                        foreach($orderHistory as $num => $order) {
                            if(isset($order['info'])) {
                                if (!in_array($order['info']->customer_id, $tempCusIds)) {
                                    $tempCusIds[$num] = $order['info']->customer_id;
                                    $customerDetail = $this->user->where('customer_id', $order['info']->customer_id)->first();
                                    if (@$customerDetail->related_type == 'App\Models\CorporateCustomer') {
                                        $customerB2bB2c[0]['y'] += 1;
                                    } elseif (@$customerDetail->related_type == 'App\Models\StandardCustomer') {
                                        $customerB2bB2c[1]['y'] += 1;
                                    
                                    }
                                }
                            }
                        }
                    }

                    $result = array(
                        'status' => true,
                        'data' => array_values($customerB2bB2c)
                    );

                    return response()->json($result, 200);
                }

                //breakdown purchase by product
                if (@$request->type == 'product') {
                    $productIds = [];
                    $sendData = [];
                    $result = array(
                        'status' => false,
                        'data' => [],
                    );
                    
                    if($orderHistory) {
                        foreach($orderHistory as $num => $order) {
                            if(isset($order['product'])) {
                                foreach($order['product'] as $product) {
                                    // $productIds[] = $product->product_id;
                                    if (in_array($product->product_id, $productIds)) {
                                        $key = array_search($product->product_id, $productIds);
                                        $sendData[$key]['y'] += 1;
                                    } else {
                                        $productIds[$num] = $product->product_id;
                                        $sendData[$num]['name'] = $product->name;
                                        $sendData[$num]['y'] = 1;
                                    }
                                }
                            }
                        }

                        $result = array(
                            'status' => true,
                            'data' => array_values($sendData),
                        );
                    }

                    return response()->json($result, 200);
                }

                //breakdown purchase by category
                if (@$request->type == 'category') {
                    $result = array(
                        'status' => false,
                        'data' => [],
                    );
                    // get category
                    $categories = $this->getProductCategories();
                    if($orderHistory) {
                        foreach($orderHistory as $num => $order) {
                            if(isset($order['product'])) {
                                foreach($order['product'] as $product) {
                                    $productIds[] = $product->product_id;
                                }
                            }
                        }

                        foreach ($categories as $key => $category) {
                            $x = 0 ;
                            $productDetail = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/product_admin/products&category='.$category['id']);
                            if (@$productDetail->success) {
                                $salesPerCategory[$key]['name'] = $category['name'];
                                foreach ($productDetail->data as $dataProduct) {
                                    if (in_array($dataProduct->id, $productIds)) {
                                        $x++;
                                    }
                                }
                                $salesPerCategory[$key]['y'] = $x;
                            } else {
                                $salesPerCategory[$key] = ['name' => $category['name'], 'y' => $x];
                            }
                        }

                        $result = array(
                            'status' => true,
                            'data' => $salesPerCategory,
                        );

                    }
                    return response()->json($result, 200);
                }

                // get category
                $categories = $this->getProductCategories();

                if($orderHistory) {
                    $numInc = 0;
                    foreach($orderHistory as $num => $order) {
                        if(isset($order['product'])) {
                            foreach($order['product'] as $product) {

                                $productSoldCounts += number_format($product->quantity);
                                $totalMoney += $product->total;
                                $productIds[] = $product->product_id;

                                if (in_array($product->name, $productName)) {
                                    $key = array_search($product->name, $productName);
                                    $topFiveProduct[$key]['product_sold'] += $productSoldCounts;
                                } else {
                                    $productName[$numInc] = $product->name;
                                    $topFiveProduct[$numInc]['product_name'] = $product->name;
                                    $topFiveProduct[$numInc]['product_sold'] = $productSoldCounts;
                                    $numInc++;
                                }
                            }
                        }

                        if(isset($order['info'])) {
                            array_push($customerIds, $order['info']->customer_id);
                            if (date('Y-m', strtotime($order['info']->date_added)) == date('Y-m')) {
                                $totalOrder = $order['total_order'];
                                if (!in_array(date('Y-m-d', strtotime($order['info']->date_added)), $date_added)) {
                                    $date_added[] = date('Y-m-d', strtotime($order['info']->date_added));
                                    $incomeDaily[] = $totalOrder;
                                } else {
                                    $key = array_search(date('Y-m-d', strtotime($order['info']->date_added)), $date_added);
                                    $incomeDaily[$key] += $totalOrder;
                                }
                            }

                            // consumer by country
                            if (!in_array($order['info']->customer_id, $tempCusIds)) {
                                $tempCusIds[$num] = $order['info']->customer_id;
                                if (in_array($order['info']->payment_country, $tempCusCountries)) {
                                    $key = array_search($order['info']->payment_country, $tempCusCountries);
                                    $customerCountries[$key]['y'] += 1;
                                } else {
                                    $tempCusCountries[$num] = $order['info']->payment_country;
                                    $customerCountries[$num]['name'] = $order['info']->payment_country;
                                    $customerCountries[$num]['y'] = 1;
                                }
                            }
                        }
                    }
                }

                foreach ($categories as $key => $category) {
                    $x = 0 ;
                    $productDetail = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/product_admin/products&category='.$category['id']);
                    if (@$productDetail->success) {
                        $salesPerCategory[$key]['name'] = $category['name'];
                        foreach ($productDetail->data as $dataProduct) {
                            if (in_array($dataProduct->id, $productIds)) {
                                $x++;
                            }
                        }
                        $salesPerCategory[$key]['y'] = $x;
                    } else {
                        $salesPerCategory[$key] = ['name' => $category['name'], 'y' => $x];
                    }
                }

                // $orderNeedApprove = $this->order->myOrderDatatable(user_info('id'))->where('order_details.status',1);
                // $countListActionRequired = count($orderNeedApprove->get());
                // if ($orderNeedApprove->count() > 0) {
                //     $htmlListrequired = '';
                //     foreach ($orderNeedApprove->get() as $key => $orderNeed) {
                //         $htmlListrequired .= '<tr>
                //                 <td align="center">'.($key+1).'</td>
                //                 <td>'.$orderNeed->email.'</td>
                //                 <td><span class="label label-success">Need Approval</span></td>
                //             </tr>';
                //     }
                // } else {
                //     $htmlListrequired = '<tr>
                //             <td colspan="3" align="center">Not Available</td>
                //         </tr>';
                // }

                if (count($topFiveProduct) > 0) {
                    $temp = 0;
                    $z = 0;
                    $datas = [];
                    foreach ($topFiveProduct as $productVal) {
                        if (count($top_five_products) <= 5) {
                            $top_five_products[$z] = $productVal;
                            $temp = $productVal['product_sold'];
                            $datas[$z] = $productVal['product_sold'];
                            $z++;
                        } else {
                            foreach ($top_five_products as $keyVal => $val) {
                                if ($productVal['product_sold'] > $val['product_sold']) {
                                    $val[$keyVal] = $productVal;
                                    $datas[$keyVal] = $productVal['product_sold'];
                                }
                            }
                        }
                    }
                    array_multisort($datas,SORT_DESC,$top_five_products);
                }

                if (count($top_five_products) > 0) {
                    $htmlListrequired = '';
                    foreach ($top_five_products as $key => $vals) {
                        $htmlListrequired .= '<tr>
                                <td align="center">'.($key+1).'</td>
                                <td>'.$vals['product_name'].'</td>
                                <td>'.$vals['product_sold'].'</td>
                            </tr>';
                    }
                } else {
                    $htmlListrequired = '<tr>
                            <td colspan="3" align="center">Not Available</td>
                        </tr>';
                }

                $result = array(
                    'status' => true,
                    'product_counts' => number_format($productCounts),
                    'product_sold_counts' => $productSoldCounts,
                    'customer_counts' => count(array_unique($customerIds)),
                    //'total_money' => '$'.number_format($totalMoney,2,',','.'),
                    //edited by puw k5 5/12/17
                    'total_money' => '$'.number_format($totalMoney,0,'.',','),
                    'income_daily' => $incomeDaily,
                    'time_income_daily' => $date_added,
                    'sales_per_category' => $salesPerCategory,
                    // 'number_action_required' => number_format($orderNeedApprove->count()),
                    'number_action_required' => 0,
                    'list_action_required' => $htmlListrequired,
                    'count_list_action_required' => number_format($countListActionRequired),
                    'customer_countries' => array_values($customerCountries),
                    'top_five_products' => array_values($top_five_products)
                );
            } else if(user_info()->inRole('super-admin')) {
                $incomeWeekly = [];
                $date_added = [];
                $salesPerCategory = [];
                $productIds = [];
                $totalMoney = 0;
                    
                $start_date = date('Y-m-d',strtotime('first day of this month'));
                $end_date = date('Y-m-d',strtotime('last day of this month'));
                $end_date1 = date('Y-m-d', strtotime($end_date.' + 6 days'));
                $week = 0;
                for($date = $start_date; $date < $end_date1; $date = date('Y-m-d', strtotime($date. ' + 8 days')))
                {
                    $week++;
                    $weeks['Week '.$week] = $this->getWeekDates($date, $start_date, $end_date);
                }

                $orderHistory = $this->orderHistory->orderHistory($request,true);
                $business = $this->user->where('related_type','App\Models\BusinessPartner')->count();
                $corporates = $this->user->where('related_type','App\Models\CorporateCustomer')->count();
                $standards = $this->user->where('related_type','App\Models\StandardCustomer')->count();


                // get category
                $categories = [];
                $responses = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/category_admin/category');                
                if ($responses->success) {
                    foreach ($responses->data->categories as $key => $value) {
                        $categories[$key]['id'] = $value->category_id;
                        $categories[$key]['name'] = $value->name;
                    }
                }

                if($orderHistory) {
                    foreach($orderHistory as $keyOrder => $order) {
                        if(isset($order['product'])) {
                            foreach($order['product'] as $product) {
                                $productIds[] = $product->product_id;
                                $totalMoney += $product->total;
                            }
                        }

                        if(isset($order['info'])) {
                            foreach ($weeks as $keyWeek => $week) {
                                if (date('Y-m-d', strtotime($order['info']->date_added)) >= date('Y-m-d', strtotime($week['from'])) && date('Y-m-d', strtotime($order['info']->date_added)) <=  date('Y-m-d', strtotime($week['to']))) {
                                    $totalOrder = $order['total_order'];
                                    if (!in_array($keyWeek, $date_added)) {
                                        $date_added[] = $keyWeek;
                                        $incomeWeekly[] = $totalOrder;
                                    } else {
                                        $key = array_search($keyWeek, $date_added);
                                        $incomeWeekly[$key] += $totalOrder;
                                    }
                                } else {
                                    if (!in_array($keyWeek, $date_added)) {
                                        $date_added[] = $keyWeek;
                                        $incomeWeekly[] = 0;
                                    }
                                }
                            }
                        }
                    }
                }

                foreach ($categories as $key => $category) {
                    $x = 0 ;
                    $productDetail = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/product_admin/products&category='.$category['id']);
                    if (@$productDetail->success) {
                        $salesPerCategory[$key]['name'] = $category['name'];
                        foreach ($productDetail->data as $dataProduct) {
                            if (in_array($dataProduct->id, $productIds)) {
                                $x++;
                            }
                        }
                        $salesPerCategory[$key]['y'] = $x;
                    } else {
                        $salesPerCategory[$key] = ['name' => $category['name'], 'y' => $x];
                    }
                }

                //GET PURCHASE BY STANDART CUSTOMER
                $dataPurchaseByConsumer = 0;
                $dataUserStandart = $this->user->select('customer_id')->where('related_type','App\Models\StandardCustomer')->get();
                foreach ($dataUserStandart as $key => $value) {
                    $orderDetailUser = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/order_admin/userorders&user='.$value->customer_id);
                    if (@$orderDetailUser->success) {
                        foreach ($orderDetailUser->data as $dataOrder) {
                            $dataPurchaseByConsumer += $dataOrder->total;
                        }
                    }
                }

                //GET PURCHASE BY CORPORATE CUSTOMER
                $dataPurchaseByCorporate = 0;
                $dataUserCorporate = $this->user->select('customer_id')->where('related_type','App\Models\CorporateCustomer')->get();
                foreach ($dataUserCorporate as $key => $value) {
                    $orderDetailUserCorporate = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/order_admin/userorders&user='.$value->customer_id);
                    if (@$orderDetailUserCorporate->success) {
                        foreach ($orderDetailUserCorporate->data as $dataOrderCorporate) {
                            $dataPurchaseByCorporate += $dataOrderCorporate->total;
                        }
                    }
                }
                

                $result = [
                    'status' => true,
                    'order' => number_format(count($orderHistory)),
                    'total_business' => number_format($business),
                    // 'total_customer' => number_format($corporates) + number_format($standards),
                    'total_customer' => number_format($standards),
                    'sales_per_category' => $salesPerCategory,
                    'income_weekly' => $incomeWeekly,
                    'corporate_user' => $corporates,
                    'consumer_user' => $standards,
                    'time_income_weekly' => $date_added,
                    //'total_money' => '$'.number_format($totalMoney,2,',','.'),
                    //edited by k5 puw 5/12/17
                    'total_money' => '$'.number_format($totalMoney,0,'.',','),
                    'total_purchase_consumer' => $dataPurchaseByConsumer,
                    'total_purchase_corporate' => $dataPurchaseByCorporate,
                ];
            }
        } catch (Exception $e) {
            $result = array('status' => false);
        }
        return response()->json($result, 200);
    }

    public function downloadUserGuide()
    {
        //CSV file is stored under project/public/file/sample_bulk_user.xlsx
        $file= public_path(). "/pdf/L1TDocs.pdf";

        $headers = array(
                  'Content-Type: application/pdf',
                );

        return Response::download($file, 'L1TDocs.pdf', $headers);
    }

    public function downloadOcUserGuide()
    {
        $file= public_path(). "/pdf/L1TOpencartTheme.pdf";

        $headers = array(
                  'Content-Type: application/pdf',
                );

        return Response::download($file, 'L1TOpencartTheme.pdf', $headers);
    }

    protected function getWeekDates($date, $start_date, $end_date)
    {
        $week =  date('W', strtotime($date));
        $year =  date('Y', strtotime($date));
        $from = date("Y-m-d", strtotime("{$year}-W{$week}+1")); //Returns the date of monday in week
        if($from < $start_date) $from = $start_date;
        $to = date("Y-m-d", strtotime("{$year}-W{$week}-7"));   //Returns the date of sunday in week
        if($to > $end_date) $to = $end_date;
        return ['from' => $from, 'to' => $to];
        // echo $from." - ".$to;//Output : Start Date-->2012-09-03 End Date-->2012-09-09
    }  

    protected function getProductCategories()
    {
        $responses = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/category_admin/category');                
        if ($responses->success) {
            foreach ($responses->data->categories as $key => $value) {
                $categories[$key]['id'] = $value->category_id;
                $categories[$key]['name'] = $value->name;
            }
        }

        return $categories;
    }
}
