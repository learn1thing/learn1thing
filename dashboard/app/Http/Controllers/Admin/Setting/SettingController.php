<?php

namespace App\Http\Controllers\Admin\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('hasaccess:setting');
    }
    /**
     * Show index page setting
     * @author Hamdan Hanafi
     * @return \Illuminate\Support\View
     */
    public function index()
    {
      return view('admin.setting.index');
    }
}
