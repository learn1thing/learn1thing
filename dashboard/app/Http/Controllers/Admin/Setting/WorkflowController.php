<?php

namespace App\Http\Controllers\Admin\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use App\Models\Workflow;
use App\Models\Department;
use App\Models\EmailTemplate;
use App\DataTables\Setting\WorkflowDataTable;

use DB;
use Datatables;

class WorkflowController extends Controller
{
	/**
	 * { function_description }
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */                    
    public function index(WorkflowDataTable $dataTable)
    {
        $workflows = Workflow::all();
        $events = Workflow::eventTrigger();
        //return $dataTable->render('admin.setting.workflow.index');
        return view( 'admin.setting.workflow.index', compact('workflows', 'events') );
    }

    /**
     * { function_description }
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function create()
    {
    	$data['categories'] = Workflow::getCategory()->pluck( 'name', 'id' );
        $data['actions'] = Workflow::getAction()->pluck( 'name', 'id' );
        $data['templates'] = EmailTemplate::all()->pluck( 'title', 'id' );
        $data['recepients'] = Workflow::getRecipient()->pluck( 'name', 'id' );
        $data['events'] = collect(Workflow::eventTrigger())->pluck( 'name', 'slug' );
        $data['access'] = Workflow::getUserAccess()->pluck( 'name', 'id' );
        $data['schedules'] = Workflow::getSchedule()->pluck( 'name', 'slug' );
       
    	return view( 'admin.setting.workflow.create', $data );
    }

    /**
     * Store Workflow
     *
     * @param      \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        //print_r($request->all()); exit;
    	DB::beginTransaction();
    	try {

    		$workflow = Workflow::create( $request->all() );
            DB::commit();
    	    
            sessionFlash('Workflow is successfully created.','success');
            return redirect()->route('admin.workflow.index');
        } catch (Exception $e) {
    		DB::rollback();
            
            sessionFlash($e->getMessage(),'error');
            return redirect()->back();
    	}
    }

    /**
     * Gets the product.
     *
     * @param      \Illuminate\Http\Request  $request  The request
     *
     * @return     <type>                    The product.
     */
    public function getProduct(Request $request)
    {
         $products = Workflow::getProduct( $request->category_id );

         return $products;
    }

    /**
     * Gets the recipe.
     *
     * @param      \Illuminate\Http\Request  $request  The request
     *
     * @return     <type>                    The recipe.
     */
    public function getRecipe( Request $request )
    {
        $recipe = Workflow::getUser( $request->role );

        return json_encode($recipe);

    }

    public function getAccess( Request $request )
    {
        if ($request->role == 2) {
            $role = Workflow::getRecipient();
            $data = $role;
        } else {
            $data = Department::getDataDepartment();
        }
        $response = array(
            'id' => $request->role,
            'data' => $data
            );
        return $response;
    }

    public function destroy( $id )
    {
        Workflow::find($id)->delete();

        sessionFlash('Workflow is successfully deleted','success');
        return redirect()->route('admin.workflow.index');
    }

    /**
     * [update description]
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function update(Request $request,$id)
    {
        try {
            $workflow = Workflow::find($id);

            $workflow->product_category_id=$request->input('product_category_id');
            $workflow->product_id=$request->input('product_id');
            $workflow->action=$request->input('action');
            $workflow->action_template=$request->input('action_template');
            $workflow->recipient_role=$request->input('recipient_role');
            $workflow->recipient_user_id=$request->input('recipient_user_id');
            $workflow->recipient_user=$request->input('recipient_user');
            $workflow->recipient_department=$request->input('recipient_department');
            //$workflow->offset=$request->input('date_offset');
            $workflow->event_trigger=$request->input('event_trigger');
            $workflow->save();
     
            sessionFlash('Workflow is successfully updated.','success');
            return redirect()->route('admin.workflow.index');

            
        } catch (Exception $e) {
   
            sessionFlash($e->getMessage(),'error');
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        $workflows = Workflow::find($id);

        $data['workflow']=$workflows;
        $data['categories'] = Workflow::getCategory()->pluck( 'name', 'id' );
        $data['actions'] = Workflow::getAction()->pluck( 'name', 'id' );
        $data['templates'] = EmailTemplate::all()->pluck( 'title', 'id' );
        $data['recepients'] = Workflow::getRecipient()->pluck( 'name', 'id' );
        $data['events'] = collect(Workflow::eventTrigger())->pluck( 'name', 'slug' );
        $data['access'] = Workflow::getUserAccess()->pluck( 'name', 'id' );
        $data['schedules'] = Workflow::getSchedule()->pluck( 'name', 'slug' );
        
        $data['users']=Workflow::getUserById($workflows->recipient_user_id);
        return view( 'admin.setting.workflow.edit', $data );
    }
}
