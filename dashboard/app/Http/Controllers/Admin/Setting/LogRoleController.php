<?php

namespace App\Http\Controllers\Admin\Setting;

use Cookie;
use App\Classes\TryCatch;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\LogRole;
use BrowserDetect;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Sentinel;
use GuzzleHttp\Client;
use App\Http\Controllers\Auth\Api\AuthenticateController;
use Illuminate\Support\Facades\Log;

class LogRoleController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';


    /**
     * Alert class for message login
     * @var string
     */
    public $alertClass;

    /**
     * Message Login
     * @var string
     */
    public $message;

    /**
     * Status header from response
     * @var integer
     */
    public $statusCode;


    /**
     * Persistences code user
     * @var string
     */
    protected $persistences;

    /**
     * token cookie for authorize
     * @var string
     */
    protected $token;

    protected $email;
    protected $login_type;
    protected $social_type;


    /**
     * Define status create cookie or not
     * @var boolean
     */
    protected $isCreateCookie;


    /**
     * { function_description }
     *
     * @param      <type>  $email  The email
     */
    public function change_roles( $email )
    {
        $user = Sentinel::getUser();
        if (!session()->has('email_refer')) {
            session()->put('email_refer',$user->email);
        }
        $this->onLogout($user->persistences->first()->code);
        Sentinel::logout();

        Cookie::forget('token');
        Cookie::forget('email');
        Cookie::forget('password');
        Cookie::forget('token_moodle');
        session()->forget('token_moodle');

        // $client = new Client();
        // $client->request('GET', 'http://store.learn1thing.com/course/login/logout.php', []);

        //redirect(env('MOODLE_BASE_URL_API').'/login/logout.php');
        return redirect(env('MOODLE_BASE_URL_API').'/auth/userkey/logout.php');

        //return $this->login( $email );

        // return redirect()->route('login')
        //                 ->withCookie(Cookie::forget('token'))
        //                 ->withCookie(Cookie::forget('email'))
        //                 ->withCookie(Cookie::forget('password'));
    }

     /**
     * Authenticate Login User [for API]
     * @param  Illuminate\Http\Request $request
     * @return Illuminate\Support\Response
     */
     public function login($email)
     {
       $url_disini_saja;
       $apiAutCont_2 = new AuthenticateController;
       $url_disini_saja=$apiAutCont_2->getCourseLoginUrl($email);

       Log::info($url_disini_saja);

       if ($url_disini_saja=='/') {
           return redirect('/');
       }

        $log = LogRole::where( 'username', $email )->first();

        $login_type = '';
        $social_type = '';
        $tokenMoodle = '';

        $credentials = [
        'email'    => $log->username,
        'password' => $log->password,
        'remember' => false
        ];
        $prosesing = new TryCatch(function() use ($credentials, $log) {

            $user = Sentinel::authenticate($credentials, $credentials['remember']);
            $this->login_type = 'normally';
            $this->social_type = 'normally';

            if (!$user) {
                $this->message = trans('auth.failed');
                $this->alertClass = 'alert-danger';
                $data = array();
            } else {

                $persistences = $user->persistences->first();

                $this->isCreateCookie = true;
                $token = $persistences->code;
                $this->token = $token;
                $this->email = $user->email;

                $data = $user;

                if(@$data->last_login){
                    session()->flash('status' , "old-user");
                } else {
                    session()->flash('status' , "new-user");
                }

                if($data->status_login == 0){
                    $data->status_login = 1;
                    $data->save();
                } else if ($data->status_login == 1) {
                    $data->status_login = 2;
                    $data->save();
                }

                //get token moodle
                $apiAutCont = new AuthenticateController;
                $request = new Request;
                $request->merge(['email' => $log->username, 'password' => $log->password]);
                $tokenMoodle = $apiAutCont->getMoodleToken($request);
                session()->put('token_moodle',$tokenMoodle);
            }

            return $data;
        }, function(\Exception $e) {

            if ($e instanceof \Cartalyst\Sentinel\Checkpoints\NotActivatedException) {
                $this->statusCode = 401;
            } else if ($e instanceof \Cartalyst\Sentinel\Checkpoints\ThrottlingException) {
                $this->statusCode = 429;
            } else {
                $this->statusCode = 400;
            }

            $this->message = $e->getMessage();

            session()->flash('alert-class' , 'alert-danger');
            session()->flash('message' , $this->message);
            return redirect('/');
        });

        if ($this->isCreateCookie) {
            $token = Cookie::queue('token', $this->token, 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
            $password = Cookie::queue('password', baseEncrypt($credentials['password']), 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
            $login_type = Cookie::queue('login_type', $this->login_type, 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
            $social_type = Cookie::queue('social_type', $this->social_type, 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
            $addEmail = Cookie::queue('email', baseEncrypt($this->email), 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
            $addTokenMoodle = Cookie::queue('token_moodle', $tokenMoodle, 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
        }

        session()->flash('alert-class' , $this->alertClass);
        session()->flash('message' , $this->message);
        Log::info('Sampai sini');

        return redirect($url_disini_saja);
    }
}
