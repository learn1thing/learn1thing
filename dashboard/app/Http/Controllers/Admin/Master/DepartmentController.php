<?php

namespace App\Http\Controllers\Admin\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\Master\DepartmentDataTable;
use App\Http\Requests\Master\DepartmentRequest;

class DepartmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('hasaccess:manage-department');
        $this->department = new \App\Models\Department;
    }

    public function index(DepartmentDataTable $dataTable)
    {
        return $dataTable->render('admin.master.departments.index');
    }

    public function create()
    {
        // $departments = $this->department->getDepartment()->whereNull('parent_id')->pluck('name','id')->toArray();
        $departments = $this->department->getDepartment()->pluck('name','id')->toArray();
        return view('admin.master.departments.create',compact('departments'));
    }

    public function store(DepartmentRequest $request)
    {
        \DB::beginTransaction();
        try {
            $request->merge(['user_id' => user_info('id')]);
            if (!empty($request->parent_id)) {
                $request->merge(['parent_id' => $request->parent_id]);
                $dataInsert = $request->except(['_token','id','_wysihtml5_mode']);
            } else {
                $dataInsert = $request->except(['_token','id','_wysihtml5_mode','parent_id']);
            }
            $insert = $this->department->create($dataInsert);
            if ($insert) {
                \DB::commit();
                sessionFlash('Department successfully added','success');
                return redirect()->route('admin.department.index');
            } else {
                sessionFlash('Something went wrong','error');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            \DB::rollback();
            sessionFlash($e->getMessage(),'error');
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        $department = $this->department->find($id);
        if ($department) {
            $departments = [];
            if (user_info()->inRole('super-admin')) {
                if (@$department->user->departments) {
                    $departments = $department->user->departments()->whereNull('parent_id')->pluck('name','id')->toArray();
                }
            } else {
                $departments = $this->department->getDepartment()->whereNull('parent_id')->pluck('name','id')->toArray();
            }
            if ($this->haveNoAccess($department)) {
                sessionFlash('Sorry, you dont have access','warning');
                return redirect()->route('admin.department.index');
            }
            return view('admin.master.departments.edit',compact('department','departments'));
        } else {
            sessionFlash('Department not found','error');
            return redirect()->back();
        }
    }

    public function update(DepartmentRequest $request, $id)
    {
        $department = $this->department->find($id);
        if ($department) {
            if ($this->haveNoAccess($department)) {
                sessionFlash('Sorry, you dont have access','warning');
                return redirect()->route('admin.department.index');
            }

            if (!empty($request->parent_id)) {
                $request->merge(['parent_id' => $request->parent_id]);
                $update = $department->update($request->except(['_token','id','_wysihtml5_mode']));
            } else {
                $update = $department->update($request->except(['_token','id','_wysihtml5_mode','parent_id']));
            }

            if ($update) {
                \DB::commit();
                sessionFlash('Department successfully added','success');
                return redirect()->route('admin.department.index');
            } else {
                sessionFlash('Something went wrong','error');
                return redirect()->back();
            }
        } else {
            sessionFlash('Department not found','error');
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        $department = $this->department->find($id);
        if ($department) {
            
            if ($this->haveNoAccess($department)) {
                sessionFlash('Sorry, you dont have access','warning');
                return redirect()->route('admin.department.index');
            }

            if ($department->users->count() > 0) {
                sessionFlash('Department have related','warning');
            } else if ($department->childs->count() > 0) {
                sessionFlash('Department have child','warning');
            } else {
                $department->delete();
                sessionFlash('Department successfully deleted','success');
            }
        } else {
            sessionFlash('Department not found','error');
        }

        return redirect()->back();
    }

    protected function haveNoAccess($department)
    {
        if (!user_info()->inRole('super-admin')) {
            if (user_info('parent_id')) {
                if ($department->user_id != user_info('id')) {
                    return true;
                }
            } else {
                $childs = user_info()->downlines()->pluck('id')->toArray();
                if (!in_array($department->user_id, $childs) && $department->user_id != user_info('id')) {
                    return true;
                }
            }
        }
        return false;
    }
}
