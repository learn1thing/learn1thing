<?php

namespace App\Http\Controllers\Admin\Master\Coach;

use App\DataTables\Coach\CourseDataTable;
use App\Http\Controllers\Controller;
use App\Models\CoachCourse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Session;
use Sentinel;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('hasaccess:manage-course');
    }
    /**
     * Display a listing of the resource.
     * @author ali
     * @return \Illuminate\Http\Response
     */
    public function index(CourseDataTable $dataTable)
    {
        return $dataTable->render('coach.course.index');
    }

    /**
     * Show the form for creating a new resource.
     * @author ali
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('coach.course.create');
    }

    /**
     * Store a newly created resource in storage.
     * @author ali
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:coach_courses',
            'description' => 'required'
        ];

        $this->validate($request,$rules);

        $coachCourse = CoachCourse::create([
            'name' => $request->name,
            'user_id' => user_info('id'),
            'description' => $request->description,
        ]);

        $client = new Client();

        $data['groups'][0] = [
            'course_id' => $coachCourse->id,
            'profiler_id' => $coachCourse->id,
            'url' => '',
            'email' => user_info('email'),
            'product_name' => $coachCourse->name,
        ];

        $post_register = $client->request('POST', env('MOODLE_BASE_URL_STORE_COURSE'), [
            'headers' => [
                'Content-Type' => "application/x-www-form-urlencoded"
            ],
            'form_params' => $data
        ]);
        info('Store Moodle to Moodle');
        info($post_register->getBody());
        $data_post_register = json_decode($post_register->getBody());

        // $this->storeMoodle($request);

        \Session::flash('alert-class', 'alert-success');
        \Session::flash('message','Course successfully added.');
        return Redirect::route('coach.course.index');
    }

    public function storeMoodle($data)
    {
        $client = new Client();

        $data['users'][0] = [
            'course_id' => $data->email,
            'profiler_id' => $data->email,
            'url' => '',
            'email' => user_info('email'),
            'product_name' => $data->name,
        ];

        $post_register = $client->request('POST', env('MOODLE_BASE_URL_STORE_COURSE'), [
          'form_params' => $data
        ]);
        info('Store Moodle to Moodle');
        info($post_register->getBody());
        $data_post_register = json_decode($post_register->getBody());
        
        return $data_post_register;
    }

    /**
     * Display the specified resource.
     * @author ali
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * @author ali
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = CoachCourse::where('id',$id)->first();
        $model = CoachCourse::pluck('name', 'id');

        return view('coach.course.edit')->with(compact('data','model'));
    }

    /**
     * Update the specified resource in storage.
     * @author ali
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|unique:coach_courses,name,'.$id.',id',
            'description' => 'required'
        ];

        $this->validate($request,$rules);

        $coachCourse = CoachCourse::where('id',$id)->update([
                'name' => $request->name,
                'description' => $request->description,
            ]);

        Session::flash('message', 'Data has been update'); 
        Session::flash('alert-class', 'alert-success'); 
        return Redirect::route('coach.course.index');
    }

    /**
     * Remove the specified resource from storage.
     * @author ali
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = CoachCourse::where('id',$id);

        if ($model->delete()) {
            Session::flash('message', 'Data has been deleted'); 
            Session::flash('alert-class', 'alert-success'); 
            return Redirect::route('coach.course.index');
        } else {
            Session::flash('message', 'Data can not be deleted'); 
            Session::flash('alert-class', 'alert-error'); 
            return Redirect::route('coach.course.index');
        }
    }

    public function ajax_list(Request $request)
    {
        $q = $request->q;
        $user = Sentinel::getUser();
        if ($user) {
            info($q);
            $results = CoachCourse::where('name', 'like', '%'.$q['term'].'%')
                                    // ->where('user_id',$user->id)
                                    ->get();
            if (count($results) > 0) {
                foreach ($results as $result) {
                    $data[] = array('id'=>$result->id,'text'=>$result->name);
                }
            } else {
                $data = array();
            }

        } else {
            $data = array();
        }

        $resData = array(
                'success'=>true,
                'code'=>200,
                'results'=>$data
            );

        return json_encode($resData);

        // return response()->json($resData,200);

    }
}
