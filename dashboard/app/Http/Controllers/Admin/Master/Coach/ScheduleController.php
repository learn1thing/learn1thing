<?php

namespace App\Http\Controllers\Admin\Master\Coach;

use App\DataTables\Coach\CourseDataTable;
use App\DataTables\Schedule\ScheduleDataTable;
use App\DataTables\Master\MasterScheduleDatatable;
use App\Http\Controllers\Admin\Master\BusinessPartnerController;
use App\Http\Controllers\Api\v1\ProductController;
use App\Http\Controllers\Controller;
use App\Models\BusinessPartner;
use App\Models\CoachCourse;
use App\Models\CoachSchedule;
use App\Models\CoachScheduleOrder;
use App\Models\LogOrderScheduleTemp;
use App\Models\Schedule;
use App\Models\ScheduleDay;
use App\Models\OrderProduct;
use App\Models\Product;
use App\Models\Coach;
use App\Models\User;
use App\Models\ProductSet;
use App\Models\CourseProfiler;
use App\Models\LogOrderSchedule;
use App\Mail\ScheduleRequestProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use MaddHatter\LaravelFullcalendar\Calendar;
use App\Http\Requests\Schedule\MasterScheduleRequest;
use Sentinel;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use App\Classes\CurlRequest;
use Cookie;
use Session;
use App\Http\Controllers\Api\v1\EnrollUserController as enrollUserApi;
use App\Models\UserEnrol;

use Illuminate\Support\Facades\Log;


class ScheduleController extends Controller
{
    protected $model_schedule;
    protected $ctrlApiProduct;

    public function __construct(Schedule $model_schedule, ProductController $ctrlApiProduct)
    {
        // $this->middleware('hasaccess:manage-schedule');
        $this->model_schedule = $model_schedule;
        $this->ctrlApiProduct = $ctrlApiProduct;
        $this->model_prductSet = new ProductSet();
        $this->model_courseProfile = new CourseProfiler();
        $this->model_coachSchedule = new CoachSchedule();
        $this->model_logOrderSchedule = new LogOrderSchedule();
        $this->model_coachScheduleOrder = new CoachScheduleOrder();
        $this->userEnrol = new UserEnrol;
    }
    /**
     * Display a listing of the resource.
     * @author ali
     * @return \Illuminate\Http\Response
     */
    public function index(ScheduleDataTable $dataTable)
    {
        return $dataTable->render('coach.schedule.index');
    }

    /**
     * Show the form for creating a new resource.
     * @author ali
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {

        if (user_info('parent_id')) {
            $userFind = ['user_id' => user_info('parent_id')];
        } else {
            $userFind = ['user_id' => user_info('id')];
        }

        $data['course'] = CoachCourse::where($userFind)->pluck('name', 'id');
        $data['actived'] = [ 1 => "Active", 0 => "Not Active" ];
        $data['user_id'] = $id ;
        $data['days'] = [
                            1 => 'Monday',
                            2 => 'Tuesday',
                            3 => 'Wednesday',
                            4 => 'Thursday',
                            5 => 'Friday',
                            6 => 'Saturday',
                            7 => 'Sunday'
                        ];
        $data['devine'] = [ 1 => "Yes", 0 => "No" ];
        $data['overlap'] = [ 1 => "Yes", 0 => "No" ];
        $data['allow'] = [0,1,2,3,4,5,6,7];
        $data['display'] = [ 1 => "Now", 2 => "Not Now" ];
        $data['reminder'] = [ 1 => "1 Days before slot", 2 => "2 Days before slot" , 3 => "3 Days before slot"];

        return view('coach.schedule.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     * @author ali
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dump($request);exit;
        $request->start_time = date('H:i:s', strtotime($request->start_time));
        $request->end_time = date('H:i:s', strtotime($request->end_time));

        $rules = [
            'course_id' => 'required',
            'schedule_date' => 'required|date_format:Y-m-d',
            'start_time' => 'required',
            'end_time' => 'required|beetwenSchedule:'.$request->start_time,
            // 'actived' => 'required|numeric'
        ];

        $this->validate($request,$rules);

        // $existSchedule = CoachSchedule::where('schedule_date',$request->schedule_date);

        $bulksInsert = [];
        if ($request->schedule_until_date) {
            for ($i = $request->schedule_date; $i <= $request->schedule_until_date ; $i++) { 
                $bulkInsert = [
                                'user_id' => empty($request->user_id) ? user_info('id') : $request->user_id,
                                'course_id' => $request->course_id,
                                'schedule_date' => $i,
                                'start_time' => $request->start_time,
                                'end_time' => $request->end_time,
                                'location' => empty($request->location) ? null : $request->location,
                                'comments' => empty($request->comments) ? null : $request->comments,
                                'choice' => 0,
                                'status' => 0,
                                'devine_slots' => empty($request->devine_slots) ? null : $request->devine_slots,
                                'duration_slots' => empty($request->duration_slots) ? null : $request->duration_slots,
                                'break_slots' => empty($request->break_slots) ? null : $request->break_slots,
                                'overlap_slots' => empty($request->overlap_slots) ? null : $request->overlap_slots,
                                'multiple_allow' => empty($request->multiple_allow) ? null : $request->multiple_allow,
                                'display_appoinment' => empty($request->display_appoinment) 
                                                        ? null 
                                                        : $request->display_appoinment,
                                'email_reminder' => empty($request->email_reminder) ? null : $request->email_reminder,
                                'actived' => empty(user_info('parent_id')) ? 1 : 0,
                            ];
                if (empty($request->day)) {
                    array_push($bulksInsert, $bulkInsert);
                } else {
                    $dateCheck = get_date_index(date('D',strtotime($i)));
                    if (in_array($dateCheck, $request->day)) {
                        array_push($bulksInsert, $bulkInsert);
                    }
                }
            }
        } else {
            $bulksInsert = [
                            'user_id' => empty($request->user_id) ? user_info('id') : $request->user_id,
                            'course_id' => $request->course_id,
                            'schedule_date' => $request->schedule_date,
                            'start_time' => $request->start_time,
                            'end_time' => $request->end_time,
                            'location' => empty($request->location) ? null : $request->location,
                            'comments' => empty($request->comments) ? null : $request->comments,
                            'choice' => 0,
                            'status' => 0,
                            'devine_slots' => empty($request->devine_slots) ? null : $request->devine_slots,
                            'duration_slots' => empty($request->duration_slots) ? null : $request->duration_slots,
                            'break_slots' => empty($request->break_slots) ? null : $request->break_slots,
                            'overlap_slots' => empty($request->overlap_slots) ? null : $request->overlap_slots,
                            'multiple_allow' => empty($request->multiple_allow) ? null : $request->multiple_allow,
                            'display_appoinment' => empty($request->display_appoinment) 
                                                    ? null 
                                                    : $request->display_appoinment,
                            'email_reminder' => empty($request->email_reminder) ? null : $request->email_reminder,
                            'actived' => empty(user_info('parent_id')) ? 1 : 0,
                        ];
        }

        // if ($existSchedule->exists()) {
        //     \Session::flash('alert-class', 'alert-error');
        //     \Session::flash('message','There have been periods of schedule');

        //     return redirect()->route('coach.schedule.create',$request->user_id);
        // } else {
            CoachSchedule::insert($bulksInsert);

            \Session::flash('alert-class', 'alert-success');
            \Session::flash('message','Schedule successfully added.');

            return redirect()->route('coach.schedule.show',$request->user_id);
        // }
        

    }

    /**
     * Display the specified resource.
     * @author ali
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('id',$id)->first();
        $events = [];

        
        $result = [];
        if (!empty($user->scheduleRelated)) {
            foreach ($user->scheduleRelated as $key => $value) {
                $name = $value->courseRelated->name;
                if (!in_array($name, $result)) {
                    $result[] = $value->courseRelated->name;
                }

                //store event
                $events[] = $this->storeEvent(  $value->courseRelated->name,
                                                $value->schedule_date." ".$value->start_time,
                                                $value->schedule_date." ".$value->end_time);
            }
        }

        $calendar = \Calendar::addEvents($events)->setCallbacks([ 
            ]);

        $return = [
                    'courses' => $result,
                    'name' => empty($user->first_name) ? "" : $user->first_name,
                    'calendar' => $calendar,
                    'user_id' => $user->id
                ];

        return view('coach.schedule.show',$return);
            
    }

    /**
     * Show the form for editing the specified resource.
     * @author ali
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * @author ali
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @author ali
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @author ali
     * @param  int  $eventName, $eventStart, $eventEnd
     * @return \Illuminate\Http\Response
     */
    public function storeEvent($eventName = null, $eventStart = null, $eventEnd = null, $id)
    {
        if ($eventName && $eventStart && $eventEnd) {
            $event = \Calendar::event(
                    'Available', //title
                    false, //full day
                    $eventStart, //start time (you can also use Carbon instead of DateTime)
                    $eventEnd, //endtime
                    $id,
                    [
                        'start' => $eventStart, // a start time (10am in this example)
                        'end' => $eventEnd, // an end time (6pm in this example)
                        'className' => 'admin',
                        //any other full-calendar supported parameters
                    ]
                );

            return $event;
        } else {
            return false;
        }

    }

    public function showEvent($eventName = null, $eventStart = null, $eventEnd = null, $id)
    {
        if ($eventName && $eventStart && $eventEnd) {
            $event = \Calendar::event(
                    'Booking', //title
                    false, //full day
                    $eventStart, //daytime
                    $eventEnd, //endtime
                    $id,
                    [
                        'start' => $eventStart, // a start time (10am in this example)
                        'end' => $eventEnd, // an end time (6pm in this example)
                        'backgroundColor' => '#800',
                        'className' => 'event_coach',
                        //any other full-calendar supported parameters
                    ]
                );

            return $event;
        } else {
            return false;
        }

    }

    public function showSchedule($id = null)
    {
        // $moodle= DB::connection('mysql_moodle')->select('select * from mdl_book');
        // dump($moodle);exit;

        $events = [];

        for ($i=1; $i < 31; $i++) { 
            
            if (!in_array($i, [1,2,8,9,15,16,22,23,29,30])) {
                for ($t=9; $t < 18; $t++) { 
                    if (!in_array($t, [13])) {
                        $tnext = $t+1;
                        $events[] = \Calendar::event(
                                    'Available', //title
                                    false, //full day
                                    '2017-04-'.$i.' '.$t.':00:00', //daytime
                                    '2017-04-'.$i.' '.$tnext.':00:00', //endtime
                                    $i //eventID
                                );
                    }
                }
            }
        }

        $calendar = \Calendar::addEvents($events)->setCallbacks([ 
            // 'dayClick' => "function(date, jsEvent, view) {
            //     $(this).closest('td').css('background-color', '#000');
            // }",
            'eventClick' => "function(calEvent, jsEvent, view) {
                    $(this).css('background-color', '#ff851b');
                }",
            'editable'=> true,
            "allDay" => true,

            // "eventDrop" =>  "function(event,dayDelta,minuteDelta,allDay,revertFunc) {
            //     alert(
            //         event.title + ' was moved ' +
            //         dayDelta +  'days and'  +
            //         minuteDelta +  'minutes.'
            //     );

            //     if (allDay) {
            //         alert('Event is now all-day');
            //     }else{
            //         alert('Event has a time-of-day');
            //     }

            // }"


                // if (!confirm('Are you sure about this change?')) {
                //     revertFunc();
                // }

            ]);

        $schedule = [
                [
                    'index' => 1,
                    'image' => 'http://www.learn1thing.com/learn/user/pix.php/67/f1.jpg',
                    'coach_name' => 'Claudia, Harss (DE)',
                    'coach_desc_1' => 'Project management; Workshop facilitation and team development);Top management coaching;Cooperation in the field of science',
                    'coach_desc_2' => 'Specialization:Project management; Workshop facilitation and team 
                                        development); Top management coaching;Cooperation in the field of science
                                        Dr. Claudia Harss

                                           

                                        Owner of the TWIST Consulting Group

                                        Psychologist (PhD), Senior Consultant

                                         
                                        Specialization:
                                        ·    Project management in organizational development and international cooperation, worldwide roll-out of personnel development projects

                                        ·    Workshop facilitation and team development (nationally and internationally on a management level)

                                        ·    Top management coaching

                                        ·    Cooperation in the field of science


                                        Background:
                                        Studies of Psychology and Educational Science in Munich and Regensburg
                                        PhD at the University of Dortmund
                                        Scientific education in psychological research at the Max-Planck-Institute
                                        Postdoc work at the University of German Federal Armed Forces Munich
                                        Numerous publications of books and articles
                                        Lectureship for Strategic HR Development (Ludwig Maximilian University Munich) and International Personnel Development (Fresenius University)
                                         
                                        Languages: German, English
                                         

                                        Project Examples:
                                        Conception and facilitation of assessment centers at a globally operating company in order to assess the potential of internationally operating managers of different locations
                                        Post-merger integration consulting for a globally operating insurance company: Top down process (from the top management to the team leaders) with the aim to introduce new company values
                                         
                                        Selected Publications:
                                        Harss, C., & Liebich, D. (2015). Sensitivity for other cultures. Business Psychology
                                        Harss, C., & Elbasi, A. (2014). Personnel development without borders. managerSeminare
                                        Harss, C., Liebich, D., & Michalka, M. (2011). Conflict management for executives (Vahlen)
                                        Harss, C., & Hoh, R. (2011). Change on prescription. managerSeminare
                                        Harss, C. & al. (2009) Dos and don’ts of int. assessment centers. ManagerSeminare
                                        Harss, C., Wagner, W., & Hofmann, K. (2009). Is the wall really gone? Personalmagazin
                                        Harss, C., & al., R. (2008). The third party in a management audit. Personalmagazin
                                        Harss, C., Elbasi, A., & Sziljer, F. (2006). The central-local balancing act. managerSeminare
                                        Hofstede, G. & Harss, C. (2010): breaks and bridges in international Enterprices. Organisationsentwicklung (3/2010)',
                    'events' => 1,
                ]
        ];
        $schedule2 = [
                [
                    'index' => 1,
                    'image' => 'http://www.learn1thing.com/learn/user/pix.php/15/f1.jpg',
                    'coach_name' => 'Jin Lee, Teo ',
                    'coach_desc_1' => 'Conflict, Culture, Change, Communication, Leadership, Influence, Project Management, Sales, Strategy, Team',
                    'coach_desc_2' => 'Specialization:Conflict, Culture, Change, Communication, Leadership, Influence, Project Management, Sales, Strategy, Team
                    Teo Jin Lee is the Founder and Managing Director of ODE Consulting Pte Ltd, with operations in Singapore, China and India. I have been in strategy and leadership roles for more than 30 years. She has held senior leadership positions in sales and business development both locally and regionally.
                     
                    She was previously Regional Manager (ASEAN/SA) in IBM Global Services and was responsible for establishing new services leveraging emerging Infocomm Technologies (ICT).
                     
                    She has led in IBM’s change initiatives, especially, in process re-engineering and sales force transformation as part of a TOTAL Quality Change process.
                     
                    She was a facilitator for SG Conversations for the Singapore Government. She has been facilitating, consulting and training in the Strategic Management, Leadership, Team Effectiveness, Change Management, Project Management, Sales and Marketing areas in numerous organizations for the past 30 years.
                     
                    Some of the organization’s she has worked with are:
                    Atos Origin, Accenture, CapitaLand, Citibank, DHL, Deutsche Bank, Ericsson, Roche, Fujitsu Asia, Hewlett Packard, IBM, IDA, IJM, Ministry of Defence, Microsoft, McKinsey, NCS, OCBC Bank, Okura, Public Bank, SwissRe,  Singapore Technologies, StarHub, Sony, Shell, Singtel, SAP Asia and 3M, but to name a few.
                     
                    Teo Jin Lee was selected as one of the top 10 winners in the Woman Entrepreneur of the Year Award in 2001.
                     
                    She was on the Global Board of Governors of the International Association of Coaching (IAC) out of the US from 2011 to 2015. I was also the IAC Regional Head of the Asia Crescent (Asia Pacific, Middle East and Africa). I am also an IAC Masteries Practitioner coach.
                     
                    She currently the co-President of ITAP International out of Princeton USA, and also certified to administer the Culture in the Workplace Questionnaire™, based on the research of Geert Hofstede.',
                    'events' => 1,
                ]
        ];

        $return = [
                    'calendar' => $calendar,
                    'schedules' => $id == 1 ? $schedule : $schedule2
                ];
        return view('coach.schedule.indexSchedule',$return);
    }

    //$id is course_id
    public function showCoach($course_id, Request $request)
    {
        if (@$request->type_order == 'corporate') {
            $str_user_id = explode("-", $request->get('user_id'));

            if (count($str_user_id) > 1) {
                $user_id = baseDecrypt( $str_user_id[1] );
            } else {
                $user_id = $str_user_id[0];
            }
            
            $order_product_id = baseDecrypt($request->get('order_product_id'));

            //check user login
            $currentUser = user_info();
            Log::info($course_id);

            if ( @$currentUser->id != $user_id ) {

                $user = User::find($user_id);
                if (count($user)>0) {
                    sessionFlash('Please login as '.$user->first_name.' '.$user->last_name. 'for set schedule','error');    
                }else{
                    sessionFlash('Please login with the appropriate account  for set schedule','error');    
                }
                
                return redirect()->route('login');
            }
            
        }

        $coaches = $this->model_courseProfile->getCoachByCourseId($course_id);
        $data['coaches'] = $coaches;
        $data['course_id'] = $course_id;
        return view('coach.schedule.indexSchedule', $data);   
    }

    public function detailCoach($course_id,$id, Request $request)
    {
        $events = [];
        $detailSchedule = array();
        $currentDate = date("Y-m-d");
        $firstDateMonth = date('Y-m-01');

        if ($request->type_order == 'corporate') {
            $order_product_id = baseDecrypt($request->order_product_id);
            $checkOrderSchedule = CoachScheduleOrder::where('order_product_id',$order_product_id)
                                                ->get();
        } else {
            $checkOrderSchedule = CoachScheduleOrder::where('order_id',$request->order_id)
                                                ->where('order_id', '!=', null)
                                                ->get();    
        }
        
        if (count($checkOrderSchedule) == 0) {

            $coach = Coach::find($id);
            if ($coach) {

                $scheduleBooked = CoachScheduleOrder::where('coach_id',$id)
                                                    ->whereDate('start_date','>=',$firstDateMonth)
                                                    ->get();

                if (count($scheduleBooked) > 0) {
                    foreach ($scheduleBooked as $key => $scheduleCust) {

                        if (date("Y-m-d",strtotime($scheduleCust->start_date)) >= $currentDate) {
                            $status = 'locked';
                        } else {
                            $status = 'locked-expired';
                        }

                        $coachSchedule['id'] = $scheduleCust->coach_schedule_id;
                        $coachSchedule['start'] = $scheduleCust->start_date.' '.$scheduleCust->start_time;
                        $coachSchedule['end'] = $scheduleCust->end_date.' '.$scheduleCust->end_time;
                        $coachSchedule['status'] = $status;
                        $interval = round((strtotime($coachSchedule['end']) - strtotime($coachSchedule['start']))/3600, 1)*60;
                        $coachSchedule['interval'] = $interval;

                        $detailSchedule[] = $coachSchedule;
                    }
                }

                $scheduleAvailable = CoachSchedule::leftjoin('coach_schedule_orders',function($join){
                    $join->on('coach_schedules.id', '=', 'coach_schedule_orders.coach_schedule_id');
                    
                })
                ->leftjoin('schedules', function($join){
                    $join->on('schedules.id', '=', 'coach_schedules.schedule_id');
                })
                ->where('coach_schedules.coach_id',$id)
                ->whereNull('coach_schedule_orders.id')
                ->get(['coach_schedules.*','schedules.interval']);

                if (count($scheduleAvailable) > 0) {
                    foreach ($scheduleAvailable as $key => $sched) {

                        if (date("Y-m-d",strtotime($sched->start_date)) >= $currentDate) {
                            $status = 'available';
                        } else {
                            $status = 'available-expired';
                        }

                        $coachScheduleDef['id'] = $sched->id;
                        $coachScheduleDef['start'] = $sched->start_date.' '.$sched->start_time;
                        $coachScheduleDef['end'] = $sched->end_date.' '.$sched->end_time;
                        $coachScheduleDef['status'] = $status;
                        $interval = round((strtotime($coachScheduleDef['end']) - strtotime($coachScheduleDef['start']))/3600, 1)*60;
                        $coachScheduleDef['interval'] = $interval;

                        $detailSchedule[] = $coachScheduleDef;
                    }
                }

                // $coachSchedules = $coach->coach_schedules;
                if (count($detailSchedule) > 0) {
                    $x = 0;
                    $c = 0;
                    $date = '';
                    foreach (array_values($detailSchedule) as $key => $schedule) {
                        if ($schedule['status'] != 'available-expired') {
                            $x++;
                        }
                        $d = date('d-m-Y',strtotime($schedule['start']));
                        if ($d != $date) {
                            $c = 0;
                            $date = $d;
                        }
                        $c++;
                        $events[] = $this->model_schedule->setEvent(
                                $schedule['start'],
                                $schedule['end'],
                                $schedule['status'],
                                $schedule['id'],
                                $schedule['interval'],
                                null,
                                $x,
                                $c
                            );
                    }
                }

            } else {
                sessionFlash('Data coach not found, please select coach','error');
                return redirect()->route('coach.scheduler',$course_id);
            }

            $calendar = \Calendar::addEvents($events)->setCallbacks([
                    // 'eventClick' => "function(event, jsEvent, view) {
                    //     var id = event.id;
                    //     if (event.className[0] === 'sc-available') {
                    //         event.className[0] = 'sc-booking';
                    //         event.title = 'BOOKING';
                    //         $(this).css('background-color', '#ff851b');
                    //         $('#value-temp').append('<span id='+id+'>'+id+'</span>');


                    //     } else if (event.className[0] === 'sc-booking') {
                    //         event.className[0] = 'sc-available';
                    //         event.title = 'AVAILABLE';
                    //         $(this).css('background-color', '#3a87ad');
                    //         $('#'+id).remove();
                    //     } else if (event.className[0] === 'sc-locked') {
                    //         return false;
                    //     } else if (event.className[0] === 'sc-expired') {
                    //         return false;
                    //     }
                    // }"

                     'eventClick' => "function(event, jsEvent, view) {
                        var id = event.id;
                        var interval = event.dataInterval;
                        var sesi = $('#sess').val();

                        var dateClassBefore=null;
                        var haveChooseBefore=$('#_haveChooseBefore').val();
                        var keyBefore=$('#_keyBefore').val();
                        var keyDateBefore=$('#_keyDateBefore').val();

                        if (event.className[0] === 'sc-available') {
                            var dt = new Date();
                            var time = dt.getHours() + '-' + dt.getMinutes() + '-' + dt.getSeconds();
                            var keyClass = event.className[1];
                            var dateClass = event.className[3];
                            var countClass = event.className[4].split('-')[1]-1;
                            var next = true;
                            var classBookNext = $('.'+keyClass+'.'+dateClass).parent('td').parent('tr').next().find('td a.'+dateClass).attr('class');


                            

                            if ($('.fc-widget-content').find('[class*=mark]').length > 0 && countSesi == 0) {
                                toastr.info('Schedule has been choose.');
                                next = false;
                                return false;
                            }

                            if (($('.'+keyClass+'.'+dateClass).parent('td').parent('tr').next().find('td a.'+dateClass).length == 0 && countSesi == 0 && maxSesi > (interval+(interval*countSesi)))){
                                toastr.info('Please choose another schedule.');
                                
                                next = false;
                                return false;
                            }

                            var check = $('.fc-widget-content').find('[class*='+dateClass+']').length;
                            if ($('.fc-popover.fc-more-popover').length > 0) {
                                check = check / 2;
                            }
                           

                            var w = Math.ceil(maxSesi / interval);
                            if ($('.fc-widget-content').find('[class*='+dateClass+']').length < w || (check - countClass) < w && countSesi == 0) {
                                toastr.info('Please choose another schedule.');
                                
                                next = false;
                                return false;
                            }


                            if (next) {
                                if ((maxSesi <= (interval+(interval*countSesi))) || (classBookNext === undefined && countSesi > 0) || (classBookNext.split(' ')[10] === undefined)) {

                                    
                                    if(haveChooseBefore==1 && keyDateBefore!=dateClass){
                                        toastr.info('Please choose another schedule. Schedule date must be same !');
                                        return false;
                                    }

                                    if ($('.'+keyClass+'.'+dateClass).parent('td').parent('tr').next().find('td a.'+dateClass).hasClass('sc-locked') == true && sesi > 1 && countSesi < sesi && haveChooseBefore==1) {
                                    //if (sesi > 1 && countSesi < sesi && haveChooseBefore==1) {
                                        toastr.info('Please choose another schedule. Session schedule has to be continous');
                                        return false;
                                    
                                    } else {

                                        event.className[0] = 'sc-booking';
                                        event.title = 'BOOKING';
                                        event.backgroundColor = '#ff851b';

                                        $('.'+keyClass+'.'+dateClass).css('background-color', '#ff851b');
                                        $('.'+keyClass+'.'+dateClass).addClass('mark-'+time);
                                        event.className[2] = 'mark-'+time;
                                        $('#value-temp').append('<span id='+id+'>'+id+'</span>');

                                        $('a.'+keyClass+'.'+dateClass+' .fc-title').html('BOOKING');

                                        $('.'+keyClass+'.'+dateClass).addClass('mark-'+time);
                                        // if (tempSesi != sesi){
                                        //     tempSesi++;
                                        //     $('.'+keyClass+'.'+dateClass).parent('td').parent('tr').next().find('td a.'+dateClass).click();
                                        // } else {
                                        //     tempSesi = 1;
                                        // }
                                        
                                        
                                        $('#_haveChooseBefore').val(0);
                                        $('#_keyDateBefore').val(dateClass);

                                        if (maxSesi > (interval+(interval*countSesi))) {
                                            countSesi++;
                                           
                                            $('.'+keyClass+'.'+dateClass).parent('td').parent('tr').next().find('td a.'+dateClass).click();
                                        } else {
                                            $('#_haveChooseBefore').val(0);
                                            countSesi = 0;
                                        }
                                        


                                    }
                                } else {
                                    toastr.info('Please choose another schedule.');
                                    
                                }
                                
                            } else {
                               
                                toastr.info('Please choose another schedule.');
                            }
                            


                        } else if (event.className[0] === 'sc-booking') {
                            var classBook = $(this).attr('class').split(' ')[10];
                            
                            event.className[0] = 'sc-available';
                            var keyClass = event.className[1];
                            var dateClass = event.className[3];

                            event.title = 'AVAILABLE';
                            $('.'+keyClass+'.'+dateClass).css('background-color', '#3a87ad');
                            event.backgroundColor = '#3a87ad';
                            $('#'+id).remove();

                            $('a.'+keyClass+'.'+dateClass+' .fc-title').html('AVAILABLE');

                            if ($(this).parent('div').length == 1) {
                                classBook = event.className[2];
                            }

                            if ($('.'+keyClass+'.'+dateClass).hasClass(classBook)) {
                                $('.'+keyClass+'.'+dateClass).removeClass(classBook);
                                event.className[2] = 'tempClass';
                                $('.'+keyClass+'.'+dateClass).parent().parent().parent().find('tr td a.'+classBook).first().click();
                            }

                            if (maxSesi > (interval+(interval*countSesi))){
                                countSesi++;
                            } else {
                                countSesi = 0;
                            }

                            $('#_haveChooseBefore').val(0);

                        } else if (event.className[0] === 'sc-locked') {
                            $('#_haveChooseBefore').val(1);
                            return false;
                        } else if (event.className[0] === 'sc-expired') {
                            $('#_haveChooseBefore').val(1);
                            return false;
                        }
                    }",

                    'eventMouseover'=> "function(event, jsEvent, view) {
                        var start = moment(event.start).format('h:mm:ss a');
                        var end = moment(event.end).format('h:mm:ss a')
                        var tooltip = '<div class=\'tooltipevent\' style=\'width:100px;height:50px;background:#ffe1b2;position:absolute;z-index:10001;border: 1px solid black;padding-top: 4px;padding-right: 5px;padding-bottom: 5px;padding-left: 5px;\'>' + start + '-' + end +'</div>';
                        var _tooltip = $(tooltip).appendTo('body');

                        $(this).mouseover(function(e) {
                            $('.tooltipevent').tooltip();
                            $(this).css('z-index', 10000);
                            _tooltip.fadeIn('500');
                            _tooltip.fadeTo('10', 1.9);
                        }).mousemove(function(e) {
                            _tooltip.css('top', e.pageY + 10);
                            _tooltip.css('left', e.pageX + 20);
                        });
                    },

                    eventMouseout: function(calEvent, jsEvent) {
                        $(this).css('z-index', 8);
                        $('.tooltipevent').remove();
                    }"
                ]);

            $sess = 1;

            if ($request->has('product_id')) {
                $product = Product::whereProductCode($request->product_id)->first();
                $sess = ($product->session > 0 ) ? $product->session : 1;
            }

            $return = [
                        'course_id' => $course_id,
                        'coach' => $coach,
                        'calendar' => $calendar,
                        'sess' => $sess
                    ];
            return view('coach.schedule.detail_coach',$return);
        } else {
            sessionFlash('Your order has been selected schedule','error');
            return redirect()->back();
        }   
    }

    public function submitLogOrderSchedule(Request $request)
    {   
        DB::beginTransaction();
        try {

            $params = $request->all();
            $coach_schedule_ids = $request->schedule_id;

            $count_schedule_temp=0;
            $count_schedule_order=0;

            if (is_array($coach_schedule_ids)) {
                Log::info(count($coach_schedule_ids));
                for ($i=0; $i <count($coach_schedule_ids) ; $i++) {
                    Log::info('perulangan-'.$i.' isinya : '.$coach_schedule_ids[$i]); 

                     $checkBookedScheduleOrder_ = CoachScheduleOrder::where('coach_schedule_id',$coach_schedule_ids[$i])->get();
                     $checkBookedScheduleTemp_ = LogOrderScheduleTemp::where('coach_schedule_id',$coach_schedule_ids[$i])->get();
                     $count_schedule_temp +=count($checkBookedScheduleTemp_);
                     $count_schedule_order +=count($checkBookedScheduleOrder_);
                }
            }else{
                
                $checkBookedScheduleOrder = CoachScheduleOrder::whereIn('coach_schedule_id',$coach_schedule_ids)->get();
                $checkBookedScheduleTemp = LogOrderScheduleTemp::whereIn('coach_schedule_id',$coach_schedule_ids)->get();
                $count_schedule_temp +=count($checkBookedScheduleTemp);
                $count_schedule_order +=count($checkBookedScheduleOrder);
            }

            Log::info('jumlah coach schedule: '.$count_schedule_temp);
            Log::info('jumlah coach schedule order: '.$count_schedule_order);

            $ids = array();

            $checkBookedScheduleOrder = CoachScheduleOrder::whereIn('coach_schedule_id',$coach_schedule_ids)->get();
            $checkBookedScheduleTemp = LogOrderScheduleTemp::whereIn('coach_schedule_id',$coach_schedule_ids)->get();
            Log::info($coach_schedule_ids);


             if ( count($checkBookedScheduleTemp) > 0 || count($checkBookedScheduleOrder) > 0 ) {
            //if (count($checkBookedScheduleOrder) > 0 ) {
                $return = array(
                        'status'=>'error',
                        'code'=>400,
                        'data'=>array(),
                        'message'=>'Schedule has been not available. Please choose another schedule.',
                    );
                return response()->json($return, $return['code']);
            }

            if (count($coach_schedule_ids) > 0) {
                if (@$params['type_order'] == 'corporate') {
                    $coachId = 0;
                    // $timestart = strtotime(date('Y-m-d H:i:s'));
                    // $timeend = strtotime(date('Y-m-d H:i:s').' +1 month');

                    $str_user_id = explode("-", $request->get('user_id'));

                    if (count($str_user_id) > 1) {
                        $user_id = baseDecrypt( $str_user_id[1] );
                    } else {
                        $user_id = $str_user_id[0];
                    }    
                    
                    $order_product_id = baseDecrypt($request->get('order_product_id'));

                    //check user login
                    $currentUser = user_info();
                    if (!$currentUser) {
                        // return redirect()->route('login');
                        $return = array(
                            'status'=>'error',
                            'code'=>403,
                            'data'=>array(),
                            'message'=>'Save data schedule failed. Please login before submit schedule',
                        );
                        return response()->json($return, $return['code']);

                    } else {
                        if ( $currentUser->id != $user_id ) {
                            $return = array(
                                'status'=>'error',
                                'code'=>403,
                                'data'=>array(),
                                'message'=>'Save data schedule failed. Access Forbidden',
                            );
                            return response()->json($return, $return['code']);
                        }
                    }
                    // End Current User

                    // if from corporate
                    $dataSchedules = CoachSchedule::whereIn('id',$params['schedule_id'])->get();
                    if (count($dataSchedules) > 0) {
                        foreach ($dataSchedules as $key => $value) {
                            
                            $value->coach_schedule_id = $value->id;
                            $value->order_product_id = $order_product_id;
                            $value->start = $value->start_date.' '.$value->start_time;
                            $value->end = $value->end_date.' '.$value->end_time;
                            $value->product_id = $request->product_id;
                            $model_scheduleOrder = new CoachScheduleOrder();
                            $data = $model_scheduleOrder->saveScheduleOrder($value);
                            
                            $ids[] = $data->id;

                            $coachId = $value->coach_id;

                            if ($key == 0) {
                                $timestart = strtotime($value->start_date.' '.$value->start_time);
                            }
                            $timeend = strtotime($value->end_date.' '.$value->end_time);
                        }
                    }

                    $courses = $this->model_prductSet->where('product_id', $request->product_id)->get();
                    // enroll user
                    if (count($courses) > 0 && $coachId != 0) {
                        $coachDetail = Coach::find($coachId);
                        $orderProductDetail = OrderProduct::find($order_product_id);
                        $userDetail = User::find($orderProductDetail->user_id);
                        
                        $dataCourse = array(
                            'moodlewsrestformat'    => 'json',
                            'field'                 => 'id',
                            'value'                 => $coachDetail->course_id,
                        );

                        $course = CurlRequest::requestMoodle('POST', 'core_course_get_courses_by_field', $dataCourse, env('TOKEN_GET_COURSE'));

                        if (@$course->courses[0]->shortname) {
                            $bbbLink = env('BASE_URL_MOODLE').'/course/view.php?name='.$course->courses[0]->shortname;

                            $orderProductDetail->update(['bbb_link' => $bbbLink]);
                        } else {
                            $return = array(
                                'status'=>'error',
                                'code'=>400,
                                'data'=>array(),
                                'message'=>'Coach course not available',
                            );
                            return response()->json($return, $return['code']);
                        }

                        foreach ($courses as $key => $course) {
                            $sendRequest = new Request;
                            $enroll = new enrollUserApi;

                            $sendRequest->merge([
                                'email' => $coachDetail->user_coach->email,
                                'email_user' => $userDetail->email,
                                'course_id' => $coachDetail->course_id,
                                'enrol_id' => $course->enrol_id,
                                'shortname' => 'name',
                                'timestart' => $timestart,
                                'timeend' => $timeend,
                                'reschedule' => false,
                                'product_id' => $request->product_id
                            ]);

                            // $dataSendRequest[$key] = $sendRequest;
                            $enroll->enrollUser($sendRequest);

                            $sendRequestEnrolProduct = new Request;
                            $sendRequestEnrolProduct->merge([
                                'email' => $coachDetail->user_coach->email,
                                'email_user' => $userDetail->email,
                                'course_id' => $course->course_id,
                                'enrol_id' => $course->enrol_id,
                                'shortname' => 'name',
                                'timestart' => $timestart,
                                'timeend' => $timeend,
                                'reschedule' => false,
                                'product_id' => $request->product_id
                            ]);

                            // $dataSendRequest[$key] = $sendRequest;
                            $enroll->enrollUser($sendRequestEnrolProduct);
                        }

                    }

                    if (!empty($ids)) {

                        $orderProduct = OrderProduct::find($order_product_id);
                        if (count($orderProduct) > 0) {
                            $customerUser = Sentinel::findById($orderProduct->user_id);
                            $product = Product::where('product_code', $request->product_id)->first();
                            $product->productSets;

                            $data_email['link_bbb'] = $bbbLink;
                            $data_email['product'] = $product;
                            $data_email['schedule'] = CoachScheduleOrder::where('order_product_id',$order_product_id)->orderBy('start_date','ASC')->orderBy('start_time','ASC')->get();

                            /*==== SEND EMAIL TO CUSTOMER =====*/
                            $data_email['subject'] = 'Your Confirmed Schedule for '.$orderProduct->product->name;
                            $data_email['content'] = '<p>You have successfully been booked for a coaching sessions with '.$coachDetail->name.'. See below for full details.</p>';
                            $data_email['user'] = $customerUser;
                            $data_email['type_email']='';
                            

                            $mail = \Mail::to( $data_email['user']->email )->send(new ScheduleRequestProduct($data_email));
                            /*===ENDSEND EMAIL TO CUSTOMER====*/

                            /*==== SEND EMAIL TO COACH ====*/
                            $data_email['subject'] = 'Booking Schedule Request Product '.$orderProduct->product->name;
                            $data_email['content'] = 'Set schedule for product <b> '.$orderProduct->product->name.' </b> has been successfully. '.$customerUser->first_name.' '.$customerUser->last_name.'Has booked your time.';
                            $data_email['user'] = $coachDetail->user_coach;
                            $data_email['type_email']='';
                            
                            $mail = \Mail::to( $data_email['user']->email )->send(new ScheduleRequestProduct($data_email));
                            /*=== END EMAIL TO COACH ===*/
                        }

                        $return = array(
                            'status'=>'success',
                            'code'=>200,
                            'data'=>$data,
                            'message'=>'Master schedule has been sucessfully saved',
                        );
                    } else {
                        $return = array(
                            'status'=>'error',
                            'code'=>400,
                            'data'=>array(),
                            'message'=>'Save data schedule failed',
                        );
                    }
                    // end from corporate
                } else {
                    /*
                    $currentUser = user_info();
                    if (!$currentUser) {

                        $return = array(
                            'status'=>'error',
                            'code'=>403,
                            'data'=>array(),
                            'message'=>'Please login before purchase product',
                        );
                        return response()->json($return, 400);
                    }
                    
                    $next = false;
                    $prodSet = ProductSet::where('product_id', $request->product_id)->first();
                    if ($prodSet) {
                        $userEnrol = $this->userEnrol->getUserEnrol($currentUser->email, $prodSet->enrol_id);
                        if ($userEnrol) {
                            $dateNow = strtotime(date('Y-m-d H:i:s'));
                            if ($dateNow > $userEnrol->timeend) {
                                $next = true;
                            }
                        }
                    }

                    if (!$next) {
                        $return = array(
                            'status'=>'error',
                            'code'=>400,
                            'data'=>array(),
                            'message'=>'The product has been purchased before. Please choose another product',
                        );
                        return response()->json($return, $return['code']);
                    }
                    */

                    $data = $this->model_logOrderSchedule->saveData($params);

                    if ($data) {
                        $return = array(
                            'status'=>'success',
                            'code'=>200,
                            'data'=>$data,
                            'message'=>'Save data log order schedule has been successfully',
                        );
                    } else {
                        $return = array(
                            'status'=>'error',
                            'code'=>400,
                            'data'=>array(),
                            'message'=>'Save data log order schedule failed',
                        );
                    }    
                }
                
                DB::commit();
            } else {
                $return = array(
                        'status'=>'error',
                        'code'=>400,
                        'data'=>array(),
                        'message'=>'Save data has been failed. Please select your schedule',
                    );
            }

        } catch (\Exception $e) {
            DB::rollback();
            $return = array(
                    'status'=>'error',
                    'code'=>400,
                    'data'=>array(),
                    'message'=>'Save data log order schedule failed. '.$e->getMessage().' '. $e->getLine(),
                );
        }

        return response()->json($return, $return['code']);

    }

    public function masterSchedule(MasterScheduleDatatable $dataTable)
    {
        return $dataTable->render('admin.setting.schedule.index');
    }

    public function masterScheduleCreate()
    {   
        $resdata['course'] = array();
        $user = Sentinel::getUser();
        $results = CoachCourse::all();
        if (count($results) > 0) {
            foreach ($results as $result) {
                $dataCours[$result->id] =$result->name;
                $resdata['course'] = $dataCours; 
            }
            
        }

        //GET DATA COURSE FROM MOODLE
        /*
        $client = new Client();
        try {

            $token = '46f0823f3d04bbbcdc28209ea428cd55';
            $email = 'undefined';
            $get_course = $client->request('POST', 'http://course.learn1thing.com/webservice/rest/server.php?wstoken='.$token.'&wsfunction=local_get_course_list&moodlewsrestformat=json&req[0][type]=course&req[0][email]='.user_info('email').'');
        
            $dataCourse = json_decode(json_decode($get_course->getBody()))->data;

            $data['course'] = $dataCourse;
            
        } catch (ClientException $e) {
            $data['course'] = array();

        }

        return json_encode($data);
        */

        return view('admin.setting.schedule.create',$resdata);
    }

    public function storeMasterSchedule(MasterScheduleRequest $request)
    {
        if (strtotime($request->get('start_date')) > strtotime($request->get('end_date'))) {
            
            sessionFlash('End Date must be greater than Start Date','error');
            return redirect()->back()->withInput();
        }

        if (strtotime($request->get('start_time')) >= strtotime($request->get('end_time'))) {
            
            sessionFlash('End Time must be greater than Start Time','error');
            return redirect()->back()->withInput();
        }

        DB::beginTransaction();
        try {
            $user = Sentinel::getUser();

            $modelSchedule = new Schedule();
            $schedule = $modelSchedule->saveSchedule($request,$user);
            if ($schedule) {
                sessionFlash('Master schedule has been sucessfully saved','success');
            } else {
                sessionFlash('Save data schedule has been failed','error');
            }
            DB::commit();
            return redirect()->route('master.schedule');   
        } catch (\Exception $e) {
            DB::rollback();
            sessionFlash($e->getMessage(),'error');
            return redirect()->back()->withInput();
        }
    }

    public function deleteMasterSchedule($id) {
        DB::beginTransaction();
        try {

            $schedule = $this->model_schedule->deleteById($id);
            DB::commit();

            if ($schedule) {
                sessionFlash('Data is successfully deleted','success');
                return redirect()->route('master.schedule');
            } else {
                sessionFlash('Delete data has been failed','error');
                return redirect()->route('master.schedule');
            }

        } catch (\Exception $e) {
            DB::rollback();
            sessionFlash($e->getMessage(),'error');
            return redirect()->back()->withInput();
               
        }
    }

    public function editScheduleOrder($course_id,$id, Request $request)
    {

        $events = [];
        $detailSchedule = array();
        $currentDate = date("Y-m-d");
        $firstDateMonth = date('Y-m-01');
        $tempCurrentValue = '';

        if (@$request->type_order == 'corporate') {
            $str_user_id = explode("-", $request->get('user_id'));

            if (count($str_user_id) > 1) {
                $user_id = baseDecrypt( $str_user_id[1] );
            } else {
                $user_id = $str_user_id[0];
            }
            
            $order_product_id = baseDecrypt($request->get('order_product_id'));

            //check user login
            $currentUser = user_info();
            if ( @$currentUser->id != $user_id ) {

                $user = User::find($user_id);

                sessionFlash('Please login as '.$user->first_name.' '.$user->last_name. 'for edit schedule','error');
                
                return redirect()->route('login');
            }
            
        }

        $coach = Coach::find($id);
        if ($coach) {

            if (@$request->type_order == 'corporate') {
                $scheduleBooked = CoachScheduleOrder::where('coach_id',$id)
                    ->whereDate('start_date','>=',$firstDateMonth)
                    ->where('order_product_id','<>',$order_product_id)
                    ->where('product_id','<>',$request->get('product_id'))
                    ->get();
            } else {
                $scheduleBooked = CoachScheduleOrder::where('coach_id',$id)
                    ->whereDate('start_date','>=',$firstDateMonth)
                    ->where('order_id','<>',$request->get('order_id'))
                    ->where('product_id','<>',$request->get('product_id'))
                    ->get();    
            }
            

            if (count($scheduleBooked) > 0) {
                foreach ($scheduleBooked as $key => $scheduleCust) {

                    if (date("Y-m-d",strtotime($scheduleCust->start_date)) >= $currentDate) {
                        $status = 'locked';
                    } else {
                        $status = 'locked-expired';
                    }

                    $coachSchedule['id'] = $scheduleCust->id;
                    $coachSchedule['start'] = $scheduleCust->start_date.' '.$scheduleCust->start_time;
                    $coachSchedule['end'] = $scheduleCust->end_date.' '.$scheduleCust->end_time;
                    $coachSchedule['status'] = $status;
                    
                    $interval = round((strtotime($coachSchedule['end']) - strtotime($coachSchedule['start']))/3600, 1)*60;
                    $coachSchedule['interval'] = $interval;

                    $detailSchedule[] = $coachSchedule;
                }
            }

            if (@$request->type_order == 'corporate') {
                $scheduleOrderBooked = CoachScheduleOrder::where('coach_id',$id)
                    ->where('order_product_id',$order_product_id)
                    ->where('product_id',$request->get('product_id'))
                    ->get();
            } else {
                $scheduleOrderBooked = CoachScheduleOrder::where('coach_id',$id)
                    ->where('order_id',$request->get('order_id'))
                    ->where('product_id',$request->get('product_id'))
                    ->get();                
            }



            if (count($scheduleOrderBooked) > 0) {
                foreach ($scheduleOrderBooked as $key => $scheduleOrder) {
                    if (date("Y-m-d",strtotime($scheduleOrder->start_date)) >= $currentDate) {
                        $status = 'ordered';
                    } else {
                        $status = 'ordered-expired';
                    }

                    $coachScheduleOrder['id'] = $scheduleOrder->coach_schedule_id;
                    $coachScheduleOrder['start'] = $scheduleOrder->start_date.' '.$scheduleOrder->start_time;
                    $coachScheduleOrder['end'] = $scheduleOrder->end_date.' '.$scheduleOrder->end_time;
                    $coachScheduleOrder['status'] = $status;
                    $interval = round((strtotime($coachScheduleOrder['end']) - strtotime($coachScheduleOrder['start']))/3600, 1)*60;
                    $coachScheduleOrder['interval'] = $interval;

                    $detailSchedule[] = $coachScheduleOrder;

                    $tempCurrentValue .= '<span id="'.$scheduleOrder->coach_schedule_id.'">"'.$scheduleOrder->coach_schedule_id.'"</span>';
                }
            }

            $scheduleAvailable = CoachSchedule::leftjoin('coach_schedule_orders',function($join){
                $join->on('coach_schedules.id', '=', 'coach_schedule_orders.coach_schedule_id');
            })
            ->where('coach_schedules.coach_id',$id)
            ->whereNull('coach_schedule_orders.id')
            ->get(['coach_schedules.*']);

            if (count($scheduleAvailable) > 0) {
                foreach ($scheduleAvailable as $key => $sched) {

                    if (date("Y-m-d",strtotime($sched->start_date)) >= $currentDate) {
                        $status = 'available';
                    } else {
                        $status = 'available-expired';
                    }

                    $coachScheduleDef['id'] = $sched->id;
                    $coachScheduleDef['start'] = $sched->start_date.' '.$sched->start_time;
                    $coachScheduleDef['end'] = $sched->end_date.' '.$sched->end_time;
                    $coachScheduleDef['status'] = $status;
                    $interval = round((strtotime($coachScheduleDef['end']) - strtotime($coachScheduleDef['start']))/3600, 1)*60;
                    $coachScheduleDef['interval'] = $interval;

                    $detailSchedule[] = $coachScheduleDef;
                }
            }

            // $coachSchedules = $coach->coach_schedules;
            // if (count($detailSchedule) > 0) {
            //     foreach ($detailSchedule as $schedule) {
            //         $events[] = $this->model_schedule->setEvent(
            //                 $schedule['start'],
            //                 $schedule['end'],
            //                 $schedule['status'],
            //                 $schedule['id']
            //             );
            //     }
            // }

            if (count($detailSchedule) > 0) {
                $x = 0;
                $c = 0;
                $date = '';
                foreach (array_values($detailSchedule) as $key => $schedule) {
                    if ($schedule['status'] != 'available-expired') {
                        $x++;
                    }
                    $d = date('d-m-Y',strtotime($schedule['start']));
                    if ($d != $date) {
                        $c = 0;
                        $date = $d;
                    }
                    $c++;
                    $events[] = $this->model_schedule->setEvent(
                            $schedule['start'],
                            $schedule['end'],
                            $schedule['status'],
                            $schedule['id'],
                            $schedule['interval'],
                            null,
                            $x,
                            $c
                        );
                }
            }


        } else {
            sessionFlash('Data coach not found','error');
            return redirect()->route('coach.scheduler',$course_id);
        }

        $calendar = \Calendar::addEvents($events)->setCallbacks([
                // 'eventClick' => "function(event, jsEvent, view) {
                //     var id = event.id;
                //     if (event.className[0] === 'sc-available') {
                //         event.className[0] = 'sc-booking';
                //         event.title = 'BOOKING';
                //         $(this).css('background-color', '#ff851b');
                //         $('#value-temp').append('<span id='+id+'>'+id+'</span>');

                //     } else if (event.className[0] === 'sc-booking' || event.className[0] === 'sc-ordered') {
                //         event.className[0] = 'sc-available';
                //         event.title = 'AVAILABLE';
                //         $(this).css('background-color', '#3a87ad');
                //         $('#'+id).remove();
                //     } else if (event.className[0] === 'sc-locked') {
                //         return false;
                //     } else if (event.className[0] === 'sc-expired') {
                //         return false;
                //     }
                // }"
                /*
                'eventClick' => "function(event, jsEvent, view) {
                        var id = event.id;
                        var sesi = $('#sess').val();
console.log('test');
console.log(event.className[0]);
                        if (event.className[0] === 'sc-available') {
console.log(event.className[0]);
                            var dt = new Date();
                            var time = dt.getHours() + '-' + dt.getMinutes() + '-' + dt.getSeconds();
                            var keyClass = event.className[1];
                            var dateClass = event.className[3];
                            var next = true;
                            var classBookNext = $('.'+keyClass+'.'+dateClass).parent('td').parent('tr').next().find('td a.'+dateClass).attr('class');

                            if ($('.fc-widget-content').find('[class*=mark]').length > 0 && tempSesi == 1) {
                                toastr.info('You have chosen the schedule.');
                                next = false;
                                return false;
                            }

                            if (($('.'+keyClass+'.'+dateClass).parent('td').parent('tr').next().find('td a.'+dateClass).length == 0 && tempSesi == 1 && sesi != 1)){
                                toastr.info('Please choose another schedule.');
                                next = false;
                                return false;
                            }

                            if (next) {
                                if ((sesi == 1) || (classBookNext === undefined && tempSesi > 1) || (classBookNext.split(' ')[10] === undefined)) {

                                    if ($('.'+keyClass+'.'+dateClass).parent('td').parent('tr').next().find('td a.'+dateClass).hasClass('sc-locked') == true && sesi > 1 && tempSesi < sesi) {

                                        toastr.info('Please choose another schedule.');
                                        return false;
                                    
                                    } else {

                                        event.className[0] = 'sc-booking';
                                        event.title = 'BOOKING';
                                        event.backgroundColor = '#ff851b';

                                        $('.'+keyClass+'.'+dateClass).css('background-color', '#ff851b');
                                        $('.'+keyClass+'.'+dateClass).addClass('mark-'+time);
                                        event.className[2] = 'mark-'+time;
                                        $('#value-temp').append('<span id='+id+'>'+id+'</span>');

                                        $('a.'+keyClass+'.'+dateClass+' .fc-title').html('BOOKING');

                                        $('.'+keyClass+'.'+dateClass).addClass('mark-'+time);
                                        if (tempSesi != sesi){
                                            tempSesi++;
                                            $('.'+keyClass+'.'+dateClass).parent('td').parent('tr').next().find('td a.'+dateClass).click();
                                        } else {
                                            tempSesi = 1;
                                        }

                                    }
                                } else {
                                    toastr.info('Please choose another schedule.');
                                }
                                
                            } else {
                                toastr.info('Please choose another schedule.');
                            }
                            


                        } else if (event.className[0] === 'sc-booking' || event.className[0] === 'sc-ordered') {
                            var classBook = $(this).attr('class').split(' ')[10];
                            
                            event.className[0] = 'sc-available';
                            var keyClass = event.className[1];
                            var dateClass = event.className[3];

                            event.title = 'AVAILABLE';
                            $('.'+keyClass+'.'+dateClass).css('background-color', '#3a87ad');
                            event.backgroundColor = '#3a87ad';
                            $('#'+id).remove();

                            $('a.'+keyClass+'.'+dateClass+' .fc-title').html('AVAILABLE');

                            if ($(this).parent('div').length == 1) {
                                classBook = event.className[2];
                            }

console.log(classBook);
console.log($('.'+keyClass+'.'+dateClass).hasClass(classBook));
console.log($('.'+keyClass+'.'+dateClass).parent().parent().parent().find('tr td a.'+classBook).first());
                            if ($('.'+keyClass+'.'+dateClass).hasClass(classBook)) {
                                $('.'+keyClass+'.'+dateClass).removeClass(classBook);
                                event.className[2] = 'tempClass';
                                $('.'+keyClass+'.'+dateClass).parent().parent().parent().find('tr td a.'+classBook).first().click();
                            }

                            if (tempSesi != sesi){
                                tempSesi++;
                            } else {
                                tempSesi = 1;
                            }

                        } else if (event.className[0] === 'sc-locked') {
                            return false;
                        } else if (event.className[0] === 'sc-expired') {
                            return false;
                        }
                    }"
                */

                     'eventClick' => "function(event, jsEvent, view) {
                        var id = event.id;
                        var interval = event.dataInterval;
                        var sesi = $('#sess').val();

                        var dateClassBefore=null;
                        var haveChooseBefore=$('#_haveChooseBefore').val();
                        var keyBefore=$('#_keyBefore').val();
                        var keyDateBefore=$('#_keyDateBefore').val();
            

                        if (event.className[0] === 'sc-available') {
                            var dt = new Date();
                            var time = dt.getHours() + '-' + dt.getMinutes() + '-' + dt.getSeconds();
                            var keyClass = event.className[1];
                            var dateClass = event.className[3];
                            var countClass = event.className[4].split('-')[1]-1;
                            var next = true;
                            var classBookNext = $('.'+keyClass+'.'+dateClass).parent('td').parent('tr').next().find('td a.'+dateClass).attr('class');

                            if ($('.fc-widget-content').find('[class*=mark]').length > 0 && countSesi == 0) {
console.log('kondisi 2');
                                toastr.info('You have chosen the schedule.');
                                next = false;
                                return false;
                            }

                            if (($('.'+keyClass+'.'+dateClass).parent('td').parent('tr').next().find('td a.'+dateClass).length == 0 && countSesi == 0 && maxSesi > (interval+(interval*countSesi)))){
console.log('kondisi 3');                      
          toastr.info('Please choose another schedule.');
                                next = false;
                                return false;
                            }

                            var check = $('.fc-widget-content').find('[class*='+dateClass+']').length;
                            if ($('.fc-popover.fc-more-popover').length > 0) {
                                check = check / 2;
                            }
                            var w = Math.ceil(maxSesi / interval);
                            if ($('.fc-widget-content').find('[class*='+dateClass+']').length < w || (check - countClass) < w && countSesi == 0) {
console.log('kondisi 4');
                                toastr.info('Please choose another schedule.');
                                next = false;
                                return false;
                            }


                            if (next) {
                                if ((maxSesi <= (interval+(interval*countSesi))) || (classBookNext === undefined && countSesi > 0) || (classBookNext.split(' ')[10] === undefined)) {
                                    
                                    if(haveChooseBefore==1 && keyDateBefore!=dateClass){
                                        toastr.info('Please choose another schedule. Schedule date must be same !');
                                        return false;
                                    }

                                    if ($('.'+keyClass+'.'+dateClass).parent('td').parent('tr').next().find('td a.'+dateClass).hasClass('sc-locked') == true && sesi > 1 && countSesi < sesi && haveChooseBefore==1) {
                                        toastr.info('Please choose another schedule. Session schedule has to be continous');
                                        return false;
                                    
                                    } else {

                                        event.className[0] = 'sc-booking';
                                        event.title = 'BOOKING';
                                        event.backgroundColor = '#ff851b';

                                        $('.'+keyClass+'.'+dateClass).css('background-color', '#ff851b');
                                        $('.'+keyClass+'.'+dateClass).addClass('mark-'+time);
                                        event.className[5] = 'mark-'+time;
                                        $('#value-temp').append('<span id='+id+'>'+id+'</span>');

                                        $('a.'+keyClass+'.'+dateClass+' .fc-title').html('BOOKING');

                                        // $('.'+keyClass+'.'+dateClass).addClass('mark-'+time);
                                        // if (tempSesi != sesi){
                                        //     tempSesi++;
                                        //     $('.'+keyClass+'.'+dateClass).parent('td').parent('tr').next().find('td a.'+dateClass).click();
                                        // } else {
                                        //     tempSesi = 1;
                                        // }


                                        $('#_haveChooseBefore').val(0);
                                        $('#_keyDateBefore').val(dateClass);

                                        if (maxSesi > (interval+(interval*countSesi))) {
                                            countSesi++;
                                            $('.'+keyClass+'.'+dateClass).parent('td').parent('tr').next().find('td a.'+dateClass).click();
                                        } else {
                                            $('#_haveChooseBefore').val(0);
                                            countSesi = 0;
                                        }

                                    }
                                } else {
                                    toastr.info('Please choose another schedule.');
                                }
                                
                            } else {
                                toastr.info('Please choose another schedule.');
                            }
                            


                        } else if (event.className[0] === 'sc-booking' || event.className[0] === 'sc-ordered') {
                            var classBook = $(this).attr('class').split(' ')[10];
console.log('kondisi 1 ordered', classBook);                            
                            event.className[0] = 'sc-available';
                            var keyClass = event.className[1];
                            var dateClass = event.className[3];
console.log('key class', keyClass);
console.log('date class', dateClass);
                            event.title = 'AVAILABLE';
                            $('.'+keyClass+'.'+dateClass).css('background-color', '#3a87ad');
                            event.backgroundColor = '#3a87ad';
                            $('#'+id).remove();

                            $('a.'+keyClass+'.'+dateClass+' .fc-title').html('AVAILABLE');

                            if ($(this).parent('div').length == 1) {
                                classBook = event.className[5];
                            }

                            if ($('.'+keyClass+'.'+dateClass).hasClass(classBook)) {
console.log('masuk sini');
                                $('.'+keyClass+'.'+dateClass).removeClass(classBook);
                                event.className[2] = 'tempClass';
console.log(keyClass);
console.log(dateClass);
console.log(classBook);
console.log($('.'+keyClass+'.'+dateClass).parent().parent().parent().find('tr td a.'+classBook).first());
                                $('.'+keyClass+'.'+dateClass).parent().parent().parent().find('tr td a.'+classBook).first().click();
                            }

                            if (maxSesi > (interval+(interval*countSesi))){
                                countSesi++;
                            } else {
                                countSesi = 0;
                            }

                            $('#_haveChooseBefore').val(0);

                        } else if (event.className[0] === 'sc-locked') {
                            $('#_haveChooseBefore').val(1);
                            return false;
                        } else if (event.className[0] === 'sc-expired') {
                            $('#_haveChooseBefore').val(1);
                            return false;
                        }
                    }",

                    'eventMouseover'=> "function(event, jsEvent, view) {
                        var start = moment(event.start).format('h:mm:ss a');
                        var end = moment(event.end).format('h:mm:ss a')
                        var tooltip = '<div class=\'tooltipevent\' style=\'width:100px;height:50px;background:#ffe1b2;position:absolute;z-index:10001;border: 1px solid black;padding-top: 4px;padding-right: 5px;padding-bottom: 5px;padding-left: 5px;\'>' + start + '-' + end +'</div>';
                        var _tooltip = $(tooltip).appendTo('body');

                        $(this).mouseover(function(e) {
                            $('.tooltipevent').tooltip();
                            $(this).css('z-index', 10000);
                            _tooltip.fadeIn('500');
                            _tooltip.fadeTo('10', 1.9);
                        }).mousemove(function(e) {
                            _tooltip.css('top', e.pageY + 10);
                            _tooltip.css('left', e.pageX + 20);
                        });
                    },

                    eventMouseout: function(calEvent, jsEvent) {
                        $(this).css('z-index', 8);
                        $('.tooltipevent').remove();
                    }"

            ]);

            $sess = 1;

            if ($request->has('product_id')) {
                $product = Product::whereProductCode($request->product_id)->first();
                $sess = ($product->session > 0 ) ? $product->session : 1;
            }

        $return = [
                    'course_id' => $course_id,
                    'coach' => $coach,
                    'calendar' => $calendar,
                    'order_id' => $request->get('order_id'),
                    'temp_current_value' => $tempCurrentValue,
                    'sess' => $sess
                ];

        return view('coach.schedule.edit_coach',$return);
    }

    public function updateScheduleOrder(Request $request)
    {   
        DB::beginTransaction();
        try {
            Log::info('masuk sini update coach');
            Log::info($request->all());

            $params = $request->all();
            $coach_schedule_ids = $request->schedule_id;
            
            if (count($coach_schedule_ids) > 0) {
                
                $data = $this->model_coachScheduleOrder->updateData($params);

                if ($data) {

                    $dataCoachScheduleOrder = $this->model_coachScheduleOrder->getScheduleByIds($data['ids']);

                    $productSet = ProductSet::where(['product_id'=>$params['product_id']])->first();
                    $link = [];
                    // foreach ($productSet as $key => $value) {
                    //     $link['course_url'][] = $value->course_url;
                    // }
                    $link['course_url'][] = $productSet->course_url;
                    (object) $link['course_url'];
                    $resLink[] = $link;

                    $client = new Client();
                    try {

                        if ($request->type_order == 'corporate') {
                            DB::commit();
                            $orderProduct = OrderProduct::find($request->order_product_id);
                            if (count($orderProduct) > 0) {
                                $customerUser = Sentinel::findById($orderProduct->user_id);
                                $coachDetail = Coach::find($dataCoachScheduleOrder[0]['coach_id']);
                                $product = Product::where('product_code', $request->product_id)->first();
                                $product->productSets;

                                $data_email['link_bbb'] = $dataCoachScheduleOrder[0]['url_course'];
                                $data_email['product'] = $product;
                                $data_email['schedule'] = CoachScheduleOrder::where('order_product_id',$request->order_product_id)->orderBy('start_date','ASC')->orderBy('start_time','ASC')->get();

                                /*==== SEND EMAIL TO CUSTOMER =====*/
                                $data_email['subject'] = 'Reschedule Request Product '.$orderProduct->product->name;
                                $data_email['content'] = 'Change schedule for product <b> '.$orderProduct->product->name.' </b> has been successfully. You can access link course on this schedule.';
                                $data_email['user'] = $customerUser;
                                
                                $emailCustomer = $customerUser->email;

                                $mail = \Mail::to( $data_email['user']->email )->send(new ScheduleRequestProduct($data_email));
                                /*===ENDSEND EMAIL TO CUSTOMER====*/

                                /*==== SEND EMAIL TO COACH ====*/
                                $data_email['subject'] = 'Booking Reschedule Request Product '.$orderProduct->product->name;
                                $data_email['content'] = 'Change schedule for product <b> '.$orderProduct->product->name.' </b> has been successfully. '.$customerUser->first_name.' '.$customerUser->last_name.'Has booked your time.';
                                $data_email['user'] = $coachDetail->user_coach;
                                
                                $mail = \Mail::to( $data_email['user']->email )->send(new ScheduleRequestProduct($data_email));
                                /*=== END EMAIL TO COACH ===*/
                            }

                        } else {
                            DB::commit();
                            $orderCus = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/order_admin/orders&id='.$params['order_id']);
                            
                            /* SEND EMAIl TO COACH */
                            $sendEmail = $client->request('POST', env('APP_URL').'/api/v1/coach/mail', [
                                            'form_params' => [
                                                'email' => $dataCoachScheduleOrder[0]['email'],
                                                'name_user' => $orderCus->data->firstname.' '.$orderCus->data->lastname,
                                                'order_id' => $params['order_id'],
                                                'links'    => json_encode($resLink),
                                                'details'  => $dataCoachScheduleOrder[0]['details'],
                                                'bbb_link' => $dataCoachScheduleOrder[0]['url_course']
                                            ]
                                        ]);
                            $emailCustomer = $orderCus->data->email;
                            /*END SEND EMAIL*/
                        }

                        /*UPDATE ENROLL*/
                        $dataSchedules = $dataCoachScheduleOrder[0]['details'];
                        $timestart = strtotime($dataSchedules[0]['start_date'].' '.$dataSchedules[0]['start_time']);
                        $timeend = strtotime($dataSchedules[count($dataSchedules)-1]['end_date'].' '.$dataSchedules[count($dataSchedules)-1]['end_time']);

                        $updateEnroll = $client->request('POST', env('APP_URL').'/api/v1/enrollUser', [
                                        'form_params' => [
                                            'course_id' => $params['course_id'],
                                            'shortname' => 'name',
                                            'enrol_id' => $productSet->enrol_id,
                                            'email' => $dataCoachScheduleOrder[0]['email'],
                                            'timestart' => $timestart,
                                            'timeend' => $timeend,
                                            'email_user' => $emailCustomer,
                                            'reschedule' => true
                                        ]
                                    ]);
                        
                        /* END UPDATE ENROLL */

                    } catch (ClientException $e) {
                        DB::rollback();
                        $return = array(
                                'status'=>'error',
                                'code'=>400,
                                'data'=>array(),
                                'message'=>'Update data order schedule failed. '.$e->getMessage().' in file '.$e->getFile(),
                            );
                        return response()->json($return, $return['code']);
                    }

                    $return = array(
                        'status'=>'success',
                        'code'=>200,
                        'data'=>$data,
                        'message'=>'Update data order schedule has been successfully',
                    );
                } else {
                    $return = array(
                        'status'=>'error',
                        'code'=>400,
                        'data'=>array(),
                        'message'=>'Update data order schedule failed',
                    );
                }

                DB::commit();
            } else {
                $return = array(
                        'status'=>'error',
                        'code'=>400,
                        'data'=>array(),
                        'message'=>'Update data has been failed. Please select your schedule',
                    );
            }

        } catch (\Exception $e) {
            DB::rollback();
            $return = array(
                    'status'=>'error',
                    'code'=>400,
                    'data'=>array(),
                    'message'=>'Update data order schedule failed. '.$e->getMessage().' '.$e->getLine().' in file '.$e->getFile(),
                );
        }

        return response()->json($return, $return['code']);

    }

}

