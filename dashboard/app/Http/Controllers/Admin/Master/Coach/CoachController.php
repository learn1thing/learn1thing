<?php

namespace App\Http\Controllers\Admin\Master\Coach;

use App\Http\Controllers\Admin\Master\Coach\ScheduleController;
use App\Http\Controllers\Api\v1\CoachController as apiCoachCtrl;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Schedule;
use App\Models\Coach;
use App\Models\Course;
use App\Models\CoachSchedule;
use Illuminate\Http\Request;
use App\DataTables\Coach\CoachDataTable;
use App\DataTables\Coach\CoachListDataTable;
use App\Http\Requests\Coach\CoachRequest;
use Auth;
use DB;
use Config;
use Sentinel;
use App\Classes\CurlRequest; 
use GuzzleHttp\Client;

class CoachController extends Controller
{
    protected $model_scedule;
    protected $model_coach_schedule;
    protected $apiCoachCtrl;

    public function __construct(Schedule $model_scedule, CoachSchedule $coachSchedule, apiCoachCtrl $apiCoachCtrl)
    {
        $this->middleware('hasaccess:manage-coach');
        $this->model_scedule = $model_scedule;
        $this->model_coach_schedule = $coachSchedule;
        $this->apiCoachCtrl = $apiCoachCtrl;
    }

    public function index(CoachDataTable $dataTable) 
    {
        return $dataTable->render('coach.index');
    }

    public function create(Request $request)
    {
        $currentUser = user_info();
        $listCoachs = array();
        if ($currentUser->inRole('business-partner')) {
            $coaches = $this->apiCoachCtrl->getListCoach($currentUser->email);
            
            $dataCoaches = json_decode($coaches->getContent());

            if ($dataCoaches->status->success) {
                if (count($dataCoaches->data) > 0) {
                    foreach ($dataCoaches->data as $key => $value) {
                        $listCoachs[$value->id_coach] = $value->name;
                    }
                }
            }
           $listCourses = $this->getListCourse($request);
	   //add by puw on 04-04-2018
	   if(count($listCourses)==1){
		$listCourses=$this->getCourseListBP($currentUser->email);
	    }

        } else {
            $listCoachs[$currentUser->id] = $currentUser->first_name.' '.$currentUser->last_name;


            $listCourses = array();
        }
        
 
        $data['coaches'] = $listCoachs;
        $data['courses'] = $listCourses;

        return view('coach.create', $data);
    }

    public function edit(Request $request, $id)
    {
        $currentUser = user_info();
        $userCoach = Coach::find($id);
        if ($userCoach) {

            $listCourses = $this->getListCourse($request,$userCoach);
            if ($currentUser->inRole('business-partner') && $currentUser->related->id != $userCoach->business_partner_id) {

                sessionFlash('User not found','error');
                return redirect()->back();
            
            } else if (!$currentUser->inRole('business-partner') && $userCoach->user_id != $currentUser->id) {
                sessionFlash('User not found','error');
                return redirect()->back();
            }

            $userCoach->courses = $listCourses;
            $userCoach->email = User::find($userCoach->user_id)->email;
            return view('coach.edit', $userCoach);

        } else {
            sessionFlash('User not found','error');
            return redirect()->back();
        }
    }

    public function update($id, CoachRequest $request)
    {
        DB::beginTransaction();
        try {

            $param = $request->all();
            $model_coach = new Coach();
            $data = $model_coach->updateCoach($id,$param);
            DB::commit();
            if ($data) {
                sessionFlash('Data coach has been successfully updated','success');
                return redirect()->route('coach.schedule.course.index');
            } else {
                sessionFlash('Update data coach has been failed','error');
                return redirect()->back()->withInput();
            }

        } catch (\Exception $e) {
            sessionFlash($e->getMessage(),'error');
            return redirect()->back()->withInput();
        }

    }

    public function store(CoachRequest $request)
    {
        $currentUser = Sentinel::getUser();
        if (!$currentUser->inRole('business-partner')) {
            $userId = $currentUser->id;
            $message_error = 'Save data coach has been failed. Coach already exist';
        } else {
            $userId = $request->coach;
            $message_error = 'Save data coach has been failed. Coach already exist, please select other coach';
        }

        $checkExistCoach = Coach::where(['user_id' => $userId])->first();
        if ($checkExistCoach) {
            sessionFlash($message_error,'error');
            return redirect()->back()->withInput();
        }

        DB::beginTransaction();
        try {
            
            $user = '';
            if (!user_info('parent_id')) {
                $user = user_info()->related->id;
            } else {
                $parent = User::find(user_info('parent_id'));
                $user = $parent->related->id;
            }
            
            $coach = new Coach;
            $coach->name = $request->name;
            $coach->description = $request->description;
            $coach->business_partner_id = $user;
            $coach->user_id = $request->coach;
            $coach->course_id = $request->course_id;

            if($request->hasFile('photo')) {   
                $pathDest = Config::get('services.public_path.image.pattern_path');
                $image = upload_image($request->file('photo'),$pathDest)['original'];
                $coach->photo = $image;
            }
            $coach->save();
        
            DB::commit();
            sessionFlash('Data coach has been sucessfully saved','success');
            return redirect()->route('coach.schedule.show',$coach->id);

        } catch (\Exception $e) {
            DB::rollback();
            sessionFlash($e->getMessage(),'error');
            return redirect()->back()->withInput();
        }
    }

    public function createEvent($id) {
        $user = Coach::find($id);

        $events = [];

        $scheduleCtrl = new ScheduleController;
        $result = [];

        $schedules = CoachSchedule::where('coach_id', $id)->get();
        $id = [];
        $scheduled = [];

        if (!$schedules->isEmpty()) {
            foreach ($schedules as $key => $value) {
                $events[] = $scheduleCtrl->showEvent(  ' ',
                                         $value->schedule_date.' '.$value->start_time,
                                        $value->schedule_date.' '.$value->end_time, $value->id);

                $id[] = $value->schedule_id;
            }
            $scheduled = DB::table('schedules')
                     ->select(DB::raw('*'))
                     ->whereNotIn('id', $id)
                     ->get();
        } else {
            $scheduled = Schedule::all();
        }

        foreach ($scheduled as $key => $value) {
            $events[] = $scheduleCtrl->storeEvent(  ' ',
                                        $value->schedule_date.' '.$value->start_time,
                                        $value->schedule_date.' '.$value->end_time, $value->id);
        }

        $calendar = \Calendar::addEvents($events)->setCallbacks([ 
                'eventClick' => "function(event, jsEvent, view) {
                        var start = event.start.format('Y-M-D H:mm:ss');
                        var end = event.end.format('Y-M-D H:mm:ss');

                        $.ajax({
                                url: '".route('coach.schedule.course.store_event', ['id' => $user->id])."',
                                type: 'post',
                                data: {
                                    'start':start,
                                    'end':end,
                                    'coach_id':".$user->id.",
                                    'schedule_id':event.id
                                },
                                success: function(data){
                                   
                                }
                
                        }); 
                        // change the border color just for fun
                        $(this).css('background-color', '#ff851b');
                }"
            ]);

        $return = [
                    'courses' => $result,
                    'name' => $user->name,
                    'photo' => $user->photo,
                    'description' => $user->description,
                    'calendar' => $calendar,
                    'user_id' => $user->id,
                ];
        return view('coach.create_event', $return);
    }

    public function storeEvent(Request $request) 
    {
        DB::beginTransaction();
        try {

            $data = $this->model_coach_schedule->saveData($request);

            DB::commit();
            $return = array(
                    'status'=>'success',
                    'code'=>200,
                    'data'=>$data,
                    'message'=>'Update schedule has been successfully',
                );

        } catch (\Exception $e) {
            DB::rollback();
            $return = array(
                    'status'=>'success',
                    'code'=>400,
                    'data'=>array(),
                    'message'=>'Update schedule has been failed. '.$e->getMessage(),
                );
        }

        return response()->json($return,$return['code']);
    }

    public function show($id)
    {
        $events = [];
        $detailsSchedule = array();
        $currentDate = date("Y-m-d");
        $firstDateMonth = date('Y-m-01');
        
        $user = Coach::find($id);
        if (!$user) {
            sessionFlash('User not found','error');
            return redirect()->route('coach.schedule.course.index');
        }

        //get schedule coach
        $schedules = CoachSchedule::where('coach_id', $user->id)
                                    ->whereDate('start_date', '>=', $firstDateMonth)
                                    ->get();

        if ($schedules) {
            foreach ($schedules as $sched) {
                if ($sched->start_date >= $currentDate ) {
                    $status = 'available-book';
                } else if ($sched->start_date < $currentDate) {
                    $status = 'available-book-expired';
                }
                $coachSchedule['schedule_id'] = $sched->schedule_id;
                $coachSchedule['status'] = $status;
                $coachSchedule['start'] = $sched->start_date.' '.$sched->start_time;
                $coachSchedule['end'] = $sched->end_date.' '.$sched->end_time;

                $detailsSchedule[] = $coachSchedule;
            }
        }
        //End get Schedule Coach

        //Get Schedule default and collect unique
        $scheduleDefault = Schedule::where('business_partner_id',$user->business_partner_id)
                                    ->whereDate('schedule_date', '>=',$firstDateMonth)
                                    ->orderBy('id','DESC')
                                    ->get();

        if (count($scheduleDefault)) {
            $flag = 0;
            $arrDeafultSchedule = array();

            foreach ($scheduleDefault as $key => $value) {
                $details = json_decode($value->details);
                foreach ($details as $det) {
                    if (count($arrDeafultSchedule) > 0) {
                        $existingData = array_where($arrDeafultSchedule, function ($value, $key) use($det)
                        {
                            $start = strtotime($value->start);
                            $end = strtotime($value->end);

                            $detStart = strtotime($det->start);
                            $detEnd = strtotime($det->end);
                            return $start == $detStart && $end == $detEnd;
                        });
                        if(count($existingData) == 0) {
                            $arrDeafultSchedule[] = $det;
                        }
                    } else {
                        $arrDeafultSchedule[] = $det;
                    }
                }
            }
            //End get schedule and collect unique
            
            if (count($arrDeafultSchedule) > 0) {
                foreach ($arrDeafultSchedule as $det) {
                    foreach ($schedules as $sch) {

                        if (date("Y-m-d H:i:s", strtotime($det->start)) == date("Y-m-d H:i:s", strtotime($sch->start_date.' '.$sch->start_time)) && 
                            date("Y-m-d H:i:s", strtotime($det->end)) == date("Y-m-d H:i:s", strtotime($sch->end_date.' '.$sch->end_time)) && 
                            $det->schedule_id == $sch->schedule_id) {

                            unset($det);
                            $flag = 1;
                            break;
                        } else {
                            $flag = 0;
                        }
                    }
                    
                    if ($flag == 0) {

                        if (date("Y-m-d",strtotime($det->start)) >= $currentDate ) {
                            $status = 'available';
                        } else {
                            $status = 'available-expired';
                        }

                        $defSchedule['schedule_id'] = $det->schedule_id;
                        $defSchedule['status'] = $status;
                        $defSchedule['start'] = $det->start;
                        $defSchedule['end'] = $det->end;
                    
                        $detailsSchedule[] = $defSchedule;
                    }
                }
            }   
        }

        if (!empty($detailsSchedule)) {
            sort($detailsSchedule);
            foreach ($detailsSchedule as $key => $value) {
                $events[] = $this->model_scedule->setEventCoach($value['start'], $value['end'], $value['status'], $value['schedule_id']);
            }
        } else {
            $events = array();
        }

        $calendar = \Calendar::addEvents($events)->setCallbacks([ 
            'eventClick' => "function(event, jsEvent, view) {
                var start = event.start.format('Y-M-D H:mm:ss');
                var end = event.end.format('Y-M-D H:mm:ss');
                var type = '';

                if (event.className[0] === 'sc-available') {
                
                    event.className[0] = 'sc-booking';
                    event.title = 'Available For Booking';
                    event.backgroundColor = '#ff851b';
                    $('.'+event.className[3]).css('background-color', '#ff851b');
                    $('a.'+event.className[3]+' .fc-title').html('Available For Booking');

                } else if (event.className[0] === 'sc-expired') {

                    return false;
                
                } else {
                    
                    event.className[0] = 'sc-available';
                    event.title = 'AVAILABLE';
                    event.backgroundColor = '#3a87ad';
                    $('.'+event.className[3]).css('background-color', '#3a87ad');
                    // $(this).css('background-color', '#3a87ad');
                    $('a.'+event.className[3]+' .fc-title').html('Available');
                
                }
                $.ajax({
                    url: '".route('coach.schedule.course.store_event', ['id' => $user->id])."',
                    type: 'post',
                    data: {
                        'start':start,
                        'end':end,
                        'coach_id':".$user->id.",
                        'schedule_id':event.id
                    },
                    success: function(data) {
                        console.log(data);
                    },
                    error: function(response) {
                        var resdata = response.responseJSON;
                        console.log(resdata);
                    }
        
                }); 
            }",
            'eventMouseover'=> "function(event, jsEvent, view) {
                var start = moment(event.start).format('h:mm:ss a');
                var end = moment(event.end).format('h:mm:ss a')
                var tooltip = '<div class=\'tooltipevent\' style=\'width:100px;height:50px;background:#ffe1b2;position:absolute;z-index:10001;border: 1px solid black;padding-top: 4px;padding-right: 5px;padding-bottom: 5px;padding-left: 5px;\'>' + start + '-' + end +'</div>';
                var _tooltip = $(tooltip).appendTo('body');

                $(this).mouseover(function(e) {
                    $('.tooltipevent').tooltip();
                    $(this).css('z-index', 10000);
                    _tooltip.fadeIn('500');
                    _tooltip.fadeTo('10', 1.9);
                }).mousemove(function(e) {
                    _tooltip.css('top', e.pageY + 10);
                    _tooltip.css('left', e.pageX + 20);
                });
            },

            eventMouseout: function(calEvent, jsEvent) {
                $(this).css('z-index', 8);
                $('.tooltipevent').remove();
            }"
        ]);

        $return = [
                    'name' => $user->name,
                    'photo' => $user->photo,
                    'description' => $user->description,
                    'calendar' => $calendar,
                    'user_id' => $user->id,
                ];
        return view('coach.show', $return);
    }

    public function deleteEvent(Request $request) {
        $schedule = CoachSchedule::where('id', $request->event_id)->where('coach_id', $request->coach_id)->first();
        $schedule->delete();

        return $schedule;
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {

            $user = Coach::find($id)->delete();
            $this->model_scedule->deleteScheduleByCoachId($id);
            DB::commit();

            sessionFlash('Delete data schedule coach has been successfully','success');
            return redirect()->route('coach.schedule.course.index');

        } catch (\Exception $e) {
            DB::rollback();
            sessionFlash($e->getMessage().' file '.$e->getFile().' in line '.$e->getLine(),'error');
            return redirect()->back()->withInput();
        }


    }

    public function coachList(CoachListDataTable $dataTable)
    {
        return $dataTable->render('coach.coach_list');
    }

    public function resetScheduleCoach($id)
    {
        DB::beginTransaction();
        try {

            $schedule = $this->model_scedule->deleteScheduleByCoachId($id);
            DB::commit();

            if ($schedule) {
                sessionFlash('The schedule of the coach has been successfully reset','success');
                return redirect()->back()->withInput();
            } else {
                sessionFlash('The schedule of the coach has been failed reset','error');
                return redirect()->back()->withInput();
            }

        } catch (\Exception $e) {
            DB::rollback();
            sessionFlash($e->getMessage().' file '.$e->getFile().' in line '.$e->getLine(),'error');
            return redirect()->back()->withInput();
        }
    }

    public function getListCourse($request,$coach=false)
    {
        $ses = (session()->has('token_moodle')) ? session()->get('token_moodle') : $request->cookie('token_moodle');
        
        $currentUser = user_info();

        if ($currentUser->inRole('business-partner')) {
            $emailUserCourse = $currentUser->email;

            $dataProfile = array(
                'moodlewsrestformat'    => 'json',
                'field'                 => 'email',
                'values'                => array(0 => $emailUserCourse),
            );
            $profileRequest = CurlRequest::requestMoodle('POST', 'core_user_get_users_by_field', $dataProfile, $ses);

        info('ambil profiler request');    
	info(json_encode($profileRequest));

            $profile = $profileRequest[0];

            $data = array(
                    'userid'   => $profile->id,
                );

	    $token_course='ffdd330075dd1847708a49459ecefc0d';
            $courseRequest = CurlRequest::requestMoodle('POST', 'core_enrol_get_users_courses', $data, $ses);	
	   // dd($courseRequest);
        } else {
            $courseRequest = Course::getCourseByid($coach->course_id);
        }        

        // $courses = json_decode($courseRequest->getBody());
        $course = [];
        $course[0] = 'Choose Course';
        
        if (count($courseRequest) > 0) {
            foreach ($courseRequest as $key => $value) {
                $course[$value->id] = $value->fullname;
            }
        }

        return $course;
    }

    public function getCourseListBP($email){	
	$client = new Client();
	$get_course = $client->request('GET','https://store.learn1thing.com/course/webservice/rest/server.php?wstoken=b401d5f20783f2cec3205a317e6a7bb2&wsfunction=local_get_course_url_by_email_data&moodlewsrestformat=json&url[0][type]=course&url[0][email]='.$email);
       	$courseRequest = json_decode(json_decode($get_course->getBody()))->data;

	$course = [];
        $course[0] = 'Choose Course';
        
        if (count($courseRequest) > 0) {
            foreach ($courseRequest as $key => $value) {
                $course[$value->course_id] = $value->course_name;
            }
        }
	return $course;

    }
}
