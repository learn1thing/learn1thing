<?php

namespace App\Http\Controllers\Admin\Master\Coach;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\Schedule\ExpiredScheduleDataTable;
use App\Models\BusinessPartner;
use App\Models\ExpiredSchedule;
use App\Http\Requests\ExpiredSchedule\ExpiredScheduleRequest;
use Auth;
use DB;
use Config;
use Sentinel;

class ExpiredSchedulesController extends Controller
{
	public function __construct()
    {
        $this->middleware('hasaccess:expired-schedule');
    }

    public function index(ExpiredScheduleDataTable $dataTable) 
    {
        return $dataTable->render('admin.setting.expired_schedules.index');
    }

    public function create()
    {

        $resdata['business_partners'] = array();
        
        if (user_info()->inRole('super-admin')) {
            $results = BusinessPartner::all();
            $dataBp[0] = user_info('first_name').' '.user_info('last_name');
        } else if (user_info()->inRole('business-partner')) {
            $bp_id = user_info()->related->id;
            $results = BusinessPartner::where('id',$bp_id)->get();
        }

        if (count($results) > 0) {
            foreach ($results as $result) {
                $dataBp[$result->id] = $result->id.' - '.$result->name;
                $resdata['business_partners'] = $dataBp; 
            }
            
        }

        return view('admin.setting.expired_schedules.create',$resdata);
    }

    public function store(ExpiredScheduleRequest $request)
    {
        DB::begintransaction();
        try {
            
            $modelExpired = new ExpiredSchedule();
            $modelExpired->business_partner_id = $request->get('business_partner_id');
            $modelExpired->expired = $request->get('expired');
            $modelExpired->save();

            if ($modelExpired) {
                sessionFlash('Expired schedule time is successfully set','success');
            } else {
                sessionFlash('Save data expired schedule time has been failed','error');
            }
            DB::commit();
            return redirect()->route('master.schedule.expired.index');   

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            sessionFlash($e->getMessage(),'error');
            return redirect()->back()->withInput();
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {

            $data = ExpiredSchedule::find($id);
            $data->delete();

            DB::commit();

            if ($data) {
                sessionFlash('Data is successfully deleted','success');
                return redirect()->route('master.schedule.expired.index');
            } else {
                sessionFlash('Delete data has been failed','error');
                return redirect()->route('master.schedule.expired.index');
            }

        } catch (\Exception $e) {
            DB::rollback();
            sessionFlash($e->getMessage(),'error');
            return redirect()->back()->withInput();
               
        }
    }

    public function edit($id)
    {
        $data = ExpiredSchedule::find($id);

        if ($data) {

            $data['business_partners'] = array();
            $data['id'] = $id;
            $data['business_partner_id'] = $data->business_partner_id;
            if (user_info()->inRole('super-admin')) {
                $results = BusinessPartner::all();
                $dataBp[0] = user_info('first_name').' '.user_info('last_name');
            } else if (user_info()->inRole('business-partner')) {
                $bp_id = user_info()->related->id;
                $results = BusinessPartner::where('id',$bp_id)->get();
            }

            if (count($results) > 0) {
                foreach ($results as $result) {
                    $dataBp[$result->id] = $result->id.' - '.$result->name;
                    $data['business_partners'] = $dataBp; 
                }
                
            }

            return view('admin.setting.expired_schedules.edit',$data);
        } else {
            sessionFlash('Data not found','error');
            return redirect()->route('master.schedule.expired.index');
        }
    }

    public function update($id, ExpiredScheduleRequest $request)
    {
        DB::beginTransaction();
        try {
            $dataParam['expired'] = $request->get('expired');
            $dataParam['business_partner_id'] = $request->get('business_partner_id');
            
            $data = ExpiredSchedule::find($id);
            $data->update($dataParam);

            DB::commit();

            if ($data) {
                sessionFlash('Data is successfully edited','success');
                return redirect()->route('master.schedule.expired.index');
            } else {
                sessionFlash('Schedule is not successfully edited','error');
                return redirect()->route('master.schedule.expired.index');
            }

        } catch (\Exception $e) {
            DB::rollback();
            sessionFlash($e->getMessage(),'error');
            return redirect()->back()->withInput();
               
        }
    }
}
