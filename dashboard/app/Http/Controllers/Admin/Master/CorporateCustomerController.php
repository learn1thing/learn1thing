<?php

namespace App\Http\Controllers\Admin\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\Master\CorporateCustomerDataTable;
use App\DataTables\Master\RequestedProductDataTable;
use App\DataTables\Master\RequestProductDataTable;
use App\DataTables\GroupCorporate\GroupCorporateDataTable;
use App\Models\CorporateCustomer;
use App\Models\CorporateGroup;
use App\Http\Requests\Master\User\CorporateCustomerRequest;
use DB;
use App\Models\RoleUser;
use App\Models\Role;
use Excel;
use Response;
use App\Models\User;
use App\Models\OrderDetail;
use App\Models\OrderProduct;
use App\Models\Permission;
use App\Models\Product;
use App\Models\ProductAssign;
use App\Models\ProductSet;
use App\Mail\UserRequestProduct;
use App\Mail\ScheduleRequestProduct;
use Input;
use Session;
use Sentinel;
use Activation;
use App\Http\Controllers\Auth\Api\AuthenticateController;
use Illuminate\Support\Collection;
use Request AS Ajax;
use View;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use App\Classes\CurlRequest;
use App\Models\UserEnrol;

use App\Http\Requests\CorporateCustomer\CorporateGroup as CRequest;
use App\Http\Controllers\Admin\Master\OrderController;

use Illuminate\Support\Facades\Log;

class CorporateCustomerController extends Controller
{
    public function __construct(CorporateCustomer $corporatecustomer, Role $role, Permission $permission)
    {
        $this->corporatecustomer = $corporatecustomer;
        $this->user = new User;
        $this->middleware('hasaccess:manage-group-corporate,mini-store');
        $this->role = $role;
        $this->permission = $permission;
        $this->authRegis = new AuthenticateController;
        $this->coachScheduleController = new \App\Models\CoachScheduleOrder;
        $this->userEnrol = new UserEnrol;
        $this->ctrOrderProduct = new OrderController;
    }

    public function index(CorporateCustomerDataTable $dataTable)
    {
        return $dataTable->render('admin.master.users.corporate_customers.index');
    }

    public function miniStore(Request $request)
    {
        $userId = (user_info('parent_id')) ? user_info('parent_id') : user_info('id');
Log::info('User Id: '.$userId);
        $query = OrderDetail::join('orders','order_details.order_id','=','orders.id')
                            ->join('products','order_details.product_code','=','products.product_code')
                            ->join('order_products','order_details.id','=','order_products.order_detail_id')
                            // ->leftJoin('product_assigns','products.product_code','=','product_assigns.product_id')
                            ->select('products.name','order_products.order_detail_id','products.id','order_details.product_code', 'orders.user_id','products.mini_store_status',DB::raw('COUNT(order_products.id) as stock'),'products.image','products.price')
                            ->where('orders.user_id','=', $userId)
                            // ->orWhere('product_assigns.corporate_id','=', $userId)
                            ->where('order_products.status', 0)
                            ->where('order_details.status', 2)
                            ->groupBy('order_products.order_detail_id','order_details.product_code','products.name','orders.user_id','mini_store_status','products.id','products.image','products.price');

        if(user_info('related_type') == "App\Models\CorporateGroup"){
            $query = $query->where('products.mini_store_status', true); //TRUE or ENABLE
        }
        $query = $query->paginate(16);
        \Log::info($userId);

        $queryProductAssign = ProductAssign::join('products','product_assigns.product_id','=','products.product_code')
                                            ->select('products.name','products.quantity','products.product_code','products.price','products.image','products.mini_store_status')
                                            ->where('product_assigns.corporate_id','=', $userId)
                                            ->paginate(16);

        if (Ajax::ajax()) {
            return Response::json(View::make('admin.master.users.corporate_customers.product_store', array('query' => $query, 'queryProductAssign' => $queryProductAssign))->render());
        }

        return view('admin.master.users.corporate_customers.mini_store')->with(['query' => $query, 'queryProductAssign' => $queryProductAssign ]);
    }

    public function autocompleteProductMiniStore(Request $request)
    {
        $query = OrderDetail::join('orders','order_details.order_id','=','orders.id')
                            ->join('products','order_details.product_code','=','products.product_code')
                            ->select('products.name', 'order_details.product_code', 'orders.user_id',DB::raw('SUM(order_details.quantity) as stock'))
                            ->where('orders.user_id','=',user_info('id'))
                            ->where('products.name','LIKE',"%{$request->input('seacrh')}%")
                            ->groupBy('order_details.product_code','products.name','orders.user_id')
                            ->get();
        
        return view('admin.master.users.corporate_customers.mini_store')->with(['query' => $query]);
    }

    public function enableProduct(Request $request)
    {
        $product = Product::where('product_code', $request->prod_id)->first();
        $product->update(['mini_store_status' => 1]);

        return redirect()->back();

    }

    public function disableProduct(Request $request)
    {
        $product = Product::where('product_code', $request->prod_id)->first();
        $product->update(['mini_store_status' => 0]);

        return redirect()->back();

    }

    public function requestProduct(Request $request)
    {
        
        $productRequest = OrderProduct::where('product_code', '=', $request->prod_id)
                ->where('user_id', $request->user_id)->orderBy('id','DESC')->first();
        if ($productRequest) {
            $next = false;
            if ($productRequest->status != 2) {
                $prodSet = ProductSet::where('product_id', $request->prod_id)->first();
                if ($prodSet) {
                    $userEnrol = $this->userEnrol->getUserEnrol(@$this->user->find($request->user_id)->email, $prodSet->enrol_id);
                    if ($userEnrol) {
                        $dateNow = strtotime(date('Y-m-d H:i:s'));
                        if ($dateNow > $userEnrol->timeend) {
                            $next = true;
                        }
                    }
                }
            }

            if (!$next) {
                $message = 'The product has been requested before';
                sessionFlash($message,'error');
                return redirect()->back();
            }
        }
        

        $product = OrderProduct::where('product_code', '=', $request->prod_id)->where('status', 0)->where('order_detail_id', $request->order_detail_id)->first();
        if($product){
            $product->update(['user_id' => $request->user_id, 'status' => 2]);
        } else {
            $message = 'This product not available !';
            sessionFlash($message,'error');
            return redirect()->back();
        }
        

        $userId = (user_info('parent_id')) ? user_info('parent_id') : user_info('id');
        $data['user_admin'] = Sentinel::findById($userId);
        $data['user'] = Sentinel::findById($request->user_id);
        $data['product'] = Product::where('product_code', $request->prod_id)->first();

        //COmment
       // $mail = \Mail::to( $data['user_admin']->email )->send(new UserRequestProduct($data));

        sessionFlash('Request has been sent.','success');
        return redirect()->back();

    }

    public function approveProduct(Request $request)
    {
        DB::beginTransaction();
        try {
            
            $product = OrderProduct::where('product_code', '=', $request->product_code)
                    // ->where('user_id', $request->user_id)
                    ->where('status', 2)->first();
            if($product){
                $product->update(['status' => 1]);
            } else {
                $message = 'The product has been approved before';
                sessionFlash($message,'error');
		return redirect()->route('admin.corporate-customer.requested-product');
                //return redirect()->back();
            }

            $method = 'POST';
            $url = route('api.check-product-course');
            $client = new Client();
            $response = $client->request($method, $url, [
                'form_params' => [
                                    'product_id'=>$request->product_code,
                                    'code'=>str_random(),
                                    'type_order'=>'corporate'
                                ],
            ]);
            $responses = json_decode($response->getBody());

            if (!empty($responses->data->product_sets[0]->course_profiles->url_schedule)) {
                $customerUser = Sentinel::findById($request->user_id);
                
                $data_email['email'] = $customerUser->email;
                $data_email['user'] = $customerUser;
                $data_email['product'] = Product::where('product_code', $request->product_code)->first();

                //add by puw on 22-02-2018
                $data_email['subject']='Your Request for Coaching Product has been Approved';
                $data_email['content']='<p>Your product request has been approved by your administrator.</p><p>Next, please select your coach and coaching time from the link below called &ldquo;View Schedule&rdquo;</p>';

                //edited by puw on 20-02-2018
                //$data_email['link_schedule'] = $responses->data->product_sets[0]->course_profiles->url_schedule.'&order_product_id='.$product->id;
                
                $data_email['link_schedule'] = $responses->data->product_sets[0]->course_profiles->url_schedule.'&order_product_id='.baseEncrypt($product->id).'&user_id=encuser-'.baseEncrypt($product->user_id);

                $data_email['type_email']='';
                $mail = \Mail::to( $data_email['user']->email )->send(new ScheduleRequestProduct($data_email));
            
            }
            DB::commit();
            sessionFlash('Order request has been approved.','success');

        } catch (\Exception $e) {
            
            DB::rollback();
            $message = 'Order request order has been failed.'.' '.$e->getMessage();
            sessionFlash($message,'error');
        
        }





        // return $responses;
        // $product = OrderProduct::where('product_code', '=', $request->product_code)
        //             ->where('user_id', $request->user_id)
        //             ->where('status', 2)->first();
        // $product->update(['status' => 1]);

        // $userId = (user_info('parent_id')) ? user_info('parent_id') : user_info('id');
        // $data['user_admin'] = Sentinel::findById($userId);
        // $data['user'] = Sentinel::findById($request->user_id);
        // $data['product'] = Product::where('product_code', $request->prod_id)->first();

        // $mail = \Mail::to( $data['user_admin']->email )->send(new UserRequestProduct($data));

        //return redirect()->back();
	return redirect()->route('admin.corporate-customer.requested-product');

    }


    public function approveRequested($product_code,$user_id)
    {

        DB::beginTransaction();
        try {
            
            $product = OrderProduct::where('product_code', '=', $product_code)
                    // ->where('user_id', $request->user_id)
                    ->where('status', 2)->first();
            if($product){
                $product->update(['status' => 1]);
            } else {
                $message = 'The product has been approved before';
                sessionFlash($message,'error');
                return redirect()->back();
            }

            $method = 'POST';
            $url = route('api.check-product-course');
            $client = new Client();
            $response = $client->request($method, $url, [
                'form_params' => [
                                    'product_id'=>$product_code,
                                    'code'=>str_random(),
                                    'type_order'=>'corporate'
                                ],
            ]);
            $responses = json_decode($response->getBody());

            if (!empty($responses->data->product_sets[0]->course_profiles->url_schedule)) {
                $customerUser = Sentinel::findById($user_id);
                
                $data_email['email'] = $customerUser->email;
                $data_email['user'] = $customerUser;
                $data_email['product'] = Product::where('product_code', $product_code)->first();
                $data_email['link_schedule'] = $responses->data->product_sets[0]->course_profiles->url_schedule.'&order_product_id='.$product->id;
                $data_email['type_email']='';
                $mail = \Mail::to( $data_email['user']->email )->send(new ScheduleRequestProduct($data_email));
            
            }
            DB::commit();
            $result = [
                'success' => false,
                'message' => 'Request has been approved',
                'type' => 'success'
            ];

            return response()->json($result,200);

        } catch (\Exception $e) {
            
            DB::rollback();
            $message = 'Request approved has been failed.'.' '.$e->getMessage();
            $result = [
                'success' => false,
                'message' => $message,
                'type' => 'error'
            ];

            return response()->json($result,200);
        
        }

    }

    public function declineRequested($product_code,$user_id)
    {

        DB::beginTransaction();
        try {
            
            $product = OrderProduct::where('product_code', '=', $product_code)
                    // ->where('user_id', $request->user_id)
                    ->where('status', 2)->first();
            if($product){
                $product->update(['status' => 0, 'user_id' => NULL]);
            } else {
                $message = 'The product has been approved before';
                sessionFlash($message,'error');
                return redirect()->back();
            }

            
            DB::commit();
            $result = [
                'success' => false,
                'message' => 'Request has been declined',
                'type' => 'success'
            ];
            return response()->json($result,200);

        } catch (\Exception $e) {
            
            DB::rollback();
            $message = 'Order request order has been failed.'.' '.$e->getMessage();
            $result = [
                'success' => false,
                'message' => $message,
                'type' => 'error'
            ];

            return response()->json($result,200);
        
        }


    }

    public function requestedProduct(RequestedProductDataTable $dataTable)
    {
        return $dataTable->with(['under-corporate' => false])->render('admin.master.users.corporate_customers.requested_product');
    }

    public function requestProductList(RequestProductDataTable $dataTable)
    {
        if (@user_info()->upline->related_type == 'App\Models\CorporateCustomer') {
            return $dataTable->with(['under-corporate' => true])->render('admin.master.users.corporate_customers.requested_product');
        }

        abort(401);

    }

    public function requestProductListDetail($id)
    {
        $orderProduct = OrderProduct::find($id);
        if ($orderProduct) {
            if ($orderProduct->user_id == user_info('id')) {
                $responses = CurlRequest::sendRequest('GET', env('OC_BASE_URL').'/index.php?route=rest/product_admin/products&id='.$orderProduct->product_code);
                if ($responses->success) {
                    
                    $method = 'POST';
                    $url = route('api.check-product-course');
                    $client = new Client();
                    $responseLink = $client->request($method, $url, [
                        'form_params' => [
                                            'product_id'=>$orderProduct->product_code,
                                            'code'=>str_random(),
                                            'type_order'=>'corporate'
                                        ],
                    ]);
                    $responseLinks = json_decode($responseLink->getBody());
                    $data['link_schedule'] = '';
                    if (!empty($responseLinks->data->product_sets[0]->course_profiles->url_schedule)) {
                        $data['link_schedule'] = $responseLinks->data->product_sets[0]->course_profiles->url_schedule.'&order_product_id='.baseEncrypt($orderProduct->id).'&user_id=encuser-'.baseEncrypt($orderProduct->user_id);
                    }

                    $productForCheck[] = $responses->data;
                    $resProducts = $this->ctrOrderProduct->checkOrderHaveSchedule($orderProduct,$productForCheck,'corporate');

                    if (json_decode($orderProduct->link)!=null){
                        $orderProduct->link = json_decode($orderProduct->link);
                    } else {
                          
                        /*$bebas = $orderProduct->link;
                        $object = new arrayObject();
                        $object[0] = array('link' => $bebas );*/
                        $orderProduct->link = array( (object) array('link' => $orderProduct->link ));
                       // (object) array('link' => $bebas );
                    }

                    $data['order_product'] = $orderProduct;
                    $data['product'] = $resProducts[0];//$responses->data;
                    $data['product_set'] = ProductSet::where('product_id',$orderProduct->product_code)->first();
                    $data['schedules'] = $this->coachScheduleController->where('order_product_id',$id)->get();

                    // print_r($orderProduct);die();
                    Log::info('Product Set Id'.$orderProduct->product_code);
                    return view('admin.master.users.corporate_customers.requested_product_detail')->with($data);
                } else {
                    sessionFlash('Product not exist', 'error');
                    return redirect()->back();
                }
            } else {
                abort(401);
            }
        } else {
            sessionFlash('Product not exist', 'error');
            return redirect()->back();
        }
    }

    public function active(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $detail = $this->corporatecustomer->find($id);
            $data = $this->activationUser($detail);
            DB::commit();
            return $data;
        } catch (Exception $e) {
            DB::rollback();
            $data['result'] = 'failed';
            $data['message'] = $e->getMessage();
            return response()->json($data,400);
        }
    }

    public function create()
    {
        $client = new Client();
        $get_countries = $client->request('GET', env('OC_BASE_URL').'/index.php?route=feed/rest_api/countries', [
            'headers' => [
              'X-Oc-Merchant-Id' => 12345,
            ]
          ]);
          $countries = json_decode($get_countries->getBody())->data;
          foreach ($countries as $country) {
            $data['countries'][$country->country_id] = $country->name;
          }
          $data['zones'] = [];
        return view('admin.master.users.corporate_customers.create',$data);
    }

    public function store(CorporateCustomerRequest $request)
    {
        \DB::beginTransaction();
        try
        {
            $insert = $this->corporatecustomer->create([
                'name' => $request->company_name,
                'address' => $request->company_address,
                'no_regis' => $request->company_register_no,
            ]);
            $request->merge(['related_id' => $insert->id,'related_type' => CorporateCustomer::class]);

            $user = \Sentinel::registerAndActivate($request->except('_token'));

            $role = \Sentinel::findRoleBySlug('corporate-customer');

            $role->users()->attach($user);

            // Register to Moodle \.start
            $user['customer_group_id'] = 4;
            $registerMoodle = $this->authRegis->registerMoodle($user,'normaly',false,$request->password);
            // Register to Moodle \.end

            // Register to OC \.start
            $registerOC = $this->authRegis->registerOC($user,'normaly',false,$request->password);
            // Register to OC \.end

            // update customer_id to user
            $user = User::find($user->id);
            $user->update(['customer_id' => $registerOC->data->customer_id]);

            sessionFlash('User successfully created.','success');
            \DB::commit();
            return redirect()->route('admin.corporate-customer.index');
        } catch (\Exception $e) {
            \DB::rollback();
            sessionFlash($e->getMessage(),'error');
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        $corporateCus = $this->corporatecustomer->find($id);
        if ($corporateCus) {
            $data['user'] = $corporateCus->user;
            $data['user']->company_name = $corporateCus->name;
            $data['user']->company_address = $corporateCus->address;
            $data['user']->company_register_no = $corporateCus->no_regis;

            $client = new Client();
            $get_countries = $client->request('GET', env('OC_BASE_URL').'/index.php?route=feed/rest_api/countries', [
                'headers' => [
                  'X-Oc-Merchant-Id' => 12345,
                ]
              ]);
              $countries = json_decode($get_countries->getBody())->data;
              foreach ($countries as $country) {
                $data['countries'][$country->country_id] = $country->name;
              }

              $get_country_detail = $client->request('GET', env('OC_BASE_URL').'/index.php?route=feed/rest_api/countries&id='.$data['user']->country, [
                'headers' => [
                  'X-Oc-Merchant-Id' => 12345,
                  'id' => $data['user']->country,
                ]
              ]);
              $country_detail = json_decode($get_country_detail->getBody())->data;
              $data['zones'] = [];
              if (@$country_detail->zone) {
                  foreach ($country_detail->zone as $zone) {
                    $data['zones'][$zone->zone_id] = $zone->name;
                  }
              }

            return view('admin.master.users.corporate_customers.edit',$data);
        } else {
            sessionFlash('User not found','error');
            return redirect()->back();
        }
    }

    public function update(CorporateCustomerRequest $request)
    {
        \DB::beginTransaction();
        try
        {
            $user = \Sentinel::findById($request->user_id);
            if ($user) {
                $corporateCus = $this->corporatecustomer->find($user->related_id);

                if (!empty($request->password)) {
                    if (strlen($request->password) < 8) {
                        \DB::commit();
                        sessionFlash('Passwords must be at least 8 characters long.');
                        return redirect()->back();
                    }

                    $corporateCus->update([
                        'name' => $request->company_name,
                        'address' => $request->company_address,
                        'no_regis' => $request->company_register_no,
                    ]);
                    \Sentinel::update($user,$request->except(['_token','id','user_id','_method']));
                    $type = 'update-password';
                    $request->merge(['new_password' => $request->password]);

                    // update password log user
                    $this->updatePasswordLogUser($user->email,$request->password);
                } else {
                    \Sentinel::update($user,$request->except(['_token','password','id','user_id','_method']));
                    $type = 'update-profile';
                }

                $updatedUser = User::find($request->user_id);
                $this->updateUserOcMoodle($updatedUser,$request,$type);

                sessionFlash('User successfully updated','success');
                \DB::commit();
                return redirect()->route('admin.corporate-customer.index');
            } else {
                sessionFlash('User not found','error');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            \DB::rollback();
            sessionFlash($e->getMessage(),'error');
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        try
        {
            $corporateCus = $this->corporatecustomer->find($id);
            if ($corporateCus) {
                $user = $corporateCus->user;
                if ($user) {
                    $user->forceDelete();

                    \DB::commit();
                    sessionFlash('User successfully deleted','success');
                    return redirect()->back();
                } else {
                    sessionFlash('User not found','error');
                    return redirect()->back();
                }
            } else {
                sessionFlash('User not found','error');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            \DB::rollback();
            sessionFlash('Something went wrong','error');
            info($e->getFile());
            info($e->getMessage());
            info($e->getLine());
            return redirect()->back();
        }
    }

    //---------------------- DATA MASTER GROUP MANAGE BY CORPORATE --------------------------------------//

    public function groupUser(GroupCorporateDataTable $dataTable)
    {
        return $dataTable->render('admin.master.users.corporate_customers.group.index');
    }

    public function createGroupUser()
    {   
        return view('admin.master.users.corporate_customers.group.create');
    }

    public function storeGroupUser(CRequest $request)
    {   
        DB::beginTransaction();
        try
        {
            $CorporateGroup = new CorporateGroup;
            $CorporateGroup->name = $request['name'];
            $CorporateGroup->description = $request['description'];
            $CorporateGroup->created_by = user_info('id');
            $CorporateGroup->save();

            // ADD NEW ROLE //
            $role = new Role;
            $role->name = $request['name'];
            $role->corporate_group_id = $CorporateGroup->id;
            $role->save();

            $roleGroup = Sentinel::findRoleById($role->id);
            $roleGroup->addPermission('dashboard',true);
            $roleGroup->save();

            sessionFlash('Group successfully added.','success');
            DB::commit();
            return redirect()->route('admin.corporate-customer.group-user');
        } catch (Exception $e) {
            sessionFlash($e->getMessage(),'error');
            DB::rollback();
            return redirect()->back();
        }
    }


    public function editGroup($id)
    {
        $data = CorporateGroup::where('id',$id)->first();
        $model = CorporateGroup::pluck('name', 'id');

        return view('admin.master.users.corporate_customers.group.edit')->with(compact('data','model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateGroup(Request $request, $id)
    {
        $rules = [
            'name' => 'required|unique:corporate_groups,name,'.$id.',id'
        ];

        $this->validate($request,$rules);

        $group = CorporateGroup::where('id',$id)->update([
                'name' => $request->name,
                'description' => $request->description,
            ]);

        Session::flash('message', 'Data has been update'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->route('admin.corporate-customer.group-user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyGroup($id)
    {
        $model = CorporateGroup::where('id',$id);

        if ($model->delete()) {
            Session::flash('message', 'Data has been deleted'); 
            Session::flash('alert-class', 'alert-success'); 
            return redirect()->route('admin.corporate-customer.group-user');
        } else {
            Session::flash('message', 'Data can not be deleted'); 
            Session::flash('alert-class', 'alert-error'); 
            return redirect()->route('admin.corporate-customer.group-user');
        }
    }

    public function manageGroup($id)
    {
        $role = $this->role->find($id);
        $permissions = $this->permission
            ->whereNotIn('slug',['manage-user','manage-permission','setting','manage-group-corporate','bulk-product',
                'bulk-user-creation','manage-group-user','manage-course','manage-schedule','manage-coach','manage-product','product-set'])
            ->paginate(10);
        return view('admin.master.users.corporate_customers.group.manage',compact('role','permissions'));
    }

    public function setPermission(Request $request)
    {
        DB::beginTransaction();
        try
        {
            $role = Role::find($request->input('id_role'));

            $role->addPermission($request->input('permission'));
            $set = $role->save();
            if($set){
                DB::commit();
                return json_encode(['result' => true, 'message' => 'Permission is successfully set', 'type' => 'success']);
            } else {
                return json_encode(['result' => false, 'message' => 'Failed set permission', 'type' => 'error']);
            }
        } catch (Exception $e) {
            return json_encode(['result' => false, 'message' => $e->getMessage(), 'type' => 'error']);
        }
        
    }

    public function unsetPermission(Request $request)
    {
        DB::beginTransaction();
        try
        {
            $role = Role::find($request->input('id_role'));

            $role->removePermission($request->input('permission'));
            $unset = $role->save();
            if($unset){
                DB::commit();
                return json_encode(['result' => true, 'message' => 'Permission is successfully unset', 'type' => 'success']);
            } else {
                return json_encode(['result' => false, 'message' => 'Failed unset permission', 'type' => 'error']);
            }
        } catch (Exception $e) {
            return json_encode(['result' => false, 'message' => $e->getMessage(), 'type' => 'error']);
        }
    }

    public function deactive($id)
    {
        $corporateCus = $this->corporatecustomer->find($id);
        if ($corporateCus) {
            $user = $corporateCus->user;
            if ($user) {
                $deactive = $user->delete();;
                // $deactive = \Activation::remove($user);
                if ($deactive) {
                    // \Activation::create($user);
                    sessionFlash('User successfully deactived','success');
                } else {
                    sessionFlash('User failed deactived','error');
                }

                return redirect()->back();
            } else {
                sessionFlash('User not found','error');
                return redirect()->back();
            }
        } else {
            sessionFlash('User not found','error');
            return redirect()->back();
        }
    }
}
