<?php

namespace App\Http\Controllers\Admin\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\Master\AssignProductPartnerDataTable;
use App\Http\Requests\Master\AssignProductPartnertRequest;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->product = new \App\Models\Product;
        $this->productAssign = new \App\Models\ProductAssign;
        $this->user = new \App\Models\User;
    }

    public function assignProduct(AssignProductPartnerDataTable $dataTable)
    {
        return $dataTable->render('admin.master.products.assign-product-partners.index');
    }

    public function assignProductCreate()
    {
        $data = $this->getDataAssignProduct();
        return view('admin.master.products.assign-product-partners.create')->with($data);
    }

    public function assignProductStore(AssignProductPartnertRequest $request)
    {
        \DB::beginTransaction();
        try
        {
            if (count($request->partner) > 0) {
                foreach ($request->partner as $partner) {
                    $productAssign = new \App\Models\ProductAssign;
                    $productAssign->business_partner_id = $partner;
                    $productAssign->product_id = $request->product;
                    $productAssign->save();
                }
            } else {
                sessionFlash('Partner is required','error');
                return redirect()->back();
            }

            \DB::commit();
            sessionFlash('Product is successfully assigned','success');
            return redirect()->route('admin.assign-product.index');
        } catch (\Exception $e) {
            \DB::rollback();
            info('message: '.$e->getMessage());
            info('file: '.$e->getFile());
            info('line: '.$e->getLine());
            sessionFlash('Something went wrong','error');
            return redirect()->back();
        }
    }

    public function assignProductEdit($product_id)
    {
        $data = $this->getDataAssignProduct();
        $data['product_code'] = $product_id;
        $data['product_name'] = @$this->product->whereProductCode($product_id)->first()->name;
        $data['assigneds'] = $this->productAssign->where('product_id',$product_id)
            ->where('corporate_id',0)
            ->pluck('business_partner_id')->toArray();

        return view('admin.master.products.assign-product-partners.edit')->with(['data' => $data]);
    }

    protected function getDataAssignProduct()
    {
        $data['partners'] = $this->user->getUsersByRole('business-partner')
                ->join('activations','activations.user_id','=','users.id')
                ->where('activations.completed',1)
                ->whereNotNull('users.related_id')
                ->select('users.related_id as id', \DB::raw("concat(users.first_name,' ',users.last_name) as partner"), 'users.email')
                ->get();

        $hasAssigned = $this->product->join('product_assigns','product_assigns.product_id','=','products.product_code')
            ->where('products.status',1)
            ->where('product_assigns.corporate_id',0)
            ->pluck('products.product_code')->toArray();

        $data['products'] = $this->product->join('users','users.id','=','products.user_id')
            ->whereStatus(1)
            ->whereNotIn('product_code', $hasAssigned)
            ->select('product_code','name','model')->get();

        return $data;
    }

    public function assignProductDelete($product_id)
    {
        $assignProducts = $this->productAssign->where('product_id',$product_id)->where('corporate_id',0);
        if ($assignProducts->count() > 0) {
            $assignProducts->delete();
            sessionFlash('Product is successfully removed','success');
            return redirect()->back();
        } else {
            sessionFlash('Product not found','error');
            return redirect()->back();
        }
    }

    public function assignProductUpdate($product_id, Request $request)
    {
        $assignProducts = $this->productAssign->where('product_id',$product_id)->where('corporate_id',0);
        if ($assignProducts->count() > 0) {
            $assignProducts->delete();
            \DB::beginTransaction();
            try
            {
                if (count($request->partner) > 0) {
                    foreach ($request->partner as $partner) {
                        $productAssign = new \App\Models\ProductAssign;
                        $productAssign->business_partner_id = $partner;
                        $productAssign->product_id = $request->id;
                        $productAssign->save();
                    }
                } else {
                    sessionFlash('Partner is required','error');
                    return redirect()->back();
                }

                \DB::commit();
                sessionFlash('Product is successfully updated','success');
                return redirect()->route('admin.assign-product.index');
            } catch (\Exception $e) {
                \DB::rollback();
                info('message: '.$e->getMessage());
                info('file: '.$e->getFile());
                info('line: '.$e->getLine());
                sessionFlash('Something went wrong','error');
                return redirect()->back();
            }
        } else {
            sessionFlash('Product not found','error');
            return redirect()->back();
        }
    }
}
