<?php

namespace App\Http\Controllers\Admin\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\Master\EmailTemplateDataTable;

use App\Models\EmailTemplate;
use DB;

class EmailTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(EmailTemplateDataTable $dataTable)
    {
        return $dataTable->render('admin.master.email_template.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Snippets
       $data['snippets'] = EmailTemplate::snippets();
       
        return view( 'admin.master.email_template.create', $data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $email = EmailTemplate::create( $request->all() );
            $path = resource_path( "views/template_email/"); 
            if (!file_exists($path)) {
                mkdir($path,0777, true);
            }
           
            
            $myfile = fopen($path.$email->slug.".blade.php", "w");
            fwrite($myfile, $email->message);
            fclose($myfile);
            
            DB::commit();
            
            sessionFlash('Master Email successfully added','success');
            return redirect()->route('admin.emailtemplate.index');
        } catch (Exception $e) {
            DB::rollback();
            
            sessionFlash($e->getMessage(),'error');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $email = EmailTemplate::find( $id );
	$snippets = EmailTemplate::snippets();
        return view( 'admin.master.email_template.edit', compact('email','snippets') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
           $email = EmailTemplate::find($id);
           $email->update( $request->all() );
            
            $path = resource_path( "views/template_email/");

            if (!file_exists($path)) {
                mkdir($path,0777, true);
            }
           
            
            $myfile = fopen($path.$email->slug.".blade.php", "w");
            fwrite($myfile, $email->message);
            fclose($myfile);

            DB::commit();
            
            sessionFlash('Master Email successfully updated','success');
            return redirect()->route('admin.emailtemplate.index');
        } catch (Exception $e) {
            DB::rollback();
            
            sessionFlash($e->getMessage(),'error');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = EmailTemplate::find( $id )->delete();
        sessionFlash('Master Email successfully deleted','success');
        return redirect()->route('admin.emailtemplate.index');
    }
}
