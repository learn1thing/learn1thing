<?php

namespace App\Http\Controllers\Admin\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\Master\BusinessPartnerDataTable;
use App\DataTables\GroupBusinessPartner\GroupBusinessPartnerDataTable;
use App\Http\Requests\Master\User\BusinessPartnerRequest;
use App\DataTables\Bulk\SubUserDataTable;
use App\Models\BusinessPartner;
use App\Models\RoleUser;
use App\Models\Role;
use App\Models\BusinessPartnerGroup;
use Excel;
use Response;
use App\Models\User;
use App\Models\Permission;
use GuzzleHttp\Client;
use Input;
use Session;
use Sentinel;
use DB;
use App\Http\Controllers\Auth\Api\AuthenticateController;

use App\Http\Requests\Bulk\PostSingleUser;
use App\Http\Requests\BusinessPartner\BusinessPartnerGroup as BPRequest;

class BusinessPartnerController extends Controller
{
    public function __construct(BusinessPartner $businesspartner, Role $role, Permission $permission)
    {
        $this->middleware('hasaccess:manage-user,manage-group-user');

        $this->businesspartner = $businesspartner;
        $this->user = new User;
        $this->role = $role;
        $this->permission = $permission;
        $this->authRegis = new AuthenticateController;
    }

    //---------------------- DATA MAASTER BUSINESS PARTNER --------------------------------------//
    public function index(BusinessPartnerDataTable $dataTable)
    {
        return $dataTable->render('admin.master.users.business_partners.index');
    }

    public function show($id)
    {
        $data = User::where('id',$id)->first();

        return view('admin.master.users.business_partners.show')->with(compact('data'));
    }

    public function create()
    {
        $client = new Client();
        $get_countries = $client->request('GET', env('OC_BASE_URL').'/index.php?route=feed/rest_api/countries', [
            'headers' => [
              'X-Oc-Merchant-Id' => 12345,
            ]
          ]);
          $countries = json_decode($get_countries->getBody())->data;
          foreach ($countries as $country) {
            $data['countries'][$country->country_id] = $country->name;
          }
          $data['zones'] = [];
        return view('admin.master.users.business_partners.create',$data);
    }

    public function store(BusinessPartnerRequest $request)
    {
        \DB::beginTransaction();
        try
        {
            $insert = $this->businesspartner->create([
                'name' => $request->company_name,
                'address' => $request->company_address,
                'no_regis' => $request->company_register_no,
            ]);
            $request->merge(['related_id' => $insert->id,'related_type' => BusinessPartner::class]);

            $user = \Sentinel::registerAndActivate($request->except('_token'));

            $role = \Sentinel::findRoleBySlug('business-partner');

            $role->users()->attach($user);

            // Register to Moodle \.start
            $user['customer_group_id'] = 2;
            $registerMoodle = $this->authRegis->registerMoodle($user,'normaly',false,$request->password);
            // Register to Moodle \.end

            // Register to OC \.start
            $registerOC = $this->authRegis->registerOC($user,'normaly',false,$request->password);
            // Register to OC \.end

            // update customer_id to user
            $user = User::find($user->id);
            $user->update(['customer_id' => $registerOC->data->customer_id]);

            $businesspartner = new BusinessPartnerGroup;
            $businesspartner->name = 'Coach';
            $businesspartner->description = 'Default coach group';
            $businesspartner->created_by = $user->id;
            $businesspartner->save();

            // ADD NEW ROLE //
            $role = new Role;
            $role->name = $businesspartner->name;
            $role->business_partner_group_id = $businesspartner->id;
            $role->save();

            $roleGroup = Sentinel::findRoleById($role->id);
            // Automatic add permission for coach
            $roleGroup->permissions = [
                'dashboard' => true,
                'manage-course' => true,
                'manage-schedule' => true,
                'manage-coach' => true,
                'manage-profile' => true,
            ];
            // End Automatic add permission for coach
            $roleGroup->save();

            sessionFlash('User successfully created.','success');
            \DB::commit();
            return redirect()->route('admin.business-partner.index');
        } catch (\Exception $e) {
            \DB::rollback();
            sessionFlash($e->getMessage(),'error');
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        $businessPartner = $this->businesspartner->find($id);
        if ($businessPartner) {
            $data['user'] = $businessPartner->user;
            $data['user']->company_name = $businessPartner->name;
            $data['user']->company_address = $businessPartner->address;

            $client = new Client();
            $get_countries = $client->request('GET', env('OC_BASE_URL').'/index.php?route=feed/rest_api/countries', [
                'headers' => [
                  'X-Oc-Merchant-Id' => 12345,
                ]
              ]);
              $countries = json_decode($get_countries->getBody())->data;
              foreach ($countries as $country) {
                $data['countries'][$country->country_id] = $country->name;
              }

              $get_country_detail = $client->request('GET', env('OC_BASE_URL').'/index.php?route=feed/rest_api/countries&id='.$data['user']->country, [
                'headers' => [
                  'X-Oc-Merchant-Id' => 12345,
                  'id' => $data['user']->country,
                ]
              ]);
              $country_detail = json_decode($get_country_detail->getBody())->data;
              $data['zones'] = [];
              if (@$country_detail->zone) {
                  foreach ($country_detail->zone as $zone) {
                    $data['zones'][$zone->zone_id] = $zone->name;
                  }
              }

            return view('admin.master.users.business_partners.edit',$data);
        } else {
            sessionFlash('User not found','error');
            return redirect()->back();
        }
    }

    public function update(BusinessPartnerRequest $request)
    {
        \DB::beginTransaction();
        try
        {
            $user = \Sentinel::findById($request->user_id);
            if ($user) {
                $businessPartner = $this->businesspartner->find($user->related_id);

                if (!empty($request->password)) {
                    if (strlen($request->password) < 8) {
                        \DB::commit();
                        sessionFlash('Passwords must be at least 8 characters long.');
                        return redirect()->back();
                    }
                    $businessPartner->update([
                        'name' => $request->company_name,
                        'address' => $request->company_address,
                        'no_regis' => $request->company_register_no,
                    ]);

                    \Sentinel::update($user,$request->except(['_token','id','user_id','_method']));
                    $type = 'update-password';
                    $request->merge(['new_password' => $request->password]);
                    
                    // update password log user
                    $this->updatePasswordLogUser($user->email,$request->password);
                } else {
                    \Sentinel::update($user,$request->except(['_token','password','id','user_id','_method']));
                    $type = 'update-profile';
                }

                $updatedUser = User::find($request->user_id);
                $this->updateUserOcMoodle($updatedUser,$request,$type);

                sessionFlash('User successfully updated','success');
                \DB::commit();
                return redirect()->route('admin.business-partner.index');
            } else {
                sessionFlash('User not found','error');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            \DB::rollback();
            sessionFlash($e->getMessage(),'error');
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        try
        {
            $businessPartner = $this->businesspartner->find($id);
            if ($businessPartner) {
                $user = $businessPartner->user;
                if ($user) {
                    $user->forceDelete();

                    \DB::commit();
                    sessionFlash('User successfully deleted','success');
                    return redirect()->back();
                } else {
                    sessionFlash('User not found','error');
                    return redirect()->back();
                }
            } else {
                sessionFlash('User not found','error');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            \DB::rollback();
            sessionFlash('Something went wrong','error');
            info($e->getFile());
            info($e->getMessage());
            info($e->getLine());
            return redirect()->back();
        }
    }

    public function active(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $detail = $this->businesspartner->find($id);
            $data = $this->activationUser($detail);
            DB::commit();
            return $data;
        } catch (Exception $e) {
            DB::rollback();
            $data['result'] = 'failed';
            $data['message'] = $e->getMessage();
            return response()->json($data,400);
        }
    }


    //---------------------- DATA SUB USER BUSINESS PARTNER --------------------------------------//


    //---------------------- DATA MASTER GROUP MANAGE BY BUSINESS PARTNER --------------------------------------//

    public function groupUser(GroupBusinessPartnerDataTable $dataTable)
    {
        return $dataTable->render('admin.master.users.business_partners.group.index');
    }

    public function createGroupUser()
    {   
        return view('admin.master.users.business_partners.group.create');
    }

    public function storeGroupUser(BPRequest $request)
    {   
        DB::beginTransaction();
        try
        {
            $businesspartner = new BusinessPartnerGroup;
            $businesspartner->name = $request['name'];
            $businesspartner->description = $request['description'];
            $businesspartner->created_by = user_info('id');
            $businesspartner->save();

            // ADD NEW ROLE //
            $role = new Role;
            $role->name = $request['name'];
            $role->business_partner_group_id = $businesspartner->id;
            $role->save();

            $roleGroup = Sentinel::findRoleById($role->id);
            $roleGroup->addPermission('dashboard',true);
            $roleGroup->save();

            sessionFlash('Group successfully added.','success');
            DB::commit();
            return redirect()->route('admin.business-partner.group-user');
        } catch (Exception $e) {
            sessionFlash($e->getMessage(),'error');
            DB::rollback();
            return redirect()->back();
        }
    }


    public function editGroup($id)
    {
        $data = BusinessPartnerGroup::where('id',$id)->first();
        $model = BusinessPartnerGroup::pluck('name', 'id');
        if (($data && @$data->created_by == user_info('id')) || user_info()->hasAccess('all')) {
            return view('admin.master.users.business_partners.group.edit')->with(compact('data','model'));
        } else {
            abort(401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateGroup(Request $request, $id)
    {
        $rules = [
            'name' => 'required|unique:business_partner_groups,name,'.$id.',id'
        ];

        $this->validate($request,$rules);

        $group = BusinessPartnerGroup::where('id',$id)->update([
                'name' => $request->name,
                'description' => $request->description,
            ]);

        Session::flash('message', 'Data has been update'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->route('admin.business-partner.group-user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyGroup($id)
    {
        $model = BusinessPartnerGroup::where('id',$id)->first();
        // $user = User::find(25);
        // $user->related_type = BusinessPartnerGroup::class;
        // $user->save();
        // echo "<pre>";
        // print_r($model->users); die();
        if (($model && @$model->created_by == user_info('id')) || user_info()->hasAccess('all')) {
            if ($model->users->count() > 0) {
                sessionFlash('This group have related','warning');
                return redirect()->back();
            } else {
                if ($model->delete()) {
                    Session::flash('message', 'Data has been deleted'); 
                    Session::flash('alert-class', 'alert-success'); 
                    return redirect()->route('admin.business-partner.group-user');
                } else {
                    Session::flash('message', 'Data can not be deleted'); 
                    Session::flash('alert-class', 'alert-error'); 
                    return redirect()->route('admin.business-partner.group-user');
                }
            }
        } else {
            abort(401);
        }
    }

    /**
     * manage group.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function manageGroup($id)
    {
        $role = $this->role->find($id);
        $businessPartnerGroup = BusinessPartnerGroup::find($role->business_partner_group_id);
        if ($this->haveNoAccess($businessPartnerGroup)) {
            sessionFlash('Sorry, you dont have access','warning');
            return redirect()->route('admin.business-partner.group-user');
        }

        $permissions = $this->permission
            ->whereNotIn('slug',['manage-user','manage-permission','setting','manage-group-corporate','bulk-order','manage-order'])
            ->paginate(10);
        return view('admin.master.users.business_partners.group.manage',compact('role','permissions'));
    }

    public function setPermission(Request $request)
    {
        DB::beginTransaction();
        try
        {
            $role = Role::find($request->input('id_role'));

            $role->addPermission($request->input('permission'));
            $set = $role->save();
            if($set){
                DB::commit();
                return json_encode(['result' => true, 'message' => 'Permission is successfully set', 'type' => 'success']);
            } else {
                return json_encode(['result' => false, 'message' => 'Failed set permission', 'type' => 'error']);
            }
        } catch (Exception $e) {
            return json_encode(['result' => false, 'message' => $e->getMessage(), 'type' => 'error']);
        }
        
    }

    public function unsetPermission(Request $request)
    {
        DB::beginTransaction();
        try
        {
            $role = Role::find($request->input('id_role'));

            $role->removePermission($request->input('permission'));
            $unset = $role->save();
            if($unset){
                DB::commit();
                return json_encode(['result' => true, 'message' => 'Permission is successfully unset', 'type' => 'success']);
            } else {
                return json_encode(['result' => false, 'message' => 'Failed unset permission', 'type' => 'error']);
            }
        } catch (Exception $e) {
            return json_encode(['result' => false, 'message' => $e->getMessage(), 'type' => 'error']);
        }
    }

    protected function haveNoAccess($data)
    {
        if (!user_info()->inRole('super-admin')) {
            if (user_info('parent_id')) {
                if ($data->created_by != user_info('id')) {
                    return true;
                }
            } else {
                $childs = user_info()->downlines()->pluck('id')->toArray();
                if (!in_array($data->created_by, $childs) && $data->created_by != user_info('id')) {
                    return true;
                }
            }
        }
        return false;
    }

    public function deactive($id)
    {
        $businessPartner = $this->businesspartner->find($id);
        if ($businessPartner) {
            $user = $businessPartner->user;
            if ($user) {
                $deactive = $user->delete();;
                // $deactive = \Activation::remove($user);
                if ($deactive) {
                    // \Activation::create($user);
                    sessionFlash('User successfully deactived','success');
                } else {
                    sessionFlash('User failed deactived','error');
                }

                return redirect()->back();
            } else {
                sessionFlash('User not found','error');
                return redirect()->back();
            }
        } else {
            sessionFlash('User not found','error');
            return redirect()->back();
        }
    }
}
