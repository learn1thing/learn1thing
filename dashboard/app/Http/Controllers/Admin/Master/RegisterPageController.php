<?php

namespace App\Http\Controllers\Admin\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\Master\RegisterPageDataTable;
use App\Http\Requests\Master\RegisterPage;

class RegisterPageController extends Controller
{
    public function __construct()
    {
        $this->middleware('hasaccess:all');
        $this->model = new \App\Models\RegisterPage;
    }

    public function index(RegisterPageDataTable $dataTable)
    {
        return $dataTable->render('admin.master.register-pages.index');
    }

    public function edit($id)
    {
        $register = $this->model->find($id);
        if ($register) {
            if ($this->haveNoAccess($register)) {
                sessionFlash('Sorry, you dont have access','warning');
                return redirect()->route('admin.register-page.index');
            }
            return view('admin.master.register-pages.edit',compact('register'));
        } else {
            sessionFlash('Register Page Not Found','error');
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        $register = $this->model->find($id);
        if ($register) {
            if ($this->haveNoAccess($register)) {
                sessionFlash('Sorry, you dont have access','warning');
                return redirect()->route('admin.register-page.index');
            }
            if( $request->hasFile( 'cover' ) ) {
                $request->merge( ['image' => $this->imageSaveOrRemove($request->only('id', 'cover'), $request->cover)['original']] );
            }
            
            $update = $register->update($request->except(['_token','id', 'cover']));
            if ($update) {
                \DB::commit();
                sessionFlash('Register Page successfully added','success');
                return redirect()->route('admin.register-page.index');
            } else {
                sessionFlash('Something went wrong','error');
                return redirect()->back();
            }
        } else {
            sessionFlash('Department not found','error');
            return redirect()->back();
        }
    }

    protected function haveNoAccess($department)
    {
        if (!user_info()->inRole('super-admin')) {
            if ($department->user_id != user_info('id')) {
                return true;
            }
        }
        return false;
    }

    public function imageSaveOrRemove($data, $image, $oldImage = '')
    {
        $path = 'images/';
        $filename = '';
        if ($data['id'] && file_exists($oldImage)) {
            $filename = delete_file($oldImage);
        }

        $filename = upload_file($image, $path);
    
        return $filename;
    }
}
