<?php

namespace App\Http\Controllers\Admin\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CreditHistory;
use App\Models\User;
use App\Http\Requests\Master\SendCredit;
use App\Http\Requests\Master\ApprovedCredit;
use App\DataTables\Master\CreditHistoryDataTable;
use App\DataTables\Master\CreditHistoryActivityDataTable;

class CreditHistoryController extends Controller
{
    public function __construct()
    {
        $this->creditHistory = new CreditHistory;
        $this->user = new User;
    }

    public function index(CreditHistoryDataTable $dataTable)
    {

        $query = $this->user->getUsersByRole('corporate-customer')
            ->join('payment_methods','payment_methods.id','=','users.payment_method_id')
            ->join('activations','activations.user_id','=','users.id')
            ->where('payment_methods.name','Credit Based System')
            ->where('activations.completed',1);

        if (user_info()->related_type == 'App\Models\CorporateCustomer') {
            $query->where('users.id',user_info()->id);
        }

        $corporates = $query->get('id','first_name','last_name');

        return $dataTable->render('admin.master.topups.index',['corporates' => $corporates]);
    }

    public function create()
    {
        
    }

    public function store(SendCredit $request)
    {
        $userTopUp = $this->user->find($request->corporate);

        if ($userTopUp) {

            if (user_info()->related_type == 'App\Models\CorporateCustomer') {
                //check payment methode corporate
                if (!user_info('payment_method')) {
                    \DB::commit();
                    sessionFlash('Sorry you dont have payment method system','warning');
                    return redirect()->route('admin.credit.index');
                } else {
                    if (strchr(strtolower(@user_info('payment_method')->name), 'credit') == false) {
                        \DB::commit();
                        sessionFlash('Payment method not allowed, Please change your payment method to Credit Based System','warning');
                        return redirect()->route('admin.credit.index');
                    }
                }
                $request->merge(['user_id' => $request->corporate, 'activity' => 'Top Up','credit' => ($userTopUp->credit + $request->mutation), 'total_payment' => $request->total_payment, 'type_created' => 'corporate' ]);
            } else {
                $request->merge(['user_id' => $request->corporate, 'activity' => 'Top Up','credit' => ($userTopUp->credit + $request->mutation), 'type_created' => 'admin', 'status' => 'pending']);
            }
            
            $data = $request->all();
            \DB::beginTransaction();
            try 
            {
                $data['total_payment'] = $data['mutation']*1;
                $insert = $this->creditHistory->create($data);
                if ($insert) {
                    sessionFlash('Credit has been sent','success');
                    \DB::commit();
                    return redirect()->route('admin.credit.index');
                } else {
                    sessionFlash('Something went wrong','error');
                    return redirect()->route('admin.credit.index');
                }
            } catch (\Exception $e) {
                \DB::rollback();
                info($e->getFile());
                info($e->getMessage());
                info($e->getLine());
                sessionFlash('Something went wrong','error');
                return redirect()->route('admin.credit.index');
            }
        } else {
            sessionFlash('User not found','error');
            return redirect()->route('admin.credit.index');
        }
    }

    public function history(CreditHistoryActivityDataTable $dataTable)
    {
        if (!user_info()->inRole('corporate-customer') || (user_info()->inRole('corporate-customer') && @strtolower(user_info('payment_method')->name) == 'pay as you go')) {
            abort(401);
        }
        return $dataTable->render('admin.master.topups.history');
    }

    public function approvedCredit(ApprovedCredit $request)
    {
        \DB::beginTransaction();
        try 
        {
            $dateApproved = null;

            $dataCredit = $this->creditHistory->find($request->id);
            $day = !empty($request->day_expired_credit)? $request->day_expired_credit:0;
            if ($dataCredit) {

                if ($request->type == 'approved') {
                    $dateApproved = date("Y-m-d H:i:s");
                }

                
                $dataCredit->update(['status' => $request->type, 'day_expired_credit' => $day, 'date_approved' => $dateApproved]);

                if ($dataCredit->status == 'approved') {
                    $user = $dataCredit->user;
                    $lastCredit = $user->credit;
                    $user->update(['credit' => $dataCredit->mutation+$lastCredit]);
                }

                $return = array(
                    'status'=>'success',
                    'code'=>200,
                    'data'=>$dataCredit,
                    'message'=> 'Update date top credit has been successfully',
                );

            } else {
                $return = array(
                    'status'=>'error',
                    'code'=>400,
                    'data'=>array(),
                    'message'=> 'Data top up credit not found',
                );
            }
            \DB::commit();
            return response()->json($return, $return['code']);
        } catch (\Exception $e) {
            \DB::rollback();
            $return = array(
                    'status'=>'error',
                    'code'=>400,
                    'data'=>array(),
                    'message'=>$e->getMessage(),
                );
            return response()->json($return, $return['code']);
        }
    }
}
