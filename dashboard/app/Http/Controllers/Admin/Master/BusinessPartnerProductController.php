<?php

namespace App\Http\Controllers\Admin\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use App\Models\ProductSet;
use App\Models\User;
use App\Models\Product;
use App\Models\ProductQuota;
use App\Models\ProductAssign;
use App\DataTables\ProductSet\ProductSetDataTable;
use App\DataTables\Master\ProductDataTable;
use App\DataTables\Master\ProductAssignDataTable;
use App\Classes\CurlRequest;
use App\Http\Requests\Bulk\BulkProductRequest;
use Excel;
use Input;
use Response;
use Illuminate\Support\Collection;

class BusinessPartnerProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('hasaccess:manage-product,bulk-product');
        $this->product = new Product;
        $this->productSet = new ProductSet;
        $this->user = new User;
    }

    public function index(ProductSetDataTable $dataTable)
    {
        $this->middleware('hasaccess:product-set');

    	$client = new Client();

        //GET DATA PRODUCT BUSINESS PARTNER
        if (user_info()->hasAccess('all')) {
            $dataProduct = $this->product->all();
        } else {

            $products = $this->product->where(function($query) {
                $query->where('user_id',user_info('id'))
                    ->orWhere('user_id',user_info('parent_id'));
            });

            $listProductSet = $this->productSet->where(function($query) {
                    $query->where('created_by', user_info('id'))
                        ->orWhere('created_by', user_info('parent_id'));
                })
                ->pluck('product_id')->toArray();

            $dataProduct = $products->whereNotIn('product_code', $listProductSet)
                ->get();
        }
        $dataProductBusinessPartner = array();

        foreach ($dataProduct as $key => $value) {


                $product['id'] = $value->product_code;
                $product['name'] = $value->name;


            array_push($dataProductBusinessPartner, $product);
        }


        //GET DATA COURSE
        try {

            $email = user_info('email');
            if (user_info('parent_id')) {
                $parentEmail = \Sentinel::findById(user_info('parent_id'))->email;
            }
            $url_course=env('BASE_URL_MOODLE').'/webservice/rest/server.php?wstoken=7e2c4e8e51cffa45aa5aad05e9fcf35e&wsfunction=local_get_course_data_by_email_data&moodlewsrestformat=json&url[0][type]=course&url[0][email]='.$email;

            $get_course = $client->request('GET', $url_course);
            $dataCourse = json_decode(json_decode($get_course->getBody()))->data;

            // if(@$dataCourse->data){
            //     $data['course'] = $dataCourse->data;
            // } else {
            //     $data['course'] = array();
            // }

            $data['course'] = $dataCourse;
            $data['detailCourse'] = json_encode($data['course']);

        } catch (ClientException $e) {
            $data['course'] = array();

        }

	    $data['products'] = $dataProductBusinessPartner;
        return $dataTable->render('admin.master.users.business_partners.product.index',$data);
    }

    public function storeProductSet(Request $request)
    {

        $client = new Client();

        $this->middleware('hasaccess:product-set');

        $client = new Client();

        $get_oc_product = $client->request('GET', env('OC_BASE_URL').'/index.php?route=rest/product_admin/products&id='.$request->product, [
            'headers' => [
              'X-Oc-Restadmin-Id' => 12345
            ]
          ]);

        // GET PRODUCT ON TABLE PRODUCTS
        $product = $this->product->where('product_code', $request->product)->first();

        $name_product = json_decode($get_oc_product->getBody())->data->product_description->{1}->name;
        if(isset($request->course)){
            foreach ($request->course as $key => $value) {

		/*
		$setProduct = new ProductSet;
                $setProduct->product_id = $request->product;
                $setProduct->product_name = $name_product;
                */

		if(isset($request->course)){
                    $get_course_detail = $client->request('GET', env('MOODLE_BASE_URL_API_COURSE_DETAIL').$value);
                    $dataCourse = json_decode(json_decode($get_course_detail->getBody()))->data;

			foreach ( $dataCourse as $val ){
			  $setProduct=new ProductSet;
			  $setProduct->product_id=$request->product;
			  $setProduct->product_name=$name_product;
			  $setProduct->course_id = $val->course_id;
			  $setProduct->course_url = $val->course_url.'&productid='.$request->product;
			  $setProduct->course_name = $val->course_name;
			  $setProduct->shortname = $val->shortname;
			  $setProduct->enrol_id = $val->enrol_id;
			  $setProduct->profiler_name = $val->profiler_name;
			  if ($val->profiler_id=="-"){
			    $setProduct->profiler_id=0;
			  }else{
			    $setProduct->profiler_id=$val->profiler_id;
			  }

			  $setProduct->created_by=user_info('id');
			  $setProduct->save();
			}

		    /*
                    $setProduct->course_id = $dataCourse->course_id;
                    $setProduct->course_url = $dataCourse->course_url.'&product_id='.$request->product;
                    $setProduct->course_name = $dataCourse->course_name;
                    $setProduct->shortname = $dataCourse->shortname;
                    $setProduct->enrol_id = $dataCourse->enrol_id;
                    $setProduct->profiler_name = $dataCourse->profiler_name;
                    if($dataCourse->profiler_id == "-" ){
                        $setProduct->profiler_id = 0;
                    } else {
                        $setProduct->profiler_id = $dataCourse->profiler_id;
                    }
                    $setProduct->created_by = user_info('id');
                    $setProduct->save();
		    */


                    // SET TYPE ON PRODUCT TABLE
                    // $product->type = "virtual-class";
                    $product->save();
                } else {

                    // SET TYPE ON PRODUCT TABLE
                    // $product->type = "non-virtual-class";
                    $product->save();
                }
                if($request->profiler){
                    // GET NAME PROFILER
                    $get_profiler = $client->request('GET', 'https://profiler.learn1thing.com/api/v1/surveys/'.$request->profiler, [
                        'headers' => [
                          'id' => $request->profiler
                        ]
                    ]);

                    $name_profiler = json_decode($get_profiler->getBody())->data->title;
                    $setProduct->profiler_id = $request->profiler;
                    $setProduct->profiler_name = $name_profiler;
                    $setProduct->survey_url = json_decode($get_profiler->getBody())->data->url;
                }

            }
        } else {
            $setProduct = new ProductSet;
            $setProduct->product_id = $request->product;
            $setProduct->product_name = $name_product;
            if(isset($request->course)){
                $setProduct->course_id = $value;

                // SET TYPE ON PRODUCT TABLE
                // $product->type = "virtual-class";
                $product->save();
            } else {
                $setProduct->course_id = 0;

                // SET TYPE ON PRODUCT TABLE
                // $product->type = "non-virtual-class";
                $product->save();
            }
            if($request->profiler){
                // GET NAME PROFILER
                $get_profiler = $client->request('GET', 'https://profiler.learn1thing.com/api/v1/surveys/'.$request->profiler, [
                    'headers' => [
                      'id' => $request->profiler
                    ]
                ]);

                $name_profiler = json_decode($get_profiler->getBody())->data->title;
                $setProduct->profiler_id = $request->profiler;
                $setProduct->profiler_name = $name_profiler;
                $setProduct->survey_url = json_decode($get_profiler->getBody())->data->url;
            } else {
                $setProduct->profiler_id = 0;
            }
            $setProduct->created_by = user_info('id');

            $setProduct->save();
        }


        sessionFlash('Product is successfully set','success');
        return redirect()->route('admin.business-partner.product');

    }

    public function destroyProductSet($id)
    {
        $this->middleware('hasaccess:product-set');

        $model = ProductSet::find($id);
        if (($model && @$model->created_by == user_info('id')) || user_info()->hasAccess('all')) {
            if ($model) {
                if ($model->delete()) {
                    sessionFlash('Data has been deleted','success');
                    return redirect()->route('admin.business-partner.product');
                } else {
                    sessionFlash('Data can not be deleted','error');
                    return redirect()->route('admin.business-partner.product');
                }
            } else {
                sessionFlash('Data not found or has been deleted','warning');
                return redirect()->back();
            }
        } else {
            abort(401);
        }
    }

    public function assignProduct(ProductAssignDataTable $dataTable)
    {
        // $this->middleware('hasaccess:product-set');


        //GET DATA PRODUCT BUSINESS PARTNER
        if (user_info()->hasAccess('all')) {
            $dataProduct = $this->product->all();
        } else {

            $products = $this->product->where(function($query) {
                $query->where('user_id',user_info('id'))
                    ->orWhere('user_id',user_info('parent_id'));
            });

            $listProductSet = $this->productSet->where(function($query) {
                    $query->where('created_by', user_info('id'))
                        ->orWhere('created_by', user_info('parent_id'));
                })
                ->pluck('product_id')->toArray();

            $dataProduct = $products->get();
        }
        $dataProductBusinessPartner = array();

        foreach ($dataProduct as $key => $value) {


                $product['id'] = $value->product_code;
                $product['name'] = $value->name;


            array_push($dataProductBusinessPartner, $product);
        }


        //GET LIST CORPORATE
        try {

            $dataCorporate = $this->user->getUsersByRole('corporate-customer')
                ->join('activations','activations.user_id','=','users.id')
                ->where('activations.completed',1)
                ->select('users.id','users.first_name','users.last_name','activations.completed','users.email','users.created_at','users.related_id')
                ->get();

            $data['corporate'] = $dataCorporate;

        } catch (ClientException $e) {
            $data['corporate'] = array();

        }

        $data['products'] = $dataProductBusinessPartner;
        return $dataTable->render('admin.master.users.business_partners.product.product_assign',$data);
    }

    public function storeProductAssign(Request $request)
    {
        \DB::beginTransaction();
        try
        {
            if (count($request->corporateId) > 0) {
                $existMessage = 'Some Corporate have already assigned(';
                $corporates = [];
                $exist = 0;
                foreach ($request->corporateId as $key => $value) {

                    $findExistingData = ProductAssign::where('product_id', $request->product)->where('corporate_id', $value)->first();

                    if($findExistingData){
                        array_push($corporates, @$this->user->find($value)->first_name.' '.@$this->user->find($value)->last_name);
                        $exist++;
                        // sessionFlash('Some Corporate is already have this product','error');
                        // return redirect()->route('admin.business-partner.assign-product');
                        // return redirect()->back();
                    } else {
                        $assignProduct = new ProductAssign;
                        $assignProduct->product_id = $request->product;
                        $assignProduct->business_partner_id = user_info('id');
                        $assignProduct->corporate_id = $value;
                        $assignProduct->save();
                    }


                }
                if ($exist == count($request->corporateId)) {
                    sessionFlash('The corporates have already assigned before for this product','warning');
                } elseif ($exist > 0 && $exist < count($request->corporateId)) {
                    sessionFlash($existMessage.implode(', ', $corporates).')','success');
                } else {
                    sessionFlash('Product is successfully assigned','success');
                }

                \DB::commit();
                return redirect()->route('admin.business-partner.assign-product');
            } else {
                sessionFlash('Please select corporate user who want to be assigned','error');
                return redirect()->route('admin.business-partner.assign-product');
            }
        } catch (\Exception $e) {
            \DB::rollback();
            sessionFlash('Something went wrong','error');
            info('File: '.$e->getFile());
            info('Line: '.$e->getLine());
            info('Message: '.$e->getMessage());
            return redirect()->route('admin.business-partner.assign-product');
        }

    }

    public function destroyProductAssign($id)
    {
        // $this->middleware('hasaccess:product-set');

        $model = ProductAssign::find($id);
        if (($model && @$model->business_partner_id == @user_info()->related->id) || user_info()->hasAccess('all')) {
            if ($model) {
                if ($model->delete()) {
                    sessionFlash('Data has been deleted','success');
                    return redirect()->route('admin.business-partner.assign-product');
                } else {
                    sessionFlash('Data can not be deleted','error');
                    return redirect()->route('admin.business-partner.assign-product');
                }
            } else {
                sessionFlash('Data not found or has been deleted','warning');
                return redirect()->back();
            }
        } else {
            abort(401);
        }
    }

    public function create()
    {
        $customerGroups = $this->getCustomerGroups();
        return view('admin.master.users.business_partners.product.create',compact('customerGroups'));
    }


    public function manufactureAutocomplete(Request $request)
    {
        $client = new Client();
        $response = $client->request('GET', env('OC_BASE_URL').'/index.php?route=rest/manufacturer_admin/manufacturer', [
            'headers' => [
                'X-Oc-Restadmin-Id' => '12345',
            ]
        ]);
        $responses = json_decode($response->getBody());

        $result = $this->getListResponse($responses->data,$request,'manufacturer_id','name');

        return json_encode($result);
    }

    public function categoryAutocomplete(Request $request)
    {
        $client = new Client();
        $response = $client->request('GET', env('OC_BASE_URL').'/index.php?route=rest/category_admin/category', [
            'headers' => [
                'X-Oc-Restadmin-Id' => '12345',
            ]
        ]);
        $responses = json_decode($response->getBody());

        $result = $this->getListResponse($responses->data->categories,$request,'category_id','name');

        return json_encode($result);
    }

    public function productRelatedAutocomplete(Request $request)
    {
        $client = new Client();
        $response = $client->request('GET', env('OC_BASE_URL').'/index.php?route=rest/product_admin/products&limit=5&page=1', [
            'headers' => [
                'X-Oc-Restadmin-Id' => '12345',
            ]
        ]);
        $responses = json_decode($response->getBody());

        $result = $this->getListResponseProduct($responses,$request);

        return json_encode($result);
    }

    public function attributeAutocomplete(Request $request)
    {
        $client = new Client();
        $attributeGroups = json_decode (
            $client->request('GET', env('OC_BASE_URL').'/index.php?route=rest/attribute_group_admin/attributegroup&limit=100&page=1', [
                'headers' => [
                    'X-Oc-Restadmin-Id' => '12345',
                ]
            ])->getBody()
        );

        $attributes = json_decode (
            $client->request('GET', env('OC_BASE_URL').'/index.php?route=rest/attribute_admin/attribute&limit=100&page=1', [
                'headers' => [
                    'X-Oc-Restadmin-Id' => '12345',
                ]
            ])->getBody()
        );

        $result = [];
        $x = 0;
        foreach ($attributes->data as $key => $attribute) {
            if ($request->q == '') {
                $result[$x]['attribute_id'] = $attribute[0]->attribute_id;
                $result[$x]['name'] = $attribute[0]->name;
                foreach ($attributeGroups->data as $keyGroup => $attrGroup) {
                    if ($keyGroup == $attribute[0]->attribute_group_id) {
                        $result[$x]['attribute_group'] = $attrGroup[0]->name;
                    }
                }
            } elseif (strpos(strtolower($attribute[0]->name), strtolower($request->q)) !== false) {
                $result[$x]['attribute_id'] = $attribute[0]->attribute_id;
                $result[$x]['name'] = $attribute[0]->name;
                foreach ($attributeGroups->data as $keyGroup => $attrGroup) {
                    if ($keyGroup == $attribute[0]->attribute_group_id) {
                        $result[$x]['attribute_group'] = $attrGroup[0]->name;
                    }
                }
            }
            $x++;
        }

        return json_encode($result);
    }

    public function productOptionAutocomplete(Request $request)
    {
        $client = new Client();
        $productOptions = json_decode (
            $client->request('GET', env('OC_BASE_URL').'/index.php?route=rest/option_admin/option&limit=100&page=1', [
                'headers' => [
                    'X-Oc-Restadmin-Id' => '12345',
                ]
            ])->getBody()
        );

        $result = [];
        $x = 0;
        foreach ($productOptions->data as $key => $option) {
            if ($request->q == '') {
                $result[$key]['category'] = ( strpos(str_replace('&amp;','',strtolower($option->name)), 'date') !== false ) ? 'Date' : $option->name;
                $result[$key]['name'] = $option->name;
                $result[$key]['option_id'] = $option->option_id;
                $result[$key]['type'] = ( strpos(str_replace(' ', '', strtolower($option->name)), 'date') !== false ) ? 'date' : str_replace(' ', '', strtolower($option->name));
                $result[$key]['option_value'] = [];
                $y = 0;
                foreach ($option->option_values as $optValue) {
                    $result[$key]['option_value'][$y]['option_value_id'] = $optValue->option_value_id;
                    $result[$key]['option_value'][$y]['image'] = $optValue->thumb;
                    foreach ($optValue->option_value_description as $keyOptValue => $value) {
                        if ($keyOptValue == 1) {
                            $result[$key]['option_value'][$y]['name'] = $value->name;
                        }
                    }
                    $y++;
                }
            } elseif (strpos(strtolower($option->name), strtolower($request->q)) !== false) {
                $result[$x]['category'] = $option->name;
                $result[$x]['name'] = $option->name;
                $result[$x]['option_id'] = $option->option_id;
                $result[$x]['type'] = str_replace(' ', '', strtolower($option->name));
                $result[$x]['option_value'] = [];
                $y = 0;
                foreach ($option->option_values as $optValue) {
                    $result[$x]['option_value'][$y]['option_value_id'] = $optValue->option_value_id;
                    $result[$x]['option_value'][$y]['image'] = $optValue->thumb;
                    foreach ($optValue->option_value_description as $keyOptValue => $value) {
                        if ($keyOptValue == 1) {
                            $result[$x]['option_value'][$y]['name'] = $value->name;
                        }
                    }
                    $y++;
                }
                $x++;
            }
        }

        return json_encode($result);
    }

    protected function getListResponseProduct($responses,$request=null)
    {
        $result = [];
        $x = 0;

        if ($responses->data > 0) {
            foreach ($responses->data as $key => $product) {
                if (@$request->q == '' || $request == null) {
                    $result[$key]['model'] = $product->model;
                    $result[$key]['name'] = $product->product_description->{1}->name;
                    $result[$key]['option'] = $product->options;
                    $result[$key]['price'] = $product->price;
                    $result[$key]['product_id'] = $product->id;
                } elseif (strpos(strtolower($product->product_description->{1}->name), strtolower($request->q)) !== false) {
                    $result[$x]['model'] = $product->model;
                    $result[$x]['name'] = $product->product_description->{1}->name;
                    $result[$x]['option'] = $product->options;
                    $result[$x]['price'] = $product->price;
                    $result[$x]['product_id'] = $product->id;
                    $x++;
                }
            }
        }

        return $result;
    }

    protected function getListResponse($responses,$request,$value,$label)
    {
        $result = [];
        if (count($responses) > 0) {
            $x = 0;
            foreach ($responses as $key => $val) {
                if ($request->q == '') {
                    $result[$key][$value] = $val->$value;
                    $result[$key][$label] = $val->$label;
                } elseif (strpos(strtolower($val->$label), strtolower($request->q)) !== false) {
                    $result[$x][$value] = $val->$value;
                    $result[$x][$label] = $val->$label;
                    $x++;
                }
            }
        }

        return $result;
    }

    public function storeProduct(Request $request)
    {
        \DB::beginTransaction();
        try
        {
            $request->merge(['sku' => 'sku-'.$request->model]);
            $body = json_encode($request->except(['image','product_image','image_primary','other_images','product_type']));
            $responses = CurlRequest::sendRequest('POST', env('OC_BASE_URL').'/index.php?route=rest/product_admin/products',null,$body);

            if (@$responses->success ) {
                $insertProduct = [
                    'user_id' => user_info('id'),
                    'product_code' => $responses->product_id,
                    'name' => $request->product_description['1']['name'],
                    'model' => $request->model,
                    'price' => (!is_numeric($request->price)) ? 0 : $request->price,
                    'quantity' => $request->quantity,
                    'status' => $request->status,
                    'type' => $request->product_type,
                    'time_limit' => $request->time_limit,
                    'credit' => (intval($request->credit) == 0) ? 1 : $request->credit,
                    'session' => (!empty($request->session_coach) ? $request->session_coach : 0)
                ];

                if (!empty($request->image_primary)) {
                    // set image as primary
                    $image = $request->image_primary;
                    $multiparts = [
                        [
                            'name'     => 'file',
                            'contents' => fopen($image->getPathname(), 'r'),
                            'filename' => $image->getClientOriginalName(),
                            'Mime-Type'=> $image->getmimeType()
                        ]
                    ];
                    $responseAddProduct = CurlRequest::sendRequest('POST', env('OC_BASE_URL').'/index.php?route=rest/product_admin/productimages&id='.$responses->product_id,$multiparts);

                    // insert product db middleware
                    $insertProduct['image'] = upload_file($request->image_primary,'uploads/products/')['original'];
                }

                // insert product .start here
                $this->product->create($insertProduct);
                // insert product .end here

                // insert product quota
                if (count($request->product_category) > 0) {
                    foreach ($request->product_category as $categoryId) {
                        $categoryDetail = CurlRequest::sendRequest('GET', env('OC_BASE_URL').'/index.php?route=rest/category_admin/category&id='.$categoryId);
                        if (@$categoryDetail->success) {
                            $flag = false;
                            foreach ($categoryDetail->data->categories as $category) {
                                if (!$flag && (strpos(strtolower($category->name), 'virtual') !== false)) {
                                    $flag = true;
                                    $storeProductQuota = ProductQuota::create(['user_id' => user_info('id'), 'product_code' => $responses->product_id, 'min_quota' => intval($request->min_quota)]);
                                }
                            }
                        }
                    }
                }

                if (count($request->other_images) > 0) {
                    foreach ($request->other_images as $key => $value) {
                        $multiparts = [
                            [
                                'name'     => 'file',
                                'contents' => fopen($value->getPathname(), 'r'),
                                'filename' => $value->getClientOriginalName(),
                                'Mime-Type'=> $value->getmimeType()
                            ]
                        ];
                        $response = CurlRequest::sendRequest('POST', env('OC_BASE_URL').'/index.php?route=rest/product_admin/productimages&id='.$responses->product_id.'&other=1',$multiparts);
                    }
                }

                \DB::commit();
                sessionFlash('Product successfully created','success');
                return redirect()->route('admin.business-partner.product.list');
            } else {
                sessionFlash('Something went wrong','error');
                return redirect()->back();
            }

        } catch (\Exception $e) {
            \DB::rollback();
            sessionFlash($e->getMessage(),'error');
            return redirect()->back();
        }
    }

    public function productList(ProductDataTable $dataTable)
    {
        return $dataTable->render('admin.master.users.business_partners.product.product_list');
    }

    public function editProduct(Request $request, $id)
    {
        $detailProduct = $this->product->findProduct($id);

        // check permission
        if ($this->hasAccessPermission($detailProduct->user)) {
            sessionFlash('Sorry, you dont have access','warning');
            return redirect()->route('admin.business-partner.product.list');
        }

        $responses = CurlRequest::sendRequest('GET', env('OC_BASE_URL').'/index.php?route=rest/product_admin/products&id='.$id);
        if ($responses->success) {
            $responses->data->related_products = [];
            if (count($responses->data->product_relateds) > 0) {
                foreach ($responses->data->product_relateds as $key => $related) {
                    $request = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/product_admin/products&id='.$related);
                    if ($request->success) {
                        $responses->data->related_products[$key]['id'] = $request->data->id;
                        $responses->data->related_products[$key]['name'] = $request->data->product_description->{1}->name;
                    }
                }
            }
            $responses->data->detailProduct = $detailProduct;
            $customerGroups = $this->getCustomerGroups();

            return view('admin.master.users.business_partners.product.edit')->with(['product'=>$responses->data, 'customerGroups' => $customerGroups]);
        } else {
            sessionFlash('Product not found or has been deleted','warning');
            return redirect()->back();
        }
    }

    public function updateProduct(Request $request)
    {
        $detailProduct = $this->product->findProduct($request->product_id);

        // check permission
        if ($this->hasAccessPermission($detailProduct->user)) {
            sessionFlash('Sorry, you dont have access','warning');
            return redirect()->route('admin.business-partner.product.list');
        }

        $request->merge(['sku' => 'sku-'.$request->model]);
        $body = json_encode($request->except(['image','product_image','image_primary','other_images','product_type']));
        $responses = CurlRequest::sendRequest('PUT',env('OC_BASE_URL').'/index.php?route=rest/product_admin/products&id='.$request->product_id,null,$body);

        if (@$responses->success ) {
            $updateProduct = [
                'name' => $request->product_description['1']['name'],
                'model' => $request->model,
                'price' => (!is_numeric($request->price)) ? 0 : $request->price,
                'quantity' => $request->quantity,
                'status' => $request->status,
                'type' => $request->product_type,
                'time_limit' => $request->time_limit,
                'credit' => (intval($request->credit) == 0) ? 1 : $request->credit,
                'session' => (!empty($request->session_coach) ? $request->session_coach : 0)
            ];

            // set image as primary
            if (!empty($request->image_primary)) {
                $image = $request->image_primary;
                $multiparts = [
                    [
                        'name'     => 'file',
                        'contents' => fopen($image->getPathname(), 'r'),
                        'filename' => $image->getClientOriginalName(),
                        'Mime-Type'=> $image->getmimeType()
                    ]
                ];

                $responseAddProduct = CurlRequest::sendRequest('POST', env('OC_BASE_URL').'/index.php?route=rest/product_admin/productimages&id='.$request->product_id,$multiparts);

                if ($detailProduct->image) {
                    delete_file($detailProduct->image);
                    $updateProduct['image'] = upload_file($request->image_primary,'uploads/products/')['original'];
                } else {
                    $updateProduct['image'] = upload_file($request->image_primary,'uploads/products/')['original'];
                }
            }

            // update product
            $detailProduct->update($updateProduct);

            // update product quota
            if (count($request->product_category) > 0) {
                foreach ($request->product_category as $categoryId) {
                    $categoryDetail = CurlRequest::sendRequest('GET', env('OC_BASE_URL').'/index.php?route=rest/category_admin/category&id='.$categoryId);
                    if (@$categoryDetail->success) {
                        $flag = false;
                        foreach ($categoryDetail->data->categories as $category) {
                            if (!$flag && (strpos(strtolower($category->name), 'virtual') !== false)) {
                                $flag = true;
                                $findProductQuota = ProductQuota::where('product_code',$detailProduct->product_code)->first();
                                if ($findProductQuota) {
                                    $storeProductQuota = ProductQuota::where('user_id',user_info('id'))->where('product_code',$detailProduct->product_code)
                                        ->update(['min_quota' => intval($request->min_quota)]);
                                } else {
                                    $storeProductQuota = ProductQuota::create(['user_id' => user_info('id'), 'product_code' => $detailProduct->product_code, 'min_quota' => intval($request->min_quota)]);
                                }
                            }
                        }
                    }
                }
            }

            // save other image
            if (count($request->other_images) > 0) {
                foreach ($request->other_images as $key => $value) {
                    $multiparts = [
                        [
                            'name'     => 'file',
                            'contents' => fopen($value->getPathname(), 'r'),
                            'filename' => $value->getClientOriginalName(),
                            'Mime-Type'=> $value->getmimeType()
                        ]
                    ];

                    $responseAddProduct = CurlRequest::sendRequest('POST', env('OC_BASE_URL').'/index.php?route=rest/product_admin/productimages&id='.$request->product_id.'&other=1',$multiparts);
                }
            }

            \DB::commit();
            sessionFlash('Updated product successfully','success');
            return redirect()->route('admin.business-partner.product.list');
        } else {
            sessionFlash('Something went wrong','error');
            return redirect()->back();
        }
        return redirect()->route('admin.business-partner.product.list');
    }

    public function destroyProduct(Request $request, $id)
    {
        $detail = $this->product->findProduct($id);

        // check permission
        if ($this->hasAccessPermission($detail->user)) {
            sessionFlash('Sorry, you dont have access','warning');
            return redirect()->route('admin.business-partner.product.list');
        }

        if ($detail->user_id != user_info('id') && user_info()->hasAccess('manage-product')) {
            if ($detail->user->parent_id == user_info('id')) {
                $delete = $this->responseDeleteProduct($detail);
                return redirect()->back();
            }

            if ($request->ajax()) {
                return response()->json(['success' => false, 'message' => 'Sorry, you dont have access', 'code' => 401],401);
            } else {
                sessionFlash('Sorry, you dont have access','warning');
                return redirect()->back();
            }
        }

        $delete = $this->responseDeleteProduct($detail);
        return redirect()->back();
    }

    protected function responseDeleteProduct($value)
    {
        if (count($value->productSets)) {
            sessionFlash('Product has relations','warning');
        } else {
            $value->delete();
            sessionFlash('Product successfully deleted','success');
        }
        return true;
    }

    public function bulkProduct()
    {
        $this->middleware('hasaccess:bulk-product');
        return view('admin.master.users.business_partners.product.bulk_product');
    }

    public function bulkProductStore(BulkProductRequest $request)
    {
        $this->middleware('hasaccess:bulk-product');
        \DB::beginTransaction();
        try {
            if($request->hasFile('bulk_product')){
                $path = $request->file('bulk_product')->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();

                if(!empty($data) && $data->count() > 0){
                    $success = 0;
                    $failed = 0;
                    foreach ($data as $key => $product) {
                        if (!empty($product->model) && !empty($product->name) && !empty($product->meta_title)) {
                            // if (strtolower(substr($product->length, strlen($product->length)-2,2)) == 'cm') {
                            //     $length_class_id = 1;
                            // } elseif (strtolower(substr($product->length, strlen($product->length)-2,2)) == 'mm') {
                            //     $length_class_id = 2;
                            // } else {
                            //     $length_class_id = 3;
                            // }

                            // if (strtolower(substr($product->weight, strlen($product->weight)-2,2)) == 'kg') {
                            //     $weight_class_id = 1;
                            // } elseif (strtolower(substr($product->weight, strlen($product->weight)-2,2)) == 'gr') {
                            //     $weight_class_id = 2;
                            // } else {
                            //     $weight_class_id = 3;
                            // }

                            $body = [
                                'model' => $product->model,
                                'sku' => $product->model,
                                // 'upc' => "",
                                // 'ean' => "",
                                // 'jan' => "",
                                // 'isbn' => "",
                                // 'mpn' => "",
                                'product_category' => (@$product->category) ? explode(',', $product->category) : [],
                                'location' => (@$product->location) ? $product->location : '',
                                'quantity' => $product->quantity,
                                'minimum' => (@$product->minimum) ? $product->minimum : 1,
                                'subtract' => (@$product->subtract) ? $product->subtract : 1,
                                'stock_status_id' => (@$product->stock_status_id) ? $product->stock_status_id : 6,
                                'shipping' => $product->shipping,
                                'status' => (@$product->status) ? $product->status : 0,
                                'manufacturer_id' => $product->manufacturer,
                                'price' => $product->price,
                                'tax_class' => $product->tax_class,
                                // 'weight' => substr($product->weight, 0,strlen($product->weight)-2),
                                // 'weight_class_id' => $weight_class_id,
                                // 'length' => substr($product->length, 0,strlen($product->length)-2),
                                // 'length_class_id' => $length_class_id,
                                // 'width' => substr($product->width, 0,strlen($product->width)-2),
                                // 'height' => substr($product->height, 0,strlen($product->height)-2),
                                // 'product_store' => [
                                //     0
                                // ],
                                'product_description' => [
                                    [
                                        'language_id' => 1,
                                        'name' => $product->name,
                                        'meta_title' => (@$product->meta_title) ? $product->meta_title : '',
                                        'meta_description' => (@$product->meta_description) ? $product->meta_description : '',
                                        'meta_keyword' => (@$product->meta_keyword) ? $product->meta_keyword : '',
                                        'description' => (@$product->description) ? $product->description : '',
                                        'tag' => (@$product->product_tags) ? $product->product_tags : ''
                                    ],
                                ],
                                'points' => (@$product->points) ? $product->points : 0,
                                'sort_order' => $product->sort_order,
                                'keyword' => (@$product->seo_keyword) ? $product->seo_keyword : '',
                                'date_available' => date('Y-m-d',strtotime($product->date_available)),
                                'product_related' => (@$product->related_product) ? explode(',', $product->related_product) : []
                            ];

                            $request = CurlRequest::sendRequest('POST',env('OC_BASE_URL').'/index.php?route=rest/product_admin/products',null,json_encode($body));
                            if ($request->success) {
                                $insertProduct = [
                                    'user_id' => user_info('id'),
                                    'product_code' => $request->product_id,
                                    'name' => $product->name,
                                    'model' => $product->model,
                                    'price' => (!is_numeric($product->price)) ? 0 : $product->price,
                                    'quantity' => $product->quantity,
                                    'status' => (@$product->status) ? $product->shipping : 0,
                                    'type' => (@$product->product_type) ? $product->product_type : 'online'
                                ];
                                $this->product->create($insertProduct);
                                $success++;
                            } else {
                                $failed++;
                            }
                        } else {
                            $failed++;
                        }
                    }
                    \DB::commit();
                    unlink($path);
                    sessionFlash('Bulk product success: '.$success.' failed: '.$failed,'success');
                    return redirect()->route('admin.business-partner.product.list');
                } else {
                    sessionFlash('Something went wrong','warning');
                    return redirect()->back();
                }

            }
        } catch (\Exception $e) {
            \DB::rollback();
            info($e->getMessage());
            info($e->getFile());
            info($e->getLine());
            sessionFlash($e->getMessage(),'error');
            return redirect()->back();
        }
    }

    public function bulkProductSampleDownload()
    {
        $this->middleware('hasaccess:bulk-product');
        $file= public_path(). "/file/file_example.csv";

        $headers = array(
                  'Content-Type: application/csv',
                );

        return Response::download($file, 'file_example.csv', $headers);
    }

    protected function getCustomerGroups()
    {
        $customerGroups = new Collection;
        $responses = CurlRequest::sendRequest('GET', env('OC_BASE_URL').'/index.php?route=rest/customer_group_admin/customergroup&limit=100&page=1');
        if (@$responses->success ) {
            $results = $responses->data;
            foreach ($results as $key => $value) {
                $customerGroups->push([
                        'customer_group_id' => $value[0]->customer_group_id,
                        'name' => $value[0]->name,
                        'sort_order' => $value[0]->sort_order,
                        'description' => $value[0]->description,
                        'language_id' => $value[0]->language_id,
                    ]);
            }
        }
        return $customerGroups;
    }

    public function resetAlertProduct(Request $request, $id)
    {
        $detailProduct = $this->product->findProduct($id);

        if ($detailProduct) {
            // check permission
            if ($this->hasAccessPermission($detailProduct->user)) {
                sessionFlash('Sorry, you dont have access','warning');
                return redirect()->route('admin.business-partner.product.list');
            }

            $productQuota = $detailProduct->productQuota;
            if ($productQuota) {
                $resetAlert = $productQuota->update(['sold_quota_num' => 0, 'is_alert' => false]);
                if ($resetAlert) {
                    sessionFlash('Product alert has been reset','success');
                    return redirect()->back();
                } else {
                    sessionFlash('Something went wrong, please try again later','error');
                    return redirect()->back();
                }
            } else {
                sessionFlash('Product category not virtual class','warning');
                return redirect()->back();
            }
        } else {
            sessionFlash('Product not found or has been deleted','warning');
            return redirect()->back();
        }
    }
}
