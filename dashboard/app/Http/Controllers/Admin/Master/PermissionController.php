<?php

namespace App\Http\Controllers\Admin\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\Master\PermissionDataTable;
use App\Http\Requests\Master\PermissionRequest;
use App\Models\Permission;
use App\Models\Role;
use DB;
use Exception;

class PermissionController extends Controller
{
    public function __construct(Permission $permission, Role $role)
    {
        $this->permission = $permission;
        $this->role = $role;
        $this->middleware('hasaccess:manage-permission');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PermissionDataTable $dataTable)
    {
        return $dataTable->render('admin.master.permissions.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.master.permissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermissionRequest $request)
    {
        DB::beginTransaction();
        try {
            $insert = $this->permission->create($request->except('_token'));
            if ($insert) {
                session()->put('notification',[
                    'message' => 'Success save data',
                    'alert-type' => 'success',
                ]);
            } else {
                session()->put('notification',[
                    'message' => 'Failed save data',
                    'alert-type' => 'error',
                ]);
            }
            DB::commit();
            return redirect()->route('admin.permission.index');
        } catch (Exception $e) {
            session()->put('notification',[
                'message' => $e->getMessage(),
                'alert-type' => 'error',
            ]);
            DB::rollback();
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function manage($slug)
    {
        $role = $this->role->findBySlug($slug);
        $permissions = $this->permission->paginate(10);
        return view('admin.master.permissions.manage',compact('role','permissions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function setPermission(Request $request)
    {
        DB::beginTransaction();
        try
        {
            $role = Role::find($request->input('id_role'));

            $role->addPermission($request->input('permission'));
            $set = $role->save();
            if($set){
                DB::commit();
                return json_encode(['result' => true, 'message' => 'Permission is successfully set', 'type' => 'success']);
            } else {
                return json_encode(['result' => false, 'message' => 'Failed set permission', 'type' => 'error']);
            }
        } catch (Exception $e) {
            return json_encode(['result' => false, 'message' => $e->getMessage(), 'type' => 'error']);
        }
        
    }

    public function unsetPermission(Request $request)
    {
        DB::beginTransaction();
        try
        {
            $role = Role::find($request->input('id_role'));

            $role->removePermission($request->input('permission'));
            $unset = $role->save();
            if($unset){
                DB::commit();
                return json_encode(['result' => true, 'message' => 'Permission is successfully unset', 'type' => 'success']);
            } else {
                return json_encode(['result' => false, 'message' => 'Failed unset permission', 'type' => 'error']);
            }
        } catch (Exception $e) {
            return json_encode(['result' => false, 'message' => $e->getMessage(), 'type' => 'error']);
        }
    }
}
