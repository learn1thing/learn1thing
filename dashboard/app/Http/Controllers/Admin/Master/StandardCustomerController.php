<?php

namespace App\Http\Controllers\Admin\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\Master\StandardCustomerDataTable;
use App\Models\StandardCustomer;
use App\Models\User;
use App\Http\Requests\Master\User\StandardCustomerRequest;
use App\Classes\CurlRequest;
use DB;
use GuzzleHttp\Client;
use App\Http\Controllers\Auth\Api\AuthenticateController;

class StandardCustomerController extends Controller
{
    public function __construct(StandardCustomer $standardcustomer)
    {
        $this->standardcustomer = $standardcustomer;
        $this->user = new User;
        $this->middleware('hasaccess:manage-user');
        $this->authRegis = new AuthenticateController;
    }

    public function index(StandardCustomerDataTable $dataTable)
    {
        return $dataTable->render('admin.master.users.standard_customers.index');
    }

    public function active(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $detail = $this->standardcustomer->find($id);
            $data = $this->activationUser($detail);
            DB::commit();
            return $data;
        } catch (Exception $e) {
            DB::rollback();
            $data['result'] = 'failed';
            $data['message'] = $e->getMessage();
            return response()->json($data,400);
        }
    }

    public function create()
    {
        $client = new Client();
        $get_countries = $client->request('GET', env('OC_BASE_URL').'/index.php?route=feed/rest_api/countries', [
            'headers' => [
              'X-Oc-Merchant-Id' => 12345,
            ]
          ]);
          $countries = json_decode($get_countries->getBody())->data;
          foreach ($countries as $country) {
            $data['countries'][$country->country_id] = $country->name;
          }
          $data['zones'] = [];
        return view('admin.master.users.standard_customers.create',$data);
    }

    public function store(StandardCustomerRequest $request)
    {
        \DB::beginTransaction();
        try
        {
            $insert = $this->standardcustomer->create([
                'name' => $request->first_name.' '.$request->last_name,
                'address' => $request->address_1,
            ]);
            $request->merge(['related_id' => $insert->id,'related_type' => StandardCustomer::class]);

            $user = \Sentinel::registerAndActivate($request->except('_token'));

            $role = \Sentinel::findRoleBySlug('standard-customer');

            $role->users()->attach($user);

            // Register to Moodle \.start
            $user['customer_group_id'] = 1;
            $registerMoodle = $this->authRegis->registerMoodle($user,'normaly',false,$request->password);
            // Register to Moodle \.end

            // Register to OC \.start
            $registerOC = $this->authRegis->registerOC($user,'normaly',false,$request->password);
            // Register to OC \.end

            // update customer_id to user
            $user = User::find($user->id);
            $user->update(['customer_id' => $registerOC->data->customer_id]);

            sessionFlash('User successfully created.','success');
            \DB::commit();
            return redirect()->route('admin.standard-customer.index');
        } catch (\Exception $e) {
            \DB::rollback();
            sessionFlash($e->getMessage(),'error');
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        $standardCus = $this->standardcustomer->find($id);
        if ($standardCus) {
            $data['user'] = $standardCus->user;
            $client = new Client();
            $get_countries = $client->request('GET', env('OC_BASE_URL').'/index.php?route=feed/rest_api/countries', [
                'headers' => [
                  'X-Oc-Merchant-Id' => 12345,
                ]
              ]);
              $countries = json_decode($get_countries->getBody())->data;
              foreach ($countries as $country) {
                $data['countries'][$country->country_id] = $country->name;
              }

              $get_country_detail = $client->request('GET', env('OC_BASE_URL').'/index.php?route=feed/rest_api/countries&id='.$data['user']->country, [
                'headers' => [
                  'X-Oc-Merchant-Id' => 12345,
                  'id' => $data['user']->country,
                ]
              ]);
              $country_detail = json_decode($get_country_detail->getBody())->data;
              $data['zones'] = [];
              if (@$country_detail->zone) {
                  foreach ($country_detail->zone as $zone) {
                    $data['zones'][$zone->zone_id] = $zone->name;
                  }
              }

            return view('admin.master.users.standard_customers.edit',$data);
        } else {
            sessionFlash('User not found','error');
            return redirect()->back();
        }
    }

    public function update(StandardCustomerRequest $request)
    {
        \DB::beginTransaction();
        try
        {
            $user = \Sentinel::findById($request->user_id);
            if ($user) {
                $standardCus = $this->standardcustomer->find($user->related_id);

                if (!empty($request->password)) {
                    if (strlen($request->password) < 8) {
                        \DB::commit();
                        sessionFlash('Passwords must be at least 8 characters long.');
                        return redirect()->back();
                    }

                    $standardCus->update([
                        'name' => $request->first_name.' '.$request->last_name,
                        'address' => $request->address_1,
                    ]);

                    \Sentinel::update($user,$request->except(['_token','id','user_id','_method']));
                    $type = 'update-password';
                    $request->merge(['new_password' => $request->password]);

                    // update password log user
                    $this->updatePasswordLogUser($user->email,$request->password);
                } else {
                    \Sentinel::update($user,$request->except(['_token','password','id','user_id','_method']));
                    $type = 'update-profile';
                }

                $updatedUser = User::find($request->user_id);
                $this->updateUserOcMoodle($updatedUser,$request,$type);

                sessionFlash('User successfully updated','success');
                \DB::commit();
                return redirect()->route('admin.standard-customer.index');
            } else {
                sessionFlash('User not found','error');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            \DB::rollback();
            sessionFlash($e->getMessage(),'error');
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        try
        {
            $standardCus = $this->standardcustomer->find($id);
            if ($standardCus) {
                $user = $standardCus->user;
                if ($user) {
                    // $standardCus->delete();
                    $user->forceDelete();

                    \DB::commit();
                    sessionFlash('User successfully deleted','success');
                    return redirect()->back();
                } else {
                    sessionFlash('User not found','error');
                    return redirect()->back();
                }
            } else {
                sessionFlash('User not found','error');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            \DB::rollback();
            sessionFlash('Something went wrong','error');
            info($e->getFile());
            info($e->getMessage());
            info($e->getLine());
            return redirect()->back();
        }
    }

    public function deactive($id)
    {
        $standardCus = $this->standardcustomer->find($id);
        if ($standardCus) {
            $user = $standardCus->user;
            if ($user) {
                // $deactive = \Activation::remove($user);
                $deactive = $user->delete();;
                if ($deactive) {
                    // \Activation::create($user);
                    sessionFlash('User successfully deactived','success');
                } else {
                    sessionFlash('User failed deactived','error');
                }

                return redirect()->back();
            } else {
                sessionFlash('User not found','error');
                return redirect()->back();
            }
        } else {
            sessionFlash('User not found','error');
            return redirect()->back();
        }
    }
}
