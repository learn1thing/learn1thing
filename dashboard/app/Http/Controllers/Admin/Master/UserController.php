<?php

namespace App\Http\Controllers\Admin\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\Master\UserDataTable;
use App\DataTables\Master\ChangePaymentMethodDataTable;
use App\Models\User;
use App\Models\LogRole;
use App\Models\PaymentMethod;
use App\Models\CreditHistory;
use DB;
use Sentinel;
use GuzzleHttp\Client;
use App\Classes\CurlRequest;
use App\Http\Requests\Auth\PostRegisterRequest;
use App\Http\Requests\Master\User\ProfileRequest;
use App\Http\Requests\Master\User\ChangePasswordRequest;
use App\Http\Requests\Master\User\LinkAccountRequest;
use Session;
use App\Mail\ChangePaymentMethodMail;

use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    public function __construct(User $user)
    {
        $this->middleware('hasaccess:manage-user,manage-profile');
        $this->user = $user;
        $this->logRole = new LogRole;
        $this->paymentMethod = new PaymentMethod;
    }

    public function index(UserDataTable $dataTable)
    {
        return $dataTable->render('admin.master.users.index');
    }

    public function active(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $detail = $this->user->find($id);
            if ($detail) {
                $user = $detail;
                $activation = \Activation::exists($user);
                if ($activation) {
                    \Activation::complete($user, $activation->code);
                    if ($activation = \Activation::completed($user)) {
                        $result['result'] = 'success';
                        $result['message'] = 'User has been activated';
                    } else {
                        $result['result'] = 'failed';
                        $result['message'] = 'User failed activated';
                    }
                } else {
                    $result['result'] = 'failed';
                    $result['message'] = 'User failed activated';
                }
            } else {
                $result['result'] = 'failed';
                $result['message'] = 'User not found';
            }
            DB::commit();
            return $result;
        } catch (Exception $e) {
            DB::rollback();
            $data['result'] = 'failed';
            $data['message'] = $e->getMessage();
            return response()->json($data,400);
        }
    }

    public function getProfile()
    {
        $currentUser = Sentinel::getUser();

        if ($currentUser) {

            $client = new Client();
            $get_countries = $client->request('GET', env('OC_BASE_URL').'/index.php?route=feed/rest_api/countries', [
                'headers' => [
                  'X-Oc-Merchant-Id' => 12345,
                ]
              ]);
              $countries = json_decode($get_countries->getBody())->data;
              $data['countries'] = $countries;

              if(isset($currentUser->country)) {
                $get_country_detail = $client->request('GET', env('OC_BASE_URL').'/index.php?route=feed/rest_api/countries&id=' . $currentUser->country, [
                    'headers' => [
                      'X-Oc-Merchant-Id' => 12345,
                      'id' => 222,
                    ]
                  ]);
                  $country_detail = json_decode($get_country_detail->getBody())->data;
                  $data['zones'] = $country_detail->zone;  
              } else {
                $data['zones'] = array();
              }

        
            $data['user'] = $currentUser;
            $data['payment_method'] = $this->paymentMethod->pluck('name','id')->toArray();

            //
            return view('admin.master.users.profile')->with($data);

        } else {
            return redirect('/');
        }
    }

    public function updateProfile(ProfileRequest $request)
    {
        try {
            
            Log::info('Metode Pembayaran: '.$request->payment_method);
	    //dd($request);
            $user = Sentinel::getUser();
            $user->process_workflow();
            //update profile to OC
            // $updateOC = $this->user->updateOcUser($request);
            // if ($updateOC['status'] == 'error') {
            //     Session::flash('error', $updateOC['message']); 
            //     return redirect()->route('admin.user.profile.update');
            // }
            //end update profile to OC
            $message = '';

            if (@user_info('payment_method')->name) {
                if (user_info('is_change_payment_method')) {
                    $message = 'Payment method cannot be change, you have been requested before';
                } else {
                    $request->merge(['is_change_payment_method' => true]);
                    $message = 'Request for change payment method has been sent';
                }
            }

            $param = $request->all();
            $data =  $this->user->updateUser($user->id,$param,$user);
            if ($data) {

                // update profile OC
                $body = json_encode([
                    "firstname" => $request->first_name,
                    "lastname" => $request->last_name,
                    "newsletter" => $request->subscribe,
                    "address" => [
                        [
                            "firstname" => $request->first_name,
                            "lastname" => $request->last_name,
                            "address_1" => $request->address_1,
                            "address_2" => $request->address_2,
                            "city" => $request->city,
                            "country_id" => $request->country,
                            "zone_id" => (($request->country==188)?188:$request->region),
                            "postcode" => $request->post_code,
                            "default" => "1"
                        ]
                    ]
                ]);


                $responses = CurlRequest::sendRequest('PUT', env('OC_BASE_URL').'/index.php?route=rest/customer_admin/customers&id='.$user->customer_id,null,$body);
                if (@$responses->success ) {
                    info('success update profile OC');
                }else{
                    Log::info('Fail update profile in opencart');
                }

                // update profile Moodle
                $userMoodle = $this->user->getUserInfoMoodle($user->email);
                if (@$userMoodle[0]) {
                    $request->merge(['email' => $user->email]);
                    $updateUserMoodle = $this->user->updateUserMoodle($request,$userMoodle[0]->id);
                    info('success update profile Moodle');
                }

            }

            sessionFlash('Update Profile has been succeed. '.$message,'success');
            return redirect()->route('admin.user.profile.update');
        
        } catch (Exception $e) {
            info($e->getFile());
            info($e->getMessage());
            info($e->getLine());
            sessionFlash('Something went wrong','error');
            return redirect()->route('admin.user.profile.update');

        }
    }

    public function getCountryDetail($id)
    {
        $client = new Client();
        $get_country_detail = $client->request('GET', env('OC_BASE_URL').'/index.php?route=feed/rest_api/countries&id='.$id, [
        'headers' => [
          'X-Oc-Merchant-Id' => 12345,
          'id' => $id,
        ]
      ]);
      $country_detail = json_decode($get_country_detail->getBody())->data;
      return response()->json(['status' => true,'data' => $country_detail->zone],200);
    }

    public function changePassword()
    {
        return view('admin.master.users.change_password');
    }

    public function updatePassword(ChangePasswordRequest $request)
    {
        $hasher = Sentinel::getHasher();
        $oldPassword = $request->old_password;
        $password = $request->new_password;
        $passwordConf = $request->password_confirmation;

        $user = Sentinel::getUser();
        $user->process_workflow();
        if (!$hasher->check($oldPassword, $user->password)) {
            sessionFlash('Password is incorrect.', 'error');
            return redirect()->back();
        }

        $user_update = Sentinel::update($user, array('password' => $password));

        // update password log user
        $this->updatePasswordLogUser($user->email,$password);

        $user_update->checking();
        // update password OC
        $body = json_encode([
            "password" => $password,
            "confirm" => $password,
            "addresses" => []
        ]);
        $responses = CurlRequest::sendRequest('PUT', env('OC_BASE_URL').'/index.php?route=rest/customer_admin/customers&id='.$user->customer_id,null,$body);
        if (@$responses->success ) {
            info('success update password OC');
        }

        // update profile Moodle
        $userMoodle = $this->user->getUserInfoMoodle($user->email);
        if (@$userMoodle[0]) {
            $user->new_password = $password;
            $updateUserMoodle = $this->user->updateUserMoodle($user,$userMoodle[0]->id,'update-password');
            info('success update password Moodle');
        }

        sessionFlash('Password successfully updated','success');
        return redirect()->route('admin.user.profile');
    }

    public function linkAccount()
    {
        if (session()->has('email_refer')) {
            if (session()->get('email_refer') != user_info('email')) {
                abort(401);
            }
        }
        $user = $this->logRole->where( 'user_id', user_info('id') )->first();
 
	$return=[];	
        if ($user) {
            $logs = $this->logRole->where( 'user_id', $user->user_id )->get();
            foreach ($logs as $key => $log) {
                $userDetail = User::where('email',$log->username)->first();
                if ($userDetail) {
                    $return[$key]['email'] = $log->username;
                    $return[$key]['name'] = $userDetail->first_name.' '.$userDetail->last_name;
                    $return[$key]['role'] = $userDetail->roles[0]->name;
                    $return[$key]['status'] = ($log->default) ? 'Default' : '-';
                    if ($log->default) {
                        $return[$key]['action'] = '-';
                    } else {
                        $return[$key]['action'] = '<button data-href="'.route('admin.user.unlink-account',['id' => $log->id]).'" class="btn btn-danger btn-xs btn-link-account" title="Unlink Account">
                                                    <i class="fa fa-unlink fa-fw"></i>
                                                </button>';
                    }
                }
            }
        }
        return view('admin.master.users.link_account')->with(['list_accounts' => $return]);
    }


    public function storeLinkAccount(LinkAccountRequest $request)
    {
        $currentUser = user_info();
        $currentRole = user_info('role')->slug;
        $credentials = [
            'email'    => $request->email,
            'password' => $request->password,
        ];
        try
        {
            if ($user = Sentinel::stateless($credentials)) {
                // Authentication successful and the user is assigned to the `$user` variable.
                $hasLinked = $this->logRole->checkUser($currentUser->id,$request->email);
                if($hasLinked) {
                    sessionFlash('This account has been linked before','warning');
                } else {
                    // if ($currentRole == 'corporate-customer' && user_info('role')->slug == 'standard-customer') {
                    //     sessionFlash('Link account cannot be downgrade','warning');
                    // } else {
                        \DB::beginTransaction();
                        try {
                            $storeLogRole = $this->logRole->create(['user_id' => $currentUser->id, 'username' => $request->email, 'password' => $request->password]);
                            if ($storeLogRole) {
                                \DB::commit();
                                sessionFlash('Your account is successfully linked','success');
                            } else {
                                sessionFlash('Something went wrong','error');
                            }
                        } catch (\Exception $e) {
                            \DB::rollback();
                            sessionFlash('Internal server error','error');
                        }
                    // }
                }

                return redirect()->back();
            } else {
                // Authentication failed.
                sessionFlash('Account not found','error');
                return redirect()->back();
            }
        } catch (\Cartalyst\Sentinel\Checkpoints\NotActivatedException $e) {
            sessionFlash('Account not active','error');
            return redirect()->back();
        } catch (\Exception $e) {
            sessionFlash('Something went wrong','error');
            return redirect()->back();
        }
    }

    public function unlinkAccount($id)
    {
        $detail = $this->logRole->find($id);
        if ($detail) {
            if ($detail->delete()) {
                sessionFlash('Account has been unlinked','success');
                return redirect()->back();
            } else {
                sessionFlash('Something went wrong','error');
                return redirect()->back();
            }
        } else {
            sessionFlash('Account not found','error');
            return redirect()->back();
        }
    }

    public function changePaymentMethod(ChangePaymentMethodDatatable $dataTable)
    {
        return $dataTable->render('admin.master.payment_methods.index');
    }

    public function changePaymentMethodApprove($userId)
    {
        $user = $this->user->find($userId);
        if ($user) {
            $currentPaymentMethodId = $user->payment_method_id;
            $changeTo = \DB::table('payment_methods')->select('id','name')->where('id','!=',$currentPaymentMethodId)->first();

            $data['user'] = $user;
            $data['before'] = $user->paymentMethod->name;
            $data['request'] = $changeTo->name;

            if (strtolower(@$user->paymentMethod->name) == 'credit based system' && $user->credit > 0) {
                // reset credit 
                CreditHistory::create(['user_id' => $user->id, 'mutation' => '-'.$user->credit, 'credit' => 0 , 'activity' => 'Change Payment Method']);
            }
            $user->credit = 0;
            $user->payment_method_id = $changeTo->id;
            $user->is_change_payment_method = false;


            if ($user->save()) {
                \Mail::to( $user->email )->send(new ChangePaymentMethodMail($data)); 

                $result = [
                    'success' => true,
                    'message' => 'Payment method is successfully changed',
                    'type' => 'success'
                ];
            }
        } else {
            $result = [
                'success' => false,
                'message' => 'User not found or has been deleted',
                'type' => 'error'
            ];
        }

        return response()->json($result,200);
    }
}
