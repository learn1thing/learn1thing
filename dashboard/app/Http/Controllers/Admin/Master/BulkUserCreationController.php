<?php

namespace App\Http\Controllers\Admin\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\Master\BusinessPartnerDataTable;
use App\DataTables\GroupBusinessPartner\GroupBusinessPartnerDataTable;
use App\DataTables\Bulk\SubUserDataTable;
use App\Models\BusinessPartner;
use App\Models\CorporateCustomer;
use App\Models\CorporateGroup;
use App\Models\StandardCustomer;
use App\Models\RoleUser;
use App\Models\Role;
use App\Models\BusinessPartnerGroup;
use App\Models\LogRole;
use Excel;
use Response;
use App\Models\User;
use App\Models\Permission;
use Input;
use Session;
use Sentinel;
use DB;
use App\Http\Requests\Bulk\PostSingleUser;
use GuzzleHttp\Client;
use App\Classes\CurlRequest;
use App\Jobs\BulkUser;
use App\Jobs\AdminBulkUser;

class BulkUserCreationController extends Controller
{

    public function __construct(BusinessPartner $businesspartner, Role $role, Permission $permission)
    {
        $this->middleware('hasaccess:bulk-user-creation');

        $this->businesspartner = $businesspartner;
        $this->corporatecustomer = new CorporateCustomer;
        $this->role = $role;
        $this->permission = $permission;
        $this->authRegis = new \App\Http\Controllers\Auth\Api\AuthenticateController;
        $this->department = new \App\Models\Department;
        $this->user = new User;
    }

    public function getDataSubUser(SubUserDataTable $dataTable)
    {
        return $dataTable->render('admin.master.users.business_partners.bulk.index');
    }

    public function bulkCreationUser()
    {
        $model = $this->getModel();
        $related_ids = $model->where('created_by',user_info('id'))->pluck('name', 'id');
        $departments = $this->department->getDepartmentByUserId(user_info('id'))->select('id','parent_id','name')->get();
        $parentData = [];
        foreach ($departments as $key => $value) {
            array_push($parentData, $value->parent_id);
        }
        return view('admin.master.users.business_partners.bulk.create', compact('related_ids','departments','parentData'));
    }

    public function downloadSample()
    {
        //CSV file is stored under project/public/file/sample_bulk_user.csv
        $file= public_path(). "/file/sample_bulk_user.csv";

        $headers = array(
                  'Content-Type: application/csv',
                );

        return Response::download($file, 'sample.csv', $headers);
    }

    public function createSingleUser()
    {
        $model = $this->getModel();
        $data['related_ids'] = $model->getGroups()->pluck('name','id');
        $data['departments'] = $this->department->getDepartment()->select('id','parent_id','name')->get();
        $data['parentData'] = [];
        foreach ($data['departments'] as $key => $value) {
            array_push($data['parentData'], $value->parent_id);
        }
        $client = new Client();
        $get_countries = $client->request('GET', env('OC_BASE_URL').'/index.php?route=feed/rest_api/countries', [
            'headers' => [
              'X-Oc-Merchant-Id' => 12345,
            ]
          ]);
          $countries = json_decode($get_countries->getBody())->data;
          foreach ($countries as $country) {
            $data['countries'][$country->country_id] = $country->name;
          }
          $data['zones'] = [];
        return view('admin.master.users.business_partners.bulk.create_single_user', $data);
    }

    public function storeSingleUser(PostSingleUser $request)
    {   
        \DB::beginTransaction();
        try
        {
            $model = $this->getModel();
            $requests = $request;
            $request->merge([
                'department_id' => (!empty($request->department_id)) ? $request->department_id : null,
                'parent_id' => user_info('id'),
                'related_id' => $request->related_id,
                'related_type' => (user_info()->inRole('business-partner') || user_info('related_type') == 'BusinessPartnerGroup') ? BusinessPartnerGroup::class : CorporateGroup::class
            ]);

            if (!empty($request->department_id)) {
                $user = \Sentinel::registerAndActivate($request->all());
            } else {
                $user = \Sentinel::registerAndActivate($request->except(['department_id']));
            }

            $rolesDetail = $this->getRoleDetail($request);


            // attach role
            Sentinel::findRoleById( $rolesDetail->id )->users()->attach(Sentinel::findById( $user->id ));


            $user = User::find($user->id);
            if (user_info('role')->slug == 'business-partner') {
                $user->customer_group_id = 2;
            } else {
                $user->customer_group_id = 4;
            }
            
            // Register to Moodle \.start
            $registerMoodle = $this->authRegis->registerMoodle($user,'normaly',false,$request->password);
            if (@$registerMoodle->exception == 'invalid_parameter_exception') {
                // update profile Moodle
                $userMoodle = $this->user->getUserInfoMoodle($request->email);
                if (@$userMoodle[0]) {
                    $user->new_password = $request->password;
                    $updateUserMoodle = $this->user->updateUserMoodle($user,$userMoodle[0]->id);
                    info('success update profile Moodle');
                    unset($user->new_password);
                }
            }
            // Register to Moodle \.end

            // Register to Moodle \.start
            // $registerMoodle = $this->authRegis->registerMoodle($user,'normaly',false,$request->password);
            // Register to Moodle \.end

            // Register to OC \.start
            $registerOC = $this->authRegis->registerOC($user,'normaly',false,$request->password);
            // Register to OC \.end
            unset($user->customer_group_id);
            if ($registerOC->success) {
                info('success register opencart');
                $user->update(['customer_id' => $registerOC->data->customer_id]);
            }else{
                info('Gagal register');
            }

            // Create log role
            $storeLogRole = LogRole::create(['user_id' => $user->id, 'username' => $user->email, 'password' => $request->password, 'default' => 1, 'active' => 1]);

            sessionFlash('User is successfully added.','success');
            \DB::commit();
            return redirect()->route('admin.business-partner.bulk.index');
        } catch (\Exception $e) {
            \DB::rollback();
            sessionFlash($e->getMessage(),'error');
            return redirect()->back();
        }

    }

    public function storeBulk(Request $request)
    {
        set_time_limit(0);
        $validator = \Validator::make($request->all(), [
            'bulk_csv' => 'required|mimes:csv,txt',
            'related_id' => 'required',
        ]);

        if($validator->fails()){
            $messages = $validator->errors();
            if ($messages->has('related_id')) {
                sessionFlash('Group is required','error');
                return redirect()->back();
            }
            if ($messages->has('bulk_csv')) {
                sessionFlash('File upload must be a file of type: csv','error');
                return redirect()->back();
            }
        }

        // Upload File
        $fileName = time().'.'.$request->bulk_csv->getClientOriginalExtension();
        $getFile = $request->bulk_csv->move(public_path('file'), $fileName);

        // Import Data To Users
        $dataExport = Excel::load($getFile, function($reader) {
            $reader->toArray();
        })->get();

        unlink($getFile);
        
        if (count($dataExport) > 0) {
            info('here');

            //komen by rdw 2017-12-29
            /*$data['request'] = $request->except(['bulk_csv']);
            $data['user'] = user_info();
            $data['pathFile'] = public_path('file/'.$fileName);
            $data['dataExport'] = $dataExport;*/
            $data['dataExport'] = $dataExport;
            $data['user'] = user_info();

            // add by rdw 2017-12-29
            $model = $this->getModel();
            $request = $request->except(['bulk_csv']);
            $pathFile = public_path('file/'.$fileName);
            //$dataExport = $this->dataExport;
            $this->userModel = new User;

            $dataExists['email'] = array();
            $invalidData['invalid'] = 0;
            $dataFailed['email'] = array();

            $rolesDetail = $this->getRoleDetailBulk($request);

            $successBulk = 0;

            \DB::beginTransaction();
            foreach ($data['dataExport'] as $element) {
                try
                {
                    if (empty($element->email)) {
                        continue;
                    }
                    if (strlen($element->password) < 8 || empty($element->first_name) || empty($element->last_name)) {
                        array_push($dataFailed['email'], $element->email);
                        continue;
                    }

                    $findAvailabeData = false;
                    $requests = new Request;
                    $requests->merge(['email' => $element->email, 'first_name' => $element->first_name, 
                        'last_name' => $element->last_name, 'phone' => $element->phone, 
                        'new_password' => $element->password, 'password' => $element->password]);

                    if($element->email === ""){
                        $invalidData['invalid'] = $invalidData['invalid'] + 1;
                    } else {
                        $findAvailabeData = $this->userModel->where('email',$element->email)->where('parent_id',$data['user']->id)->first();
                    }
                    //return print_r($data['user']->id);break;
                    
                    info('------------------------------------------------------------');
                    info('find available data');
                    info(json_encode($findAvailabeData));
                    if(!$findAvailabeData && !empty($element->email)){
                        if ($this->userModel->whereEmail($element->email)->first()) {
                            array_push($dataExists['email'], $element->email);
                            info('email yang gagal : '.$element->email);
                            continue;
                        }
                        info('Register middleware : '.$element->email);
                        $user = Sentinel::registerAndActivate([
                            'first_name' => $element->first_name,
                            'last_name' => $element->last_name,
                            'email' => $element->email,
                            'phone' => $element->phone,
                            'password' => $element->password,
                            'department_id' => (!empty($request['department_id'])) ? $request['department_id'] : null,
                            'parent_id' => $data['user']->id,
                            'related_id' => $request['related_id'],
                            'related_type' => ($data['user']->inRole('business-partner') || @$data['user']->related_type == 'BusinessPartnerGroup') ? BusinessPartnerGroup::class : CorporateGroup::class
                        ]);

                        // attach role
                        Sentinel::findRoleById( $rolesDetail->id )->users()->attach($user);

                        $user = $this->userModel->find($user->id);
                        if ($data['user']->roles[0]->slug == 'business-partner') {
                            $user->customer_group_id = 2;
                        } else {
                            $user->customer_group_id = 4;
                        }
                        $requests->merge(['customer_group_id' => $user->customer_group_id]);
                        // Register to Moodle \.start
                        $registerMoodle = $this->authRegis->registerMoodle($requests,'normaly',false,$element->password);
                        if (@$registerMoodle->exception == 'invalid_parameter_exception') {
                            // update profile Moodle
                            $userMoodle = $this->userModel->getUserInfoMoodle($element->email);
                            if (@$userMoodle[0]) {
                                $updateUserMoodle = $this->userModel->updateUserMoodle($requests,$userMoodle[0]->id);
                                info('success update profile Moodle');
                            }
                        }
                        // Register to Moodle \.end

                        // Register to OC \.start
                        $registerOC = $this->authRegis->registerOC($requests,'normaly',false,$element->password);
                        unset($user->customer_group_id);
                        if (!$registerOC->success) {
                            $requests->merge(['customer_id' => $user->customer_id]);
                            $updateUserOC = $this->userModel->updateUserOC($requests,'bulk');
                            if ($updateUserOC) {
                                info('success update profile OC');
                            }
                        } else {
                            $user->update(['customer_id' => $registerOC->data->customer_id]);
                            info('gagal register oc maka update OC');
                        }
                        // Register to OC \.end

                        // Create log role
                        if (@$user) {
                            $storeLogRole = LogRole::firstOrCreate(['user_id' => $user->id, 'username' => $user->email, 'password' => $element->password, 'default' => 1, 'active' => 1]);
                        }

                    } else {
                        if ($findAvailabeData) {
                            $user = Sentinel::findById($findAvailabeData->id);
                            Sentinel::update($user, [
                                'first_name' => $element->first_name,
                                'last_name' => $element->last_name,
                                'password' => $element->password,
                                'phone' => $element->phone,
                                'related_id' => $request['related_id'],
                                'department_id' => (@$request['department_id']) ? $request['department_id'] : null,
                            ]);

                            if ($user->roles()->first()) {
                                $user->roles[0]->users()->detach( $user );
                            } else {
                                $role = new RoleUser;
                                $role->user_id = $user->id;
                                $role->role_id = $rolesDetail->id;
                                $role->save();
                            }
                            Sentinel::findRoleById( $rolesDetail->id )->users()->attach($user);

                            // update profile Moodle
                            $userMoodle = $this->userModel->getUserInfoMoodle($element->email);
                            if (@$userMoodle[0]) {
                                $updateUserMoodle = $this->userModel->updateUserMoodle($requests,$userMoodle[0]->id,'update-password');
                                info('success update profile Moodle');
                            }

                            //update profile OC
                            $requests->merge(['customer_id' => $user->customer_id]);
                            $updateUserOC = $this->userModel->updateUserOC($requests,'bulk');
                            if ($updateUserOC) {
                                info('success update profile OC');
                            }

                            // update log user
                            LogRole::where('username',$element->email)
                                ->update(['password' => $element->password]);
                        }
                        
                    }
                    \DB::commit();
                    $successBulk++;

                } catch (\Exception $e) {
                    \DB::rollback();
                    // sessionFlash('Something went wrong!','error');
                    info('error');
                    info($e);
                    info('-----------------------------------------------------------');
                }
            }
            
            
            //inser log
            /*BulkLog::insert([
                'user_id' => $this->user->id,
                'data' => json_encode($dataFailed['email']),
                'description' => 'Bulk user success: '.$successBulk.' failed: '.count($dataFailed['email']),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);*/

            if(count($dataExists['email']) > 0){
                $this->userModel->process_workflow( $dataExists['email'], true, 'storeBulk', $data['user']->email );
            }

            if(count($dataFailed['email']) > 0){
                $this->userModel->process_workflow( $dataFailed['email'], true, 'storeBulk', $data['user']->email );
            }

            // if($invalidData['invalid'] > 0){
            // }

            if(count($dataExists['email']) == 0 && $invalidData['invalid'] == 0) {
                $this->userModel->process_workflow(null, null, 'storeBulk', $data['user']->email);
            }
            //komen by rdw 2017-12-29
            // $this->dispatch(new BulkUser($data));

            //sessionFlash('System is processing bulk user', 'success');
            //print_r($dataExport);break;
            sessionFlash('Bulk user success: '.$successBulk.' failed: '.count($dataFailed['email']), 'success');
        } else {
            sessionFlash('Data is empty', 'error');
        }


        // $model = $this->getModel();

        // Upload File
        // $fileName = time().'.'.$request->bulk_csv->getClientOriginalExtension();
        // $getFile = $request->bulk_csv->move(public_path('file'), $fileName);

        // // Import Data To Users
        // $dataExport = Excel::load($getFile, function($reader) {
        //     $reader->toArray();
        // })->get();

        // $dataExists['email'] = array();
        // $invalidData['invalid'] = 0;

        // $rolesDetail = $this->getRoleDetail($request);

        // \DB::beginTransaction();
        // foreach ($dataExport as $element) {
        //     try
        //     {
        //         $findAvailabeData = false;
        //         $requests = new Request;
        //         $requests->merge(['email' => $element->email, 'first_name' => $element->first_name, 
        //             'last_name' => $element->last_name, 'phone' => $element->phone, 
        //             'new_password' => $element->password, 'password' => $element->password]);

        //         if($element->email === ""){
        //             $invalidData['invalid'] = $invalidData['invalid'] + 1;
        //         } else {
        //             $findAvailabeData = User::where('email',$element->email)->where('parent_id',user_info('id'))->first();
        //         }
        //         if(!$findAvailabeData && !empty($element->email)){
        //             if (User::whereEmail($element->email)->first()) {
        //                 array_push($dataExists['email'], $element->email);
        //                 continue;
        //             }
        //             $user = \Sentinel::registerAndActivate([
        //                 'first_name' => $element->first_name,
        //                 'last_name' => $element->last_name,
        //                 'email' => $element->email,
        //                 'password' => $element->password,
        //                 'department_id' => (!empty($request->department_id)) ? $request->department_id : null,
        //                 'parent_id' => user_info('id'),
        //                 'related_id' => $request->related_id,
        //                 'related_type' => (user_info()->inRole('business-partner')) ? BusinessPartnerGroup::class : CorporateGroup::class
        //             ]);

        //             // attach role
        //             Sentinel::findRoleById( $rolesDetail->id )->users()->attach($user);

        //             $user = User::find($user->id);
        //             if (user_info('role')->slug == 'business-partner') {
        //                 $user->customer_group_id = 2;
        //             } else {
        //                 $user->customer_group_id = 4;
        //             }
        //             $requests->merge(['customer_group_id' => $user->customer_group_id]);
        //             // Register to Moodle \.start
        //             $registerMoodle = $this->authRegis->registerMoodle($requests,'normaly',false,$element->password);
        //             if (@$registerMoodle->exception == 'invalid_parameter_exception') {
        //                 // update profile Moodle
        //                 $userMoodle = $this->user->getUserInfoMoodle($element->email);
        //                 if (@$userMoodle[0]) {
        //                     $updateUserMoodle = $this->user->updateUserMoodle($requests,$userMoodle[0]->id);
        //                     info('success update profile Moodle');
        //                 }
        //             }
        //             // Register to Moodle \.end

        //             // Register to OC \.start
        //             $registerOC = $this->authRegis->registerOC($requests,'normaly',false,$element->password);
        //             unset($user->customer_group_id);
        //             if (!$registerOC->success) {
        //             //     $getUserOC = CurlRequest::sendRequest('GET', env('OC_BASE_URL').'/index.php?route=rest/customer_admin/customers&email='.$element->email);
        //             //     if (@$getUserOC->success ) {
        //             //     info(json_encode($getUserOC));
        //             //         // update profile OC
        //             //         $body = json_encode([
        //             //             "firstname" => $element->first_name,
        //             //             "lastname" => $element->last_name,
        //             //             "telephone" => $element->phone,
        //             //             "password" => $element->password,
        //             //             "confirm" => $element->password,
        //             //             "address" => []
        //             //         ]);
        //             //         $responses = CurlRequest::sendRequest('PUT', env('OC_BASE_URL').'/index.php?route=rest/customer_admin/customers&id='.$getUserOC->data->customer_id,null,$body);
        //             //         if (@$responses->success ) {
        //             //             info('success update profile OC');
        //             //             $user->update(['customer_id' => $getUserOC->data->customer_id]);
        //             //         }
        //             //     }
        //             } else {
        //                 $user->update(['customer_id' => $registerOC->data->customer_id]);
        //             }
        //             // Register to OC \.end

        //         } else {
        //             if ($findAvailabeData) {
        //                 $user = Sentinel::findById($findAvailabeData->id);
        //                 Sentinel::update($user, [
        //                     'first_name' => $element->first_name,
        //                     'last_name' => $element->last_name,
        //                     'password' => $element->password,
        //                     'related_id' => $request->related_id,
        //                     'department_id' => (@$request->department_id) ? $request->department_id : null,
        //                 ]);

        //                 if ($user->roles()->first()) {
        //                     $user->roles[0]->users()->detach( $user );
        //                 } else {
        //                     $role = new RoleUser;
        //                     $role->user_id = $user->id;
        //                     $role->role_id = $rolesDetail->id;
        //                     $role->save();
        //                 }
        //                 Sentinel::findRoleById( $rolesDetail->id )->users()->attach($user);

        //                 // update profile Moodle
        //                 $userMoodle = $this->user->getUserInfoMoodle($element->email);
        //                 if (@$userMoodle[0]) {
        //                     $updateUserMoodle = $this->user->updateUserMoodle($requests,$userMoodle[0]->id);
        //                     info('success update profile Moodle');
        //                 }

        //                 //update profile OC
        //                 $requests->merge(['customer_id' => $user->customer_id]);
        //                 $updateUserOC = $this->user->updateUserOC($request);
        //                 if ($updateUserOC) {
        //                     info('success update profile OC');
        //                 }
        //             }
                    
        //         }

        //         // Create log role
        //         if (@$user) {
        //             $storeLogRole = LogRole::create(['user_id' => $user->id, 'username' => $user->email, 'password' => $element->password, 'default' => 1, 'active' => 1]);
        //         }

        //     } catch (\Exception $e) {
        //         \DB::rollback();
        //         // \Session::flash('invalid',$e->getMessage());
        //         sessionFlash('Something went wrong!','error');
        //         return redirect()->route('admin.business-partner.bulk.index');
        //     }
        // }
        // if($dataExists['email']){
        //     // \Session::flash('exists','Users with email <b style="font-color:#000">'.implode(', ', $dataExists['email']).'</b> already added before.');
        //     \DB::commit();
        //     $this->user->process_workflow( $dataExists['email'], true ); 
        //     sessionFlash('Sorry, Users with email '.implode(', ', $dataExists['email']).' is already added before.','warning');
        // }

        // if($invalidData['invalid'] > 0){
        //     // \Session::flash('invalid','Oppss '.$invalidData['invalid'].' data cannot be saved because email is empty.');
        //     sessionFlash('Sorry, '.$invalidData['invalid'].' data cannot be saved because email is empty.','warning');
        // }

        // if(count($dataExists['email']) == 0 && $invalidData['invalid'] == 0) {
        //     \DB::commit();
        //     // \Session::flash('flash_message','Users is successfully added/updated.');
        //     $this->user->process_workflow(); 
        //     sessionFlash('Users are successfully added/updated.','success');
        // }

        // unlink($getFile);

        return redirect()->route('admin.business-partner.bulk.index');

    }

    public function editUser($id)
    {
        $model = $this->getModel();
        $data['user'] = User::where('id',$id)->first();

        if ($this->haveNoAccess($data['user'])) {
            sessionFlash('Sorry, you dont have access','warning');
            return redirect()->route('admin.business-partner.bulk.index');
        }
        $data['related_ids'] = $model->getGroups()->pluck('name','id');

        // $data['departments'] = $this->department->getDepartmentByUserId(user_info('id'))->select('id','parent_id','name')->whereNull('parent_id')->get();
        $data['departments'] = $this->department->getDepartment()->select('id','parent_id','name')->get();
        // if (($data['user'] && @$data['user']->parent_id == user_info('id')) || user_info()->hasAccess('all')) {
            $client = new Client();
            $get_countries = $client->request('GET', env('OC_BASE_URL').'/index.php?route=feed/rest_api/countries', [
                'headers' => [
                  'X-Oc-Merchant-Id' => 12345,
                ]
              ]);
              $countries = json_decode($get_countries->getBody())->data;
              foreach ($countries as $country) {
                $data['countries'][$country->country_id] = $country->name;
              }

              $get_country_detail = $client->request('GET', env('OC_BASE_URL').'/index.php?route=feed/rest_api/countries&id='.$data['user']->country, [
                'headers' => [
                  'X-Oc-Merchant-Id' => 12345,
                  'id' => $data['user']->country,
                ]
              ]);
              $country_detail = json_decode($get_country_detail->getBody())->data;
              $data['zones'] = [];
              if (@$country_detail->zone) {
                  foreach ($country_detail->zone as $zone) {
                    $data['zones'][$zone->zone_id] = $zone->name;
                  }
              }
            $data['parentData'] = [];
            foreach ($data['departments'] as $key => $value) {
                array_push($data['parentData'], $value->parent_id);
            }
            return view('admin.master.users.business_partners.bulk.edit',$data);
        // } else {
        //     abort(401);
        // }
    }

    public function updateUser(Request $request, $id)
    {
        $rules = [
            'email' => 'required|unique:users,email,'.$id.',id'

        ];

        // print_r($request->phone);die();
        $this->validate($request,$rules);

        $requests = $request;

        $user = Sentinel::findById($id);
        if ($this->haveNoAccess($user)) {
            sessionFlash('Sorry, you dont have access','warning');
            return redirect()->route('admin.business-partner.bulk.index');
        }

        $rolesDetail = $this->getRoleDetail($request);

        if (empty($request->password)) {
            $updateData = $request->except(['_token','password']);
            $type = 'update-profile';
        } else {
            if (strlen($request->password) < 8) {
                sessionFlash('Passwords must be at least 8 characters long.');
                return redirect()->back();
            }
            $updateData = $request->except(['_token']);
            $type = 'update-password';
            $requests->merge(['new_password' => $request->password]);
            
            // update password log user
            $this->updatePasswordLogUser($user->email,$request->password);
        }
        if (!empty($request->department_id)) {
            $updateData = $updateData;
        } else {
            $updateData['department_id'] = null;
        }
        Sentinel::update($user, $updateData);
        // $user->email = $request->email;
        // $user->first_name = $request->first_name;
        // $user->last_name = $request->last_name;
        // $user->phone = $request->phone;
        // $user->related_id = $request->related_id;
        // $user->save();

        // $user->update([
        //         'email' => $request->email,
        //         'first_name' => $request->first_name,
        //         'last_name' => $request->last_name,
        //         'phone' => $request->phone,
        //         'related_id' => $request->related_id,
        //     ]);

        $user->roles[0]->users()->detach( $user );
        Sentinel::findRoleById( $rolesDetail->id )->users()->attach(Sentinel::findById( $id ));

        $udpatedUser = User::find($id);

        // update profile Moodle
        $userMoodle = $this->user->getUserInfoMoodle($udpatedUser->email);
        if (@$userMoodle[0]) {
            $updateUserMoodle = $this->user->updateUserMoodle($requests,$userMoodle[0]->id,$type);
            info('success update profile Moodle');
        }

        if ($type == 'update-profile') {
            $body = json_encode([
                "firstname" => $request->first_name,
                "lastname" => $request->last_name,
                "address" => [
                    [
                        "firstname" => $request->first_name,
                        "lastname" => $request->last_name,
                        "address_1" => $request->address_1,
                        "address_2" => $request->address_2,
                        "city" => $request->city,
                        "country_id" => $request->country,
                        "zone_id" => $request->region,
                        "postcode" => $request->post_code,
                        "default" => "1"
                    ]
                ]
            ]);
            
        } else {
            $body = json_encode([
                "firstname" => $request->first_name,
                "lastname" => $request->last_name,
                "password" => $request->password,
                "confirm" => $request->password,
                "address" => [
                    [
                        "firstname" => $request->first_name,
                        "lastname" => $request->last_name,
                        "address_1" => $request->address_1,
                        "address_2" => $request->address_2,
                        "city" => $request->city,
                        "country_id" => $request->country,
                        "zone_id" => $request->region,
                        "postcode" => $request->post_code,
                        "default" => "1"
                    ]
                ]
            ]);
        }

        // update profile OC
        if ($udpatedUser->customer_id) {
            $responses = CurlRequest::sendRequest('PUT', env('OC_BASE_URL').'/index.php?route=rest/customer_admin/customers&id='.$udpatedUser->customer_id,null,$body);
            if (@$responses->success ) {
                info('success update profile OC');
            }
        }

        sessionFlash('Data has been update','success');
        return redirect()->route('admin.business-partner.bulk.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyUser($id)
    {
        $model = User::where('id',$id)->first();
        // if (($model && @$model->parent_id == user_info('id')) || user_info()->hasAccess('all')) {
        if ($this->haveNoAccess($model)) {
            sessionFlash('Sorry, you dont have access','warning');
            return redirect()->route('admin.business-partner.bulk.index');
        }
            if ($model->forceDelete()) {
                Session::flash('message', 'Data has been deleted'); 
                Session::flash('alert-class', 'alert-success'); 
                return redirect()->route('admin.business-partner.bulk.index');
            } else {
                Session::flash('message', 'Data can not be deleted'); 
                Session::flash('alert-class', 'alert-error'); 
                return redirect()->route('admin.business-partner.bulk.index');
            }
        // } else {
        //     abort(401);
        // }
    }

    protected function getModel()
    {
        if (user_info('role')->slug == 'business-partner') {
            $model = new BusinessPartnerGroup;
        } else {
            if (user_info('parent_id')) {
                $user = User::find(user_info('parent_id'));
                if ($user->inRole('business-partner')) {
                    $model = new BusinessPartnerGroup;    
                } else {
                    $model = new CorporateGroup;
                }
                return $model;
            }
            $model = new CorporateGroup;
        }
        return $model;
    }

    protected function haveNoAccess($data)
    {
        if (!user_info()->inRole('super-admin')) {
            if (user_info('parent_id')) {
                if ($data->parent_id != user_info('id')) {
                    return true;
                }
            } else {
                $childs = user_info()->downlines()->pluck('id')->toArray();
                if (!in_array($data->parent_id, $childs) && $data->parent_id != user_info('id')) {
                    return true;
                }
            }
        }
        return false;
    }

    protected function getRoleDetail($request)
    {
        $upline = false;
        if (user_info()->upline)
            $upline = user_info()->upline->inRole('business-partner');

        if (user_info('role')->slug == 'business-partner' || $upline) {
            $rolesDetail = Role::join('business_partner_groups','business_partner_groups.id','roles.business_partner_group_id')
                ->select('roles.id','roles.name')
                ->where('roles.business_partner_group_id',$request->related_id);
        } else {
            $rolesDetail = Role::join('corporate_groups','corporate_groups.id','roles.corporate_group_id')
                ->select('roles.id','roles.name')
                ->where('roles.corporate_group_id',$request->related_id);
        }

        if (user_info('parent_id')) {
            $rolesDetail = $rolesDetail->where(function($query) {
                $query->where('created_by',user_info('id'))
                    ->orWhere('created_by',user_info('parent_id'));
            });
        } else {
            $childs = user_info()->downlines()->pluck('id')->toArray();
            $rolesDetail = $rolesDetail->where(function($query) use($childs) {
                $query->where('created_by',user_info('id'))
                    ->orWhereIn('created_by',$childs);
            });
        }

        return $rolesDetail->first();
    }


    // add by rdw 2017-12-29
    protected function getRoleDetailBulk($request)
    {
        $upline = false;
        if (user_info()->upline)
            $upline = user_info()->upline->inRole('business-partner');

        if (user_info('role')->slug == 'business-partner' || $upline) {
            $rolesDetail = Role::join('business_partner_groups','business_partner_groups.id','roles.business_partner_group_id')
                ->select('roles.id','roles.name')
                ->where('roles.business_partner_group_id',$request['related_id']);
        } else {
            $rolesDetail = Role::join('corporate_groups','corporate_groups.id','roles.corporate_group_id')
                ->select('roles.id','roles.name')
                ->where('roles.corporate_group_id',$request['related_id']);
        }

        if (user_info('parent_id')) {
            $rolesDetail = $rolesDetail->where(function($query) {
                $query->where('created_by',user_info('id'))
                    ->orWhere('created_by',user_info('parent_id'));
            });
        } else {
            $childs = user_info()->downlines()->pluck('id')->toArray();
            $rolesDetail = $rolesDetail->where(function($query) use($childs) {
                $query->where('created_by',user_info('id'))
                    ->orWhereIn('created_by',$childs);
            });
        }

        return $rolesDetail->first();
    }



    public function adminCreateBulk()
    {
        return view('admin.master.users.bulk_user');
    }

    public function adminStoreBulk(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'bulk_csv' => 'required|mimes:csv,txt',
            'related_id' => 'required',
        ]);

        if($validator->fails()){
            $messages = $validator->errors();
            if ($messages->has('bulk_csv')) {
                sessionFlash('File upload must be a file of type: csv','error');
                return redirect()->back();
            }
        }

        // Upload File
        $fileName = time().'.admin-'.$request->bulk_csv->getClientOriginalExtension();
        $getFile = $request->bulk_csv->move(public_path('file'), $fileName);

        // Import Data To Users
        $dataExport = Excel::load($getFile, function($reader) {
            $reader->toArray();
        })->get();

        unlink($getFile);
        
        if (count($dataExport) > 0) {
            $data['dataExport'] = $dataExport;
            $data['user'] = user_info();
           
            //added by rdw k5 04/12/2017
            $this->userModel = new User;
            $dataExists['email'] = array();
            $invalidData['invalid'] = 0;
            $dataFailed['email'] = array();
            $successBulk = 0;

            \DB::beginTransaction();
            foreach ($data['dataExport'] as $element) {
                try {
                    if (empty($element->email)) {
                        continue;
                    }
                    if (strlen($element->password) < 8 || empty($element->first_name) || empty($element->last_name)) {
                        array_push($dataFailed['email'], $element->email);
                        continue;
                    }
                    $findAvailabeData = false;
                    $requests = new Request;
                    $requests->merge(['email' => $element->email, 'first_name' => $element->first_name, 
                        'last_name' => $element->last_name, 'phone' => $element->phone, 
                        'new_password' => $element->password, 'password' => $element->password]);

                    if($element->email === ""){
                        $invalidData['invalid'] = $invalidData['invalid'] + 1;
                    } else {
                        $findAvailabeData = $this->userModel->where('email',$element->email)->first();
                    }

                    if(!$findAvailabeData && !empty($element->email)){
                        if ($this->userModel->whereEmail($element->email)->first()) {
                            array_push($dataExists['email'], $element->email);
                            continue;
                        }
                        
                        // register standard customer
                        $insert = StandardCustomer::create([
                            'name' => $element->first_name.' '.$element->last_name,
                            'address' => '',
                        ]);

                        $user = Sentinel::registerAndActivate([
                            'first_name' => $element->first_name,
                            'last_name' => $element->last_name,
                            'email' => $element->email,
                            'password' => $element->password,
                            'phone' => $element->phone,
                            'related_id' => $insert->id,
                            'related_type' => StandardCustomer::class
                        ]);

                        // attach role
                        $role = Sentinel::findRoleBySlug('standard-customer');
                        $role->users()->attach($user);

                        $requests->merge(['customer_group_id' => 1]);
                        // Register to Moodle \.start
                        $registerMoodle = $this->authRegis->registerMoodle($requests,'normaly',false,$element->password);
                        if (@$registerMoodle->exception == 'invalid_parameter_exception') {
                            // update profile Moodle
                            $userMoodle = $this->userModel->getUserInfoMoodle($element->email);
                            if (@$userMoodle[0]) {
                                $updateUserMoodle = $this->userModel->updateUserMoodle($requests,$userMoodle[0]->id);
                                info('success update profile Moodle');
                            }
                        }
                        // Register to Moodle \.end

                        // Register to OC \.start
                        $registerOC = $this->authRegis->registerOC($requests,'normaly',false,$element->password);
                        unset($user->customer_group_id);
                        if (!$registerOC->success) {
                            $requests->merge(['customer_id' => $user->customer_id]);
                            $updateUserOC = $this->userModel->updateUserOC($requests,'bulk');
                            if ($updateUserOC) {
                                info('success update profile OC');
                            }
                        } else {
                            $user->update(['customer_id' => $registerOC->data->customer_id]);
                        }
                        // Register to OC \.end

                        // Create log role
                        if (@$user) {
                            $storeLogRole = LogRole::firstOrCreate(['user_id' => $user->id, 'username' => $user->email, 'password' => $element->password, 'default' => 1, 'active' => 1]);
                        }

                    } else {
                        if ($findAvailabeData) {
                            $user = Sentinel::findById($findAvailabeData->id);
                            Sentinel::update($user, [
                                'first_name' => $element->first_name,
                                'last_name' => $element->last_name,
                                'password' => $element->password,
                                'phone' => $element->phone
                            ]);

                            if ($user->roles()->first()) {
                                $user->roles[0]->users()->detach( $user );
                            }

                            $role = Sentinel::findRoleBySlug('standard-customer');
                            $role->users()->attach($user);

                            // update profile Moodle
                            $userMoodle = $this->userModel->getUserInfoMoodle($element->email);
                            if (@$userMoodle[0]) {
                                $updateUserMoodle = $this->userModel->updateUserMoodle($requests,$userMoodle[0]->id,'update-password');
                                info('success update profile Moodle');
                            }

                            //update profile OC
                            $requests->merge(['customer_id' => $user->customer_id]);
                            $updateUserOC = $this->userModel->updateUserOC($requests);
                            if ($updateUserOC) {
                                info('success update profile OC');
                            }

                            // update log user
                            LogRole::where('username',$element->email)
                                ->update(['password' => $element->password]);
                        }
                        
                    }

                    $successBulk++;

                } catch (Exception $e) {
                    \DB::rollback();
                    info($e->getMessage());
                    info($e->getFile());
                    info($e->getLine());
                    sessionFlash($e->getMessage(),'error');
                    return redirect()->back();
                }
            }
            \DB::commit();



            sessionFlash('Bulk user success: '.$successBulk.' failed: '.count($dataFailed['email']), 'success');
            //return redirect()->back();

            return redirect()->route('admin.standard-customer.index');

            //komen by rdw k5 04/12/2017
            //$this->dispatch(new AdminBulkUser($data));

            //sessionFlash('System is processing bulk user ', 'success');
        } else {
            sessionFlash('Data is empty', 'error');
            return redirect()->back();
        }

        //komen by rdw k5 04/12/2017
        return redirect()->back();
    }
}
