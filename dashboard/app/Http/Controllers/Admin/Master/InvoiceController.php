<?php

namespace App\Http\Controllers\Admin\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\Master\InvoiceDataTable;
use App\Models\Invoice;
use App\Models\User;
use App\Mail\MailInvoice;
use PDF;

class InvoiceController extends Controller
{
    public function __construct()
    {
        $this->invoice = new Invoice;
        $this->user = new User;
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $return['status'] = false;
            $return['message'] = 'Data not available';
            $invoices = $this->invoice->getInvoice($request->start_date,$request->end_date,$request->corporate);
            if (count($invoices) > 0) {
                $return['status'] = true;
                $return['message'] = 'Data available';
            }
            $return['data'] = $this->getInvoice($invoices,$request->corporate,$request->start_date,$request->end_date);
            return response()->json($return, 200);
        } else {
            $corporates = $this->user->getUsersByRole('corporate-customer')
                ->join('payment_methods','payment_methods.id','=','users.payment_method_id')
                ->join('activations','activations.user_id','=','users.id')
                ->where('activations.completed',1)
                ->get('id','first_name','last_name');

            return view('admin.master.invoices.index')->with(['corporates' => $corporates]);
        }
        // $this->restrictAccess();
        // return $dataTable->render('admin.master.invoices.index');
    }

    public function detail($month,$year)
    {
        $this->restrictAccess();
        $invoices = $this->invoice->getInvoiceByDate($month,$year,user_info('id'));
        return view('admin.master.invoices.detail')->with(['invoices' => $invoices]);
    }

    protected function restrictAccess()
    {
        if (!user_info()->inRole('super-admin')) {
            abort(401);
        }
    }

    protected function getInvoice($invoices,$corporate,$start_date,$last_date)
    {
        $tempInvoice = [];
        $html = '<table class="table table-striped">
                  <tbody>
                    <tr>
                      <th>ID</th>
                      <th>Date</th>
                      <th>Product</th>
                      <th>Status</th>
                      <th>Quantity</th>
                      <th>Unit Price</th>
                      <th>Total</th>
                      <th>Action</th>
                    </tr>';
        $total = 0;
        if(count($invoices) > 0) {
            foreach($invoices as $key => $invoice) {
                $total += $invoice->total;
                $html .= '<tr>
                    <td>'.$invoice->invoice_id.'</td>
                    <td>'.date('d F Y H:i:s',strtotime($invoice->date)).'</td>
                    <td>'.$invoice->product_name.'</td>
                    <td>';
                    if($invoice->status == 1) {
                        $html .= '<span class="label label-warning">Pending</span>';
                        $tempInvoice[$key] = $invoice;
                    } elseif($invoice->status == 2) {
                        $html .= '<span class="label label-success">Complete</span>';
                    } else{
                        $html .= '<span class="label label-danger">Reject</span>';
                    }
                $html .= '</td>
                    <td style="text-align: right;">'.$invoice->quantity.'</td>
                    <td style="text-align: right;">$'.number_format($invoice->price).'</td>
                    <td style="text-align: right;">$'.number_format($invoice->total).'</td>
                    <td style="text-align: center;">';
                    if($invoice->status == 1) {
                        $html .= '<button class="btn btn-xs btn-success invoice-complete" data-send="'.base64_encode(json_encode($invoice)).'" data-id="'.$invoice->invoice_id.'"><i class="fa fa-check"></i></button>';
                    } else {
                        $html .= '-';
                    }
                    $html .= '</td></tr>';
            }
        } else {
            $html .= '<tr>
                      <td colspan="8" align="center"><i>No Transaction</i></td>
                    </tr>
                  </tbody></table>';
            return $html;
        }

        $html .= '</tbody>
            <tfoot>
                <tr>
                  <td colspan="6" style="text-align: right;"><b>Total</b></td>
                  <td style="text-align: right;"><b>$'.number_format($total).'</b></td>
                </tr>
            </tfoot>
            </table>
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <button class="btn btn-danger print-pdf" data-user="'.$corporate.'" data-first_date="'.$start_date.'" data-last_date="'.$last_date.'" data-print="'.base64_encode(json_encode($tempInvoice)).'iLd4F"><i class="fa fa-file-pdf-o"></i> Print PDF</button>
                        <button class="btn btn-info send-mail" data-user="'.$corporate.'" data-send="'.base64_encode(json_encode($tempInvoice)).'iLd4F"><i class="fa fa-envelope"></i> Send Email</button>
                    </div>
                </div>
            </div>';

        return $html;
    }

    public function sendMail(Request $request)
    {
        $data['invoices'] = json_decode(base64_decode(substr($request->data, 0,strlen($request->data) - 5)));
        $data['user'] = $this->user->find($request->user);
        $data['status'] = 'Pending';

        if (count($data['invoices']) > 0) {
            if ($data['user']) {
                $mail = \Mail::to( $data['user']->email )->send(new MailInvoice($data)); 
                return response()->json(['status' => true, 'message' => 'Email has been sent', 'data' => (object) array()]);
            } else {
                return response()->json(['status' => false, 'message' => 'User not found', 'data' => (object) array()]);
            }
        } else {
            return response()->json(['status' => false, 'message' => 'Nothing to send, Invoice has been completed', 'data' => (object) array()]);
        }
    }

    public function updateStatusOrder(Request $request)
    {
        \DB::beginTransaction();
        try
        {
            $invoice = $this->invoice->find($request->id);
            if ($invoice) {
                $data['user'] = $invoice->user;
                $data['invoices'][] = json_decode(base64_decode($request->data));
                $data['status'] = 'Complete';

                // Update invoice
                $invoice->update(['status' => 2]);

                //update invoice detail
                foreach ($invoice->details as $detail) {
                    $detail->update(['status' => 2]);
                }

                // $mail = \Mail::to( $data['user']->email )->send(new MailInvoice($data));
                \DB::commit();
                return response()->json(['status' => true, 'message' => 'Status has been completed', 'data' => (object) array()]);
            } else {
                return response()->json(['status' => false, 'message' => 'Invoice not found', 'data' => (object) array()]);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            return response()->json(['status' => false, 'message' => 'Something went wrong', 'data' => (object) array()]);
        }
    }

    public function printPDF(Request $request)
    {
        $print['invoices'] = json_decode(base64_decode(substr($request->data, 0,strlen($request->data) - 5)));
        $print['user'] = $this->user->find($request->user);
        $print['status'] = 'Pending';

        if (count($print['invoices']) > 0) {

            if ($print['user']) {
                if (@$request->check) {
                    return response()->json(['status' => true, 'message' => 'Success', 'data' => (object) array()]);
                } else {
                    $printSend = [
                        'user' => $print['user'],
                        'invoices' => $print['invoices'],
                        'status' => $print['status'],
                    ];
                    $pdf = PDF::loadView('emails.invoice_send', $printSend);
                    return $pdf->download('invoice-PayAsYouGo-'.$print['user']->email.'/'.$request->first_date.'~'.$request->last_date.'.pdf');
                }
            } else {
                return response()->json(['status' => false, 'message' => 'User not found', 'data' => (object) array()]);
            }
        } else {
            return response()->json(['status' => false, 'message' => 'Nothing to send, Invoice has been completed', 'data' => (object) array()]);
        }
    }
}
