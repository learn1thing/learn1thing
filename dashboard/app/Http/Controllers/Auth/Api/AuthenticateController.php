<?php

namespace App\Http\Controllers\Auth\Api;

use Sentinel;
use Reminder;
use App\Classes\TryCatch;
use App\Classes\ApiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\PostLoginReqeust;
use App\Http\Requests\Auth\PostRegisterRequest;
use App\Http\Requests\Auth\PostForgotRequest;
use App\Http\Requests\Auth\PostResetPasswordRequest;
use Cartalyst\Sentinel\Persistences\EloquentPersistence as Persistences;
use App\Http\Requests\Auth\PostRegisterSocialRequest;
use App\Models\BusinessPartnerGroup;
use App\Models\Role;
use App\Models\StandardCustomer;
use App\Models\BusinessPartner;
use App\Models\CorporateCustomer;
use App\Models\Social;
use App\Models\User;
use App\Models\RegisterPage;
use App\Models\LogRole;
use App\Models\PaymentMethod;
use GuzzleHttp\Client;
use DB;
use Cookie;
use Config;
use App\Classes\CurlRequest;
use Illuminate\Support\Facades\Log;

class AuthenticateController extends Controller
{

    /**
     * Status header from response
     * @var integer
     */
    public $statusCode;


    /**
     * Persistences code user
     * @var string
     */
    protected $persistences;

    /**
     * token cookie for authorize
     * @var string
     */
    protected $token;

    protected $email;
    protected $login_type;
    protected $social_type;

    /**
     * Define status create cookie or not
     * @var boolean
     */
    protected $isCreateCookie;


    protected $login_url_moodle;

    /**
     * Authenticate Login User [for API]
     * @param  Illuminate\Http\Request $request
     * @return Illuminate\Support\Response
     */
    public function postLogin(PostLoginReqeust $request)
    {
      $email = '';
      $login_type = '';
      $social_type = '';

        $credentials = [
            'email'    => $request->email,
            'password' => $request->password,
            'remember' => $request->remember ? (bool)$request->remember : false
        ];

        $ipAddress = $request->ip();

        $prosesing = new TryCatch(function() use ($credentials, $ipAddress,$request) {
        $checkCredentail = [
            'login' => $credentials['email'],
        ];
        $exitsLogin = Sentinel::findByCredentials($checkCredentail);

        if ($exitsLogin) {
            $this->onExistLogin($exitsLogin);
        }

        if ($request->login_type == 'social-media') {
            $findUser = User::where('email',$request->email)->first();
            $social_account = $findUser->socialRelated()->orderBy('updated_at')->first();

            $user = Sentinel::login($findUser);
            $this->login_type = $request->login_type;
            $this->social_type = $social_account->provider;

        } else {
            $user = Sentinel::authenticate($credentials, $credentials['remember']);
            $this->login_type = 'normaly';
            $this->social_type = 'normaly';

        }

        if (!$user) {
            $this->statusCode = 401;
            $this->onFailLogin();
            $this->token = '';
            $data = new ApiResponse([
                'success' => false,
                'message' => trans('auth.failed'),
                'code' => $this->statusCode
            ], (object)[]);
        } else {
            $persistences = $user->persistences->first();

            if ($user->inRole('corporate-customer') || $user->inRole('business-partner') || $user->inRole('super-admin') || $user->inRole('standard-customer')) {
                $this->isCreateCookie = true;
            } else {
                $this->isCreateCookie = false;
            }

            $token = $persistences->code;
            $this->statusCode = 200;
            $this->token = $token;
            $this->email = $user->email;
            // $this->saveUserInfo($persistences, $ipAddress);

            // login OC
	    // Comment by puw on 23-03-2018 replace with curl
	    /*
            $client = new Client();
            $loginOC = $client->request('POST', env('OC_BASE_URL').'/index.php?route=rest/login/login', [
              'headers' => [
                'X-Oc-Merchant-Id' => 12345,
              ],
              'body' => json_encode(['email' => $request->email, 'password' => $request->password])
            ]);
	    */

	    //Added by puw on 22-03-2018
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,env('OC_BASE_URL').'/index.php?route=rest/login/login');
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Oc-Merchant-Id:12345'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,array('email'=>$request->email,'password'=>$request->password));

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $server_output = curl_exec ($ch);

            curl_close ($ch);

            $curl_result=json_decode($server_output);

            //$resultLoginOC = json_decode($loginOC->getBody());
	    $resultLoginOC=$curl_result;

            if ($resultLoginOC->success) {

              //get token moodle
              $tokenMoodle = $this->getMoodleToken($request);

              $login_url_moodle=$this->getCourseLoginUrl($request->email);


              $data = new ApiResponse([
                  'success' => true,
                  'message' => trans('auth.success'),
                  'code' => $this->statusCode
              ], $this->mapUserWithMoodle($user, $token,$login_url_moodle ,$resultLoginOC->data->session,baseEncrypt($credentials['password']),$tokenMoodle));
            } else {
              $data = new ApiResponse([
                  'success' => false,
                  'message' => $resultLoginOC->error->warning,
                  'code' => 400
              ], (object)[]);
            }
        }

        return $data;
      }, function(\Exception $e) {

        if ($e instanceof \Cartalyst\Sentinel\Checkpoints\NotActivatedException) {
          $this->statusCode = 401;
        } else if ($e instanceof \Cartalyst\Sentinel\Checkpoints\ThrottlingException) {
          $this->statusCode = 429;
        } else {
          $this->statusCode = 400;
        }

        $this->token = '';
        $this->onFailLogin();
        $data = new ApiResponse([
          'success' => false,
          'message' => $e->getMessage(),
          'code' => $this->statusCode
        ], (object)[]);
        return $data;
      });
      if ($this->isCreateCookie) {
        $this->token = Cookie::queue('token', $this->token, 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
        $password = Cookie::queue('password', baseEncrypt($credentials['password']), 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
        $login_type = Cookie::queue('login_type', $login_type, 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
        $social_type = Cookie::queue('social_type', $social_type, 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
        $this->email = Cookie::queue('email', baseEncrypt($this->email), 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
      }
      return response()->json($prosesing->getCallback(), $this->statusCode);
    }

    /**
     * When Exist Login, will automatic create session notice force logout [For API]
     * @param  App\Models\Master\User $user
     * @return void
     */
    public function onExistLogin($user)
    {
      $persistences = $user->persistences->first();
      if ($persistences) {
        $this->persistences = $persistences->code;
        cache()->put($persistences->code, trans('auth.force_logout'), 60);
      }
    }

    /**
     * On fail login user [For API]
     * @return void
     */
    public function onFailLogin()
    {
      if ($this->persistences) {
        if (cache()->has($this->persistences)) {
          cache()->forget($this->persistences);
        }
      }
    }

    /**
     * Logout proses [For API]
     * @param  \Illuminate\Http\Request $request
     * @return Illuminate\Support\Response
     */
    public function postLogout(Request $request)
    {
      Log::info('logout via api');
      $prosesing = new TryCatch(function() use($request) {
          $token = $this->getTokenFromReqeust($request);
          // return $token;die();
          $user = Sentinel::findByPersistenceCode($token);
          if ($user) {
            $this->onLogout($token);
            $this->statusCode = 200;
            Sentinel::logout($user, true);
            return new ApiResponse([
              'success' => true,
              'message' => trans('auth.success_logout'),
              'code' => 200
            ], (object)[]);
          } else {
            throw new \Exception("Failed For Login", 500);
          }
      }, function(\Exception $e) {
         $this->statusCode = 500;
          return new ApiResponse([
            'success' => false,
            'message' => trans('auth.failed_logout'),
            'code' => 500
          ], (object)[]);
      });

      return response()->json($prosesing->getCallback(), $this->statusCode)->cookie(cookie()->forget('token'));
    }

    /**
     * get user base on token|persistence code [For API]
     * @param  \Illuminate\Http\Request $request
     * @return Illuminate\Support\Response
     */
    public function getUserFromIp(Request $request)
    {
      $hasPersistences = $this->getFromPersistences($request->ip());
      if ($hasPersistences) {
        $data = new ApiResponse([
          'success' => true,
          'message' => trans('auth.success_token'),
          'code' => 200
        ], $hasPersistences->code);
        return response()->json($data, 200);
      }

      $data = new ApiResponse([
        'success' => false,
        'message' => trans('auth.token_not_provide'),
        'code' => 401
      ], (object)[]);

      return response()->json($data, 401);
    }

    /**
     * Get Persistences From IP
     * @param  string $ipAddress
     * @return Cartalyst\Sentinel\Persistences\EloquentPersistence
     */
    public function getFromPersistences($ipAddress)
    {
      $persistenceCode = Persistences::where('ip_address', $ipAddress)->where('browser', \BrowserDetect::browserFamily())->first();
      if ($persistenceCode) {
        return $persistenceCode;
      }

      return false;
    }

    /**
     * Ovverider user repository sentinel [For API]
     * @param  Sentinel $userRepository
     * @param  string $token
     * @return array
     */
    public function mapUser($userRepository, $token, $tokenOC = null, $password=null,$tokenMoodle=null)
    {
      $role = $userRepository->roles->first();

      if ($role) {
        unset($role->created_at);
        unset($role->updated_at);
        unset($role->pivot);
      }

      return [
              'id' => $userRepository->id,
              'customer_id' => $userRepository->customer_id,
              'user_moodle_id' => $this->getMoodleUserId($userRepository->email),
              'first_name' => $userRepository->first_name,
              'last_name' => $userRepository->last_name,
              'email' => baseEncrypt($userRepository->email),
              'password' => $password,
              'token' => $token,
              'token_oc' => $tokenOC,
              'token_moodle' => $tokenMoodle,
              'role' => $role,
              'url_image' => url_to_image($userRepository->image),
              'url_logo' => url_to_image(@$userRepository->related->logo,'logo'),
              'description' => @$userRepository->related->description,
              'company_name' => @$userRepository->related->name];
    }

    //add by puw on 21-03-2018

    public function mapUserWithMoodle($userRepository, $token,$moodle_url_redirect, $tokenOC = null, $password=null,$tokenMoodle=null)
    {
      $role = $userRepository->roles->first();

      if ($role) {
        unset($role->created_at);
        unset($role->updated_at);
        unset($role->pivot);
      }

      return [
              'id' => $userRepository->id,
              'customer_id' => $userRepository->customer_id,
              'user_moodle_id' => $this->getMoodleUserId($userRepository->email),
              'first_name' => $userRepository->first_name,
              'last_name' => $userRepository->last_name,
              'email' => baseEncrypt($userRepository->email),
              'password' => $password,
              'token' => $token,
              'token_oc' => $tokenOC,
              'token_moodle' => $tokenMoodle,
              'role' => $role,
              'url_image' => url_to_image($userRepository->image),
              'url_logo' => url_to_image(@$userRepository->related->logo,'logo'),
              'description' => @$userRepository->related->description,
              'company_name' => @$userRepository->related->name,
              'moodle_url_redirect'=>$moodle_url_redirect
            ];
    }

    /**
     * Get user from token [For API]
     * @param  Request $request
     * @return \App\Classes\ApiResponse
     */
    public function getUser(Request $request)
    {
        $token = $this->getTokenFromReqeust($request);
        $user = Sentinel::findByPersistenceCode($token);
        if ($user) {
            $data = new ApiResponse([
                'success' => true,
                'message' => 'Success for get profile',
                'code' => 200
            ], $this->mapUser($user, $token));
            return response()->json($data, 200);
        }

        $data = new ApiResponse([
            'success' => false,
            'message' => trans('unauthorized'),
            'code' => 401
        ], (object)[]);
        return response()->json($data, 401);
    }

    /**
     * [getRegister description]
     * @author iddan
     * @return [type] [description]
     */
    public function getRegister(Request $request, $register_type = null, $id_user_social = null)
    {
      $data['register_type'] = $register_type;
      $client = new Client();
      $get_countries = $client->request('GET', env('OC_BASE_URL').'/index.php?route=feed/rest_api/countries', [
        'headers' => [
          'X-Oc-Merchant-Id' => 12345,
        ]
      ]);
      $countries = json_decode($get_countries->getBody())->data;
      $data['countries'] = $countries;

      $get_country_detail = $client->request('GET', env('OC_BASE_URL').'/index.php?route=feed/rest_api/countries&id=222', [
        'headers' => [
          'X-Oc-Merchant-Id' => 12345,
          'id' => 222,
        ]
      ]);
      $country_detail = json_decode($get_country_detail->getBody())->data;
      $data['zones'] = $country_detail->zone;

      //ali
      $data['id_user_social'] = $id_user_social;

      $data['payment_method'] = PaymentMethod::all();

      if ($register_type != 'social-media') {
        return view('auth.register')->with($data);
      } else {
        return view('auth.register_social')->with($data);
      }
    }

    public function detailCountry($country_id)
    {
      $client = new Client();
      $get_country_detail = $client->request('GET', env('OC_BASE_URL').'/index.php?route=feed/rest_api/countries&id='.$country_id, [
        'headers' => [
          'X-Oc-Merchant-Id' => 12345,
          'id' => $country_id,
        ]
      ]);
      $country_detail = json_decode($get_country_detail->getBody())->data;
      return $country_detail->zone;
    }

    /**
     * [postRegister description]
     * @author iddan
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postRegister(PostRegisterRequest $request)
    {
        // modify fadly .start here
        \DB::beginTransaction();
        try
        {
            $role_slug = get_role_slug($request->customer_group_id);

            // Register to Moodle
            $data_post_register_moodle = $this->registerMoodle($request,'normaly','normaly',$request->password);

            if (!empty($data_post_register_moodle)) {
                if (!is_array($data_post_register_moodle) && property_exists($data_post_register_moodle, 'exception')) {
                    // update profile Moodle
                    $userModel = new User;
                    $userMoodle = $userModel->getUserInfoMoodle($request->email);
                    if (@$userMoodle[0]) {
                        $request->merge(['new_password' => $request->password]);
                        $updateUserMoodle = $userModel->updateUserMoodle($request,$userMoodle[0]->id);
                        info('success update profile Moodle');
                        // unset($request->new_password);
                    }
                    // $result = new ApiResponse([
                    //     'success' => false,
                    //     'message' => (empty($data_post_register_moodle->debuginfo) ? 'On Course :'.$data_post_register_moodle->message:'On Course: '.@$data_post_register_moodle->debuginfo),
                    //     'code' => 400
                    // ], (object)[]);
                    // return response()->json($result,200);
                }
            }

            Log::info('Register Moodle selesai');

            //End Register to Moodle

            // Register API OC #iddan
            $data_post_register = $this->registerOC($request,'normaly','normaly',$request->password);

            if (property_exists($data_post_register, "success") && empty($data_post_register->success)) {
                $result = new ApiResponse([
                    'success' => false,
                    'message' => (empty($data_post_register->error->warning) ?'On Store: Something went wrong' : 'On Store: '.$data_post_register->error->warning),
                    'code' => 400
                ], (object)[]);
                return response()->json($result,200);
            }

            // END REGISTER OC
            Log::info('Register Opencart selesai');

            //doCreateDataRelated
            $related = $this->ceateDataRelated($request,$role_slug);
            // Register
            $request->merge(['customer_id' => $data_post_register->data->customer_id]);

            if ($request->customer_group_id == 1) {
              $user = Sentinel::registerAndActivate($request->all());
            } else {

              session()->flash('alert-class' , 'success');
              session()->flash('message' , 'Register has been successfully.');

              $user = Sentinel::register($request->all());
              $activation = \Activation::create($user);
            }

            Log::info('Register Dashboard selesai');

            if (!$user) {
                // modify fadly .start here
                $result = new ApiResponse([
                    'success' => false,
                    'message' => 'Something went wrong',
                    'code' => 400
                ], (object)[]);
                return response()->json($result,200);
                // modify fadly .end here
            }

            if ($related) {
                $user->related_id = $related['data']->id;
                $user->related_type = $related['class'];
                $user->save();
            }

            // Attach user to role
            $role = Sentinel::findRoleBySlug($role_slug);
            if (!$role) {
                // modify fadly .start here
                $result = new ApiResponse([
                    'success' => false,
                    'message' => 'Role not found',
                    'code' => 400
                ], (object)[]);
                return response()->json($result,200);
                // modify fadly .end here
            }
            $role->users()->attach($user);

            if ($role_slug == 'business-partner') {
                $businesspartner = new BusinessPartnerGroup;
                $businesspartner->name = 'Coach';
                $businesspartner->description = 'Default coach group';
                $businesspartner->created_by = $user->id;
                $businesspartner->save();

                // ADD NEW ROLE //
                $role = new Role;
                $role->name = $businesspartner->name;
                $role->business_partner_group_id = $businesspartner->id;
                $role->save();

                $roleGroup = Sentinel::findRoleById($role->id);
                // $roleGroup->addPermission('dashboard', true);
                // Automatic add permission for coach
                $roleGroup->permissions = [
                    'dashboard' => true,
                    'manage-course' => true,
                    'manage-schedule' => true,
                    'manage-coach' => true,
                    'manage-profile' => true,
                ];
                // End Automatic add permission for coach
                $roleGroup->save();
            } else {
            }
                // Create log role
                $storeLogRole = LogRole::create(['user_id' => $user->id, 'username' => $user->email, 'password' => $request->password, 'default' => 1, 'active' => 1]);

            // End register API OC #iddan


            //add by puw on 20-03-2018 | redirect for login moodle
            $moodle_url_redirect=$this->getCourseLoginUrl($request->email);

            if ($moodle_url_redirect =='/') {
              $moodle_url_redirect = redirect()->route('login');
            }

            // modify fadly .start here
            $dataUser = [
                'id'          => $user->id,
                'email'       => $user->email,
                'role'        => ($user->roles->first()) ? $user->roles->first()->slug : '',
                'first_name'  => $user->first_name,
                'last_name'   => $user->last_name,
                'token'       => '',
                'redirect_url'=>$moodle_url_redirect
            ];



            $data = $this->login_from_register( $request->email, $request->password );


            $result = new ApiResponse([
                'success' => true,
                'message' => 'Register success',
                'code' => 200
            ],$dataUser);

            \DB::commit();
            return response()->json($result,200);
            // modify fadly .end here
        } catch (\Exception $e) {
            \DB::rollback();
            $result = new ApiResponse([
                'success' => false,
                'message' => $e->getMessage(),
                'code'    => 400
            ], (object)[]);

            return response()->json($result,400);
        }
        // modify fadly .end here
    }

    /**
     * [postRegisterSocial description]
     * @author aLi
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postRegisterSocial(PostRegisterSocialRequest $request)
    {
        // Register
        DB::beginTransaction();
        try
        {
            $login_type = '';
            $social_type = '';
            $email = '';
            $password = '';

            $role_slug = get_role_slug($request->customer_group_id);

            $user = Sentinel::findById($request->id_user_social);
            if (!$user) {
                return response()->json([
                    'result' => 'Error',
                    'message' => 'Error register',
                ], 200);
            }

            // Attach user to role
            $role = Sentinel::findRoleBySlug($role_slug);
            if (!$role) {
                return response()->json([
                    'result' => 'Error',
                    'message' => 'Role not found',
                ], 200);
            }
            $role->users()->attach($user);

            if ($role_slug == 'business-partner') {
                $businesspartner = new BusinessPartnerGroup;
                $businesspartner->name = 'Coach';
                $businesspartner->description = 'Default coach group';
                $businesspartner->created_by = $user->id;
                $businesspartner->save();

                // ADD NEW ROLE //
                $role = new Role;
                $role->name = $businesspartner->name;
                $role->business_partner_group_id = $businesspartner->id;
                $role->save();

                $roleGroup = Sentinel::findRoleById($role->id);
                // $roleGroup->addPermission('dashboard', true);
                // Automatic add permission for coach
                $roleGroup->permissions = [
                    'dashboard' => true,
                    'manage-course' => true,
                    'manage-schedule' => true,
                    'manage-coach' => true,
                    'manage-profile' => true,
                ];
                // End Automatic add permission for coach
                $roleGroup->save();
            }


            //get password from social id first registration
            $social_account = $user->socialRelated()->orderBy('updated_at','DESC')->first();

            // $first_social_account = $user->socialRelated()->orderBy('id','ASC')->first();
            $randPassword = Config::get('services.static_password');//$social_account->social_id;


            // Register to Moodle
            $user->customer_group_id = $request->customer_group_id;
            $data_post_register_moodle = $this->registerMoodle($user,'social-media',$social_account,$randPassword);

            if (!empty($data_post_register_moodle)) {
                if (!is_array($data_post_register_moodle) && property_exists($data_post_register_moodle, 'exception')) {
                    $result = new ApiResponse([
                        'success' => false,
                        'message' => (empty($data_post_register_moodle->debuginfo) ? 'On Course :'.$data_post_register_moodle->message:'On Course: '.@$data_post_register_moodle->debuginfo),
                        'code' => 400
                    ], (object)[]);
                    return response()->json($result,200);
                }
            }
            //End Register to Moodle

            // Register API OC #iddan
            $data_post_register = $this->registerOC($user,'social-media',$social_account,$randPassword);

            if (property_exists($data_post_register, "success") && empty($data_post_register->success)) {
                $result = new ApiResponse([
                    'success' => false,
                    'message' => (empty($data_post_register->error->warning) ?'On Store: Something went wrong' : 'On Store: '.$data_post_register->error->warning),
                    'code' => 400
                ], (object)[]);
                return response()->json($result,200);
            }
            // END REGISTER OC


            //update customer_id user
            unset($user->customer_group_id);
            $user->customer_id = $data_post_register->data->customer_id;
            $user->save();

            $dataUser = [
                    'id'          => $user->id,
                    'email'       => $user->email,
                    'role'        => ($user->roles->first()) ? $user->roles->first()->slug : '',
                    'first_name'  => $user->first_name,
                    'last_name'   => $user->last_name,
                    'token'       => ''
                ];

                $result = new ApiResponse([
                    'success' => true,
                    'message' => 'Register success',
                    'code' => 200
                ],$dataUser);

            $related = $this->ceateDataRelated($request,$role_slug);
            if ($related) {
                $user->related_id = $related['data']->id;
                $user->related_type = $related['class'];
                $user->save();
            }

            // if role is standar customer and busines partner dont actived
            if ($role_slug != 'standard-customer') {

                session()->flash('alert-class' , 'alert-success');
                session()->flash('message' , trans('auth.waiting_verification'));

                $result = new ApiResponse([
                        'success' => true,
                        'message' => trans('auth.waiting_verification'),
                        'code' => 200
                    ], (object)[]);

            } else {
                $active = $this->activationUser($user->related);
                $userLogin = Sentinel::login($user);

                if (!$userLogin) {
                    $this->statusCode = 401;
                    $this->onFailLogin();
                    $this->token = '';
                    $result = new ApiResponse([
                        'success' => false,
                        'message' => trans('auth.failed'),
                        'code' => $this->statusCode
                    ], (object)[]);
                } else {
                    $persistences = $userLogin->persistences->first();

                    if ($userLogin->inRole('business-partner') || $userLogin->inRole('super-admin') || $userLogin->inRole('standard-customer') || $userLogin->inRole('corporate-customer')) {
                        $this->isCreateCookie = true;
                    } else {
                        $this->isCreateCookie = false;
                    }

                    //add by puw on 21-03-2018
                    //api-redirect-moodle
                    $moodle_url_redirect=$this->getCourseLoginUrl($userLogin->email);


                    $token = $persistences->code;
                    $this->statusCode = 200;
                    $this->token = $token;
                    $result = new ApiResponse([
                        'success' => true,
                        'message' => trans('auth.success'),
                        'code' => $this->statusCode
                    ], $this->mapUserWithMoodle($userLogin, $token, $moodle_url_redirect));
                }

                if ($this->isCreateCookie) {
                    $this->token = Cookie::queue('token', $this->token, 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
                    $password = Cookie::queue('password', baseEncrypt($randPassword), 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
                    $login_type = Cookie::queue('login_type', 'social-media', 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
                    $social_type = Cookie::queue('social_type', @$social_account->provider, 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
                    $email = Cookie::queue('email', baseEncrypt($user->email), 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
                }
            }

            // Create log role
            // if ($role_slug != 'business-partner') {
              $storeLogRole = LogRole::create(['user_id' => $user->id, 'username' => $user->email, 'password' => $randPassword, 'default' => 1, 'active' => 1]);
            // }

            DB::commit();

            return response()->json($result,200);
                            // ->withCookie($this->token);
                            // ->withCookie($password)
                            // ->withCookie($login_type)
                            // ->withCookie($social_type)
                            // ->withCookie($email);

        } catch (\Exception $e) {
            info($e->getMessage());
            info($e->getLine());
            info($e->getFile());

            DB::rollback();
            $result = new ApiResponse([
                'success' => false,
                'message' => $e->getMessage(),
                'code'    => 400
            ], (object)[]);

            return response()->json($result,400);
        }
    }

    public function getForgot()
    {
      return view('auth.forgot-password');
    }

    public function postForgot(PostForgotRequest $request)
    {
      $find_user = Sentinel::findByCredentials(['login' => $request->email]);
      Reminder::removeExpired();
      $reminder = (Reminder::exists($find_user)) ? Reminder::exists($find_user) : Reminder::create($find_user);

      // Log::info('Find User');
      // Log::info($find_user);

      // Log:info('Reminder');
      // Log::info($reminder);
      // forgot password API OC #iddan
      $client = new Client();
      $data = [
        'email' => $request->email,
        'code' => $reminder->code,
      ];

      $post_forgot = $client->request('POST', env('OC_BASE_URL').'/index.php?route=rest/forgotten/forgotten', [
        'headers' => [
          'X-Oc-Merchant-Id' => 12345,
        ],
        'body' => json_encode($data)
      ]);

      $data_post_forgot = json_decode($post_forgot->getBody());

      $result = new ApiResponse([
        'success' => true,
        'message' => 'Forgot password success',
        'code' => 200
      ], $data_post_forgot);
      // End forgot password API OC #iddan

      return response()->json($result, 200);
    }

    public function postResetPassword(PostResetPasswordRequest $request)
    {
      $user = Sentinel::findByCredentials(['login' => $request->email]);
      $credentials = [
        'password' => $request->password,
      ];
      $user_update = Sentinel::update($user, $credentials);

      // if ($reminder['result'] = Reminder::complete($user, $request->code, $request->password)) {
      //   $reminder['status'] = 'Password Changed!';
      // } else {
      //   $reminder['status'] = 'Error Token!';
      // }

      $result = new ApiResponse([
        'success' => true,
        'message' => 'Reset password',
        'code' => 200
      ], $user_update);

      return response()->json($result, 200);
    }

    public function registerOC($user,$login_type='normaly',$social_type=false,$randPassword=false)
    {
        $client = new Client();

        $data = [
            'firstname' => $user->first_name,
            'lastname' => (empty($user->last_name) ? 'Last name' : $user->last_name),
            'email' => @$user->email,
            'telephone' => (@$user->phone) ? $user->phone : '123',
            'fax' => empty($user->fax)?'123456789': $user->fax,
            'address_1' => empty(@$user->address_1) ? 'address 1' : $user->address_1,
            'address_2' => empty(@$user->address_2) ? '' : $user->address_2,
            'city' => empty(@$user->city) ? 'city' : $user->city,
            'postcode' => empty(@$user->post_code) ? '1234' : $user->post_code,
            'country_id' => empty(@$user->country) ? '1' : $user->country,
            'zone_id' => empty(@$user->region) ? '0':$user->region,
            'password' => $randPassword,
            'confirm' => $randPassword,
            'agree' => true,
            'company' => empty($user->company) ?'company': $user->company,
            'login_type' => $login_type,
            'social_type'=> @$social_type->provider,
            'social_id'=> @$social_type->social_id
        ];

        info(json_encode($data));

        $post_register = $client->request('POST', env('OC_BASE_URL').'/index.php?route=rest/register/register', [
          'headers' => [
            'X-Oc-Merchant-Id' => 12345,
          ],
          'form_params' => $data
        ]);

        $data_post_register = json_decode($post_register->getBody());

        return $data_post_register;

        // End register API OC #iddan
    }

    public function registerMoodle($user,$login_type='normaly',$social_type=false,$randPassword=false)
    {
        $data['users'][0] =[
                    'username'=>strtolower($user->email),
                    'password'=>$randPassword,
                    'createpassword'=>'0',
                    'firstname'=>$user->first_name,
                    'lastname'=>(@$user->last_name) ? $user->last_name : '',
                    'email'=>$user->email,
                    'auth'=>'manual',
                    'idnumber'=>'string',
                    'lang'=>'en',
                    'calendartype'=>'string',
                    'theme'=>'',
                    'timezone'=>'99',
                    'mailformat'=>'1',
                    'description'=>'description',
                    'city'=>'city',
                    'country'=>'country',
                    'firstnamephonetic'=>'1wertg',
                    'lastnamephonetic'=>'lastnamephonetic',
                    'middlename'=>$user->first_name,
                    'alternatename'=>$user->first_name,
                    'preferences'=>array(array('type'=>'1wertg','value'=>'1wertg')),
                    'customfields'=>array(array('type'=>'1wertg','value'=>'1wertg')),
                  ];

        $token = env('MOODLE_TOKEN_REGISTER');
        $responses = CurlRequest::requestMoodle('POST', 'core_user_create_users', $data,$token);

        if (is_array($responses) && !empty($responses[0]->id)) {

            $roleMoodle = getRoleMoodle($user->customer_group_id);
            $param['role_id'] = $roleMoodle['id'];
            $param['user_id'] = $responses[0]->id;
            $param['contextid'] = $roleMoodle['contextid'];

            $this->assignRoleUserMoodle($param);
        }
        return $responses;

        // End register API OC #iddan
    }

    public function assignRoleUserMoodle($param)
    {
        $data['assignments'][0] =[
                    'roleid'=>$param['role_id'],
                    'userid'=>$param['user_id'],
                    'contextid'=>$param['contextid'],
                    // 'contextlevel'=>'',
                ];
        $token = env('MOODLE_TOKEN_ASSIGN_ROLE');
        $responses = CurlRequest::requestMoodle('POST', 'core_role_assign_roles', $data,$token);

        return $responses;
    }

    public function login_from_register( $email, $password )
    {

        $credentials = [
          'email'    => $email,
          'password' => $password,
          'remember' => false
        ];
        $prosesing = new TryCatch(function() use ($credentials) {
        $user = Sentinel::authenticate($credentials, $credentials['remember']);

        if (!$user) {
          $userSocial = User::join('social_logins','users.id','=','social_logins.user_id')
                    ->where(['users.email' => $credentials["email"],
                             'social_logins.social_id' => $credentials["password"]])
                    ->first();

          if ($userSocial) {
            $user = Sentinel::findById($userSocial->user_id);
            Sentinel::login($user);

            $persistences = $user->persistences->first();

            $data = redirect()->route('admin-dashboard');

          } else {
            $this->message = trans('auth.failed');
            $this->alertClass = 'alert-danger';
            $data = redirect()->route('login');
          }

        } else {
          $persistences = $user->persistences->first();
          if ($user->inRole('corporate-customer') || $user->inRole('business-partner') || $user->inRole('super-admin') || $user->inRole('standard-customer')) {

            $password_encrypted = baseEncrypt($credentials['password']);
            cookie()->queue('token', $persistences->code, 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
            cookie()->queue('password', $password_encrypted, 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
            cookie()->queue('login_type', 'normaly', 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
            cookie()->queue('social_type', 'normaly', 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
            cookie()->queue('email', baseEncrypt($user->email), 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
          }

          $data = redirect()->route('admin-dashboard');
        }

        return $data;
      }, function(\Exception $e) {

        if ($e instanceof \Cartalyst\Sentinel\Checkpoints\NotActivatedException) {
          $this->statusCode = 401;
        } else if ($e instanceof \Cartalyst\Sentinel\Checkpoints\ThrottlingException) {
          $this->statusCode = 429;
        } else {
          $this->statusCode = 400;
        }
        return redirect()->route('login');
      });

      return $prosesing->getCallback();
    }

    public function ceateDataRelated($request,$role_slug)
    {
        $return = false;
        if ($role_slug == 'standard-customer') {
            $model = StandardCustomer::class;

            $checkData = new StandardCustomer();
            $checkData->name = @$request->first_name.' '.@$request->last_name;
            $checkData->address = @$request->address_1;
            $checkData->save();

            $return['data'] = $checkData;
            $return['class'] = $model;
        } else if ($role_slug == 'business-partner') {
            $model = BusinessPartner::class;

            $checkData = new BusinessPartner();
            $checkData->name = @$request->company_name;
            $checkData->address = @$request->company_address;
            $checkData->phone_number = @$request->phone_number;
            $checkData->no_regis = @$request->company_register_no;
            $checkData->save();

            $return['data'] = $checkData;
            $return['class'] = $model;

        } else if ($role_slug == 'corporate-customer') {
            $model = CorporateCustomer::class;

            $checkData = new CorporateCustomer();
            $checkData->name = $request->company_name;
            $checkData->address = $request->company_address;
            $checkData->phone_number = @$request->phone_number;
            $checkData->no_regis = @$request->company_register_no;
            $checkData->save();

            $return['data'] = $checkData;
            $return['class'] = $model;
        }

        return $return;
    }

    public function registerBusiness()
    {
      $registers = RegisterPage::all();
      return view( 'layouts.register_business', compact('registers') );
    }

    public function apiRegisterSocial(Request $request)
    {
        \DB::beginTransaction();
        try
        {
            $this->statusCode = 200;
            $role_slug = get_role_slug($request->customer_group_id);

            if (!@$request->email) {
                $modelUser = new User();
                $request->email = $modelUser->generateEmail($request->nickname);
            }

            $dataUser = User::where('email','=',$request->email)->first();

            if ($dataUser) {
                $cekSocial = Social::where('user_id', '=', $dataUser->id)
                            ->where('provider','=',strtolower($request->provider))
                            ->first();

                if (!$cekSocial) {
                    $socialData = new Social;
                    $socialData->user_id = $dataUser->id;
                    $socialData->social_id = $request->social_id;
                    $socialData->provider = strtolower($request->provider);
                    $socialData->save();
                } else {
                    $cekSocial->social_id = $request->social_id;
                    $cekSocial->provider = strtolower($request->provider);
                    $cekSocial->save();
                }
                \DB::commit();
                return $this->loginFunc($dataUser, Config::get('services.static_password'));

                // $userLogin = Sentinel::login($dataUser);

                // $persistences = $userLogin->persistences->first();

                // $token = $persistences->code;

                // $result = new ApiResponse([
                //     'success' => true,
                //     'message' => trans('auth.success'),
                //     'code' => $this->statusCode
                // ], $this->mapUser($userLogin, $token));

                // return response()->json($result,200);
            } else {
                $sameSocialId = Social::where('social_id', '=', $request->social_id)
                                ->where('provider', '=', strtolower($request->provider))
                                ->first();

                if (!$sameSocialId) {

                    //Create new user
                    $name = explode(' ', $request->name);

                    if (count($name) >= 1) {
                        $request->merge(['first_name' => $name[0]]);
                    }

                    if (count($name) >= 2) {
                        $request->merge(['last_name' => $name[1]]);
                    }

                    // Register to Moodle
                    $data_post_register_moodle = $this->registerMoodle($request,'normaly','normaly',Config::get('services.static_password'));

                    if (!empty($data_post_register_moodle)) {
                        if (!is_array($data_post_register_moodle) && property_exists($data_post_register_moodle, 'exception')) {
                            $result = new ApiResponse([
                                'success' => false,
                                'message' => (empty($data_post_register_moodle->debuginfo) ? 'On Course :'.$data_post_register_moodle->message:'On Course: '.@$data_post_register_moodle->debuginfo),
                                'code' => 400
                            ], (object)[]);
                            return response()->json($result,200);
                        }
                    }

                    //End Register to Moodle

                    // Register API OC
                    $data_post_register = $this->registerOC($request,'normaly','normaly',Config::get('services.static_password'));

                    if (property_exists($data_post_register, "success") && empty($data_post_register->success)) {
                        $result = new ApiResponse([
                            'success' => false,
                            'message' => (empty($data_post_register->error->warning) ?'On Store: Something went wrong' : 'On Store: '.$data_post_register->error->warning),
                            'code' => 400
                        ], (object)[]);
                        return response()->json($result,200);
                    }

                    // END REGISTER OC

                    //doCreateDataRelated
                    $related = $this->ceateDataRelated($request,$role_slug);
                    // Register
                    $request->merge(['customer_id' => $data_post_register->data->customer_id]);
                    $userIdentity = [
                        "email" => $request->email,
                        "first_name"=> @$request->first_name ? $request->first_name : '',
                        "last_name"=> @$request->last_name ? $request->last_name : '',
                        "password"=> Config::get('services.static_password'),
                        "customer_id"=> $request->customer_id
                    ];

                    $dataUser = Sentinel::registerAndActivate($userIdentity);

                    if (!$dataUser) {
                        // modify fadly .start here
                        $result = new ApiResponse([
                            'success' => false,
                            'message' => 'Something went wrong',
                            'code' => 400
                        ], (object)[]);
                        return response()->json($result,200);
                        // modify fadly .end here
                    }

                    $socialData = new Social;
                    $socialData->user_id = $dataUser->id;
                    $socialData->social_id = $request->social_id;
                    $socialData->provider = strtolower($request->provider);
                    $socialData->save();

                    if ($related) {
                        $dataUser->related_id = $related['data']->id;
                        $dataUser->related_type = $related['class'];
                        $dataUser->save();
                    }

                    // Attach user to role
                    $role = Sentinel::findRoleBySlug($role_slug);
                    if (!$role) {
                        // modify fadly .start here
                        $result = new ApiResponse([
                            'success' => false,
                            'message' => 'Role not found',
                            'code' => 400
                        ], (object)[]);
                        return response()->json($result,200);
                        // modify fadly .end here
                    }
                    $role->users()->attach($dataUser);

                    // Create log role
                    // if ($role_slug != 'business-partner') {
                      $storeLogRole = LogRole::create(['user_id' => $dataUser->id, 'username' => $dataUser->email, 'password' => Config::get('services.static_password'), 'default' => 1, 'active' => 1]);
                    // }
                    \DB::commit();
                    return $this->loginFunc($dataUser, Config::get('services.static_password'));
                    // $userLogin = Sentinel::login($dataUser);

                    // $persistences = $userLogin->persistences->first();

                    // $token = $persistences->code;

                    // $result = new ApiResponse([
                    //     'success' => true,
                    //     'message' => trans('auth.success'),
                    //     'code' => $this->statusCode
                    // ], $this->mapUser($userLogin, $token));

                    // return response()->json($result,200);

                } else {
                    $sameSocialId->social_id = $request->social_id;
                    $sameSocialId->provider = strtolower($request->provider);
                    $sameSocialId->save();
                    $dataUser = $sameSocialId->user;
                    \DB::commit();
                    return $this->loginFunc($dataUser, Config::get('services.static_password'));

                    // $userLogin = Sentinel::login($dataUser);

                    // $persistences = $userLogin->persistences->first();

                    // $token = $persistences->code;

                    // $result = new ApiResponse([
                    //     'success' => true,
                    //     'message' => trans('auth.success'),
                    //     'code' => $this->statusCode
                    // ], $this->mapUser($userLogin, $token));

                    // return response()->json($result,200);
                }
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $result = new ApiResponse([
                'success' => false,
                'message' => $e->getMessage().' '.$e->getLine(),
                'code' => 500
            ], (object)[]);

            return response()->json($result,200);
        }
    }

    protected function loginFunc($dataUser,$password)
    {
        $userLogin = Sentinel::login($dataUser);

        $persistences = $userLogin->persistences->first();

        $token = $persistences->code;

        // login OC
        $client = new Client();
        $loginOC = $client->request('POST', env('OC_BASE_URL').'/index.php?route=rest/login/login', [
          'headers' => [
            'X-Oc-Merchant-Id' => 12345,
          ],
          'body' => json_encode(['email' => $dataUser->email, 'password' => $password])
        ]);

        $resultLoginOC = json_decode($loginOC->getBody());

	//Added by puw on 26-03-2018
	    $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,env('OC_BASE_URL').'/index.php?route=rest/login/login');
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Oc-Merchant-Id:12345'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,array('email'=>$dataUser->email,'password'=>$password));

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $server_output = curl_exec ($ch);

            curl_close ($ch);

            $curl_result=json_decode($server_output);

	$resultLoginOC=$curl_result;

        if ($resultLoginOC->success) {
          //get token moodle
          $request = new Request;
          $request->merge(['email',$userLogin->email, 'password' => $password]);
          $tokenMoodle = $this->getMoodleToken($request);

          $login_url_moodle=$this->getCourseLoginUrl($userLogin->email);

          $result = new ApiResponse([
              'success' => true,
              'message' => trans('auth.success'),
              'code' => 200
          ], $this->mapUserWithMoodle($userLogin, $token,$login_url_moodle, $resultLoginOC->data->session, baseEncrypt(Config::get('services.static_password'))));
        } else {
          $result = new ApiResponse([
              'success' => false,
              'message' => $resultLoginOC->error->warning,
              'code' => 400
          ], (object)[]);
        }

        return response()->json($result,200);
    }

    public function getMoodleToken($request)
    {
      $client = new Client();
      $getTokenMoodle = $client->request('POST', env('BASE_URL_MOODLE').'/login/token.php', [
        'form_params' => ['username' => $request->email, 'password' =>$request->password , 'service' => 'moodle_mobile_app']
      ]);
      $resultGetTokenMoodle = json_decode($getTokenMoodle->getBody());
      $tokenMoodle = (@$resultGetTokenMoodle->token) ? $resultGetTokenMoodle->token : '';

      log::info('token_moodle: '.$tokenMoodle);

      return $tokenMoodle;
    }

    public function getMoodleUserId($email)
    {
      $userMoodleId = 0;
      $userModel = new User;
      $userMoodle = $userModel->getUserInfoMoodle($email);
      if (@$userMoodle[0]) {
        $userMoodleId = $userMoodle[0]->id;
      }

      return $userMoodleId;
    }

    public function getCourseLoginUrl($data_email)
    {
      $client = new Client();

      $path= '&wantsurl=' .env('APP_URL');
      $response = $client->request('POST', env('MOODLE_BASE_URL_API').'/webservice/rest/server.php', [
      'form_params' => ['wstoken'=>env('MOODLE_TOKEN_USERKEY'),'wsfunction' => 'auth_userkey_request_login_url','moodlewsrestformat'=>'json','user'=>['username'=>$data_email]]
      ]);

      $result=json_decode($response->getBody());

      $login_url  =(@$result->loginurl)?$result->loginurl:'';

      return ($login_url=='')?'/':$login_url.$path;
    }

}
