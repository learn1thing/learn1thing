<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Auth\Api\AuthenticateController;
use App\Http\Controllers\Controller;
use App\Models\RoleUser;
use App\Models\Social;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Sentinel;
use Session;
use Socialite;
use DB;

class SocialController extends Controller
{
    /**
     * Display a listing of the resource.
     * @author ali
     * @param string $provider 
     * @return \Illuminate\Http\Response
     */
    public function getSocialRedirect( $provider )
    {
        $providerKey = Config::get('services.'. $provider);

        if (empty($providerKey)) {
            return view('page.status')
                ->with('error','No such provider');
        }

        return Socialite::driver($provider)->redirect();
    }

    /**
     * Display a listing of the resource.
     * @author ali
     * @param string $provider 
     * @return \Illuminate\Http\Response
     */

    public function getSocialHandleBackup( $provider )
    {
        try {
            $user = Socialite::driver( $provider )->user();
            
            $socialUser = null;

            //Check is this email present
            $userCheck = User::where('email','=',$user->email)->first();


            if (!$user->email) {
                Session::flash('message', ' Email not found !'); 
                Session::flash('alert-class', 'alert-danger'); 
                return redirect('/login');
            } else {
                $email = $user->email;
            }


            if (!empty($userCheck)) {
                $socialUser = $userCheck;

                $cekSocial = Social::where('user_id', '=', $socialUser->id)
                            ->first();
                            
                if (!$cekSocial) {
                    $socialData = new Social;
                    $socialData->user_id = $socialUser->id;
                    $socialData->social_id = $user->id;
                    $socialData->provider = $provider;
                    $socialData->save();
                }

            } else {
                $sameSocialId = Social::where('social_id', '=', $user->id)
                                ->where('provider', '=', $provider)
                                ->first();
                if (empty($sameSocialId)) {
                    
                    //Create new user

                    $name = explode(' ', $user->name);

                    if (count($name) >= 1) {
                        $first_name = $name[0];
                    }

                    if (count($name) >= 2) {
                        $last_name = $name[1];
                    }
                    
                    $userIdentity = [
                        "email" => $email,
                        "first_name"=> empty($first_name) ? null : $first_name,
                        "last_name"=> empty($last_name) ? null : $last_name,
                        "password"=> $user->id,
                    ];

                    $userActivation = Sentinel::registerAndActivate($userIdentity);

                    $socialData = new Social;
                    $socialData->user_id = $userActivation->id;
                    $socialData->social_id = $user->id;
                    $socialData->provider = $provider;
                    $socialData->save();

                    $socialUser = $userActivation;
                } else {
                    $socialUser = $sameSocialId->user;
                }
            }
            $roleUser = RoleUser::where('user_id',$socialUser->id);
            if ($roleUser->exists()) {
                $credentials = [
              ];
                $data = [
                            'social' => strtoupper($provider),
                            'email'    => $socialUser->email,
                            'password' => $user->id,
                            'avatar' => $user->avatar
                        ];
                return view('auth.login-social',$data);
            } else {
                return \Redirect::route('auth-register',['social-media',$socialUser->id]);
            }

        } catch (\Exception $e) {
            // dump($e);exit;
            Session::flash('message', strtoupper($provider).' DENY PERMISSION !'); 
            Session::flash('alert-class', 'alert-danger'); 
            return redirect('/login');
        }

    }


    public function getSocialHandle( $provider )
    {
        try {
            DB::beginTransaction();
                $dataUser = null;

                $user = Socialite::driver( $provider )->user();

                if (!$user->email) {
                    $modelUser = new User();
                    $user->email = $modelUser->generateEmail($user->nickname);
                }

                $dataUser = User::where('email','=',$user->email)->first();
                $email = $user->email;
                
                if (!empty($dataUser)) {
                    $cekSocial = Social::where('user_id', '=', $dataUser->id)
                                ->where('provider','=',$provider)
                                ->first();
                                
                    if (!$cekSocial) {
                        $socialData = new Social;
                        $socialData->user_id = $dataUser->id;
                        $socialData->social_id = $user->id;
                        $socialData->provider = $provider;
                        $socialData->save();
                    } else {
                        $cekSocial->social_id = $user->id;
                        $cekSocial->provider = $provider;
                        $cekSocial->save();
                    }

                } else {
                    $sameSocialId = Social::where('social_id', '=', $user->id)
                                    ->where('provider', '=', $provider)
                                    ->first();
                    if (empty($sameSocialId)) {
                        
                        //Create new user

                        $name = explode(' ', $user->name);

                        if (count($name) >= 1) {
                            $first_name = $name[0];
                        }

                        if (count($name) >= 2) {
                            $last_name = $name[1];
                        }
                        
                        $userIdentity = [
                            "email" => $email,
                            "first_name"=> empty($first_name) ? '' : $first_name,
                            "last_name"=> empty($last_name) ? '' : $last_name,
                            "password"=> Config::get('services.static_password')//$user->id,
                        ];

                        $dataUser = Sentinel::register($userIdentity);

                        $activation = \Activation::create($dataUser);

                        $socialData = new Social;
                        $socialData->user_id = $dataUser->id;
                        $socialData->social_id = $user->id;
                        $socialData->provider = $provider;
                        $socialData->save();

                    } else {
                        $sameSocialId->social_id = $user->id;
                        $sameSocialId->provider = $provider;
                        $sameSocialId->save();
                        $dataUser = $sameSocialId->user;
                    }
                }
                $roleUser = RoleUser::where('user_id',$dataUser->id);
        
            DB::commit();

            if ($roleUser->exists()) {
                $data = [
                            'social'        => strtoupper($provider),
                            'email'         => $dataUser->email,
                            'password'      => Config::get('services.static_password'),
                            'avatar'        => $user->avatar,
                            'login_type'    => 'social-media'
                        ];
                return view('auth.login-social',$data);
            } else {
                return \Redirect::route('auth-register',['social-media',$dataUser->id]);
            }

        } catch (\Exception $e) {
            info($e->getMessage());
            info($e->getLine());
            info($e->getFile());
            DB::rollback();
            Session::flash('message', strtoupper($provider).' DENY PERMISSION !'); 
            Session::flash('alert-class', 'alert-danger'); 
            return redirect('/login');
        }
    }

    /**
     * Display a listing of the resource.
     * @author ali
     * @param string $provider 
     * @return \Illuminate\Http\Response
     */

}
