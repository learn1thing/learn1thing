<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Mail\UserConfirmationMail;

use Sentinel;
use Mail;
use DB;

class RegisterController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransactions();
        try {
            $user = Sentinel::register( $request->all() );

            if ($user) {

                if ($request->customer_group_id == 1) {
                    $partner = 'standard-customer';
                } elseif ($request->customer_group_id == 2) {
                    $partner = 'corporate-customer';
                } else {
                    $partner = 'business-partner';
                }

                // Attach user to role
                $role = Sentinel::findRoleBySlug($partner);
                if (!$role) {
                    // modify fadly .start here
                    $result = new ApiResponse([
                        'success' => false,
                        'message' => 'Role not found',
                        'code' => 400
                    ], (object)[]);
                    return response()->json($result,200);
                    // modify fadly .end here
                }
                $role->users()->attach($user);

                if ($partner == 'business-partner') {
                    $businesspartner = new BusinessPartnerGroup;
                    $businesspartner->name = 'Coach';
                    $businesspartner->description = 'Default coach group';
                    $businesspartner->created_by = $user->id;
                    $businesspartner->save();

                    // ADD NEW ROLE //
                    $role = new Role;
                    $role->name = $businesspartner->name;
                    $role->business_partner_group_id = $businesspartner->id;
                    $role->save();

                    $roleGroup = Sentinel::findRoleById($role->id);
                    // $roleGroup->addPermission('dashboard', true);
                    // Automatic add permission for coach
                    $roleGroup->permissions = [
                        'dashboard' => true,
                        'manage-course' => true,
                        'manage-schedule' => true,
                        'manage-coach' => true,
                        'manage-profile' => true,
                    ];
                    // End Automatic add permission for coach
                    $roleGroup->save();
                }

                 // End register API OC #iddan
                if ($request->customer_group_id == 2 || $request->customer_group_id == 4) {
                   
                    $activation = Activation::create( $user );

                }
            }

            DB::commit();
            sessionFlash('Register Success.','success');
            return redirect()->route( 'login' );

        } catch (Exception $e) {

            DB::rollback();
            return redirect()->back()->withInput();

        }
    }

    public function confirmation($id, $code)
    {
        $user = Sentinel::find( $id );
        if ( Activation::complate( $user, $code ) ) { 

            $mail = Mail::to( $user->email )
            ->send(new UserConfirmationMail($activation));  
            sessionFlash($user->email.' is actived.','success');
            return redirect()->back(); 
        } else { 

            sessionFlash($user->email.' Expired Token.','errors');
            return redirect()->back();
        }
    }
}
