<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Classes\CurlRequest;

use App\Mail\UserConfirmationMail;
use App\Models\LogRole;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function activationUser($data)
    {
        if ($data) {
            $user = $data->user;

            $activation = \Activation::exists($user);
            if ($activation) {
                \Activation::complete($user, $activation->code);
                if ($activation = \Activation::completed($user)) {
                    
                    $mail = \Mail::to( $user->email )->send(new UserConfirmationMail($user)); 

                    $result['result'] = 'success';
                    $result['message'] = 'User has been activated';
                } else {
                    $result['result'] = 'failed';
                    $result['message'] = 'User failed activated';
                }
            } else {
                $result['result'] = 'failed';
                $result['message'] = 'User failed activated';
            }
        } else {
            $result['result'] = 'failed';
            $result['message'] = 'User not found';
        }

        return response()->json($result,200);

    }

    /**
     * Save user info after login
     * @param  Cartalyst\Sentinel\Users\EloquentUser $persistences
     * @param  string $ipAddress
     * @return void
     */
    public function saveUserInfo($persistences, $ipAddress)
    {
      $persistences->ip_address = $ipAddress;
      $persistences->browser = \BrowserDetect::browserFamily();
      $persistences->save();
    }

    /**
     * Get token from request
     * @param  \Illuminate\Http\Request $request
     * @return string
     */
    public function getTokenFromReqeust($request)
    {
        info('masuk sini');
      return $request->get('token') ? $request->get('token') : $request->header('Authorization');
    }

    /**
     * Get User base on persitance code
     * @param  string $persistanceCode
     * @return Sentinel
     */
    public function getUserLogin($persistanceCode)
    {
      return \Sentinel::findByPersistenceCode($persistanceCode);
    }

    /**
     * Proses when logout user success
     * @param  string $token
     * @return void
     */
    public function onLogout($token)
    {
      $client = new \GuzzleHttp\Client();
      $urlAction = env('OC_BASE_URL').'/index.php?route=rest/logout/logout';
      $response = $client->request('POST', $urlAction, [
        'headers' => ['X-Oc-Merchant-Id' => '12345'],
        'token'   => $token
      ]);
      info('Logout to oc');
      info($response->getBody());
    }

    public function updateUserOcMoodle($udpatedUser,$request,$type)
    {
        $this->user = new \App\Models\User;
        // update profile Moodle
        $userMoodle = $this->user->getUserInfoMoodle($udpatedUser->email);
        if (@$userMoodle[0]) {
            $updateUserMoodle = $this->user->updateUserMoodle($request,$userMoodle[0]->id,$type);
            info('success update profile Moodle');
        }

        if ($type == 'update-profile') {
            $body = json_encode([
                "firstname" => $request->first_name,
                "lastname" => $request->last_name,
                "address" => [
                    [
                        "firstname" => $request->first_name,
                        "lastname" => $request->last_name,
                        "address_1" => $request->address_1,
                        "address_2" => $request->address_2,
                        "city" => $request->city,
                        "country_id" => $request->country,
                        "zone_id" => $request->region,
                        "postcode" => $request->post_code,
                        "default" => "1"
                    ]
                ]
            ]);
            
        } else {
            $body = json_encode([
                "firstname" => $request->first_name,
                "lastname" => $request->last_name,
                "password" => $request->password,
                "confirm" => $request->password,
                "address" => [
                    [
                        "firstname" => $request->first_name,
                        "lastname" => $request->last_name,
                        "address_1" => $request->address_1,
                        "address_2" => $request->address_2,
                        "city" => $request->city,
                        "country_id" => $request->country,
                        "zone_id" => $request->region,
                        "postcode" => $request->post_code,
                        "default" => "1"
                    ]
                ]
            ]);
        }

        // update profile OC
        if ($udpatedUser->customer_id) {
            $responses = CurlRequest::sendRequest('PUT', env('OC_BASE_URL').'/index.php?route=rest/customer_admin/customers&id='.$udpatedUser->customer_id,null,$body);
            if (@$responses->success ) {
                info('success update profile OC');
            }
        }

        return true;
    }

    public function hasAccessPermission($data)
    {
        if (!user_info()->inRole('super-admin')) {
            if (user_info('parent_id')) {
                if ($data->id != user_info('id')) {
                    return true;
                }
            } else {
                $childs = user_info()->downlines()->pluck('id')->toArray();
                if (count($childs) > 0) {
                    if (!in_array($data->id, $childs) && $data->id != user_info('id')) {
                        return true;
                    }
                } else {
                    if (user_info('id') != $data->id) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public function updatePasswordLogUser($username, $password)
    {
        // update log user
        LogRole::where('username',$username)
                ->update(['password' => $password]);

        return true;
    }
}
