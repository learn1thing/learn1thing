<?php

namespace App\Http\Controllers\Api\v1;

use App\Classes\ApiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\User;
use App\Http\Requests\Api\v1\DepartmentUserRequest;

class DepartmentController extends Controller
{
  public function index()
  {
  	try {
      $departments = Department::all();
      $data = new ApiResponse([
          'success' => true,
          'message' => 'Success get data departments',
          'code' => 200
      ], $departments);

  	} catch (Exception $e) {
  		$data = new ApiResponse([
        'success' => false,
        'message' => $e->getMessage(),
        'code' => 400
      ], []);
  	}
    return response()->json($data, 200);
  }

  public function customerValidation(DepartmentUserRequest $request)
  {
    try {
      $departmentId = $request->input('department_id');
      $customerIds = $request->input('customer_ids');
      // print_r($departmentId);
      // print_r($customerIds);
      // die();
      $customerIds = explode(',', $customerIds);
      $departments = User::where('department_id', $departmentId)
        ->whereIn('customer_id', $customerIds)
        ->get()
        ->toArray();
      $validCustomerIds = array_pluck($departments, 'customer_id');
      $data = new ApiResponse([
          'success' => true,
          'message' => 'Success customer validation',
          'code' => 200
      ], array('customer_ids' => implode(',', $validCustomerIds) ));

    } catch (Exception $e) {
      $data = new ApiResponse([
        'success' => false,
        'message' => $e->getMessage(),
        'code' => 400
      ], []);
    }
    return response()->json($data, 200);
  }
}
