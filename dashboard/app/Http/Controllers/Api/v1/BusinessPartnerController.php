<?php

namespace App\Http\Controllers\Api\v1;

use App\Classes\ApiResponse;
use App\Http\Controllers\Controller;
use App\Models\BusinessPartner;
use App\Models\Master\User;
use Illuminate\Http\Request;

class BusinessPartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BusinessPartner  $businessPartner
     * @return \Illuminate\Http\Response
     */
    public function show(BusinessPartner $businessPartner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BusinessPartner  $businessPartner
     * @return \Illuminate\Http\Response
     */
    public function edit(BusinessPartner $businessPartner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BusinessPartner  $businessPartner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BusinessPartner $businessPartner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BusinessPartner  $businessPartner
     * @return \Illuminate\Http\Response
     */
    public function destroy(BusinessPartner $businessPartner)
    {
        //
    }

    public function getListBusinessPartner()
    {
        $token = empty(\Request::header('authorization'))
                 ? \Request::header('token')
                 : \Request::header('authorization');

        $user = \Sentinel::findByPersistenceCode($token);
        
        if ($user) {

            $businessPartner = User::join('role_users','users.id','=','role_users.user_id')
                                ->join('roles','role_users.role_id','=','roles.id')
                                ->join('activations','activations.user_id','=','users.id')
                                ->select('users.id','users.first_name','users.last_name','activations.completed','users.email','users.created_at')
                                ->where('roles.slug','=','business-partner')
                                ->get();

            // $businessPartner = BusinessPartner::all(); 
            $returnBusiness = [];
            if ($businessPartner->count() > 0) {
                foreach ($businessPartner as $key => $value) {
                    $returnBusiness[] = [
                        'id_business_user' => $value->id,
                        'name' => $value->first_name.' ' .$value->last_name,
                    ];
                }
                $success = true;
                $status = 200;
                $message = "success";
            } else {
                $success = false;
                $status = 404;
                $message = "No data";
            }

            return $this->generateReturnArray($returnBusiness,$success,$message,$status);
        }

        $data = new ApiResponse([
            'success' => false,
            'message' => trans('unauthorized'),
            'code' => 401
        ], (object)[]);

        return response()->json($data, 401);

    }

    /**
     * Display a listing of the resource.
     * @author ali
     * @return \Illuminate\Http\Response
     */
    public function generateReturnArray($return,$success,$message,$status)
    {
        $data = new ApiResponse([
          'success' => $success,
          'message' => $message,
          'code' => $status
        ], $return);

        return response()->json($data, $status);
    }
}
