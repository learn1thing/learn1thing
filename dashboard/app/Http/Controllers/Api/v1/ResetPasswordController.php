<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Classes\ApiResponse;
use App\Models\User;

use Sentinel;

class ResetPasswordController extends Controller
{
	public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function reset(Request $request)
    {
    	$password = $request->new_password;
        $passwordConf = $request->password_confirmation;

        $user = User::where('email', $request->customer_id)->first();

        Sentinel::update($user, array('password' => $password));

        // update profile Moodle
        $userMoodle = $user->getUserInfoMoodle($user->email);
        if (@$userMoodle[0]) {
            $user->new_password = $password;
            $updateUserMoodle = $user->updateUserMoodle($user,$userMoodle[0]->id,'update-password');
            info('success update password Moodle');
        }

        $result = array();

        $data = new ApiResponse([
                'success' => true,
                'message' => 'Success Reset Password',
                'code' => 200
            ], $result); 

        return response()->json($data, 200); 

    }
}
