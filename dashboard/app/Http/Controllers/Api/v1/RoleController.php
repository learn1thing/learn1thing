<?php

namespace App\Http\Controllers\Api\v1;

use App\Classes\ApiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\RoleUser;
use App\Http\Requests\Api\v1\RoleUserRequest;

class RoleController extends Controller
{
  public function index()
  {
  	try {
      $departments = Role::all();
      $data = new ApiResponse([
          'success' => true,
          'message' => 'Success get data roles',
          'code' => 200
      ], $departments);

  	} catch (Exception $e) {
  		$data = new ApiResponse([
        'success' => false,
        'message' => $e->getMessage(),
        'code' => 400
      ], []);
  	}
    return response()->json($data, 200);
  }

  public function userValidation(RoleUserRequest $request)
  {
    try {
      $roleId = $request->input('role_id');
      $customerIds = $request->input('customer_ids');
      $customerIds = explode(',', $customerIds);
      $roleUsers = RoleUser::where('role_id', $roleId)
        ->whereHas('user', function ($query) use($customerIds)
        {
          $query->whereIn('customer_id', $customerIds);
        })
        ->with('user')
        ->get()
        ->toArray();
      $validCustomerIds = array_pluck($roleUsers, 'user.customer_id');
      $data = new ApiResponse([
          'success' => true,
          'message' => 'Success customer validation',
          'code' => 200
      ], array('customer_ids' => implode(',', $validCustomerIds)));

    } catch (Exception $e) {
      $data = new ApiResponse([
        'success' => false,
        'message' => $e->getMessage(),
        'code' => 400
      ], []);
    }
    return response()->json($data, 200);
  }

}
