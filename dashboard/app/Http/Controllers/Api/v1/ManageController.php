<?php
namespace App\Http\Controllers\Api\v1;

use Cookie;
use App\Classes\TryCatch;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\LogRole;
use BrowserDetect;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Sentinel;
use GuzzleHttp\Client;
use App\Http\Controllers\Auth\Api\AuthenticateController;
use Illuminate\Support\Facades\Log;
use App\Classes\ApiResponse;

class ManageController extends Controller
{
		use AuthenticatesUsers;

    protected $redirectTo = '/home';
    public $alertClass;
    public $message;
    public $statusCode;
    protected $persistences;
    protected $token;
    protected $email;
    protected $login_type;
    protected $social_type;

    protected $isCreateCookie;


	  public function __construct()
    {
    	
    }

    /**
     * [loginByEmail description]
     * @param  [type] $email [description]
     * @return [type]        [description]
     */
    
    public function logRole($email)
    {
    	 
    	 $credentials = [
			    'login' => baseDecrypt(Cookie::get('email')),
			];

			$user = Sentinel::findByCredentials($credentials);

    	 //dd($user);
        if (!session()->has('email_refer')) {
            session()->put('email_refer',$user->email);
        }
        $this->onLogout($user->persistences->first()->code);
        Sentinel::logout();

        Cookie::forget('token');
        Cookie::forget('email');
        Cookie::forget('password');
        Cookie::forget('token_moodle');
        session()->forget('token_moodle');

        // $client = new Client();
        // $client->request('GET', 'http://store.learn1thing.com/course/login/logout.php', []);

        //redirect(env('MOODLE_BASE_URL_API').'/login/logout.php');
        return redirect(env('MOODLE_BASE_URL_API').'/auth/userkey/logout.php?ismanage=1&email='.$email);
    }

    public function loginByEmail($email)
    {
    	try {
  				$message='';
          $status=true;
          $status_code=200;
          

	        $redirect_url;
		    	$AuthController=new AuthenticateController;
		    	$redirect_url=$AuthController->getCourseLoginUrl($email);

		    	if ($redirect_url=='/') {
		    		$message='redirect url not found';
		    		$status_code=404;
		    	}

		    	$log=LogRole::where('username',$email)->first();

		    	$login_type='';
		    	$social_type='';
		    	$tokenMoodle='';

		    	$credentials=[
		    		'email'=>$log->username,
		    		'password'=>$log->password,
		    		'remember'=>false
		    	];

		    	$result=array();

		    	$user=Sentinel::authenticate($credentials,$credentials['remember']);
		    	
		    	$this->login_type='normally';
		    	$this->social_type='normally';

		    	if (!$user) {
		    		$message='User not found';
		    		$status_code='404';
		    	}else{
		    		$persistences=$user->persistences->first();

		    		$this->isCreateCookie=true;
		    		$token=$persistences->code;
		    		$this->token=$token;
		    		$this->email=$user->email;

		    		$data=$user;

		    		if ($data->status_login==0) {
		    			$data->status_login=1;
		    			$data->save();
		    		}elseif ($data->status_login==1) {
		    			$data->status_login=2;
		    			$data->save();
		    		}

		    		//Get Token moodle
		    		$AuthAPI=new AuthenticateController;
		    		$request=new Request;
		    		$request->merge(['email'=>$log->username,'password'=>$log->password]);

		    		$tokenMoodle=$AuthAPI->getMoodleToken($request);

		    		if ($this->isCreateCookie) {
		            $token = Cookie::queue('token', $this->token, 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
		            $password = Cookie::queue('password', baseEncrypt($credentials['password']), 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
		            $login_type = Cookie::queue('login_type', $this->login_type, 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
		            $social_type = Cookie::queue('social_type', $this->social_type, 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
		            $addEmail = Cookie::queue('email', baseEncrypt($this->email), 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
		            $addTokenMoodle = Cookie::queue('token_moodle', $tokenMoodle, 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
		        }
		    	}

		    	$result=array(
		    		'redirect_url'=>$redirect_url
		    	);


		    	$data = new ApiResponse([
                'success' => true,
                'message' => $message,
                'code' => $status_code
            ],$result);

          return response()->json($data, $status_code);

    	} catch (Exception $e) {
    		$data = new ApiResponse([
                'success' => false,
                'message' => $e,
                'code' => 404
            ], (object)[]);
          return response()->json($data, 404);
    	}

	    	
    }
}
