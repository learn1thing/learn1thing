<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use App\Classes\CurlRequest;
use App\Classes\ApiResponse;
use App\Models\Product;
use App\Models\ProductSet;
use App\Models\User;
use App\Models\CourseProfiler;
use Validator;
use Cookie;
use Session;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->product = new Product;
        $this->user = new User;
        $this->productAssigned = new \App\Models\ProductAssign;
    }

    public function getProductById($id)
    {
        $product = $this->product->findProduct($id);
        if ($product) {
            $response = [
                'product_id' => $product->product_code,
                'model' => $product->model,
                'email' => $product->user->email,
                'user_name' => $product->user->first_name.' '.$product->user->last_name,
                'type' => $product->type
            ];
            $result = new ApiResponse([
                'success' => true,
                'message' => 'Success get product',
                'code' => 200
            ], $response);
        } else {
            $result = new ApiResponse([
                'success' => false,
                'message' => 'Product not found',
                'code' => 400
            ], (object)[]);
        }

        return response()->json($result,200);
    }

    public function getProduct(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // 'type' => 'required',
            'email' => 'required',
        ]);

        if($validator->fails()){
            $messages = $validator->errors();
            if ($messages->has('type') && $messages->has('email')) {
                $result = new ApiResponse([
                    'success' => false,
                    'message' => 'The type and email field is required',
                    'code' => 400
                ], []);
            } elseif ($messages->has('type')) {
                $result = new ApiResponse([
                    'success' => false,
                    'message' => $messages->first('type'),
                    'code' => 400
                ], []);
            } elseif ($messages->has('email')) {
                $result = new ApiResponse([
                    'success' => false,
                    'message' => $messages->first('email'),
                    'code' => 400
                ], []);
            }
        } else {
            // $findUser = $this->user->where('email', $request->email)->first();
            // if($findUser){
            //     $banner = url_to_image(@$findUser->related->banner, 'banner');
            // } else {
            //     $banner = "";
            // }
            $data = \Sentinel::findByCredentials(['login' => $request->email]);
            if ($data) {
                $company['name'] = (@$data->related->name) ? $data->related->name : 'Unknown';
                $company['adress'] = (@$data->related->address) ? $data->related->address : '';
                $company['phone_number'] = (@$data->related->phone_number) ? $data->related->phone_number : '';
                $company['no_register'] = (@$data->related->no_regis) ? $data->related->no_regis : '';
                
                $productsTemp = $this->product->getProductsForStore(@$request->type,$request->email);
                
                $productsSend = $this->productAssigned->join('products','products.product_code','=','product_assigns.product_id')
                    ->whereNotIn('product_id',$productsTemp->pluck('products.product_code')->toArray())
                    ->where('product_assigns.business_partner_id',$data->related_id)
                    ->select('products.product_code as product_id','products.model','products.type')
                    ->union($productsTemp->select('products.product_code as product_id','products.model','products.type'))
                    ->get();
                
                $data->avatar = (@$data->image) ? url_to_image($data->image):link_to_avatar();
                $data->banner = url_to_image(@$data->related->banner, 'banner');
                $data->company = $company;
                $data->items = $productsSend;
                if (count($productsSend) > 0) {
                    $result = new ApiResponse([
                        'success' => true,
                        'message' => 'Product available',
                        'code' => 200
                    ], $data);
                } else {
                    $result = new ApiResponse([
                        'success' => false,
                        'message' => 'Product is empty',
                        'code' => 400
                    ], []);
                }
            } else {
                $result = new ApiResponse([
                    'success' => false,
                    'message' => 'User not found',
                    'code' => 400
                ], []);
            }
        }

        return response()->json($result,200);
    }

    public function checkProduct(Request $request)
    {
        $product = Product::where('product_code',$request->product_id)->first();
        if (count($product) > 0) {
            $product->image = get_file($product->image,'compressed');
            $product_sets = $product->productSets;
            if (count($product_sets) > 0) {
                foreach ($product_sets as $key => $value) {
                    if ($value->course_profiles) {
                        $decodeCoachId = json_decode($value->course_profiles->coach_id);
                        if ($decodeCoachId[0] == 0000 || count($decodeCoachId) == 0 ) {
                            unset($value->course_profiles);
                            $value->course_profiles = null;
                        } else {
                            $value->course_profiles->coach_id = json_decode($value->course_profiles->coach_id);
                            $value->course_profiles->url_schedule = route('coach.scheduler',$value->course_profiles->course_id).'?product_id='.$request->product_id.'&code='.$request->code.'&type_order='.$request->type_order;
                        }
                        
                    }
                }
                $result = new ApiResponse([
                    'success' => true,
                    'message' => 'Product available',
                    'code' => 200
                ], $product);
            } else {
                $result = new ApiResponse([
                    'success' => false,
                    'message' => 'Product is empty',
                    'code' => 400
                ], []);
            }

        } else {
            $result = new ApiResponse([
                    'success' => false,
                    'message' => 'Product is empty',
                    'code' => 400
                ], []);
        }

        // session()->put('code_schedule', $request->code);
        // session()->save();
        // $code_schedule = Cookie::queue('code_schedule', $request->code, 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
        // return response()->json($result,200)->cookie(
        //                                             'code_schedule', $request->code, 300, '/', env('APP_SUB_DOMAIN', '.learn1thing.com'));
        return response()->json($result,200);
    }

}
