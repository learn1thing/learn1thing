<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\StandardCustomer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Http\Requests\Master\User\StandardCustomerRequest;
use App\Classes\CurlRequest;
use DB;
use Sentinel;
use GuzzleHttp\Client;
use App\Classes\ApiResponse;
use App\Http\Controllers\Auth\Api\AuthenticateController;
use App\Http\Requests\Master\User\ProfileRequest;

class UserController extends Controller
{
    public function __construct()
    {
        $this->user = new User;
    }
    public function updateUser(ProfileRequest $request)
    {
        try {
            $message='';
            $status=true;
            $status_code=200;

            $credentials = [
                'email' => $request->email,
            ];

            $user = Sentinel::findByCredentials($credentials);

            $user->process_workflow(null,null,null,$user);

            if (@user_info('payment_method')->name) {
                if (user_info('is_change_payment_method')) {
                    $message = 'Payment method cannot be change, you have been requested before';
                } else {
                    $request->merge(['is_change_payment_method' => true]);
                    $message = 'Request for change payment method has been sent';
                }
            }
            
            $param = $request->all();

            $data =  $this->UserUpdate($user->id,$param);
            if ($data) {

                // update profile OC
                $body = json_encode([
                    "firstname" => $request->first_name,
                    "lastname" => $request->last_name,
                    "newsletter" => $request->subscribe,
                    "address" => [
                        [
                            "firstname" => $request->first_name,
                            "lastname" => $request->last_name,
                            "address_1" => $request->address_1,
                            "address_2" => $request->address_2,
                            "city" => $request->city,
                            "country_id" => $request->country,
                            "zone_id" => (($request->country==188)?188:$request->region),
                            "postcode" => $request->post_code,
                            "default" => "1",
			    "zone_id"=>empty($request->region)?$request->country:$request->region
                        ]
                    ]
                ]);

                //$responses = CurlRequest::sendRequest('PUT', env('OC_BASE_URL').'/index.php?route=rest/customer_admin/customers&id='.$user->customer_id,null,$body);
		
		$ch = curl_init();

	        curl_setopt($ch, CURLOPT_URL,env('OC_BASE_URL').'/index.php?route=rest/customer_admin/customers&id='.$user->customer_id);
        	curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Oc-Restadmin-Id:12345'));
        	curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'PUT');
	        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	        curl_setopt($ch, CURLOPT_POSTFIELDS,$body);

        	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	        $server_output = curl_exec ($ch);

        	curl_close ($ch);

	        $curl_result=json_decode($server_output);
		$responses=$curl_result;                
//dd($responses);
                if (@$responses->success ) {
                    $status=true;
                    $status_code=200;
                }else{
                    $message='Fail update profile in opencart';
                    $status_code=401;
                    $status=false;
                }

                // update profile Moodle
               // if ($status) {
                    $userMoodle = $this->user->getUserInfoMoodle($user->email);
                    if (@$userMoodle[0]) {
                        $request->merge(['email' => $user->email]);
                        $updateUserMoodle = $this->user->updateUserMoodle($request,$userMoodle[0]->id);
                        $status=true;
                        $status_code=200;
                    }
                //}

            }

            $update_user = Sentinel::findByCredentials($credentials);

            $data = new ApiResponse([
                'success' => $status,
                'message' => ($status)?'Success update profile': $message,
                'code' => $status_code
            ],($status)?$update_user:(object)[]);

            return response()->json($data, $status_code);
        } catch (Exception $e) {
             $data = new ApiResponse([
                'success' => false,
                'message' => $e,
                'code' => 404
            ], (object)[]);
             return response()->json($data, 404);
        }
    }

    public function UserUpdate($id,$req)
    {
        $user=User::find($id);
        $user->first_name=$req['first_name'];
        $user->last_name=$req['last_name'];
        $user->email=$req['email'] ;
        $user->subscribe=$req['subscribe'] ;
        $user->address_1=$req['address_1'];
        $user->city=$req['city'] ;
        $user->post_code=$req['post_code'] ;
        $user->country=$req['country'];
	$user->region=$req['region'];

        if($user->save()){
            return $user;
        } else {
            return false;
        }
    }
}
