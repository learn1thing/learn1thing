<?php

namespace App\Http\Controllers\Api\v1;

use App\Classes\ApiResponse;
use App\Http\Controllers\Controller;
use App\Models\Master\User;
use Illuminate\Http\Request;

use App\Models\Order;
use App\Models\Coach;
use App\Models\Product;
use App\Models\ProductSet;
use App\Models\OrderHistory;
use App\Models\CoachScheduleOrder;
use App\Models\CoachSchedule;
use App\Models\Schedule;
use App\Models\CourseProfiler;

use App\Mail\InvoiceOrder;
use App\Mail\InvoiceOrderHistory;
use App\Classes\CurlRequest;
use DB;

class CoachController extends Controller
{

    public function __construct()
    {
        $this->model_courseProfile = new CourseProfiler();
        $this->model_schedule = new Schedule();
        $this->model_product = new ProductSet();
        $this->model_coach_schedule_order = new CoachScheduleOrder();
        $this->model_coach = new Coach();
    }

    /**
     * Display the specified resource.
     * @author ali
     * @param  int  $id_bussiness_partner
     * @return \Illuminate\Http\Response
     */

    public function getListCoach($email = false)
    {
        info('test');
        if (!$email) {
            $data = new ApiResponse([
                'success' => false,
                'message' => 'Email is required',
                'code' => 400
            ], []);
            return response()->json($data, 200);
        }
        // $token = empty(\Request::header('authorization'))
        //          ? \Request::header('token')
        //          : \Request::header('authorization');

        $user = \Sentinel::findByCredentials(['login' => $email]);
        // $result = User::join('business_partner_groups','business_partner_groups.created_by','=','users.id')
        //     ->select('users.id','users.first_name','users.last_name')
        //     // ->where('business_partner_groups.created_by',$user->id)
        //     ->where('users.parent_id',$user->id)
        //     ->get();
        $return = [];
        if ($user) {
            if ($user->parent_id) {
                $user = \Sentinel::findById($user->parent_id);
            }
            if (count($user->groups) > 0) {
                $x = 0;
                foreach ($user->groups as $group) {
                    if ($group->role->hasAccess('manage-coach')) {
                        $groupUsers = $group->users;
                        if (count($groupUsers) > 0) {
                            foreach ($groupUsers as $user) {
                                if ($user->email != $email) {
                                    $return[$x]['id_coach'] = $user->id;
                                    $return[$x]['email'] = $user->email;
                                    $return[$x]['name'] = $user->first_name.' '.$user->last_name;
                                    $x++;
                                }
                            }
                        }
                    }
                }
            }

            $data = new ApiResponse([
                'success' => true,
                'message' => 'Success get data coach',
                'code' => 200
            ], $return);
            // if ($email) {
            //     $arrayWhere = ['roles.name' => 'coach' , 'users.email' => $email];
            // } else {
            //     $arrayWhere = ['roles.name' => 'coach'];
            // };

            // $coach = User::join('role_users','users.id','=','role_users.user_id')
            //         ->join('roles','role_users.role_id','=','roles.id')
            //         ->select('users.id','users.first_name','users.last_name')
            //         ->get();

            // $returnCoach = [];
            // if ($coach->count() > 0) {
            //     foreach ($coach as $key => $value) {
            //         $returnCoach[] = [
            //             'id_coach' => $value->id,
            //             'name' => $value->first_name.' ' .$value->last_name,
            //         ];
            //     }
            //     $success = true;
            //     $status = 200;
            //     $message = "success";
            // } else {
            //     $success = false;
            //     $status = 404;
            //     $message = "data not found";
            // }

            // $scheduleCtrl = new BusinessPartnerController();

            // return $scheduleCtrl->generateReturnArray($returnCoach,$success,$message,$status);
        } else {

            $data = new ApiResponse([
                'success' => false,
                'message' => 'User not found',
                'code' => 400
            ], $return);

        }

        return response()->json($data, 200);
    }

    /**
     * Sends a mail.
     *
     * @param      \Illuminate\Http\Request  $request  The request
     */
    public function sendMail( Request $request )
    {
        try {
            $coachScheduelOrder = CoachScheduleOrder::where('order_id',$request->order_id)->get(['coach_id'])->groupBy('coach_id');
            if (count($coachScheduelOrder) > 0) {
                foreach ($coachScheduelOrder as $key => $value) {
                    $coach = Coach::find($key);
                    $result['user'] = $coach->user_coach;
                    $result['coach']  = $this->model_coach_schedule_order->getCoachScheduleOrderByCoachId($key,$request->order_id);

                    $orderCus = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/order_admin/orders&id='.$request->order_id);
                    $result['customer'] = $orderCus->data;

                    //added by puw on 01-02-2018
                    $result['data_products'] = $orderCus->data->products;
                    

                    $dataCourse = array(
                        'moodlewsrestformat'    => 'json',
                        'field'                 => 'id',
                        'value'                 => $coach->course_id,
                    );
                    $course = CurlRequest::requestMoodle('POST', 'core_course_get_courses_by_field', $dataCourse, env('TOKEN_GET_COURSE'));
                    $result['bbb_link'] = env('BASE_URL_MOODLE').'/course/view.php?name='.$course->courses[0]->shortname;
                    
                    $result['course_link'] = '';
                    $productScheduleOrder = CoachScheduleOrder::where('order_id',$request->order_id)
                                                ->where('coach_id',$key)
                                                ->get(['product_id'])
                                                ->groupBy('product_id');
                    info('product schedule order on send email');
                    
                    if (count($productScheduleOrder) > 0) {
                        foreach ($productScheduleOrder as $keyProd => $prod) {
                            $productSet = ProductSet::where('product_id',$keyProd)
                                    // ->where('course_id',$coach->course_id)
                                    ->first();
                            info('product set');
                            info(json_encode($productSet));
                            if ($productSet) {
                                $result['course_link'][] = $productSet->course_url;
                            }

                        }
                    }
                    info('send email to coach');
                    
                    $mail = \Mail::to( $coach->user_coach->email )->send(new InvoiceOrderHistory($result));

                    $return = [];
                    $data = new ApiResponse([
                            'success' => true,
                            'message' => 'Success send email to coach',
                            'code' => 200
                        ], $return); 

                    return response()->json($data, 200);
                }
            }
        } catch (Exception $e) {
            info($e->getMessage());
            info($e->getLine());
            info($e->getFile());
            $return = [];
            $data = new ApiResponse([
                    'success' => false,
                    'message' => 'failed send email to coach.'.' '.$e->getMessage().' '.$e->getFile().' '.$e->getLine(),
                    'code' => 400
                ], $return); 
            return response()->json($data, 200);
        }

        /*
        $result['user'] = User::where( 'email', $request->email )->first();
        $coach = new CoachScheduleOrder;
        $result['coach']  = $coach->getCoachScheduleOrderByEmail( $request->email, $request->order_id );
        $result['customer'] = $request->name_user;
        $result['link']     = json_decode($request['links']);
        $result['bbb_link']  = (@$request['bbb_link']) ? @$request['bbb_link'] : '';

        $mail = \Mail::to( $request->email )->send(new InvoiceOrderHistory($result));
        $return = [];
        $data = new ApiResponse([
                'success' => true,
                'message' => 'Success get data coach',
                'code' => 200
            ], $return); 

        return response()->json($data, 200);
        */        
    }

    public function ListCoach(Request $request)
    {
        $data = [];
        try {
            $course_id = $request->get('course_id');
            $coaches = $this->model_courseProfile->getCoachByCourseId($course_id);
            if (count($coaches) > 0) {
                foreach ($coaches as $key => $value) {
                    $value->photo = url_to_image($value->photo);
                }
            }
            $data['coaches'] = $coaches;
            $data['course_id'] = $course_id;

            $res = new ApiResponse([
                    'success' => true,
                    'message' => 'Success get data coach',
                    'code' => 200
                ], $data);     
        } catch (Exception $e) {
            $res = new ApiResponse([
                    'success' => false,
                    'message' => 'Failed get data coach',
                    'code' => 400
                ], $data); 
        }
        return response()->json($res, 200);
        
    }

    public function showScheduleCoach(Request $request)
    {
        try {

            $coach_id = $request->get('coach_id');
            $course_id = $request->get('course_id');
            $code = $request->get('code');
            $product_id = $request->get('product_id');

            $events = [];
            $return = [];
            $detailSchedule = array();
            $currentDate = date("Y-m-d");
            $firstDateMonth = date('Y-m-01');

            $product = Product::find($product_id);

            if ($request->type_order == 'corporate') {
                $checkOrderSchedule = CoachScheduleOrder::where('order_product_id',$request->order_product_id)
                                                    ->get();
            } else {
                $checkOrderSchedule = CoachScheduleOrder::where('order_id',$request->order_id)
                                                    ->where('order_id', '!=', null)
                                                    ->get();    
            }
            
            if (count($checkOrderSchedule) == 0) {

                $coach = Coach::find($coach_id);
                if ($coach) {
                    $coach->photo = url_to_image($coach->photo);

                    $scheduleBooked = CoachScheduleOrder::where('coach_id',$coach_id)
                                                        ->whereDate('start_date','>=',$firstDateMonth)
                                                        ->get();

                    if (count($scheduleBooked) > 0) {
                        foreach ($scheduleBooked as $key => $scheduleCust) {

                            if (date("Y-m-d",strtotime($scheduleCust->start_date)) >= $currentDate) {
                                $status = 'locked';
                            } else {
                                $status = 'locked-expired';
                            }

                            $coachSchedule['id'] = $scheduleCust->coach_schedule_id;
                            $coachSchedule['start_date'] = $scheduleCust->start_date;
                            $coachSchedule['end_date'] = $scheduleCust->end_date;
                            $coachSchedule['start_time'] = $scheduleCust->start_time;
                            $coachSchedule['end_time'] = $scheduleCust->end_time;
                            $coachSchedule['status'] = $status;

                            $detailSchedule[] = $coachSchedule;
                        }
                    }

                    $scheduleAvailable = CoachSchedule::leftjoin('coach_schedule_orders',function($join){
                        $join->on('coach_schedules.id', '=', 'coach_schedule_orders.coach_schedule_id');
                    })
                    ->where('coach_schedules.coach_id',$coach_id)
                    ->whereNull('coach_schedule_orders.id')
                    ->get(['coach_schedules.*']);

                    if (count($scheduleAvailable) > 0) {
                        foreach ($scheduleAvailable as $key => $sched) {

                            if (date("Y-m-d",strtotime($sched->start_date)) >= $currentDate) {
                                $status = 'available';
                            } else {
                                $status = 'available-expired';
                            }

                            $coachScheduleDef['id'] = $sched->id;
                            $coachScheduleDef['start_date'] = $sched->start_date;
                            $coachScheduleDef['end_date'] = $sched->end_date;
                            $coachScheduleDef['start_time'] = $sched->start_time;
                            $coachScheduleDef['end_time'] = $sched->end_time;
                            $coachScheduleDef['status'] = $status;

                            $detailSchedule[] = $coachScheduleDef;
                        }

                        /* Grouping schedule by date */
                        foreach ($detailSchedule as $val) {
                            $coachScheduleDefRes['id'] = $val['id'];
                            $coachScheduleDefRes['date'] = $val['start_date'];
                            $coachScheduleDefRes['time'] = array(
                                    'start_time'=>$val['start_time'],
                                    'end_time' => $val['end_time'],
                                    'status' => $val['status'],
                                );
                            $return[] = $coachScheduleDefRes;
                        }
                        /* End Grouping By Date */
                    }
                } else {
                    $res = new ApiResponse([
                        'success' => false,
                        'message' => 'Failed get data schedule coach',
                        'code' => 400
                    ], $return);
                    return response()->json($res, 200);
                }

                $return = [
                            'course_id' => $course_id,
                            'coach' => $coach,
                            'schedule' => $return,
                            'product' => $product,
                            'code' => $code
                        ];

                $res = new ApiResponse([
                        'success' => true,
                        'message' => 'success',
                        'code' => 200
                    ], $return);
                    return response()->json($res, 200);

            } else {
                $res = new ApiResponse([
                        'success' => false,
                        'message' => 'Your order has been selected schedule',
                        'code' => 400
                    ], $return);
                return response()->json($res, 200);
            }
            
        } catch (Exception $e) {
            $res = new ApiResponse([
                        'success' => false,
                        'message' => 'Get schedule coach failed. '.$e->getMessage().' '.$e->getFile().' '.$e->getLine(),
                        'code' => 400
                    ], $return);
                return response()->json($res, 200);
        }
    }
}
