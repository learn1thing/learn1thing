<?php

namespace App\Http\Controllers\Api\v1;

use App\Classes\TryCatch;
use App\Classes\ApiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BussinesPartnerController extends Controller
{

    /**
     * Request Instance
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Status code from response
     * @var integer
     */
    protected $statusCode;

    /**
     * Init Request Instance
     * @param  Request $request
     * @return void
     */
    public function __construct(Request $request)
    {
      $this->request = $request;
    }

    /**
     * Get Bussines Partner base on ID
     * @return \Illuminate\Support\Response
     */
    public function show()
    {
      $proses = new TryCatch(function()  {
        $user = $this->getUserLogin($this->getTokenFromReqeust($this->request));
        $bussinesPartner = $user->related;
        $roleName = $user->roles->first()->slug;
        if ($user && $bussinesPartner) {
          if ($roleName == 'business-partner') {
            $this->statusCode = 200;
            unset($bussinesPartner->created_at);
            unset($bussinesPartner->updated_at);
            return $data = new ApiResponse([
              'success' => true,
              'message' => 'Success for get Bussines Partner',
              'code' => $this->statusCode
            ], $bussinesPartner);
          } else {
            throw new \Exception('Your Are Not Bussines Partner', 403);
          }
        } else {
          throw new \Exception("Data Bussines Partner Not Found", 404);
        }

      }, function(\Exception $e) {
        $this->statusCode = $e->getCode();
        return $data = new ApiResponse([
          'success' => false,
          'message' => $e->getMessage(),
          'code' => $this->statusCode
        ], (object)[]);
      });

      return response()->json($proses->getCallback(), $this->statusCode);

    }
}
