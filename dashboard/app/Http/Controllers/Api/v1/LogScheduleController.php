<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LogOrderSchedule;
use App\Models\CoachScheduleOrder;
use App\Classes\ApiResponse;
use App\Classes\CurlRequest;
use DB;


class LogScheduleController extends Controller
{

    public function __construct()
    {
        $this->model_logOrderSchedule = new LogOrderSchedule();
    }

	public function getLogOrderShcedule($code = false)
    {
        if (!$code) {
            $data = new ApiResponse([
                'success' => false,
                'message' => 'Code is required',
                'code' => 400
            ], []);
            return response()->json($data, 200);
        }
        
        $dataOrder = LogOrderSchedule::where('code',$code)->get();
        if ($dataOrder) {

        	$data = new ApiResponse([
                'success' => true,
                'message' => 'Data found',
                'code' => 200
            ], $dataOrder);

        } else {
        	$data = new ApiResponse([
                'success' => false,
                'message' => 'Data not found',
                'code' => 400
            ], []);
        }

        return response()->json($data, 200);
    }

    public function paymentOrderSchedule(Request $request)
    {
        DB::beginTransaction();
        try {
            $data = array();
            $ids = array();
            $coachSchedule = array();

            $getLogSchedule = LogOrderSchedule::where(['code'=>$request->code])->get();
            if (count($getLogSchedule) > 0) {
                foreach ($getLogSchedule as $log) {
                    $dataSchedule = json_decode($log->details);
                    if (count($dataSchedule) > 0) {
                        foreach ($dataSchedule as $key => $value) {
                            $model_scheduleOrder = new CoachScheduleOrder();
                            $value->product_id = $log->product_id;
                            $value->order_id = $request->order_id;
                            $data = $model_scheduleOrder->saveScheduleOrder($value);
                            
                            $ids[] = $data->id;
                        }

                        $log->delete();
                    
                    }
                }

                if (count($ids) > 0) {
                    // // get schedule coach by ids
                    $coachSchedule = $model_scheduleOrder->getScheduleByIds($ids);
                    
                }

                $return = new ApiResponse([
                    'success' => true,
                    'message' => 'Save data log order has been successfully',
                    'code' => 200
                ], $coachSchedule);

                DB::commit();

            } else {
                $return = new ApiResponse([
                    'success' => false,
                    'message' => 'Data not found',
                    'code' => 400
                ], []);
            }

        } catch (\Exception $e) {
            DB::rollback();
            $return = new ApiResponse([
                    'success' => false,
                    'message' => $e->getMessage(),
                    'code' => 400
                ], []);
        }
        return response()->json($return, 200);
    }

    public function userEnrolment(Request $request)
    {
        if (!$request->id) {
            $data = new ApiResponse([
                'success' => false,
                'message' => 'Id is required',
                'code' => 400
            ], []);
            return response()->json($data, 200);
        }

        if (!$request->user_enrolments_id) {
            $data = new ApiResponse([
                'success' => false,
                'message' => 'User Enrolment Id is required',
                'code' => 400
            ], []);
            return response()->json($data, 200);
        }
        
        DB::beginTransaction();
        try {
            $data=[];
            $coachSchedule = new CoachScheduleOrder();
            $schedule = $coachSchedule->find($request->id);
            $userEnrolmentsId = json_decode($schedule->user_enrolments_id);

            if (count($userEnrolmentsId)>0) {
                foreach ($userEnrolmentsId as $key => $value) {
                    if (!in_array($value, $data)) {
                        $data[] = $value;
                    }
                }
            }

            if (!in_array($request->user_enrolments_id, $data)) {
                $data[] = $request->user_enrolments_id;
            }

            $schedule->user_enrolments_id = json_encode($data);
            $schedule->save();
            DB::commit(); 

            $return = new ApiResponse([
                'success' => true,
                'message' => 'Save data user enrolments id has been successfully',
                'code' => 200
            ], $schedule);
        } catch (Exception $e) {
            DB::rollback();
            $return = new ApiResponse([
                'success' => false,
                'message' => $e->getMessage(),
                'code' => 400
            ], []);   
        }
    }

    public function checkSchedule(Request $request, $code, $coachId)
    {
        try {
            $result = [];
            $detailLog = [];
            $data = [];
            $response = [
                'success' => true,
                'message' => 'Schedule Is Empty',
                'code' => 200
            ];
            // Get Schedule
            $getLogSchedule = LogOrderSchedule::where(['code'=>$request->code])->get();
            if (count($getLogSchedule) > 0) {
                foreach ($getLogSchedule as $log) {
                    $detailLog[] = json_decode($log->details);
                }

                if (count($detailLog) > 0) {
                    foreach ($detailLog as $key => $log) {
                        foreach ($log as $key => $value) {
                        info($value->start);
                            // Get coach schedule order
                            $cso = CoachScheduleOrder::whereCoachScheduleId($value->coach_schedule_id)->get();
                            // Compare data
                            info($cso);
                            if (count($cso) > 0) {
                                foreach ($cso as $key => $val) {
                                    $orders = CoachScheduleOrder::where('start_time', '>=', date('H:i:s', strtotime($val->start)))
                                        ->where('end_time', '<=', date('H:i:s', strtotime($val->end)))
                                        ->where('start_date', '>=', date('Y-m-d', strtotime($val->start)))
                                        ->where('end_date', '<=', date('Y-m-d', strtotime($val->end)))
                                        ->whereCoachId($val->coach_id)
                                        ->where('id', '!=', $val->id)
                                        ->where('order_id', '>', 0)
                                        ->where('order_id', '!=', null)
                                        ->get();
                                    info($orders);
                                    if (count($orders) > 0) {
                                        foreach ($orders as $key => $order) {
                                            // Collect Data
                                            $orderCus = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/order_admin/orders&id='.$order->order_id);
                                            if ($orderCus->success) {
                                                /**
                                                 * Order Status OC
                                                 * -----------------------
                                                 * 1  => Pending
                                                 * 2  => Processing
                                                 * 3  => Shipped
                                                 * 5  => Complete
                                                 * 7  => Canceled
                                                 * 8  => Denied
                                                 * 9  => Canceled Reversal
                                                 * 10 => Failed
                                                 * 11 => Refunded
                                                 * 12 => Reversed
                                                 * 13 => Chargeback
                                                 * 14 => Expired
                                                 * 15 => Processed
                                                 * 16 => Voided
                                                 * ----------------------
                                                 */
                                                $oStatus = [1, 2, 3, 5, 15 ];
                                                if (in_array($orderCus->data->order_status_id, $oStatus)) {
                                                    $data[] = $order;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (count($data) > 0) {
                    $response = [
                        'success' => true,
                        'message' => 'Schedule Is Already Booked',
                        'code' => 401
                    ];
                }
                $return = new ApiResponse($response, $data);
            } else {
                $return = new ApiResponse([
                    'success' => false,
                    'message' => 'Data not found',
                    'code' => 400
                ], $data);
            }

        } catch (\Exception $e) {
            $return = new ApiResponse([
                    'success' => false,
                    'message' => $e->getMessage(),
                    'code' => 400
                ], []);
        }

        return response()->json($return, 200);
    }


    // course_id:"168"
    // product_id:"20"
    // schedule_id:["1030"]
    // code: "12345678"
    public function submitLogOrderSchedule(Request $request)
    {   
        DB::beginTransaction();
        try {

            $params = $request->all();
            $coach_schedule_ids = $request->schedule_id;
            $ids = array();

            if (count($coach_schedule_ids) > 0) {
                    
                $data = $this->model_logOrderSchedule->saveData($params);
                if ($data) {
                    $return = new ApiResponse([
                        'success' => true,
                        'message' => 'Save data has been successfully',
                        'code' => 200
                    ], $data);
                } else {
                    $return = new ApiResponse([
                        'success' => false,
                        'message' => 'Save data has been failed. Please select your schedule',
                        'code' => 400
                    ], []);
                }    

                DB::commit();
            } else {
                $return = new ApiResponse([
                    'success' => false,
                    'message' => 'Save data has been failed. Please select your schedule',
                    'code' => 400
                ], []);
            }

        } catch (\Exception $e) {
            DB::rollback();
            $return = new ApiResponse([
                    'success' => false,
                    'message' => $e->getMessage(),
                    'code' => 400
                ], []);
        }

        return response()->json($return, 200);

    }
}





