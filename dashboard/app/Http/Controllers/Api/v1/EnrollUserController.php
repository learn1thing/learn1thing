<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use App\Classes\CurlRequest;
use App\Classes\ApiResponse;
use App\Models\User;
use App\Models\Coach;
use App\Models\ProductSet;
use App\Models\Product;
use App\Models\ProductQuota;
use App\Mail\SendAlertMinQuota;

use Illuminate\Support\Facades\Log;

class EnrollUserController extends Controller
{
    public function enrollUser(Request $request){

	Log::info('+++++++++++++request enroll+++++++++++ ');
	Log::info($request);

    	$client = new Client();
        $res = [];
    	if (@$request->email) {
            $response = $client->request('POST', env('MOODLE_ENROL_USER'), [
             	'form_params' => [
                    'url[0][type]' => 'enrol',
    	    		'url[0][course_id]' => $request->course_id,
    	    		'url[0][shortname]' => $request->shortname,
    	    		'url[0][enrol_id]' => $request->enrol_id,
                    'url[0][email]' => $request->email,
                    'url[0][timestart]' => ($request->timestart) ? $request->timestart : '-',
    	    		'url[0][timeend]' => ($request->timeend) ? $request->timeend : '-',
                    ]
            ]);
            $res[] = json_decode($response->getBody());
	
	    //Log::info(json_decode($response->getBody()));	

        }

        if (@$request->email_user) {
            $response = $client->request('POST', env('MOODLE_ENROL_USER'), [
                'form_params' => [
                    'url[0][type]' => 'enrol',
                    'url[0][course_id]' => $request->course_id,
                    'url[0][shortname]' => $request->shortname,
                    'url[0][enrol_id]' => $request->enrol_id,
                    'url[0][email]' => $request->email_user,
                    'url[0][timestart]' => ($request->timestart) ? $request->timestart : '-',
                    'url[0][timeend]' => ($request->timeend) ? $request->timeend : '-',
                    ]
            ]);
            $res[] = json_decode($response->getBody());

		//Log::info(json_decode($response->getBody()));

        }

        $coach = User::join('coaches', 'users.id', '=', 'coaches.user_id')
            ->whereEmail($request->email)
            ->where('course_id', '>', 0)
            ->where('course_id', '!=', null)
            ->first();

        if ($coach) {
            $response = $client->request('POST', env('MOODLE_ENROL_USER'), [
                'form_params' => [
                    'url[0][type]' => 'enrol-coach-course',
                    'url[0][course_id]' => $coach->course_id,
                    'url[0][shortname]' => $request->shortname,
                    'url[0][enrol_id]' => $request->enrol_id,
                    'url[0][email]' => $request->email_user,
                    'url[0][timestart]' => ($request->timestart) ? $request->timestart : '-',
                    'url[0][timeend]' => ($request->timeend) ? $request->timeend : '-',
                    ]
            ]);

            $response = $client->request('POST', env('MOODLE_ENROL_USER'), [
                'form_params' => [
                    'url[0][type]' => 'enrol-coach-course',
                    'url[0][course_id]' => $coach->course_id,
                    'url[0][shortname]' => $request->shortname,
                    'url[0][enrol_id]' => $request->enrol_id,
                    'url[0][email]' => $request->email,
                    'url[0][timestart]' => ($request->timestart) ? $request->timestart : '-',
                    'url[0][timeend]' => ($request->timeend) ? $request->timeend : '-',
                    ]
            ]);
		//Log::info(json_decode($response->getBody()));
            // $res[] = json_decode($response->getBody());   
        }
        
        if (@$request->reschedule == false) {
            $responses = CurlRequest::sendRequest('GET', env('OC_BASE_URL').'/index.php?route=rest/product_admin/products&id='.$request->product_id);
            if ($responses->success) {
                if (count(@$responses->data->category) > 0) {
                    $state = false;
                    foreach ($responses->data->category as $categories) {
                        foreach ($categories as $category) {
                            if (!$state) {
                                if (strpos(strtolower(@$category->name), 'virtual') !== false) {
                                    $state = true;
                                    $productQuota = ProductQuota::where('product_code',$request->product_id)->first();
                                    if ($productQuota) {
                                        $updateSoldQuota = ProductQuota::where('product_code',$request->product_id)
                                            ->update(['sold_quota' => $productQuota->sold_quota+1, 'sold_quota_num' => $productQuota->sold_quota_num+1]);

                                        if ($productQuota->min_quota == ($productQuota->sold_quota_num+1) && !$productQuota->is_alert) {
                                            $updateIsAlert = ProductQuota::where('product_code',$request->product_id)
                                                ->update(['is_alert' => true]);

                                            // send email to BP .start here
                                            $data = [
                                                'user' => $productQuota->user,
                                                'product' => $productQuota->product
                                            ];
                                            $mail = \Mail::to( $productQuota->user->email )->send(new SendAlertMinQuota($data));
                                            // send email to BP .end here
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return $res;
    }
}
