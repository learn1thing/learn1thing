<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use App\Classes\CurlRequest;
use App\Classes\ApiResponse;
use App\Models\Product;
//add by puw on 30-12-2017
use App\Models\OrderProduct;
use App\Models\OrderHistory;
use App\Models\OrderHistoryDetail;
use Sentinel;
use App\Models\User;
use Validator;

class ProfilerController extends Controller
{
    
    public function __construct()
    {
        $this->user = new User;
    }

    public function dataProfiler($param){

        $client = new Client();
        if(strpos($param, '@') !== false) {
            $data = $this->user->where('email',$param)->first();
            if($data){
                $id = $data->id;
            } else {
                $result = new ApiResponse([
                    'success' => false,
                    'message' => 'Data not found',
                    'code' => 400
                ], []);
            }
            
        } else {
            $id = $param;
        }
        
        try {
            $get_profiler = $client->request('GET', env('PROFILER_URL').'/api/v1/surveys?creator_id='.$id);
        
            $all_profiler = json_decode($get_profiler->getBody())->data->surveys;

            $result = new ApiResponse([
                'success' => true,
                'message' => 'Success get profiler',
                'code' => 200
            ], $all_profiler);

            return response()->json($result,200);
            
        } catch (ClientException $e) {
            
            $result = new ApiResponse([
                'success' => true,
                'message' => 'Profiler not found',
                'code' => 200
            ], []);

            return response()->json($result,200);
        }
    }

    public function detailDataProfiler($id){

        $client = new Client();
        try {
            $get_profiler = $client->request('GET', env('PROFILER_URL').'/api/v1/surveys/'.$id, [
            'headers' => [
              'id' => $id
            ]
           ]);
        
            $all_profiler = json_decode($get_profiler->getBody())->data;

            $result = new ApiResponse([
                'success' => true,
                'message' => 'Success get profiler',
                'code' => 200
            ], $all_profiler);

            return response()->json($result,200);
            
        } catch (ClientException $e) {
            
            $result = new ApiResponse([
                'success' => true,
                'message' => 'Profiler not found',
                'code' => 200
            ], []);

            return response()->json($result,200);
        }
    }

    public function getDataUser($param){
        if(strpos($param, '@') !== false) {
            $data = $this->user->where('email',$param)->first();
            if($data){
                $id = $data->customer_id;
            } else {
                $result = new ApiResponse([
                    'success' => false,
                    'message' => 'Data not found',
                    'code' => 400
                ], []);
            }
            
        } else {
            $id = $param;
        }

        $user = $this->user->where('customer_id',$id)->first();

        if($user){

            $data['id'] = $user->customer_id;
            $data['email'] = $user->email;
            $data['first_name'] = $user->first_name;
            $data['last_name'] = $user->last_name;
            $data['phone'] = $user->phone;

            $result = new ApiResponse([
                'success' => true,
                'message' => 'Success get data',
                'code' => 200
            ], $data);
            return response()->json($result,200);

        } else {
            $result = new ApiResponse([
                'success' => false,
                'message' => 'Data not found',
                'code' => 200
            ], []);
            return response()->json($result,200);

        } 

    }

    //add by puw on 30-12-2017
    public function getCustomerUrl($email,Request $req)
    {
        try {
            $id=3;
            $customer_id=0;
            if(strpos($email, '@') !== false) {
                $data = User::where('email',$email)->first();
                if($data){
                    $id = $data->id;
                    $customer_id=$data->customer_id;
                } else {
                    $result = new ApiResponse([
                        'success' => false,
                        'message' => 'Data not found',
                        'code' => 400
                    ], []);
                }
                
            } else {
                $id = $email;
            }

            //berarti ini tidak bisa.
            $id_course=$req->input('id_course');
            $id_profiler=$req->input('id_profiler');

	   
            //Get order Product
            if(user_info()->inRole('standard-customer')){
                $orderProductDetail=OrderHistoryDetail::join('order_histories','order_history_details.order_history_id','=','order_histories.id')
                                                      ->join('product_sets','product_sets.product_id','=','order_history_details.product_id')
                                                      ->where('order_histories.customer_id','=',$customer_id)
                                                      ->where('link_profiler','!=',null)
                                                      ->where('link_profiler','!=','empty')
                                                      ->where('link_profiler','!=','')
                                                      ->where('link_profiler','like','%'.$id_profiler.'%')
                                                      ->where('product_sets.course_id','=',$id_course)
                                                      ->where('product_sets.profiler_id','=',$id_profiler)
                                                      ->select('order_histories.customer_id','order_history_details.link_profiler','product_sets.course_id','product_sets.profiler_id')
                                                      ->get();
              }else{
                $orderProductDetail = OrderProduct::join('product_sets','product_sets.product_id','order_products.product_code')
                                            ->where('product_sets.course_id','=',$id_course)
                                            ->where('product_sets.profiler_id','=',$id_profiler)
                                            ->where('order_products.link','!=',null)
                                            ->where('order_products.user_id','=',$id)->get();    
              }             
            
            //old query
            //$orderProductDetail = OrderProduct::where('user_id',$id)->get();
                    //->where('status', 2)->first();
                    //
            foreach ($orderProductDetail as $key => $value) {
                if(!user_info()->inRole('standard-customer')){
                    $datalink = json_decode($value['link']);
                }else{
                    $datalink = json_decode($value['link_profiler']);
                }

                if ($datalink!=null) {
                    
                    foreach ($datalink as $k => $v) {
                        if($v->profiler_id==$id_profiler)
                            $datalink = $v->link;
                    }
                } else {
                    if(!user_info()->inRole('standard-customer')){
                        $datalink = $value['link'];
                    }else{
                        $datalink = $value['link_profiler'];
                    }
                }

                $orderProductDetail[$key]['link']=$datalink;
            }

            $all_profiler = ['data'=>$orderProductDetail,'email'=>$email,'id_profiler'=>$id_profiler,'id_course'=>$id_course];

            $result = new ApiResponse([
                'success' => true,
                'message' => 'Success get customer profiler',
                'code' => 200
            ], $all_profiler);

            return response()->json($result,200);
            
        } catch (ClientException $e) {
            
            $result = new ApiResponse([
                'success' => true,
                'message' => 'customer Profiler not found',
                'code' => 200
            ], []);

            return response()->json($result,200);
        }
    }
}
