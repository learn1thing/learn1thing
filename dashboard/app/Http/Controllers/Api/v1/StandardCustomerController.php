<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\StandardCustomer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StandardCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StandardCustomer  $standardCustomer
     * @return \Illuminate\Http\Response
     */
    public function show(StandardCustomer $standardCustomer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StandardCustomer  $standardCustomer
     * @return \Illuminate\Http\Response
     */
    public function edit(StandardCustomer $standardCustomer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StandardCustomer  $standardCustomer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StandardCustomer $standardCustomer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StandardCustomer  $standardCustomer
     * @return \Illuminate\Http\Response
     */
    public function destroy(StandardCustomer $standardCustomer)
    {
        //
    }
}
