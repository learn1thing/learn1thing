<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Sentinel;
use GuzzleHttp\Client;
use App\Http\Controllers\DashboardController as DashboardController;
use App\Models\User;
use App\Models\Coach;

class GeneralController extends Controller
{
    public function getCountry()
    {
        $countries = [];
        $client = new Client();
        $get_countries = $client->request('GET', env('OC_BASE_URL').'/index.php?route=feed/rest_api/countries', [
            'headers' => [
              'X-Oc-Merchant-Id' => 12345,
            ]
          ]);
        $response = json_decode($get_countries->getBody());
        if (@$response->success) {
            $countries = json_decode($get_countries->getBody())->data;
            return response()->json(['status' => true, 'message' => 'Successfully get data','data' => $countries],200);
        }

        return response()->json(['status' => false, 'message' => 'Failed get data','data' => $countries],200);
    }

    public function getCountryDetail($id)
    {
        $country_detail = (object) array();
        $client = new Client();
        $get_country_detail = $client->request('GET', env('OC_BASE_URL').'/index.php?route=feed/rest_api/countries&id='.$id, [
            'headers' => [
              'X-Oc-Merchant-Id' => 12345,
              'id' => $id,
            ]
        ]);

        $response = json_decode($get_country_detail->getBody());
        if (@$response->success) {
            $country_detail = json_decode($get_country_detail->getBody())->data;
            return response()->json(['status' => true, 'message' => 'Successfully get data','data' => $country_detail],200);
        }
        
        return response()->json(['status' => false, 'message' => 'Failed get data','data' => $country_detail],200);
    }

    public function dashboard(Request $request)
    {
        $dashboard = new DashboardController;
        $token = $request->header('Authorization'); 
        $userLogin = Sentinel::findByPersistenceCode($token);

        if ($userLogin && $userLogin->email == user_info('email')) {

            $role = $userLogin->roles[0]->slug;

            $result = json_decode($dashboard->analytic($request)->getContent());

            if ($result->status) {
                unset($result->status);
                return response()->json(['status' => true, 'message' => 'Success get data','data' => $result],200);
            } else {
                return response()->json(['status' => false, 'message' => 'Failed get data','data' => (object) array()],200);
            }
        } else {
            return response()->json(['status' => false, 'message' => 'Token not match','data' => (object) array()],200);
        }
    }

    public function getEmailById(Request $request)
    {
        if ($request->has('user_id')) {
            $user_id = $request->user_id;
            // $coach = Coach::find($user_id);
            $user = User::find($user_id);
            return response()->json(['status' => true, 'message' => 'Success get data','data' => $user],200);
        } else {
            return response()->json(['status' => false, 'message' => 'Token not match','data' => (object) array()],200);
        }
    }
}
