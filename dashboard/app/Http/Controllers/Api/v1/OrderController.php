<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use App\Classes\CurlRequest;
use App\Classes\ApiResponse;
use App\Models\User;
use App\Models\ProductSet;
use App\Models\Product;
use App\Models\OrderHistory;
use App\Models\OrderHistoryDetail;
use App\Models\OrderLink;
use App\Http\Controllers\Admin\Master\OrderController as orderCtrl;
use App\Http\Controllers\Api\v1\LogScheduleController as logScheduleCtrl;
use App\Http\Controllers\Api\v1\CoachController as coachCtrl;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller
{
    public function orderSurvey(Request $request){

    	//Get Detail User 
    	$user = User::where('customer_id', $request->user_id)->first();

    	//Find Survey
    	$survey = ProductSet::where('product_id', $request->product_id)->first();

    	//Get Model Product
    	$model = Product::where('product_code', $request->product_id)->first();


    	$client = new Client();
    	if($survey){
	    	try {
		        $response = $client->request('POST', env('PROFILER_URL').'/api/v1/buysurvey', [
		            'form_params' => [
		                'survey_id' => $survey->profiler_id,
		                'product_id' => $survey->product_id,
		                'model' => $model->model,
		                'user_id' => $user->id, //$survey->created_by, //edit by mamat
		                'email' => $user->email,
		                'first_name' => $user->first_name,
		                'last_name' => $user->last_name
		            ]
		        ]);

		        return response()->json(json_decode($response->getBody()),200);
	    	} catch (ClientException $e) {
	            $response =  new ApiResponse([
	                'success' => false,
	                'message' => 'Data Not Found',
	                'code' => 400
	            ], (object)[]);
	            return response()->json($response,200);
	        }
	    } else {
	    	$response =  new ApiResponse([
                'success' => false,
                'message' => 'Data Not Found',
                'code' => 400
            ], (object)[]);
            return response()->json($response,200);
	    }
        
    }

    public function getOrderHistory(Request $request)
    {
        $client = new Client();
        $result = [];
        $x = 0;
        if (@$request->order_id && @$request->user_id) {
            $responses = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/order_admin/orders&id='.$request->order_id);
            if (@$responses->success) {
                $result['info'] = $responses->data;
                $result['products'] = $responses->data->products;
                if (count($responses->data->products) > 0) {
                    foreach ($responses->data->products as $product) {

                        //Get Detail User 
                        $user = User::where('customer_id', @$responses->data->customer_id)->first();

                        //Course
                        $courses = ProductSet::where('product_id', $product->product_id)->get();

                        //Get Model Product
                        $model = Product::where('product_code', $product->product_id)->first();

                        //Get Link Course BBB
                        $links = OrderLink::where([
                                'order_code' => $request->order_id,
                                'product_code' => $product->product_id
                            ])->get();

                        $orderHistoryGetLink = OrderHistory::join('order_history_details','order_history_details.order_history_id','=','order_histories.id')
                            ->select('order_history_details.link_profiler')
                            ->where('order_histories.order_code',$request->order_id)
                            ->where('order_histories.customer_id',@$responses->data->customer_id)
                            ->where('order_history_details.product_id',$product->product_id)->first();

                        $client = new Client();

                        if (count($courses) > 0) {
                            foreach ($courses as $key => $course) {
                                $product->course_url[$key] = $course->course_url;
                                $product->courses[$key]['course_id'] = $course->course_id;
                                $product->courses[$key]['enrol_id'] = $course->enrol_id;
                                $product->courses[$key]['shortname'] = $course->shortname;
                                $product->courses[$key]['course_url'] = $course->course_url;
                            }

                            if (!$orderHistoryGetLink && @$request->buysurvey) {
                                try
                                {
                                    $i=0;
                                    $link_profiler_customer=array();

                                    foreach ($courses as $key) {
                                        
                                        $responses = $client->request('POST', env('PROFILER_URL').'/api/v1/buysurvey', [
                                            'form_params' => [
                                                'survey_id' => $key->profiler_id,
                                                'product_id' => $key->product_id,
                                                'model' => $model->model,
                                                'user_id' => $user->id, //$courses[0]->created_by, //edit by mamat
                                                'email' => $user->email,
                                                'first_name' => $user->first_name,
                                                'last_name' => $user->last_name
                                            ]
                                        ]);

                                        $response = json_decode($responses->getBody());
                                        if (@$response->status->succeded) {
                                            $link_profiler_customer[$i]['profiler_id']=$key->profiler_id;
                                            $link_profiler_customer[$i]['link']=@$response->data->link;
                                            
                                        } else {
                                            $link_profiler_customer[$i]['profiler_id']='';
                                            $link_profiler_customer[$i]['link']='';
                                        }

                                        $i++;
                                    }
                                    $product->profiler_url=$link_profiler_customer;

                                } catch (ClientException $e) {
                                    if ($e->hasResponse()) {
                                        $response = json_decode($e->getResponse()->getBody());
                                        info('Error get profiler: '.$response->status->message);
                                    }
                                    $product->profiler_url = '';
                                }

                                $product->coaches = [];
                                $is_coaching = false;

                                $productDetail = CurlRequest::sendRequest('GET', env('OC_BASE_URL').'/index.php?route=rest/product_admin/products&id='.$product->product_id);
                                if ($productDetail->success) {
                                    if (count($productDetail->data->category) > 0) {
                                        $state = false;
                                        foreach ($productDetail->data->category as $categories) {
                                            foreach ($categories as $category) {
                                                if (!$state) {
                                                    if (strpos(strtolower(@$category->name), 'coaching') !== false) {
                                                        $state = true;
                                                        $is_coaching = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if (!empty($request->code) && $is_coaching) {
                                    $logScheduleCtrl = new logScheduleCtrl;
                                    $newRequest = new Request;
                                    $newRequest->merge(['code' => $request->code, 'order_id' => $request->order_id]);
                                    $reqLogSchedules = json_decode($logScheduleCtrl->paymentOrderSchedule($newRequest)->getContent());

                                    if (count($reqLogSchedules->data) > 0) {
                                        $coaches = [];
                                        foreach ($reqLogSchedules->data as $schedule) {
                                            $arrayCoach = [
                                                'email' => $schedule->email,
                                                'name_user' => $user->first_name.' '.$user->last_name,
                                                'details' => $schedule->details,
                                                'url_course' => $schedule->url_course
                                            ];

                                            array_push($coaches, $arrayCoach);

                                            // store link
                                            $requestBBB = new Request;
                                            $requestBBB->merge(['order_id' => @$request->order_id, 'link' => @$schedule->url_course, 'product_id' => @$product->product_id]);
                                            $this->storeBBBLink($requestBBB);
                                        }

                                        $product->coaches = $coaches;

                                        $coachCtrl = new coachCtrl;
                                        $requestMail = new Request;
                                        $requestMail->merge(['order_id' => $request->order_id]);
                                        $coachCtrl->sendMail($requestMail);
                                    }
                                }
                            }
                        } else {
                            // $product->profiler_url = '';
                            $product->course_url = [];
                            $product->courses = [];
                        }

                        
                        if ($orderHistoryGetLink) {
                            $product->profiler_url = $orderHistoryGetLink->link_profiler;
                        }

                        if (count($links) > 0) {
                            foreach ($links as $key => $link) {
                                $product->bbb_link[$key] = $link->link;
                            }
                        } else {
                            $product->bbb_link = [];
                        }

                        $x++;
                    }                    
                        unset($responses->data->products);
                }
                $response =  new ApiResponse([
                    'success' => true,
                    'message' => 'Data available',
                    'code' => 200
                ], $result);
            } else {
                $response =  new ApiResponse([
                    'success' => true,
                    'message' => 'Data Not Found',
                    'code' => 200
                ], $result);
            }
        } else {
        	$responses = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/order_admin/userorders&user='.$request->user_id);
            if ($responses->success) {
                foreach ($responses->data as $order) {
                    $orderCus = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/order_admin/orders&id='.$order->order_id);
                    if ($orderCus->success) {
                        $result[$x]['info'] = $orderCus->data;
                        $result[$x]['products'] = $orderCus->data->products;
                        if (count($orderCus->data->products) > 0) {
                            foreach ($orderCus->data->products as $product) {
                                //Get Detail User 
                                $user = User::where('customer_id', $request->user_id)->first();

                                //Course
                                $courses = ProductSet::where('product_id', $product->product_id)->get();

                                //Get Model Product
                                $model = Product::where('product_code', $product->product_id)->first();

                                //Get Link Course BBB
                                $links = OrderLink::where([
                                        'order_code' => $order->order_id,
                                        'product_code' => $product->product_id
                                    ])->get();

                                $orderHistoryGetLink = OrderHistory::join('order_history_details','order_history_details.order_history_id','=','order_histories.id')
                                    ->select('order_history_details.link_profiler')
                                    ->where('order_histories.order_code',$order->order_id)
                                    ->where('order_histories.customer_id',$request->user_id)
                                    ->where('order_history_details.product_id',$product->product_id)->first();

                                $client = new Client();

                                if (count($courses) > 0) {
                                    foreach ($courses as $key => $course) {
                                        $product->course_url[$key] = $course->course_url;
                                        $product->courses[$key]['course_id'] = $course->course_id;
                                        $product->courses[$key]['enrol_id'] = $course->enrol_id;
                                        $product->courses[$key]['shortname'] = $course->shortname;
                                        $product->courses[$key]['course_url'] = $course->course_url;
                                    }
                                    
                                    if (!$orderHistoryGetLink && @$request->buysurvey) {
                                        try
                                        {
                                            $i=0;
                                            $link_profiler_customer=array();

                                            foreach ($courses as $key) {
                                            
                                                $responses = $client->request('POST', env('PROFILER_URL').'/api/v1/buysurvey', [
                                                    'form_params' => [
                                                        'survey_id' => $key->profiler_id,
                                                        'product_id' => $key->product_id,
                                                        'model' => $model->model,
                                                        'user_id' => $user->id, //$courses[0]->created_by, //edit by mamat
                                                        'email' => $user->email,
                                                        'first_name' => $user->first_name,
                                                        'last_name' => $user->last_name
                                                    ]
                                                ]);

                                                $response = json_decode($responses->getBody());
                                                if ($response->status->succeded) {
                                                    //$product->profiler_url = @$response->data->link;
                                                    $link_profiler_customer[$i]['profiler_id']=$key->profiler_id;
                                                    $link_profiler_customer[$i]['link']=@$response->data->link;

                                                } else {
                                                    $link_profiler_customer[$i]['profiler_id']=$key->profiler_id;
                                                    $link_profiler_customer[$i]['link']=@$response->data->link;
                                                }
                                                $i++;
                                            }
                                            $product->profiler_url=$link_profiler_customer;

                                        } catch (ClientException $e) {
                                            if ($e->hasResponse()) {
                                                $response = json_decode($e->getResponse()->getBody());
                                                info('Error get profiler: '.$response->status->message);
                                            }
                                            $product->profiler_url = '';
                                        }
                                    }
                                } else {
                                    // $product->profiler_url = '';
                                    $product->course_url = [];
                                }

                                if ($orderHistoryGetLink) {
                                    $product->profiler_url = $orderHistoryGetLink->link_profiler;
                                }

                                if (count($links) > 0) {
                                    foreach ($links as $key => $link) {
                                        $product->bbb_link[$key] = $link->link;
                                    }
                                } else {
                                    $product->bbb_link = [];
                                }
                            }
                        }
                        unset($orderCus->data->products);
                        $x++;
                    }
                }
                $response =  new ApiResponse([
                    'success' => true,
                    'message' => 'Data available',
                    'code' => 200
                ], $result);
            } else {
                $response =  new ApiResponse([
                    'success' => true,
                    'message' => 'Data Not Found',
                    'code' => 200
                ], $result);
            }
        }

        return response()->json($response,200);
    }

    public function createOrderHistory(Request $request)
    {
        \DB::beginTransaction();
        try {
            $products = json_decode($request->products);
            $orderDetail = $this->OrderHistoryOC($request->order_code);

            if (count($products) > 0 && @$orderDetail->success) {
                //insert order history
                $orderHistory = new OrderHistory;
                $insertOrderHistory = $orderHistory->create(['order_code' => $request->order_code, 'customer_id' => $orderDetail->data->customer_id,
                    'status'=>'test',
                    'total'=>0,
                    'products'=>0,
                    'date_added'=>date('Y-m-d H:i:s'),
                    'date_modified'=>date('Y-m-d H:i:s'),
                ]);

                //insert order history detail
                foreach ($products as $product) {
                    $orderHistoryDetail = new OrderHistoryDetail;
                    $orderHistoryDetail->create([
                        'order_history_id' => $insertOrderHistory->id,
                        'product_id' => $product->product_id,
                        'link_profiler' => $product->profiler_url,
                        'link_course' => json_encode($product->course_url)
                    ]);
                }

                \DB::commit();
                $response =  new ApiResponse([
                    'success' => true,
                    'message' => 'Data successfully added',
                    'code' => 200
                ], (object)[]);
            } else {
                $response =  new ApiResponse([
                    'success' => false,
                    'message' => 'Data failed',
                    'code' => 400
                ], (object)[]);
            }

            return response()->json($response,200);
        } catch (\Exception $e) {
            \DB::rollback();
            $response =  new ApiResponse([
                'success' => false,
                'message' => $e->getMessage().' '.$e->getLine(),
                'code' => 500
            ], (object)[]);
            
            return response()->json($response,200);
        }
    }

    public function storeBBBLink(Request $request)
    {
        \DB::beginTransaction();
        try
        {
            OrderLink::firstOrCreate(['order_code' => @$request->order_id, 'link' => @$request->link, 'product_code' => @$request->product_id]);
            
            \DB::commit();

            $response =  new ApiResponse([
                'success' => true,
                'message' => 'Link has been saved',
                'code' => 200
            ], (object)[]);

            return response()->json($response,200);
        } catch (\Exception $e) {
            \DB::rollback();
            $response =  new ApiResponse([
                'success' => false,
                'message' => $e->getMessage().' '.$e->getLine(),
                'code' => 500
            ], (object)[]);
            
            return response()->json($response,200);
        }
    }

    protected function OrderHistoryOC($orderId)
    {
        $orderDetail = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/order_admin/orders&id='.$orderId);

        if ($orderDetail->success) {
            return $orderDetail;
        }

        return false;
    }

    //Added by puw on 10012018
    public function getOrderHistoryStore(Request $request)
    {
        $client = new Client();
        $result = [];
        $x = 0;
        if (@$request->order_id && @$request->user_id) {
            $responses = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/order_admin/orders&id='.$request->order_id);
            if (@$responses->success) {
                $result['info'] = $responses->data;
                $result['products'] = $responses->data->products;
                if (count($responses->data->products) > 0) {
                    foreach ($responses->data->products as $product) {

                        //Get Detail User 
                        $user = User::where('customer_id', @$responses->data->customer_id)->first();

                        //Course
                        $courses = ProductSet::where('product_id', $product->product_id)->get();

                        //Get Model Product
                        $model = Product::where('product_code', $product->product_id)->first();

                        //Get Link Course BBB
                        $links = OrderLink::where([
                                'order_code' => $request->order_id,
                                'product_code' => $product->product_id
                            ])->get();

                        $orderHistoryGetLink = OrderHistory::join('order_history_details','order_history_details.order_history_id','=','order_histories.id')
                            ->select('order_history_details.link_profiler')
                            ->where('order_histories.order_code',$request->order_id)
                            ->where('order_histories.customer_id',@$responses->data->customer_id)
                            ->where('order_history_details.product_id',$product->product_id)->first();


                        Log::info('searc order history');
                        $order_history_id=OrderHistory::where('order_code','=',$request->order_id)
                                                    ->where('customer_id','=',@$responses->data->customer_id)
                                                    ->select('id')->pluck('id');

                        Log::info('searc order history details');
                        Log::info('order history id: '.$order_history_id);
                        Log::info('product id : '.$product->product_id);
                        Log::info('request order_id : '.$request->order_id);

                        if (!is_null($order_history_id) && isset($product->product_id)) {
                            if (count($order_history_id)>0) {
                                $order_history_data_details=OrderHistoryDetail::where('order_history_id','=',$order_history_id)
                                                                          ->where('product_id','=',$product->product_id)
                                                                          ->first();
                            }
                        }

                        $y=0;
                        $order_history_data = OrderHistory::join('order_history_details','order_history_details.order_history_id','=','order_histories.id')
                            ->where('order_histories.order_code',$request->order_id)
                            ->where('order_histories.customer_id',@$responses->data->customer_id)
                            ->where('order_history_details.product_id',$product->product_id)->first();

                        $client = new Client();

                        if (count($courses) > 0) {
                            foreach ($courses as $key => $course) {
                                $product->course_url[$key] = $course->course_url;
                                $product->courses[$key]['course_id'] = $course->course_id;
                                $product->courses[$key]['enrol_id'] = $course->enrol_id;
                                $product->courses[$key]['shortname'] = $course->shortname;
                                $product->courses[$key]['course_url'] = $course->course_url;
                            }

                            if ($order_history_data && @$request->buysurvey) {
                                Log::info('Create Link Profiler for order from store');
                                try
                                {
                                    foreach ($courses as $key) {
                                        
                                        $responses = $client->request('POST', env('PROFILER_URL').'/api/v1/buysurvey', [
                                            'form_params' => [
                                                'survey_id' => $key->profiler_id,
                                                'product_id' => $key->product_id,
                                                'model' => $model->model,
                                                'user_id' => $user->id, //$courses[0]->created_by, //edit by mamat
                                                'email' => $user->email,
                                                'first_name' => $user->first_name,
                                                'last_name' => $user->last_name
                                            ]
                                        ]);

                                        $response = json_decode($responses->getBody());
                                        if (@$response->status->succeded) {
                                            $link_profiler_customer[$y]['profiler_id']=$key->profiler_id;
                                            $link_profiler_customer[$y]['link']=@$response->data->link;
                                        } else {
                                            $link_profiler_customer[$y]['profiler_id']='';
                                            $link_profiler_customer[$y]['link']='';
                                        }

                                        $y++;
                                    }
                                    //END FOREACH COURSE
                                    $product->profiler_url=json_encode($link_profiler_customer);
                                    Log::info(json_encode($link_profiler_customer));

                                } catch (ClientException $e) {
                                    if ($e->hasResponse()) {
                                        $response = json_decode($e->getResponse()->getBody());
                                        info('Error get profiler: '.$response->status->message);
                                    }
                                    $product->profiler_url = '';
                                }
                                //END TRY CATCH

                                $product->coaches = [];
                                $is_coaching = false;

                                $productDetail = CurlRequest::sendRequest('GET', env('OC_BASE_URL').'/index.php?route=rest/product_admin/products&id='.$product->product_id);
                                if ($productDetail->success) {
                                    if (count($productDetail->data->category) > 0) {
                                        $state = false;
                                        foreach ($productDetail->data->category as $categories) {
                                            foreach ($categories as $category) {
                                                if (!$state) {
                                                    if (strpos(strtolower(@$category->name), 'coaching') !== false) {
                                                        $state = true;
                                                        $is_coaching = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if (!empty($request->code) && $is_coaching) {
                                    $logScheduleCtrl = new logScheduleCtrl;
                                    $newRequest = new Request;
                                    $newRequest->merge(['code' => $request->code, 'order_id' => $request->order_id]);
                                    $reqLogSchedules = json_decode($logScheduleCtrl->paymentOrderSchedule($newRequest)->getContent());

                                    if (count($reqLogSchedules->data) > 0) {
                                        $coaches = [];
                                        foreach ($reqLogSchedules->data as $schedule) {
                                            $arrayCoach = [
                                                'email' => $schedule->email,
                                                'name_user' => $user->first_name.' '.$user->last_name,
                                                'details' => $schedule->details,
                                                'url_course' => $schedule->url_course
                                            ];

                                            array_push($coaches, $arrayCoach);

                                            // store link
                                            $requestBBB = new Request;
                                            $requestBBB->merge(['order_id' => @$request->order_id, 'link' => @$schedule->url_course, 'product_id' => @$product->product_id]);
                                            $this->storeBBBLink($requestBBB);
                                        }

                                        $product->coaches = $coaches;

                                        $coachCtrl = new coachCtrl;
                                        $requestMail = new Request;
                                        $requestMail->merge(['order_id' => $request->order_id]);
                                        $coachCtrl->sendMail($requestMail);
                                    }
                                }
                            }
                        } else {
                            // $product->profiler_url = '';
                            $product->course_url = [];
                            $product->courses = [];
                        }

                        //UPDATE LINK PROFILER HERE
                        if (@$request->buysurvey){
                            if (isset($link_profiler_customer)) {
                                if (!is_null ($order_history_data_details)) {
                                    $order_history_data_details->link_profiler=json_encode($link_profiler_customer);
                                    $order_history_data_details->save();
                                }
                            }
                        }
                        
                        if ($orderHistoryGetLink) {
                            $product->profiler_url = $orderHistoryGetLink->link_profiler;
                        }

                        if (count($links) > 0) {
                            foreach ($links as $key => $link) {
                                $product->bbb_link[$key] = $link->link;
                            }
                        } else {
                            $product->bbb_link = [];
                        }

                        $x++;
                    }                    
                        unset($responses->data->products);
                }
                $response =  new ApiResponse([
                    'success' => true,
                    'message' => 'Data available',
                    'code' => 200
                ], $result);
            } else {
                $response =  new ApiResponse([
                    'success' => true,
                    'message' => 'Data Not Found',
                    'code' => 200
                ], $result);
            }
        } else {
            $responses = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/order_admin/userorders&user='.$request->user_id);
            if ($responses->success) {
                foreach ($responses->data as $order) {
                    $orderCus = CurlRequest::sendRequest('GET',env('OC_BASE_URL').'/index.php?route=rest/order_admin/orders&id='.$order->order_id);
                    if ($orderCus->success) {
                        $result[$x]['info'] = $orderCus->data;
                        $result[$x]['products'] = $orderCus->data->products;
                        if (count($orderCus->data->products) > 0) {
                            foreach ($orderCus->data->products as $product) {
                                //Get Detail User 
                                $user = User::where('customer_id', $request->user_id)->first();

                                //Course
                                $courses = ProductSet::where('product_id', $product->product_id)->get();

                                //Get Model Product
                                $model = Product::where('product_code', $product->product_id)->first();

                                //Get Link Course BBB
                                $links = OrderLink::where([
                                        'order_code' => $order->order_id,
                                        'product_code' => $product->product_id
                                    ])->get();

                                $orderHistoryGetLink = OrderHistory::join('order_history_details','order_history_details.order_history_id','=','order_histories.id')
                                    ->select('order_history_details.link_profiler')
                                    ->where('order_histories.order_code',$order->order_id)
                                    ->where('order_histories.customer_id',$request->user_id)
                                    ->where('order_history_details.product_id',$product->product_id)->first();

                                Log::info('searc order history');
                                $order_history_id=OrderHistory::where('order_code','=',$request->order_id)
                                                    ->where('customer_id','=',@$responses->data->customer_id)
                                                    ->select('id')->pluck('id');
                                
                                Log::info('searc order history details');
                                if (!is_null($order_history_id) && isset($product->product_id)) {
                                    if (count($order_history_id)>0) {
                                        $order_history_data_details=OrderHistoryDetail::where('order_history_id','=',$order_history_id)
                                                                                  ->where('product_id','=',$product->product_id)
                                                                                  ->first();
                                    }
                                }

                                $jk=0;

                                $order_history_data = OrderHistory::join('order_history_details','order_history_details.order_history_id','=','order_histories.id')
                                    ->where('order_histories.order_code',$request->order_id)
                                    ->where('order_histories.customer_id',@$responses->data->customer_id)
                                    ->where('order_history_details.product_id',$product->product_id)->first();

                                $client = new Client();

                                if (count($courses) > 0) {
                                    foreach ($courses as $key => $course) {
                                        $product->course_url[$key] = $course->course_url;
                                        $product->courses[$key]['course_id'] = $course->course_id;
                                        $product->courses[$key]['enrol_id'] = $course->enrol_id;
                                        $product->courses[$key]['shortname'] = $course->shortname;
                                        $product->courses[$key]['course_url'] = $course->course_url;
                                    }
                                    
                                    if ($order_history_data && @$request->buysurvey) {
                                        try
                                        {
                                            foreach ($courses as $key) {
                                            
                                                $responses = $client->request('POST', env('PROFILER_URL').'/api/v1/buysurvey', [
                                                    'form_params' => [
                                                        'survey_id' => $key->profiler_id,
                                                        'product_id' => $key->product_id,
                                                        'model' => $model->model,
                                                        'user_id' => $user->id, //$courses[0]->created_by, //edit by mamat
                                                        'email' => $user->email,
                                                        'first_name' => $user->first_name,
                                                        'last_name' => $user->last_name
                                                    ]
                                                ]);

                                                $response = json_decode($responses->getBody());
                                                if ($response->status->succeded) {
                                                    //$product->profiler_url = @$response->data->link;
                                                    $link_profiler_customer_2[$jk]['profiler_id']=$key->profiler_id;
                                                    $link_profiler_customer_2[$jk]['link']=@$response->data->link;

                                                } else {
                                                    $link_profiler_customer_2[$jk]['profiler_id']=$key->profiler_id;
                                                    $link_profiler_customer_2[$jk]['link']=@$response->data->link;
                                                }
                                                $jk++;
                                            }
                                            $product->profiler_url=json_encode($link_profiler_customer_2);
                                            

                                        } catch (ClientException $e) {
                                            if ($e->hasResponse()) {
                                                $response = json_decode($e->getResponse()->getBody());
                                                info('Error get profiler: '.$response->status->message);
                                            }
                                            $product->profiler_url = '';
                                        }
                                    }

                                } else {
                                    // $product->profiler_url = '';
                                    $product->course_url = [];
                                }

                                if ($orderHistoryGetLink) {
                                    $product->profiler_url = $orderHistoryGetLink->link_profiler;
                                }

                                //SAVE LINK_PROFILER
                                if (@$request->buysurvey){
                                    if (isset($link_profiler_customer_2)) {
                                        if (!is_null($order_history_data_details)) {
                                            $order_history_data_details->link_profiler=json_encode($link_profiler_customer_2);
                                            $order_history_data_details->save();
                                        }
                                    }
                                    
                                }

                                if (count($links) > 0) {
                                    foreach ($links as $key => $link) {
                                        $product->bbb_link[$key] = $link->link;
                                    }
                                } else {
                                    $product->bbb_link = [];
                                }
                            }
                        }
                        unset($orderCus->data->products);
                        $x++;
                    }
                }
                $response =  new ApiResponse([
                    'success' => true,
                    'message' => 'Data available',
                    'code' => 200
                ], $result);
            } else {
                $response =  new ApiResponse([
                    'success' => true,
                    'message' => 'Data Not Found',
                    'code' => 200
                ], $result);
            }
        }

        return response()->json($response,200);
    }
}
