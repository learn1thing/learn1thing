<?php

namespace App\Http\Controllers\Api\v1;

use App\Classes\ApiResponse;
use App\Http\Controllers\Controller;
use App\Models\BusinessPartner;
use App\Models\User;
use App\Models\CoachSchedule;
use App\Models\CoachCourse;
use Illuminate\Http\Request;
use App\Http\Requests\Schedule\ScheduleRequest;

class ScheduleApiController extends Controller
{

    /**
     * Display a listing of the resource.
     * @author ali
     * @return \Illuminate\Http\Response
     */

    public function getListApiSchedule($id = null)
    {
        $token = empty(\Request::header('authorization'))
                 ? \Request::header('token')
                 : \Request::header('authorization');

        $userLogin = \Sentinel::findByPersistenceCode($token);

        if ($userLogin) {
            $user = User::where('id', $id)->first();
            $returnSchedule = [];
                if ($user->count() > 0 && $user->scheduleRelated) {
                    // dump($user->scheduleRelated);exit;
                    foreach ($user->scheduleRelated as $key => $value) {
                        $returnSchedule[] = [
                            'id' => $value->id,
                            'user_id' => $value->user_id,
                            'course_id' => $value->course_id,
                            'course_name' => $value->courseRelated->name,
                            'schedule_date' => $value->schedule_date,
                            'start_time' => $value->start_time,
                            'end_time' => $value->end_time,
                            'active' => $value->actived,
                        ];
                    }
                    $success = true;
                    $status = 200;
                    $message = "success";
                } else {
                    $success = false;
                    $status = 404;
                    $message = "data not found";
                }

            return $this->generateReturnArray($returnSchedule,$success,$message,$status);
        }
        
        $data = new ApiResponse([
            'success' => false,
            'message' => trans('unauthorized'),
            'code' => 401
        ], (object)[]);

        return response()->json($data, 401);
    }


    public function saveApiSchedule(ScheduleRequest $request)
    {
      $token = empty(\Request::header('authorization'))
               ? \Request::header('token')
               : \Request::header('authorization');

      $userLogin = \Sentinel::findByPersistenceCode($token);

      if ($userLogin) {
        // $available_schedule = Schedule::where('course_id', $request->input('course_id')->where('schedule_date', $request->input('schedule_date'))->where('start_time', $request->input('start_time')))->first();
        // if ($available_schedule) {
        //   # code...
        // }
        // $course = CoachCourse::find($request->input('course_id'));
        if ($course) {
          $schedule = CoachSchedule::create($request->except('_token'));
          $success = true;
          $status = 200;
          $message = "success save schedule";

          return $this->generateReturnArray($schedule,$success,$message,$status);
        }
        $data = new ApiResponse([
          'success' => false,
          'message' => 'cannot find course',
          'code' => 400
        ], (object)[]);

        return response()->json($data, 401);
      }
      
      $data = new ApiResponse([
          'success' => false,
          'message' => trans('unauthorized'),
          'code' => 401
      ], (object)[]);

      return response()->json($data, 401);
    }

    public function editApiSchedule(Request $request) {
      $token = empty(\Request::header('authorization'))
               ? \Request::header('token')
               : \Request::header('authorization');

      $userLogin = \Sentinel::findByPersistenceCode($token);

      if ($userLogin) {

        $schedule = CoachSchedule::where('course_id', $request->input('id'))->first();
        if ($schedule) {
          $schedule->start_time = $request->input('start_time');
          $schedule->end_time = $request->input('end_time');
          $schedule->date = $request->input('date');
          $schedule->save();

          $success = true;
          $status = 200;
          $message = "success update schedule";

          return $this->generateReturnArray($schedule,$success,$message,$status);
        }
          $data = new ApiResponse([
            'success' => true,
            'message' => 'cannot find schedule',
            'code' => 400
          ], (object)[]);

          return response()->json($data, 401); 
      }
      
      $data = new ApiResponse([
          'success' => false,
          'message' => trans('unauthorized'),
          'code' => 401
      ], (object)[]);

      return response()->json($data, 401); 
    }

    public function generateReturnArray($return,$success,$message,$status)
    {
        $data = new ApiResponse([
          'success' => $success,
          'message' => $message,
          'code' => $status
        ], $return);

        return response()->json($data, $status);
    }
}
