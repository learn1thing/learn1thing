<?php

namespace App\Http\Controllers\Api\v1;

use App\Classes\TryCatch;
use App\Classes\ApiResponse;
use Illuminate\Http\Request;
use App\Models\CorporateCustomer;
use App\Http\Controllers\Controller;

class CorporateCustomerController extends Controller
{
  /**
   * Request Instance
   * @var \Illuminate\Http\Request
   */
  protected $request;

  /**
   * Status code from response
   * @var integer
   */
  protected $statusCode;

  /**
   * Init Request Instance
   * @param  Request $request
   * @return void
   */
  public function __construct(Request $request)
  {
    $this->request = $request;
  }

  /**
   * Get Bussines Partner base on ID
   * @return \Illuminate\Support\Response
   */
  public function index()
  {
    $proses = new TryCatch(function()  {
      $user = $this->getUserLogin($this->getTokenFromReqeust($this->request));
      $roleName = $user->roles->first()->slug;

      if ($roleName == 'super-admin') {
        $corporteCustomer = CorporateCustomer::get();
        if (count($corporteCustomer) > 0) {
          $this->statusCode = 200;
          $message = 'Success for get Corporate Customer';
        } else {
          $this->statusCode = 404;
          $message = 'Data Not Found';
          $corporteCustomer = (object)[];
        }
        return $data = new ApiResponse([
          'success' => true,
          'message' => $message,
          'code' => $this->statusCode
        ], $corporteCustomer);
      } else {
        throw new \Exception('Your Are Not Super Admin Role', 403);
      }

    }, function(\Exception $e) {
      $this->statusCode = $e->getCode();
      return $data = new ApiResponse([
        'success' => false,
        'message' => $e->getMessage(),
        'code' => $this->statusCode
      ], (object)[]);
    });

    return response()->json($proses->getCallback(), $this->statusCode);

  }

  /**
   * Show spesific corporate customer
   * @return Illuminate\Support\Response
   */
  public function show()
  {
    $proses = new TryCatch(function()  {
      $user = $this->getUserLogin($this->getTokenFromReqeust($this->request));
      $corporateCustomer = $user->related;
      $roleName = $user->roles->first()->slug;
      if ($user && $corporateCustomer) {
        if ($roleName == 'corporate-customer') {
          $this->statusCode = 200;
          unset($corporateCustomer->created_at);
          unset($corporateCustomer->updated_at);
          return $data = new ApiResponse([
            'success' => true,
            'message' => 'Success for get Corporate Customer',
            'code' => $this->statusCode
          ], $corporateCustomer);
        } else {
          throw new \Exception('Your Are Not Corporate Customer', 403);
        }
      } else {
        throw new \Exception("Data Corporate Customer Not Found", 404);
      }

    }, function(\Exception $e) {
      $this->statusCode = $e->getCode();
      return $data = new ApiResponse([
        'success' => false,
        'message' => $e->getMessage(),
        'code' => $this->statusCode
      ], (object)[]);
    });

    return response()->json($proses->getCallback(), $this->statusCode);
  }
}
