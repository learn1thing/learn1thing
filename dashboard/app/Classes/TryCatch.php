<?php
namespace App\Classes;

/**
 * This is class for global try catch prosessing
 */
use Closure;
use Exception;

class TryCatch
{

  /**
   * Define closure proses
   * @var Closure
   */
  protected $closure;

  /**
   * Init status from proses closure
   * @var boolean
   */
  public $status;

  /**
   * Callbck from prosessing closure
   * @var Closure
   */
  public $callback;

  /**
   * Init Closure function
   * @param Closure $closure
   */
  function __construct(Closure $closure, $onError = null)
  {
    $this->_onProsesing($closure, $onError);
  }

  /**
   * Run Closure and get callback
   * @param  Closure $closure 
   * @return void          
   */
  public function _onProsesing($closure, $onError)
  {

    try {
      $this->callback = $closure();
      $this->status = true;
    } catch (Exception $e) {
      $this->status = false;
      $this->saveToLog($e);
      if ($onError == null) {
        $this->callback = [
          'status' => 'Error',
          'error_message' => $e->getMessage()
        ];
      } else {
        $this->callback = $onError($e);
      }
    }
  }


  /**
   * Get response from prosessing closure
   * @return array|Closure
   */
  public function getCallback()
  {
    return $this->callback;
  }

  /**
   * Save error to laravel.log
   * @param  Exception $e
   * @return void
   */
  public function saveToLog($e)
  {
    info($e->getMessage());
    info($e->getLine());
    info($e->getFile());
  }

}
