<?php
namespace App\Classes;

class ApiResponse
{

  /**
   * statuses from response
   * @var array
   */
  public $status;

  /**
   * data from response
   * @var array
   */
  public $data;

  /**
   * Init Data for response
   * @param array $statuses
   * @param array $data
   */
  public function __construct($status, $data = [])
  {
    $this->status = $status;
    $this->data = $data;
    $this->render();
  }

  /**
   * render and compile data
   * @return array
   */
  public function render()
  {
    return [
      'status' => $this->status,
      'data' => $this->data
    ];
  }

}
