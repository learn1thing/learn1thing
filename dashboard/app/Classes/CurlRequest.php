<?php

namespace App\Classes;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class CurlRequest extends Model
{
    public static function sendRequest($method,$url,$multipart=null,$body=null,$key='12345')
    {
        $client = new Client();
        $response = $client->request($method, $url, [
            'headers' => [
                'X-Oc-Restadmin-Id' => $key,
            ],
            'body' => $body,
            'multipart' => $multipart
        ]);
        $responses = json_decode($response->getBody());
        return $responses;
    }

    public static function requestMoodle($method,$action,$data,$token)
    {
        $url = env('MOODLE_BASE_URL').'?wstoken='.$token.'&moodlewsrestformat=json&wsfunction='.$action;
        
        $client = new Client();
        $res = $client->request($method, $url, [
          'form_params' => $data
        ]);
            
        info('Register to Moodle');
        // info($res->getBody());
        // info(json_encode($data));
        $response = json_decode($res->getBody());
        return $response;
    }
}
