<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Controllers\Api\v1\EnrollUserController as enrollUserApi;
use Illuminate\Http\Request;
use App\Mail\SendProduct as SendProductMail;

class SendProduct implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    protected $sendRequest;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data['data'];
        $this->sendRequest = $data['sendRequest'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // enroll user
        if (count($this->sendRequest) > 0) {

            foreach ($this->sendRequest as $key => $request) {

                $enroll = new enrollUserApi;

                $enroll->enrollUser($request);

            }

        }

        $mail = \Mail::to( $this->data->email )->send(new SendProductMail($this->data));
    }
}
