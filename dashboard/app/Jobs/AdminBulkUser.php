<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\StandardCustomer;
use App\Models\LogRole;
use App\Models\User;
use App\Models\BulkLog;
use Sentinel;
use App\Http\Controllers\Auth\Api\AuthenticateController;
use Illuminate\Http\Request;

class AdminBulkUser implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $dataExport;
    protected $authRegis;
    protected $user;
    protected $userModel;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->dataExport = $data['dataExport'];
        $this->authRegis = new AuthenticateController;
        $this->user = $data['user'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->userModel = new User;
        $dataExists['email'] = array();
        $invalidData['invalid'] = 0;
        $dataFailed['email'] = array();

        $successBulk = 0;

        \DB::beginTransaction();
        foreach ($this->dataExport as $element) {
            try
            {
                if (empty($element->email)) {
                    continue;
                }
                if (strlen($element->password) < 8 || empty($element->first_name) || empty($element->last_name)) {
                    array_push($dataFailed['email'], $element->email);
                    continue;
                }

                $findAvailabeData = false;
                $requests = new Request;
                $requests->merge(['email' => $element->email, 'first_name' => $element->first_name, 
                    'last_name' => $element->last_name, 'phone' => $element->phone, 
                    'new_password' => $element->password, 'password' => $element->password]);

                if($element->email === ""){
                    $invalidData['invalid'] = $invalidData['invalid'] + 1;
                } else {
                    $findAvailabeData = $this->userModel->where('email',$element->email)->first();
                }

                if(!$findAvailabeData && !empty($element->email)){
                    if ($this->userModel->whereEmail($element->email)->first()) {
                        array_push($dataExists['email'], $element->email);
                        continue;
                    }
                    
                    // register standard customer
                    $insert = StandardCustomer::create([
                        'name' => $element->first_name.' '.$element->last_name,
                        'address' => '',
                    ]);

                    $user = Sentinel::registerAndActivate([
                        'first_name' => $element->first_name,
                        'last_name' => $element->last_name,
                        'email' => $element->email,
                        'password' => $element->password,
                        'phone' => $element->phone,
                        'related_id' => $insert->id,
                        'related_type' => StandardCustomer::class
                    ]);

                    // attach role
                    $role = Sentinel::findRoleBySlug('standard-customer');
                    $role->users()->attach($user);

                    $requests->merge(['customer_group_id' => 1]);
                    // Register to Moodle \.start
                    $registerMoodle = $this->authRegis->registerMoodle($requests,'normaly',false,$element->password);
                    if (@$registerMoodle->exception == 'invalid_parameter_exception') {
                        // update profile Moodle
                        $userMoodle = $this->userModel->getUserInfoMoodle($element->email);
                        if (@$userMoodle[0]) {
                            $updateUserMoodle = $this->userModel->updateUserMoodle($requests,$userMoodle[0]->id);
                            info('success update profile Moodle');
                        }
                    }
                    // Register to Moodle \.end

                    // Register to OC \.start
                    $registerOC = $this->authRegis->registerOC($requests,'normaly',false,$element->password);
                    unset($user->customer_group_id);
                    if (!$registerOC->success) {
                        $requests->merge(['customer_id' => $user->customer_id]);
                        $updateUserOC = $this->userModel->updateUserOC($requests,'bulk');
                        if ($updateUserOC) {
                            info('success update profile OC');
                        }
                    } else {
                        $user->update(['customer_id' => $registerOC->data->customer_id]);
                    }
                    // Register to OC \.end

                    // Create log role
                    if (@$user) {
                        $storeLogRole = LogRole::firstOrCreate(['user_id' => $user->id, 'username' => $user->email, 'password' => $element->password, 'default' => 1, 'active' => 1]);
                    }

                } else {
                    if ($findAvailabeData) {
                        $user = Sentinel::findById($findAvailabeData->id);
                        Sentinel::update($user, [
                            'first_name' => $element->first_name,
                            'last_name' => $element->last_name,
                            'password' => $element->password,
                            'phone' => $element->phone
                        ]);

                        if ($user->roles()->first()) {
                            $user->roles[0]->users()->detach( $user );
                        }

                        $role = Sentinel::findRoleBySlug('standard-customer');
                        $role->users()->attach($user);

                        // update profile Moodle
                        $userMoodle = $this->userModel->getUserInfoMoodle($element->email);
                        if (@$userMoodle[0]) {
                            $updateUserMoodle = $this->userModel->updateUserMoodle($requests,$userMoodle[0]->id,'update-password');
                            info('success update profile Moodle');
                        }

                        //update profile OC
                        $requests->merge(['customer_id' => $user->customer_id]);
                        $updateUserOC = $this->userModel->updateUserOC($requests);
                        if ($updateUserOC) {
                            info('success update profile OC');
                        }

                        // update log user
                        LogRole::where('username',$element->email)
                            ->update(['password' => $element->password]);
                    }
                    
                }

                $successBulk++;

            } catch (\Exception $e) {
                \DB::rollback();
                // sessionFlash('Something went wrong!','error');
            }
        }
        \DB::commit();
    
        //inser log
        BulkLog::insert([
            'user_id' => $this->user->id,
            'data' => json_encode($dataFailed['email']),
            'description' => 'Bulk user success: '.$successBulk.' failed: '.count($dataFailed['email']),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        if(count($dataExists['email']) > 0){
            // $this->userModel->process_workflow( $dataExists['email'], true, 'storeBulk', $this->user->email );
        }

        if(count($dataFailed['email']) > 0){
            // $this->userModel->process_workflow( $dataFailed['email'], true, 'storeBulk', $this->user->email );
        }

        // if($invalidData['invalid'] > 0){
        // }

        if(count($dataExists['email']) == 0 && $invalidData['invalid'] == 0) {
            // $this->userModel->process_workflow(null, null, 'storeBulk', $this->user->email);
        }
    }
}
