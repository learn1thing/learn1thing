<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Classes\CurlRequest;
use GuzzleHttp\Client;
use App\Models\OrderHistory;
use App\Models\OrderHistoryDetail;

class BulkOrder implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    protected $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data['order'];
        $this->user = $data['user_login'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $orderDetails = $this->data->details;
        if ($this->user->inRole('business-partner')) {
            $orderDetails = $this->data->details()->join('products','products.product_code','=','order_details.product_code')
                ->where('products.user_id',$this->user->id)
                ->select('order_details.id','order_details.order_id','order_details.product_code','order_details.quantity',
                    'order_details.created_at','order_details.updated_at','order_details.status',
                    'products.status as product_status','products.model','products.quantity as product_qty')
                ->get();
        }

        $customer = $this->data->user;
        $country_detail = null;
        if (count($orderDetails) > 0) {
            $productTemps = [];
            $products = [];
            $quantityProd = 0;
            $productCode = 0;
            $tempX = 0;
            foreach ($orderDetails as $key => $detail) {
                if ($detail->product_status == '0' || $detail->product_status == 0) {
                    array_push($productTemps, $detail->product_code);
                    $sendData = [
                        'model' => $detail->model,
                        'quantity' => $detail->product_qty,
                        'status' => "1",
                    ];
                    $updateProduct = CurlRequest::sendRequest('PUT',env('OC_BASE_URL').'/index.php?route=rest/product_admin/products&id='.$detail->product_code,null,json_encode($sendData));
                    \DB::table('products')->where('product_code','=',$detail->product_code)->update(['status' => 1]);
                }
                // if ($this->user->inRole('business-partner')) {
                $tempArray = [
                    'product_id' => $detail->product_code,
                    'quantity' => $detail->quantity,
                ];
                array_push($products, $tempArray);
                    // $products[$key]['product_id'] = $detail->product_code;
                    // $products[$key]['quantity'] = $detail->quantity;
                // } elseif ($this->user->inRole('super-admin')) {
                    // for ($i=$tempX; $i < $detail->quantity; $i++) { 
                    //     $products[$i]['product_id'] = $detail->product_code;
                    //     $products[$i]['quantity'] = '1';
                    //     $tempX++;
                    // }
                // }
            }
            // if ($this->user->inRole('business-partner')) {
            //     $products[0]['product_id'] = $productCode;
            //     $products[0]['quantity'] = $quantityProd;
            // }

            if(isset($customer->country)) {
                $client = new Client();
                $get_country_detail = $client->request('GET', env('OC_BASE_URL').'/index.php?route=feed/rest_api/countries&id=' . $customer->country, [
                    'headers' => [
                      'X-Oc-Merchant-Id' => 12345,
                    ]
                ]);

                $country_detail = json_decode($get_country_detail->getBody())->data;
                $zones = $country_detail->zone;  
            } else {
                $zones = array();
            }
            $dataZone = [];
            if (count($zones) > 0) {
                foreach ($zones as $zone) {
                    if ($zone->zone_id == $customer->region) {
                        $dataZone['zone_id'] = $zone->zone_id;
                        $dataZone['country_id'] = $zone->country_id;
                        $dataZone['name'] = $zone->name;
                        $dataZone['status'] = $zone->status;
                        $dataZone['code'] = $zone->code;
                    }
                }
            }

            $body = [
                'customer' => [
                    'customer_id' => $customer->customer_id,
                    'customer_group_id' => 1,
                    'firstname' => $customer->first_name,
                    'lastname' => $customer->last_name,
                    'telephone' => $customer->phone,
                    'fax' => "1",
                    'email' => $customer->email,
                    'custom_ield' => '',
                ],
                'payment_address' => [
                    "firstname" => $customer->first_name,
                    "lastname" => $customer->last_name,
                    "company" => "company",
                    "company_id" => "company",
                    "tax_id" => "1",
                    "address_1" => $customer->address_1,
                    "address_2" => $customer->address_2,
                    "postcode" => $customer->post_code,
                    "city" => $customer->city,
                    "zone_id" => $customer->region,
                    "zone" => (@$customer->region) ? $dataZone['name'] : '',
                    "zone_code" => (@$customer->region) ? $dataZone['code'] : '',
                    "country_id" => $customer->country,
                    "country" => ($customer->country) ? $country_detail->name : ''
                ],
                'payment_method' => [
                    'title' => 'Cash On Delivery',
                    'code' => 'cod'
                ],
                'shipping_address' => [
                    "firstname" => $customer->first_name,
                    "lastname" => $customer->last_name,
                    "company" => "company",
                    "company_id" => "company",
                    "tax_id" => "1",
                    "address_1" => $customer->address_1,
                    "address_2" => $customer->address_2,
                    "postcode" => $customer->post_code,
                    "city" => $customer->city,
                    "zone_id" => $customer->region,
                    "zone" => (@$customer->region) ? $dataZone['name'] : '',
                    "zone_code" => (@$customer->region) ? $dataZone['code'] : '',
                    "country_id" => $customer->country,
                    "country" => ($customer->country) ? $country_detail->name : ''
                ],
                'shipping_method' => [
                    'title' => 'Free Shipping',
                    'code' => 'free'
                ],
                "comment" => "test comment",
                "affiliate_id" => "",
                "commission" => "",
                "marketing_id" => "",
                "tracking" => "",
                "products" => $products
            ];

            $responses = CurlRequest::sendRequest('POST',env('OC_BASE_URL').'/index.php?route=rest/order_admin/orderv2',null,json_encode($body));

            foreach ($orderDetails as $key => $detail) {
                if (in_array($detail->product_code, $productTemps)) {
                    $sendData = [
                        'model' => $detail->model,
                        'quantity' => $detail->product_qty,
                        'status' => "0",
                    ];
                    $updateProduct = CurlRequest::sendRequest('PUT',env('OC_BASE_URL').'/index.php?route=rest/product_admin/products&id='.$detail->product_code,null,json_encode($sendData));
                    \DB::table('products')->where('product_code','=',$detail->product_code)->update(['status' => 0]);
                }
            }

            if (@$responses->success ) {
                info('success-order');
                $body = ['order_status_id' => 5, 'notify' => 1, 'comment' => 'Payment has been completed'];
                $updateOrderHistory = CurlRequest::sendRequest('PUT',env('OC_BASE_URL').'/index.php?route=rest/order_admin/orderhistory&id='.$responses->data->id,null,json_encode($body));
                if (@$updateOrderHistory->success) {
                    info('success-update-order');
                    return true;
                }
                // $updateOrderHistory = CurlRequest::sendRequest('PUT',env('OC_BASE_URL').'/index.php?route=rest/order_admin/orderstatus&id='.$responses->data->id,null,json_encode(['status' => 'complete']));
                //save order history
                // $orderHistory = new OrderHistory;
                // $orderHistory->create(['order_code' => $responses->data->id, 'customer_id' => $customer->customer_id]);

                // //save order history detail
                // foreach ($products as $product) {
                //     $orderHistDetail = new OrderHistoryDetail;
                //     $orderHistDetail->create(['order_history_id' => $orderHistory->id, 'product_id' => $product->product_id]);
                // }
            } else {
                info('failed-order');
                return false;
            }
        }
    }
}
