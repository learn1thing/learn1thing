<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Excel;
use App\Models\CorporateGroup;
use App\Models\BusinessPartnerGroup;
use App\Models\Role;
use Illuminate\Http\Request;
use Sentinel;
use App\Models\LogRole;
use App\Models\User;
use App\Http\Controllers\Auth\Api\AuthenticateController;
use App\Models\BulkLog;

class BulkUser implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $request;
    protected $user;
    protected $userModel;
    protected $authRegis;
    protected $pathFile;
    protected $dataExport;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->request = $data['request'];
        $this->user = $data['user'];
        // $this->userModel = new User;
        $this->authRegis = new AuthenticateController;
        $this->pathFile = $data['pathFile'];
        $this->dataExport = $data['dataExport'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $model = $this->getModel();
        $request = $this->request;
        $pathFile = $this->pathFile;
        $dataExport = $this->dataExport;
        $this->userModel = new User;

        $dataExists['email'] = array();
        $invalidData['invalid'] = 0;
        $dataFailed['email'] = array();

        $rolesDetail = $this->getRoleDetail($request);

        $successBulk = 0;

        \DB::beginTransaction();
        foreach ($dataExport as $element) {
            try
            {
                if (empty($element->email)) {
                    continue;
                }
                if (strlen($element->password) < 8 || empty($element->first_name) || empty($element->last_name)) {
                    array_push($dataFailed['email'], $element->email);
                    continue;
                }

                $findAvailabeData = false;
                $requests = new Request;
                $requests->merge(['email' => $element->email, 'first_name' => $element->first_name, 
                    'last_name' => $element->last_name, 'phone' => $element->phone, 
                    'new_password' => $element->password, 'password' => $element->password]);

                if($element->email === ""){
                    $invalidData['invalid'] = $invalidData['invalid'] + 1;
                } else {
                    $findAvailabeData = $this->userModel->where('email',$element->email)->where('parent_id',$this->user->id)->first();
                }
                info(json_encode($findAvailabeData));
                if(!$findAvailabeData && !empty($element->email)){
                    if ($this->userModel->whereEmail($element->email)->first()) {
                        array_push($dataExists['email'], $element->email);
                        continue;
                    }
                    $user = Sentinel::registerAndActivate([
                        'first_name' => $element->first_name,
                        'last_name' => $element->last_name,
                        'email' => $element->email,
                        'phone' => $element->phone,
                        'password' => $element->password,
                        'department_id' => (!empty($request['department_id'])) ? $request['department_id'] : null,
                        'parent_id' => $this->user->id,
                        'related_id' => $request['related_id'],
                        'related_type' => ($this->user->inRole('business-partner') || @$this->user->related_type == 'BusinessPartnerGroup') ? BusinessPartnerGroup::class : CorporateGroup::class
                    ]);

                    // attach role
                    Sentinel::findRoleById( $rolesDetail->id )->users()->attach($user);

                    $user = $this->userModel->find($user->id);
                    if ($this->user->roles[0]->slug == 'business-partner') {
                        $user->customer_group_id = 2;
                    } else {
                        $user->customer_group_id = 4;
                    }
                    $requests->merge(['customer_group_id' => $user->customer_group_id]);
                    // Register to Moodle \.start
                    $registerMoodle = $this->authRegis->registerMoodle($requests,'normaly',false,$element->password);
                    if (@$registerMoodle->exception == 'invalid_parameter_exception') {
                        // update profile Moodle
                        $userMoodle = $this->userModel->getUserInfoMoodle($element->email);
                        if (@$userMoodle[0]) {
                            $updateUserMoodle = $this->userModel->updateUserMoodle($requests,$userMoodle[0]->id);
                            info('success update profile Moodle');
                        }
                    }
                    // Register to Moodle \.end

                    // Register to OC \.start
                    $registerOC = $this->authRegis->registerOC($requests,'normaly',false,$element->password);
                    unset($user->customer_group_id);
                    if (!$registerOC->success) {
                        $requests->merge(['customer_id' => $user->customer_id]);
                        $updateUserOC = $this->userModel->updateUserOC($requests,'bulk');
                        if ($updateUserOC) {
                            info('success update profile OC');
                        }
                    } else {
                        $user->update(['customer_id' => $registerOC->data->customer_id]);
                    }
                    // Register to OC \.end

                    // Create log role
                    if (@$user) {
                        $storeLogRole = LogRole::firstOrCreate(['user_id' => $user->id, 'username' => $user->email, 'password' => $element->password, 'default' => 1, 'active' => 1]);
                    }

                } else {
                    if ($findAvailabeData) {
                        $user = Sentinel::findById($findAvailabeData->id);
                        Sentinel::update($user, [
                            'first_name' => $element->first_name,
                            'last_name' => $element->last_name,
                            'password' => $element->password,
                            'phone' => $element->phone,
                            'related_id' => $request['related_id'],
                            'department_id' => (@$request['department_id']) ? $request['department_id'] : null,
                        ]);

                        if ($user->roles()->first()) {
                            $user->roles[0]->users()->detach( $user );
                        } else {
                            $role = new RoleUser;
                            $role->user_id = $user->id;
                            $role->role_id = $rolesDetail->id;
                            $role->save();
                        }
                        Sentinel::findRoleById( $rolesDetail->id )->users()->attach($user);

                        // update profile Moodle
                        $userMoodle = $this->userModel->getUserInfoMoodle($element->email);
                        if (@$userMoodle[0]) {
                            $updateUserMoodle = $this->userModel->updateUserMoodle($requests,$userMoodle[0]->id,'update-password');
                            info('success update profile Moodle');
                        }

                        //update profile OC
                        $requests->merge(['customer_id' => $user->customer_id]);
                        $updateUserOC = $this->userModel->updateUserOC($requests,'bulk');
                        if ($updateUserOC) {
                            info('success update profile OC');
                        }

                        // update log user
                        LogRole::where('username',$element->email)
                            ->update(['password' => $element->password]);
                    }
                    
                }

                $successBulk++;

            } catch (\Exception $e) {
                \DB::rollback();
                // sessionFlash('Something went wrong!','error');
            }
        }
        \DB::commit();
        
        //inser log
        BulkLog::insert([
            'user_id' => $this->user->id,
            'data' => json_encode($dataFailed['email']),
            'description' => 'Bulk user success: '.$successBulk.' failed: '.count($dataFailed['email']),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        if(count($dataExists['email']) > 0){
            $this->userModel->process_workflow( $dataExists['email'], true, 'storeBulk', $this->user->email );
        }

        if(count($dataFailed['email']) > 0){
            $this->userModel->process_workflow( $dataFailed['email'], true, 'storeBulk', $this->user->email );
        }

        // if($invalidData['invalid'] > 0){
        // }

        if(count($dataExists['email']) == 0 && $invalidData['invalid'] == 0) {
            $this->userModel->process_workflow(null, null, 'storeBulk', $this->user->email);
        }


    }

    protected function getModel()
    {
        if ($this->user->roles[0]->slug == 'business-partner') {
            $model = new BusinessPartnerGroup;
        } else {
            if ($this->user->parent_id) {
                $userCurrent = $this->userModel->find($this->user->parent_id);
                if ($userCurrent->inRole('business-partner')) {
                    $model = new BusinessPartnerGroup;    
                } else {
                    $model = new CorporateGroup;
                }
                return $model;
            }
            $model = new CorporateGroup;
        }
        return $model;
    }

    protected function getRoleDetail($request)
    {
        $upline = false;
        if ($this->user->upline)
            $upline = $this->user->upline->inRole('business-partner');

        if ($this->user->roles[0]->slug == 'business-partner' || $upline) {
            $rolesDetail = Role::join('business_partner_groups','business_partner_groups.id','roles.business_partner_group_id')
                ->select('roles.id','roles.name')
                ->where('roles.business_partner_group_id',$request['related_id']);
        } else {
            $rolesDetail = Role::join('corporate_groups','corporate_groups.id','roles.corporate_group_id')
                ->select('roles.id','roles.name')
                ->where('roles.corporate_group_id',$request['related_id']);
        }

        if ($this->user->parent_id) {
            $rolesDetail = $rolesDetail->where(function($query) {
                $query->where('created_by',$this->user->id)
                    ->orWhere('created_by',$this->user->parent_id);
            });
        } else {
            $childs = $this->user->downlines()->pluck('id')->toArray();
            $rolesDetail = $rolesDetail->where(function($query) use($childs) {
                $query->where('created_by',$this->user->id)
                    ->orWhereIn('created_by',$childs);
            });
        }

        return $rolesDetail->first();
    }
}
