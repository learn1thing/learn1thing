<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Mail\WorkflowMail;

use App\Models\Workflow;
use App\Models\EmailTemplate;


use Mail;

class WorkflowJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;


    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $data )
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    
    /**
     * Cari email yg di tuju
     * Jika Tidak ada ke email sendiri dlu
     */
    public function handle()
    {
        // \Log::info($this->data['workflow']->action_template);

        // $email = EmailTemplate::find($this->data['workflow']->action_template); // Query Content Template Email
        
        // $data['template'] = $email; // Simpen di data
        // $data['msg'] = $this->data['msg']; // Cek message jika ada
        
        //  //Mail::to($request->user())->send(new WorkflowMail( $data ) );
        // Mail::to( $this->data['to'] )->send(new WorkflowMail( $data ) );
        // \Log::info($this->data['to']);
        $to = $this->data['user'];
        $email = EmailTemplate::find($this->data['workflow']->action_template);
        $data['template'] = $email;
        $data['msg'] = $this->data['msg'];
        \Log::info($to);
        Mail::to( $to )->send(new WorkflowMail( $data ) );
    }
}
