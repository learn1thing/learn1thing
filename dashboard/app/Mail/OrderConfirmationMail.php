<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderConfirmationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.bulk_order_confirmation')
            ->subject('Bulk Order Confirmation')
            ->with([
                'name' => $this->data['user']->first_name.' '. $this->data['user']->last_name,
                'order' => $this->data['order'],
                'orderDetails' => $this->data['orderDetails'],
            ]);
    }
}
