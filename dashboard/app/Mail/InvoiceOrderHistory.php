<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvoiceOrderHistory extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $data )
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.coach_v2')
        ->subject('learn1thing:'.$this->data['data_products'][0]->name.' You have been scheduled for a coaching session')
        ->with([
            'coach' => $this->data['coach'][0],
            'user' => $this->data['user'],
            'customer' => $this->data['customer'],
            // 'link'  => $this->data['link'][0],
            'link'  => $this->data['course_link'],
            'bbb_link' => @$this->data['bbb_link'],
            'data_products'=>$this->data['data_products']
            ]);
    }
}
