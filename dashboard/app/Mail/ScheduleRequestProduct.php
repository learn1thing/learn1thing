<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ScheduleRequestProduct extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $data )
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.schedule_request_product')
            ->subject((!empty(@$this->data['subject'])) ? @$this->data['subject']: 'Request Schedule Order Product')
            ->with([
                'user' => $this->data['user'],
                'product' => $this->data['product'],
                'link_schedule'=>@$this->data['link_schedule'],
                'schedule'=>@$this->data['schedule'],
                'content'=>@$this->data['content'],
                'link_bbb'=>@$this->data['link_bbb'],
                'type_email'=>@$this->data['type_email'],
                'title'=>@$this->data['subject']
            ]);
    }
}
