<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendProduct extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.send_product')
            ->subject((!empty(@$this->data['title_email'])) ? @$this->data['title_email']: 'Your Product Request has been Approved')
            ->with([
                'user' => $this->data['user'],
                'product' => $this->data['product'],
                'profiler_url' =>(is_object($this->data['profiler_url']))? json_decode($this->data['profiler_url'][0]->link): $this->data['profiler_url'],
                'courses' => $this->data['courses'],
                'title_email'=>$this->data['title_email']
            ]);
    }
}
