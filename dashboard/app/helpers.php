<?php

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Intervention\Image\ImageManager;
// use Intervention\Image\Facades\Image;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\LogRole;
use App\Models\User;

if (! function_exists('user_info')) {
    /**
     * Get logged user info.
     *
     * @param  string $column
     * @return mixed
     */
    function user_info($column = null)
    {
        if ($user = Sentinel::check()) {
            if (is_null($column)) {
                return $user;
            }

            if ('full_name' == $column) {
                return user_info('first_name').' '.user_info('last_name');
            }

            if ('role' == $column) {
                return user_info()->roles[0];
            }

            if ('payment_method' == $column) {
                return user_info()->paymentMethod;
            }

            if ('merchant_page' == $column) {
                return env('OC_BASE_URL').'/index.php?route=product/store&p='.base64_encode(user_info('email')).'&username='.strtolower(user_info('first_name')).'-'.strtolower(user_info('last_name'));
            }

            return $user->{$column};
        }

        return null;
    }
}

if (! function_exists('link_to_avatar')) {
    /**
     * Generates link to avatar.
     *
     * @param  null|string $path
     * @return string
     */
    function link_to_avatar($path = null)
    {
      //  if (is_null($path) || ! file_exists(avatar_path($path))) {
        if (is_null($path)) {
            return 'https://s3.amazonaws.com/wll-community-production/images/no-avatar.png';
        }

        return url('/').DIRECTORY_SEPARATOR.trim($path, DIRECTORY_SEPARATOR);
    }
}

if (! function_exists('sessionFlash')) {
    function sessionFlash($message,$type)
    {
        session()->put('notification',[
            'message' => $message,
            'alert-type' => $type,
        ]);
    }
}

if (! function_exists('get_date_index')) {
    /**
     * Generates link to avatar.
     * @author ali
     * @param  null|string $dateAlias
     * @return string
     */
    function get_date_index($dateAlias)
    {
        $date = [
                    'Mon' => 'Monday',
                    'Tue' => 'Tuesday',
                    'Wed' => 'Wednesday',
                    'Thu' => 'Thursday',
                    'Fri' => 'Friday',
                    'Sat' => 'Saturday',
                    'Sun' => 'Sunday' 
                ];

        return $date[$dateAlias];

    }
}

if (! function_exists('url_to_image')) {
    /**
     * Generates link to avatar.
     *
     * @param  null|string $path
     * @return string
     */
    function url_to_image($name_image,$type=false)
    {
        if (!$type) {
            $path = Config::get('services.public_path.image.full_path');
            $pattern_path = Config::get('services.public_path.image.pattern_path');
        }

        if ($type) {
            $path = Config::get('services.public_path.'.$type.'.full_path');
            $pattern_path = Config::get('services.public_path.'.$type.'.pattern_path');

        }

        if (is_null($name_image) || ! file_exists($path) || empty($name_image)) {
            if ($type == 'logo') {
                return env('APP_URL').'/images/default.png';
            } elseif ($type == 'banner') {
                return env('APP_URL').'/images/banner-default.jpg';
            }
            return 'https://s3.amazonaws.com/wll-community-production/images/no-avatar.png';
        }

        return env('APP_URL').$pattern_path.'/'.$name_image;
    }
}


function baseEncrypt($value)
{
    return strtr(base64_encode(openssl_encrypt($value, 'AES-128-ECB', 'l1tmiddleware')), '+/=', '-_,');
}

function baseDecrypt($value)
{
    return  openssl_decrypt(base64_decode(strtr($value, '-_,', '+/=')), 'AES-128-ECB', 'l1tmiddleware');
}

if (! function_exists('upload_file')) {
    function upload_file($data, $filepath = 'uploads/', $filetype = 'image', $type = 'public')
    {
        if (!empty($data) && $data->isValid()) {
            $fileExtension = strtolower($data->getClientOriginalExtension());
            $newFilename = str_random(20) . '.' . $fileExtension;

            if(!File::exists($filepath)) {
                File::makeDirectory($filepath, $mode = 0777, true, true);
            }

            if($filetype == 'image'){
                $file = Image::make($data);
                $file->save($filepath.$newFilename);
                $compressedImage = compress_image($filepath.$newFilename);
                $imageThumbnail = image_thumbnail($filepath.$newFilename);
            }else{
                $file = $data->move($filepath, $newFilename);
                $compressedImage = '';
                $imageThumbnail = '';
            }
            $result['original'] = $filepath.$newFilename;
            $result['standard'] = $compressedImage;
            $result['thumbnail'] = $imageThumbnail;

            return  $result;
        }
        
        return '';
    }
}

if (! function_exists('get_file')) {
    function get_file($path, $preview = 'compressed', $type = 'public')
    {
        $path_default = 'http://vignette2.wikia.nocookie.net/legendmarielu/images/b/b4/No_image_available.jpg/revision/latest?cb=20130511180903';
        if(! File::exists($path)) {
            return URL::to($path_default);
        }

        if($type == 'public'){
            if($preview == 'thumbnail'){
                return URL::to(dirname($path).'/thumb/'.basename($path));
            }else{
                return URL::to($path);
            }

        }else{
            //storage path
        }
    }
}

if (! function_exists('delete_file')) {
    function delete_file($path, $type = 'public')
    {
        if($type == 'public'){
            $dirname = dirname($path);
            $filename = basename($path);
            if(file_exists(public_path().'/'.$path)){
                File::delete($path); // original
            }
            
            if(file_exists(public_path().'/'.$dirname.'/compressed/'.$filename)){
                File::delete($dirname.'/compressed/'.$filename);
            }

            if(file_exists(public_path().'/'.$dirname.'/thumb/'.$filename)){
                File::delete($dirname.'/thumb/'.$filename);
            }
        }else{
            if(Storage::has($path)){
                return Storage::delete($path);
            }
        }
    }
}

if (! function_exists('compress_image')) {
    function compress_image($path, $width = 1366, $type = 'public')
    {
        if($type == 'public'){
            $thumb_path = public_path().'/'.dirname($path).'/compressed/';
            list($img_width, $img_height) = getimagesize(public_path().'/'.$path);

            if($img_width < $width){
                $width = $img_width;
            }

            if(!File::exists($thumb_path)) {
                File::makeDirectory($thumb_path, $mode = 0777, true, true);
            }

            $img = Image::make(public_path() .'/'.$path);
            $img->resize($width, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumb_path.basename($path), 20);

            return dirname($path).'/compressed/'.basename($path);
        }else{
            //storage path
        }
    }
}

if (! function_exists('image_thumbnail')) {
    function image_thumbnail($path, $width = 350, $type = 'public')
    {
        if($type == 'public'){
            $thumb_path = public_path().'/'.dirname($path).'/thumb/';
            list($img_width, $img_height) = getimagesize(public_path().'/'.$path);

            if($img_width < $width){
                $width = $img_width;
            }

            if(!File::exists($thumb_path)) {
                File::makeDirectory($thumb_path, $mode = 0777, true, true);
            }

            $img = Image::make(public_path() .'/'.$path);
            $img->resize($width, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            
            $img->save($thumb_path.basename($path), 60);

            return dirname($path).'/thumb/'.basename($path);
        }else{
            //storage path
        }
    }
}

if (! function_exists('upload_image')) {
    function upload_image($data, $filepath = 'uploads/', $filetype = 'image', $type = 'public')
    {
        if (!empty($data) && $data->isValid()) {
            $fileExtension = strtolower($data->getClientOriginalExtension());
            $newFilename = str_random(20) . '.' . $fileExtension;

            $fullPath = public_path().'/'.$filepath;
            if(!File::exists($fullPath)) {
                File::makeDirectory($fullPath, $mode = 0777, true, true);
            }

            if($filetype == 'image'){
                $file = Image::make($data);
                $file->save($fullPath.'/'.$newFilename);
                $compressedImage = compress_image($filepath.'/'.$newFilename);
                $imageThumbnail = image_thumbnail($filepath.'/'.$newFilename);
            }else{
                $file = $data->move($fullPath, $newFilename);
                $compressedImage = '';
                $imageThumbnail = '';
            }
            $result['original'] = $newFilename;
            $result['standard'] = $compressedImage;
            $result['thumbnail'] = $imageThumbnail;

            return  $result;
        }
        
        return '';
    }
}

if (! function_exists('delete_image')) {
    function delete_image($path, $type = 'public')
    {
        if($type == 'public'){
            $dirname = dirname($path);
            $filename = basename($path);
            if(file_exists(public_path().'/'.$path)){
                File::delete(public_path().'/'.$path); // original
            }
            
            if(file_exists(public_path().'/'.$dirname.'/compressed/'.$filename)){
                File::delete(public_path().'/'.$dirname.'/compressed/'.$filename);
            }

            if(file_exists(public_path().'/'.$dirname.'/thumb/'.$filename)){
                File::delete(public_path().'/'.$dirname.'/thumb/'.$filename);
            }
        }else{
            if(Storage::has(public_path().'/'.$path)){
                return Storage::delete(public_path().'/'.$path);
            }
        }
    }
}

function get_role_slug($id)
{
    if ($id == 1) {
        $role_slug = 'standard-customer';
    } else if ($id == 2) {
        $role_slug = 'business-partner';
    } else if ($id == 4 ) {
        $role_slug = 'corporate-customer';
    }

    return $role_slug;
}

function getRoleMoodle($id)
{
    // 1 = standard-customer, 2 = business-parner, 4 = corporate-customer

    $role['id'] = 5; // is student
    $role['role_name'] = 'student'; // is student
    
    if ($id == 1 || $id == 4 ) {
        $role['id'] = 5; // is student
        $role['role_name'] = 'student'; // is student
        $role['contextid'] = 2; // is student
    } else if ($id == 2) {
        $role['id'] = 2;
        $role['role_name'] = 'manager'; 
        $role['contextid'] = 1;
    }

    return $role;
}

function date_range(Carbon\Carbon $from, Carbon\Carbon $to, $inclusive = true)
{
    if ($from->gt($to)) {
        return null;
    }

    // Clone the date objects to avoid issues, then reset their time
    $from = $from->copy()->startOfDay();
    $to = $to->copy()->startOfDay();

    // Include the end date in the range
    if ($inclusive) {
        $to->addDay();
    }

    $step = Carbon\CarbonInterval::day();
    $period = new DatePeriod($from, $step, $to);

    // Convert the DatePeriod into a plain array of Carbon objects
    $range = [];

    foreach ($period as $day) {
        $range[] = new Carbon\Carbon($day);
    }

    return ! empty($range) ? $range : null;
}

function log_roles( $email )
{
    $li = '';
    if (session()->has('email_refer')) {
        $email = session()->get('email_refer',$email);
    }
    $user = LogRole::where( 'username', $email )->first();
    $addLink = ' <li>
        <a href="'.route('admin.user.link-account').'" class=""><i class="fa fa-link"></i> Manage Account</a>
      </li>';
        
    if (!empty($user)) {
          $user_id = $user->user_id;
          $logs = LogRole::where( 'user_id', $user_id )->get();
    
        foreach ($logs as $log) {
            $userDetail = User::join('activations','activations.user_id','=','users.id')
                ->select('users.id','users.first_name','users.last_name','users.email','users.image','activations.completed')
                ->where('email',$log->username)->first();
            if ($userDetail) {
                $avatar = isset($userDetail->image) ? url_to_image($userDetail->image):link_to_avatar();
               $li .= ' <li'.((!$userDetail->completed) ? ' style="background:#f1f1f1;"' : '' ).'>
                            <a href="'. (($userDetail->completed) ? route( "change.role", $log->username ) : "#") .'" '.((!$userDetail->completed) ? ' style="cursor:not-allowed;"' : '' ).'>
                                
                                <h4 style="margin-left:0px">
                                    '.$userDetail->first_name.' '.$userDetail->last_name.'
                                    <small '.((!$userDetail->completed) ? ' style="color:red;"' : '' ).'>'.(($userDetail->completed) ? $userDetail->roles[0]->name : 'Deactive').'</small>
                                </h4>
                                <p style="margin-left:0px">'.$userDetail->email.'</p>
                            </a>
                          </li>';
            }
        }
    }
info('email_refer: '.$email);
    if ($email == user_info('email')) {
        $li .= $addLink;
    }
    // if (@user_info()->inRole('standard-customer') || @user_info()->inRole('corporate-customer') ) {
        // $li .= $addLink;
    // }

    return $li;
}

function remove_space( $string )
{
    $replace = str_replace(' ', '_', $string);

    return $replace;
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function orderDetailProductOrder($userId, $productId, $orderDetailId) {
    $orderProduct = new \App\Models\OrderProduct;
    $product = $orderProduct->where('product_code', '=', $productId)
        // ->where(function($query) {
        //     $query->where('status', 2)
        //     $query->orWhere('status', 1)
        // })
        ->where('order_detail_id', $orderDetailId)
        ->where('user_id', $userId)
        ->first();

    return $product;
}