<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use DB;

//use Illmunate\Support\Facades\URL;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
	//URL::forceSchema('https');

        \Validator::extend('beetwenSchedule', function ($attribute, $value, $parameters, $validator) {
            if (date('H:i:s', strtotime($value)) > date('H:i:s', strtotime($parameters[0]))) {
                return true;
            }

            return false;
        });

        \Validator::extend('iunique', function ($attribute, $value, $parameters, $validator) {
            $query = DB::table($parameters[0])
                // ->where($parameters[1],'like','%'.$value.'%')
                ->where(\DB::raw("lower(`".$parameters[1]."`)"),'=',strtolower($value))
                ->where('created_by',user_info('id'))
                ->first();

            if ($query) {
                return false;
            }

            return true;
        });

        \Validator::extend('uniqueDepartment', function ($attribute, $value, $parameters, $validator) {
            $query = DB::table($parameters[0])
                ->where(\DB::raw("lower(`".$parameters[1]."`)"),'=',strtolower($value))
                ->where('user_id',user_info('id'));

            if ( @$parameters[2] ) {
                $query = $query->where('id','!=',$parameters[2]);
            }

            if ($query->first()) {
                return false;
            }

            return true;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
       //$this->app['request']->server->set('HTTPS',true);
    }
}
