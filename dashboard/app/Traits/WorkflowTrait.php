<?php 

/**
 * @author Yoga <thetaramolor@gmail.com>
 */

namespace App\Traits;

use Illuminate\Support\Facades\Route;

use App\Models\Workflow;
use App\Models\User;

use App\Jobs\WorkflowJob;

trait WorkflowTrait 
{	

	/**
	 * Ngetest Heula
	 *
	 * @param      <type>  $data   The data
	 */
	public function checking( $testing = null, $failed = null )
	{
		$method = static::getNameFunction( Route::currentRouteAction() );

		if ($failed == 6) {
			$data['workflow'] = Workflow::where( 'event_trigger', $method )->where( 'action_template', 6 )->first();
		} else {
			$data['workflow'] = Workflow::where( 'event_trigger', $method )->first();
		}
		
		$data['data'] = $this;
		$data['msg'] = static::msg_email( $testing );
		$data['to'] = \Sentinel::getUser()->email;
		
		$job = (new WorkflowJob($data) )->onQueue('default');

        dispatch($job);
        
	}

	public static function msg_email( $email )
	{
		$msg = null;
		if ($email != null) {
			
			$msg = implode('<br>', $email);
		}

		return $msg;
	}

	public static function getNameFunction( $name )
	{
		$name_function = explode('@', $name);

		return $name_function[1];
	}

	/**
	 * 1. Cek Penerimanya ada atau tidak
	 * 2. Cek Email Template Bedasarkan Event Trigger
	 * 3. Cek Bedasarkan Role
	 */
	public function process_workflow( $data_fail = null, $failed = null, $route = null, $recipient = null )
	{
		if ($route) {
			$method = $route;
		} else {
			$method = static::getNameFunction( Route::currentRouteAction() ); // Mengambil Route dari Action
		}
		
		// Get Request Parameter
		$request = $this;
		// print_r(\Sentinel::getUser()->email); exit;
		$user = '';
		
		// Cek Jika Bulk User Request Fail
		$fail = $failed == true ? 6 : '';
		// Cek Method nya
		
		//var_dump($method);

		$query = Workflow::where( 'event_trigger', $method );
		if ($failed == true) {
			$query->where( 'action_template', $fail );
		}
		$workflow = $query->first();

		// Cek Recipent
		if ($recipient) {
			$user = $recipient;
		} else {
			if ( !empty($workflow->recipient_user_id) ) {

				if(User::find( $workflow->recipient_user_id )==NULL){
					$user = \Sentinel::getUser()->email;
				}else{
					$user = User::find( $workflow->recipient_user_id )->email;
				}
				
			} else {
				$user = \Sentinel::getUser()->email;
			}
		}
		
		$data['workflow'] = $workflow;
	//	print_r($data['workflow']); exit;
		
		$data['data'] = $data;
		$data['user'] = $user;

		$data['msg'] = static::msg_email( $data_fail );
		
		$job = (new WorkflowJob($data) )->onQueue('default');

        dispatch($job);
		
	}

}
