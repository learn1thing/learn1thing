<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Username or Password is wrong.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'expired' => 'Your Login Has been Expired, Please Login Again',
    'force_logout' => 'Sorry Your Account has been used on another user',
    'unauthorized' => 'Unauthorized',
    'unprocessable_entity' => 'Unprocessable Entity',
    'success' => 'Successfully for login',
    'token_not_provide' => 'Token Not Provided',
    'success_token' => 'Successfully for get token',
    'success_logout' => 'Successfully For Logout',
    'failed_logout' => 'Fail for Logout',
    'guest' => 'Your dont\'t have credentials, please login',
    'waiting_verification' => 'Register has been successfully and waiting verification by admin'

];
