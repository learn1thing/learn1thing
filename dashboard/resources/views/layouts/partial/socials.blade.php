<div class="social-auth-links text-center">
    <p>- OR -</p>
    <a href="{{ url('social/redirect/facebook') }}" class="btn btn-block btn-social btn-facebook btn-flat">
        <i class="fa fa-facebook"></i> Sign in using Facebook
    </a>
    <a href="{{ url('social/redirect/google') }}" class="btn btn-block btn-social btn-google btn-flat">
        <i class="fa fa-google"></i> Sign in using Google+
    </a>
    <a href="{{ url('social/redirect/linkedin') }}" class="btn btn-block btn-social btn-linkedin btn-flat">
        <i class="fa fa-linkedin"></i> Sign in using LinkedIn
    </a>
    <a href="{{ url('social/redirect/twitter') }}" class="btn btn-block btn-social btn-twitter btn-flat" >
        <i class="fa fa-twitter"></i> Sign in using Twitter
    </a>
</div>