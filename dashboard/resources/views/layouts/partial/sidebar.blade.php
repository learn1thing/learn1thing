<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{isset(user_info()->image) ? url_to_image(user_info()->image):link_to_avatar()}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>
                    {{(user_info()->user) ? user_info()->user->name : user_info('full_name')}}
                </p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form (Optional)
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form> -->
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            @if(user_info()->inRole('super-admin'))
            <li class="header">EXTERNAL LINK</li>
                <li>
                  <a href="{{ env('MOODLE_BASE_URL_API').'/my' }}" target="_blank" data-step='2' data-intro='Hello step two!'><i class="fa fa-external-link"></i><span> Link To Purchases </span></a>

                </li>
            @elseif ( user_info()->inRole('business-partner') )
             <li data-tooltipClass='intro-menu' tooltipClass='intro-menu' data-step='1' data-intro='Link to Store leads you to the storefront where you can shop faster.
                You can see products, buy, and compare them.
                Here, you can also see our information and contact us.'>
                  <a href="{{ env('OC_BASE_URL') }}" target="_blank"  ><i class="fa fa-external-link" ></i><span> Link To Store </span></a>
                </li>
                <li>
                  <a href="{{ env('MOODLE_BASE_URL_API').'/my' }}" target="_blank" data-step='2' data-intro='Hello step two!'><i class="fa fa-external-link"></i><span> Link To Purchases </span></a>

                </li>
                <li data-tooltipClass='intro-menu' tooltipClass='intro-menu' data-step='3' data-intro='Link to Survey brings you to Profiler tools where you can manage the survey, formula, and reports.'>
                  <a href="{{ env('SURVEY_BASE_URL').'/'.Sentinel::getUser()->persistences->first()->code.'/surveys' }}" ><i class="fa fa-external-link" target="_blank"></i><span> Link To Survey </span></a>
                </li>


            @elseif (user_info()->inRole('standard-customer') || user_info()->inRole('corporate-customer') )
                <li class="header">EXTERNAL LINK</li>
                <li>
                  <a href="{{ env('OC_BASE_URL') }}" target="_blank"><i class="fa fa-external-link"></i><span> Link To Store </span></a>
                </li>
                <li data-tooltipClass='intro-menu' tooltipClass='intro-menu' data-step='2' data-intro='Link to Purchases takes you to our course site.
                Here, you can manage your own course!'>
                  <a href="{{ env('MOODLE_BASE_URL_API').'/my' }}" target="_blank"><i class="fa fa-external-link"></i><span> Link To Purchases </span></a>
                </li>
            @endif
            <!-- Menu for user group created by Business Partner -->
            @if(user_info('role')->name == "Coach" || user_info('role')->name == "coach" && user_info('role')->business_partner_id_group_id != null )
            <li class="header">EXTERNAL LINK</li>
                <li data-tooltipClass='intro-menu' tooltipClass='intro-menu' data-step='1' data-intro='Link to Store leads you to the storefront where you can shop faster.
                You can see products, buy, and compare them.
                Here, you can also see our information and contact us.'>
                  <a href="{{ env('OC_BASE_URL') }}" target="_blank"  ><i class="fa fa-external-link" ></i><span> Link To Store </span></a>
                </li>
                <li>
                  <a href="{{ env('MOODLE_BASE_URL_API').'/my' }}" target="_blank" data-step='2' data-intro='Hello step two!'><i class="fa fa-external-link"></i><span> Link To Purchases </span></a>

                </li>
                <li data-tooltipClass='intro-menu' tooltipClass='intro-menu' data-step='3' data-intro='Link to Survey brings you to Profiler tools where you can manage the survey, formula, and reports.'>
                  <a href="{{ env('SURVEY_BASE_URL').'/'.Sentinel::getUser()->persistences->first()->code.'/surveys' }}" ><i class="fa fa-external-link" target="_blank"></i><span> Link To Survey </span></a>
                </li>
            @endif
            <!-- END -->

            <!-- Menu for user group crated by Corporate Customer -->
            @if(user_info('role')->corporate_group_id != null)
            <li class="header">EXTERNAL LINK</li>
                <li data-tooltipClass='intro-menu' tooltipClass='intro-menu' data-step='1' data-intro='Link to Store leads you to the storefront where you can shop faster.
                You can see products, buy, and compare them.
                Here, you can also see our information and contact us.'>
                  <a href="{{ env('OC_BASE_URL') }}" target="_blank"  ><i class="fa fa-external-link" ></i><span> Link To Store </span></a>
                </li>
                <li>
                  <a href="{{ env('MOODLE_BASE_URL_API').'/my' }}" target="_blank" data-step='2' data-intro='Hello step two!'><i class="fa fa-external-link"></i><span> Link To Purchases </span></a>

                </li>
            @endif
            @if(user_info('related_type') == "App\Models\CorporateGroup" || user_info('related_type') == "App\Models\BusinessPartnerGroup")
                {{--<li class="header">EXTERNAL LINK</li>
                
                <li>
                  <a href="{{ env('MOODLE_BASE_URL_API').'/my' }}" target="_blank" data-step='2' data-intro='Hello step two!'><i class="fa fa-external-link"></i><span> Link To Purchases </span></a>

                </li>--}}
            @endif
            <!-- END -->

            <li class="header">MENU</li>
            <li class="{!! (url(route('admin-dashboard')) == Request::url()) ? 'active' : '' !!}">
                <a href="{{route('admin-dashboard')}}"><i class='fa fa-dashboard'></i> <span>Dashboard</span></a>
            </li>
            @if(user_info()->inRole('corporate-customer') || user_info('related_type') == "App\Models\CorporateGroup" )
                <li class="{!! (url(route('admin.corporate-customer.mini-store')) == Request::url()) ? 'active' : '' !!}">
                    <a href="{{route('admin.corporate-customer.mini-store')}}"><i class='fa fa-shopping-cart'></i> <span>Mini Store</span></a>
                </li> 
            @endif
            @if(user_info()->inRole('corporate-customer'))
                <li class="{!! (url(route('admin.corporate-customer.requested-product')) == Request::url()) ? 'active' : '' !!}">
                    <a href="{{route('admin.corporate-customer.requested-product')}}"><i class='fa fa-reply'></i> <span>Product Access Request</span></a>
                </li> 
            @endif
            @if(@user_info()->upline->related_type == 'App\Models\CorporateCustomer')
                <li class="{!! (url(route('admin.corporate-customer.request-product-list')) == Request::url()) ? 'active' : '' !!}">
                    <a href="{{route('admin.corporate-customer.request-product-list')}}"><i class='fa fa-reply'></i> <span>Requested Product Lists</span></a>
                </li> 
            @endif
            @if(user_info()->inRole('super-admin'))
                <li class="treeview{!! ((Request::is('admin/master/*') OR Request::is('admin/master')) AND !Request::is('admin/master/user/business-partner/coach/*')) ? ' active' : '' !!}">
                    <a href="#"><i class="fa fa-link"></i> <span>User Administration</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="treeview{!! (Request::is('admin/master/user/*') OR Request::is('admin/master/user')) ? ' active' : '' !!}">
                            <a href="#"><i class="fa fa-user"></i> <span>Type of User Accounts</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{!! (url(route('admin.standard-customer.index')) == Request::url() OR Request::is('admin/master/user/standard-customer*')) ? 'active' : '' !!}"><a href="{{route('admin.standard-customer.index')}}"><i class="fa fa-circle-o"></i> Standard Customer</a></li>
                                <li class="{!! (url(route('admin.corporate-customer.index')) == Request::url() OR Request::is('admin/master/user/corporate-customer*')) ? 'active' : '' !!}"><a href="{{route('admin.corporate-customer.index')}}"><i class="fa fa-circle-o"></i> Corporate Customer</a></li>
                                <li class="{!! (url(route('admin.business-partner.index')) == Request::url() OR Request::is('admin/master/user/business-partner*')) ? 'active' : '' !!}"><a href="{{route('admin.business-partner.index')}}"><i class="fa fa-circle-o"></i> Business Partner</a></li>
                            </ul>
                        </li>

                        <li class="{!! (Request::is('admin/master/permission*') OR Request::is('admin/master/permission') ) ? 'active' : '' !!}"><a href="{{route('admin.permission.index')}}"><i class="fa fa-exchange"></i>Permission</a></li>
                    </ul>
                </li>
            @endif

            @if(!user_info()->inRole('super-admin') && user_info()->hasAnyAccess('manage-product','all'))
                <li class="treeview{!! (Request::is('admin/product/*') OR Request::is('admin/product')) ? ' active' : '' !!}" data-tooltipClass='intro-menu' tooltipClass='intro-menu' data-step='4' data-intro='Product: You can manage your products here.
                You can add, edit, delete, and also combine products into one package.'>
                    <a href="#"><i class="fa fa-shopping-bag"></i> <span>Product</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{!! (url(route('admin.business-partner.product.list')) == Request::url()) ? 'active' : '' !!}">
                            <a href="{{route('admin.business-partner.product.list')}}"><i class='fa fa-cubes'></i> <span>Product List</span></a>
                        </li>
                        <li class="{!! (url(route('admin.business-partner.create-product')) == Request::url()) ? 'active' : '' !!}">
                            <a href="{{route('admin.business-partner.create-product')}}"><i class='fa fa-plus'></i> <span>Add Product</span></a>
                        </li>
                        @if(user_info()->hasAnyAccess('product-set','all'))
                            <li class="{!! (url(route('admin.business-partner.product')) == Request::url()) ? 'active' : '' !!}">
                                <a href="{{route('admin.business-partner.product')}}"><i class='fa fa-shopping-bag'></i> <span>Link Product To Course</span></a>
                            </li>
                        @endif
                        @if(user_info()->hasAnyAccess('bulk-product','all'))
                            <li class="{!! (url(route('admin.business-partner.bulk-product')) == Request::url()) ? 'active' : '' !!}">
                                <a href="{{route('admin.business-partner.bulk-product')}}"><i class='fa fa-upload'></i> <span>Bulk Products</span></a>
                            </li>
                        @endif
                        <li class="{!! (url(route('admin.business-partner.assign-product')) == Request::url()) ? 'active' : '' !!}">
                            <a href="{{route('admin.business-partner.assign-product')}}"><i class='fa fa-share'></i> <span>Assign Product</span></a>
                        </li>

                    </ul>
                </li>
            @endif

            @if(user_info()->hasAnyAccess('manage-schedule','all') || user_info()->hasAnyAccess('manage-course','all','manage-coach'))
                <li class="treeview{!! (Request::is('coach/*') OR Request::is('admin/coach')) ? ' active' : '' !!}" data-tooltipClass='intro-menu' tooltipClass='intro-menu' data-step='5' data-intro='Manage Coach Schedule: Schedules are set here. You enable to set schedules and delete them.'>
                    <a href="#"><i class="fa fa-mortar-board"></i> <span>Manage Coach Schedule</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                    @if( !user_info()->inRole('super-admin') )
                        @if(user_info()->hasAnyAccess('manage-course'))                    
                            <!-- <li class="{!! (url(route('coach.course.index')) == Request::url() OR Request::is('admin/coach/course/*')) ? 'active' : '' !!}">
                                <a href="{{route('coach.course.index')}}">
                                    <i class='fa fa-book'></i> <span>Master Course</span>
                                </a>
                            </li> -->
                        @endif
                        @if(user_info()->hasAnyAccess('manage-schedule','all'))
                            <li class="{!! (url(route('master.schedule')) == Request::url() OR Request::is('/master-schedule')) ? 'active' : '' !!}">
                                <a href="{{route('master.schedule')}}">
                                    <i class='fa fa-book'></i> <span>Master Schedule</span>
                                </a>
                            </li>
                            <!-- <li class="{!! (url(route('coach.schedule.index')) == Request::url() OR Request::is('admin/coach/schedule/*')) ? 'active' : '' !!}">
                                <a href="{{route('coach.schedule.index')}}">
                                    <i class='fa fa-calendar'></i> <span>Set Course Coach</span>
                                </a>
                            </li> -->
                        @endif
                        @if(user_info()->hasAnyAccess('manage-coach','all'))
                        <li class="{!! (url(route('coach.schedule.course.index')) == Request::url() OR Request::is('coach/schedule/*')) ? 'active' : '' !!}">
                            <a href="{{route('coach.schedule.course.index')}}">
                                <i class='fa fa-calendar'></i> <span>Schedule Coach</span>
                            </a>
                        </li>
                        @endif
                    @endif
                        @if(user_info()->hasAnyAccess('expired-schedule','all'))
                        <li class="{!! (url(route('master.schedule.expired.index')) == Request::url() OR Request::is('master-schedule/expired-schedule/*')) ? 'active' : '' !!}">
                            <a href="{{route('master.schedule.expired.index')}}">
                                <i class='fa fa-calendar'></i> <span>Setting Expired Schedule</span>
                            </a>
                        </li>
                        @endif
                    </ul>
                </li>
            @endif

            @if(user_info()->hasAccess('bulk-order'))
                {{--<li class="{!! (url(route('admin.order.product')) == Request::url()) ? 'active' : '' !!}">
                    <a href="{{route('admin.order.product')}}"><i class='fa fa-cubes'></i> <span>Product List</span></a>
                </li>--}}
            @endif

            @if(user_info()->hasAnyAccess('bulk-order','all','order-history'))
                <li class="treeview{!! (Request::is('admin/order/*') OR Request::is('admin/order')) ? ' active' : '' !!}">
                    <a href="#"><i class="fa fa-opencart"></i> <span>Order</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @if(user_info()->hasAnyAccess('manage-order','all'))
                            @if(user_info()->inRole('corporate-customer'))
                                <li class="{!! (url(route('admin.order.bulk.create')) == Request::url()) ? 'active' : '' !!}">
                                    <a href="{{route('admin.order.bulk.create')}}"><i class='fa fa-cart-arrow-down'></i> <span>Bulk Purchase Products</span></a>
                                </li>
                            @endif
                            @if(!user_info()->inRole('super-admin'))
                                <li class="{!! (url(route('admin.order.index')) == Request::url()) ? 'active' : '' !!}">
                                    <a href="{{route('admin.order.index')}}"><i class='fa fa-cart-arrow-down'></i> <span>Bulk Order Lists</span></a>
                                </li>
                            @endif
                        @endif
                        @if(user_info()->hasAnyAccess('order-history','all'))
                            <li class="{!! (url(route('admin.order.history')) == Request::url() OR Request::is('admin/order/order-history*')) ? 'active' : '' !!}">
                                <a href="{{route('admin.order.history')}}"><i class='fa fa-comment-o'></i> <span>Order History</span></a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif

            <!-- if(user_info()->inRole('corporate-customer') && strtolower(@user_info('payment_method')->name) == 'pay as you go') -->
            @if(user_info()->inRole('super-admin'))
                <li class="{!! (url(route('admin.bulk-user.index')) == Request::url() OR Request::is('admin/bulk-user*')) ? 'active' : '' !!}">
                    <a href="{{route('admin.bulk-user.index')}}"><i class='fa fa-users'></i> <span>Bulk User Consumer</span></a>
                </li>
                <li class="{!! (url(route('admin.assign-product.index')) == Request::url() OR Request::is('admin/assign-product-partner*')) ? 'active' : '' !!}">
                    <a href="{{route('admin.assign-product.index')}}"><i class='fa fa-reply'></i> <span>Assign To Partner</span></a>
                </li>
                <li class="{!! (url(route('admin.invoice.index')) == Request::url()) ? 'active' : '' !!}">
                    <a href="{{route('admin.invoice.index')}}"><i class='fa fa-newspaper-o'></i> <span>Invoice</span></a>
                </li>
                <li class="{!! (url(route('admin.change-payment-method.index')) == Request::url() OR Request::is('admin/change-payment-method*')) ? 'active' : '' !!}">
                    <a href="{{route('admin.change-payment-method.index')}}"><i class='fa fa-recycle'></i> <span>Request Change<br>Payment Method</span></a>
                </li>
            @endif

            @if(user_info()->hasAccess('all'))
                <li class="{!! (url(route('admin.coach.index')) == Request::url()) ? 'active' : '' !!}">
                    <a href="{{route('admin.coach.index')}}"><i class='fa fa-user'></i> <span>Coach lists</span></a>
                </li>
            @endif

            @if(!user_info()->inRole('super-admin') && user_info()->hasAnyAccess('manage-department','all'))
                <li class="{!! (Request::is('admin/department*') OR Request::is('admin/department')) ? ' active' : '' !!}">
                    <a href="{{route('admin.department.index')}}"><i class='fa fa-building'></i> <span>Department</span></a>
                </li>
            @endif

            @if(!user_info()->inRole('super-admin') && user_info()->hasAnyAccess('manage-group-user','all'))
                <li class="{!! (url(route('admin.business-partner.group-user')) == Request::url()) ? 'active' : '' !!}" data-tooltipClass='intro-menu' tooltipClass='intro-menu' data-step='8' data-intro='BP Group Management: Here you can manage your partner groups. You are allowed to make role groups, delete or edit them, and adjust their permission.'>
                    <a href="{{route('admin.business-partner.group-user')}}"><i class='fa fa-group'></i> <span>Business Partner Role List</span></a>
                </li>
            @endif

            @if(!user_info()->inRole('super-admin') && user_info()->hasAnyAccess('manage-group-corporate','all'))
                <li class="{!! (url(route('admin.corporate-customer.group-user')) == Request::url()) ? 'active' : '' !!}">
                    <a href="{{route('admin.corporate-customer.group-user')}}"><i class='fa fa-group'></i> <span>Corporate Role List</span></a>
                </li>
            @endif

            @if(user_info()->hasAnyAccess('bulk-user-creation'))
                <li class="{!! (url(route('admin.business-partner.bulk.index')) == Request::url()) ? 'active' : '' !!}" data-tooltipClass='intro-menu' tooltipClass='intro-menu' data-step='6' data-intro='Users: This is users management section. Here, you can create new users with role group you create in group management section. You are also permitted to edit and delete accounts.'>
                    <a href="{{route('admin.business-partner.bulk.index')}}"><i class='fa fa-user'></i> <span>Users</span></a>
                </li>
            @endif

            @if(!user_info()->inRole('super-admin') && user_info()->hasAnyAccess('bulk-user-creation','all'))
                <li class="{!! (url(route('admin.business-partner.bulk-user')) == Request::url()) ? 'active' : '' !!}" data-tooltipClass='intro-menu' tooltipClass='intro-menu' data-step='7' data-intro='Bulk Users Upload: Rather to create users one by one, here you can insert bulk of users at once by collecting the data in a CSV file and uploading it.'>
                    <a href="{{route('admin.business-partner.bulk-user')}}"><i class='fa fa-group'></i> <span>Bulk User Creation</span></a>
                </li>
            @endif

            <!-- modified by rdw k5 4/12/2017 -->
            @if(user_info()->hasAccess('all') || user_info()->hasAnyAccess('top-up-credit','all') || user_info()->inRole('corporate-customer') && user_info('payment_method_id')!=2 )
                <li class="{!! (Request::is('admin/credit*') OR Request::is('admin/credit')) ? ' active' : '' !!}">
                    <a href="{{route('admin.credit.index')}}"><i class='fa fa-money'></i> <span>Top Up </span></a>
                </li>
            @endif

            <!--
            koding lama 
             @if(user_info()->hasAccess('all') || user_info()->hasAnyAccess('top-up-credit','all'))
                <li class="{!! (Request::is('admin/credit*') OR Request::is('admin/credit')) ? ' active' : '' !!}">
                    <a href="{{route('admin.credit.index')}}"><i class='fa fa-money'></i> <span>Top Up</span></a>
                </li>
            @endif -->

            @if(user_info()->hasAnyAccess('super-admin','all'))
                <li class="{!! (Request::is('admin/register-page*') OR Request::is('admin/register-page')) ? ' active' : '' !!}">
                    <a href="{{route('admin.register-page.index')}}"><i class='fa fa-dashboard'></i> <span>Register Page</span></a>
                </li>
            @endif

            @if(user_info()->inRole('super-admin'))
                <!-- <li class="{!! (url(route('admin.setting')) == Request::url()) ? 'active' : '' !!}">
                    <a href="{{route('admin.setting')}}"><i class='fa fa-cog'></i> <span>Setting</span></a>
                </li> -->
            @endif


            @if(user_info()->hasAnyAccess('manage-coach','all'))
                <!-- <li class="treeview{!! (Request::is('coach/*') OR Request::is('coach')) ? ' active' : '' !!}">
                    <a href="#"><i class="fa fa-mortar-board"></i> <span>Coaching</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{!! (url(route('coach.schedule.course.index')) == Request::url() OR Request::is('coach/schedule/*')) ? 'active' : '' !!}">
                            <a href="{{route('coach.schedule.course.index')}}">
                                <i class='fa fa-calendar'></i> <span>Schedule Course</span>
                            </a>
                        </li>
                    </ul>
                </li> -->
            @endif
            
            @if(user_info()->inRole('corporate-customer') || user_info()->inRole('standard-customer'))
                <!-- <li>
                    <li>
                      <a href="http://store.learn1thing.com/" target="_blank"><i class="fa fa-external-link"></i> Link To Store</a>
                    </li>
                </li> -->
            @endif


            @if(user_info()->inRole('super-admin'))
                <li class="{!! (url(route('admin.workflow.index')) == Request::url()) ? 'active' : '' !!}">
                    <a href="{{route('admin.workflow.index')}}"><i class='fa fa-tasks'></i> <span>Workflow</span></a>
                </li>
                <li class="{!! (url(route('admin.emailtemplate.index')) == Request::url()) ? 'active' : '' !!}">
                    <a href="{{route('admin.emailtemplate.index')}}"><i class='fa fa-envelope-o'></i> <span>Email Template</span></a>
                </li>
            @endif
            <li class="treeview">
                <a href="#"><i class="fa fa-question-circle"></i> <span>Help</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @if(user_info()->inRole('super-admin'))
                        <li>
                            <a href="{{ route('admin.business-partner.download-guide')}}"><i class='fa fa-file-pdf-o'></i> <span>User Guide</span></a>
                        </li>
                        <li>
                            <a href="{{ route('admin.business-partner.download-oc-guide')}}"><i class='fa fa-file-pdf-o'></i> <span>OpenCart Theme< User Guide</span></a>
                        </li>
                        <li>
                            <a href="{{env('APP_URL').'/user_guide/learn1thing-superadmin-guide.pdf'}}" download><i class='fa fa-file-pdf-o'></i> <span>User Guide Super Admin</span></a>
                        </li>
                    @endif

                    @if(user_info()->inRole('business-partner'))
                        <li>
                            <a href="{{env('APP_URL').'/user_guide/learn1thing-business-partner-guide.pdf'}}" download><i class='fa fa-file-pdf-o'></i> <span>User Guide</span></a>
                        </li>
                    @endif

                    @if(user_info()->inRole('corporate-customer'))
                        <li>
                            <a href="{{env('APP_URL').'/user_guide/learn1thing-corporate-customer-guide.pdf'}}" download><i class='fa fa-file-pdf-o'></i> <span>User Guide</span></a>
                        </li>
                    @endif

                    @if(user_info()->inRole('standard-customer'))
                        <li>
                            <a href="{{env('APP_URL').'/user_guide/learn1thing-standard-customer-guide.pdf'}}" download><i class='fa fa-file-pdf-o'></i> <span>User Guide</span></a>
                        </li>
                    @endif

                    @if(@user_info('related_type') == 'App\Models\CorporateGroup')
                        <li>
                            <a href="{{env('APP_URL').'/user_guide/learn1thing-under-corporate-customer-guide.pdf'}}" download><i class='fa fa-file-pdf-o'></i> <span>User Guide</span></a>
                        </li>
                    @endif

                    @if(@user_info('related_type') == 'App\Models\BusinessPartnerGroup')
                        <li>
                            <a href="{{env('APP_URL').'/user_guide/learn1thing-under-business-partner-guide.pdf'}}" download><i class='fa fa-file-pdf-o'></i> <span>User Guide</span></a>
                        </li>
                    @endif
                </ul>
            </li>

        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
