<header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo" style="background-color: #035AA4;">
        <span class="logo-lg" style="display: none;">
            <img src="{{url('images/logo-white.png')}}" class="img-thumbnail img-logo" alt="Logo - Learn 1 Thing" style="    max-width: 46% !important; background-color:transparent !important; border:none; margin-top: -5px;">
        </span>
        <span class="logo-lg">
            <img src="{{url('images/logo-white.png')}}" class="img-thumbnail img-logo" alt="Logo - Learn 1 Thing" style="    max-width: 46% !important; background-color:transparent !important; border:none; margin-top: -5px;">
        </span>
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
            @if(user_info()->inRole('business-partner'))
                <li>
                    <a href="{{user_info('merchant_page')}}" target="_blank">
                        <i class="fa fa-bookmark"></i> Merchant Page
                    </a>
                </li>
            @endif
            @if(user_info()->inRole('corporate-customer') && strtolower(@user_info('payment_method')->name) == 'credit based system')
                <li class="dropdown messages-menu">
                    <a href="{{route('admin.credit.history')}}">
                      <i class="fa fa-money"></i> {{number_format(user_info('credit'))}}
                      <span class="label label-warning"></span>
                    </a>
                </li>
            @endif
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <i class="fa fa-users"></i> Switch Account
                      <span class="label label-warning"></span>
                    </a>
                    <ul class="dropdown-menu">
                      <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                        {!! log_roles( \Sentinel::getUser()->email ) !!}
                        </ul>
                      </li>
                    </ul>
                </li>
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <img src="{{isset(user_info()->image) ? url_to_image(user_info()->image):link_to_avatar()}}" class="user-image" alt="User Image">
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">
                            {{(user_info()->user) ? user_info()->user->name : user_info('full_name')}}
                        </span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <img src="{{isset(user_info()->image) ? url_to_image(user_info()->image):link_to_avatar()}}" class="img-circle" alt="User Image">
                            <p>
                                {{(user_info()->user) ? user_info()->user->name : user_info('full_name')}}
                                <small>{{ @user_info('role')->name }}</small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ route('admin.user.profile') }}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <form action="{{ route('logout') }}" method="post">
                                  {{ csrf_field() }}
                                  <button type="submit" class="btn btn-default btn-flat">Sign out</a>
                                </form>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
