<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <!-- <a href="{{ route('admin.business-partner.download-guide')}}">User Guide</a> |  -->
        <strong>learn1thing dashboard</strong>
    </div>
    <strong>learn1thing dashboard &copy; {{ date('Y') }} <a href="#" target="_blank">learn1thing dashboard</a>. </strong> All rights reserved.
</footer>