<!DOCTYPE html>
<html>
    <head>
        {!! Html::meta(null, null, ['charset' => 'UTF-8']) !!}
        {!! Html::meta(null, 'IE=edge', ['http-equiv' => 'X-UA-Compatible']) !!}
        {!! Html::meta('csrf-token', csrf_token()) !!}
        {!! Html::meta('product', 'learn1thing') !!}
        {!! Html::meta('description', 'learn1thing') !!}
        {!! Html::meta('author', 'RPS') !!}
        {!! Html::meta('viewport', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no') !!}
        <title>@yield('title', 'Admin') - {{ env('APP_NAME', 'learn1thing Web Admin') }}</title>
        <!-- Bootstrap 3.3.6 -->
        {!! Html::style('bower_components/AdminLTE/bootstrap/css/bootstrap.min.css') !!}
        <!-- Font Awesome -->
        {!! Html::style('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css') !!}
        <!-- select2 -->
        {!! Html::style('bower_components/AdminLTE/plugins/select2/select2.min.css') !!}
        <!-- Theme style -->
        {!! Html::style('bower_components/AdminLTE/dist/css/AdminLTE.min.css') !!}
        {!! Html::style('bower_components/AdminLTE/dist/css/skins/_all-skins.min.css') !!}
        <!-- Pace style -->
        {!! Html::style('bower_components/AdminLTE/plugins/pace/pace.min.css') !!}
        {!! Html::style('bower_components/toastr/toastr.min.css') !!}
        {!! Html::style('bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css') !!}
        <!-- wysihtml5 -->
        {!! Html::style('bower_components/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}

        <!-- Intro Js -->
        {!! Html::style('bower_components/intro.js/introjs.css') !!}

        <script src="https://cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Dyanamic header -->
        <!-- jQuery-->
        {!! Html::script('bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js') !!}
        {!! Html::script('bower_components/AdminLTE/plugins/jQueryUI/jquery-ui.min.js') !!}
        {!! Html::script('bower_components/AdminLTE/plugins/select2/select2.full.min.js') !!}
        
        <style type="text/css">
            .user-panel>.image>img {
                height: 45px;
            }
        </style>
        @yield('header')
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <!-- Header -->
            <header class="main-header">
                <!-- Header Navbar -->
                <nav class="navbar navbar-static-top" role="navigation" style="background-color: #035AA4; margin-left: 0;">
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu" style="float: none;text-align: center;">
                        <span class="logo-lg">
                                <img src="{{url('images/logo-white.png')}}" class="img-thumbnail img-logo" alt="Logo - Learn 1 Thing" style="    max-width: 46% !important; background-color:transparent !important; border:none; margin-top: -5px; height: 100px;">
                            </span>
                    </div>
                </nav>
</header>


            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper" style="margin-left: 0;">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        @yield('page_title', 'Admin')
                        <small>@yield('page_description', 'Admin')</small>
                    </h1>
                    @yield('breadcrumb')
                </section>
                <!-- Main content -->
                <section class="content">
                    <!-- Your Page Content Here -->
                    @yield('content')
                </section>
            </div>

            <!-- Footer -->
            <footer class="main-footer" style="margin-left: 0px;">
                <div class="pull-right hidden-xs">
                    <!-- <a href="{{ route('admin.business-partner.download-guide')}}">User Guide</a> |  -->
                    <strong>learn1thing dashboard</strong>
                </div>
                <strong>learn1thing dashboard &copy; {{ date('Y') }} <a href="#" target="_blank">learn1thing dashboard</a>. </strong> All rights reserved.
            </footer>

        </div>
        <!-- Bootstrap-->
        {!! Html::script('bower_components/AdminLTE/bootstrap/js/bootstrap.min.js') !!}
        {!! Html::script('bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js') !!}
        {!! Html::script('bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js') !!}
        <!-- AdminLTE App -->
        {!! Html::script('bower_components/AdminLTE/dist/js/app.min.js') !!}
        <!-- PACE -->
        {!! Html::script('bower_components/AdminLTE/plugins/pace/pace.min.js') !!}
        <!-- TOASTR -->
        {!! Html::script('bower_components/toastr/toastr.min.js') !!}
        <!-- wysihtml5 -->
        {!! Html::script('bower_components/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}

        {!! Html::script('bower_components/intro.js/intro.js') !!}

        <!-- Dyanamic scripts -->
        @yield('scripts')
    </body>
</html>
