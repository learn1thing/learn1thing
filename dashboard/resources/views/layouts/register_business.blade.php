<!DOCTYPE html> <!--[if lt IE 9]><html
class="no-js lt-ie9" lang="fr_FR"> <![endif]--> <!--[if IE 9]><html
class="no-js ie9" lang="fr_FR"> <![endif]--> <!--[if gt IE 9]><!-->
<html class="no-js" lang="fr_FR"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>Register - learn1thing</title>
	<noscript>
		<style type="text/css">
			.page-template-rm-contact-php .section .section-wrapper{
				opacity: 1!important;
			}
		</style>
	</noscript>
	<style type="text/css">
		.chapter-title {
			color: white;
		    text-shadow: 4px 4px 4px #000000;
		}
	</style>
	<!--meta
	name="description" content="Retrouvez les notions clés qui interviennent dans la construction d’une prestation digitale. Vous saurez ainsi quel est le temps nécessaire à la réalisation de votre projet."/><link
	rel="canonical" href="http://residence-mixte.com/guide" /><link
	rel="publisher" href="https://plus.google.com/+RésidenceMixteParis/"/><meta
	property="og:locale" content="fr_FR" /><meta
	property="og:type" content="website" /><meta
	property="og:title" content="Résidence Mixte" /><meta
	property="og:description" content="Résidence Mixte est une agence de communication basée à Paris et Marseille" /><meta
	property="og:url" content="http://residence-mixte.com/guide" /><meta
	property="og:site_name" content="Guide de Résidence Mixte" /><meta
	property="article:publisher" content="https://www.facebook.com/residencemixte" /><meta
	property="fb:app_id" content="171782879516539" /><meta
	property="og:image" content="https://www.facebook.com/residencemixte/photos/a.707175145977307.1073741826.171782879516539/983524251675727/?type=3&amp;theater" /><meta
	name="twitter:card" content="summary"/><meta
	name="twitter:description" content="Retrouvez les notions clés qui interviennent dans la construction d’une prestation digitale. Vous saurez ainsi quel est le temps nécessaire à la réalisation de votre projet."/><meta
	name="twitter:title" content="Guide de Résidence Mixte -"/><meta
	name="twitter:site" content="@ResidenceMixte"/><meta
	name="twitter:domain" content="Guide de Résidence Mixte"/><meta
	name="twitter:image:src" content="https://www.facebook.com/residencemixte/photos/a.707175145977307.1073741826.171782879516539/983524251675727/?type=3&#038;theater"/-->
	<!--script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"WebSite","url":"http:\/\/residence-mixte.com\/guide\/","name":"Guide de R\u00e9sidence Mixte","alternateName":"Guide de R\u00e9sidence Mixte","potentialAction":{"@type":"SearchAction","target":"http:\/\/residence-mixte.com\/guide\/?s={search_term}","query-input":"required name=search_term"}}</script><script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"Organization","url":"http:\/\/residence-mixte.com\/guide","sameAs":["https:\/\/www.facebook.com\/residencemixte","index.html\/\/instagram.com\/residencemixte\/","index.html\/\/www.linkedin.com\/company\/residence-mixte","index.html\/\/plus.google.com\/+R\u00e9sidenceMixteParis\/","index.html\/\/www.youtube.com\/channel\/UCHf96xf-bQu9IsMXJqHlw4Q","index.html\/\/www.pinterest.com\/residencemixte\/","https:\/\/twitter.com\/ResidenceMixte"],"name":"R\u00e9sidence Mixte","logo":"http:\/\/residence-mixte.com\/guide\/wp-content\/uploads\/2014\/11\/eco_num2.png"}</script><link
	rel="alternate" type="application/rss+xml" title="Guide de Résidence Mixte &raquo; Flux" href="feed/index.html" /><link
	rel="alternate" type="application/rss+xml" title="Guide de Résidence Mixte &raquo; Flux des commentaires" href="comments/feed/index.html" /><script type="text/javascript">
				window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/residence-mixte.com\/guide\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.2.2"}};
				!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
			</script-->
	<style type="text/css">
	img.wp-smiley,
	img.emoji {
		display: inline !important;
		border: none !important;
		box-shadow: none !important;
		height: 1em !important;
		width: 1em !important;
		margin: 0 .07em !important;
		vertical-align: -0.1em !important;
		background: none !important;
		padding: 0 !important;
	}
	chapters-table .chapter-cell.second-chapter .introduction {
	    background-color: #a2bbc0 !important;
	}
	</style>
	<link rel='stylesheet' id='style-css' href='{{ asset("css/stylef809.css?ver=12052015") }}' type='text/css' media='all' />
	<script>
		var jquery_placeholder_url = 'wp-content/plugins/gravity-forms-placeholders/jquery.placeholder-1.0.1.js';
	</script>
	<!--[if lte IE 9]><script src="http://residence-mixte.com/guide/wp-content/themes/guide-residence-mixte/js/vendor/respond.min.js"></script><![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
	
</head>
<body class="home blog">
	<div id="main">
		<h1 class="site-title" style="display:none">Register</h1>
		<!-- <p class="site-tagline">How can be learn1thing affecting you</p>
		<p class="credits"><!--a href="http://residence-mixte.com/" target="_blank" title="Résidence Mixte">Résidence Mixte</a></p-->
		<div class="chapters-wrapper chapters-table">
			@foreach($registers as $key => $value)
			@php
				$chapt = ''; 
				$direct = ''; 
				if ($key == 0) {
					$chapt = 'first'; 
					$direct = 'standar-customer';
				} else if ($key == 1) {
					$chapt = 'second'; 
					$direct = 'corporate';
				} else {
					$chapt = 'third'; 
					$direct = 'partner';
				}
			@endphp
			<section class="section {{$chapt}}-chapter chapter-cell cf" style="background-image:url({{get_file($value->image)}}) !important;">
				<h2 class="chapter-title">{{$value->title}}</h2>
				<p class="button-wrapper"><a class="button" href="{{ route('auth-register') }}?r={{ $direct }} ">Register Now</a></p>
				<div class="introduction">
					<!--svg version="1.1" class="triangle" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="155px" viewBox="0 0 455 155" enable-background="new 0 0 455 155" xml:space="preserve" preserveAspectRatio="none" style="display:none">
						<polygon fill="#27ae8d" points="455,0 0,155 455,155 "/-->
					</svg>
					<p style="height: 90px; color:#000000">{{$value->description}}</p>
				</div>
			</section>
			@php
				if ($key == 2) {
					break;
				}
			@endphp
			@endforeach
		</div>
		<!--footer class="footer" id="main-footer">
			<div class="menu-pied-de-page-container">
				<ul id="menu-pied-de-page" class="menu">
					<li id="menu-item-867" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-867">
						<a href="propos/index.html">À propos</a></li>
					<li id="menu-item-751" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-751">
						<a href="contact/index.html">Contact</a></li>
				</ul>
			</div>
		</footer-->
	</div>
	<script type='text/javascript' src='wp-content/plugins/wp-retina-2x/js/retina.min6f3e.js?ver=1.3.0'></script>
</body>
</html>