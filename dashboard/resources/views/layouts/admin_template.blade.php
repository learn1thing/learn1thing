<!DOCTYPE html>
<html>
    <head>
	
	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

	{!! Html::meta(null, null, ['charset' => 'UTF-8']) !!}
        {!! Html::meta(null, 'IE=edge', ['http-equiv' => 'X-UA-Compatible']) !!}
        {!! Html::meta('csrf-token', csrf_token()) !!}
        {!! Html::meta('product', 'learn1thing') !!}
        {!! Html::meta('description', 'learn1thing') !!}
        {!! Html::meta('author', 'RPS') !!}
        {!! Html::meta('viewport', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no') !!}
        <title>@yield('title', 'Admin') - {{ env('APP_NAME', 'learn1thing Web Admin') }}</title>
        <!-- Bootstrap 3.3.6 -->
        {!! Html::style('bower_components/AdminLTE/bootstrap/css/bootstrap.min.css') !!}
        <!-- Font Awesome -->
        {!! Html::style('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css') !!}
        <!-- select2 -->
        {!! Html::style('bower_components/AdminLTE/plugins/select2/select2.min.css') !!}
        <!-- Theme style -->
        {!! Html::style('bower_components/AdminLTE/dist/css/AdminLTE.min.css') !!}
        {!! Html::style('bower_components/AdminLTE/dist/css/skins/_all-skins.min.css') !!}
        <!-- Pace style -->
        {!! Html::style('bower_components/AdminLTE/plugins/pace/pace.min.css') !!}
        {!! Html::style('bower_components/toastr/toastr.min.css') !!}
        {!! Html::style('bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css') !!}
        <!-- wysihtml5 -->
        {!! Html::style('bower_components/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}

        <!-- Intro Js -->
        {!! Html::style('bower_components/intro.js/introjs.css') !!}

        <script src="https://cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Dyanamic header -->
        <!-- jQuery-->
        {!! Html::script('bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js') !!}
        {!! Html::script('bower_components/AdminLTE/plugins/jQueryUI/jquery-ui.min.js') !!}
        {!! Html::script('bower_components/AdminLTE/plugins/select2/select2.full.min.js') !!}
        {!! Html::style('css/custom.css') !!}
        <style type="text/css">
            .user-panel>.image>img {
                height: 45px;
            }
        </style>
        @yield('header')

    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <!-- Header -->
            @include('layouts.partial.header')

            <!-- Sidebar -->
            @include('layouts.partial.sidebar')

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        @yield('page_title', 'Admin')
                        <small>@yield('page_description', 'Admin')</small>
                    </h1>
                    @yield('breadcrumb')
                </section>
                <!-- Main content -->
                <section class="content">
                    <!-- Your Page Content Here -->
                    @yield('content')
                </section>
            </div>

            <!-- Footer -->
            @include('layouts.partial.footer')

        </div>
        <!-- Bootstrap-->
        {!! Html::script('bower_components/AdminLTE/bootstrap/js/bootstrap.min.js') !!}
        {!! Html::script('bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js') !!}
        {!! Html::script('bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js') !!}
        <!-- AdminLTE App -->
        {!! Html::script('bower_components/AdminLTE/dist/js/app.min.js') !!}
        <!-- PACE -->
        {!! Html::script('bower_components/AdminLTE/plugins/pace/pace.min.js') !!}
        <!-- TOASTR -->
        {!! Html::script('bower_components/toastr/toastr.min.js') !!}
        <!-- wysihtml5 -->
        {!! Html::script('bower_components/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}

        {!! Html::script('bower_components/intro.js/intro.js') !!}

        <script type="text/javascript">
            $(document).ready(function() {
                setInterval(function() {
                  check_log()
                }, 10000);
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var _baseUrl = '{{ url('/') }}';

            // To make Pace works on Ajax calls
            $(document).ajaxStart(function() {
                Pace.restart();
            });

            $(document).ready(function(){
                @if(Session::has('notification'))
                  var type = "{{ Session::get('notification.alert-type', 'info') }}";
                  var message = "{{ Session::get('notification.message') }}";
                  "{{Session::forget('notification')}}";
                  switch(type){
                    case 'info':
                    toastr.info(message);
                    break;
                    case 'warning':
                    toastr.warning(message);
                    break;
                    case 'success':
                    toastr.success(message);
                    break;
                    case 'error':
                    toastr.error(message);
                    break;
                  }
                @endif

                $(".textarea").wysihtml5();

                $('.wysihtml5-toolbar .bootstrap-wysihtml5-insert-link-modal [data-wysihtml5-dialog-action="cancel"]').remove();

                $('.box-body form .form-group').each(function(key,value){ 
                    var text = $(this).find('label').text();
                    if ($(this).find('input').length == 1) {
                        $(this).find('input').attr('placeholder',text);
                    }
                    if ($(this).find('textarea').length == 1) {
                        $(this).find('textarea').attr('placeholder',text);
                    }
                });

                var contentWrapper = $('.content-wrapper .content-header h1 small').text().replace('L1T','learn1thing');
                //replace L1T to learn1thing
                $('.content-wrapper .content-header h1 small').text(contentWrapper);
            });

            $(document).on('click','.activation-user',function(){
              var url = $(this).data('href');
              if ($(this).hasClass('processing')) {
                toastAlert('Please wait, system is running background process!','warning');
                return false;
              } else {
                  $.ajax({
                    url:url,
                    method:'POST',
                    dataType:'json',
                    beforeSend: function(data) {
                        $('.datatable a').addClass('processing');
                        $('.datatable a').removeAttr('id');
                    },
                    success: function(data) {
                      if (data.result == 'failed') {
                        toastAlert(data.message,'error');
                      } else {
                        toastAlert(data.message,'success');
                      }
                      $('.datatable').DataTable().ajax.reload();
                    },
                    error: function(data) {
                      alert(data.statusText);
                      toastAlert(data.statusText,'error');
                      $('.datatable').DataTable().ajax.reload();
                    }
                  });
              }
            });

            $(document).on('click','.datatable a',function(e) {
                e.preventDefault();
                if ($(this).hasClass('processing')) {
                    toastAlert('Please wait, system is running background process!','warning');
                    return false;
                } else {
                    // if ($(this).attr('title') == 'Edit') {
                        window.location.href = $(this).attr('href');
                    // }
                }
                return true;
            });

            function toastAlert(message,type) {
                switch(type){
                    case 'info':
                    toastr.info(message);
                    break;
                    case 'warning':
                    toastr.warning(message);
                    break;
                    case 'success':
                    toastr.success(message);
                    break;
                    case 'error':
                    toastr.error(message);
                    break;
                }
            }

            function getZone(element,id) {
                var url = "{{url('')}}";
                element.removeAttr('readonly');
                $.ajax({
                    method: 'get',
                    url: url+"/country-detail/"+id,
                    dataType: 'json',
                    success: function(data) {
                        element.html('<option value="">Select Region</option>');
                        if (data.status) {
                            if (data.data.length == 0) {
                                element.attr('readonly',true);
                                element.html('<option value="0"></option>');
                            } else {
                                data.data.forEach(function(value) {
                                    element.append('<option value="'+value.zone_id+'">'+value.name+'</option>');
                                });
                            }
                        }
                    }
                });
            }

            jQuery(".number_only").on('keydown',function(event) {
              // Allow: backspace, delete, tab, escape, and enter
              if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
                  // Allow: Ctrl+A
                (event.keyCode == 65 && event.ctrlKey === true) ||
                  // Allow: home, end, left, right
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                  // let it happen, don't do anything
                  return;
              }
              else {
                // Ensure that it is a number and stop the keypress
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105  )) {
                  event.preventDefault();
                }
              }
            });
            function check_log() {
                if(typeof(EventSource) !== "undefined") {
                    var source = new EventSource("{{route('admin.check.log')}}");
                    source.onmessage = function(data) {
                        if(data.data !== "no notification"){
                            var obj = JSON.parse(data.data);
                            if (obj !== null) {
                                toastr.info(obj.description);
                                $.ajax({
                                    url: "{{route('admin.check.log')}}",
                                    method: "GET",
                                    data: {'id':obj.id},
                                    dataType: "json",
                                    success: function(result) {
                                        return true;
                                    }
                                });
                            }
                        }

                        source.close();
                    }
                }
            }
        </script>

        @if(session('status') == "new-user" AND user_info('role')->slug == "business-partner")
        <script type="text/javascript">
            function startTour() {
                var tour = introJs()
                tour.setOption('tooltipPosition', 'auto');
                tour.setOption('positionPrecedence', ['right'])
                tour.start()
            }
            $(document).ready(function() {
                startTour();
            });
        </script>
        @endif

        <!-- Dyanamic scripts -->
        @yield('scripts')
        @yield('part_js')
    </body>
</html>
