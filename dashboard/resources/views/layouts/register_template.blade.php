<!DOCTYPE html>
<html>
    <head>
        {!! Html::meta(null, null, ['charset' => 'UTF-8']) !!}
        {!! Html::meta(null, 'IE=edge', ['http-equiv' => 'X-UA-Compatible']) !!}
        {!! Html::meta('csrf-token', csrf_token()) !!}
        {!! Html::meta('product', 'MyCMS') !!}
        {!! Html::meta('description', 'MyCMS') !!}
        {!! Html::meta('author', 'Iddan Gunawan') !!}
        {!! Html::meta('viewport', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no') !!}
        <title>@yield('title', 'Admin') - {{ env('APP_NAME', 'MyCms Web Admin') }}</title>
        <!-- Bootstrap 3.3.6 -->
        {!! Html::style('bower_components/AdminLTE/bootstrap/css/bootstrap.min.css') !!}
        <!-- Font Awesome -->
        {!! Html::style('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css') !!}
        <!-- select2 -->
        {!! Html::style('bower_components/AdminLTE/plugins/select2/select2.min.css') !!}
        <!-- Theme style -->
        {!! Html::style('bower_components/AdminLTE/dist/css/AdminLTE.min.css') !!}
        {!! Html::style('bower_components/AdminLTE/dist/css/skins/_all-skins.min.css') !!}
        <!-- Pace style -->
        {!! Html::style('bower_components/AdminLTE/plugins/pace/pace.min.css') !!}
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

        {!! Html::style('bower_components/toastr/toastr.min.css') !!}
        

                <!-- select2 -->
        {!! Html::style('css/custom.css') !!}

        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Dyanamic header -->
        <!-- jQuery-->
        {!! Html::script('bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js') !!}
        {!! Html::script('bower_components/AdminLTE/plugins/jQueryUI/jquery-ui.min.js') !!}
        @yield('header')
    </head>
    <body class="hold-transition login-page">
        @yield('content')

        <!-- Bootstrap-->
        {!! Html::script('bower_components/AdminLTE/bootstrap/js/bootstrap.min.js') !!}
        <!-- AdminLTE App -->
        {!! Html::script('bower_components/AdminLTE/dist/js/app.min.js') !!}
        <!-- PACE -->
        {!! Html::script('bower_components/AdminLTE/plugins/pace/pace.min.js') !!}
        <!-- select2 -->
        {!! Html::script('bower_components/AdminLTE/plugins/select2/select2.full.min.js') !!}

        <!-- TOASTR -->
        {!! Html::script('bower_components/toastr/toastr.min.js') !!}
        

        <script type="text/javascript">
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // To make Pace works on Ajax calls
            $(document).ajaxStart(function() {
                Pace.restart();
            });

            $(document).ready(function(){

                @if(Session::has('notification'))
                  var type = "{{ Session::get('notification.alert-type', 'info') }}";
                  var message = "{{ Session::get('notification.message') }}";
                  "{{Session::forget('notification')}}";
                  switch(type){
                    case 'info':
                    toastr.info(message);
                    break;
                    case 'warning':
                    toastr.warning(message);
                    break;
                    case 'success':
                    toastr.success(message);
                    break;
                    case 'error':
                    toastr.error(message);
                    break;
                  }
                @endif


            });
            
        </script>
        <!-- Dyanamic scripts -->
        @yield('scripts')
    </body>
</html>
