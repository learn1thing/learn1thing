<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>{{ $title }}</title>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
<div style="width: 680px;"><a href="<?php echo env('OC_BASE_URL'); ?>" title="learn1thing.com" style="text-align: center;"><img width="200" height="100" src="<?php echo env('OC_BASE_URL').'/image//l1t-logo2.png'; ?>" alt="learn1thing.com" style="margin-bottom: 20px; border: none;text-align: center;" /></a>
  <p style="margin-top: 0px; margin-bottom: 20px;">Dear {{ $user->first_name }},</p>
  
  @if (!empty(@$content))
    {!! @$content !!}
  @else
    Request your product has been approved by Admin.
  @endif 
  <h4 style="margin-top: 0px; margin-bottom: 10px;font-size: 12px;">Product Details: </h4>

  <table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
    <thead>
      <tr>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">Product</td>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">Model</td>
        @if (!empty(@$schedule))
          <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;">Session Time</td>
        @endif
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;">Schedule</td>
        @if($type_email !='approve')
        @if (empty(@$link_bbb))
          <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;">Course</td>
        @endif
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;">Coaching Room</td>
        @endif

      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">{{$product->name}}</td>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">{{$product->model}}</td>
        @if (!empty(@$schedule))
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">
            <p>Start : {{ date('l, d F Y',strtotime($schedule[0]->start_date)).' '.$schedule[0]->start_time }}</p>
            <p>Ends : {{ date('l, d F Y',strtotime($schedule[count($schedule)-1]->end_date)).' '.$schedule[count($schedule)-1]->end_time }}</p>
        </td>
        @endif
        
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">
              
              <a href="{{ $link_schedule }}" target="_blank">View Schedule</a><br>
        </td>
        @if($type_email !='approve')
        @if (empty(@$link_bbb))
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">
            @if (!empty(@$product->productSets[0]->course_url))
              <a href="{{ @$product->productSets[0]->course_url }}" target="_blank">View Course</a><br>
            @else
              -
            @endif
        </td>
        @endif
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">
            @if (!empty(@$link_bbb))
              <a href="{{ @$link_bbb }}" target="_blank">Link to Coaching Room</a><br>
            @else
              -
            @endif
        </td>
        @endif

      </tr>
    </tbody>
  </table>

</div>
</body>
</html>