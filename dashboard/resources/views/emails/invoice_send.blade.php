<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Invoice - Pay As You Go</title>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
<div style="width: 680px;"><a href="<?php echo env('OC_BASE_URL'); ?>" title="learn1thing.com" style="text-align: center;"><img width="200" height="100" src="<?php echo env('OC_BASE_URL').'/image//l1t-logo2.png'; ?>" alt="learn1thing.com" style="margin-bottom: 20px; border: none;text-align: center;" /></a>
  <p style="margin-top: 0px; margin-bottom: 20px;">Dear, {{$user->first_name.' '.$user->last_name}}</p>
  <h4 style="margin-top: 0px; margin-bottom: 10px;font-size: 14px;">Invoice - Pay As You Go</h4>

  <table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
    <thead>
      <tr>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">Order Details</td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">
          <b>Payment Method:</b> Pay As You Go<br />
          <b>E-mail:</b> <?php echo $user->email; ?><br />
          <b>Telephone:</b> <?php echo $user->phone; ?><br />
          <b>Order Status:</b> {{ (@$status) ? $status : 'Pending' }}<br /></td>
      </tr>
    </tbody>
  </table>
  
  <table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
    <thead>
      <tr>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">Date</td>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">Product</td>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;">Quantity</td>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;">Price</td>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;">Total</td>
      </tr>
    </thead>
    <tbody>
        <?php $total = 0; ?>
        @foreach($invoices as $invoice)
            @if($invoice->status == 1)
              <?php $total += $invoice->total; ?>
              <tr>
                  <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">{{date('d F Y H:i:s',strtotime($invoice->date))}}
                  <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">{{$invoice->product_name}}
                  <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">{{number_format($invoice->quantity)}}</td>
                  <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">${{number_format($invoice->price)}}</td>
                  <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">${{number_format($invoice->total)}}</td>
              </tr>
            @endif
        @endforeach
    </tbody>
    <tfoot>
      <tr>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;" colspan="4"><b>Total:</b></td>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;"><?php echo '$'.number_format($total); ?></td>
      </tr>
    </tfoot>
  </table>

</div>
</body>
</html>