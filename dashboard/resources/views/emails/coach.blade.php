Hi {{ $user->first_name }} {{ $user->last_name }}, {{ $customer }} has booked your time on <br> 
<ul>
	<li>
		{{ date('l, d F Y',strtotime($coach['details'][0]['start_date'])).' '.$coach['details'][0]['start_time'] }}
		-
		{{ date('l, d F Y',strtotime($coach['details'][count($coach['details'])-1]['end_date'])).' '.$coach['details'][count($coach['details'])-1]['end_time'] }}
	</li>
</ul>
for this course : 

@if( !empty($link) && count($link) > 0 ) 
	@foreach($link as $course) 

		<p style="margin-top: 0px; margin-bottom: 20px;">Course:</p>
		<a href="{{ str_replace(' ','%20',$course) }}">{{ str_replace(' ','%20',$course) }}</a>

	@endforeach
@endif
<br/>
<br/>
@if( $bbb_link != '' )
<p style="margin-top: 0px; margin-bottom: 20px;">BBB:</p>
<a href="{{ str_replace(' ','%20',$bbb_link) }}">{{ str_replace(' ','%20',$bbb_link) }}</a>
@endif
