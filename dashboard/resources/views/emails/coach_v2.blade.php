<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Dear {{ $user->first_name }} {{ $user->last_name }},<br />
<br />
Your coaching time has been booked. Please refer to the details below:<br />
<br />
Program: {{ $data_products[0]->name }}<br />
Coachee: {{$customer->firstname}} {{$customer->lastname }}<br />
Email: {{ $customer->email }}<br />
Telephone: {{ $customer->telephone }}</span></span></p>

<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Session 1: {{date('d-m-Y',strtotime($coach['details'][0]['start_date'])).' ('.$coach['details'][0]['start_time'].' - '.$coach['details'][count($coach['details'])-1]['end_time'].')'}} Timezone: Asia/Singapore</span></span></p>

<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif">To access the virtual coaching session on Learn1thing.com for the first time, please follow the following simple steps:</span></span></p>

<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif">1. Click <a href="{{ str_replace(' ','%20',$bbb_link) }}" style="color:blue; text-decoration:underline">here</a> for the {{ $data_products[0]->name }}<br />
2. <a href="https://store.learn1thing.com/dashboard">Login</a> using your learn1thing user account.<br />
<br />
<a href="http://www.learn1thing.com/learn/calendar/ical.php?id=66f041e16a60928b05a7e228a89c3799" style="color:blue; text-decoration:underline">http://www.learn1thing.com/learn/calendar/ical.php?id=66f041e16a60928b05a7e228a89c3799</a></span></span></p>

<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Warmest Regards,<br />
<br />
Administrator<br />
learn1thing.com<br />
<br />
*This is system-generated email<br />
==========<br />
<br />
For further assistance, please contact <a href="mailto:lea@learn1thing.com" style="color:blue; text-decoration:underline">lea@learn1thing.com</a></span></span></p>
