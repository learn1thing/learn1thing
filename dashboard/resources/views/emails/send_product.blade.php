<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>{{ $title_email }}</title>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
<div style="width: 680px;"><a href="<?php echo env('OC_BASE_URL'); ?>" title="learn1thing.com" style="text-align: center;"><img width="200" height="100" src="<?php echo env('OC_BASE_URL').'/image//l1t-logo2.png'; ?>" alt="learn1thing.com" style="margin-bottom: 20px; border: none;text-align: center;" /></a>
  <p style="margin-top: 0px; margin-bottom: 20px;">Dear {{$user->first_name}},</p>
  <br>
  <p style="margin-top: 0px; margin-bottom: 20px;">Your product request has been approved by your administrator. Please see the link below for more details. 
</p>
  <h4 style="margin-top: 0px; margin-bottom: 10px;font-size: 14px;">Product</h4>

  <table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
    <thead>
      <tr>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">Product</td>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">Model</td>
        @if(!empty($profiler_url))
          <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;">Profiler</td>
        @else
          <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;">Course</td>
        @endif
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">{{$product->name}}</td>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">{{$product->model}}</td>
        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">
          @if(!empty($profiler_url))
            <a href="{{$profiler_url}}" target="_blank">{{$profiler_url}}</a>
          @else
            @if(count($courses) > 0)
              @php($x=0)
              @foreach($courses as $course)
                <a href="{{@$course->course_url}}" target="_blank">{{@$course->course_url}}</a><br>
              @endforeach
            @else
              -
            @endif
          @endif
        </td>
      </tr>
    </tbody>
  </table>

</div>
</body>
</html>