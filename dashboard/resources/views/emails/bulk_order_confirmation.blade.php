<p>Dear {{$name}},</p>
@if($order->status == 2)
    <p>Status bulk order: Completed</p>
@else
    <p>Status bulk order: Rejected</p>
@endif
<br>
<table class="table table-bordered" width="100%">
    <thead>
        <tr>
            <th>Product Name</th>
            <th>Model</th>
            <th>Price</th>
            <th>Quantity</th>
        </tr>
    </thead>
    <tbody>
        <?php $priceTotal = 0; ?>
        <?php $quantityTotal = 0; ?>

        @foreach($orderDetails as $detail)
            <?php $priceTotal += $detail->product->price; ?>
            <?php $quantityTotal += $detail->quantity; ?>
            <tr>
                <td>{{$detail->product->name}}</td>
                <td>{{$detail->product->model}}</td>
                <td align="right">{{$detail->product->price}}</td>
                <td align="right">{{$detail->quantity}}</td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td align="right" colspan="2"><label>Total</label></td>
            <td align="right">{{$priceTotal}}</td>
            <td align="right">{{$quantityTotal}}</td>
        </tr>
    </tfoot>
</table>
<br>
<p>Best regards,<br>learn1thing</p>