@extends('layouts.register_template')

@section('header')
<style type="text/css">
    .login-box, .register-box {
        width: 360px;
        margin: 2% auto;
    }
</style>
{!! Html::style('css/amaran.min.css') !!}
{!! Html::style('css/animate.min.css') !!}
@endsection

@section('content')
<div class="login-box">
    <div class="login-logo">
        {!! Html::image('images/base-logo.png', null, ['class'=>"center-block img-responsive", 'width'=>'200px']) !!}
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Enter the e-mail address associated with your account. Click Continue to have a password reset link e-mailed to you.</p>
        @include('partials.message')
        <form id="form-forgot">
            <h4>Forgot Your Password?</h4><hr>
            <div class="form-group" id="forgot-email">
                <label class="control-label" for="email">{!! trans('label.required') !!}Email Address</label>
                <input type="email" name="email" class="form-control" id="email" placeholder="Email" required="">
            </div>
            <button type="submit" class="btn btn-primary btn-block btn-flat" id="button-forgot">Continue</button>
        </form>
        <a href="{{ route('login') }}" class="text-center">I already have a membership.</a>
    </div>
</div>
@endsection

@section('scripts')
{!! Html::script('js/jquery.amaran.min.js') !!}
<script>
    $('#button-forgot').click(function(event) {
        event.preventDefault();
        var $btn = $(this).button('loading');
        $('.form-group').removeClass('has-error');
        $('.help-block').remove();
        $.ajax({
            type:'post',
            url:'{{ route("auth-forgot-post") }}',
            data: $('#form-forgot').serialize(),
            dataType: 'json',
            success:function(data) {
                console.log(data);
                    
                     $.amaran({
                            'theme'     :'colorful',
                            'content'   :{
                               bgcolor:'#8bb8f1',
                               color:'#fff',
                               message:'Success! a reset url has been sent to your email address'
                            },
                            'position'  :'top right'
                        });
                $btn.button('reset');
                // window.location = '{{ route("login") }}';
                window.setTimeout(function(){
                    window.location = '{{ route("login") }}';
                }, 5000);

            },
            error:function(data) {
                console.log(data);
                var data = data.responseJSON;
                if (typeof data.result !== 'undefined') {
                    if (data.result) {
                        $('.form-group').addClass('has-error');
                    }
                }
                

                $.each(data.data,function(key, val){
                    $('#forgot-'+key).addClass('has-error');
                    $('<span class="help-block">'+val+'</span>').insertAfter($('#'+key));
                });
                $btn.button('reset');
            }
        });
    });
</script>

@endsection
