@extends('layouts.register_template')

@section('header')
<!-- iCheck -->
{!! Html::style('bower_components/AdminLTE/plugins/iCheck/square/blue.css') !!}
<style type="text/css">
    .login-box, .register-box {
        width: 360px;
        margin: 2% auto;
    }
    .grad-background {
      background: linear-gradient( #0575E6, #021B79); /* Standard syntax */
      background: -webkit-linear-gradient(#0575E6, #021B79); /* For Safari 5.1 to 6.0 */
      background: -o-linear-gradient(#0575E6, #021B79); /* For Opera 11.1 to 12.0 */
      background: -moz-linear-gradient(#0575E6, #021B79); /* For Firefox 3.6 to 15 */
    }
    .title-register {
        color: white;
    }
    .c-fld-tooltip-hint__cue {
        font-size: 14px;
        cursor: help;
        /*margin-left: 6px;
        line-height: 1.4;
        cursor: help;
        -webkit-transition: color .2s ease-in-out;
        -o-transition: color .2s ease-in-out;
        transition: color .2s ease-in-out;
        vertical-align: middle;
        color: #bbb;*/
    }
    .c-icon {
        font-family: "bl_icons_v3" !important;
        speak: none;
        font-style: normal;
        font-weight: normal;
        font-variant: normal;
        text-transform: none;
        line-height: 1;
        vertical-align: middle;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }
    .c-fld-tooltip-hint__cue:hover {
        color: #3c8dbc;
    }
    .c-icon--help:before {
        content: "\f059";
        font-family: FontAwesome;
        font-style: normal;
        font-weight: normal;
        text-decoration: inherit;
    }
    /* Tooltip */
    .tooltip > .tooltip-inner {
        padding: 15px;
        max-width: 100%;
    }
</style>
@endsection

@section('content')
<div class="login-box">
    <div class="login-logo" style="margin-bottom: 0px">
        {!! Html::image('images/logo-white.png', null, ['class'=>"center-block img-responsive", 'width'=>'200px']) !!}
    </div>
    <p class="login-box-msg title-register">Please enter your personal details</p>
    <div class="login-box-body">
        @include('partials.message')
        <form id="form-register">
            @if ($register_type != 'social-media')
            <input type="hidden" class="customer-group" value="{{$_GET['r']}}">

            <div class="row">
                <div class="col-md-12" id="step-1-form">
                    <h3 class="login-box-msg for-company" style="display: none;">-Step 1-</h3>
                    <div class="form-group " id="register-customer_group_id" style="display: none;">
                        <!-- <label class="control-label" for="customer_group_id">{!! trans('label.required') !!}Customer Group</label> -->
                        <label class="radio-inline corporate-radio">
                            <input type="radio" <?php echo !empty($_GET['r']) && $_GET['r'] == 'corporate' ? 'checked' : '' ?> name="customer_group_id" id="customer_group_id_3" value="4"> Corporate
                        </label>
                        <label class="radio-inline partner-radio">
                            <input type="radio" <?php echo !empty($_GET['r']) && $_GET['r'] == 'partner' ? 'checked' : '' ?> name="customer_group_id" id="customer_group_id_2" value="2"> Partner
                        </label>

                        <label class="radio-inline standard-radio">
                            <input type="radio" <?php echo !empty($_GET['r']) && $_GET['r'] == 'standar-customer' ? 'checked' : '' ?> name="customer_group_id" id="customer_group_id_1" value="1"> Consumer
                        </label>
                        <div id="customer_group_id"></div>
                    </div>
                    <div class="form-group" id="register-first_name">
                        <input type="text" name="first_name" class="form-control" id="first_name" placeholder="First name*" required="">
                    </div>
                    <div class="form-group" id="register-last_name">
                        <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Last name*" required="">
                    </div>
                    <div class="form-group" id="register-email">
                        <input type="email" name="email" class="form-control" id="email" placeholder="you@email.com*" required="">
                    </div>
                    <div class="form-group" id="register-phone">
                        <input type="text" name="phone" class="form-control" id="phone" placeholder="+6512345678" required="">
                    </div>
                    @if( $_GET['r'] == 'standar-customer' )
                    
                    <!-- add by rdw k5 --><hr>
                    
                    <div class="form-group" id="register-address_1">
                        <textarea name="address_1" class="form-control" id="address_1" placeholder="Address " required=""></textarea>
                    </div>
                    <div class="form-group" id="register-city">
                        <input type="text" name="city" class="form-control" id="city" placeholder="City" required="" value="">
                    </div>
                    <div class="form-group" id="register-post_code">
                        <input type="text" name="post_code" class="form-control" id="post_code" placeholder="Postal Code" required="" value="">
                    </div>
                    


                    <div class="form-group" id="register-country">
                        <select name="country" class="form-control select2" onchange="countryId(this.value)" style="width: 100% !important;" required="">
                            <option value="">-Country-</option>
                            @foreach ($countries as $country)
                                <option value="{{ $country->country_id }}" {{ $country->country_id == 222 ? 'selected' : ''}}>{{ $country->name }}</option>
                            @endforeach
                        </select>
                        <div id="country"></div>
                    </div>
                    <!-- 
                    komen by rdw k5
                    <div class="form-group" id="register-region">
                        <select name="region" class="form-control select2" id="region-by-country" style="width: 100% !important;">
                            <option value="0">-Region-</option>
                            @foreach ($zones as $zone)
                                <option value="{{ $zone->zone_id }}">{{ $zone->name }}</option>
                            @endforeach
                        </select>
                        <div id="region"></div>
                    </div> -->


                    @endif
                    <hr>
                    <div class="form-group" id="register-password">
                        <input type="password" name="password" class="form-control" id="password" placeholder="Password*" required="">
                    </div>
                    <div class="form-group" id="register-password_confirm">
                        <input type="password" name="password_confirm" class="form-control" id="password_confirm" placeholder="Password Confirm*" required="">
                    </div>

                    <div style="display: none;">
                        <hr><h4>Newsletter</h4><hr>
                        
                        <div class="form-group" id="register-subscribe">
                            <label class="control-label" for="subscribe">Subscribe</label>
                            <label class="radio-inline">
                                <input type="radio" name="subscribe" id="subscribe1" value="1"> Yes
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="subscribe" id="subscribe2" value="0" checked=""> No
                            </label>
                        </div>
                    </div>

                </div>
                <div class="col-md-12" id="step-2-form" style="display: none;">
                    <h3 class="login-box-msg for-company" style="display: none;">-Step 2-</h3>
                    <div class="form-group" id="register-company_name">
                        <input type="text" name="company_name" class="form-control" id="company_name" placeholder="Company Name" value="{{($_GET['r'] == 'standar-customer') ? 'Company Name' : ''}}" required>
                    </div>
                    <div class="form-group" id="register-company_register_no">
                        <input type="text" name="company_register_no" class="form-control" id="company_register_no" value="{{($_GET['r'] == 'standar-customer') ? '1234567890' : ''}}" placeholder="Company Registration Number" required>
                    </div>
                    @if( $_GET['r'] != 'standar-customer' )
                    <div class="form-group" id="register-country">
                        <select name="country" class="form-control select2" onchange="countryId(this.value)" style="width: 100% !important;">
                            <option value="0">-Country-</option>
                            @foreach ($countries as $country)
                                <option value="{{ $country->country_id }}" {{ $country->country_id == 222 ? 'selected' : ''}}>{{ $country->name }}</option>
                            @endforeach
                        </select>
                        <div id="country"></div>
                    </div>
                    <div class="form-group" id="register-region">
                        <select name="region" class="form-control select2" id="region-by-country" style="width: 100% !important;">
                            <option value="0">-Region-</option>
                            @foreach ($zones as $zone)
                                <option value="{{ $zone->zone_id }}">{{ $zone->name }}</option>
                            @endforeach
                        </select>
                        <div id="region"></div>
                    </div>
                    @endif
                    <div class="form-group" id="register-company_address">
                        <textarea name="company_address" class="form-control" id="company_address" placeholder="Company Address" required>{{($_GET['r'] == 'standar-customer') ? 'Company Address' : ''}}</textarea>
                    </div>
                    <div class="form-group" id="register-post_code">
                        <input type="text" name="post_code" class="form-control" id="post_code" placeholder="Post Code" required="" value="{{($_GET['r'] == 'standar-customer') ? '123456' : ''}}">
                    </div>

                    @if($_GET['r'] == 'corporate')
                        <div class="form-group" id="register-payment_method_id">
                            <select class="form-control" name="payment_method_id" id="payment_method_id">
                                <option value="">Select Payment Method</option>
                                @if(count($payment_method))
                                    @foreach($payment_method as $method)
                                        <option data-type="{{$method->name}}" value="{{$method->id}}">{{$method->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <p>
                                <span data-toggle="tooltip" title="Credit Based System is a payment method where corporate trades their amount of dollars to amount of credit that can be used to buy products. The remaining credit/token at the end of the month will not be accumulated to the following month." class="c-fld-tooltip-hint__cue"><i>Credit Based System</i></span>
                                or
                                <span data-toggle="tooltip" title="Pay as You Go is a payment method where corporate has a special agreement with learn1thing to 'buy' any product from the mini-store. ODE will send invoice to the corporate at the end of the month, according to what they have used/bought." class="c-fld-tooltip-hint__cue"><i>Pay As You Go</i></span>
                            </p>
                        </div>
                    @endif
                </div>
                <!-- kondisi by rdw k5 -->
                @if( $_GET['r'] != 'standar-customer' )
                <div class="col-md-12" style="display: none;">
                    <h4 class="register-address-title">Your Address</h4><hr>
                    
                    <div id="corporate-field" style="display:<?php echo !empty($_GET['r']) && $_GET['r'] == 'standar-customer' ? 'none' : '' ?>">
                        
                    </div>
                    <div class="form-group" id="register-address_1">
                        <label class="control-label" for="address_1">{!! trans('label.required') !!}Address 1</label>
                        <textarea name="address_1" class="form-control" id="address_1" placeholder="Address 1" required=""></textarea>
                    </div>
                    <div class="form-group" id="register-address_2">
                        <label class="control-label" for="address_2">Address 2</label>
                        <textarea name="address_2" class="form-control" id="address_2" placeholder="Address 2" required=""></textarea>
                    </div>
                    <div class="form-group" id="register-city">
                        <label class="control-label" for="city">{!! trans('label.required') !!}City</label>
                        <input type="text" name="city" class="form-control" id="city" placeholder="City" required="" value="city">
                    </div>
                </div>
                @endif
            </div>
            @endif
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="submit-complete">
                            <div class="{{($_GET['r'] == 'standar-customer') ? 'col-xs-8' : 'col-xs-12'}}">
                                <div class="checkbox icheck" id="register-terms">
                                    <label>
                                        <input type="checkbox" name="terms" required=""> I have read and agree to the <a href="{{ URL::to(env('OC_BASE_URL').'/index.php?route=information/information&information_id=3') }}">Privacy Policy</a>
                                    </label>
                                </div>
                                <div id="terms"></div>
                            </div>
                            <div class="col-xs-6" style="{{($_GET['r'] == 'standar-customer') ? 'display: none;' : 'display:block;'}}">
                                <button type="button" class="btn btn-primary btn-block btn-flat" id="btn-back-step">Back</button>
                            </div>
                            <div class="{{($_GET['r'] == 'standar-customer') ? 'col-xs-4' : 'col-xs-6'}}">
                                <button type="submit" class="btn btn-primary btn-block btn-flat" id="button-register">Register</button>
                            </div>
                        </div>
                        @if($_GET['r'] == 'partner' || $_GET['r'] == 'corporate')
                            <div class="submit-first">
                                <div class="col-xs-12">
                                    <button type="button" class="btn btn-primary btn-block btn-flat" id="btn-next-step">Next</button>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </form>
        @if($_GET['r'] == 'standar-customer')
            <div class="row">
                <div class="col-md-12">
                    @include('layouts.partial.socials')
                </div>
            </div>
        @endif
        <a href="{{ route('login') }}" class="text-center">Back to login page</a>
    </div>
</div>

<!-- modal confirm change payment method .start here -->
<div class="modal fade" id="modal-payment-method">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Description Payment Method</h4>
      </div>
      <div class="modal-body">
        <p class="ask-confirm">Are you sure for do this?</p>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times m-right-10"></i> No</button>
        <button type="button" class="btn btn-primary" id="btn-confirm"><i class="fa fa-check m-right-10"></i> Yes</button>
      </div> -->
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- modal confirm change payment method .end here -->

<!-- Modal content-->
<div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="myModalLabel">Success Register</h4>
      </div>
      <div class="modal-body">
        <p>Thanks for registering.</p>
      </div>
      <div class="modal-footer">
        <a href="{{ route("login") }}" id="btn_confirm_login" name="btn_confirm_login"><button type="button" class="btn btn-primary btn-ok">Login</button></a>
      </div>
    </div>
  </div>
</div>


@endsection

@section('scripts')
<!-- iCheck -->
{!! Html::script('bower_components/AdminLTE/plugins/iCheck/icheck.min.js') !!}
<!-- Laravel Javascript Validation -->
{!! Html::script('vendor/jsvalidation/js/jsvalidation.min.js') !!}
{!! JsValidator::formRequest('App\Http\Requests\Auth\PostRegisterRequest', '#form-register'); !!}
<script type="text/javascript">
    var step = 'complete';
    var type = $('.customer-group').val();
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        $("input[name='customer_group_id']").on('ifChecked', function(event){
            var val_group = $(this).val();
            if ( val_group == 2 || val_group == 4) {
                $('#corporate-field').show();
            } else {
                $('#corporate-field').hide();
            }
        });

    });

    $(".select2").select2({
        'placeholder': 'Select',
    });

    function countryId(country_id) {
        $.ajax({
            type:'get',
            url:'detail-country/'+country_id,
            dataType: 'json',
            success:function(data) {
                
                if(country_id!=188){
                    $('#register-region').show();
                }

                $('#region-by-country').html('').select2({data: null});
                $.each(data,function(key, val){
                    $('#region-by-country').append('<option value="'+val.zone_id+'">'+val.name+'</option>');
                });

                if(country_id==188){
                    $('#register-region').hide();
                }
                
            },
            error:function(data) {
                console.log(data);
            }
        });
    }

    $('#button-register').click(function(event) {
        event.preventDefault();
        var $btn = $(this).button('loading');
        var error1 = 0;
        var error2 = 0;
        $('.form-group').removeClass('has-error');
        $('.help-block').remove();
        $.ajax({
            type:'post',
            url:'{{ $register_type != "social-media" ? route("auth-register-post") : route("auth-register-post-social",["id_user_social" => $id_user_social]) }}', // edited ali
            data: $('#form-register').serialize(),
            dataType: 'json',
            success:function(data) {
                if (data.status.code == 200) {
                    
		              console.log(data.status);
		              console.log(data);

                    $('#btn_confirm_login').attr('href',data.data.redirect_url);

                    var type = $('.customer-group').val();
                    var message = '';
                    if (type == 'standar-customer') {
                        message = 'Thanks for registering';
                    } else {
                        message = 'Thanks for registering, your account is being verified by admin';
                    }
                    $("#modal-confirm").modal();
                     toastr.success(message);
                    // setTimeout(function(){ window.location = '{{ route("login") }}'; }, 3000);
                    
                } else {    
                    toastr.warning(data.status.message);
                };
                $btn.button('reset');
            
            },
            error:function(data) {
                var resdata = data.responseJSON;
                if (data.status == 422) {
                    if (resdata.result) {
                        $('.form-group').addClass('has-error');
                    }

                    $.each(resdata.data,function(key, val){
                        $('#register-'+key).addClass('has-error');
                        $('<span class="help-block">'+val+'</span>').insertAfter($('#'+key));

                        if ($('#register-'+key).parent('div#step-1-form').attr('id') == 'step-1-form') {
                            error1++;
                        }

                        if ($('#register-'+key).parent('div#step-2-form').attr('id') == 'step-2-form') {
                            error2++;
                        }

                    });
                    if (type == 'partner' || type == 'corporate') {
                        getError(error1, error2);
                    }
                } else {
                    toastr.error(resdata.status.message);
                }
                
                $btn.button('reset');
            }
        });
    });

    function getError(error1, error2) {
        if (error1 > 0) {
            stepRegister(false);
        }
    }

    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
        var typeRole = '';
        if (type == 'partner') {
            $('.corporate-radio').remove();
            $('.standard-radio').remove();
            typeRole = 'Business Partner';
            $('.login-page').addClass('grad-background');
            step = 'first';
            $('.for-company').show();
        }

        if (type == 'corporate') {
            $('.partner-radio').remove();
            $('.standard-radio').remove();
            typeRole = 'Corporate Customer';
            $('.login-page').addClass('grad-background');
            step = 'first';
            $('.for-company').show();
        }

        if (type == 'standar-customer') {
            $('.corporate-radio').remove();
            $('.partner-radio').remove();
            typeRole = 'Consumer';
            $('.login-page').addClass('grad-background');
        }

        var title = $('.title-register').text();
        var addressTitle = $('.register-address-title').text();
        $('.title-register').html(typeRole +' Registration<br><small>'+ title+'</small>');
        $('.register-address-title').html(typeRole +'<br><small>'+ addressTitle+'</small>');

        if (step == 'first') {
            $('.submit-complete').hide();
        }
    });

    $('#btn-next-step').click(function(){
        stepRegister();
    });

    $('#btn-back-step').click(function(){
        stepRegister(false);
    });

    function stepRegister(next=true) {
        if (next) {
            $('#step-2-form').show();
            $('#step-1-form').hide();
            $('.submit-complete').show();
            $('.submit-first').hide();
            $('.submit-complete-back').show();
        } else {
            $('#step-2-form').hide();
            $('#step-1-form').show();
            $('.submit-complete').hide();
            $('.submit-first').show();
            $('.submit-complete-back').hide();
        }
    }

    $(document).on('change','#payment_method_id',function() {
        // var selected = $(this).find('option:selected');
        // var type = selected.data('type'); 
        // if (type !== undefined) {
        //     if (type.toLowerCase() == 'credit based system') {
        //         $('.ask-confirm').html('Credit Based System is a payment method where corporate trades their amount of dollars to amount of credit that can be used to buy products. The remaining credit/token at the end of the month will not be accumulated to the following month.');
        //         $('#modal-payment-method').modal({
        //             backdrop: 'static',
        //             keyboard: false
        //         });
        //     }

        //     if (type.toLowerCase() == 'pay as you go') {
        //         $('.ask-confirm').html("Pay as You Go is a payment method where corporate has a special agreement with learn1thing to 'buy' any product from the mini-store. ODE will send invoice to the corporate at the end of the month, according to what they have used/bought.");
        //         $('#modal-payment-method').modal({
        //             backdrop: 'static',
        //             keyboard: false
        //         });
        //     }
        // }
    });
</script>
@endsection
