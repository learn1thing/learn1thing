@extends('layouts.register_template')

@section('header')
    <!-- iCheck -->
    {!! Html::style('bower_components/AdminLTE/plugins/iCheck/square/blue.css') !!}
    <style type="text/css">
        .login-box, .register-box {
            width: 360px;
            margin: 2% auto;
        }
    </style>
@endsection

@section('content')
<div class="login-box">
    <div class="login-logo">
        {!! Html::image('images/base-logo.png', null, ['class'=>"center-block img-responsive", 'width'=>'200px']) !!}
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        @include('partials.message')
        @include('partials.login.input-login')
        @include('layouts.partial.socials')
        <a href="{{ route('auth-forgot') }}">I forgot my password</a><br>
        <a href="{{ route('auth-register-business') }}" class="text-center">Register a new membership</a>
    </div>
</div>
@endsection

@section('scripts')
    <!-- iCheck -->
    {!! Html::script('bower_components/AdminLTE/plugins/iCheck/icheck.min.js') !!}
    <script type="text/javascript">
        var _baseUrl = '{{ url('/') }}';
        var _urlSuccessLogin = '{{ route('admin-dashboard') }}';
    </script>
    {!! Html::script('js/auth/auth.js') !!}
@endsection
