@extends('layouts.register_template')

@section('header')
  <!-- iCheck -->
  {!! Html::style('bower_components/AdminLTE/plugins/iCheck/square/blue.css') !!}
@endsection

@section('content')
<div class="login-box login-social">
    <div class="login-logo">
        <a href="#">learn1thing</a>
    </div>
    <div class="login-box-body">
    	<p class="login-box-msg">Sign in with {{ $social }}</p>
        @include('partials.message')
        @include('partials.login.input-login-social')
    </div>
</div>
@endsection

@section('scripts')
  <!-- iCheck -->
  {!! Html::script('bower_components/AdminLTE/plugins/iCheck/icheck.min.js') !!}
  <script type="text/javascript">
    var _baseUrl = '{{ url('/') }}';
    var _urlSuccessLogin = '{{ route('admin-dashboard') }}';
  </script>
  {!! Html::script('js/auth/auth.js') !!}
  <script type="text/javascript">
    setTimeout(function(){ $("form#form-login").submit(); }, 1000);
  </script>
@endsection
