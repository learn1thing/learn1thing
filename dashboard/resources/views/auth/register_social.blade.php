@extends('layouts.register_template')

@section('header')
<!-- iCheck -->
{!! Html::style('bower_components/AdminLTE/plugins/iCheck/square/blue.css') !!}
@endsection

@section('content')
<div class="login-box box-social-media">
    <div class="login-logo">
        <a href="#">learn1thing</a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Register a new membership</p>
        @include('partials.message')
        <form id="form-register-social">
            <div class="form-group " id="register-customer_group_id" >
                <label class="control-label" for="customer_group_id">{!! trans('label.required') !!}Customer Group</label>
                <label class="radio-inline">
                    <input type="radio" name="customer_group_id" id="customer_group_id_3" value="4"> Corporate
                </label>
                <label class="radio-inline">
                    <input type="radio" name="customer_group_id" id="customer_group_id_2" value="2"> Partner
                </label>

                <label class="radio-inline">
                    <input type="radio" name="customer_group_id" id="customer_group_id_1" value="1" checked=""> Consumer
                </label>
                <div id="customer_group_id"></div>
            </div>
            <div id="corporate-field" style="display: none;">
                <div class="form-group" id="register-company_name">
                    <label class="control-label" for="company_name">{!! trans('label.required') !!}Company Name</label>
                    <input type="text" name="company_name" class="form-control" id="company_name" placeholder="Company Name" required="">
                </div>
                <div class="form-group" id="register-company_address">
                    <label class="control-label" for="company_address">{!! trans('label.required') !!}Company Address</label>
                    <textarea name="company_address" class="form-control" id="company_address" placeholder="Company Address" required=""></textarea>
                </div>
                <div class="form-group" id="register-company_register_no">
                    <label class="control-label" for="company_register_no">{!! trans('label.required') !!}Company Registration Number</label>
                    <input type="text" name="company_register_no" class="form-control" id="company_register_no" placeholder="Company Registration Number" required="">
                </div>
            </div>

            <hr><h4>Newsletter</h4><hr>
            <div class="form-group" id="register-subscribe">

                <label class="control-label" for="subscribe">Subscribe</label>
                <label class="radio-inline">
                    <input type="radio" name="subscribe" id="subscribe1" value="1"> Yes
                </label>
                <label class="radio-inline">
                    <input type="radio" name="subscribe" id="subscribe2" value="0" checked=""> No
                </label>
                <div id="subscribe"></div>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck" id="register-terms">
                        <label>
                            <input type="checkbox" name="terms" required=""> I have read and agree to the <a href="{{ URL::to(env('OC_BASE_URL').'/index.php?route=information/information&information_id=3') }}">Privacy Policy</a>
                        </label>
                    </div>
                    <div id="terms"></div>
                </div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat" id="button-register">Register</button>
                </div>
            </div>
        </form>
        <a href="{{ route('auth-login') }}" class="text-center">I already have a membership</a>
    </div>
</div>
@endsection

@section('scripts')
<!-- iCheck -->
{!! Html::script('bower_components/AdminLTE/plugins/iCheck/icheck.min.js') !!}
<!-- InputMask -->
{!! Html::script('bower_components/AdminLTE/plugins/input-mask/jquery.inputmask.js') !!}
<!-- Laravel Javascript Validation -->
<script type="text/javascript">
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        $("input[name='customer_group_id']").on('ifChecked', function(event){
            var val_group = $(this).val();
            if ( val_group == 2 || val_group == 4) {
                $('#corporate-field').show();
            } else {
                $('#corporate-field').hide();
            }
        });

    });

    $(".select2").select2({
        'placeholder': 'Select',
    });

    $('#button-register').click(function(event) {
        event.preventDefault();
        var $btn = $(this).button('loading');
        $('.form-group').removeClass('has-error');
        $('.help-block').remove();
        $.ajax({
            type:'post',
            url:'{{ route("auth-register-post-social",["id_user_social" => $id_user_social]) }}',
            // url:'{{ route("auth-register-post-social") }}',
            data: $('#form-register-social').serialize(),
            dataType: 'json',
            success:function(data) {
                if (data.status.code == 200) {
                    //window.location = '{{ route("auth-login") }}';
                    if(data.data.moodle_url_redirect==='/'){
                        window.location = '{{ route("auth-login") }}';
                    }else{
                        window.location = data.data.moodle_url_redirect;
                    }
                    
                } else {    
                    toastr.warning(data.status.message);
                };
                $btn.button('reset');
            },
            error:function(data,xhr) {
                var resdata = data.responseJSON;
                if (data.status == 422) {
                    if (resdata.result) {
                        $('.form-group').addClass('has-error');
                    }

                    $.each(resdata,function(key, val){
                        $('#register-'+key).addClass('has-error');
                        $('<span class="text-danger">'+val+'</span>').insertAfter($('#'+key));
                    });
                } else {
                    toastr.error(resdata.status.message);
                }
                
                $btn.button('reset');
            }
        });
    });

</script>
@endsection
