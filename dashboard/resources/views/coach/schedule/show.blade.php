@extends('layouts.admin_template')

@section('title', 'Schedule Course')
@section('page_title', 'Coach')
@section('page_description', 'learn1thing')

@section('header')
{!! Html::style('bower_components/fullcalendar/dist/fullcalendar.min.css') !!}
{!! Html::script('bower_components/moment/min/moment.min.js') !!}
{!! Html::script('bower_components/fullcalendar/dist/fullcalendar.min.js') !!}
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Coach</li>
        <li>Schedule</li>
    </ol>
@endsection

@section('content')

<div class="row">
    
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                
                <div class="col-sm-6 col-md-offset-3">
                    <!-- <h1 class="box-title" style="margin-top:10px">Schedule <b>{{ $name }}</b></h1> -->
                    <form class="form-horizontal">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">Name</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="name" placeholder="Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="decription" class="col-sm-2 control-label">Description</label>

                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="3" placeholder="Decription"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile" class="col-sm-2 control-label">Photo</label>
                                <div class="col-sm-10">
                                    <input type="file" id="exampleInputFile">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- <div class="col-sm-6 text-right">
                    <a href="{{ route('coach.schedule.create',$user_id) }}" class="btn btn-primary">
                        <i class="fa fa-plus margin-r-5"></i> {{trans('label.create')}}
                    </a>
                </div> -->
            </div>
            <div class="box-body">
                @include('partials.message')
                <div class="col-md-12">
                    <div class="box box-primary box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Coach Calendar</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>

                        <div class="box-body" style="display: block;">
                            {!! $calendar->calendar() !!}
                            {!! $calendar->script() !!}
                        </div>
                    </div>
                </div>
               <!--  <div class="col-md-4">
                    <div class="box box-widget widget-user-2">
                        <div class="widget-user-header bg-primary" style="padding:1px 20px">
                            <h3>Course</h3>
                            </div>
                            <div class="box-footer no-padding">
                                <ul class="nav nav-stacked">
                                    @if ( sizeof( $courses ) > 0)
                                        @foreach( $courses as $course)
                                            <li>
                                                <a href="#">{{ $course }} </a>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div> -->
                <!-- /.widget-user -->
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script type="text/javascript">
    $('div.alert').delay(3000).slideUp(300);
</script>
@endsection