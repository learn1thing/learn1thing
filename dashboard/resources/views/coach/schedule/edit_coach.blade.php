@extends('layouts.register_template')

@section('header')
{!! Html::style('bower_components/fullcalendar/dist/fullcalendar.min.css') !!}
{!! Html::script('bower_components/moment/min/moment.min.js') !!}
{!! Html::script('bower_components/fullcalendar/dist/fullcalendar.min.js') !!}

<style type="text/css">
    .login-box , .register-box {
        width: auto;
        max-width: 95%;
        padding-top: 10px;
        margin-top: 50px;
    }
    .login-box-body , .register-box-body {
        overflow: hidden;
        overflow-y: scroll;
    }
    .overflow-content {
        max-height: 80px;
        min-height: 80px;
        overflow: hidden;
        overflow-y: scroll;
    }
    .widget-user-2 .widget-user-header {
        min-height: 90px;
    }
    .header_dummy {
        width: 100%;
        height: 55px;
        background: #2d4c8c;
        /*position: fixed;*/
        top: 0;
    }
    #content {
        width: 95%;
        margin: 0 auto;
        padding: 60px 0;
    }
    .footer_dummy {
        width: 100%;
        height: 100px;
        /*position: fixed;*/
        bottom: 0;
    }
    .login-logo {
        margin-bottom:10px;
    }
    .profile-user-img {
        margin: 0 auto;
        width: 170px;
        padding: 3px;
        border: 3px solid #d2d6de;
    }

    #gradiennt{
        background-color:#ffffff;
        filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#ffffff, endColorstr=#3ed1fb);
        background-image:-moz-linear-gradient(top, #ffffff 10%, #3ed1fb 48%,#2c5897 100%);
        background-image:linear-gradient(top, #ffffff 10%, #3ed1fb 48%,#2c5897 100%);
        background-image:-webkit-linear-gradient(top, #ffffff 10%, #3ed1fb 48%,#2c5897 100%);
        background-image:-o-linear-gradient(top, #ffffff 10%, #3ed1fb 48%,#2c5897 100%);
        background-image:-ms-linear-gradient(top, #ffffff 10%, #3ed1fb 48%,#2c5897 100%);
        background-image:-webkit-gradient(linear, right top, right bottom, color-stop(10%,#ffffff), color-stop(48%,#3ed1fb),color-stop(100%,#2c5897));
        }

</style>
@endsection

@section('content')
<div class="header_dummy"> 
    <div class="col-sm-2">
        <img src="{{URL::asset('images/l1t-logo-white.png')}}" height="50px" width="100px">
    </div>
    <div class="col-sm-2">
        
    </div>
</div>
<div id="content" class="login-box">
    <div class="login-logo">
        Available Coaches
    </div>
    <div class="login-box-body">
       
        <div class="row task-schedule" style="padding-top: 10px;">
            <div class="col-sm-2" >
                <img class="profile-user-img img-responsive img-circle" src="{{ url_to_image($coach->photo) }}" alt="User profile picture" height="100px" width="170px">
            </div>
            <div class="col-sm-9" style="padding: 5px">
                <b>{{ $coach->name }}</b> <br><br>
                <small>
                    {!! $coach->description !!}
                </small>
                </br>
                @if(@$_GET['product_id'])
                <b>
                    {{ @$sess }} Session
                </b>
                @endif
            </div>
            <BR>
            <input type="hidden" name="sess" id="sess" value="{{$sess}}">
            <div class="col-sm-12 text-right"> 
                <!-- <a href="http://future.learn1thing.com/index.php?route=checkout/cart" class="btn btn-success">Submit</a> -->
                @if (@$_GET['type_order'] == 'corporate')
                    <a href="{{ route('admin.corporate-customer.request-product-list.detail',baseDecrypt($_GET['order_product_id'])) }}" class="btn btn-primary">Back</a>
                @else
                    <a href="{{ route('admin.order.history.detail',$order_id) }}" class="btn btn-primary">Back</a>

                @endif
                <button class="btn btn-success" id="act-submit"><i class="fa fa-check m-right-10"></i>Submit</button>
                <!-- <a href="javascript:void(0)" class="btn btn-success" id="act-submit">Submit</a> -->
            </div>
        </div>
        <span id="value-temp" style="display:none">
            {!! $temp_current_value !!}
        </span>

            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Calendar for Customers</h3>
                </div>
                <div class="box-body ">
            
                    <div class="row">
                        <div class="col-md-12">
                            <span class="label label-default" style="background-color: #3a87ad; color:#fff">Available</span>
                            &nbsp :  the schedules that are available to choose
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <span class="label label-default" style="background-color: #ff851b; color:#fff"">Booking</span>
                            &nbsp :  the schedule you booked
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12"><span class="label label-default" style="background-color: #474f54; color:#fff"">Passed</span>
                        &nbsp :  the expired schedules, cannot be chosen
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12"><span class="label label-default" style="background-color: #ff0000; color:#fff"">Booked</span>
                        &nbsp :  the schedules booked by other customers
                        </div>
                    </div>
                </div>
            </div>
        

        <div class="row task-schedule" style="">
            <div class="col-sm-12">
                <hr>
                <div class="box box-primary box-solid">
                    <div class="box-body" style="display: block;">
                        {!! $calendar->calendar() !!}
                        {!! $calendar->script() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer_dummy"> 
        <img src="{{URL::asset('images/footer-dummy.png')}} " width="100%">
</div>
@endsection

@section('scripts')
  <!-- iCheck -->
  {!! Html::script('bower_components/AdminLTE/plugins/iCheck/icheck.min.js') !!}
  <!-- <script type="text/javascript">
    function getSchedule(index) {
        $(".task-schedule"+index).css('display','block');
        $(".task-schedule").css('display','block');
        $(".task-coach").css('display','none');
        $('.fc-agendaWeek-button').click();
        $('.fc-agendaWeek-button').css('display','none');
        $('.fc-month-button').css('display','none');
        $('.fc-agendaDay-button').css('display','none');
        $('.fc-today-button').css('display','none');
        $('.fc-center').html('');
        $('.fc-center').html('<div class="row"><div class="col-sm-4"><span class="pull-right badge" style="background-color:#6aa4c1"> Available </span></div> &nbsp; <div class="col-sm-4"><span class="pull-right badge bg-orange"> Selected </span></div></div>');
    }

    function close_window() {
        window.close()
    }

    function back(index) {
        $(".task-schedule"+index).css('display','none');
        $(".task-schedule").css('display','none');
        $(".task-coach").css('display','block');
    }
  </script> -->
  {!! Html::script('js/auth/auth.js') !!}


  <script type="text/javascript">
        // var tempSesi = 1;
        var maxSesi = 30*'{{ $sess }}';
        var countSesi = 0;
        $(document).ready(function(){
            $("#act-submit").on('click', function(){
                var temp_id = $("#value-temp").children();
                var temp_ids = [];
                var type_order = '{{ @$_GET["type_order"] }}';
                var order_id = "{{ @$_GET['order_id'] }}";
                var order_product_id = "{{ baseDecrypt(@$_GET['order_product_id']) }}";

                temp_id.each(function(){
                    temp_ids.push($(this).attr('id'));
                });
                $('#act-submit').html('Processing').attr('disabled', true);

                if (type_order == 'corporate') {
                    var directLink = '{{ route("admin.corporate-customer.request-product-list.detail",baseDecrypt(@$_GET["order_product_id"])) }}';
                } else {
                    var directLink = '{{ route("admin.order.history.detail",@$_GET["order_id"]) }}';
                }

                $.ajax({
                    type:'post',
                    url:'{{ route("coach.schedule.order.update") }}',
                    data: {course_id : "{{ $course_id }}", schedule_id : temp_ids, product_id:"{{ $_GET['product_id'] }}", type_order: type_order, order_id:order_id, order_product_id: order_product_id },
                    success:function(data) {
                        if (data.code == 200) {
                            toastr.success(data.message);
                            window.location = directLink;
                        } else {    
                            toastr.warning(data.message);
                        };
                        $('#act-submit').html('Submit').attr('disabled', false);
                    },
                    error:function(data) {
                        var resdata = data.responseJSON;
                        toastr.error(resdata.message);
                        $('#act-submit').html('Submit').attr('disabled', false);
                        // $btn.button('reset');
                    }
                });

            });
            
        });
  </script>

@endsection


