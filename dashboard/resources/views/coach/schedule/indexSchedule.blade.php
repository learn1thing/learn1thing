@extends('layouts.register_template')

@section('header')
{!! Html::style('bower_components/fullcalendar/dist/fullcalendar.min.css') !!}
{!! Html::script('bower_components/moment/min/moment.min.js') !!}
{!! Html::script('bower_components/fullcalendar/dist/fullcalendar.min.js') !!}

<style type="text/css">
    .login-box , .register-box {
        width: auto;
        max-width: 95%;
        padding-top: 10px;
        margin-top: 50px;
    }
    .login-box-body , .register-box-body {
        max-height: 410px;
        overflow: hidden;
        overflow-y: scroll;
    }
    .overflow-content {
        max-height: 80px;
        min-height: 80px;
        overflow: hidden;
        overflow-y: scroll;
    }
    .widget-user-2 .widget-user-header {
        min-height: 90px;
    }
    .header_dummy {
        width: 100%;
        height: 55px;
        background: #2d4c8c;
        /*position: fixed;*/
        top: 0;
    }
    #content {
        width: 95%;
        margin: 0 auto;
        padding: 60px 0;
    }
    .footer_dummy {
        width: 100%;
        height: 100px;
        /*position: fixed;*/
        bottom: 0;
    }
    .login-logo {
        margin-bottom:10px;
    }

    #gradiennt{
        background-color:#ffffff;
        filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#ffffff, endColorstr=#3ed1fb);
        background-image:-moz-linear-gradient(top, #ffffff 10%, #3ed1fb 48%,#2c5897 100%);
        background-image:linear-gradient(top, #ffffff 10%, #3ed1fb 48%,#2c5897 100%);
        background-image:-webkit-linear-gradient(top, #ffffff 10%, #3ed1fb 48%,#2c5897 100%);
        background-image:-o-linear-gradient(top, #ffffff 10%, #3ed1fb 48%,#2c5897 100%);
        background-image:-ms-linear-gradient(top, #ffffff 10%, #3ed1fb 48%,#2c5897 100%);
        background-image:-webkit-gradient(linear, right top, right bottom, color-stop(10%,#ffffff), color-stop(48%,#3ed1fb),color-stop(100%,#2c5897));
        }

</style>
@endsection

@section('content')
<div class="header_dummy"> 
    <div class="col-sm-2">
        <img src="{{URL::asset('images/l1t-logo-white.png')}}" height="50px" width="100px">
    </div>
    <div class="col-sm-2">
        
    </div>
</div>
<div id="content" class="login-box">
    <div class="login-logo">
        Available Coaches
    </div>
    <div class="row">
        <div class="row task-coach" style="display:block;">
            @if ( count($coaches) > 0 )
                @foreach ($coaches as $coach)
                <div class="col-md-3"" style="border: solid 0px;padding: 10px;max-height: 200px;min-height: 200px;">
                    <div class="box box-widget widget-user-2">
                        <div class="widget-user-header bg-blue">
                            <div class="widget-user-image">
                                <img class="profile-user-img img-responsive img-circle" src="{{ url_to_image($coach->photo) }}" alt="User profile picture">
                            </div>
                            <h3 class="widget-user-username" style="font-size:20px;"><a href="{{ route('detail.coach',[$course_id,$coach->id]).'?code='.$_GET['code'].'&product_id='.$_GET['product_id'].(!empty($_GET['order_product_id']) ?'&order_product_id='.$_GET['order_product_id']:'').(!empty($_GET['type_order'])?'&type_order='.$_GET['type_order']:'').(!empty($_GET['user_id'])?'&user_id='.$_GET['user_id']:'') }}" style="color:white;">{{ $coach->name }}</a></h3>
                        </div>
                        <div class="box-footer no-padding overflow-content">
                            <ul class="nav nav-stacked" style="padding:10px">
                                {!! $coach->description !!}
                            </ul>
                        </div>
                    </div>
                </div>
                @endforeach
            @else
                <div class="alert alert-success">
                    <strong>Success!</strong> No available data coaches.
                </div>
            @endif
        </div>
        <div class="row">
            <div class="col-md-6">
                @if (!empty($_GET['type_order']) && $_GET['type_order'] == 'corporate')
                <a href="{{ route('login') }}" type="button" class="btn btn-default">
                    <i class="fa fa-angle-double-left m-right-10"></i>Back 
                </a>
                @else
                <a href="{{ env('OC_BASE_URL') }}" type="button" class="btn btn-default">
                    <i class="fa fa-angle-double-left m-right-10"></i>Back 
                </a>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="footer_dummy"> 
        <img src="{{URL::asset('images/footer-dummy.png')}} " width="100%">
</div>
@endsection

@section('scripts')
  <!-- iCheck -->
  {!! Html::script('bower_components/AdminLTE/plugins/iCheck/icheck.min.js') !!}
  <script type="text/javascript">
    function getSchedule(index) {
        $(".task-schedule"+index).css('display','block');
        $(".task-schedule").css('display','block');
        $(".task-coach").css('display','none');
        $('.fc-agendaWeek-button').click();
        $('.fc-agendaWeek-button').css('display','none');
        $('.fc-month-button').css('display','none');
        $('.fc-agendaDay-button').css('display','none');
        $('.fc-today-button').css('display','none');
        $('.fc-center').html('');
        $('.fc-center').html('<div class="row"><div class="col-sm-4"><span class="pull-right badge" style="background-color:#6aa4c1"> Available </span></div> &nbsp; <div class="col-sm-4"><span class="pull-right badge bg-orange"> Selected </span></div></div>');
    }

    function close_window() {
        window.close()
    }

    function back(index) {
        $(".task-schedule"+index).css('display','none');
        $(".task-schedule").css('display','none');
        $(".task-coach").css('display','block');
    }
  </script>
  {!! Html::script('js/auth/auth.js') !!}
@endsection


