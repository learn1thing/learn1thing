@extends('layouts.admin_template')

@section('title', 'Schedule Course')
@section('page_title', 'Schedule Course')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Coach</li>
        <li>Coach Schedule</li>
    </ol>
@endsection

@section('content')

<div class="row">
    @include('partials.message')
    
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <div class="col-sm-6">
                    <h1 class="box-title" style="margin-top:10px">List Coach Schedule</h1>
                </div>
            </div>
            <div class="box-body">
                @if(Session::has('flash_message'))
                    <div class="alert alert-success"><em> {!! session('flash_message') !!}</em></div>
                @endif
                @if(Session::has('error'))
                    <div class="alert alert-error"><em> {!! session('error') !!}</em></div>
                @endif
                
                <div class="table-responsive">
                    {!! $dataTable->table(['class' => 'datatable table table-striped', 'cellspacing'=>"0", 'width'=>"100%"]) !!}
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('scripts')
{!! $dataTable->scripts() !!}
@endsection