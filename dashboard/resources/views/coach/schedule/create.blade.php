@extends('layouts.admin_template')

@section('title', 'Schedule Course')
@section('page_title', 'Schedule Course')
@section('page_description', 'learn1thing')

@section('header')
{!! Html::style('bower_components/AdminLTE/plugins/datepicker/datepicker3.css') !!}
{!! Html::style('bower_components/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.css') !!}
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('coach.course.index') }}"> List Course</a></li>
        <li>Create Course</li>
    </ol>
@endsection

@section('content')
    {!! Form::open([
            'route'     =>['coach.schedule.store'],
            'method'    => 'POST',
            'class' =>  'form-horizontal',
            'id'    =>  'form-course',
        ]) !!}
        <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-create"></i> Create</h3>
            </div>
            <div class="box-body">
                @include('partials.message')
                @include('coach.schedule._form')
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <a href="{{ route('coach.schedule.show',$user_id) }}" class="btn btn-default">
                            {{trans('label.cancel')}}
                        </a>
                        <button type="submit" class="btn btn-primary" id="btn-submit">{{trans('label.save')}}</button>
                    </div>
                </div>              
            </div>
        </div>
    {!! Form::close() !!}
@endsection
@section('scripts')
{!! Html::script('bower_components/AdminLTE/plugins/datepicker/bootstrap-datepicker.js') !!}
{!! Html::script('bower_components/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.js') !!}
<script type="text/javascript">
     $(function () {
        //Initialize Select2 Elements

        //Date picker
        $('.datepicker').datepicker({
          autoclose: true,
          format:"yyyy-mm-dd"
        });

        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });

    });
    
    $('div.alert').delay(3000).slideUp(300);
</script>
@endsection


