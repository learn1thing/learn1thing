@extends('layouts.register_template')

@section('header')
{!! Html::style('bower_components/fullcalendar/dist/fullcalendar.min.css') !!}
{!! Html::script('bower_components/moment/min/moment.min.js') !!}
{!! Html::script('bower_components/fullcalendar/dist/fullcalendar.min.js') !!}

<style type="text/css">
    .login-box , .register-box {
        width: auto;
        max-width: 95%;
        padding-top: 10px;
        margin-top: 50px;
    }
    .login-box-body , .register-box-body {
        overflow: hidden;
        overflow-y: scroll;
    }
    .overflow-content {
        max-height: 80px;
        min-height: 80px;
        overflow: hidden;
        overflow-y: scroll;
    }
    .widget-user-2 .widget-user-header {
        min-height: 90px;
    }
    .header_dummy {
        width: 100%;
        height: 55px;
        background: #2d4c8c;
        /*position: fixed;*/
        top: 0;
    }
    #content {
        width: 95%;
        margin: 0 auto;
        padding: 60px 0;
    }
    .footer_dummy {
        width: 100%;
        height: 100px;
        /*position: fixed;*/
        bottom: 0;
    }
    .login-logo {
        margin-bottom:10px;
    }
    .profile-user-img {
        margin: 0 auto;
        width: 170px;
        padding: 3px;
        border: 3px solid #d2d6de;
    }

    #gradiennt{
        background-color:#ffffff;
        filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#ffffff, endColorstr=#3ed1fb);
        background-image:-moz-linear-gradient(top, #ffffff 10%, #3ed1fb 48%,#2c5897 100%);
        background-image:linear-gradient(top, #ffffff 10%, #3ed1fb 48%,#2c5897 100%);
        background-image:-webkit-linear-gradient(top, #ffffff 10%, #3ed1fb 48%,#2c5897 100%);
        background-image:-o-linear-gradient(top, #ffffff 10%, #3ed1fb 48%,#2c5897 100%);
        background-image:-ms-linear-gradient(top, #ffffff 10%, #3ed1fb 48%,#2c5897 100%);
        background-image:-webkit-gradient(linear, right top, right bottom, color-stop(10%,#ffffff), color-stop(48%,#3ed1fb),color-stop(100%,#2c5897));
        }

</style>
@endsection

@section('content')
<div class="header_dummy"> 
    <div class="col-sm-2">
        <img src="{{URL::asset('images/l1t-logo-white.png')}}" height="50px" width="100px">
    </div>
    <div class="col-sm-2">
        
    </div>
</div>
<div id="content" class="login-box">
    <div class="login-logo">
        Available Coaches
    </div>
    <div class="login-box-body">
       
        <div class="row task-schedule" style="padding-top: 10px;">
            <div class="col-sm-2" >
                <img class="profile-user-img img-responsive img-circle" src="{{ url_to_image($coach->photo) }}" alt="User profile picture" height="100px" width="170px">
            </div>
            <div class="col-sm-9" style="padding: 5px;">
                <b>{{ $coach->name }}</b> <br><br>
                <small>
                    {!! $coach->description !!}
                </small>
                <br/>
                @if(@$_GET['product_id'])
                <!-- <b>
                    {{ @$sess }} Session
                </b>
		-->
                @endif
                <input type="hidden" name="sess" id="sess" value="{{$sess}}">
                <input type="hidden" name="_haveChooseBefore" id="_haveChooseBefore" value="0">
                <input type="hidden" name="_keyBefore" id="_keyBefore" value="0">
                <input type="hidden" name="_keyDateBefore" id="_keyDateBefore" value="0">
            </div>
            <BR>
            <div class="col-sm-12 text-right"> 
                <!-- <a href="http://future.learn1thing.com/index.php?route=checkout/cart" class="btn btn-success">Submit</a> -->
                <a href="{{ route('coach.scheduler', $course_id).'?code='.$_GET['code'].'&product_id='.$_GET['product_id'] }}" class="btn btn-default"><i class="fa fa-angle-double-left m-right-10"></i> Back</a>
                <button class="btn btn-success" type="button" id="act-submit">
                    <i class="fa fa-check m-right-10"></i>Submit
                </button>
            </div>
        </div>
        <span id="value-temp" style="display:none"></span>
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Calendar for Customers</h3>
            </div>
            <div class="box-body ">
                <div class="row">
                    <div class="col-md-12">
                        <span class="label label-default" style="background-color: #3a87ad; color:#fff">Available</span>
                        &nbsp :  the schedules that are available to choose
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <span class="label label-default" style="background-color: #ff851b; color:#fff"">Booking</span>
                        &nbsp :  the schedule you booked
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12"><span class="label label-default" style="background-color: #474f54; color:#fff"">Passed</span>
                    &nbsp :  the expired schedules, cannot be chosen
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12"><span class="label label-default" style="background-color: #ff0000; color:#fff"">Booked</span>
                    &nbsp :  the schedules booked by other customers
                    </div>
                </div>
            </div>
        </div>
        <div class="row task-schedule" style="">
            <div class="col-sm-12">
                <hr>
                <div class="box box-primary box-solid">
                    <div class="box-body" style="display: block;">
                        {!! $calendar->calendar() !!}
                        {!! $calendar->script() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer_dummy"> 
        <img src="{{URL::asset('images/footer-dummy.png')}} " width="100%">
</div>
@endsection

@section('scripts')
  <!-- iCheck -->
  {!! Html::script('bower_components/AdminLTE/plugins/iCheck/icheck.min.js') !!}
  <!-- <script type="text/javascript">
    function getSchedule(index) {
        $(".task-schedule"+index).css('display','block');
        $(".task-schedule").css('display','block');
        $(".task-coach").css('display','none');
        $('.fc-agendaWeek-button').click();
        $('.fc-agendaWeek-button').css('display','none');
        $('.fc-month-button').css('display','none');
        $('.fc-agendaDay-button').css('display','none');
        $('.fc-today-button').css('display','none');
        $('.fc-center').html('');
        $('.fc-center').html('<div class="row"><div class="col-sm-4"><span class="pull-right badge" style="background-color:#6aa4c1"> Available </span></div> &nbsp; <div class="col-sm-4"><span class="pull-right badge bg-orange"> Selected </span></div></div>');
    }

    function close_window() {
        window.close()
    }

    function back(index) {
        $(".task-schedule"+index).css('display','none');
        $(".task-schedule").css('display','none');
        $(".task-coach").css('display','block');
    }
  </script> -->
  {!! Html::script('js/auth/auth.js') !!}


  <script type="text/javascript">
    var maxSesi = 30*'{{ $sess }}';
    var countSesi = 0;
        $(document).ready(function(){
            $("#act-submit").on('click', function(){
            $('#act-submit').html('Processing').attr('disabled', true);
                var temp_id = $("#value-temp").children();
                var temp_ids = [];
                temp_id.each(function(){
                    temp_ids.push($(this).attr('id'));
                });

                $.ajax({
                    type:'post',
                    url:'{{ route("schedule.coach.submit.order") }}',
                    data: {course_id : "{{ $course_id }}", 
                            schedule_id : temp_ids, 
                            code:"{{ @$_COOKIE['code'] }}",
                            product_id:"{{ @$_GET['product_id'] }}",
                            order_product_id:"{{ @$_GET['order_product_id'] }}",
                            type_order:"{{ @$_GET['type_order'] }}",
                            user_id:"{{ @$_GET['user_id'] }}",
                        },
                    success:function(data) {
                        if (data.code == 200) {
                            toastr.success(data.message);
                            var type_order = '{{ @$_GET["type_order"] }}';
                            if (type_order == 'corporate') {
                                window.location = '{{ route("admin-dashboard") }}';    
                            } else {
                                window.location = '{{ env("OC_BASE_URL") }}'+'/index.php?route=checkout/cart';    
                            }
                        } else {    
                            toastr.warning(data.message);
                        };
                        $('#act-submit').html('Submit').attr('disabled', false);
                    },
                    error:function(data) {
                        var resdata = data.responseJSON;
                        if (resdata.code == 403) {
                            setTimeout(function() {
                                window.location = '{{ route("login") }}';
                            }, 2000);
                        }
                        toastr.error(resdata.message);
                        $('#act-submit').html('Submit').attr('disabled', false);    
                    }
                });

            });
            
        });
  </script>

@endsection


