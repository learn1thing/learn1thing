<div class="form-group {{ $errors->has('course_id') ? 'has-error' : ''}}">
    {!! Form::label('course_id', trans('label.course.name'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-4">
        {!! Form::hidden('user_id', $user_id , ['class' => 'form-control']) !!}
        {!! Form::select('course_id', $course, null, ['class' => 'form-control']) !!}
        {!! $errors->first('course_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('schedule_date') ? 'has-error' : ''}}">
    {!! Form::label('schedule_date', trans('label.schedule.date'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-2">
        {!! Form::text('schedule_date', old('schedule_date') , ['class' => 'datepicker form-control']) !!}
        {!! $errors->first('schedule_date', '<p class="help-block">:message</p>') !!}
    </div>
    {!! Form::label('schedule_date', trans('label.schedule.date_until'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-2">
        {!! Form::text('schedule_until_date', old('schedule_until_date') , ['class' => 'datepicker form-control']) !!}
        {!! $errors->first('schedule_until_date', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('start_time') ? 'has-error' : ''}}">
    {!! Form::label('start_time', trans('label.schedule.start_time'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="bootstrap-timepicker col-sm-2">
        {!! Form::text('start_time', old('label.schedule.start_time') , ['class' => 'form-control timepicker']) !!}
        {!! $errors->first('start_time', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group {{ $errors->has('end_time') ? 'has-error' : ''}}">
        {!! Form::label('end_time', trans('label.schedule.end_time'), ['class' => 'col-sm-2 control-label']) !!}
        <div class="bootstrap-timepicker col-sm-2">
            {!! Form::text('end_time', old('end_time') , ['class' => 'form-control timepicker']) !!}
            {!! $errors->first('end_time', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<div class="form-group {{ $errors->has('appoinment') ? 'has-error' : ''}}">
    {!! Form::label('actived', trans('label.schedule.appoinment'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        @foreach ($days as $day)
            {{ Form::checkbox('day[]', $day) }} &nbsp;
            {{ Form::label('day', $day) }}<br>
        @endforeach

        {!! $errors->first('appoinment', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('devine') ? 'has-error' : ''}}">
    {!! Form::label('devine', trans('label.schedule.devine'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-2">
        {!! Form::select('devine', $devine, null, ['class' => 'form-control']) !!}
        {!! $errors->first('devine', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('duration') ? 'has-error' : ''}}">
    {!! Form::label('duration', trans('label.schedule.duration'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-2">
        {!! Form::text('duration', old('label.schedule.duration') , ['class' => 'form-control']) !!}
        {!! $errors->first('duration', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('break') ? 'has-error' : ''}}">
    {!! Form::label('break', trans('label.schedule.break'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-2">
        {!! Form::text('break', old('label.schedule.break') , ['class' => 'form-control']) !!}
        {!! $errors->first('break', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('overlap') ? 'has-error' : ''}}">
    {!! Form::label('overlap', trans('label.schedule.overlap'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-2">
        {!! Form::select('overlap', $overlap, null, ['class' => 'form-control']) !!}
        {!! $errors->first('overlap', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('allow') ? 'has-error' : ''}}">
    {!! Form::label('allow', trans('label.schedule.allow'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-2">
        {!! Form::select('allow', $allow, null, ['class' => 'form-control']) !!}
        {!! $errors->first('allow', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('location') ? 'has-error' : ''}}">
    {!! Form::label('location', trans('label.schedule.location'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-2">
        {!! Form::text('location', old('label.schedule.location') , ['class' => 'form-control']) !!}
        {!! $errors->first('location', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('display') ? 'has-error' : ''}}">
    {!! Form::label('display', trans('label.schedule.display'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-2">
        {!! Form::select('display', $display, null, ['class' => 'form-control']) !!}
        {!! $errors->first('display', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('reminder') ? 'has-error' : ''}}">
    {!! Form::label('reminder', trans('label.schedule.reminder'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-3">
        {!! Form::select('reminder', $reminder, null, ['class' => 'form-control']) !!}
        {!! $errors->first('reminder', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('actived') ? 'has-error' : ''}}">
    {!! Form::label('actived', trans('label.schedule.status'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-2">
        {!! Form::select('actived', $actived, null, ['class' => 'form-control']) !!}
        {!! $errors->first('actived', '<p class="help-block">:message</p>') !!}
    </div>
</div>