@extends('layouts.admin_template')

@section('title', 'Schedule Course')
@section('page_title', 'Coach')
@section('page_description', 'learn1thing')

@section('header')
{!! Html::style('bower_components/fullcalendar/dist/fullcalendar.min.css') !!}
{!! Html::script('bower_components/moment/min/moment.min.js') !!}
{!! Html::script('bower_components/fullcalendar/dist/fullcalendar.min.js') !!}
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Coach</li>
        <li>Schedule</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">   
        <div class="col-md-3">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Coach</h3>
                </div>
                <div class="box-body box-profile">
                @if (!$photo)
                    <img class="profile-user-img img-responsive img-circle" src="https://s3.amazonaws.com/wll-community-production/images/no-avatar.png" alt="User profile picture">
                @else
                    <img class="profile-user-img img-responsive img-circle" src="{{ asset($photo) }}" alt="User profile picture">
                @endif
                    <h3 class="profile-username text-center">{{ $name }}</h3>
                    <p class="text-muted text-center">{{ $description }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="box box-widget widget-user-2">
                <div class="widget-user-header bg-primary" style="padding:1px 20px">
                    <h4>Coach Event</h4>
                </div>
                <div class="box-footer no-padding">
                    <ul class="nav nav-stacked">
                        <li>
                            <a href="#">{{ $name }}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        @include('partials.message')
        <div class="col-md-12">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Coach Calendar</h3>
                    <a class='btn btn-warning pull-right send-btn' href="{{ url('coach')}}">Save</a>
                </div>
                <div class="box-body" style="display: block;">
                    {!! $calendar->calendar() !!}
                    {!! $calendar->script() !!}
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('scripts')
<script type="text/javascript">
    $('div.alert').delay(3000).slideUp(300);
    window.document.onload = function(){
        // $(".calendar").hide();
    };
   
    $(document).ready(function(){
        // $(".calendar").hide();
        $('.send-btn').click(function(){
              
        $.ajax({
                url: 'store',
                type: "post",
                data: {
                    'name':$('#name').val(),
                    'description':$('#description').val(),
                    '_token': $('input[name=_token]').val()},
                success: function(data){
                    var coah_id = data.id;
                    $(".register").hide();
                    $(".calendar").show();
                }
            });      
        }); 
    });
</script>
@endsection