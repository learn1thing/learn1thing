@extends('layouts.admin_template')

@section('title', 'Dashboard')
@section('page_title', 'Dashboard')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Coach</li>
        <li>Course</li>
    </ol>
@endsection

@section('content')

<div class="row">    
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                @include('partials.message')
                <div class="col-sm-6">
                    <h1 class="box-title" style="margin-top:10px">List Course</h1>
                </div>
                <div class="col-sm-6 text-right">
                    <a href="{{route('coach.course.create')}}" class="btn btn-primary">
                        <i class="fa fa-plus margin-r-5"></i> {{trans('label.create')}}
                    </a>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    {!! $dataTable->table(['class' => 'datatable table table-striped', 'cellspacing'=>"0", 'width'=>"100%"]) !!}
                </div>
            </div>
        </div>
    </div>
</div>
@include('partials.delete-modal')

@endsection
@section('scripts')
{!! $dataTable->scripts() !!}
<script type="text/javascript">
    $('div.alert').delay(3000).slideUp(300);
</script>
@endsection