@extends('layouts.admin_template')

@section('title', 'Dashboard')
@section('page_title', 'Dashboard')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('coach.course.index') }}"> List Course</a></li>
        <li>Edit Course</li>
    </ol>
@endsection

@section('content')
    {!! Form::model($data, [
            'route'     =>['coach.course.update', $data->id],
            'method'    => 'PATCH',
            'class' =>  'form-horizontal',
            'id'    =>  'form-course',
        ]) !!}
        <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-edit"></i> Edit</h3>
            </div>
            <div class="box-body">
                @include('coach.course._form')
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <a href="{{ route('coach.course.index') }}" class="btn btn-default">
                            {{trans('label.cancel')}}
                        </a>
                        <button type="submit" class="btn btn-primary" id="btn-submit">{{trans('label.save')}}</button>
                    </div>
                </div>              
            </div>
        </div>
    {!! Form::close() !!}
@endsection
@section('scripts')
@endsection
