@extends('layouts.admin_template')

@section('title', 'Coach Lists')
@section('page_title', 'Coach Lists')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-user m-right-10"></i> Coach Lists</li>
    </ol>
@endsection

@section('content')

<div class="row">    
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                <div class="table-responsive">
                    {!! $dataTable->table(['class' => 'datatable table table-striped', 'cellspacing'=>"0", 'width'=>"100%"]) !!}
                </div>
            </div>
        </div>
    </div>
</div>
@include('partials.delete-modal')

@endsection
@section('scripts')
{!! $dataTable->scripts() !!}
@endsection