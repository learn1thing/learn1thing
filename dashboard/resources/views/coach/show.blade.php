@extends('layouts.admin_template')

@section('title', 'Detail Coach')
@section('page_title', 'Detail Coach')
@section('page_description', 'learn1thing')

@section('header')
{!! Html::style('bower_components/fullcalendar/dist/fullcalendar.min.css') !!}
{!! Html::script('bower_components/moment/min/moment.min.js') !!}
{!! Html::script('bower_components/fullcalendar/dist/fullcalendar.min.js') !!}
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-mortar-board m-right-10"></i> Manage Coach schedule</li>
        <li>Schedule Coach</li>
        <li>Detail</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">   
        <div class="col-md-3">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Coach</h3>
                </div>
                <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="{{ url_to_image($photo) }}" alt="User profile picture">
                    <h3 class="profile-username text-center">{{ $name }}</h3>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="box box-widget widget-user-2">
                <div class="widget-user-header bg-primary" style="padding:1px 20px">
                    <h4>Coach Event</h4>
                </div>
                <div class="box-footer no-padding">
                    <ul class="nav nav-stacked">
                        <li>
                            <a href="#">{!! $description !!}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">   
        <div class="col-md-12">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Calendar for Business Partner’s Admin and Coaches </h3>
                </div>
                <div class="box-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <span class="label label-default" style="background-color: #3a87ad; color:#fff">
                                Available
                            </span> &nbsp : The schedules that are available to choose.
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <span class="label label-default" style="background-color: #ff851b; color:#fff"">
                                Available for Booking
                            </span> &nbsp : The schedules already chosen by the admin/coaches, appear in the Customer’s Calendar.
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <span class="label label-default" style="background-color: #474f54; color:#fff"">
                                Passed
                            </span> &nbsp : the expired schedules, cannot be chosen.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
    <div class="col-xs-12">
        @include('partials.message')
        <div class="col-md-12">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Coach Calendar</h3>
                </div>
                <div class="box-body" style="display: block;">
                    {!! $calendar->calendar() !!}
                    {!! $calendar->script() !!}
                </div>
            </div>
        </div>
        <div class="col-md-12" style="text-align:right;">
            <a class='btn btn-default send-btn' href="{{ route('coach.schedule.course.index') }}">
                <i class="fa fa-angle-double-left m-right-10"></i> Back
            </a>
        </div>
    </div>
</div>


@endsection
@section('scripts')
<script type="text/javascript">
    $('div.alert').delay(3000).slideUp(300);
    window.document.onload = function(){
        // $(".calendar").hide();
    };
   
    $(document).ready(function(){
        // $(".calendar").hide();
        $('.send-btn').click(function(){
              
        $.ajax({
                url: 'store',
                type: "post",
                data: {
                    'name':$('#name').val(),
                    'description':$('#description').val(),
                    '_token': $('input[name=_token]').val()},
                success: function(data){
                    var coah_id = data.id;
                    $(".register").hide();
                    $(".calendar").show();
                }
            });      
        }); 
    });
</script>
@endsection