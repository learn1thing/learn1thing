@extends('layouts.admin_template')

@section('title', 'Add Coach')
@section('page_title', 'Add Coach')
@section('page_description', 'learn1thing')

@section('header')
{!! Html::style('bower_components/fullcalendar/dist/fullcalendar.min.css') !!}
{!! Html::script('bower_components/moment/min/moment.min.js') !!}
{!! Html::script('bower_components/fullcalendar/dist/fullcalendar.min.js') !!}
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-mortar-board m-right-10"></i> Manage Coach schedule</li>
        <li>Schedule Coach</li>
        <li>Add</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            {!! Form::open(array('route' => 'coach.schedule.course.store','method'=>'POST', 'id'=>'myform', 'class'=>'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
                <div class="box-body">
                    <div class="form-group {{ $errors->has('coach') ? 'has-error' : ''}}" >
                        {!! Form::label('coach', 'Owner', ['class' => 'col-sm-2 control-label']) !!}
                        <div class="col-sm-8">
                            {{ Form::select('coach', $coaches, old('coach'), ['class' => 'form-control','id'=>'coach']) }}
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-sm-2 control-label">Name</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="name" placeholder="Name" name="name" value="{{ old('name') }}">

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="name" class="col-sm-2 control-label">E-mail</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="email" placeholder="E-Mail" name="email" value="{{ old('email') }}" readonly="readonly">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('course_id') ? 'has-error' : ''}}" >
                        {!! Form::label('course_id', 'Course (Coach Room)', ['class' => 'col-sm-2 control-label']) !!}
                        <div class="col-sm-8">
                            {{ Form::select('course_id', $courses, old('course_id'), ['class' => 'form-control','id'=>'course_id']) }}
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                        <label for="decription" class="col-sm-2 control-label">Description</label>

                        <div class="col-sm-8">
                            <textarea class="form-control textarea" id="description" name="description" rows="6" placeholder="Decription">{{ old('description') }}</textarea>

                            @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                     <div class="form-group">
                        <label class="col-sm-2 control-label">Image</label>
                        <div class="col-sm-8 upload-image">
                            <input type="file" id="upload-file" name="photo" value="{!! old('photo') !!}">

                            <img class="img-thumbnail" id="upload-thumbnail" src="https://s3.amazonaws.com/wll-community-production/images/no-avatar.png" height="250" width="250">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 m-top-10 text-right">
                            <a href="{{url('coach')}}" class="btn btn-default">
                               <i class="fa fa-times m-right-10"></i> Cancel
                            </a>
                            
                            <button type="submit" class="btn btn-primary form-submit-product">
                                <i class="fa fa-check m-right-10"></i> Save
                            </button>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script type="text/javascript">
    $('div.alert').delay(3000).slideUp(300);
</script>

<script type="text/javascript">
    $(function () {
        $(":file").change(function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
            }
        });
    });

    function imageIsLoaded(e) {
        $('#upload-thumbnail').attr('src', e.target.result);
    };

    $(document).on('change', 'select[name=coach]', function() {
        $.ajax({
            method: 'GET',
            url: "{{route('api.get.email')}}",
            data: {
                'user_id':$(this).val(),
            },
            dataType: 'json',
            success: function(data) {
                $('input[name=email]').val(data.data.email);
            }
        });
    });
</script>
@endsection