@extends('layouts.admin_template')

@section('title', 'Dashboard')
@section('page_title', 'Dashboard')
@section('page_description', 'learn1thing')

@section('header')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    </ol>
@endsection

@section('content')
@if(user_info()->inRole('super-admin') || user_info()->inRole('business-partner') || user_info()->inRole('corporate-customer') || user_info()->inRole('standard-customer'))
    <div class="row">
        @if(user_info()->inRole('standard-customer'))
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3 class="order-completed-count"><i class="fa fa-refresh fa-spin"></i></h3>
                        <p>Completed Orders</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3 class="order-pending-count"><i class="fa fa-refresh fa-spin"></i></h3>

                  <p>Pending Orders</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3 class="order-course-count"><i class="fa fa-refresh fa-spin"></i></h3>

                  <p>Courses Ordered</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3 class="order-profiler-count"><i class="fa fa-refresh fa-spin"></i></h3>

                  <p>Profilers Ordered</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
              </div>
            </div>
        @elseif(user_info()->inRole('corporate-customer'))
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3 class="order-completed-count"><i class="fa fa-refresh fa-spin"></i></h3>
                        <p>Completed Orders</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3 class="product-count"><i class="fa fa-refresh fa-spin"></i></h3>

                  <p>Total Products</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3 class="user-count"><i class="fa fa-refresh fa-spin"></i></h3>

                  <p>Total Users</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person"></i>
                </div>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3 class="department-count"><i class="fa fa-refresh fa-spin"></i></h3>

                  <p>Total Departments</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
              </div>
            </div>
        @elseif(user_info()->inRole('business-partner'))
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3 class="product-count"><i class="fa fa-refresh fa-spin"></i></h3>
                        <p>Total number of products</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3 class="product-sold-count"><i class="fa fa-refresh fa-spin"></i></h3>

                  <p>Total number of products sold</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3 class="customer-count"><i class="fa fa-refresh fa-spin"></i></h3>

                  <p>Total number of customers</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person"></i>
                </div>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3 class="money-count"><i class="fa fa-refresh fa-spin"></i></h3>

                  <p>Total Revenue</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
              </div>
            </div>
        @elseif(user_info()->inRole('super-admin'))
            <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3 class="order-history-count"><i class="fa fa-refresh fa-spin"></i></h3>

              <p>{{(user_info()->inRole('business-partner') || user_info()->hasAccess('all')) ? 'New Orders' : 'Orders'}}</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="{{route('admin.order.history')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        {{-- @if(user_info()->hasAccess('all')) --}}
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3 class="business-partner"><i class="fa fa-refresh fa-spin"></i></h3>

                  <p>Total number of Business Partners</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="{{route('admin.business-partner.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3 class="corporate-customer"><i class="fa fa-refresh fa-spin"></i></h3>

                  <p>Total number of Corporate Customers</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="{{route('admin.corporate-customer.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3 class="standard-customer"><i class="fa fa-refresh fa-spin"></i></h3>

                  <p>Total number of Standard Customers</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="{{route('admin.standard-customer.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
        @endif
    </div>
@endif

 <div class="row">
    <div class="col-xs-6 col-sm-6">
        <div class="box">
            <div class="box-body">
                <div id="container" style="width:100%; height:400px;"></div>
            </div>
        </div>
    </div>

    <div class="col-xs-6 col-sm-6">
        <div class="box">
            <div class="box-body">
                <div id="container1" style="width:100%; height:400px;"></div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    {!! Html::script('bower_components/highcharts/highcharts.js') !!}
    {!! Html::script('bower_components/highcharts/modules/exporting.js') !!}

    <script>
        $(function () { 


            var role = "{{ user_info('role')->slug }}";
            if(role == "business-partner"){
                Highcharts.chart('container', {
                chart: {
                    type: 'line'
                },
                title: {
                    text: 'Total of Daily Income'
                },
                xAxis: {
                    categories: []
                },
                yAxis: {
                    title: {
                        text: 'Amount ( USD )'
                    }
                },
                series: [{
                    name: 'USD',
                    data: []
                }]
            });
            

            Highcharts.chart('container1', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Total of Sales per Category'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.y}',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Percentage',
                    colorByPoint: true,
                    data: []
                }]
            });
            } else if(role == "super-admin"){
                Highcharts.chart('container', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Monthly total revenue'
                },
                xAxis: {
                    categories: []
                },
                yAxis: {
                    title: {
                        text: 'Amount ( USD )'
                    }
                },
                series: [{
                    name: 'Total',
                    data: []
                }]
            });

            Highcharts.chart('container1', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Total revenue by category'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.y}',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Percentage',
                    colorByPoint: true,
                    data: []
                }]
            });

            } else if(role == "corporate-customer"){
                Highcharts.chart('container1', {
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Purchase per Category'
                    },
                    xAxis: {
                        categories: []
                    },
                    yAxis: {
                        title: {
                            text: 'Amount ( USD )'
                        }
                    },
                    series: [{
                        name: 'Total',
                        data: []
                    }]
                });

                Highcharts.chart('container', {
                    chart: {
                        type: 'line'
                    },
                    title: {
                        text: 'Monthly Purchase'
                    },
                    xAxis: {
                        categories: []
                    },
                    yAxis: {
                        title: {
                            text: 'Amount ( USD )'
                        }
                    },
                    series: [{
                        name: 'Total',
                        data: []
                    }]
                });

                
            }
            

            
            $.ajax({
                url: window.location.href + '/analytic',
                method: 'get',
                dataType: 'json',
                success: function(data) {
                    if(role == "standard-customer") {
                        $('.order-completed-count').html(data.order_completed);
                        $('.order-pending-count').html(data.order_pending);
                        $('.order-course-count').html(data.order_course);
                        $('.order-profiler-count').html(data.order_profiler);
                    } else if(role == 'corporate-customer') {
                        $('.order-completed-count').html(data.order_completed);
                        $('.product-count').html(data.product_counts);
                        $('.user-count').html(data.user_counts);
                        $('.department-count').html(data.department_counts);

                        // total of purchase per category
                        Highcharts.chart('container1', {
                            chart: {
                                plotBackgroundColor: null,
                                plotBorderWidth: null,
                                plotShadow: false,
                                type: 'pie'
                            },
                            title: {
                                text: 'Purchase per Category'
                            },
                            tooltip: {
                                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                            },
                            plotOptions: {
                                pie: {
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    dataLabels: {
                                        enabled: true,
                                        format: '<b>{point.name}</b>: {point.y}',
                                        style: {
                                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                        }
                                    }
                                }
                            },
                            series: [{
                                name: 'Percentage',
                                colorByPoint: true,
                                data: data.product_buy_per_category
                            }]
                        });

                        Highcharts.chart('container', {
                            chart: {
                                type: 'line'
                            },
                            title: {
                                text: 'Monthly Purchase'
                            },
                            xAxis: {
                                categories: data.date_added
                            },
                            yAxis: {
                                title: {
                                    text: 'Amount ( USD )'
                                }
                            },
                            series: [{
                                name: 'Total',
                                data: data.purchase_monthly
                            }]
                        });
                    } else if(role == 'business-partner') {
                        $('.product-count').html(data.product_counts);
                        $('.product-sold-count').html(data.product_sold_counts);
                        $('.customer-count').html(data.customer_counts);
                        $('.money-count').html(data.total_money);

                        // monthly revenue
                        Highcharts.chart('container', {
                            chart: {
                                type: 'line'
                            },
                            title: {
                                text: 'Monthly Revenue'
                            },
                            xAxis: {
                                categories: data.time_income_daily
                            },
                            yAxis: {
                                title: {
                                    text: 'Amount ( USD )'
                                }
                            },
                            series: [{
                                name: 'USD',
                                data: data.income_daily
                            }]
                        });

                        // sales per category
                        Highcharts.chart('container1', {
                            chart: {
                                plotBackgroundColor: null,
                                plotBorderWidth: null,
                                plotShadow: false,
                                type: 'pie'
                            },
                            title: {
                                text: 'Total of Sales per Category'
                            },
                            tooltip: {
                                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                            },
                            plotOptions: {
                                pie: {
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    dataLabels: {
                                        enabled: true,
                                        format: '<b>{point.name}</b>: {point.y}',
                                        style: {
                                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                        }
                                    }
                                }
                            },
                            series: [{
                                name: 'Percentage',
                                colorByPoint: true,
                                data: data.sales_per_category
                            }]
                        });

                    } else if(role == 'super-admin') {
                        $('.order-history-count').html(data.order);
                        $('.business-partner').html(data.business);
                        $('.corporate-customer').html(data.corporates);
                        $('.standard-customer').html(data.standards);

                        // monthly total revenue
                        Highcharts.chart('container', {
                            chart: {
                                type: 'bar'
                            },
                            title: {
                                text: 'Monthly total revenue'
                            },
                            xAxis: {
                                categories: data.time_income_weekly
                            },
                            yAxis: {
                                title: {
                                    text: 'Amount ( USD )'
                                }
                            },
                            series: [{
                                name: 'Total',
                                data: data.income_weekly
                            }]
                        });

                        // get sales per category
                        Highcharts.chart('container1', {
                            chart: {
                                plotBackgroundColor: null,
                                plotBorderWidth: null,
                                plotShadow: false,
                                type: 'pie'
                            },
                            title: {
                                text: 'Total revenue by category'
                            },
                            tooltip: {
                                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                            },
                            plotOptions: {
                                pie: {
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    dataLabels: {
                                        enabled: true,
                                        format: '<b>{point.name}</b>: {point.y}',
                                        style: {
                                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                        }
                                    }
                                }
                            },
                            series: [{
                                name: 'Percentage',
                                colorByPoint: true,
                                data: data.sales_per_category
                            }]
                        });
                    }
                }
            });
        });
    </script>
@endsection
