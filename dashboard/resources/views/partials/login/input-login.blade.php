<form id="form-login" action="{{ route('post.login') }}" method="POST">
  {{ csrf_field() }}
  <div class="form-group has-feedback {{ !empty($errors) ? ($errors->has('email') ? 'has-error' : '') : ' ' }}">
    <input type="name" class="form-control" name="email" autocomplete="off" id="email" placeholder="Email" value="{{ old('email') }}">
    {!! !empty($errors) ? ( $errors->has('email') ? $errors->first('email', '<span class="help-block">:message</span>') : '') : '' !!}
    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
  </div>
  <div class="form-group has-feedback {{ !empty($errors) ? ($errors->has('password') ? 'has-error' : '') : ' ' }}">
    <input type="password" class="form-control" name="password" autocomplete="off" id="password" placeholder="Password">
    {!! !empty($errors) ? ( $errors->has('password') ? $errors->first('password', '<span class="help-block">:message</span>') : '') : '' !!}
    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
  </div>
  <div class="row">
    <div class="col-xs-8">
      <div class="checkbox icheck">
        <label>
          <input type="checkbox" class="remember-me" id="remember" value="1"> Remember Me
        </label>
      </div>
    </div>
    <!-- /.col -->
    <div class="col-xs-4">
      <button type="submit" class="btn btn-primary btn-block btn-flat" id="btn-login">Sign In</button>
    </div>
    <!-- /.col -->
  </div>
</form>
