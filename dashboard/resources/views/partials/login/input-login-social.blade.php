  <div class="login-box-msg">
    <img class="img-circle" src="{{$avatar}}" alt="User Avatar">
  </div>
  <div class="login-box-msg">
    <i class="fa fa-spinner fa-spin" aria-hidden="true"></i>
  </div>


<div style="display:none">
<form id="form-login" action="{{ route('post.login') }}" method="POST">
  {{ csrf_field() }}
  <div class="form-group has-feedback {{ !empty($errors) ? ($errors->has('email') ? 'has-error' : '') : ' ' }}">
    <input type="name" class="form-control" name="email" autocomplete="off" id="email" placeholder="Email" value="{{ empty(old('email')) ? (empty($email) ? '' : $email) : old('email') }}">
    <input type="hidden" class="form-control" name="login_type" autocomplete="off" id="login_type" placeholder="Email" value="{{@$login_type}}">
    
    {!! !empty($errors) ? ( $errors->has('email') ? $errors->first('email', '<span class="help-block">:message</span>') : '') : '' !!}
    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
  </div>
  <div class="form-group has-feedback {{ !empty($errors) ? ($errors->has('password') ? 'has-error' : '') : ' ' }}">
    <input type="password" class="form-control" name="password" autocomplete="off" id="password" placeholder="Password" value="{{ empty($password) ? '' : $password }}">
    {!! !empty($errors) ? ( $errors->has('password') ? $errors->first('password', '<span class="help-block">:message</span>') : '') : '' !!}
    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
  </div>
</form>
<div>


