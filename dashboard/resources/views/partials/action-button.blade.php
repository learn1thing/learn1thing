@if( isset( $show_url ) )
<a href="{!! empty( $show_url ) ? 'javascript:void(0)' : $show_url !!}" class="btn btn-success btn-xs {!! empty( $show_url ) ? 'disabled' : '' !!}" title="Show" data-button="show">
    <i class="fa fa-search fa-fw"></i>
</a>
@endif
@if (isset( $detail_url ))
    <a href="javascript:void(0)" id="detailData" class="btn btn-info detailData btn-xs {!! empty( $detail_url ) ? 'disabled' : '' !!}" title="Detail" data-href="{!! empty( $detail_url ) ? 'javascript:void(0)' : $detail_url !!}" data-button="Detail">
        <i class="fa fa-info fa-fw"></i>
    </a>
@endif
@if( isset( $edit_url ) )
<a href="{!! empty( $edit_url ) ? 'javascript:void(0)' : $edit_url !!}" class="btn btn-success btn-xs {!! empty( $edit_url ) ? 'disabled' : '' !!}" title="Edit" data-button="edit">
    <i class="fa fa-pencil-square-o fa-fw"></i>
</a>
@endif
@if( isset( $delete_url ) )
    <a href="javascript:void(0)" id="deleteData" class="btn btn-danger btn-xs {!! empty( $delete_url ) ? 'disabled' : '' !!}" title="Delete" data-href="{!! empty( $delete_url ) ? 'javascript:void(0)' : $delete_url !!}" data-button="delete">
        <i class="fa fa-trash-o fa-fw"></i>
    </a>
@endif
@if( isset( $status ) )
    <label class="switch">
        <input type="checkbox" {!! ($status == 1 || $status != null) ? 'checked' : '' !!} id="updateStatus" data-href="{!! $updateStatus_url !!}" data-button="{!! ($status != null)?$status:0 !!}">
        <div class="slider round" data-href="{!! $updateStatus_url !!}" data-button="{!! ($status != null)?$status:0 !!}"></div>
    </label>
@endif
@if( isset( $permission_url ) )
    <a href="{{$permission_url}}" class="btn btn-success permission-role btn-xs" title="Manage Permission">
        <i class="fa fa-exchange fa-fw"></i>
    </a>
@endif
@if( isset( $accept_url ) )
    @if (@!$user->completed)
        <a href="javascript:void(0)" id="acceptData" class="btn btn-success activation-user btn-xs {!! empty( $accept_url ) ? 'disabled' : '' !!}" title="Accept" data-href="{!! empty( $accept_url ) ? 'javascript:void(0)' : $accept_url !!}" data-button="Accept">
            <i class="fa fa-check-square fa-fw"></i>
        </a>
    @endif
@endif
@if (isset( $approve_url ))
    <a href="javascript:void(0)" id="acceptData" class="btn btn-success app-dec btn-xs {!! empty( $approve_url ) ? 'disabled' : '' !!}" title="Approve" data-href="{!! empty( $approve_url ) ? 'javascript:void(0)' : $approve_url !!}" data-button="Approve">
        <i class="fa fa-check-square fa-fw"></i>
    </a>
@endif
@if( isset( $decline_url ) )
    <a href="javascript:void(0)" id="declineData" class="btn btn-danger app-dec btn-xs {!! empty( $decline_url ) ? 'disabled' : '' !!}" title="Decline" data-href="{!! empty( $decline_url ) ? 'javascript:void(0)' : $decline_url !!}" data-button="Decline">
        <i class="fa fa-close fa-fw"></i>
    </a>
@endif
@if( isset( $reset ) )
    <a href="javascript:void(0)" id="resetData" class="btn btn-primary btn-xs {!! empty( $reset ) ? 'disabled' : '' !!}" title="Reset Password" data-href="{!! empty( $reset ) ? 'javascript:void(0)' : $reset !!}" data-button="Reset Password">
        <i class="fa fa-refresh fa-fw"></i>
    </a>
@endif
@if( isset( $custom_show ) )
<a href="{!! empty( $custom_show['show_url'] ) ? 'javascript:void(0)' : $custom_show['show_url'] !!}" class="btn btn-success btn-xs {!! empty( $custom_show['show_url'] ) ? 'disabled' : '' !!}" title="Show" data-button="show">
    <i class="fa fa-search fa-fw"></i> {!! empty( $custom_show['extra_caption'] ) ? "" : $custom_show['extra_caption'] !!}
</a>
@endif
@if( isset( $reset_url ) )
    <a href="javascript:void(0)" id="resetData" class="btn btn-danger btn-xs {{ empty( $reset_url ) ? 'disabled' : '' }}" title="Reset Scheduler" data-href="{{ empty( $reset_url ) ? 'javascript:void(0)' : $reset_url }}" data-button="reset">
        <i class="fa fa-refresh fa-fw"></i>
    </a>
@endif
@if( isset( $restart_url ) )
    <a href="javascript:void(0)" id="resetData" class="btn btn-danger btn-xs {{ empty( $restart_url ) ? 'disabled' : '' }}" title="Reset Alert" data-href="{{ empty( $restart_url ) ? 'javascript:void(0)' : $restart_url }}" data-button="reset">
        <i class="fa fa-refresh fa-fw"></i>
    </a>
@endif
@if( isset( $deactive_url ) )
    @if (@$user->completed)
        <a href="javascript:void(0)" id="deactiveData" class="btn btn-danger deactive-user btn-xs {!! empty( $deactive_url ) ? 'disabled' : '' !!}" title="Deactive" data-href="{!! empty( $deactive_url ) ? 'javascript:void(0)' : $deactive_url !!}" data-button="Deactive">
            <i class="fa fa-power-off fa-fw"></i>
        </a>
    @endif
@endif
@if( isset( $approved_url ) )
    @if (user_info()->hasAccess('all'))
        <a href="javascript:void(0)" id="approved" data-type="approved" class="btn btn-success approved_top_up btn-xs {!! empty( $approved_url ) ? 'disabled' : '' !!}" title="Approve" data-href="{!! empty( $approved_url ) ? 'javascript:void(0)' : $approved_url !!}" data-button="Approved" data-id="{!! $id !!}">
            <i class="fa fa-check"></i>
        </a>
    @endif
@endif
@if( isset( $rejected_url ) )
    @if (user_info()->hasAccess('all'))
        <a href="javascript:void(0)" id="rejected" data-type="rejected" class="btn btn-danger approved_top_up btn-xs {!! empty( $rejected_url ) ? 'disabled' : '' !!}" title="Reject" data-href="{!! empty( $rejected_url ) ? 'javascript:void(0)' : $rejected_url !!}" data-button="Rejected" data-id="{!! $id !!}">
            <i class="fa fa-close"></i>
        </a>
    @endif
@endif