@if(session()->has('message'))
    <div class="alert {!! session()->get('alert-class') !!} alert-dismissible ">
        {!! session()->get('message') !!}
    </div>
@endif
