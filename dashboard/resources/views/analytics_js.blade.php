<script type="text/javascript">
    $(function () { 
            var role = "{{ user_info('role')->slug }}";
            
        $.ajax({
        url: window.location.href + '/analytic',
        method: 'get',
        dataType: 'json',
        success: function(data) {

            //added by puw k5 5/12/17
            Highcharts.setOptions({
                lang:{
                    thousandsSep:','
                }
            });

            if(role == "standard-customer") {
                $('.order-completed-count').html(data.order_completed);
                $('.order-pending-count').html(data.order_pending);
                $('.order-course-count').html(data.order_course);
                $('.order-profiler-count').html(data.order_profiler);

                $('.expired-product').html(data.product_expired);
                $('.course-not-started').html(data.order_course);
                $('#product-bought').html(data.html_product_bought);
                $('#product-start').html(data.html_product_start);
                $('#product-rec').html(data.html_product_rec);
                $('.recomended-product').html(data.recomended);
                // Highcharts.chart('container-2', {
                //     title: {
                //         text: 'Monthly Purchase'
                //     },
                //     xAxis: {
                //         type: 'datetime'
                //     },
                //     plotOptions: {
                //         series: {
                //             pointStart: Date.UTC(2017, 3, 1),
                //             pointIntervalUnit: 'month'
                //         }
                //     },
                //     series: [{
                //             name: 'USD',
                //         data: [29.9, 71.5, 106.4, 100]
                //     }]
                // });

                Highcharts.chart('container9', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Purchase per Category'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y}',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                        name: 'Percentage',
                        colorByPoint: true,
                        data: data.product_buy_per_category
                    }]
                });
            } else if(role == 'corporate-customer') {
                $('.order-completed-count').html(data.order_completed);
                // $('.product-count').html(data.product_counts);
                $('.product-left').html(data.product_left);
                $('#product-left-assigned').html(data.html_product_left);
                $('#course-bought').html(data.html_course_bought);
                $('#requested-product').html(data.html_request);
                $('.payment-summary').html(data.payment_summary);
                // $('.user-count').html(data.user_counts);
                $('.product-expired').html(data.product_counts);

                // total of purchase per category
                Highcharts.chart('container9', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Purchase per Category'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y}',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                        name: 'Percentage',
                        colorByPoint: true,
                        data: data.product_buy_per_category
                    }]
                });

                Highcharts.chart('container-2', {
                    chart: {
                        type: 'line'
                    },
                    title: {
                        text: 'Monthly Purchase'
                    },
                    xAxis: {
                        categories: data.date_added
                    },
                    yAxis: {
                        title: {
                            text: 'Amount ( USD )'
                        }
                    },
                    series: [{
                        name: 'Total',
                        data: data.purchase_monthly
                    }]
                });
            } else if(role == 'business-partner') {
                $('.product-count-sold').html(data.product_counts);
                $('.product-sold-count').html(data.product_sold_counts);
                $('.customer-count').html(data.customer_counts);
                $('.money-count').html(data.total_money);
                // $('.action-required').html(data.count_list_action_required);
                $('.action-required').html(data.product_counts);
                $('#list-action-required').html(data.list_action_required);

                // monthly revenue
                Highcharts.chart('container-2', {
                    chart: {
                        type: 'line'
                    },
                    title: {
                        text: 'Monthly Revenue'
                    },
                    xAxis: {
                        categories: data.time_income_daily
                    },
                    yAxis: {
                        title: {
                            text: 'Amount ( USD )'
                        }
                    },
                    series: [{
                        name: 'USD',
                        data: data.income_daily
                    }]
                });

                // sales per category
                Highcharts.chart('container9', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Total Product'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y}',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                        name: 'Percentage',
                        colorByPoint: true,
                        data: data.sales_per_category
                    }]
                });

                //Consumer

                Highcharts.chart('container-3', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Customer'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y:.0f}',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                        name: 'Percentage',
                        colorByPoint: true,
                        data: data.customer_countries
                    }]
                });

            } else if(role == 'super-admin') {
                // $('.order-history-count').html(data.order);
                $('.product-count').html(data.order);
                $('.corporate-customer').html(data.corporates);
                $('.total-money').html(data.total_money);
                $('.customer-count').html(data.total_customer);
                $('.business-partner-count').html(data.total_business);

                // monthly total revenue
                Highcharts.chart('container9', {
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Monthly total revenue'
                    },
                    xAxis: {
                        categories: data.time_income_weekly
                    },
                    yAxis: {
                        title: {
                            text: 'Amount ( USD )'
                        }
                    },
                    series: [{
                        name: 'Total',
                        data: data.income_weekly
                    }]
                });

                // get sales per category
                Highcharts.chart('container-2', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },

                    title: {
                        text: 'Total revenue by category'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                //format: '<b>{point.name}</b>: {point.y}',
                                //edited by puw 5/12/17
                                format: '<b>{point.name}</b>: $ {point.y:,.0f}',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                        name: 'Percentage',
                        colorByPoint: true,
                        data: data.sales_per_category
                    }]
                });

                // Total consumer & Business Partner
                Highcharts.chart('container-3', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Total Standard Consumer & Corporate Customer'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: $ {point.y:,.0f}',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                        name: 'Percentage',
                        colorByPoint: true,
                        data: [{
                            name: 'Corporate Customer',
                            y: data.corporate_user
                        }, {
                            name: 'Standard Consumer',
                            y: data.consumer_user
                        }]
                    }]
                });

                // Total Purchase consumer & Business Partner
                Highcharts.chart('container-4', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Total Amount Standard & Corporate Customer ( USD )'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: $ {point.y:,.0f}',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                        name: 'Percentage',
                        colorByPoint: true,
                        data: [{
                            name: 'Corporate Customer',
                            y: data.total_purchase_corporate
                        }, {
                            name: 'Standard Consumer',
                            y: data.total_purchase_consumer
                        }]
                    }]
                });
            }
        }
    });
});

$(document).on('change','#category-customer',function() {
    $.ajax({
        url: window.location.href + '/analytic?type='+$(this).val(),
        method: 'get',
        dataType: 'json',
        beforeSend: function(data) {
            toastr.info('Please wait...');
        },
        success: function(data) {
            toastr.success('Success');  
            Highcharts.chart('container-3', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Customer'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y:.0f}',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                        name: 'Percentage',
                        colorByPoint: true,
                        data: data.data
                    }]
                });
        },
        error: function (data) {
            toastr.error('Something went wrong');
        }
    });
});

$(document).on('change','#productPurchase',function() {
    $.ajax({
        url: window.location.href + '/analytic?type='+$(this).val(),
        method: 'get',
        dataType: 'json',
        beforeSend: function(data) {
            toastr.info('Please wait...');
        },
        success: function(data) {
            toastr.success('Success');  
            Highcharts.chart('container9', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Total Product'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y}',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                        name: 'Percentage',
                        colorByPoint: true,
                        data: data.data
                    }]
                });
        },
        error: function (data) {
            toastr.error('Something went wrong');
        }
    });
});
</script>