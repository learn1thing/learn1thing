<input type="hidden" name="id" value="{{@$product_code}}">
<div class="form-group {{ $errors->has('product') ? 'has-error' : ''}}">
    {!! Form::label('product', trans('Product'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-7">
        @if(@$product_code)
            <input type="text" name="product" class="form-control" value="{{$product_name}}" disabled="true">
        @else
        <select class="form-control select2-product" name="product">
            @if(count($products) > 0)
                <option></option>
                @foreach($products as $product)
                    <option value="{{$product->product_code}}">{{$product->name.' - '.$product->model}}</option>
                @endforeach
            @endif
        </select>
        @endif
        {!! $errors->first('product', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('partner') ? 'has-error' : ''}}">
    {!! Form::label('name', trans('Business Partner'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-7">
        <select class="form-control select2-partner" multiple name="partner[]">
            @if(count($partners) > 0)
                <option></option>
                @foreach($partners as $partner)
                    <option value="{{$partner->id}}" {{(@$assigneds) ? (in_array($partner->id,@$assigneds) ? 'selected' : '') : ''}}>{{$partner->email.' - '.$partner->partner}}</option>
                @endforeach
            @endif
        </select>
        {!! $errors->first('partner', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@section('part_js')
<script type="text/javascript">
    $(document).ready(function() {
        $('.select2-partner').select2({
            placeholder: "Select Partner User"
        });

        $('.select2-product').select2({
            placeholder: "Select Product",
            allowClear: true
        });
    });
</script>
@endsection