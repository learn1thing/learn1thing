
@extends('layouts.admin_template')

@section('title', 'Add Assign Product To Partner')
@section('page_title', 'Add Assign Product To Partner')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.assign-product.index') }}"><i class="fa fa-reply m-right-10"></i> Assign To Partner</a></li>
        <li>Add</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
            {!! Form::open(['route' =>'admin.assign-product.store','class' =>  'form-horizontal','id' => 'form-assign-product']) !!}
            @include('admin.master.products.assign-product-partners._form',['products' => $products, 'partners' => $partners])
            <div class="row">
                <div class="col-xs-12 m-top-10 text-right">
                    <a href="{{route('admin.assign-product.index')}}" class="btn btn-default">
                       <i class="fa fa-times m-right-10"></i> Cancel
                    </a>
                
                    <button type="submit" class="btn btn-primary" id="btn-submit">
                        <i class="fa fa-check m-right-10"></i> Save
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
    
</div>
@endsection
@section('scripts')
{!! Html::script('vendor/jsvalidation/js/jsvalidation.min.js') !!}
{!! JsValidator::formRequest('App\Http\Requests\Master\AssignProductPartnertRequest', '#form-assign-product'); !!}
@endsection
