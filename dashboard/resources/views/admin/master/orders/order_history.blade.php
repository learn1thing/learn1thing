@extends('layouts.admin_template')

@section('title', 'Order History')
@section('page_title', 'Order History')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-opencart m-right-10"></i> Order</a></li>
        <li>Order History</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
              <div class="table-responsive">
                <table class="table table-striped" data-page-length="25">
                    <thead>
                        <tr>
                            <th>Order ID</th>
                            <th>Customer</th>
                            <th>Status</th>
                            <th>Total</th>
                            <th>Date Added</th>
                            <th>Date Modified</th>
                            <th width="5%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($orders) > 0)
                            @foreach($orders as $order)
                                <tr>
                                    <td>{{$order['info']->order_id}}</td>
                                    <td>{{$order['info']->firstname.' '.$order['info']->lastname}}</td>
                                    <td>{{$order['info']->histories[count($order['info']->histories) - 1]->status}}</td>
                                    <td>{{$order['total_order']}}</td>
                                    <td>{{date('d M Y',strtotime($order['info']->date_added))}}</td>
                                    <td>{{date('d M Y',strtotime($order['info']->date_modified))}}</td>
                                    <td align="center"><a href="{{route('admin.order.history.detail',['id' => $order['info']->order_id])}}" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a></td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.table').DataTable({
                "order": [[ 0, "desc" ]],
                // "columnDefs": [ {
                //     "targets": [4,5],
                //     "orderable": false
                // } ]
            });
        });
    </script>
@endsection
