@extends('layouts.admin_template')

@section('title', 'Detail Order History')
@section('page_title', 'Detail Order History')
@section('page_description', 'learn1thing')

@section('header')
    <!-- iCheck -->
    {!! Html::style('bower_components/AdminLTE/plugins/iCheck/square/blue.css') !!}
    <style type="text/css">
        .nav li span.li-text {
            position: relative;
            display: block;
            padding: 10px 15px;
        }
    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Order</li>
        <li><a href="{{route('admin.order.history')}}">Order History</a></li>
        <li>Detail Order History</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-sm-4">
        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-shopping-cart"></i>

              <h3 class="box-title">Order Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <ul class="nav nav-stacked">
                    <li><span class="li-text">{{$data->store_name}}</span></li>
                    <li><span class="li-text">{{date('d/m/Y',strtotime($data->date_added))}}</span></li>
                    <li><span class="li-text">{{$data->payment_method}}</span></li>
                </ul>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
    <div class="col-sm-4">
        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-user"></i>

              <h3 class="box-title">Customer Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <ul class="nav nav-stacked">
                    <li><span class="li-text">{{$data->firstname.' '.$data->lastname}}</span></li>
                    <li><span class="li-text">{{$data->email}}</span></li>
                    <li><span class="li-text">{{$data->telephone}}</span></li>
                </ul>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
    <div class="col-sm-4">
        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-gear"></i>

              <h3 class="box-title">Options</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <ul class="nav nav-stacked">
                    <li><span class="li-text">Invoice<span class="pull-right ">{{$data->invoice_prefix.$data->invoice_no}}</span></span></li>
                    <li><span class="li-text">Affiliate<span class="pull-right ">{{(user_info()->inRole('business-partner')) ? $data->total_price_for_BP : $data->totals[count($data->totals) - 1]->text}}</span></span></li>
                </ul>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>

<!-- Order -->
<div class="row">
    <div class="col-sm-12">
        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">Order (#{{$data->order_id}})</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <td><b>Payment Address</b></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                {{$data->payment_firstname.' '.$data->payment_lastname}}<br>
                                {{$data->payment_city}}<br>
                                {{$data->payment_address_1}}<br>
                                {{$data->payment_zone}}<br>
                                {{$data->payment_country}}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <table class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <td><b>Product</b></td>
                            <td width="15%"><b>Model</b></td>
                            <td width="10%"><b>Quantity</b></td>
                            <td width="15%"><b>Unit Price</b></td>
                            <td width="15%"><b>Total</b></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $product_ids = []; ?>
                        @if(count($products) > 0)
                            @foreach($products as $key => $product)
                                <?php $product_ids[$key] = $product->product_id; ?>
                                <tr>
                                    <td>{{$product->name}}<br>
                                        @if(($data->payment_code == 'bank_transfer' && ($data->order_status == 'Completed' || $data->order_status == 'Complete')) || ($data->order_status == 'Completed' || $data->order_status == 'Complete'))
                                            @if(!empty($product->profiler_url)&&!user_info()->inRole('standard-customer'))
                                                Link Profiler: <a href="{{$product->profiler_url}}" target="_blank">{{$product->profiler_url}}</a><br>
                                            @else

                                                @if(count($product->course_url) > 0)
                                                    @if(count($product->bbb_link) > 0) 
                                                        
                                                    @else
                                                    Link : <br>
                                                        @foreach($product->course_url as $course_url)
                                                            <a href="{{$course_url}}" target="_blank">{{$course_url}}</a><br>
                                                        @endforeach
                                                    @endif 
                                                @endif

                                            @endif

                                            @if(count($product->bbb_link) > 0) 
                                                Link to the Virtual Coaching Room : <br>
                                                @foreach($product->bbb_link as $bbb_link)
                                                    <a href="{{$bbb_link}}" target="_blank">{{$bbb_link}}</a><br>
                                                @endforeach
                                            @endif
                                            <br>
                                            @if( !empty(@$product->schedules) && count(@$product->schedules) > 0 )
                                                The coaching schedule has been set at <br>
                                                    <b>{{date('l, d F Y',strtotime($product->schedules[0]->start_date)).' ('.$product->schedules[0]->start_time}}
                                                    -
{{--                                                     {{date('l, d F Y',strtotime($product->schedules[count($product->schedules)-1]->end_date)).' '.$product->schedules[count($product->schedules)-1]->end_time}} --}}
                                                        {{$product->schedules[count($product->schedules)-1]->end_time.')'}}
                                                    </b></br>
                                                You can access the coaching room at time mentioned
                                            @endif

                                            @if ($product->can_edit)
                                                </br>
                                                <a href="{{ route('coach.schedule.order.edit',[$product->course_id,$product->coach_id]).'?order_id='.$data->order_id.'&product_id='.$product->product_id }}" class="btn btn-success btn-xs " title="Edit Schedule" data-button="edit">
                                                <i class="fa fa-pencil-square-o fa-fw"></i> Edit Schedule
                                                </a>
                                            @endif

                                        @endif
                                    </td>
                                    <td>{{$product->model}}</td>
                                    <td align="right">{{$product->quantity}}</td>
                                    <td align="right">{{$product->price_formated}}</td>
                                    <td align="right">{{$product->total_formated}}</td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                    <tfoot>
                        @if(user_info()->inRole('business-partner'))
                            <tr>
                                <td colspan="4" align="right">Sub-Total</td>
                                <td align="right">{{$data->sub_total_price_for_BP}}</td>
                            </tr>
                            <tr>
                                <td colspan="4" align="right">Flat Shipping Rate</td>
                                <td align="right">{{$data->total_tax_for_BP}}</td>
                            </tr>
                            <tr>
                                <td colspan="4" align="right">Total</td>
                                <td align="right">{{$data->total_price_for_BP}}</td>
                            </tr>
                        @else
                            @if(count($data->totals) > 0)
                                @foreach($data->totals as $total)
                                    <tr>
                                        <td colspan="4" align="right">{{$total->title}}</td>
                                        <td align="right">{{$total->text}}</td>
                                    </tr>
                                @endforeach
                            @endif
                        @endif
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div> <!-- /.Order -->

<!-- Order History -->
<div class="row">
    <div class="col-sm-12">
        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-comment-o"></i>

              <h3 class="box-title">Order History</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="#tab_1" data-toggle="tab">History</a></li>
                      <li><a href="#tab_2" data-toggle="tab">Additional</a></li>
                    </ul>
                    <div class="tab-content">
                      <div class="tab-pane active" id="tab_1">
                        <table class="table table-bordered" width="100%" id="order-history" data-page-length='5' data-filter="false" data-length-change="false" data-ordering="false">
                            <thead>
                                <tr>
                                    <th>Date Added</th>
                                    <th>Comment</th>
                                    <th>Status</th>
                                    <th>Customer Notified</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($data->histories))
                                    @foreach($data->histories as $history)
                                        <tr>
                                            <td>{{$history->date_added}}</td>
                                            <td>{!! $history->comment !!}</td>
                                            <td>{{$history->status}}</td>
                                            <td>{{$history->notify}}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                        @if(user_info()->inRole('super-admin') || user_info()->inRole('business-partner'))
                            <br>
                            <h4>Add Order History</h4>
                            <form id="add-history-form">
                                <input type="hidden" name="order_id" value="{{$data->order_id}}">
                                <input type="hidden" name="product_ids" value="{{json_encode($product_ids)}}">
                                <table class="table" width="100%">
                                    <tbody>
                                        <tr>
                                            <td width="20%" align="right"><b>Order Status</b></td>
                                            <td>
                                                <select name="order_status_id" id="input-order-status" class="form-control">
                                                    <option value="7">Canceled</option>
                                                    <option value="9">Canceled Reversal</option>
                                                    <option value="13">Chargeback</option>
                                                    <option value="5" selected="selected">Complete</option>
                                                    <option value="8">Denied</option>
                                                    <option value="14">Expired</option>
                                                    <option value="10">Failed</option>
                                                    <option value="1">Pending</option>
                                                    <option value="15">Processed</option>
                                                    <option value="2">Processing</option>
                                                    <option value="11">Refunded</option>
                                                    <option value="12">Reversed</option>
                                                    <option value="3">Shipped</option>
                                                    <option value="16">Voided</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right"><b>Notify Customer</b></td>
                                            <td>
                                                <div class="checkbox icheck">
                                                    <label>
                                                        <input type="checkbox" name="notify">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right"><b>Comment</b></td>
                                            <td>
                                                <textarea class="form-control" rows="5" name="comment"></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="right"><button type="submit" class="btn btn-primary add-history"><i class="fa fa-check m-right-10"></i>Add History</button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </form>
                        @endif
                      </div>
                      <!-- /.tab-pane -->
                      <div class="tab-pane" id="tab_2">
                        <table class="table table-bordered" width="100%">
                            <thead>
                                <tr>
                                    <th colspan="2">Browser</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>IP Address</td>
                                    <td>{{$data->ip}}</td>
                                </tr>
                                <tr>
                                    <td>User Agent</td>
                                    <td>{{$data->user_agent}}</td>
                                </tr>
                                <tr>
                                    <td>Accept Language</td>
                                    <td>{{$data->accept_language}}</td>
                                </tr>
                            </tbody>
                        </table>
                      </div>
                      <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div> <!-- /.Order History -->
@endsection
@section('scripts')
    <!-- iCheck -->
    {!! Html::script('bower_components/AdminLTE/plugins/iCheck/icheck.min.js') !!}
    <script type="text/javascript">
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        $(document).ready(function(){
            $('#order-history').DataTable();
        });

        $(document).on('submit','#add-history-form',function(e){
            e.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                url: "{{route('admin.order.history.add')}}",
                type: "post",
                dataType: "json",
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                beforeSend: function(data) {
                    $('button.add-history').attr('disabled','disabled');
                },
                success: function(data) {
                    if (data.success) {
                        setTimeout(function(){ window.location.reload(); }, 1000);
                    }
                    toastr.success(data.message);
                }
            });
        });
    </script>
@endsection
