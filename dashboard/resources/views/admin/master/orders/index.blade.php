@extends('layouts.admin_template')

@section('title', 'Order List')
@section('page_title', 'Order List')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-opencart m-right-10"></i> Order</a></li>
        <li><a href="{{ route('admin.order.index') }}">Order List</a></li>
    </ol>
@endsection

@section('content')

<div class="row">
    @if(user_info()->inRole('corporate-customer'))
      <div class="col-xs-3 pull-right">
          <a href="{{ route('admin.order.bulk.create')}}" class="btn btn-primary pull-right" id="btn-submit">
             <i class="fa fa-plus m-right-10"></i> Add Bulk Order
          </a>
      </div>
    @endif
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
              <div class="table-responsive">
                 {!! $dataTable->table(['class' => 'datatable table table-striped', 'cellspacing'=>"0", 'width'=>"100%"]) !!}
              </div>
            </div>
        </div>
    </div>
</div>
@include('partials.delete-modal')
@endsection
@section('scripts')
{!! $dataTable->scripts() !!}
<script type="text/javascript">
  $(document).on('click','.app-dec',function(){
    $.ajax({
      url: $(this).data('href'),
      method: 'patch',
      dataType: 'json',
      beforeSend: function(data) {
        toastAlert('Please wait, system is running background process!','warning');
        $('.datatable a').addClass('processing');
        $('.datatable a').removeAttr('id');
      },
      success:function(data) {
        switch(data.type){
          case 'info':
          toastr.info(data.message);
          break;
          case 'warning':
          toastr.warning(data.message);
          break;
          case 'success':
          toastr.success(data.message);
          break;
          case 'error':
          toastr.error(data.message);
          break;
        }

        $('.datatable').DataTable().ajax.reload();
      },
      error: function(data) {
        toastr.error('Something went wrong');
      }
    });
  });

  $(document).on('click', '.detailData', function () {
      var table = $('.datatable').DataTable();
      var tr = $(this).closest('tr');
      var row = table.row( tr );
      
      if ( row.child.isShown() ) {
          // This row is already open - close it
          row.child.hide();
          tr.removeClass('shown');
      }
      else {
          // Open this row
          $.ajax({
              url: $(this).data('href'),
              method: "get",
              dataType: "json",
              success: function(data) {
                  if (data.success) {
                      row.child( childCategoryTemplate(data.data) ).show();
                      tr.addClass('shown');
                  }
              }
          });
      }
  });

  function childCategoryTemplate ( d ) {
      var totalPrice = 0;
      var totalQuantity = 0;
      var icon = '<i class="fa fa-usd"></i> ';
      //if (d[0].payment_method.toLowerCase() == 'credit based system') {
      //  icon = '<i class="fa fa-money"></i> ';
      //}
      var table = '<table class="table table-bordered">'+
          '<thead>'+
              '<tr>'+
                  '<td><label>Product Name</label></td>'+
                  '<td><label>Model</label></td>'+
                  '<td><label>Status</label></td>'+
                  '<td><label>Price</label></td>'+
                  '<td><label>Quantity</label></td>'+
                  '<td><label>Total Price</label></td>'+
              '</tr>'+
          '</thead>'+
          '<tbody>';
          d.forEach(function(val){
              var price = val.price;
              if (val.payment_method.toLowerCase() == 'credit based system') {
                //price = val.credit;
              }

              totalPrice += parseInt(price*val.quantity);
              totalQuantity += parseInt(val.quantity);
              
	      var status = 'Pending';
              if (val.status == 2) {
                status = 'Completed';
              } else if (val.status == 3) {
                status = 'Rejected';
              }
              table += '<tr>'+
                      '<td>'+val.name+'</td>'+
                      '<td>'+val.model+'</td>'+
                      '<td>'+status+'</td>'+
                      '<td align="right">'+icon+price.toFixed(2)+'</td>'+
                      '<td align="right">'+val.quantity+'</td>'+
                      '<td align="right">'+icon+(price*val.quantity).toFixed(2)+'</td>'+
                  '</tr>';
          });
              
          table += '<tfoot>'
              +'<tr>'
                +'<td align="right" colspan=4><label>Total</label></td>'
                +'<td align="right"><label>'+totalQuantity.toFixed(2)+'</label></td>'
                +'<td align="right"><label>'+icon+totalPrice.toFixed(2)+'</label></td>'
              +'</tr>'
            +'</tfoot>'
            '</tbody>'+
          '</table>';
      return table;
  }
</script>
@endsection
