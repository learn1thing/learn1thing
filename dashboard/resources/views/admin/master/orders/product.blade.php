@extends('layouts.admin_template')

@section('title', 'Product')
@section('page_title', 'Product')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Product List</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
              <div class="table-responsive">
                {!! $dataTable->table(['class' => 'datatable table table-striped', 'cellspacing'=>"0", 'width'=>"100%"]) !!}
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
{!! $dataTable->scripts() !!}
<script type="text/javascript">
    $(document).on('click','.send-product',function(){
        var orderDetailId = $(this).data('order-detail-id');
        var productId = $(this).data('product-id');
        var email = $(this).parent().prev().children('select').val();
        if (email == '') {
            toastr.warning('Please select user first');
            return false;
        } else {
            toastr.warning('Please wait, system is running background process!');
            $.ajax({
                method: 'post',
                url: "{{route('admin.order.send-product-order')}}",
                data: {'order_detail_id':orderDetailId, 'product_id':productId, 'email': email},
                dataType: 'json',
                success: function(data) {
                    if (data.status.success) {
                        toastr.success(data.status.message);
                    } else {
                        toastr.error(data.status.message);
                    }

                    $('.datatable').DataTable().ajax.reload();
                }
            });
        }
    });
</script>
@endsection
