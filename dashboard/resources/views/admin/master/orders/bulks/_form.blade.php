<div style="margin-top: 20px;"></div>
<div id="bulk-order">
    @if(@$order->id)
        @foreach($orderDetails as $key => $detail)
            <div class="row">
                <div class="col-sm-5 products">
                    <div class="form-group {{ $errors->has('product_code') ? 'has-error' : ''}}">
                        {!! Form::select('product_code[0]['.$detail->id.']', [''=>'Select product'] + $products, $detail->product_code, ['class' => 'form-control select2']) !!}
                        {!! $errors->first('product_code', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-sm-5 quantitys">
                    <div class="form-group {{ $errors->has('quantity') ? 'has-error' : ''}}">
                        {!! Form::label('quantity', trans('Quantity'), ['class' => 'control-label','style' => 'display:none']) !!}
                        {!! Form::text('quantity[0]['.$detail->id.']', $detail->quantity , ['class' => 'form-control']) !!}
                        {!! $errors->first('quantity', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-sm-2 text-left">
                    @if($key == 0)
                        <button class="btn btn-primary add-order" type="button"><i class="fa fa-plus"></i></button>
                    @endif
                </div>
            </div>
        @endforeach
    @else
        <div class="row">
            <div class="col-sm-3 products">
                <div class="form-group {{ $errors->has('business_partner') ? 'has-error' : ''}}">
                    {!! Form::select('business_partner[0]', [''=>'Select business partner'] + $businessPartners, old('id'), ['class' => 'form-control select2 business-partner', 'data-no' => 0]) !!}
                    {!! $errors->first('business_partner', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="col-sm-3 products">
                <div class="form-group {{ $errors->has('product_code') ? 'has-error' : ''}}">
                    {!! Form::select('product_code[0]', [''=>'Select product'] + $products, old('product_code'), ['class' => 'form-control select2 product-list-0', 'data-no' => 0]) !!}
                    {!! $errors->first('product_code', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="col-sm-3 quantitys">
                <div class="form-group {{ $errors->has('quantity') ? 'has-error' : ''}}">
                    {!! Form::label('quantity', trans('Quantity'), ['class' => 'control-label','style' => 'display:none']) !!}
                    {!! Form::text('quantity[0]', old('quantity') , ['class' => 'form-control quantity-0']) !!}
                    {!! $errors->first('quantity', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="col-sm-3">
                <button class="btn btn-info add-order" type="button"><i class="fa fa-plus"></i></button>
            </div>
        </div>
    @endif
</div>

@section('part_js')
<script type="text/javascript">
    $(document).on('click','#btn-submit',function(e){
        e.preventDefault();
        $('form button').attr('disabled',true);
        $('form a').attr('disabled',true);
        $(this).html('Please wait...');

        $('#form-bulk-order').submit();

        if ($('form .has-error').length > 0) {
            $('form button').removeAttr('disabled');
            $('form a').removeAttr('disabled');
            $(this).html('Save');
        } else {
            toastr.warning('Please wait, system is running background process!');
        }
    });

    $(document).on('change','.business-partner',function(){
        var no = $(this).data('no');
        $.ajax({
            method: 'GET',
            url: "{{route('admin.order.detail-product')}}",
            data: {
                'user_id':$(this).val(),
            },
            dataType: 'json',
            success: function(data) {
                $('.quantity-'+no).val('');
                $('.product-list-'+no).html('<option value="">Select product</option>');
                if (data.result) {
                    data.data.forEach(function(value) {
                        $('.product-list-'+no).append('<option value="'+value.product_code+'">'+value.name+'</option>');
                    });
                }
            }
        });
    });
</script>
@endsection