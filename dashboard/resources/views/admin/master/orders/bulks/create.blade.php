
@extends('layouts.admin_template')

@section('title', 'Bulk Orders')
@section('page_title', 'Bulk Orders')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-opencart m-right-10"></i> Order</a></li>
        <li><a href="{{ route('admin.order.index') }}">Order List</a></li>
        <li>Bulk Orders</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
            {!! Form::open(['route' =>'admin.order.bulk.store','class' =>  '','id' => 'form-bulk-order']) !!}
                @include('admin.master.orders.bulks._form')
                <div class="row">
                    <div class="col-xs-12 m-top-10 text-right">
                        <a href="{{route('admin.order.index')}}" class="btn btn-default">
                           <i class="fa fa-times m-right-10"></i> Cancel
                        </a>
                    
                        <button type="submit" class="btn btn-primary" id="btn-submit">
                            <i class="fa fa-check m-right-10"></i> Save
                        </button>
                    </div>
                </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
    
</div>
@endsection
@section('scripts')
{!! Html::script('vendor/jsvalidation/js/jsvalidation.min.js') !!}
{!! JsValidator::formRequest('App\Http\Requests\Order\BulkOrderRequest', '#form-bulk-order'); !!}
<script type="text/javascript">
    $('.select2').select2();
    // var max_fields            = 5; //maximum input box allowed
    var add_bulk      = $('.add-order'); 

    // add more for saprotan .start here
    var x = 0; //initlal text box count
    var y = 0; //initlal text box count
    add_bulk.click(function(e) { //on add input button click
        e.preventDefault();
        // if(x < max_fields) { //max input box allowed
            x++; //text box increment
            y++; //text box increment
            $("#bulk-order").append(''
                +'<div class="row">'
                    +'<div class="col-sm-3">'
                        +"<div class=\"form-group {{ $errors->has('business_partner') ? 'has-error' : ''}}\">"
                            +'<select class=\"select2 form-control business-partner" data-no='+y+' name="business_partner['+y+']\">'
                                +'<option value="">Select business partner</option>'
                                @foreach($businessPartners as $key => $bp)
                                    +'<option value="{{$key}}">{{$bp}}</option>'
                                @endforeach
                            +'</select>'
                            +"{!! $errors->first('business_partner', '<p class=\"help-block\">:message</p>') !!}"
                        +'</div>'
                    +'</div>'
                    +'<div class="col-sm-3 products">'
                        +'<div class=\"form-group {{ $errors->has('product_code') ? 'has-error' : ''}}\">'
                            +'<select class="select2 form-control product-list-'+y+'" data-no='+y+' name="product_code['+y+']">'
                                +'<option value="">Select product</option>'
                                @foreach($products as $key => $product)
                                    +'<option value="{{$key}}">{{$product}}</option>'
                                @endforeach
                            +'</select>'
                            +"{!! $errors->first('product_code', '<p class=\"help-block\">:message</p>') !!}"
                        +'</div>'
                    +'</div>'
                    +'<div class="col-sm-3 quantitys">'
                        +"<div class=\"form-group {{ $errors->has('quantity') ? 'has-error' : ''}}\">"
                            +'<label class="contro-label" style="display:none">Quantity</label>'
                            +'<input class="form-control quantity-'+y+'" name="quantity['+y+']" placeholder="Quantity">'
                            +"{!! $errors->first('quantity', '<p class=\"help-block\">:message</p>') !!}"
                        +'</div>'
                    +'</div>'
                    +'<div class="col-sm-3">'
                        +'<button class="btn btn-danger remove" type="button"><i class="fa fa-trash"></i></button>'
                    +'</div>'
                +'</div>'); //add 
            $('.select2').select2();
        // } else
        // if(x = max_fields) {
        //     toastr.warning('Maximum 5 allowed');
        // }
        $('.select2').select2();
    });
    
    $("#bulk-order").on("click",".remove", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').parent('div').remove(); x--;
    });
</script>
@endsection
