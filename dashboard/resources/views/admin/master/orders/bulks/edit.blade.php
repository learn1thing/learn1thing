
@extends('layouts.admin_template')

@section('title', 'Edit Bulk Orders')
@section('page_title', 'Edit Bulk Orders')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-opencart m-right-10"></i> Order</a></li>
        <li><a href="{{ route('admin.order.index') }}">Order List</a></li>
        <li>Edit Bulk Orders</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
            {!! Form::open(['route' =>['admin.order.bulk.update',$order->id],'class' =>  '','id' => 'form-bulk-order', 'method' => 'PATCH']) !!}
            @include('admin.master.orders.bulks._form')
            <div class="form-group">
                <!-- <div class="col-sm-10"> -->
                    <a href="{{route('admin.order.index')}}" class="btn btn-default">Cancel</a>
                    <button type="submit" class="btn btn-primary" id="btn-submit">Save</button>
                <!-- </div> -->
            </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
    
</div>
@endsection
@section('scripts')
{!! Html::script('vendor/jsvalidation/js/jsvalidation.min.js') !!}
{!! JsValidator::formRequest('App\Http\Requests\Order\BulkOrderRequest', '#form-bulk-order'); !!}
<script type="text/javascript">
    $('.select2').select2();
    // var max_fields            = 5; //maximum input box allowed
    var add_bulk      = $('.add-order'); 

    // add more for saprotan .start here
    var x = 0; //initlal text box count
    var y = 0; //initlal text box count
    add_bulk.click(function(e) { //on add input button click
        e.preventDefault();
        // if(x < max_fields) { //max input box allowed
            x++; //text box increment
            y++; //text box increment
            $("#bulk-order").append(''
                +'<div class="row">'
                    +'<div class="col-sm-4 products">'
                        +'<div class="form-group {{ $errors->has('product_code') ? 'has-error' : ''}}">'
                            +'<select class="select2 form-control" name="product_code['+y+']">'
                                +'<option value="">Select product</option>'
                                @foreach($products as $key => $product)
                                    +'<option value="{{$key}}">{{$product}}</option>'
                                @endforeach
                            +'</select>'
                            +'{!! $errors->first('product_code', '<p class="help-block">:message</p>') !!}'
                        +'</div>'
                    +'</div>'
                    +'<div class="col-sm-4 quantitys">'
                        +'<div class="form-group {{ $errors->has('quantity') ? 'has-error' : ''}}">'
                            +'<label class="contro-label" style="display:none">Quantity</label>'
                            +'<input class="form-control" name="quantity['+y+']" placeholder="Quantity">'
                            +'{!! $errors->first('quantity', '<p class="help-block">:message</p>') !!}'
                        +'</div>'
                    +'</div>'
                    +'<div class="col-sm-2">'
                        +'<button class="btn btn-danger remove" type="button"><i class="fa fa-trash"></i></button>'
                    +'</div>'
                +'</div>'); //add 
        // } else
        // if(x = max_fields) {
        //     toastr.warning('Maximum 5 allowed');
        // }
    });
    
    $("#bulk-order").on("click",".remove", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').parent('div').remove(); x--;
    });
</script>
@endsection
