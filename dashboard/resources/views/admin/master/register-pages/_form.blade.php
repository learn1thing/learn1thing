@section('header')
<link rel="stylesheet" href="{{ asset('bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css') }}" />
<style>
    .currency {
        text-align: right;
    }
</style>
@endsection

<input type="hidden" name="id" value="{{@$department->id}}">
<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Title', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::text('title', old('title') , ['class' => 'form-control']) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::textarea('description', old('description') , ['class' => 'form-control']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    <label for="" class="col-sm-2 control-label"> Image </label>
    <div class="{!! $errors->has('image') !!} col-sm-10">
        <div class="fileinput fileinput-new" data-provides="fileinput">
            <div class="fileinput-new thumbnail" style="width: 200px; height: auto;">
                <img src="{!! ($register->image) ? get_file($register->image, 'compressed') : \Config::get('image.example')  !!}">
            </div>
            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
            <div>
                <span class="btn btn-default btn-file">
                    <span class="fileinput-new"><i class="fa fa-search m-right-10"></i> Browse</span>
                    <span class="fileinput-exists">Edit</span>
                    <input type="file" name="cover">
                </span>
                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Delete</a>
            </div>
        </div>
    </div>
    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
</div>