
@extends('layouts.admin_template')

@section('title', 'Edit Register Page')
@section('page_title', 'Edit Register Page')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.register-page.index') }}"><i class="fa fa-dashboard"></i> Register Page</a></li>
        <li>Edit</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
            {!! Form::model($register, [
                'route'     =>['admin.register-page.update', $register->id],
                'method'    => 'PATCH',
                'class'     =>  'form-horizontal',
                'id'        =>  'form-register-page',
                'enctype'   => 'multipart/form-data',
            ]) !!}
            @include('admin.master.register-pages._form')
            <div class="row">
                <div class="col-xs-12 m-top-10 text-right">
                    <a href="{{route('admin.register-page.index')}}" class="btn btn-default">
                       <i class="fa fa-times m-right-10"></i> Cancel
                    </a>
                
                    <button type="submit" class="btn btn-primary" id="btn-submit">
                        <i class="fa fa-check m-right-10"></i> Save
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
    
</div>
@endsection
@section('scripts')
<script src="{{ asset('bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js') }}"></script>
{!! Html::script('vendor/jsvalidation/js/jsvalidation.min.js') !!}
{!! JsValidator::formRequest('App\Http\Requests\Master\DepartmentRequest', '#form-register-page'); !!}
@endsection
