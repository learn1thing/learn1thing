@extends('layouts.admin_template')

@section('title', 'Top Up')
@section('page_title', 'Top Up')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Top Up</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        @if(user_info()->hasAccess('all') || user_info()->hasAccess('top-up-credit'))
            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Send Credit</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" id="form-send-credit" action="{{route('admin.credit.store')}}" method="POST">
                  <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('mutation') ? 'has-error' : ''}}">
                                {!! Form::text('mutation', old('mutation') , ['class' => 'number_only form-control','placeholder' => 'Insert Amount', 'id' => 'mutation']) !!}
                                {!! $errors->first('mutation', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('credit') ? 'has-error' : ''}}" {{ user_info()->hasAccess('top-up-credit') ? 'style=display:none':'' }}>
                                <select class="form-control select2" name="corporate">
                                    @if(user_info()->hasAccess('all'))
                                        <option value=""></option>
                                    @endif
                                    @if(count($corporates) > 0)
                                        @foreach($corporates as $corporate)
                                            <option value="{{$corporate->id}}">{{$corporate->first_name.' '.$corporate->last_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                {!! $errors->first('credit', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                    @if( user_info()->hasAccess('top-up-credit') && user_info()->related_type == 'App\Models\CorporateCustomer' )
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('total_payment') ? 'has-error' : ''}}">
                                    <div class="input-group">
                                    <span class="input-group-addon">S$</span>
                                        {!! Form::text('total_payment', old('total_payment') , ['class' => 'number_only form-control','placeholder' => 'Total Payment', 'id' => 'total_payment', 'readonly' => true]) !!}
                                        {!! $errors->first('total_payment', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                   <button type="submit" class="form-control btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                  <!-- /.box-body -->
                </form>
            </div>
        @endif

        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Top Up History</h3>
            </div>
            <!-- /.box-header -->
              <div class="box-body">
                <div class="table-responsive">
                 {!! $dataTable->table(['class' => 'datatable table table-striped', 'cellspacing'=>"0", 'width'=>"100%"]) !!}
                </div>
              </div>
              <!-- /.box-body -->
          </div>
    </div>
</div>

<div id="act-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <p id="text-body-confirm"></p>
                <div class="row">
                    <div class="col-md-12">
                        <form id="form-approved">
                            <input type="text" name="id" id="id" style="display:none">
                            <div class="form-group {{ $errors->has('day_expired_credit') ? 'has-error' : ''}}">
                                {!! Form::text('day_expired_credit', old('day_expired_credit') , ['class' => 'number_only form-control','placeholder' => 'Please insert maximum days credit can use', 'id'=>'day_expired_credit']) !!}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            <a id="delete-modal-cancel" href="#" class="btn btn-default pull-left" data-dismiss="modal"><i class="fa fa-times m-right-10"></i> {!! trans('label.cancel') !!}
            </a>
            <button class="btn btn-success" id="submit" type="button">
                <i class="fa fa-check m-right-10"></i> Continue
            </button>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
{!! $dataTable->scripts() !!}
{!! Html::script('vendor/jsvalidation/js/jsvalidation.min.js') !!}
{!! JsValidator::formRequest('App\Http\Requests\Master\SendCredit', '#form-send-credit'); !!}
<script type="text/javascript">
    $(document).ready(function(){
        var urlApproved = '';

        $('.select2').select2({
            placeholder: "Select Corporate User",
            allowClear: true
        });

        $(document).on('keyup', '#mutation', function() {
            $("#total_payment").val($(this).val());
        });

        $(document).on('click', '.approved_top_up', function() {
            var _type = $(this).data('type');
            urlApproved = $(this).data('href');
            $("#act-modal").modal('show');
            $("#act-modal .modal-title").html($(this).attr('title')+' Top Up Credit');
            $("#act-modal #id").val($(this).data('id'));
            $("#act-modal #day_expired_credit").val('');
            $("#act-modal #text-body-confirm").text('Are you sure want continue to '+$(this).attr('title')+' Top Up Credit');
            if (_type == 'approved') {
                $("#act-modal #day_expired_credit").show();
            } else {
                $("#act-modal #day_expired_credit").hide();
            }
        });

        $(document).on('click', '#submit', function() {

            $(".tooltip-field").remove();

            $.ajax({
              method: 'POST',
              url: urlApproved,
              data: $("#act-modal #form-approved").serialize(),
              beforeSend: function (data) {
                $('.btn').attr('disabled','disabled');
            
              },
              success: function (data) {
                $("#act-modal").modal('hide');
                toastr.success(data.message);
                setTimeout(function() {
                    window.location.reload(true);
                }, 100);
                $('.btn').removeAttr('disabled');
              },
              error: function (response) {
                if (response.status === 422) {
                    var data = response.responseJSON;
                    $.each(data,function(key,val){
                        $('<span class="text-danger tooltip-field"><span>'+val+'</span>').insertAfter($('#act-modal #'+key));
                        $('.'+key).addClass('has-error');
                    });
                } else {
                    toastr.error(response.responseJSON.message);
                }

                $('.btn').removeAttr('disabled');
              }
            });
        });
    });
</script>
@endsection
