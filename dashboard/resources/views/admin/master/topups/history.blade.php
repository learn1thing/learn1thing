@extends('layouts.admin_template')

@section('title', 'Credit History')
@section('page_title', 'Credit History')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Credit History</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
              <div class="box-body">
                <div class="table-responsive">
                 {!! $dataTable->table(['class' => 'datatable table table-striped', 'cellspacing'=>"0", 'width'=>"100%"]) !!}
                </div>
              </div>
              <!-- /.box-body -->
          </div>
    </div>
</div>
@endsection
@section('scripts')
{!! $dataTable->scripts() !!}
@endsection
