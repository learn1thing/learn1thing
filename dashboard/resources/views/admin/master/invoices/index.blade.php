@extends('layouts.admin_template')

@section('title', 'Invoice')
@section('page_title', 'Invoice')
@section('page_description', 'learn1thing')

@section('header')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Invoice</li>
    </ol>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <select class="form-control select2" name="corporate">
                      <option value="">Choose Corporate</option>
                      @if(count($corporates) > 0)
                        @foreach($corporates as $corporate)
                        <option value="{{$corporate->id}}">{{$corporate->first_name.' '.$corporate->last_name}}</option>
                        @endforeach
                      @endif
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="input-group">
                      <input type="text" name="start_date" class="form-control datepicker" id="first_date" placeholder="Start date">
                      <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="input-group">
                      <input type="text" name="end_date" class="form-control datepicker" id="end_date" placeholder="End Date">
                      <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <button class="pull-right btn btn-primary get-transaction">List Transaction</button>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-12" id="render-result">
                  <table class="table table-hover">
                    <tbody><tr>
                      <th>ID</th>
                      <th>Date</th>
                      <th>Product</th>
                      <th>Status</th>
                      <th>Quantity</th>
                      <th>Unit Price</th>
                      <th>Total</th>
                    </tr>
                    <tr>
                      <td colspan="7" align="center"><i>No Transaction</i></td>
                    </tr>
                  </tbody></table>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
      var first_date = '';
      var end_date = '';

     $(function () {
        //Initialize Select2 Elements

        //Date picker
        $('.datepicker').datepicker({
          autoclose: true,
          dateFormat:"yy-mm-dd",
        });

        $("#end_date").change(function() {
            end_date = $(this).val();
            first_date = $("#first_date").val();
        });

        $("#first_date").bind("change",function(){
            var minValue = $(this).val();
            var maxValue = $('#end_date').val();
            maxValue = $.datepicker.parseDate("yy-mm-dd", maxValue);
            minValue = $.datepicker.parseDate("yy-mm-dd", minValue);
            $("#end_date").datepicker( "option", "minDate", minValue );
            if (maxValue !== null){
                if (minValue > maxValue) {
                    toastr.warning('End date must be greater than first date');
                }
            }
            
        })

    });
    
</script>
<script type="text/javascript">
  $(document).ready(function(){
      $('.select2').select2({
          placeholder: "Select Corporate User",
          allowClear: true
      });
  });

  $(document).on('click','.get-transaction',function() {
    var corporate = $('select[name=corporate]').val();
    var start_date = $('input[name=start_date]').val();
    var end_date = $('input[name=end_date]').val();

    if (corporate != '') {
      $.ajax({
        method: 'GET',
        dataType: 'json',
        url: "{{route('admin.invoice.index')}}",
        data: {
          'corporate': corporate,
          'start_date': start_date,
          'end_date': end_date
        },
        success: function (data) {
          $('#render-result').html(data.data);
        },
        error: function (data) {
          toastr.error('Something went wrong');
        }
      });
    } else {
      toastr.warning('Please choose corporate first');
      return false;
    }
  });

  $(document).on('click', '.send-mail', function() {
    $.ajax({
      method: 'GET',
      url: "{{route('admin.invoice.send-mail')}}",
      dataType: 'json',
      data: {
        'data': $(this).data('send'),
        'user': $(this).data('user'),
      },
      beforeSend: function (data) {
        $('.btn').attr('disabled','disabled');
        toastr.info('Sending email, please wait');
      },
      success: function (data) {
        toastr.success(data.message);
        $('.btn').removeAttr('disabled');
      },
      error: function (data) {
        toastr.error('Something went wrong');
        $('.btn').removeAttr('disabled');
      }
    });
  });

  $(document).on('click','.invoice-complete', function() {
    $.ajax({
      method: 'GET',
      url: "{{route('admin.invoice.update')}}",
      dataType: 'json',
      data: {
        'id': $(this).data('id'),
        'data': $(this).data('send'),
      },
      beforeSend: function (data) {
        $('.btn').attr('disabled','disabled');
        toastr.info('Processing, please wait');
      },
      success: function (data) {
        toastr.success(data.message);
        $('.btn').removeAttr('disabled');
        $('.get-transaction').click();
      },
      error: function (data) {
        toastr.error('Something went wrong');
        $('.btn').removeAttr('disabled');
      }
    });
  });

  $(document).on('click','.print-pdf',function() {
    var url = window.location.href;
    var dataPrint = $(this).data('print');
    var dataUser = $(this).data('user');
    var first_date = $(this).data('first_date');
    var last_date = $(this).data('last_date');

    $.ajax({
      method: 'GET',
      url: "{{route('admin.invoice.print-pdf')}}",
      dataType: 'json',
      data: {
        'data': dataPrint,
        'user': dataUser,
        'first_date': first_date,
        'last_date': last_date,
        'check': true
      },
      beforeSend: function (data) {
        $('.btn').attr('disabled','disabled');
        toastr.info('Processing, please wait');
      },
      success: function (data) {
        if (data.status) {
          setTimeout(function(){ $('.btn').removeAttr('disabled'); }, 5000);
          window.location = url+'/print-pdf?user='+dataUser+'&data='+dataPrint+'&first_date='+first_date+'&last_date='+last_date;
        } else {
          $('.btn').removeAttr('disabled');
          toastr.success(data.message);
        }
      },
      error: function (data) {
        toastr.error('Something went wrong');
        $('.btn').removeAttr('disabled');
      }
    });
  });
</script>
@endsection
