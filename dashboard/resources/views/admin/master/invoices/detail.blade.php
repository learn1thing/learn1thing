@extends('layouts.admin_template')

@section('title', 'Detail Invoice')
@section('page_title', 'Detail Invoice')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Detail Invoice</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
              <div class="box-body">
                <table class="table table-striped">
                  <tbody>
                    <tr>
                      <th>Product</th>
                      <th>Status</th>
                      <th>Quantity</th>
                      <th>Unit Price</th>
                      <th>Total</th>
                    </tr>
                    @php($total=0)
                    @if(count($invoices) > 0)
                      @foreach($invoices as $invoice)
                      @php($total+=$invoice->total)
                      <tr>
                        <td>{{$invoice->product_name}}</td>
                        <td>
                          @if($invoice->status == 1)
                            <span class="label label-warning">Pending</span>
                          @elseif($invoice->status == 2)
                            <span class="label label-success">Complete</span>
                          @else
                            <span class="label label-danger">Reject</span>
                          @endif
                        </td>
                        <td style="text-align: right;">{{$invoice->quantity}}</td>
                        <td style="text-align: right;">{{number_format($invoice->price)}}</td>
                        <td style="text-align: right;">{{number_format($invoice->total)}}</td>
                      </tr>
                      @endforeach
                    @endif
                  </tbody>
                  <tfoot>
                    <!-- <tr>
                      <td colspan="4" style="text-align: right;">Sub-Total</td>
                      <td>{{number_format($total)}}</td>
                    </tr> -->
                    <tr>
                      <td colspan="4" style="text-align: right;">Total</td>
                      <td style="text-align: right;">{{number_format($total)}}</td>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{route('admin.invoice.index')}}" class="btn btn-primary">Back</a>
              </div>
          </div>
    </div>
</div>
@endsection
@section('scripts')

@endsection
