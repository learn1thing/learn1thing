@extends('layouts.admin_template')

@section('title', 'Create Permission')
@section('page_title', 'Create Permission')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li> Master</li>
        <li> Create Permission</li>
    </ol>
@endsection

@section('content')
    {!! Form::open([
            'route' =>  'admin.permission.store',
            'class' =>  'form-horizontal',
            'id'    =>  'form-permission',
        ]) !!}
        <div class="box">
            <div class="box-body">
                @include('admin.master.permissions._form')  
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <a href="{{route('admin.permission.index')}}" class="btn btn-default">{{trans('label.cancel')}}</a>
                        <button type="submit" class="btn btn-primary" id="btn-submit">{{ trans('label.save') }}</button>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection
@section('scripts')
<script type="text/javascript" src="{{ url('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! JsValidator::formRequest('App\Http\Requests\Master\PermissionRequest') !!}
@endsection