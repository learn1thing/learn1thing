@extends('layouts.admin_template')

@section('title', 'Permission')
@section('page_title', 'Permission')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-link m-right-10"></i> User Administration</li>
        <li>Permission</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <!-- <br>
        <div class="row">
            <div class="col-xs-12">
                <a href="{{route('admin.permission.create')}}" class="btn btn-sm btn-info">Add Permission</a>
            </div>
        </div>
        <br> -->
        <div class="box">
            <div class="box-body">
             {!! $dataTable->table(['class' => 'datatable table table-striped', 'cellspacing'=>"0", 'width'=>"100%"]) !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
{!! $dataTable->scripts() !!}
@endsection