@extends('layouts.admin_template')

@section('title', 'Manage Permission')
@section('page_title', 'Manage Permission')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-link m-right-10"></i> User Administration</li>
        <li><a href="{{route('admin.permission.index')}}">Permission</a></li>
        <li>Manage Permission</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-title">
                <h4 class="col-md-12">{{ $role->name }}</h4>
            </div>
            <div class="box-body">
                <input type="hidden" name="id_role" value="{{ $role->id }}">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th width="20%">Feature Name</th>
                            <th width="70%">Description</th>
                            <!-- <th width="60%">Feature URL</th> -->
                            <th width="10%">Access</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($permissions as $permission)
                            <tr>
                                <td>{{$permission->name}}</td>
                                <td>{{$permission->description}}</td>
                                <!-- <td>{{$permission->url}}</td> -->
                                <td><input type="checkbox" name="permission" value="{!! $permission->slug !!}" {!! $role->hasAccess($permission->slug) !!}></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <table class="table">
                    <tr>
                        <td><a href="{!! route('admin.permission.index') !!}" class="btn btn-mini btn-default m-top-20">
                         <i class="fa fa-angle-double-left m-right-10"></i> Back</a></td>
                        <td class="pull-right">{!! $permissions->links() !!}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $('input[name=permission]').click(function(){
            var id_role = $('input[name=id_role]').val();
            var permission = $(this).val();
            if($(this).is(':checked')) {
                // set permissions to this role
                $.ajax({
                    url: '{{ route('admin.permission.set') }}',
                    type: 'POST',
                    dataType:'json',
                    data: { id_role : id_role, permission : permission },
                })
                .done(function(response) {
                    toastAlert(response.message,response.type);
                    if (!response.result) {
                        setTimeout(function(){
                            window.location.reload();
                        },500);
                    }
                })
                .fail(function(e) {
                    window.location.reload();
                });
                
            }else{
                // unset permissions to this role
                $.ajax({
                    url: '{{ route('admin.permission.unset') }}',
                    type: 'POST',
                    dataType:'json',
                    data: { id_role : id_role, permission : permission },
                })
                .done(function(response) {
                    toastAlert(response.message,response.type);
                    if (!response.result) {
                        setTimeout(function(){
                            window.location.reload();
                        },500);
                    }
                })
                .fail(function(e) {
                    window.location.reload();
                });
            }
        });
    </script>
@endsection