@extends('layouts.admin_template')

@section('title', 'Request Change Payment Method List')
@section('page_title', 'Request Change Payment Method List')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-dashboard"></i> Dashboard</li>
        <li>Request Change Payment Method List</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
              <div class="table-responsive">
                 {!! $dataTable->table(['class' => 'datatable table table-striped', 'cellspacing'=>"0", 'width'=>"100%"]) !!}
              </div>
            </div>
        </div>
    </div>
</div>
@include('partials.delete-modal')
@endsection
@section('scripts')
{!! $dataTable->scripts() !!}
<script type="text/javascript">
  $(document).on('click','.app-dec',function(){
    $.ajax({
      url: $(this).data('href'),
      method: 'patch',
      dataType: 'json',
      beforeSend: function(data) {
        toastAlert('Please wait, system is running background process!','warning');
        $('.datatable a').addClass('processing');
        $('.datatable a').removeAttr('id');
      },
      success:function(data) {
        switch(data.type){
          case 'info':
          toastr.info(data.message);
          break;
          case 'warning':
          toastr.warning(data.message);
          break;
          case 'success':
          toastr.success(data.message);
          break;
          case 'error':
          toastr.error(data.message);
          break;
        }

        $('.datatable').DataTable().ajax.reload();
      },
      error: function(data) {
        toastr.error('Something went wrong');
      }
    });
  });
</script>
@endsection
