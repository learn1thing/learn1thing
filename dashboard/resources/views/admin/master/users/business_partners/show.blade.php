@extends('layouts.admin_template')

@section('title', 'Dashboard')
@section('page_title', 'Dashboard')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>User</li>
        <li>Detail Business Partner</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                        <h2>{{ $data->first_name.' '.$data->last_name}}</h2>
                        <div class="col-md-3 col-lg-3 " align="center"> 
                            <img alt="User Pic" src="https://s3.amazonaws.com/wll-community-production/images/no-avatar.png" class="img-circle img-responsive"> </div>
            
                        </div>
                        <div class="col-md-8">
                        <h2><!--  --></h2>
                        <div class=" col-md-9 col-lg-9 ">
                            <table class="table table-user-information">
                                <tbody>
                                    <tr>
                                        <td><b>First Name :</b></td>
                                        <td>{{ $data->first_name }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Last Name :</b></td>
                                        <td>{{ $data->last_name }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Email :</b></td>
                                        <td>{{ $data->email }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Joined :</b></td>
                                        <td>{{ $data->created_at }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Phone Number :</b></td>
                                        <td>{{ $data->phone }}</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@endsection
