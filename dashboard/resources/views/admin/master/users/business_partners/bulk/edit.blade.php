
@extends('layouts.admin_template')

@section('title', 'Edit Users')
@section('page_title', 'Edit Users')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('admin.business-partner.bulk.index')}}"><i class="fa fa-user m-right-10"></i> Users</a></li>
        <li>Edit</li>
    </ol>
@endsection

@section('content')
    {!! Form::model($user, [
            'route'     =>['admin.business-partner.bulk.update', $user->id],
            'method'    => 'PATCH',
            'class' =>  'form-horizontal',
            'id'    =>  'form-user',
        ]) !!}
        <div class="box">
            <div class="box-body">
                @include('admin.master.users.business_partners.bulk._form')
                <div class="row">
                    <div class="col-xs-12 m-top-10 text-right">
                        <a href="{{route('admin.business-partner.bulk.index')}}" class="btn btn-default">
                           <i class="fa fa-times m-right-10"></i> Cancel
                        </a>
                    
                        <button type="submit" class="btn btn-primary" id="btn-submit">
                            <i class="fa fa-check m-right-10"></i> Save
                        </button>
                    </div>
                </div>          
            </div>
        </div>
    {!! Form::close() !!}
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).on('change','#country',function(){
            getZone($('#region'),$(this).val());
        });
    </script>
@endsection
