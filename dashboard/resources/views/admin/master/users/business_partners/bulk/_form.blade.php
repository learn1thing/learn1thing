<style type="text/css">
    input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
</style> 
<div class="form-group {{ $errors->has('first_name') ? 'has-error' : ''}}">
    {!! Form::label('first_name', trans('First Name'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::text('first_name', old('first_name') , ['class' => 'form-control']) !!}
        {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('last_name') ? 'has-error' : ''}}">
    {!! Form::label('last_name', trans('Last Name'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::text('last_name', old('last_name') , ['class' => 'form-control']) !!}
        {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('related_id') ? 'has-error' : ''}}">
    {!! Form::label('related_id', trans('Role Group'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::select('related_id', $related_ids, old('related_id'), ['class' => 'form-control']) !!}
        {!! $errors->first('related_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('department_id') ? 'has-error' : ''}}">
    {!! Form::label('department_id', trans('Department'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        <select class="form-control select2" name="department_id">
            <option value="">Please Select Department</option>
            @if(count($departments) > 0)
                @foreach($departments as $parent)
                    @if(in_array($parent->id, $parentData ))
                    <optgroup label="{{$parent->name}}">
                    @foreach($parent->childs as $child)
                        @if($parent->parent_id != "")
                            <option value="{{ $parent->id }}" {{($parent->id == @$user->department_id) ? 'selected' : ''}}>- {{ $parent->name }}</option>
                        @endif
                        <option value="{{ $child->id }}" {{($child->id == @$user->department_id) ? 'selected' : ''}}>- {{ $child->name }}</option>
                    @endforeach
                    </optgroup>
                    @endif
                @endforeach
            @endif
            <!-- @if(count($departments) > 0)
                @foreach($departments as $parent)
                    <option value="{{$parent->id}}" class="optionGroup" {{($parent->id == @$user->department_id) ? 'selected' : ''}}>{{$parent->name}}</option>
                    @if($parent->childs->count())
                        @foreach($parent->childs as $child)
                            <option value="{{$child->id}}" {{($child->id == @$user->department_id) ? 'selected' : ''}}>--- {{$child->name}}</option>
                        @endforeach
                    @endif
                @endforeach
            @endif -->
        </select>
        {!! $errors->first('department_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}" style="display: {{(@$user->id) ? 'none' : 'block'}};">
    {!! Form::label('email', trans('Email'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::email('email', old('email') , ['class' => 'form-control']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
    {!! Form::label('phone', trans('Phone'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::number('phone', old('phone') , ['class' => 'form-control']) !!}
        {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}" style="display: {{(@$user->id) ? 'none' : 'block'}};">
    {!! Form::label('password', trans('New Password'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::password('password' , ['class' => 'form-control']) !!}
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('country') ? 'has-error' : ''}}">
    {!! Form::label('country', trans('Country'), ['class' => 'control-label col-sm-2']) !!}
    <div class="col-sm-5">
        {!! Form::select('country', [''=>'Select Country'] + $countries, old('country'), ['class' => 'select2 form-control']) !!}
        {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
        </div>
</div>
<div class="form-group {{ $errors->has('region') ? 'has-error' : ''}}">
    {!! Form::label('region', trans('region'), ['class' => 'control-label col-sm-2']) !!}
    <div class="col-sm-5">
        {!! Form::select('region', (@zones) ? [''=>'Select Region'] + @$zones : [''=>'Select Region'], old('region'), ['class' => 'select2 form-control']) !!}
        {!! $errors->first('region', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@section('part_js')
{!! Html::script('vendor/jsvalidation/js/jsvalidation.min.js') !!}
{!! JsValidator::formRequest('App\Http\Requests\Bulk\PostSingleUser', '#form-user'); !!}
<script type="text/javascript">
    $(document).ready(function(){
        $('.select2').select2();
    });
</script>
@endsection