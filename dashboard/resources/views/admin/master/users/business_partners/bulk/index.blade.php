@extends('layouts.admin_template')

@section('title', 'Users')
@section('page_title', 'Users')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-user m-right-10"></i> Users</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-3 pull-right">
        <a href="{{ route('admin.business-partner.create.single-user')}}" class="btn btn-primary pull-right" id="btn-submit">
           <i class="fa fa-plus m-right-10"></i> Add User
        </a>
    </div>
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
            @if(Session::has('flash_message'))
                <div class="alert alert-success"><em> {!! session('flash_message') !!}</em></div>
            @endif
            @if(Session::has('exists'))
                <div class="alert alert-warning"><em> {!! session('exists') !!}</em></div>
            @endif
             @if(Session::has('invalid'))
                <div class="alert alert-error"><em> {!! session('invalid') !!}</em></div>
            @endif
            <div class="table-responsive">
              {!! $dataTable->table(['class' => 'datatable table table-striped', 'cellspacing'=>"0", 'width'=>"100%"]) !!}
            </div>
            </div>
        </div>
    </div>
</div>
@include('partials.delete-modal')
@endsection
@section('scripts')
{!! $dataTable->scripts() !!}
<script type="text/javascript">
    $('div.alert').delay(5000).slideUp(300);
</script>
<script type="text/javascript">
    // $(document).on('click','form#destroy #submit',function(e) {
    //     e.preventDefault();
    //     $('#delete-modal').modal('toggle');
    //     toastr.error("Sorry, user can't be deleted");
    // });
</script>
@endsection
