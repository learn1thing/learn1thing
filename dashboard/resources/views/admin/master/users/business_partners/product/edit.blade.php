@extends('layouts.admin_template')

@section('title', 'Edit Product')
@section('page_title', 'Edit Product')
@section('page_description', 'learn1thing')

@section('header')
    <link href="{{asset('css/summernote.css')}}" rel="stylesheet">
    <link href="{{asset('css/bootstrap-datetimepicker.min.css')}}" type="text/css" rel="stylesheet" media="screen">
    <link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet" />
    <style type="text/css">
        .well.well-sm {
            text-align: left;
        }
        #tab-option ul#option li{
            text-align: left;
        }
    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-shopping-bag m-right-10"></i> Product</li>
        <li>Product List</li>
        <li>Edit</li>
    </ol>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                @if(Session::has('flash_message'))
                    <div class="alert alert-success"><em> {!! session('flash_message') !!}</em></div>
                @endif
                @if(Session::has('error'))
                    <div class="alert alert-error"><em> {!! session('error') !!}</em></div>
                @endif
                <div class="alert alert-error" style="display: none;"><em> </em></div>
            
                <form action="{{route('admin.business-partner.product.update',['id' => $product->id])}}" method="post" enctype="multipart/form-data" id="form-product" class="form-horizontal text-center">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
                        <li><a href="#tab-data" data-toggle="tab">Data</a></li>
                        <li><a href="#tab-links" data-toggle="tab">Links</a></li>
                        <li><a href="#tab-attribute" data-toggle="tab">Attribute</a></li>
                        <!-- <li><a href="#tab-option" data-toggle="tab">Option</a></li> -->
                        <!-- <li><a href="#tab-recurring" data-toggle="tab">Recurring</a></li> -->
                        <!-- <li><a href="#tab-subscribe" data-toggle="tab">Subscriptions</a></li> -->
                        <li><a href="#tab-discount" data-toggle="tab">Discount</a></li>
                        <li><a href="#tab-special" data-toggle="tab">Special</a></li>
                        <li><a href="#tab-image" data-toggle="tab">Image</a></li>
                        <!-- <li><a href="#tab-reward" data-toggle="tab">Reward Points</a></li> -->
                        <!-- <li><a href="#tab-design" data-toggle="tab">Design</a></li> -->
                    </ul>
                    <br>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-general">
                            <ul class="nav nav-tabs" id="language">
                                <li class="active"><a href="#language1" data-toggle="tab" aria-expanded="true"><img src="https://railsignalling.org/signalwiki/images/e/e9/Flag_en-gb.png" width="20%" title="English"> English</a></li>
                            </ul>
                            <br>
                            <input type="hidden" name="product_description[1][language_id]" value="1">
                            <input type="hidden" name="product_id" value="{{$product->id}}">
                            <div class="tab-content">
                                <div class="tab-pane active" id="language1">
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-product_type"><span style="color: red">*</span>Product Type</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="product_type">
                                                <option value="">Select Product Type</option>
                                                <option value="offline" {{($product->detailProduct->type == 'offline') ? 'selected' : ''}}>Offline</option>
                                                <option value="online" {{($product->detailProduct->type == 'online') ? 'selected' : ''}}>Online</option>
                                                <option value="mix-product" {{($product->detailProduct->type == 'mix-product') ? 'selected' : ''}}>MIX Product</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-name1"><span style="color: red">*</span>Product Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="product_description[1][name]" placeholder="Product Name" id="input-name1" value="{{$product->product_description->{1}->name}}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-description1">Description</label>
                                        <div class="col-sm-10">
                                            <textarea name="product_description[1][description]" placeholder="Description" id="input-description1" class="form-control textarea">{{$product->product_description->{1}->description}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-meta-title1"><span style="color: red">*</span>Meta Tag Title</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="product_description[1][meta_title]" value="{{$product->product_description->{1}->meta_title}}" placeholder="Meta Tag Title" id="input-meta-title1" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-meta-description1">Meta Tag Description</label>
                                        <div class="col-sm-10">
                                            <textarea name="product_description[1][meta_description]" rows="5" placeholder="Meta Tag Description" id="input-meta-description1" class="form-control">{{$product->product_description->{1}->meta_description}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-meta-keyword1">Meta Tag Keywords</label>
                                        <div class="col-sm-10">
                                            <textarea name="product_description[1][meta_keyword]" rows="5" placeholder="Meta Tag Keywords" id="input-meta-keyword1" class="form-control">{{$product->product_description->{1}->meta_keyword}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-tag1"><span data-toggle="tooltip" title="" data-original-title="Comma separated">Product Tags</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="product_description[1][tag]" value="{{$product->product_description->{1}->tag}}" placeholder="Product Tags" id="input-tag1" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-data">
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-model"><span style="color: red">*</span>Model</label>
                                <div class="col-sm-10">
                                    <input type="text" name="model" value="{{$product->model}}" placeholder="Model" id="input-model" class="form-control">
                                </div>
                            </div>
                            <div class="form-group" style="display: none;">
                                <label class="col-sm-2 control-label" for="input-sku"><span data-toggle="tooltip" title="" data-original-title="Stock Keeping Unit">SKU</span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="sku" value="{{$product->sku}}" placeholder="SKU" id="input-sku" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-upc"><span data-toggle="tooltip" title="" data-original-title="Universal Product Code">UPC</span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="upc" value="{{$product->upc}}" placeholder="UPC" id="input-upc" class="form-control">
                                </div>
                            </div>
                            <div style="display:none">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="input-ean"><span data-toggle="tooltip" title="" data-original-title="European Article Number">EAN</span></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="ean" value="{{$product->ean}}" placeholder="EAN" id="input-ean" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="input-jan"><span data-toggle="tooltip" title="" data-original-title="Japanese Article Number">JAN</span></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="jan" value="{{$product->jan}}" placeholder="JAN" id="input-jan" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="input-isbn"><span data-toggle="tooltip" title="" data-original-title="International Standard Book Number">ISBN</span></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="isbn" value="{{$product->isbn}}" placeholder="ISBN" id="input-isbn" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="input-mpn"><span data-toggle="tooltip" title="" data-original-title="Manufacturer Part Number">MPN</span></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="mpn" value="{{$product->mpn}}" placeholder="MPN" id="input-mpn" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-location">Location</label>
                                <div class="col-sm-10">
                                    <input type="text" name="location" value="{{$product->location}}" placeholder="Location" id="input-location" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-price">Price</label>
                                <div class="col-sm-10">
                                    <input type="text" name="price" maxlength="11" value="{{$product->price}}" placeholder="Price" id="input-price" class="form-control number_only">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-credit">Credit</label>
                                <div class="col-sm-10">
                                    <input type="text" maxlength="11" name="credit" value="{{number_format(@$product->detailProduct->credit)}}" placeholder="Credit" id="input-credit" class="form-control number_only">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-tax-class">Tax Class</label>
                                <div class="col-sm-10">
                                    <select name="tax_class_id" id="input-tax-class" class="form-control">
                                        <option value="0" {{($product->tax_class_id == 0) ? 'selected' : ''}}> --- None --- </option>
                                        <option value="9" {{($product->tax_class_id == 9) ? 'selected' : ''}}>Taxable Goods</option>
                                        <option value="10" {{($product->tax_class_id == 10) ? 'selected' : ''}}>Downloadable Products</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-time_limit">Time Limit (hours)</label>
                                <div class="col-sm-10">
                                    <input type="text" name="time_limit" value="{{number_format($product->detailProduct->time_limit)}}" placeholder="Time Limit" id="input-time_limit" class="form-control number_only">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-min_quota">Minimum Quota</label>
                                <div class="col-sm-10">
                                    <input type="text" name="min_quota" value="{{number_format(@$product->detailProduct->productQuota->min_quota)}}" placeholder="Minimum Quota" id="input-min_quota" class="form-control number_only">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-min_quota">Session</label>
                                <div class="col-sm-10">
                                    <input type="text" name="session_coach" value="{{number_format($product->detailProduct->session)}}" placeholder="Session" id="input-session_coach" class="form-control number_only" maxlength="11">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-quantity">Quantity</label>
                                <div class="col-sm-10">
                                    <input type="text" name="quantity" value="{{$product->quantity}}" placeholder="Quantity" id="input-quantity" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-minimum"><span data-toggle="tooltip" title="" data-original-title="Force a minimum ordered amount">Minimum Quantity</span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="minimum" value="{{$product->minimum}}" placeholder="Minimum Quantity" id="input-minimum" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-subtract">Subtract Stock</label>
                                <div class="col-sm-10">
                                    <select name="subtract" id="input-subtract" class="form-control">
                                        <option value="1" {{($product->subtract == 1) ? 'selected' : ''}}>Yes</option>
                                        <option value="0" {{($product->subtract == 0) ? 'selected' : ''}}>No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-stock-status"><span data-toggle="tooltip" title="" data-original-title="Status shown when a product is out of stock">Out Of Stock Status</span></label>
                                <div class="col-sm-10">
                                    <select name="stock_status_id" id="input-stock-status" class="form-control">
                                        <option value="6" {{($product->stock_status == 6) ? 'selected' : ''}}>2-3 Days</option>
                                        <option value="7" {{($product->stock_status == 7) ? 'selected' : ''}}>In Stock</option>
                                        <option value="5" {{($product->stock_status == 5) ? 'selected' : ''}}>Out Of Stock</option>
                                        <option value="8" {{($product->stock_status == 8) ? 'selected' : ''}}>Pre-Order</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" style="display:none">
                                <label class="col-sm-2 control-label">Requires Shipping</label>
                                <div class="col-sm-10">
                                    <label class="radio-inline">
                                    <input type="radio" name="shipping" value="1" {{($product->shipping == 1) ? 'checked' : ''}}>
                                    Yes                                      </label>
                                    <label class="radio-inline">
                                    <input type="radio" name="shipping" value="0" {{($product->shipping == 0) ? 'checked' : ''}}>
                                    No                                      </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-keyword"><span data-toggle="tooltip" title="" data-original-title="Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.">SEO URL</span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="keyword" value="{{$product->keyword}}" placeholder="SEO URL" id="input-keyword" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-date-available">Date Available</label>
                                <div class="col-sm-3">
                                    <div class="input-group date">
                                        <input type="text" name="date_available" value="{{$product->date_available}}" placeholder="Date Available" data-date-format="YYYY-MM-DD" id="input-date-available" class="form-control">
                                        <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="display:none">
                                <label class="col-sm-2 control-label" for="input-length">Dimensions (L x W x H)</label>
                                <div class="col-sm-10">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <input type="text" name="length" value="{{$product->length}}" placeholder="Length" id="input-length" class="form-control">
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" name="width" value="{{$product->width}}" placeholder="Width" id="input-width" class="form-control">
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" name="height" value="{{$product->height}}" placeholder="Height" id="input-height" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="display:none">
                                <label class="col-sm-2 control-label" for="input-length-class">Length Class</label>
                                <div class="col-sm-10">
                                    <select name="length_class_id" id="input-length-class" class="form-control">
                                        <option value="1" {{($product->length_class_id == 1) ? 'selected' : ''}}>Centimeter</option>
                                        <option value="2" {{($product->length_class_id == 2) ? 'selected' : ''}}>Millimeter</option>
                                        <option value="3" {{($product->length_class_id == 3) ? 'selected' : ''}}>Inch</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" style="display:none">
                                <label class="col-sm-2 control-label" for="input-weight">Weight</label>
                                <div class="col-sm-10">
                                    <input type="text" name="weight" value="{{$product->weight}}" placeholder="Weight" id="input-weight" class="form-control">
                                </div>
                            </div>
                            <div class="form-group" style="display:none">
                                <label class="col-sm-2 control-label" for="input-weight-class">Weight Class</label>
                                <div class="col-sm-10">
                                    <select name="weight_class_id" id="input-weight-class" class="form-control">
                                        <option value="1" {{($product->weight_class_id == 1) ? 'selected' : ''}}>Kilogram</option>
                                        <option value="2" {{($product->weight_class_id == 2) ? 'selected' : ''}}>Gram</option>
                                        <option value="5" {{($product->weight_class_id == 5) ? 'selected' : ''}}>Pound </option>
                                        <option value="6" {{($product->weight_class_id == 6) ? 'selected' : ''}}>Ounce</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status">Show At Store</label>
                                <div class="col-sm-10">
                                    <select name="status" id="input-status" class="form-control">
                                        <option value="1" {{($product->status == 1) ? 'selected' : ''}}>Enabled</option>
                                        <option value="0" {{($product->status == 0) ? 'selected' : ''}}>Disabled</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-skip_import"><span data-toggle="tooltip" title="" data-original-title="The field allows to exclude the product from 'CSV Product Import' operations. No product data will be updated during the import procedure.">Skip Import</span></label>
                                <div class="col-sm-10">
                                    <select name="skip_import" id="input-skip_import" class="form-control">
                                        <option value="0" selected="selected">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-sort-order">Sort Order</label>
                                <div class="col-sm-10">
                                    <input type="text" name="sort_order" value="{{$product->sort_order}}" placeholder="Sort Order" id="input-sort-order" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-links">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-manufacturer"><span data-toggle="tooltip" title="" data-original-title="(Autocomplete)">Manufacturer</span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="manufacturer" value="{{$product->manufacturer}}" placeholder="Manufacturer" id="input-manufacturer" class="form-control" autocomplete="off">
                                    <ul class="dropdown-menu"></ul>
                                    <input type="hidden" name="manufacturer_id" value="{{($product->manufacturer_id) ? $product->manufacturer_id : 0}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-category"><span data-toggle="tooltip" title="" data-original-title="(Autocomplete)">Categories</span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="category" value="" placeholder="Categories" id="input-category" class="form-control" autocomplete="off">
                                    <ul class="dropdown-menu"></ul>
                                    <div id="product-category" class="well well-sm" style="height: 150px; overflow: auto;">
                                        @if(count($product->category) > 0)
                                            @foreach($product->category as $key => $category)
                                                <div id="product-category{{$key}}">
                                                    <i class="fa fa-minus-circle"></i> {{$category[0]->name}}
                                                    <input type="hidden" name="product_category[]" value="{{$key}}">
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-filter"><span data-toggle="tooltip" title="" data-original-title="(Autocomplete)">Filters</span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="filter" value="" placeholder="Filters" id="input-filter" class="form-control" autocomplete="off">
                                    <ul class="dropdown-menu"></ul>
                                    <div id="product-filter" class="well well-sm" style="height: 150px; overflow: auto;">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Stores</label>
                                <div class="col-sm-10">
                                    <div class="well well-sm" style="height: 150px; overflow: auto;">
                                        <div class="checkbox">
                                            <label>
                                            <input type="checkbox" name="product_store[]" value="0" checked="checked">
                                            Default                                              </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-download"><span data-toggle="tooltip" title="" data-original-title="(Autocomplete)">Downloads</span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="download" value="" placeholder="Downloads" id="input-download" class="form-control" autocomplete="off">
                                    <ul class="dropdown-menu"></ul>
                                    <div id="product-download" class="well well-sm" style="height: 150px; overflow: auto;">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-recommended"><span data-toggle="tooltip" title="" data-original-title="Type the product name to add it to the list (autocomplete).">Recommended Products</span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="recommended" value="" placeholder="Recommended Products" id="input-recommended" class="form-control" autocomplete="off">
                                    <ul class="dropdown-menu"></ul>
                                    <div id="product-recommended" class="well well-sm" style="height: 150px; overflow: auto;">
                                    </div>
                                </div>
                                <label class="col-sm-2 control-label" for="input-recommended-module"><span data-toggle="tooltip" title="" data-original-title="Settings will be taken from selected module.">Recommended Module</span></label>
                                <div class="col-sm-10">
                                    <select name="recommended_module" id="input-tax-class" class="form-control">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-related"><span data-toggle="tooltip" title="" data-original-title="(Autocomplete)">Related Products</span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="related" value="" placeholder="Related Products" id="input-related" class="form-control" autocomplete="off">
                                    <ul class="dropdown-menu"></ul>
                                    <div id="product-related" class="well well-sm" style="height: 150px; overflow: auto;">
                                        @if(count($product->related_products) > 0)
                                            @foreach($product->related_products as $related)
                                                <div id="product-related{{$related['id']}}">
                                                    <i class="fa fa-minus-circle"></i> {{$related['name']}}
                                                    <input type="hidden" name="product_related[]" value="{{$related['id']}}">
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-attribute">
                            <div class="table-responsive">
                                <table id="attribute" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <td class="text-left">Attribute</td>
                                            <td class="text-left">Text</td>
                                            <td></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count(@$product->product_attributes->attributes) > 0)
                                            @php($keyAttr = 0)
                                            @foreach($product->product_attributes->attributes as $key => $attribute)
                                                <tr id="attribute-row{{$keyAttr}}">
                                                    <td class="text-left" style="width: 20%;">
                                                        <input type="text" name="product_attribute[{{$keyAttr}}][name]" value="{{$attribute->{1}->name}}" placeholder="Attribute" class="form-control" autocomplete="off">
                                                        <input type="hidden" name="product_attribute[{{$keyAttr}}][attribute_id]" value="{{$attribute->{1}->attribute_id}}">
                                                    </td> 
                                                    <td class="text-left">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <img src="{{env('OC_BASE_URL')}}/admin/language/en-gb/en-gb.png" title="English">
                                                            </span>
                                                            <input type="hidden" name="product_attribute[{{$keyAttr}}][product_attribute_description][1][language_id]" value="1">
                                                            <textarea name="product_attribute[{{$keyAttr}}][product_attribute_description][1][text]" rows="5" placeholder="Text" class="form-control">{{$attribute->{1}->text}}</textarea>
                                                        </div>
                                                    </td> 
                                                    <td class="text-left">
                                                        <button type="button" onclick="$('#attribute-row{{$keyAttr}}').remove();" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="Remove"><i class="fa fa-minus-circle"></i></button>
                                                    </td>
                                                </tr>
                                                @php($keyAttr++)
                                            @endforeach
                                        @endif
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="2"></td>
                                            <td class="text-left"><button type="button" onclick="addAttribute();" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Add Attribute"><i class="fa fa-plus-circle"></i></button></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <!-- <div class="tab-pane" id="tab-option">
                            <div class="row">
                                <div class="col-sm-2">
                                    <ul class="nav nav-pills nav-stacked" id="option">
                                        <li>
                                            <input type="text" name="option" value="" placeholder="Option" id="input-option" class="form-control" autocomplete="off">
                                            <ul class="dropdown-menu"></ul>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-sm-10">
                                    <div class="tab-content">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-recurring">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <td class="text-left">Recurring Profile</td>
                                            <td class="text-left">Customer Group</td>
                                            <td class="text-left"></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="2"></td>
                                            <td class="text-left"><button type="button" onclick="addRecurring()" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Add Recurring"><i class="fa fa-plus-circle"></i></button></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div> -->
                        <!-- <div class="tab-pane" id="tab-subscribe">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <td>Allow Single Sale</td>
                                            <td>
                                                <input type="checkbox" name="single_sale" value="1">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Choose Payment Date<span data-toggle="tooltip" title="" data-original-title="Set to Yes to allow your customers to choose the day of the month to be billed.  This can ONLY be used with monthly subscriptions.  If you are using day, week, semi-month, year, or specific day for your subscription, this option will not be available!"></span></td>
                                            <td>
                                                <input type="radio" name="choose_payment_date" value="1">&nbsp;Yes&nbsp;&nbsp;
                                                <input type="radio" name="choose_payment_date" value="0" checked="checked">&nbsp;No                                                         
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <td class="left">Subscription</td>
                                                <td class="center">Price</td>
                                                <td class="left"></td>
                                            </tr>
                                        </thead>
                                        <tbody id="subscriptions">
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2"></td>
                                                <td class="left"><a onclick="addSubscription()" class="btn btn-primary">Add Subscription</a></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div> -->
                        <div class="tab-pane" id="tab-discount">
                            <div class="table-responsive">
                                <table id="discount" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <td class="text-left">Customer Group</td>
                                            <td class="text-right">Quantity</td>
                                            <td class="text-right">Priority</td>
                                            <td class="text-right">Price</td>
                                            <td class="text-left">Date Start</td>
                                            <td class="text-left">Date End</td>
                                            <td></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($product->discounts) > 0)
                                            @foreach($product->discounts as $key => $discount)
                                                <tr id="discount-row{{$key}}">
                                                    <td class="text-left">
                                                        <select name="product_discount[{{$key}}][customer_group_id]" class="form-control">
                                                            <option value="4" {{($discount->customer_group_id == 4) ? 'selected' : ''}}>Corporate</option>
                                                            <option value="1" {{($discount->customer_group_id == 1) ? 'selected' : ''}}>Default</option>
                                                            <option value="2" {{($discount->customer_group_id == 2) ? 'selected' : ''}}>Partner</option>
                                                        </select>
                                                    </td>
                                                    <td class="text-right">
                                                        <input type="text" name="product_discount[{{$key}}][quantity]" value="{{$discount->quantity}}" placeholder="Quantity" class="form-control">
                                                    </td>
                                                    <td class="text-right">
                                                        <input type="text" name="product_discount[{{$key}}][priority]" value="{{$discount->priority}}" placeholder="Priority" class="form-control">
                                                    </td>
                                                    <td class="text-right">
                                                        <input type="text" name="product_discount[{{$key}}][price]" value="{{$discount->price}}" placeholder="Price" class="form-control">
                                                    </td>
                                                    <td class="text-left" style="width: 20%;">
                                                        <div class="input-group date">
                                                            <input type="text" name="product_discount[{{$key}}][date_start]" value="{{$discount->date_start}}" placeholder="Date Start" data-date-format="YYYY-MM-DD" class="form-control">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td class="text-left" style="width: 20%;">
                                                        <div class="input-group date">
                                                            <input type="text" name="product_discount[{{$key}}][date_end]" value="{{$discount->date_end}}" placeholder="Date End" data-date-format="YYYY-MM-DD" class="form-control">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td class="text-left">
                                                        <button type="button" onclick="$('#discount-row{{$key}}').remove();" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="Remove"><i class="fa fa-minus-circle"></i></button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="6"></td>
                                            <td class="text-left"><button type="button" onclick="addDiscount();" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Add Discount"><i class="fa fa-plus-circle"></i></button></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-special">
                            <div class="table-responsive">
                                <table id="special" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <td class="text-left">Customer Group</td>
                                            <td class="text-right">Priority</td>
                                            <td class="text-right">Price</td>
                                            <td class="text-left">Date Start</td>
                                            <td class="text-left">Date End</td>
                                            <td></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($product->special) > 0)
                                            @foreach($product->special as $key => $special)
                                                <tr id="special-row{{$key}}">
                                                    <td class="text-left">
                                                        <select name="product_special[{{$key}}][customer_group_id]" class="form-control">
                                                            @foreach($customerGroups as $group)
                                                                <option value="{{$group['customer_group_id']}}" {{($special->customer_group_id == $group['customer_group_id']) ? 'selected' : '' }}>{{$group['name']}}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td class="text-right">
                                                        <input type="text" name="product_special[{{$key}}][priority]" value="{{$special->priority}}" placeholder="Priority" class="form-control">
                                                    </td>
                                                    <td class="text-right">
                                                        <input type="text" name="product_special[{{$key}}][price]" value="{{(int)$special->price}}" placeholder="Price" class="form-control">
                                                    </td>
                                                    <td class="text-left" style="width: 20%;">
                                                        <div class="input-group date">
                                                            <input type="text" name="product_special[{{$key}}][date_start]" value="{{$special->date_start}}" placeholder="Date Start" data-date-format="YYYY-MM-DD" class="form-control">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td class="text-left" style="width: 20%;">
                                                        <div class="input-group date">
                                                            <input type="text" name="product_special[{{$key}}][date_end]" value="{{$special->date_end}}" placeholder="Date End" data-date-format="YYYY-MM-DD" class="form-control">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td class="text-left">
                                                        <button type="button" onclick="$('#special-row{{$key}}').remove();" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="Remove"><i class="fa fa-minus-circle"></i></button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="5"></td>
                                            <td class="text-left"><button type="button" onclick="addSpecial();" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Add Special"><i class="fa fa-plus-circle"></i></button></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-image">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <td class="text-left">Image</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-left"><a href="javascript:;" onclick="$('.image-primary').click();" id="thumb-image" class="img-thumbnail"><img id="primary-image" style="width: 100px;height: 100px;" src="{{(!empty($product->image)) ? $product->image : env('APP_SERVER_DOMAIN').'image/cache/no_image-100x100.png'}}" alt="" title="" data-placeholder="{{(!empty($product->image)) ? $product->image : env('APP_SERVER_DOMAIN').'image/cache/no_image-100x100.png'}}" class="img-responsive"></a><input type="hidden" name="image" value="" id="input-image"><input type="file" name="image_primary" class="image-primary" style="display: none;"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="table-responsive">
                                <table id="images" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <td class="text-left">Additional Images</td>
                                            <td class="text-right">Sort Order</td>
                                            <td></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($product->images) > 0)
                                            @foreach($product->images as $key => $image)
                                                <tr id="image-row{{$key}}">
                                                    <td class="text-left">
                                                        <a href="" id="thumb-image0" data-toggle="image" class="img-thumbnail">
                                                            <img src="{{(!empty($image)) ? $image : env('APP_SERVER_DOMAIN').'image/cache/catalog/products/36-100x100.jpg'}}" alt="" title="" data-placeholder="{{(!empty($image)) ? $image : env('APP_SERVER_DOMAIN').'image/cache/catalog/products/36-100x100.jpg'}}">
                                                        </a>
                                                        <input type="hidden" name="product_image[{{$key}}][image]" value="{{(!empty($image)) ? $image : env('APP_SERVER_DOMAIN').'image/cache/catalog/products/36-100x100.jpg'}}" id="input-image{{$key}}">
                                                    </td>
                                                    <td class="text-right">
                                                        <input type="text" name="product_image[{{$key}}][sort_order]" value="{{$key}}" placeholder="Sort Order" class="form-control">
                                                    </td>
                                                    <td class="text-left">
                                                        <button type="button" onclick="$('#image-row{{$key}}').remove();" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="Remove"><i class="fa fa-minus-circle"></i></button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="2"></td>
                                            <td class="text-left"><button type="button" onclick="addImage();" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Add Image"><i class="fa fa-plus-circle"></i></button></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <!-- <div class="tab-pane" id="tab-reward">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-points"><span data-toggle="tooltip" title="" data-original-title="Number of points needed to buy this item. If you don't want this product to be purchased with points leave as 0.">Points</span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="points" value="" placeholder="Points" id="input-points" class="form-control">
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <td class="text-left">Customer Group</td>
                                            <td class="text-right">Reward Points</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-left">Default</td>
                                            <td class="text-right"><input type="text" name="product_reward[1][points]" value="" class="form-control"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Marketing</td>
                                            <td class="text-right"><input type="text" name="product_reward[4][points]" value="" class="form-control"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Partner</td>
                                            <td class="text-right"><input type="text" name="product_reward[2][points]" value="" class="form-control"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-design">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <td class="text-left">Stores</td>
                                            <td class="text-left">Layout Override</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-left">Default</td>
                                            <td class="text-left">
                                                <select name="product_layout[0]" class="form-control">
                                                    <option value=""></option>
                                                    <option value="6">Account</option>
                                                    <option value="10">Affiliate</option>
                                                    <option value="3">Category</option>
                                                    <option value="7">Checkout</option>
                                                    <option value="12">Compare</option>
                                                    <option value="8">Contact</option>
                                                    <option value="4">Default</option>
                                                    <option value="1">Home</option>
                                                    <option value="11">Information</option>
                                                    <option value="14">Journal Blog</option>
                                                    <option value="15">Journal Blog Post</option>
                                                    <option value="5">Manufacturer</option>
                                                    <option value="2">Product</option>
                                                    <option value="13">Search</option>
                                                    <option value="9">Sitemap</option>
                                                </select>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div> -->
                    </div>
                    <div class="row">
                        <div class="col-xs-12 m-top-10 text-right">
                            <a href="{{url('admin/product/list')}}" class="btn btn-default">
                               <i class="fa fa-times m-right-10"></i> Cancel
                            </a>
                            
                            <button type="button" form="form-product" data-toggle="tooltip" title="" class="btn btn-primary form-submit-product" data-original-title="Save">
                                <i class="fa fa-check m-right-10"></i> Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
    $('#example').DataTable();
} );
var opencartUrl = "{{env('OC_BASE_URL')}}";
var baseUrl = "{{env('APP_URL')}}";
</script>

<script type="text/javascript" src="{{asset('js/summernote.js')}}"></script>
<script src="{{asset('js/moment.js')}}" type="text/javascript"></script>
<script src="{{asset('js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script src="{{ env('APP_SERVER_DOMAIN') }}admin/view/javascript/common.js"></script>
@include('admin.master.users.business_partners.product.js.product_js')
@endsection
