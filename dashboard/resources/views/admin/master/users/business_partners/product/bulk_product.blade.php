@extends('layouts.admin_template')

@section('title', 'Bulk Products')
@section('page_title', 'Bulk Products')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-shopping-bag m-right-10"></i> Product</li>
        <li>Bulk Products</li>
    </ol>
@endsection

@section('content')

        <div class="box">
            <div class="box-body">
            {!! Form::open(['route' =>'admin.business-partner.bulk-product.store','class' =>'form-horizontal bulk-product','id' => 'form-set', 'style' => "padding-top:25px",'enctype' => 'multipart/form-data']) !!}
                <div class="form-group {{ $errors->has('bulk_product') ? 'has-error' : ''}}">
                    <label class="col-sm-2 control-label">FILE</label>
                    <div class="col-sm-5">
                        <input type="file" name="bulk_product" class="form-control" accept=".csv">
                        {!! $errors->first('bulk_product', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-5">
                        <a href="{{route('admin.business-partner.bulk-product.sample-download')}}" class="btn btn-warning">
                            <i class="fa fa-download m-right-10"></i>Download Sample
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 m-top-10 text-right">
                        <a href="{{url('admin/product/list')}}" class="btn btn-default">
                            <i class="fa fa-times m-right-10"></i> Cancel</a>
                        <button type="submit" class="btn btn-primary" id="btn-submit">
                            <i class="fa fa-check m-right-10"></i> Submit
                        </button>
                    </div>
                </div>
            {!! Form::close() !!}
            </div>
        </div>

@endsection
@section('scripts')
<script type="text/javascript">
</script>
@endsection
