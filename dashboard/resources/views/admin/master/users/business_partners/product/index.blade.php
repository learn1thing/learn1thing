@extends('layouts.admin_template')

@section('title', 'Link Product To Course')
@section('page_title', 'Link Product To Course')
<!-- @section('page_description', 'learn1thing Product') -->

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-shopping-bag m-right-10"></i> Product</li>
        <li>Link Product To Course</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
            @if(Session::has('flash_message'))
                <div class="alert alert-success"><em> {!! session('flash_message') !!}</em></div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-error"><em> {!! session('error') !!}</em></div>
            @endif
            <span></span>
            <h5><b>LINK PRODUCT TO COURSE :</b></h5>
            {!! Form::open(['route' =>'admin.business-partner.product.set','class' =>'set-product','id' => 'form-set', 'style' => "padding-top:25px"]) !!}
                <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <h5><b> Product List : </b></h5>
                        <select class="form-control select2-product" id="product" name="product" required="required">
                          <option></option>
                            @foreach($products as $value)
                                <option value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                            @endforeach                  
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <h5><b> <sup>*</sup>Course List : </b></h5>
                        <select class="form-control select2-course" id="course" name="course[]" multiple="multiple" required="required">
                          <option></option>
                            @foreach($course as $value)
                                <option value="{{ $value->shortname }}">{{ $value->course_name }}</option>
                            @endforeach
                        </select>
                      </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <button type="submit" class="pull-right btn btn-primary setProduct">
                           <i class="fa fa-check m-right-10"></i> Link Product to Course
                        </button>
                      </div>
                    </div>
                </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-body">
                <span></span>
                <h5><b>PRODUCT LINKED WITH COURSE :</b></h5>
                <br>
                <div class="table-responsive">
                  {!! $dataTable->table(['class' => 'datatable table table-striped', 'cellspacing'=>"0", 'width'=>"100%"]) !!}
                </div>
            </div>
        </div>
    </div>
</div>
@include('partials.delete-modal')
@endsection
@section('scripts')
{!! $dataTable->scripts() !!}
<script type="text/javascript">

    // $(document).ready(function() {
    //     var table = $('#set-product-table').DataTable();
    //     $('#btn-submit-set').on( 'click', function (e) {
    //         var product = $('#product').val();
    //         var course =  (($('#course').val() !== null) ? $('#course').val() : '<i>Not Set</i>');
    //         e.preventDefault();
    //         if(document.forms['form-set'].product.value === "" )
    //           {
    //             // alert("Product and Course cannot be empty ");
    //             toastAlert('Product and Course cannot be empty','warning');
    //             return false;
    //           }
    //           var rows = [
    //                 product,
    //                 course,
    //                 '<i>Not Set</i>',
    //                 '<button Onclick="ConfirmDelete()">Delete</button>'
    //             ];
    //         table.row.add( rows ).draw( true );
     
    //     } );
     
        
    // });

    // function ConfirmDelete()
    // {
    //   var x = confirm("Are you sure you want to delete this set ?");
    //   if (x)
    //       location.reload();
    //   else
    //     return false;
    // }

     $(document).on('click','.setProduct',function(){
        $('#set-product-table').DataTable();
    });

    $(".select2-product").select2({
            'placeholder': 'Select Product',
    });
    $(".select2-course").select2({
            'placeholder': 'Select Course',
    });
    $(".select2-profiler").select2({
            'placeholder': 'Select Profiler',
            'width': 'resolve'
    });
</script>

<script type="text/javascript">
    $('div.alert').delay(5000).slideUp(300);
</script>
@endsection
