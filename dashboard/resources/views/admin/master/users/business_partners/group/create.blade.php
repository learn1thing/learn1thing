
@extends('layouts.admin_template')

@section('title', 'Business Partner Role List')
@section('page_title', 'Business Partner Role List')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.business-partner.group-user') }}">
                <i class="fa fa-users m-right-10"></i> Business Partner Role List
            </a>
        </li>
        <li>Add</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
            @if(Session::has('flash_message'))
                <div class="alert alert-success"><em> {!! session('flash_message') !!}</em></div>
            @endif
            
            {!! Form::open(['route' =>'admin.business-partner.store.group-user','class' =>'form-horizontal','id' => 'form-user']) !!}
            @include('admin.master.users.business_partners.group._form')
            <div class="row">
                <div class="col-xs-12 m-top-10 text-right">
                    <a href="{{url('admin/management-group/group-user')}}" class="btn btn-default">
                       <i class="fa fa-times m-right-10"></i> Cancel
                    </a>
                
                    <button type="submit" class="btn btn-primary" id="btn-submit">
                        <i class="fa fa-check m-right-10"></i> Save
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $('div.alert').delay(5000).slideUp(300);
</script>
@endsection
