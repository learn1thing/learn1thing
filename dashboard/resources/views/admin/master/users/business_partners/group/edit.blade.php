
@extends('layouts.admin_template')

@section('title', 'Business Partner Role List')
@section('page_title', 'Business Partner Role List')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.business-partner.group-user') }}">
                <i class="fa fa-users m-right-10"></i> Business Partner Role List
            </a>
        </li>
        <li>Edit</li>
    </ol>
@endsection

@section('content')
    {!! Form::model($data, [
            'route'     =>['admin.business-partner.store.group-user.update', $data->id],
            'method'    => 'PATCH',
            'class' =>  'form-horizontal',
            'id'    =>  'form-course',
        ]) !!}
        <div class="box">
            <div class="box-body">
                @include('coach.course._form')
                <div class="row">
                    <div class="col-xs-12 m-top-10 text-right">
                        <a href="{{url('admin/management-group/group-user')}}" class="btn btn-default">
                           <i class="fa fa-times m-right-10"></i> Cancel
                        </a>
                    
                        <button type="submit" class="btn btn-primary" id="btn-submit">
                            <i class="fa fa-check m-right-10"></i> Save
                        </button>
                    </div>
                </div>       
            </div>
        </div>
    {!! Form::close() !!}
@endsection
@section('scripts')
@endsection
