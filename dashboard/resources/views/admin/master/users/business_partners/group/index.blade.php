@extends('layouts.admin_template')

@section('title', 'Business Partner Role List')
@section('page_title', 'Business Partner Role List')
@section('page_description', 'learn1thing')

@section('header')
<style type="text/css">
    .c-fld-tooltip-hint__cue {
        font-size: 14px;
        cursor: help;
        /*margin-left: 6px;
        line-height: 1.4;
        cursor: help;
        -webkit-transition: color .2s ease-in-out;
        -o-transition: color .2s ease-in-out;
        transition: color .2s ease-in-out;
        vertical-align: middle;
        color: #bbb;*/
    }
    .c-icon {
        font-family: "bl_icons_v3" !important;
        speak: none;
        font-style: normal;
        font-weight: normal;
        font-variant: normal;
        text-transform: none;
        line-height: 1;
        vertical-align: middle;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }
    .c-fld-tooltip-hint__cue:hover {
        color: #3c8dbc;
    }
    .c-icon--help:before {
        content: "\f059";
        font-family: FontAwesome;
        font-style: normal;
        font-weight: normal;
        text-decoration: inherit;
    }
    /* Tooltip */
    .tooltip > .tooltip-inner {
        padding: 15px;
        max-width: 100%;
    }
</style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.business-partner.group-user') }}">
                <i class="fa fa-users m-right-10"></i> Business Partner Role List
            </a>
        </li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-3 pull-right">
        <a href="{{ route('admin.business-partner.create-group-user')}}" class="btn btn-primary pull-right" id="btn-submit">
           <i class="fa fa-plus m-right-10"></i> Add Role
        </a>
    </div>
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
            @if(Session::has('flash_message'))
                <div class="alert alert-success"><em> {!! session('flash_message') !!}</em></div>
            @endif
            <div class="table-responsive">
              {!! $dataTable->table(['class' => 'datatable table table-striped', 'cellspacing'=>"0", 'width'=>"100%"]) !!}
            </div>
            </div>
        </div>
    </div>
</div>
@include('partials.delete-modal')
@endsection
@section('scripts')
{!! $dataTable->scripts() !!}
<script type="text/javascript">
    $('div.alert').delay(5000).slideUp(300);
</script>
@endsection
