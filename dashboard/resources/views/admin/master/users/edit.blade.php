@extends('layouts.app')

@section('title', trans('title.setting_user'))

@section('page_header')
    @parent
    <h1>
        {{ trans('title.setting_user') }}
        <small>{{ trans('label.edit') }}</small>
    </h1>
@endsection

@section('breadcrumbs')
    @parent
    <li><a href="#">{{ trans('title.setting') }}</a></li>
    <li><a href="{{ route('admin.user.index') }}">{{ trans('title.setting_user') }}</a></li>
    <li class="active">{{ trans('label.edit') }}</li>
@endsection

@section('content')
    {!! Form::model($user, [
            'route'     =>['admin.user.update', $user->id],
            'method'    => 'PATCH',
            'class' =>  'form-horizontal',
            'id'    =>  'form-user',
        ]) !!}
        <div class="box">
            <div class="box-body">
                @include('admin.users._form')  
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <a href="{{ route('admin.user.index') }}" class="btn btn-default">{{trans('label.cancel')}}</a>
                        <button type="submit" class="btn btn-primary" id="btn-submit">{{ trans('label.save') }}</button>
                    </div>
                </div>              
            </div>
        </div>
    </form>
@endsection
@section('custom_js')
<!-- Laravel Javascript Validation -->
<script type="text/javascript" src="{{ url('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! JsValidator::formRequest('App\Http\Requests\UsersRequest') !!}
@endsection