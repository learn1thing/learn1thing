
@extends('layouts.admin_template')

@section('title', 'Bulk Users')
@section('page_title', 'Bulk Users')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-users m-right-10"></i> Bulk User</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-body">
            {!! Form::open(['route' =>'admin.bulk-user.store','class' =>'form-horizontal','id' => 'form-user-csv','files' => true]) !!}
                <div class="form-group">
                    <!-- <div class="col-sm-"> -->
                        {!! Form::label('bulk_csv', trans('File :'), ['class' => 'col-sm-2 control-label']) !!}
                        <div class="col-sm-5">
                            <input type="file" class="form-control" accept=".csv" name="bulk_csv" required="required" />
                        </div>
                    <!-- </div> -->
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-5">
                        <a href="{{ route('admin.business-partner.download-sample')}}"><button type="button" class="btn btn-warning"><i class="fa fa-download m-right-10"></i> Download Sample</button></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 m-top-10 text-right">
                        <a href="{{route('admin.bulk-user.index')}}" class="btn btn-default">
                            <i class="fa fa-times m-right-10"></i> Cancel</a>
                        <button type="submit" class="btn btn-primary" id="btn-submit">
                            <i class="fa fa-check m-right-10"></i> Submit
                        </button>
                    </div>
                </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $('div.alert').delay(5000).slideUp(300);

    $(document).on('click','#btn-submit',function(e){
        e.preventDefault();
        $('form button').attr('disabled',true);
        $('form a').attr('disabled',true);
        $(this).html('Please wait...');

        $('#form-user-csv').submit();

        if ($('form .has-error').length > 0) {
            $('form button').removeAttr('disabled');
            $('form a').removeAttr('disabled');
            $(this).html('Save');
        } else {
            toastr.warning('Please wait, system is running background process!');
        }
    });
</script>
@endsection
