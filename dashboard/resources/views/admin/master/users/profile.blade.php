
@extends('layouts.admin_template')

@section('title', 'Profile')
@section('page_title', 'Profile')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-user m-right-10"></i> User</li>
        <li>Profile</li>
    </ol>
@endsection

@section('content')
<div class="row" style="margin-bottom: 5px;">
    <div class="col-xs-12 text-right">
        <!-- @if(@user_info()->upline)
            @if(@user_info()->upline->inRole('corporate-customer'))
                <a href="{{route('admin.user.link-account')}}" class="btn btn-primary">Link Account</a>
            @endif
        @endif

        @if(@user_info()->inRole('standard-customer') || @user_info()->inRole('corporate-customer') )
            <a href="{{route('admin.user.link-account')}}" class="btn btn-primary">Link Account</a>
        @endif -->
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
            @if(Session::has('flash_message'))
                <div class="alert alert-success"><em> {!! session('flash_message') !!}</em></div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-error"><em> {!! session('error') !!}</em></div>
            @endif
            {!! Form::open(['route' =>'admin.user.profile.update','id' => 'form-user','files'=> true]) !!}
            <div class="row">
                <div class="col-md-6">
                    <h4>Your Personal Details</h4><hr>
                    <div class="form-group" id="register-first_name">
                        <label class="control-label" for="first_name">{!! trans('label.required') !!}First Name</label>
                        <input type="text" name="first_name" class="form-control" id="first_name" placeholder="First name" value="{{ $user->first_name }}" required="">
                        <input type="hidden" name="id" class="form-control" value="{{ $user->id }}" required="">
                    </div>
                    <div class="form-group" id="register-last_name">
                        <label class="control-label" for="last_name">{!! trans('label.required') !!}Last Name</label>
                        <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Last name" value="{{ $user->last_name }}" required="">
                    </div>
                    <div class="form-group" id="register-email">
                        <label class="control-label" for="email">Email</label>
                        <input type="text" name="email" class="form-control" id="email" placeholder="Last name" value="{{ $user->email }}" readonly="">
                    </div>
                    <div class="form-group text-right">
                        <a href="{{route('admin.user.change-password')}}" class="btn btn-primary"><i class="fa fa-lock m-right-10"></i> Change Password</a>
                    </div>
                    <!-- <div class="form-group" id="register-email">
                        <label class="control-label" for="email">{!! trans('label.required') !!}Email</label>
                        <input type="hidden" name="email" class="form-control" id="email" placeholder="Email" value="{{ $user->email }}" required="">
                    </div> -->
                    <div class="form-group" id="register-image">
                        <label class="control-label" for="image">Image</label>
                        <input type="file" name="image" id="image" class="form-control">
                    </div>
                    <div class="form-group" id="register-image">
                        <img src="{{ url_to_image($user->image) }}" class="image-profile" width="50%">
                    </div>
                    @if ($user->related_type == 'App\Models\BusinessPartner')
                        <input type="hidden" name="customer_group_id" id="customer_group_id_2" value="2"> <!-- Partner -->
                        <div class="form-group" id="register-banner">
                            <label class="control-label" for="banner">Banner</label>
                            <input type="file" name="banner" id="banner" class="form-control">
                        </div>
                        <div class="form-group" id="register-banner">
                            <img src="{{ url_to_image(@$user->related->banner, 'banner') }}" class="image-banner" width="50%">
                        </div>
                    @endif

                    @if (!empty($user->related_type) AND ($user->related_type == 'App\Models\CorporateCustomer' || $user->related_type == 'App\Models\BusinessPartner'))
                        <div class="form-group" id="register-logo">
                            <label class="control-label" for="logo">Logo</label>
                            <input type="file" name="logo" id="logo" class="form-control">
                        </div>
                        <div class="form-group" id="register-logo">
                            <img src="{{ url_to_image(@$user->related->logo,'logo') }}" class="img-logo" width="50%">
                        </div>                    
                    @endif

                    <hr><h4>Newsletter</h4><hr>
                    
                    <div class="form-group" id="register-subscribe">
                        <label class="control-label" for="subscribe">Subscribe</label>
                        <label class="radio-inline">
                            <input type="radio" name="subscribe" id="subscribe1" value="1"> Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="subscribe" id="subscribe2" value="0" checked=""> No
                        </label>
                    </div>
                </div>
                <div class="col-md-6">
                    <h4>Your Address</h4><hr>
                    @if ($user->related_type == 'App\Models\CorporateCustomer')
                        <input type="hidden" name="customer_group_id" id="customer_group_id_3" value="4"> <!-- Corporate -->
                    @endif
                    @if ($user->related_type == 'App\Models\StandardCustomer')
                        <input type="hidden" name="customer_group_id" id="customer_group_id_1" value="1" checked=""> <!-- Standar -->
                    @endif
                    @if (!empty($user->related_type) AND ($user->related_type == 'App\Models\CorporateCustomer' || $user->related_type == 'App\Models\BusinessPartner'))
                        <div id="corporate-field">
                            <div class="form-group" id="register-company_name">
                                <label class="control-label" for="company_name">{!! trans('label.required') !!}Company Name</label>
                                <input type="text" name="company_name" class="form-control" id="company_name" placeholder="Company Name" value="{{ @$user->related->name }}" required>
                            </div>
                            <div class="form-group" id="register-company_address">
                                <label class="control-label" for="company_address">{!! trans('label.required') !!}Company Address</label>
                                <textarea name="company_address" class="form-control" id="company_address" placeholder="Company Address" required>{{ @$user->related->address }}</textarea>
                            </div>
                            <div class="form-group" id="register-company_register_no">
                                <label class="control-label" for="company_register_no">{!! trans('label.required') !!}Company Registration Number</label>
                                <input type="text" name="company_register_no" class="form-control" id="company_register_no" placeholder="Company Registration Number" value="{{ @$user->related->no_regis }}" required>
                            </div>
                           <!--  Editted by puw k5 on 13-12-2017
                           <div class="form-group" id="register-description">
                               <label class="control-label" for="address_2">Description</label>
                               <textarea name="description" class="form-control" id="description" placeholder="Address 2" required="">{{ @$user->related->description }}</textarea>
                           </div>  -->
                           <input type="hidden" name="description" id="description" value="{{ @$user->related->description }}">
                        </div>
                    @endif

                        <div class="form-group" id="register-address_1">
                            <label class="control-label" for="address_1">{!! trans('label.required') !!}Address 1</label>
                            <textarea name="address_1" class="form-control" id="address_1" placeholder="Address 1" required>{{ $user->address_1 }}</textarea>
                        </div>

                        <!--  Added by puw k5  -->
                        @if (!empty($user->related_type) AND ($user->related_type != 'App\Models\CorporateCustomer' ))
                            <div class="form-group" id="register-address_2">
                                <label class="control-label" for="address_2">Address 2</label>
                                <textarea name="address_2" class="form-control" id="address_2" placeholder="Address 2" required="">{{ $user->address_2 }}</textarea>
                            </div>
                        @endif

                        <!--  Added by rdw k5  -->
                        @if (!empty($user->related_type) AND ($user->related_type == 'App\Models\CorporateCustomer' ))
                            <div class="form-group" id="register-address_2">
                                <input type="hidden" name="address_2" id="address_2" value="{{ $user->address_2 }}">
                            </div>
                        @endif

                        <div class="form-group" id="register-city">
                            <label class="control-label" for="city">{!! trans('label.required') !!}City</label>
                            <input type="text" name="city" class="form-control" id="city" value="{{$user->city}}" placeholder="City" required="">
                        </div>
                        <div class="form-group" id="register-post_code">
                            <label class="control-label" for="post_code">{!! trans('label.required') !!}Postal Code</label>
                            <input type="text" name="post_code" class="form-control" id="post_code" value="{{ $user->post_code }}" placeholder="Postal Code" required="">
                        </div>
                        <div class="form-group" id="register-country">
                            <label class="control-label" for="country">{!! trans('label.required') !!}Country</label>
                            <select name="country" class="country form-control select2" onchange="countryId(this.value)">
                                <option></option>
                                @foreach ($countries as $country)
                                    <option value="{{ $country->country_id }}" {{ $country->country_id == $user->country ? 'selected' : ''}}>{{ $country->name }}</option>
                                @endforeach
                            </select>
                            <div id="country"></div>
                        </div>

                        
                        <!--  Added by puw k5  -->
                        @if (!empty($user->related_type) AND ($user->related_type != 'App\Models\CorporateCustomer'))
                            <div class="form-group" id="register-region">
                                <label class="control-label" for="region">Region / State</label>
                                <select name="region" class="form-control select2" id="region-by-country">
                                    <option></option>
                                    @foreach ($zones as $zone)
                                        <option value="{{ $zone->zone_id }}" {{ $zone->zone_id == $user->region ? 'selected' : ''}}>{{ $zone->name }}</option>
                                    @endforeach
                                </select>
                                <div id="region"></div>
                            </div>
                        @endif

                         <!--  Added by rdw k5  -->
                        @if (!empty($user->related_type) AND ($user->related_type == 'App\Models\CorporateCustomer' ))
                            <div class="form-group" id="register-address_2">
                                <input type="hidden" name="region" id="region" value="{{ $user->region }}">
                            </div>
                        @endif

                        @if(user_info()->inRole('corporate-customer'))
                            <hr><h4>Payment Method</h4>
                            <div class="form-group" id="register-payment_method">
                                <select class="form-control select2" name="payment_method">
                                    @if(count($payment_method) > 0)
                                        @foreach($payment_method as $key => $payment)
                                            <option value="{{$key}}" {{$user->payment_method_id == $key ? 'selected' : ''}}>{{$payment}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 m-top-10 text-right">
                    <a href="{{route('admin-dashboard')}}" class="btn btn-default">
                       <i class="fa fa-times m-right-10"></i> Cancel
                    </a>
                
                    <button type="submit" class="btn btn-primary" id="btn-submit">
                        <i class="fa fa-check m-right-10"></i> Save
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
    
</div>

<!-- modal confirm change payment method .start here -->
<div class="modal fade" id="modal-payment-method">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Change Payment Method</h4>
      </div>
      <div class="modal-body">
        <p class="ask-confirm">Are you sure for do this?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times m-right-10"></i> No</button>
        <button type="button" class="btn btn-primary" id="btn-confirm"><i class="fa fa-check m-right-10"></i> Yes</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- modal confirm change payment method .end here -->

@endsection
@section('scripts')
<!-- Laravel Javascript Validation -->
{!! Html::script('vendor/jsvalidation/js/jsvalidation.min.js') !!}
{!! JsValidator::formRequest('App\Http\Requests\Master\User\ProfileRequest', '#form-user'); !!}
<script type="text/javascript">
    $('div.alert').delay(5000).slideUp(300);
    var currentPayment = "{{user_info('payment_method_id')}}";
    var currentPaymentMethod = "{{@user_info('payment_method')->name}}";
    /*
    $("#button-submit").unbind('click').bind('click', function () {
        // update();              
    });

    function update()
    {
        var fd = new FormData();
        var file_data = $('#image').prop('files')[0];
        if(file_data != undefined){
            fd.append('image',file_data);
        }
        var other_data = $('#form-user').serializeArray();
        $.each(other_data,function(key,input){
            fd.append(input.name,input.value);
        });
        $('.notice').html('');
        $.ajax({
            url: "{{ route('admin.user.profile.update') }}",
            type: "POST",
            dataType: 'json',
            processData:false,
            contentType:false,
            data: fd,
            success: function (data) {
                console.log(data);
                $('.notice').html('<div class="alert alert-success"><em>'+data.message+'</em></div>');
            },
            error: function(response){
                if (response.status === 422) {
                    return false;
                    var data = response.responseJSON;
                    $.each(data,function(key,val){
                        $('<span class="text-danger tooltip-field"><span>'+val+'</span>').insertAfter($('#'+key));
                        $('.'+key).addClass('has-error');
                    });
                } else {
                    $('.notice').html('<div class="alert alert-danger"><em>'+response.responseJSON.message+'</em></div>');
                }
            }
        });
    }
    */
   
   $(".select2").select2({
        'placeholder': 'Select',
    });
   
   function countryId(country_id) {
        if(country_id) {
            $.ajax({
                type:'get',
                url:'/dashboard/country-detail/'+country_id,
                dataType: 'json',
                success:function(response) {
                    var data = response.data;
                    $('#region-by-country').html('');//.select2({data: null});
                    $('#region-by-country').append('<option></option>');
                    $.each(data,function(key, val){
                        $('#region-by-country').append('<option value="'+val.zone_id+'">'+val.name+'</option>');
                    });
                },
                error:function(data) {
                    console.log(data);
                }
            });
        } else {
            $('#region-by-country').html('');
            $('#region-by-country').append('<option></option>');
        }
    }

    function readURLAvatar(input,image) {
        if (input.files && input.files[0]) {

          var reader = new FileReader();

          reader.onload = function (e) {
            image.attr('src', e.target.result);
          }

          reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on('change',"#image",function(){
        var image = $('.image-profile');

        readURLAvatar(this,image);
    });

     $(document).on('change',"#banner",function(){
        var image = $('.image-banner');

        readURLAvatar(this,image);
    });

    $(document).on('change',"#logo",function(){
        var image = $('.img-logo');

        readURLAvatar(this,image);
    });

    $(document).on('change','.country',function(){
        getZone($('#region-by-country'),$(this).val());
    });

    $(document).on('click','#btn-submit',function(e) {
        e.preventDefault();
        @if(user_info()->inRole('corporate-customer'))
            var paymentMethod = $('select[name="payment_method"]').val();
            if (paymentMethod != currentPayment) {
                if (currentPaymentMethod.toLowerCase() == 'credit based system') {
                    $('.ask-confirm').html('Are you sure you want to change your payment method? Your credits will be reset.');
                } else {
                    $('.ask-confirm').html('Are you sure you want to change your payment method?');
                }
                $('#modal-payment-method').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            } else {
                $('#form-user').submit();
            }
        @else
            $('#form-user').submit();
        @endif
    });

    $(document).on('click','#btn-confirm',function() {
        $('#form-user').submit();
    });

</script>
@endsection
