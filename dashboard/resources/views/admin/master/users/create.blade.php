@extends('layouts.app')

@section('title', trans('title.setting_user'))

@section('page_header')
    @parent
    <h1>
        {{ trans('title.setting_user') }}
        <small>{{ trans('label.add') }}</small>
    </h1>
@endsection

@section('breadcrumbs')
    @parent
    <li><a href="#">{{ trans('title.setting') }}</a></li>
    <li><a href="{{ url('admin/setting/user') }}">{{ trans('title.setting_user') }}</a></li>
    <li class="active">{{ trans('label.add') }}</li>
@endsection

@section('content')
    {!! Form::open([
            'route' =>  'admin.user.store',
            'class' =>  'form-horizontal',
            'id'    =>  'form-user',
        ]) !!}
        <div class="box">
            <div class="box-body">
                @include('admin.master.users._form')  
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <a href="{{url('admin/setting/user')}}" class="btn btn-default">{{trans('label.cancel')}}</a>
                        <button type="submit" class="btn btn-primary" id="btn-submit">{{ trans('label.save') }}</button>
                    </div>
                </div>              
            </div>
        </div>
    </form>
@endsection
<!-- Laravel Javascript Validation -->
@section('custom_js')
<script type="text/javascript" src="{{ url('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! JsValidator::formRequest('App\Http\Requests\UsersRequest') !!}
@endsection