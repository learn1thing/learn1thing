
@extends('layouts.admin_template')

@section('title', 'Change Password')
@section('page_title', 'Change Password')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>User</li>
        <li>Profile</li>
        <li>Change Password</li>
    </ol>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
            {!! Form::open(['route' =>'admin.user.change-password.update','id' => 'form-user']) !!}
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('old_password') ? 'has-error' : ''}}">
                        <label class="control-label" for="old_password">{!! trans('label.required') !!}Old Password</label>
                        <input type="password" name="old_password" class="form-control" id="old_password" placeholder="Old Password" value="{{old('old_password')}}">
                        {!! $errors->first('old_password', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('new_password') ? 'has-error' : ''}}">
                        <label class="control-label" for="new_password">{!! trans('label.required') !!}New Password</label>
                        <input type="password" name="new_password" class="form-control" id="new_password" placeholder="Last name" value="{{old('new_password')}}">
                        {!! $errors->first('new_password', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('password_confirm') ? 'has-error' : ''}}">
                        <label class="control-label" for="password_confirm">{!! trans('label.required') !!}New Password Confirmation</label>
                        <input type="password" name="password_confirm" class="form-control" id="password_confirm" placeholder="Last name" value="{{old('password_confirm')}}">
                        {!! $errors->first('password_confirm', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 m-top-10 text-right">
                    <a href="{{route('admin.user.profile')}}" class="btn btn-default">
                       <i class="fa fa-times m-right-10"></i> Cancel
                    </a>
                
                    <button type="submit" class="btn btn-primary" id="btn-submit">
                        <i class="fa fa-check m-right-10"></i> Save
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
    
</div>
@endsection
@section('scripts')
<script type="text/javascript">
</script>
@endsection
