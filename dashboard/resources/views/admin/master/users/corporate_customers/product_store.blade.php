<div class="row">
@foreach($query as $value)
    <div class="col-md-3">
        <div class="box box-widget widget-user box-product">
          @if(user_info()->inRole('corporate-customer'))
            <div class="ribbon-wrapper">
              @if($value['mini_store_status'] == 1)
                <div class="ribbon">Enabled</div>
              @else
                <div class="ribbon">Disabled</div>
              @endif
            </div>
          @endif
          <div class="img-product" style="background-image:url('{{ get_file($value['image'],'thumbnail') }}'); background-repeat: no-repeat; background-position: center;">
            <div class="box-header" style="height: 188px;"></div>
          </div>
          <div class="box-body" style="margin-top: 25px">
              <div class="row">
                <div class="col-sm-12">
                  <a href="#" data-toggle="tooltip" title="{{ $value['name'] }}"> <h6 class="widget-user-username" style="font-size: 18px !important" >{{ strlen($value['name']) > 20 ? substr($value['name'],0, 23)."..." : $value['name'] }}</h6> </a>
                </div>
              </div>
              <div class="row" style="margin-top: 20px">
                <div class="col-sm-6 border-right">
                  <div class="description-block">
                    <input type="hidden" name="product_id" value="{{ $value['product_code'] }}">
                    <h5 class="description-header">{{ $value['stock'] }}</h5>
                    <span class="description-text">STOCK</span>
                    
                  </div>
                </div>
                <div class="col-sm-6 border-right">
                  <div class="description-block">
                    @if(user_info()->inRole('corporate-customer') && $value['mini_store_status'] == 1)
                        <center><button type="button"  class="btn btn-danger disable-prod" data-toggle="modal" href="#modal-form-disable" data-id="{{ $value['product_code'] }}"  data-detail-id="$value['order_detail_id']">Disable</button></center>
                    @elseif(user_info()->inRole('corporate-customer') && $value['mini_store_status'] == 0)
                        <center><button type="button" class="btn btn-primary enable-prod" data-toggle="modal" href="#modal-form-enable" data-id="{{ $value['product_code'] }}" >Enable</button></center>
                    @else
                        <center>
                          @if($value['stock'] == 0)
                            <button type="button" class="btn btn-warning request-prod" data-toggle="modal" href="#modal-form-request" data-id="{{ $value['product_code'] }}" data-detail-id="{{ $value['order_detail_id'] }}" disabled>Request</button>
                          @else
                            @if(orderDetailProductOrder(user_info('id'), $value['product_code'], $value['order_detail_id']))
                              <button type="button" class="btn btn-default" style="cursor: default;">Requested</button>
                            @else
                              <button type="button" class="btn btn-warning request-prod" data-toggle="modal" href="#modal-form-request" data-id="{{ $value['product_code'] }}" data-detail-id="{{ $value['order_detail_id'] }}">Request</button>
                            @endif
                          @endif
                        </center>
                    @endif
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
@endforeach
</div>
<div class="row">
    @foreach($queryProductAssign as $value)
        <div class="col-md-3">
            <div class="box box-widget widget-user box-product">
              <div class="img-product" style="background-image:url('{{ get_file($value['image'],'thumbnail') }}'); background-repeat: no-repeat; background-position: center;">
                <div class="box-header" style="height: 188px;"></div>
              </div>
              <div class="box-body" style="margin-top: 25px">
                  <div class="row">
                    <div class="col-sm-12">
                      <a href="#" data-toggle="tooltip" title="{{ $value['name'] }}"> <h6 class="widget-user-username" style="font-size: 18px !important" >{{ strlen($value['name']) > 20 ? substr($value['name'],0, 28)."..." : $value['name'] }}</h6> </a>
                    </div>
                  </div>
                  <div class="row" style="margin-top: 20px">
                    <div class="col-sm-6 border-right">
                      <div class="description-block">
                        <input type="hidden" name="product_id" value="{{ $value['product_code'] }}">
                        <h5 class="description-header">{{ $value['stock'] }}</h5>
                        <span class="description-text">FROM BP</span>
                        
                      </div>
                    </div>
                    <div class="col-sm-6 border-right">
                      <div class="description-block">
                        @if(user_info()->inRole('corporate-customer') && $value['mini_store_status'] == 1)
                            <center><button type="button"  class="btn btn-danger disable-prod" data-toggle="modal" href="#modal-form-disable" data-id="{{ $value['product_code'] }}">Disable</button></center>
                        @elseif(user_info()->inRole('corporate-customer') && $value['mini_store_status'] == 0)
                            <center><button type="button" class="btn btn-primary enable-prod" data-toggle="modal" href="#modal-form-enable" data-id="{{ $value['product_code'] }}" >Enable</button></center>
                        @else
                            <center><button type="button" class="btn btn-warning request-prod" data-toggle="modal" href="#modal-form-request" data-id="{{ $value['product_code'] }}">Request</button></center>
                        @endif
                      </div>
                    </div>
                  </div>
              </div>
            </div>
        </div>
    @endforeach
</div>
{!! $query->render() !!}
<!-- Modal -->
<div id="modal-form-request" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request Product</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure to request this product ?</p>
      {!! Form::open(['route' =>'request-product','class' =>'','id' => 'form-request-product']) !!}
        <input type="hidden" name="prod_id" id="prod_id" value=""/>
        <input type="hidden" name="user_id" id="user_id" value="{{ user_info('id') }}"/>
        <input type="hidden" name="order_detail_id" id="order_detail_id" value=""/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
        <button type="submit" class="btn btn-primary">Yes</button>
      </div>
      {!! Form::close() !!}
    </div>

  </div>
</div>
<div id="modal-form-enable" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Enble Product</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure to enable this product ?</p>
      {!! Form::open(['route' =>'enable-product','class' =>'','id' => 'form-enable-product']) !!}
        <input type="hidden" name="prod_id" id="prod_id" value=""/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
        <button type="submit" class="btn btn-primary">Yes</button>
      </div>
      {!! Form::close() !!}
    </div>

  </div>
</div>

<div id="modal-form-disable" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Disable Product</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure to disable this product ?</p>
      {!! Form::open(['route' =>'disable-product','class' =>'','id' => 'form-enable-product']) !!}
        <input type="hidden" name="prod_id" id="prod_id_disable" value=""/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
        <button type="submit" class="btn btn-primary">Yes</button>
      </div>
      {!! Form::close() !!}
    </div>

  </div>
</div>

<script type="text/javascript">
  $(document).on("click", ".enable-prod", function () {
     var id = $(this).data('id');
     $(".modal-body #prod_id").val( id );
     
  });

  $(document).on("click", ".disable-prod", function () {
     var id = $(this).data('id');
     $(".modal-body #prod_id_disable").val( id );
     
  });

  $(document).on("click", ".request-prod", function () {
     var id = $(this).data('id');
     var idDetail = $(this).data('detail-id');
     $(".modal-body #prod_id").val( id );
     $(".modal-body #order_detail_id").val( idDetail );
     
  });
</script>