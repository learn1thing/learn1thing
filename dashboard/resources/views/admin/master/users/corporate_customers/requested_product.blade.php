@extends('layouts.admin_template')

@section('title', 'Requested Product')
@section('page_title', 'Requested Product')
@section('page_description', 'Requested')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-link m-right-10"></i> Product</li>
        <li>Requested Product</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
              <div class="table-responsive">
                 {!! $dataTable->table(['class' => 'datatable table table-striped', 'cellspacing'=>"0", 'width'=>"100%"]) !!}
              </div>
            </div>
        </div>
    </div>
</div>
@include('partials.delete-modal')
@endsection
@section('scripts')
{!! $dataTable->scripts() !!}
<script type="text/javascript">
  $(document).on('click','.app-dec',function(){
    $.ajax({
      url: $(this).data('href'),
      method: 'post',
      dataType: 'json',
      beforeSend: function(data) {
        toastAlert('Please wait, system is running background process!','warning');
        $('.datatable a').addClass('processing');
        $('.datatable a').removeAttr('id');
        $('.datatable a').attr('disabled','disabled');
      },
      success:function(data) {
        if (data.status.success) {
          toastr.success(data.status.message);
        } else {
          toastr.error(data.status.message);
        }
        // switch(data.type){
        //   case 'info':
        //   toastr.info(data.message);
        //   break;
        //   case 'warning':
        //   toastr.warning(data.message);
        //   break;
        //   case 'success':
        //   toastr.success(data.message);
        //   break;
        //   case 'error':
        //   toastr.error(data.message);
        //   break;
        // }

        $('.datatable').DataTable().ajax.reload();
      },
      error: function(data) {
        toastr.error('Something went wrong');
        $('.datatable').DataTable().ajax.reload();
      }
    });
  });

  $(document).on('click', '.detailData', function () {
      var table = $('.datatable').DataTable();
      var tr = $(this).closest('tr');
      var row = table.row( tr );
      
      if ( row.child.isShown() ) {
          // This row is already open - close it
          row.child.hide();
          tr.removeClass('shown');
      }
      else {
          // Open this row
          $.ajax({
              url: $(this).data('href'),
              method: "get",
              dataType: "json",
              success: function(data) {
                  if (data.success) {
                      row.child( childCategoryTemplate(data.data) ).show();
                      tr.addClass('shown');
                  }
              }
          });
      }
  });
</script>
@endsection
