
@extends('layouts.admin_template')

@section('title', 'Edit Corporate Customer')
@section('page_title', 'Edit Corporate Customer')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>User</li>
        <li>Corporate Customer</li>
        <li>Edit</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
            @if(Session::has('flash_message'))
                <div class="alert alert-success"><em> {!! session('flash_message') !!}</em></div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-error"><em> {!! session('error') !!}</em></div>
            @endif
            {!! Form::model($user,['route' =>['admin.corporate-customer.update',$user->related_id],'class' =>'','id' => 'form-corporate-customer','method' => 'PATCH']) !!}
            @include('admin.master.users.corporate_customers._form')
            <div class="row">
                <div class="col-xs-12 m-top-10 text-right">
                    <a href="{{route('admin.corporate-customer.index')}}" class="btn btn-default">
                       <i class="fa fa-times m-right-10"></i> Cancel
                    </a>
                
                    <button type="submit" class="btn btn-primary" id="btn-submit">
                        <i class="fa fa-check m-right-10"></i> Save
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
    
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $('div.alert').delay(5000).slideUp(300);

    $(document).on('change','#country',function(){
        getZone($('#region'),$(this).val());
    });
</script>
@endsection
