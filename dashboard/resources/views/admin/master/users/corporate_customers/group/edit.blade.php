@extends('layouts.admin_template')

@section('title', 'Corporate Role List')
@section('page_title', 'Corporate Role List')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-users m-right-10"></i> Corporate Role List</li>
        <li>Edit</li>
    </ol>
@endsection

@section('content')
    {!! Form::model($data, [
            'route'     =>['admin.corporate-customer.store.group-user.update', $data->id],
            'method'    => 'PATCH',
            'class' =>  'form-horizontal',
            'id'    =>  'form-course',
        ]) !!}
        <div class="box">
            <div class="box-body">
                @include('admin.master.users.corporate_customers.group._form')
                <div class="row">
                    <div class="col-xs-12 m-top-10 text-right">
                        <a href="{{ route('admin.corporate-customer.group-user') }}" class="btn btn-default">
                           <i class="fa fa-times m-right-10"></i> {{trans('label.cancel')}}
                        </a>
                    
                        <button type="submit" class="btn btn-primary" id="btn-submit">
                            <i class="fa fa-check m-right-10"></i> {{trans('label.save')}}
                        </button>
                    </div>
                </div>             
            </div>
        </div>
    {!! Form::close() !!}
@endsection
@section('scripts')
@endsection
