@extends('layouts.admin_template')

@section('title', 'Requested Product Detail')
@section('page_title', 'Requested Product Detail')
@section('page_description', 'Requested')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-link m-right-10"></i> Product</li>
        <li><a href="{{route('admin.corporate-customer.request-product-list')}}">Requested Product</a></li>
        <li>Detail</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="page-header">
                          <i class="fa fa-cubes"></i> {{$product->product_description->{1}->name}}
                        </h2>
                    </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    About Product
                    <address>
                      {!! $product->product_description->{1}->description !!}
                    </address>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-12">

                    <!--Link Product<br> -->
                    <table class="table table-striped" width="100%">
                      <!-- <thead>
                      <tr>
                        <th>Profiler</th>
                        <th>Course</th>
                        <th>BBB</th>
                        <th>Set Schedule</th>
                      </tr>
                      </thead> -->
                      <tbody>
                      @if(@$order_product->link)
                        <tr>
                          {{-- <td width="30%">Profile Link</td>
                          <td width="30%">
                            @foreach($order_product->link as $key => $val)
                              <a href="{{$val->link}}" target="_blank">{{$val->link}}</a><br/>
                            @endforeach
                          </td> --}}
                          @if (!@$order_product->bbb_link)
                            <td>Course</td>
                            <td>
                              <a href="{{ $product_set->course_url }}" target="_blank">{{$product_set->course_url}}</a><br>
                            </td>
                          @endif
                        </tr>
                      @else

                        @if(@$product_set->course_url)
                          <tr>
                            <td>Course</td>
                            <td>
                                <a href="{{ $product_set->course_url }}" target="_blank">{{$product_set->course_url}}</a><br>
                                @if(count($schedules) > 0)
                                  <p><b>Schedule Time:</b></p>
                                  @foreach($schedules as $key => $schedule)
                                    <p>{{($key+1)}}. {{date('l, d F Y',strtotime($schedule->start_date)).' '.$schedule->start_time}} - {{date('l, d F Y',strtotime($schedule->end_date)).' '.$schedule->end_time}}</p>
                                  @endforeach
                                @endif
                            </td>
                          </tr>
                        @endif

                      @endif
                      
                        @if(@$order_product->bbb_link)
                          <tr>
                            <td>Coaching room</td>
                            <td>
                                <a href="{{ $order_product->bbb_link }}" target="_blank">{{$order_product->bbb_link}}</a><br>
                                @if(count($schedules) > 0)
                                  <p><b>Schedule Time:</b></p>
                                  @foreach($schedules as $key => $schedule)
                                    <p>{{($key+1)}}. {{date('l, d F Y',strtotime($schedule->start_date)).' '.$schedule->start_time}} - {{date('l, d F Y',strtotime($schedule->end_date)).' '.$schedule->end_time}}</p>
                                  @endforeach
                                @endif
                            </td>
                          </tr>
                        @endif

                        @if ($product->can_edit)
                          <tr>
                            <td colspan="2">
                              <a href="{{ route('coach.schedule.order.edit',[$product->course_id,$product->coach_id]).'?product_id='.$product->id.'&type_order=corporate&order_product_id='.baseEncrypt($order_product->id).'&user_id=encuser-'.baseEncrypt($order_product->user_id)}}" class="btn btn-success btn-xs " title="Edit Schedule" data-button="edit">
                              <i class="fa fa-pencil-square-o fa-fw"></i> Edit Schedule
                              </a>
                            </td>
                          </tr>
                        @endif

                        @if(!$order_product->bbb_link)
                          @if(@$link_schedule)
                            <tr>
                              <td>Set Schedule</td>
                              <td>
                                  <a href="{{ $link_schedule }}" target="_blank">{{$link_schedule}}</a>
                              </td>
                            </tr>
                          @endif
                        @endif
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
</script>
@endsection
