@extends('layouts.admin_template')

@section('title', 'Mini Store')
@section('page_title', 'Mini Store')
@section('page_description', 'Product')

@section('header')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>  

<style type="text/css">
.ribbon-wrapper {
  width: 85px;
  height: 88px;
  opacity: 0.8;
  overflow: hidden;
  position: absolute;
  top: -3px;
  right: -3px;
}

.ribbon {
  font: bold 15px Sans-Serif;
  color: #333;
  text-align: center;
  text-shadow: rgba(255,255,255,0.5) 0px 1px 0px;
  -webkit-transform: rotate(45deg);
  -moz-transform:    rotate(45deg);
  -ms-transform:     rotate(45deg);
  -o-transform:      rotate(45deg);
  position: relative;
  padding: 7px 0;
  left: -5px;
  top: 15px;
  width: 120px;
  background-color: #BFDC7A;
  background-image: -webkit-gradient(linear, left top, left bottom, from(#BFDC7A), to(#8EBF45)); 
  background-image: -webkit-linear-gradient(top, #BFDC7A, #8EBF45); 
  background-image:    -moz-linear-gradient(top, #BFDC7A, #8EBF45); 
  background-image:     -ms-linear-gradient(top, #BFDC7A, #8EBF45); 
  background-image:      -o-linear-gradient(top, #BFDC7A, #8EBF45); 
  color: #6a6340;
  -webkit-box-shadow: 0px 0px 3px rgba(0,0,0,0.3);
  -moz-box-shadow:    0px 0px 3px rgba(0,0,0,0.3);
  box-shadow:         0px 0px 3px rgba(0,0,0,0.3);
}

.ribbon:before, .ribbon:after {
  content: "";
  border-top:   3px solid #6e8900;   
  border-left:  3px solid transparent;
  border-right: 3px solid transparent;
  position:absolute;
  bottom: -3px;
}

.ribbon:before {
  left: 0;
}
.ribbon:after {
  right: 0;
}​
</style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    </ol>
@endsection
@section('content')
<div class="minstore">
  <div class="form-group">
    <input class="typeahead form-control" style="margin:0px auto;width:300px;" type="text" placeholder="Search Product Here!">
  </div>
  @include('admin.master.users.corporate_customers.product_store')
</div>
@endsection

@section('scripts')
    <script>
      $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();   
      });
    </script>
    <script type="text/javascript">
        var path = "{{ route('autocomplete') }}";
        $('input.typeahead').typeahead({
            source:  function (seacrh, process) {
            return $.get(path, { seacrh: seacrh }, function (data) {
                    return process(data);
                });
            }
        });
    </script>
    <script>
      $(window).on('hashchange', function() {
          if (window.location.hash) {
              var page = window.location.hash.replace('#', '');
              if (page == Number.NaN || page <= 0) {
                  return false;
              } else {
                  getPosts(page);
              }
          }
      });
      $(document).ready(function() {
          $(document).on('click', '.pagination a', function (e) {
              getPosts($(this).attr('href').split('page=')[1]);
              e.preventDefault();
          });
      });
      function getPosts(page) {
          $.ajax({
              url : '?page=' + page,
              dataType: 'json',
          }).done(function (data) {
              $('.minstore').html(data);
              location.hash = page;
          }).fail(function () {
              alert('Posts could not be loaded.');
          });
      }
    </script> 
    {!! Html::script('bower_components/highcharts/highcharts.js') !!}
    {!! Html::script('bower_components/highcharts/modules/exporting.js') !!}
@endsection
