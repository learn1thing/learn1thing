@extends('layouts.admin_template')

@section('title', 'Corporate Customer')
@section('page_title', 'Corporate Customer')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-link m-right-10"></i> User Administration</li>
        <li>Corporate Customer</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-3 pull-right">
        <a href="{{ route('admin.corporate-customer.create')}}" class="btn btn-primary pull-right" id="btn-submit">
           <i class="fa fa-plus m-right-10"></i> Add User
        </a>
    </div>
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
              <div class="table-responsive">
                 {!! $dataTable->table(['class' => 'datatable table table-striped', 'cellspacing'=>"0", 'width'=>"100%"]) !!}
              </div>
            </div>
        </div>
    </div>
</div>
@include('partials.delete-modal')
@endsection
@section('scripts')
{!! $dataTable->scripts() !!}
<script type="text/javascript">
    // $(document).on('click','form#destroy #submit',function(e) {
    //     e.preventDefault();
    //     $('#delete-modal').modal('toggle');
    //     toastr.error("Sorry, user can't be deleted");
    // });
</script>
@endsection
