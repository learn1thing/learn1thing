
@extends('layouts.admin_template')

@section('title', 'Manage Account')
@section('page_title', 'Manage Account')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-users m-right-10"></i> Manage Account</li>
    </ol>
@endsection

@section('content')
<h4>Link Account</h4>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
            {!! Form::open(['route' =>'admin.user.link-account.store','id' => 'form-user']) !!}
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                        <label class="control-label" for="email">{!! trans('label.required') !!}Email</label>
                        <input type="text" name="email" class="form-control" id="email" placeholder="you@email.com" value="{{old('email')}}">
                        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                        <label class="control-label" for="password">{!! trans('label.required') !!}Password</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Password" value="{{old('password')}}">
                        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 m-top-10 text-right">
                    <a href="{{route('admin.user.profile')}}" class="btn btn-default">
                       <i class="fa fa-times m-right-10"></i> Cancel
                    </a>
                
                    <button type="submit" class="btn btn-primary" id="btn-submit">
                        <i class="fa fa-check m-right-10"></i> Submit
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
    
</div>
<h4>Linked Account</h4>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        @if(count($list_accounts) > 0)
                            @foreach($list_accounts as $list)
                                <tr>
                                    <td>{{$list['name']}}</td>
                                    <td>{{$list['email']}}</td>
                                    <td>{{$list['role']}}</td>
                                    <td>{{$list['status']}}</td>
                                    <td>{!!$list['action']!!}</td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
</div>

<!-- modal confirm unlink account .start here -->
<div class="modal fade" id="modal-unlink-account">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Unlink Account</h4>
      </div>
      <div class="modal-body">
        <p class="ask-confirm">Are you sure you want to unlink this account?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times m-right-10"></i> No</button>
        <button type="button" class="btn btn-primary" id="btn-confirm"><i class="fa fa-check m-right-10"></i> Yes</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- modal confirm unlink account .end here -->
@endsection
@section('scripts')
{!! Html::script('vendor/jsvalidation/js/jsvalidation.min.js') !!}
{!! JsValidator::formRequest('App\Http\Requests\Master\User\LinkAccountRequest', '#form-user'); !!}
<script type="text/javascript">
    var urlUnlink = '';
    $(document).on('click','.btn-link-account',function() {
        urlUnlink = $(this).data('href');
        $('#modal-unlink-account').modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    $(document).on('click','#btn-confirm',function() {
        window.location.href = urlUnlink;
    });

</script>
@endsection
