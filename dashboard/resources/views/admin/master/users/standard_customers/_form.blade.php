<style type="text/css">
    input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
</style> 
<input type="hidden" name="id" value="{{@$user->related_id}}">
<input type="hidden" name="user_id" value="{{@$user->id}}">
<div class="row">
    <div class="col-sm-6">
        <h4>Your Personal Details</h4><hr>
        <div class="form-group {{ $errors->has('first_name') ? 'has-error' : ''}}">
            {!! trans('label.required') !!}{!! Form::label('first_name', trans('First Name'), ['class' => 'control-label']) !!}
            {!! Form::text('first_name', old('first_name') , ['class' => 'form-control']) !!}
            {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('last_name') ? 'has-error' : ''}}">
            {!! trans('label.required') !!}{!! Form::label('last_name', trans('Last Name'), ['class' => 'control-label']) !!}
            {!! Form::text('last_name', old('last_name') , ['class' => 'form-control']) !!}
            {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}" style="{{(@$user->id) ? 'display:none;' : 'display:block;'}}">
            {!! trans('label.required') !!}{!! Form::label('email', trans('Email'), ['class' => 'control-label']) !!}
            {!! Form::email('email', old('email') , ['class' => 'form-control']) !!}
            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
            {!! Form::label('phone', trans('Phone'), ['class' => 'control-label']) !!}
            {!! Form::number('phone', old('phone') , ['class' => 'form-control']) !!}
            {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}" >
            {!! trans('label.required') !!}{!! Form::label('password', trans('Password'), ['class' => 'control-label']) !!}
            {!! Form::password('password' , ['class' => 'form-control']) !!}
            {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="col-sm-6">
        <h4>Your Address</h4><hr>
        <div class="form-group {{ $errors->has('address_1') ? 'has-error' : ''}}">
            {!! trans('label.required') !!}{!! Form::label('address_1', trans('Address 1'), ['class' => 'control-label']) !!}
            {!! Form::textarea('address_1', old('address_1') , ['class' => 'form-control','rows' => '3']) !!}
            {!! $errors->first('address_1', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('address_2') ? 'has-error' : ''}}">
            {!! Form::label('address_2', trans('Address 2'), ['class' => 'control-label']) !!}
            {!! Form::textarea('address_2', old('address_2') , ['class' => 'form-control','rows' => '3']) !!}
            {!! $errors->first('address_2', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('city') ? 'has-error' : ''}}">
            {!! trans('label.required') !!}{!! Form::label('city', trans('City'), ['class' => 'control-label']) !!}
            {!! Form::text('city', old('city') , ['class' => 'form-control']) !!}
            {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('post_code') ? 'has-error' : ''}}">
            {!! trans('label.required') !!}{!! Form::label('post_code', trans('Postal Code'), ['class' => 'control-label']) !!}
            {!! Form::number('post_code', old('post_code') , ['class' => 'form-control']) !!}
            {!! $errors->first('post_code', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('country') ? 'has-error' : ''}}">
            {!! trans('label.required') !!}{!! Form::label('country', trans('Country'), ['class' => 'control-label']) !!}
            {!! Form::select('country', [''=>'Select Country'] + $countries, old('country'), ['class' => 'select2 form-control']) !!}
            {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('region') ? 'has-error' : ''}}">
            {!! Form::label('region', trans('region'), ['class' => 'control-label']) !!}
            {!! Form::select('region', (@zones) ? [''=>'Select Region'] + @$zones : [''=>'Select Region'], old('region'), ['class' => 'select2 form-control']) !!}
            {!! $errors->first('region', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>

@section('part_js')
<script type="text/javascript">
    $(document).ready(function(){
        $('.select2').select2();
    });
</script>
@endsection