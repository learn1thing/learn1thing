
@extends('layouts.admin_template')

@section('title', 'Create Standard Customer')
@section('page_title', 'Create Standard Customer')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-link m-right-10"></i> User Administration</li>
        <li>Standard Customer</li>
        <li>Add</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
            @if(Session::has('flash_message'))
                <div class="alert alert-success"><em> {!! session('flash_message') !!}</em></div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-error"><em> {!! session('error') !!}</em></div>
            @endif
            {!! Form::open(['route' =>'admin.standard-customer.store','class' =>'','id' => 'form-standard-customer']) !!}
            @include('admin.master.users.standard_customers._form')
            <div class="row">
                <div class="col-xs-12 m-top-10 text-right">
                    <a href="{{url('admin/master/user/standard-customer')}}" class="btn btn-default">
                       <i class="fa fa-times m-right-10"></i> Cancel
                    </a>
                
                    <button type="submit" class="btn btn-primary" id="btn-submit">
                        <i class="fa fa-check m-right-10"></i> Save
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
    
</div>
@endsection
@section('scripts')
{!! Html::script('vendor/jsvalidation/js/jsvalidation.min.js') !!}
{!! JsValidator::formRequest('App\Http\Requests\Master\User\StandardCustomerRequest', '#form-standard-customer'); !!}
<script type="text/javascript">
    $('div.alert').delay(5000).slideUp(300);

    $(document).on('change','#country',function(){
        getZone($('#region'),$(this).val());
    });
</script>
@endsection
