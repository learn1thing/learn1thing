
@extends('layouts.admin_template')

@section('title', 'Edit Email Template')
@section('page_title', 'Edit Email Template')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.emailtemplate.index') }}"><i class="fa fa-building m-right-10"></i> Email template</a></li>
        <li>Add</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
            {!! Form::model($email, [
                'route'     =>['admin.emailtemplate.update', $email->id],
                'method'    => 'PATCH',
                'class' =>  'form-horizontal',
                'id'    =>  'form-emailtemplate',
            ]) !!}
            @include('admin.master.email_template.form')

            <p>
                Variable Snippets <br><i><bold>
            Copy and paste variable snippets to body email.</bold></i>
            </p>
           
            @foreach( $snippets as $snippet )
                <span class="label label-default"> {{ $snippet }} </span> &nbsp
            @endforeach
            
            <div class="row">
                <div class="col-xs-12 m-top-10 text-right">
                    <a href="{{route('admin.emailtemplate.index')}}" class="btn btn-default">
                       <i class="fa fa-times m-right-10"></i> Cancel
                    </a>
                
                    <button type="submit" class="btn btn-primary" id="btn-submit">
                        <i class="fa fa-check m-right-10"></i> Save
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
    
</div>
@endsection
@section('scripts')
{!! Html::script('vendor/jsvalidation/js/jsvalidation.min.js') !!}
{!! JsValidator::formRequest('App\Http\Requests\Master\EmailTemplateRequest', '#form-emailtemplate'); !!}
@endsection
