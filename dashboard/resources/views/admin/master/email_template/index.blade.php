@extends('layouts.admin_template')

@section('title', 'Email Template')
@section('page_title', 'Email Template')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-building m-right-10"></i> Email Template</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-3 pull-right">
        <a href="{{ route('admin.emailtemplate.create')}}" class="btn btn-primary pull-right" id="btn-submit">
           <i class="fa fa-plus m-right-10"></i> Add Email Template
        </a>
    </div>
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
              <div class="table-responsive">
                 {!! $dataTable->table(['class' => 'datatable table table-striped', 'cellspacing'=>"0", 'width'=>"100%"]) !!}
              </div>
            </div>
        </div>
    </div>
</div>
@include('partials.delete-modal')
@endsection
@section('scripts')
{!! $dataTable->scripts() !!}
@endsection
