<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', trans('Name'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
       {!! Form::text('title', null , ['class' => 'form-control']) !!}
       {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
   </div>
</div>
<div class="form-group {{ $errors->has('subject') ? 'has-error' : ''}}">
    {!! Form::label('subject', trans('Subject'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
       {!! Form::text('subject', null , ['class' => 'form-control']) !!}
       {!! $errors->first('subject', '<p class="help-block">:message</p>') !!}
   </div>
</div>
<div class="form-group {{ $errors->has('message') ? 'has-error' : ''}}">
    {!! Form::label('message', trans('Message :'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-9">
       {!! Form::textarea('message', null , ['class' => 'form-control', 'id' => 'message']) !!}
       {!! $errors->first('message', '<p class="help-block">:message</p>') !!}
   </div>
</div>
@section( 'scripts' )
<script type="text/javascript">
      $("#message").wysihtml5();
</script>
@endsection