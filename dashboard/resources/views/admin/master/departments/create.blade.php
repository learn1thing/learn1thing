
@extends('layouts.admin_template')

@section('title', 'Create Department')
@section('page_title', 'Create Department')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.department.index') }}"><i class="fa fa-building m-right-10"></i> Departement</a></li>
        <li>Add</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
            {!! Form::open(['route' =>'admin.department.store','class' =>  'form-horizontal','id' => 'form-department']) !!}
            @include('admin.master.departments._form')
            <div class="row">
                <div class="col-xs-12 m-top-10 text-right">
                    <a href="{{route('admin.department.index')}}" class="btn btn-default">
                       <i class="fa fa-times m-right-10"></i> Cancel
                    </a>
                
                    <button type="submit" class="btn btn-primary" id="btn-submit">
                        <i class="fa fa-check m-right-10"></i> Save
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
    
</div>
@endsection
@section('scripts')
{!! Html::script('vendor/jsvalidation/js/jsvalidation.min.js') !!}
{!! JsValidator::formRequest('App\Http\Requests\Master\DepartmentRequest', '#form-department'); !!}
@endsection
