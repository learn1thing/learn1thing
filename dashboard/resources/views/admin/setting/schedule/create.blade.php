@extends('layouts.admin_template')

@section('title', 'Add Master Schedule')
@section('page_title', 'Add Master Schedule')
@section('page_description', 'learn1thing')

@section('header')
{!! Html::style('bower_components/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.css') !!}
{!! Html::style('bower_components/AdminLTE/plugins/timepicker/bootstrap-timepicker.css') !!}
{!! Html::style('bower_components/AdminLTE/plugins/datepicker/datepicker3.css') !!}
{!! Html::style('bower_components/bootstrap-toggle/css/bootstrap-toggle.min.css') !!}
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-mortar-board m-right-10"></i> Manage Coach schedule</li>
        <li>Master Schedule</li>
        <li>Add</li>
    </ol>
@endsection

@section('content')

<div class="row">    
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                @include('partials.message')
            <!-- /.box-header -->
            <!-- form start -->
                {!! Form::open(array('route' => 'master.schedule.store','method'=>'POST', 'id'=>'myform', 'class'=>'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
                    <div class="box-body">
                        <div class="form-group {{ $errors->has('course') ? 'has-error' : ''}}" style="display:none">
                            {!! Form::label('course', 'Course', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-5">
                                {{ Form::select('course', $course, old('course'), ['class' => 'form-control','id'=>'course','placeholder'=>'Select Course']) }}
                                <input type="hidden" name="course_name" value="" id="course_name">
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}" style="display:none">
                            {!! Form::label('type', 'Type', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-5">
                                {{ Form::select('type', ['public'=>'Public','private'=>'Private'], 'public', ['class' => 'form-control','id'=>'type','placeholder'=>'Select Type']) }}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('start_date') ? 'has-error' : ''}}">
                            <label for="start_date" class="col-sm-2 control-label">Start Date</label>
                            <div class="input-group col-sm-5 date" data-provide="datepicker" style="padding-left: 15px;width: 244px;">
                                <input type="text" class="form-control" id="start_date" name="start_date" value="{{old('start_date')}}">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                            @if ($errors->has('start_date'))
                                <span class="help-block col-sm-3 col-md-offset-2">
                                    <strong>{{ $errors->first('start_date') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('end_date') ? 'has-error' : ''}}">
                            <label for="end_date" class="col-sm-2 control-label">End Date</label>

                            <div class="input-group col-sm-5 date" data-provide="datepicker" style="padding-left: 15px;width: 244px;">
                                <input type="text" class="form-control" id="end_date" name="end_date" value="{{ old('end_date') }}">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                            @if ($errors->has('end_date'))
                                <span class="help-block col-sm-3 col-md-offset-2">
                                    <strong>{{ $errors->first('end_date') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('start_time') ? 'has-error' : ''}}">
                            {!! Form::label('start_time', trans('label.schedule.start_time'), ['class' => 'col-sm-2 control-label']) !!}
                            <div class="bootstrap-timepicker col-sm-3">
                                {!! Form::text('start_time', old('start_time') , ['class' => 'form-control timepicker']) !!}
                                {!! $errors->first('start_time', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('end_time') ? 'has-error' : ''}}">
                            {!! Form::label('end_time', trans('label.schedule.end_time'), ['class' => 'col-sm-2 control-label']) !!}
                            <div class="bootstrap-timepicker col-sm-3">
                                {!! Form::text('end_time', old('end_time') , ['class' => 'form-control timepicker']) !!}
                                {!! $errors->first('end_time', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('interval') ? 'has-error' : ''}}">
                            {!! Form::label('interval', 'Interval (Minutes)', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-5">
                                {{ Form::select('interval', ['30'=>'30 Minutes','60'=>'60 Minutes','90'=>'90 Minutes'], '30', ['class' => 'form-control','id'=>'interval','placeholder'=>'Select Interval']) }}
                                {!! $errors->first('interval', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('day') ? 'has-error' : ''}}" id="day">
                            <label for="inputEmail3" class="col-sm-2 control-label">Add Appoinment On</label>
                            <div class="col-sm-10">
                                <label>
                                    <input type="checkbox" name="day[]" value="Monday">
                                    Monday
                                </label><br/>

                                <label>
                                    <input type="checkbox" name="day[]" value="Tuesday">
                                    Tuesday
                                </label><br/>

                                <label>
                                    <input type="checkbox" name="day[]" value="Wednesday">
                                    Wednesday
                                </label><br/>

                                <label>
                                    <input type="checkbox" name="day[]" value="Thursday">
                                    Thursday
                                </label><br/>

                                <label>
                                    <input type="checkbox" name="day[]" value="Friday"> 
                                    Friday
                                </label><br/>

                                <label>
                                    <input type="checkbox" name="day[]" value="Saturday">
                                    Saturday
                                </label><br/>

                                <label>
                                    <input type="checkbox" name="day[]" value="Sunday">
                                    Sunday
                                </label><br/>
                                {!! $errors->first('day', '<p class="help-block">:message</p>') !!}
                            </div>

                        </div>
                    </div>
              <!-- /.box-body -->
                    <div class="row">
                        <div class="col-xs-12 m-top-10 text-right {{ $errors->has('end_time') ? 'has-error' : ''}}">
                            <a href="{{url('master-schedule')}}" class="btn btn-default">
                               <i class="fa fa-times m-right-10"></i> Cancel
                            </a>
                        
                            <button type="button" class="btn btn-primary" id="act-submit">
                                <i class="fa fa-check m-right-10"></i> {{trans('label.save')}}
                            </button>
                        </div>
                    </div>
              <!-- /.box-footer -->
                {!! Form::close() !!}
            </div>

            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Information</h4>
                  </div>
                  <div class="modal-body">
                    <p>This data cannot be edited. Continue to save?</p>
                  </div>
                  <div class="modal-footer">
                    <a id="delete-modal-cancel" href="#" class="btn btn-default pull-left" data-dismiss="modal">    <i class="fa fa-times m-right-10"></i> {!! trans('label.cancel') !!}
                    </a>
                    <button class="btn btn-primary" id="act-save" type="button">
                        <i class="fa fa-check m-right-10"></i> Save
                    </button>
                  </div>
                </div>

              </div>
            </div>
            <!-- END MODAL -->

        </div>
    </div>
</div>

@include('partials.delete-modal')

@endsection
@section('scripts')
{!! Html::script('bower_components/bootstrap-toggle/js/bootstrap-toggle.min.js') !!}
{!! Html::script('bower_components/AdminLTE/plugins/datepicker/bootstrap-datepicker.js') !!}
{!! Html::script('bower_components/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.js') !!}
{!! Html::script('bower_components/AdminLTE/plugins/timepicker/bootstrap-timepicker.js') !!}
<script type="text/javascript">
    var urlListCours = "{{ route('coach.course.lists') }}";


    $(document).ready(function(){
        $("#act-submit").on('click',function(){
            $("#myModal").modal('show');
            $("#act-save").on('click',function(){
                $("#myform").submit();
            });
        });
    });

    // $("#course").select2({
    //     ajax: {
    //         url: "{{ route('coach.course.lists') }}",
    //         dataType: "json",
    //         type: "GET",
    //         data: function (term, page) {
    //             return {
    //                 q: term
    //             };
    //         },
    //         results: function (data, page) {
    //             console.log(data);
    //             return { results: data.results };
    //         }

    //     },
    //     initSelection: function (item, callback) {
    //         var id = item.val();
    //         var text = item.data('option');

    //         if(id > 0){

    //             var data = { id: id, text: text };
    //             callback(data);
    //         }
    //     },
    //     formatAjaxError:function(a,b,c){return"Data Not Found .."}
    // });

    $(function () {
        //Initialize Select2 Elements
        // Date picker
        $('.datepicker').datepicker({
          autoclose: true,
          format:"yyyy-mm-dd"
        });

        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false,
          showMinutes : false,
          defaultTime : false,
          minuteStep : 60
        });

        // $("#course").on('change',function(){
        //     $("#course_name").val($("#course option:selected").text());
        // });

    });
    
    $('div.alert').delay(3000).slideUp(300);

    $('form').submit(function () {
        $("input[name=day]").each( function () {
          var day = $(this).val();
          if (name === '') {
            alert('Text-field is empty.');
            return false;
          }
        });
    });
</script>
@endsection