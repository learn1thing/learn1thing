@extends('layouts.admin_template')

@section('title', 'Edit Expired Schedule')
@section('page_title', 'Edit Expired Schedule')
@section('page_description', 'learn1thing')

@section('header')
{!! Html::style('bower_components/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.css') !!}
{!! Html::style('bower_components/AdminLTE/plugins/timepicker/bootstrap-timepicker.css') !!}
{!! Html::style('bower_components/AdminLTE/plugins/datepicker/datepicker3.css') !!}
{!! Html::style('bower_components/bootstrap-toggle/css/bootstrap-toggle.min.css') !!}
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-mortar-board m-right-10"></i> Manage Coach schedule</li>
        <li>Expired Schedule</li>
        <li>Edit</li>
    </ol>
@endsection

@section('content')

<div class="row">    
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                @include('partials.message')
                <!-- /.box-header -->
                <!-- form start -->
                {!! Form::open(array('route' => ['master.schedule.expired.update',$id],'method'=>'PUT', 'id'=>'myform', 'class'=>'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
                    {!! Form::hidden('id', $id , ['class' => 'form-control']) !!}
                    <div class="box-body">
                        <div class="form-group {{ $errors->has('business_partner_id') ? 'has-error' : ''}}" style=" {{  (user_info()->inRole('business-partner'))? 'display:none':'' }} " >
                            {!! Form::label('business_partner_id', 'Business Partner', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-3">
                                {{ Form::select('business_partner_id', $business_partners, $business_partner_id, ['class' => 'form-control','id'=>'business_partner_id']) }}
                                {!! $errors->first('business_partner_id', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('expired') ? 'has-error' : ''}}">
                            {!! Form::label('expired', 'Expired (hours)', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-3">
                                {{ Form::select('expired', ['48'=>'48','72'=>'72'], $expired, ['class' => 'form-control','id'=>'expired']) }}
                                {!! $errors->first('expired', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
              <!-- /.box-body -->
                    <div class="row">
                        <div class="col-xs-12 m-top-10 text-right {{ $errors->has('end_time') ? 'has-error' : ''}}">
                            <a href="{{url('master-schedule/expired-schedule')}}" class="btn btn-default">
                               <i class="fa fa-times m-right-10"></i> Cancel
                            </a>
                        
                            <button type="submit" class="btn btn-primary" id="act-submit">
                                <i class="fa fa-check m-right-10"></i> {{trans('label.save')}}
                            </button>
                        </div>
                    </div>
              <!-- /.box-footer -->
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@include('partials.delete-modal')

@endsection
@section('scripts')
{!! Html::script('bower_components/bootstrap-toggle/js/bootstrap-toggle.min.js') !!}
{!! Html::script('bower_components/AdminLTE/plugins/datepicker/bootstrap-datepicker.js') !!}
{!! Html::script('bower_components/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.js') !!}
{!! Html::script('bower_components/AdminLTE/plugins/timepicker/bootstrap-timepicker.js') !!}
<script type="text/javascript">
    var urlListCours = "{{ route('coach.course.lists') }}";


    // $("#course").select2({
    //     ajax: {
    //         url: "{{ route('coach.course.lists') }}",
    //         dataType: "json",
    //         type: "GET",
    //         data: function (term, page) {
    //             return {
    //                 q: term
    //             };
    //         },
    //         results: function (data, page) {
    //             console.log(data);
    //             return { results: data.results };
    //         }

    //     },
    //     initSelection: function (item, callback) {
    //         var id = item.val();
    //         var text = item.data('option');

    //         if(id > 0){

    //             var data = { id: id, text: text };
    //             callback(data);
    //         }
    //     },
    //     formatAjaxError:function(a,b,c){return"Data Not Found .."}
    // });

    $(function () {
        //Initialize Select2 Elements
        // Date picker
        $('.datepicker').datepicker({
          autoclose: true,
          format:"yyyy-mm-dd"
        });

        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false,
          showMinutes : false,
          defaultTime : false,
          minuteStep : 60
        });

        // $("#course").on('change',function(){
        //     $("#course_name").val($("#course option:selected").text());
        // });

    });
    
    $('div.alert').delay(3000).slideUp(300);

    $('form').submit(function () {
        $("input[name=day]").each( function () {
          var day = $(this).val();
          if (name === '') {
            alert('Text-field is empty.');
            return false;
          }
        });
    });
</script>
@endsection