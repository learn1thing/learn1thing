@extends('layouts.admin_template')

@section('title', 'Expired Schedule')
@section('page_title', 'Expired Schedule')
@section('page_description', 'learn1thing')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-mortar-board m-right-10"></i> Manage Coach schedule</li>
        <li>Expired Schedule</li>
    </ol>
@endsection

@section('content')

<div class="row">   
    <div class="col-xs-3 pull-right">
        <a href="{{route('master.schedule.expired.create')}}" class="btn btn-primary pull-right" id="btn-submit">
           <i class="fa fa-plus m-right-10"></i> Add Expired Schedule
        </a>
    </div> 
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                @if (session('message'))
                    <div class="alert alert-warning">
                        {{ session('message') }}
                    </div>
                @endif
                <div class="table-responsive">
                    {!! $dataTable->table(['class' => 'datatable table table-striped', 'cellspacing'=>"0", 'width'=>"100%"]) !!}
                </div>
            </div>
        </div>
    </div>
</div>
@include('partials.delete-modal')

@endsection
@section('scripts')
{!! $dataTable->scripts() !!}
<script type="text/javascript">
    $('div.alert').delay(3000).slideUp(300);
</script>
@endsection