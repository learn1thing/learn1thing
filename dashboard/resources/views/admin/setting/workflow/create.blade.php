@extends('layouts.admin_template')

@section('title', 'Create Workflow')
@section('page_title', 'Create Workflow')
@section('page_description', 'learn1thing')

@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{ route('admin.workflow.index') }}">Workflow</a></li>
    <li>Create</li>
</ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        {!! Form::open(['route' =>'admin.workflow.store', 'class' =>  'form-horizontal','id' => 'form-workflow']) !!}

        <div class="box">
            <div class="box-body">
                @include( 'admin.setting.workflow.form' )
            </div>

            <div class="box-footer text-right">
                <div class="row">
                    <div class="col-xs-12 m-top-10 text-right">
                    <a href="{{ route('admin.workflow.index') }}" class="btn btn-default">
                         <i class="fa fa-times m-right-10"></i> Cancel
                     </a>

                     <button type="submit" class="btn btn-primary">
                        <i class="fa fa-check m-right-10"></i> Save
                    </button>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>

</div>
@endsection
@section('scripts')
{!! Html::script('vendor/jsvalidation/js/jsvalidation.min.js') !!}
{!! JsValidator::formRequest('App\Http\Requests\Setting\WorkflowRequest', '#form-workflow'); !!}
@endsection
