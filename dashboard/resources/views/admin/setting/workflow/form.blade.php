@section( 'header' )
<style type="text/css">
    .optionGroup {
        font-weight: bold;
    }
</style>
@endsection
<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('product_category', trans('Product Category'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-4">
        {!! Form::select('product_category_id', $categories, null, ['class' => 'form-control', 'placeholder' => 'Choose Category', 'id' => 'product_category_id' ]) !!}
        {!! $errors->first('product_category_id', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-4">
     {!! Form::select('product_id', [], null, ['class' => 'form-control', 'id' => 'product', 'placeholder' => 'Select Specific Product' ]) !!}
     {!! $errors->first('product_id', '<p class="help-block">:message</p>') !!}
 </div>
</div>

<div class="form-group {{ $errors->has('action') ? 'has-error' : ''}}">
    <label class="col-sm-2 control-label">Action <label style="color: red;">*</label></label>
    <div class="col-sm-4">
        {!! Form::select('action', $actions, null, ['class' => 'form-control' ]) !!}
        {!! $errors->first('action', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-4">
        {!! Form::select('action_template', $templates, null, [ 'required' => true, 'class' => 'form-control', 'placeholder' => 'Select Template' ]) !!}
        {!! $errors->first('action_template', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('recepients') ? 'has-error' : ''}}">
    <label class="col-sm-2 control-label">Recipients <label style="color: red;">*</label></label>
    <div class="col-sm-2">
        {!! Form::select('recipient_role', $recepients, null, ['required' => true, 'class' => 'form-control', 'placeholder' => 'Choose Recepients', 'id' => 'recipient_role' ]) !!}
        {!! $errors->first('recipient_role', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-2">
        {!! Form::select('recipient_user_id', [], null, ['class' => 'form-control', 'id' => 'userid', 'required' => true ]) !!}
        {!! $errors->first('recipient_user_id', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-2">
     {!! Form::select('recipient_user', $access, null, ['class' => 'form-control', 'id' => 'access' , 'placeholder' => 'Select Access' ]) !!}
     {!! $errors->first('recipient_user', '<p class="help-block">:message</p>') !!}
 </div>
 <div class="col-sm-2">
    {!! Form::select('recipient_department', [], null, ['class' => 'form-control', 'id' => 'roles' ]) !!}
    {!! $errors->first('recipient_department', '<p class="help-block">:message</p>') !!}
</div>
</div>

<!-- <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('', '', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-2">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-2">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-2">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-2">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div> -->

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('product_category_id', trans('Trigger'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-4">
        {!! Form::select('event_trigger', $events, null, ['class' => 'form-control', 'placeholder' => 'Select Event' ]) !!}
        {!! $errors->first('event_trigger', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-2">
        {!! Form::number('day', null, ['class' => 'form-control', 'placeholder' => 'Days', 'min' => 1 ]) !!}
        {!! $errors->first('day', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-2">
        {!! Form::select('date_offset', $schedules, null, ['class' => 'form-control', 'placeholder' => 'Date Offset' ]) !!}
        {!! $errors->first('date_offset', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@section( 'scripts' )
<script type="text/javascript">

    // Get Product
    $( "#product_category_id" ).change( function() {
        var product_category_id = $(this).val();
        $.ajax({
            method : 'GET',
            url : "{!! route( 'admin.workflow.getproduct' ) !!}",
            data : { category_id: product_category_id },
            dataType: 'json',
            cache : true,
            success: function( result ) {
                $("#product").empty();
                $("#product").append($("<option/>", {
                    value:0,
                    text: 'All'
                }));
                $.each( result, function (key, value) {
                    $("#product").append($("<option/>", {
                        value: value.id,
                        text: value.name
                    }));
                });
            }
        });
    });

    // Get Recepients
    $( "#recipient_role" ).change( function() {
        var role = $(this).val();
        $.ajax({
            method : 'GET',
            url : "{!! route( 'admin.workflow.getuser' ) !!}",
            data : { role: role },
            dataType : 'json',
            success: function( result ) {
                $("#userid").empty();
                $("#userid").append($("<option/>", {
                    value:0,
                    text: 'All'
                }));
                $.each( result, function (key, value) {

                    $("#userid").append($("<option/>", {
                        value: value.id,
                        text: value.name
                    }));
                });
            }
        });
    });

    // Get Recepients Department / Role
    $( "#access" ).change( function() {
        var role = $(this).val();
        $.ajax({
            method : 'GET',
            url : "{!! route( 'admin.workflow.getaccess' ) !!}",
            data : { role: role },
            dataType : 'json',
            success: function( result ) {
                $("#roles").empty();
                if (result.id == 1) {
                    $("#roles").append(result.data);
                } else {
                    $("#roles").append($("<option/>", {
                        value:0,
                        text: 'All'
                    }));
                    $.each( result.data, function (key, value) {
                        $("#roles").append($("<option/>", {
                            value: value.id,
                            text: value.name
                        }));
                    });
                }
            }
        });
    });

</script>
@endsection

