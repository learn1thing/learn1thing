@extends('layouts.admin_template')

@section('title', 'Workflow')
@section('page_title', 'Workflow')
@section('page_description', 'learn1thing')
@section('header')
{!! Html::style('bower_components/AdminLTE/plugins/datepicker/datepicker3.css') !!}
{!! Html::style('bower_components/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.css') !!}
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
  <li><i class="fa fa-building m-right-10"></i> Workflow</li>
</ol>
@endsection

@section('content')

<div class="row">
  <div class="col-xs-3 pull-right">
    <a href="{{ route('admin.workflow.create')}}" class="btn btn-primary pull-right" id="btn-submit">
     <i class="fa fa-plus m-right-10"></i> Add Workflow
   </a>
 </div>
 <div class="col-xs-12">
  <div class="box">
    <div class="box-body">
      <div class="table-responsive">
       <table class="table table-striped" id="workflows">
         <thead>
           <tr>
             <th>Product</th>
             <th>Recipients</th>
             <th>Action</th>
             <th>Trigger</th>
             <th>Status</th>
             <th>Datetime</th>
             <th></th>
           </tr>
         </thead>
         <tbody>
          @foreach( $workflows as $workflow )
           <tr>
             <td>{{ !empty($workflow->product->name) ?  $workflow->product->name : '-' }}</td>
             <td>
               @if( !empty($workflow->recipient) )
    {{ $workflow->recipient->first_name }} {{ $workflow->recipient->last_name }}               
               @else
                All
               @endif
             </td>
             <td>{{ $workflow->action == 2 ? 'Email Template' : 'SMS Template' }} - {{ $workflow->email_template->title }}</td>
             <td>{{ $workflow->event_trigger }}</td>
             <td></td>
             <td><input type="text" name="datetime[{{ $workflow->id }}]" class="form-control datepicker" id="datepicker"></td>
             <td>
             <button type="button" class="btn btn-primary btn-xs" data-id="{{ $workflow->id }}"><i class="fa fa-paper-plane" data-toggle="tooltip" data-placement="bottom" title="Send Mail"></i></button>
              
              <a href="{{ route('admin.workflow.edit', $workflow->id) }}" class="btn btn-success btn-xs" title="Edit" data-button="edit">
                <i class="fa fa-pencil-square-o fa-fw"></i>
              </a>

              <a href="javascript:void(0)" id="deleteData" class="btn btn-danger btn-xs" title="Delete" data-href="{{ route( 'admin.workflow.destroy', $workflow->id ) }}" data-button="delete">
                <i class="fa fa-trash-o fa-fw"></i>
              </a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
</div>
@include('partials.delete-modal')
@endsection
@section('scripts')
{!! Html::script('bower_components/AdminLTE/plugins/datepicker/bootstrap-datepicker.js') !!}
{!! Html::script('bower_components/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.js') !!}

<script type="text/javascript">
  $("#workflows").DataTable();

   //Date picker
   
   $('.datepicker').each(function(){
    $(this).datepicker({
      autoclose: true
    });
  });
</script>
@endsection
