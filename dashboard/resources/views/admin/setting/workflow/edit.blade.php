@extends('layouts.admin_template')

@section('title', 'Update Workflow')
@section('page_title', 'Update Workflow')
@section('page_description', 'learn1thing')

@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{ route('admin.workflow.index') }}">Workflow</a></li>
    <li>Update</li>
</ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        {!! Form::model($workflow,['route' =>['admin.workflow.update',$workflow->id], 'class' =>  'form-horizontal','id' => 'form-workflow']) !!}

        <div class="box">
            <div class="box-body">
                @include( 'admin.setting.workflow.form_edit')
            </div>

            <div class="box-footer text-right">
                <div class="row">
                    <div class="col-xs-12 m-top-10 text-right">
                    <a href="{{ route('admin.workflow.index') }}" class="btn btn-default">
                         <i class="fa fa-times m-right-10"></i> Cancel
                     </a>

                     <button type="submit" class="btn btn-primary">
                        <i class="fa fa-check m-right-10"></i> Update
                    </button>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>

</div>
@endsection
@section('scripts')
{!! Html::script('vendor/jsvalidation/js/jsvalidation.min.js') !!}
{!! JsValidator::formRequest('App\Http\Requests\Setting\WorkflowRequest', '#form-workflow'); !!}
@endsection
