@extends('layouts.admin_template')

@section('title', 'Setting')
@section('page_title', 'Setting')
@section('page_description', 'Setting Appication')

@section('header')
  <!-- iCheck -->
  {!! Html::style('bower_components/AdminLTE/plugins/iCheck/square/blue.css') !!}
  {!! Html::style('css/custom.css') !!}
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Setting</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
              <div class="col-sm-6 col-xs-12 col-sm-offset-3">
                <form class="form-setting">
                  <div class="form-group">
                    <label for="label-switch" class="label-switch">Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</label>
                    <label class="switch">
                      <input type="checkbox">
                      <div class="slider round"></div>
                    </label>
                  </div>
                  <div class="form-group">
                    <label for="label-switch" class="label-switch">Lorem ipsum dolor sit amet</label>
                    <label class="switch">
                      <input type="checkbox">
                      <div class="slider round"></div>
                    </label>
                  </div>
                  <div class="form-group">
                    <label for="label-switch" class="label-switch">Lorem ipsum dolor sit amet</label>
                    <label class="switch">
                      <input type="checkbox">
                      <div class="slider round"></div>
                    </label>
                  </div>
                  <div class="form-group">
                    <label for="label-switch" class="label-switch">Lorem ipsum dolor sit amet</label>
                    <label class="switch">
                      <input type="checkbox">
                      <div class="slider round"></div>
                    </label>
                  </div>
                  <div class="form-group">
                    <label for="label-switch" class="label-switch">Lorem ipsum dolor sit amet</label>
                    <label class="switch">
                      <input type="checkbox">
                      <div class="slider round"></div>
                    </label>
                  </div>
                  <div class="form-group">
                    <div class="show-advance">
                      <button type="button" name="advance-options" class="btn btn-success round-btn btn-show-advance">Show Advance Setting</button>
                    </div>
                  </div>
                  <div class="advance-options" hidden>
                    <h3>Lorem ipsum dolor sit amet</h3>
                    <div class="form-group">
                      <label for="label-switch" class="label-switch">Lorem ipsum dolor sit amet</label>
                      <label class="switch">
                        <input type="checkbox">
                        <div class="slider round"></div>
                      </label>
                    </div>
                    <div class="form-group">
                      <label for="label-switch" class="label-switch">Lorem ipsum dolor sit amet</label>
                      <label class="switch">
                        <input type="checkbox">
                        <div class="slider round"></div>
                      </label>
                    </div>
                    <div class="form-group">
                      <label for="label-switch" class="label-switch">Lorem ipsum dolor sit amet</label>
                      <label class="switch">
                        <input type="checkbox">
                        <div class="slider round"></div>
                      </label>
                    </div>
                    <div class="form-group">
                      <label for="label-switch" class="label-switch">Lorem ipsum dolor sit amet</label>
                      <label class="switch">
                        <input type="checkbox">
                        <div class="slider round"></div>
                      </label>
                    </div>
                  </div>
                </form>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
  <!-- iCheck -->
  {!! Html::script('bower_components/AdminLTE/plugins/iCheck/icheck.min.js') !!}
  <script type="text/javascript">
    var _urlSuccessLogin = '{{ route('admin-dashboard') }}';
  </script>
  {!! Html::script('js/setting/setting.js') !!}
@endsection
