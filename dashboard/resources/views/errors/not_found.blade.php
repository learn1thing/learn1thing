@extends('layouts.error_template')

@section('title', '404 Not Found')

@section('header')
    <style type="text/css">
        .containers {
                text-align: center;
                vertical-align: middle;
                padding: 0 200px;
            }

            div.content {
                width: 100%;
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 40px;
                font-weight: 600;
            }
    </style>
@endsection

@section('content')
    <div class="containers">
        <div class="content" style="margin-top: 150px;">
            <div class="title">Sorry, page not found.
            </div>
            <h3><a style="font-size: 20px;" href="{{route('admin-dashboard')}}">Back to dashboard</a></h3>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(function() {
        $('section.content-header').remove();
    });
</script>
@endsection
