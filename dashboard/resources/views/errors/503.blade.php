@extends('layouts.error_template')

@section('title', '503 Internal Server Error')

@section('header')
    <style type="text/css">
        .containers {
                text-align: center;
                vertical-align: middle;
                padding: 0 200px;
            }

            div.content {
                width: 100%;
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 40px;
                margin-bottom: 40px;
                font-weight: 600;
            }
    </style>
@endsection

@section('content')
    <div class="containers">
        <div class="content" style="margin-top: 150px;">
            <div class="title">Something went wrong, the request went missing on the server. You will be redirected to Dashboard.
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(function() {
        $('section.content-header').remove();

        setTimeout(function() { 
            window.location.href = "{{route('login')}}";
        }, 3000);
    });
</script>
@endsection
