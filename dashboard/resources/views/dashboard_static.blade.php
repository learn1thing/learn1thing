@extends('layouts.admin_template') @section('title', 'Dashboard') @section('page_title', 'Dashboard') @section('page_description', 'learn1thing') @section('header')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"> @endsection @section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
</ol>
@endsection @section('content') @if(user_info()->inRole('super-admin') || user_info()->inRole('business-partner') || user_info()->inRole('corporate-customer') || user_info()->inRole('standard-customer'))
<div class="row">
    @if(user_info()->inRole('standard-customer'))
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3 class="order-completed-count"><i class="fa fa-refresh fa-spin"></i></h3>
                <p>Total transaction in dollars</p>
            </div>
            <div class="icon">
                <i class="fa fa-dollar"></i>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3 class="course-not-started"><i class="fa fa-refresh fa-spin"></i></h3>

                <p>Number of Product Unused</p>
            </div>
            <div class="icon">
                <i class="fa fa-mortar-board"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3 class="expired-product"><i class="fa fa-refresh fa-spin"></i></h3>

                <p>Products expiring in 30 days</p>
            </div>
            <div class="icon">
                <i class="fa fa-clock-o"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3 class="recomended-product"><i class="fa fa-refresh fa-spin"></i></h3>

                <p>Products that are recommended</p>
            </div>
            <div class="icon">
                <i class="fa fa-thumbs-o-up"></i>
            </div>
        </div>
    </div>
    @elseif(user_info()->inRole('corporate-customer'))
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3 class="order-completed-count"><i class="fa fa-refresh fa-spin"></i></h3>
                <p>Total transaction in dollars</p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3 class="payment-summary"><i class="fa fa-refresh fa-spin"></i></h3>
                <p style="line-height: 15px; margin: 0;">Payment system summary
                    <br/><span style="font-size: 12px;">(credit based / pay as you go)</span></p>
            </div>
            <div class="icon">
                <i class="fa fa-credit-card"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3 class="product-left"><i class="fa fa-refresh fa-spin"></i></h3>

                <p style="line-height: 15px; margin: 0;">Total number of product left
                    <br/>to be assigned</p>
            </div>
            <div class="icon">
                <i class="fa fa-cubes"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3 class="product-expired"><i class="fa fa-refresh fa-spin"></i></h3>

                <p>Products expiring in 30 days</p>
            </div>
            <div class="icon">
                <i class="fa fa-clock-o"></i>
            </div>
        </div>
    </div>
    @elseif(user_info()->inRole('business-partner'))
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3 class="product-sold-count"><i class="fa fa-refresh fa-spin"></i></h3>
                <p>Total order in quantity</p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3 class="money-count"><i class="fa fa-refresh fa-spin"></i></h3>

                <p>Total sales in dollars</p>
            </div>
            <div class="icon">
                <i class="fa fa-dollar"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3 class="customer-count"><i class="fa fa-refresh fa-spin"></i></h3>

                <p>Total number of customers</p>
            </div>
            <div class="icon">
                <i class="ion ion-person"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3 class="action-required"><i class="fa fa-refresh fa-spin"></i></h3>

                <p>Total of products</p>
            </div>
            <div class="icon">
                <i class="fa fa-cubes"></i>
            </div>
        </div>
    </div>
    @elseif(user_info()->inRole('super-admin'))
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3 class="product-count"><i class="fa fa-refresh fa-spin"></i></h3>
                <p>Total order in quantity</p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3 class="total-money"><i class="fa fa-refresh fa-spin"></i></h3>

                <p>Total sales in dollars</p>
            </div>
            <div class="icon">
                <i class="fa fa-dollar"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3 class="customer-count"><i class="fa fa-refresh fa-spin"></i></h3>

                <p>Total number of consumer</p>
            </div>
            <div class="icon">
                <i class="ion ion-person"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3 class="business-partner-count"><i class="fa fa-refresh fa-spin"></i></h3>

                <p>Total number of business partner</p>
            </div>
            <div class="icon">
                <i class="ion ion-person"></i>
            </div>
        </div>
    </div>
    @endif
</div>
@endif

<div class="row">
    <div class="col-xs-6 col-sm-6">
        <div class="box">
            <div class="box-body">
                @if(user_info()->inRole('business-partner'))
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-12">
                        <select name="category" class="form-control" style="width: 100%" id="productPurchase">
                            <option value="category">Product Purchase breakdown by Category</option>
                            <option value="product">Product Purchase breakdown by Product</option>
                        </select>
                    </div>
                </div>
                <div id="container9" style="width:100%; height:370px;"></div>
                @else
                <div id="container9" style="width:100%; height:400px;"></div>
                @endif
            </div>
        </div>
    </div>
 
    <div class="col-xs-6 col-sm-6">
        <div class="box">
            <div class="box-body">
            @if(user_info()->inRole('standard-customer'))
                <div id="container-2" style="width:100%; height:400px; overflow-x: auto;">
                    <table class="table">
                        <thead>
                            <th>10 Poducts has yet to start</th>
                        </thead>
                        <tbody id="product-start">
                            <td colspan="1" align="center"><i class="fa fa-refresh fa-spin"></i></td>
                        </tbody>

                    </table>
                </div>  
                @else
                  <div id="container-2" style="width:100%; height:400px; overflow-x: auto;"></div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-6 col-sm-6">
        <div class="box">
            <div class="box-body">
                @if(user_info()->inRole('business-partner'))
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-12">
                        <select name="category" class="form-control" style="width: 100%" id="category-customer">
                            <option value="country">Customer breakdown by Country</option>
                            <!-- <option value="industry">Customer breakdown by Industry</option> -->
                            <option value="b2b_b2c">Customer breakdown by B2B/B2C</option>
                        </select>
                    </div>
                </div>
                <div id="container-3" style="width:100%; height:370px;"></div>
                @elseif(user_info()->inRole('standard-customer'))
                <div id="container-3" style="width:100%; height:400px; overflow-x: auto;">
                    <table class="table">
                        <thead>
                            <th>10 Poducts expiring in 30 days</th>
                        </thead>
                        <tbody id="product-bought">
                            <td colspan="2" align="center"><i class="fa fa-refresh fa-spin"></i></td>
                        </tbody>
                    </table>
                </div>
                
                @elseif(user_info()->inRole('corporate-customer'))
                <div id="container-3" style="width:100%; height:400px; overflow-x: auto;">
                    <h4 class="text-center">Number of product left to be assigned</h4>
                    <table class="table table-striped">
                        <thead>
                            <th class="text-center">#</th>
                            <th>Product Name</th>
                            <th class="text-center">Assigned</th>
                            <th class="text-center">Unassigned</th>
                            <th class="text-center">Completed</th>
                        </thead>
                        <tbody id="product-left-assigned">
                            <tr>
                              <td colspan="5" align="center"><i class="fa fa-refresh fa-spin"></i></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                @else
                <div id="container-3" style="width:100%; height:400px;"></div>
                @endif
            </div>
        </div>
    </div>

    <div class="col-xs-6 col-sm-6">
        <div class="box">
            <div class="box-body">
                <div id="container-4" style="width:100%; height:400px; overflow-x: auto;">
                    @if(user_info()->inRole('corporate-customer'))
                    <h4 class="text-center">List of courses bought and its expiration</h4>
                    <table class="table table-striped">
                        <thead>
                            <th>Course Name</th>
                            <th>Expired On</th>
                        </thead>
                        <tbody id="course-bought">
                            <td colspan="2" align="center"><i class="fa fa-refresh fa-spin"></i></td>
                        </tbody>
                    </table>
                    @endif
                    @if(user_info()->inRole('business-partner'))
                    <h4 class="text-center list-title">Top Five Product Sold</h4>
                    <table class="table table-striped">
                     <thead>
                         <th>#</th>
                         <th>Name</th>
                         <th>Total</th>
                     </thead>
                     <tbody id="list-action-required">
                         <tr>
                             <td colspan="3" align="center">No Available</td>
                         </tr>
                     </tbody>
                    </table>
                    @endif
                    @if(user_info()->inRole('standard-customer'))
                    <table class="table">
                        <thead>
                            <th>10 Products recomended</th>
                        </thead>
                        <tbody id="product-rec">
                            <td colspan="1" align="center"><i class="fa fa-refresh fa-spin"></i></td>
                        </tbody>

                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @if(user_info()->inRole('corporate-customer'))
    <!-- <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-body">
                <div id="container-5" style="width:100%; height:400px; overflow-x: auto;">
                    <h4 class="text-center">Requested Product</h4>
                    <table class="table table-striped">
                        <thead>
                            <th>Product Name</th>
                            <th>User</th>
                            <th>Status</th>
                        </thead>
                        <tbody id="requested-product">
                            <td colspan="3" align="center"><i class="fa fa-refresh fa-spin"></i></td>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> -->
    @endif
</div>
@endsection @section('scripts') {!! Html::script('bower_components/highcharts/highcharts.js') !!} {!! Html::script('bower_components/highcharts/modules/exporting.js') !!}

<!-- JS ANALYTICS -->
@include('analytics_js') @endsection