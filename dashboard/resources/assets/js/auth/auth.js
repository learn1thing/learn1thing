var _AppFunc = (function($) {
  var defineIcheck = function() {
    $('input.remember-me').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%'
    });
  }

  var onReady = function() {
    defineIcheck();
    disabledBtn();
  }

  var onInput = function() {
    disabledBtn();
  }

  var onSubmitBtnLogin = function() {
    var _submitBtn = $('button#btn-login');
    var _spanLabel = $('span.help-block');
    var _hasFeedbackElm = $('div.form-group.has-feedback');
    var _prefixElm = 'input#';
    var _data = {};
    _hasFeedbackElm.removeClass('has-error');
    _spanLabel.html('');
    _submitBtn.prop('disabled', true);
    _data['email']  = $('input#email').val();
    _data['password'] = $('input#password').val();
    _data['remember'] = $('input#remember').prop('checked') ? 1 : 0;
    ajaxService(_baseUrl+'/api/v1/auth', _data, 'POST', function (response) {
      _submitBtn.prop('disabled', false);
      if (response.status.success) {
        window.location.href = _urlSuccessLogin;
      }
    }, function (response) {
      _submitBtn.prop('disabled', false);
      _data = response.responseJSON;
      if (!_data.status.success && _data.status.code === 422) {
        $.each(_data.data, function(key, value) {
          _input = $(_prefixElm+key);
          _input.parent().addClass('has-error');
          _input.next().html(value[0]);
        });
      } else if (!_data.status.success && $.inArray(_data.status.code, [401, 429]) !== -1) {
        alertService(_data.status.message, 5000);
      } else {
        alert('Internal Server Error!');
      }
    })
  }

  var alertService = function(message, interval) {
    var _alert = $('div#alert-message');
    _alert.find('p.message').html(message);
    _alert.show();
    setTimeout(function () {
      _alert.hide();
    }, interval);
  }

  var disabledBtn = function() {
    var _submitBtn = $('button#btn-login');
    var _email = $('input#email');
    var _password = $('input#password');
    if (_email.val() !== '' && _password.val() !== '') {
      _submitBtn.prop('disabled', false);
    } else {
      _submitBtn.prop('disabled', true);
    }
  }

  var ajaxService = function(url, param, method, onSuccess, onError, dataType) {
    $.ajax({
      url: url,
      method: method,
      dataType: dataType,
      data: param,
      success: onSuccess,
      error: onError !== undefined ? onError : function(response) {
        alert('Internal Server Error!');
      }
    });
  }

  return {
    onReady : onReady,
    onSubmitBtnLogin : onSubmitBtnLogin,
    disabledBtn : disabledBtn,
    onInput: onInput
  }
})(jQuery);


var _App = $(document);
_App.ready(function() {
  _AppFunc.onReady();
  $(_App).on('input', 'input#email, input#password', function() {
    _AppFunc.onInput();
  });
  $(_App).on('submit', 'form#form-login', function() {
    $('button#btn-login').html('Processing').prop('disabled', true);
  });
});
