var _AppMain = $(document);
var _AppFunc = (function($) {

    onBtnAdvanceClick = function(button) {
      _advanceFormElm = $('div.advance-options');
      _advanceFormElm.toggle();
      if (_advanceFormElm.css('display') == 'none') {
        button.html('Show Advance Setting');
      } else {
        button.html('Hide Advance Setting');
      }
    }

  return {
    onBtnAdvanceClick : onBtnAdvanceClick
  }
})(jQuery);

_AppMain.ready(function() {
  _AppMain.on('click', 'button.btn-show-advance', function() {
    _AppFunc.onBtnAdvanceClick($(this));
  });
});
