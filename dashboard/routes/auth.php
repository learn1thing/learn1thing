<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Auth routes for your application.
|
*/

Route::group(['namespace' => 'Auth\Api'], function() {
  Route::post('auth', ['as' => 'auth.login', 'uses' => 'AuthenticateController@postLogin']);
  Route::post('logout', ['as' => 'auth.logout', 'uses' => 'AuthenticateController@postLogout']);
  Route::get('token', ['as' => 'auth.token', 'uses' => 'AuthenticateController@getUserFromIp']);
  Route::get('profile', ['as' => 'auth.profile', 'uses' => 'AuthenticateController@getUser']);

  /**
   * @author iddan
   */
  Route::get('register/{register_type?}/{id?}', array('as' => 'auth-register', 'uses' => 'AuthenticateController@getRegister')); //edited ali
  Route::post('register', array('as' => 'auth-register-post', 'uses' => 'AuthenticateController@postRegister'));
  Route::get('detail-country/{country_id}', array('as' => 'auth-detail-country-get', 'uses' => 'AuthenticateController@detailCountry'));
  Route::get('forgot', array('as' => 'auth-forgot', 'uses' => 'AuthenticateController@getForgot'));
  Route::post('forgot', array('as' => 'auth-forgot-post', 'uses' => 'AuthenticateController@postForgot'));
  Route::post('reset-password', array('as' => 'auth-reset-password-post', 'uses' => 'AuthenticateController@postResetPassword'));

  // @author = ali
    Route::post('register-post-social', array('as' => 'auth-register-post-social',
                                            'uses' => 'AuthenticateController@postRegisterSocial'));
    // Route::get('getListBusinessPartner',array('as' => 'api.schedule', 'uses' => 'ScheduleController@getListApiSchedule'));
  // end @author

    Route::post('register-social',['as' => 'auth.register.social-media', 'uses' => 'AuthenticateController@apiRegisterSocial']);
});

Route::get('login', array('as' => 'auth-login', 'uses' => 'Auth\LoginController@index'));
