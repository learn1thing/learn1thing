<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/getloginmoodle',array('as' => 'getloginmoodle', 'uses' => 'TryController@getLoginMoodle'));
Route::get('/test',array('as' => 'test', 'uses' => 'TryController@test'));

Route::group(['middleware' => 'guest'], function() {
    Route::get('/login', array('as' => 'login', 'uses' => 'Auth\LoginController@index'));
    Route::post('/login', array('as' => 'post.login', 'uses' => 'Auth\LoginController@postLogin'));
    Route::get('/social/redirect/{provider}',
            ['as' => 'socialredirect',   'uses' => 'Auth\SocialController@getSocialRedirect']);
    Route::get('/social/handle/{provider}',
            ['as' => 'socialhandle',     'uses' => 'Auth\SocialController@getSocialHandle']);
    Route::post('/social/login',
            ['as' => 'sociallogin',     'uses' => 'Auth\SocialController@getSocialLogin']);

    Route::post('/register', array('as' => 'post.register', 'uses' => 'Auth\RegisterController@store'));
    Route::get('/register/{id}/{code}', array('as' => 'post.register', 'uses' => 'Auth\RegisterController@confirmation'));
    
  Route::get( '/register-business', [ 'as' => 'auth-register-business', 'uses' => 'Auth\Api\AuthenticateController@registerBusiness' ]);
});

Route::get('download-guide', [
    'as' => 'admin.business-partner.download-guide',
    'uses' => 'DashboardController@downloadUserGuide'
]);

Route::get('download-oc-guide', [
    'as' => 'admin.business-partner.download-oc-guide',
    'uses' => 'DashboardController@downloadOcUserGuide'
]);

Route::get('country-detail/{id}', array('as' => 'countr.detail', 'uses' => 'HomeController@getCountryDetail'));

Route::group(['middleware' => 'auth.access'], function(){
    Route::get('/', array('as' => 'admin-dashboard', 'uses' => 'DashboardController@index'));
    Route::get('/analytic', array('as' => 'dashboard.analytic', 'uses' => 'DashboardController@analytic'));
    Route::get('check-log', ['as' => 'admin.check.log', 'uses' => 'HomeController@checkBulkLog']);
    // Route::group(['namespace' => 'Admin\Master'], function(){
    //     Route::get('country-detail/{id}', array('as' => 'countr.detail', 'uses' => 'DashboardController@getCountryDetail'));
    // });

    // Master user
    Route::group(['prefix'=>'admin/master', 'namespace' => 'Admin\Master'], function(){
        // user
        Route::group(['prefix'=>'user'], function(){
            Route::get('',[
                        'as'    =>  'admin.user.index',
                        'uses'  =>  'UserController@index'
                    ]);
            Route::get('edit/{id}', [
                        'as'    =>  'admin.user.edit',
                        'uses'  =>  'UserController@edit'
                    ]);
            Route::delete('destroy/{id}', [
                        'as'    =>  'admin.user.destroy',
                        'uses'  =>  'UserController@destroy'
                    ]);
            Route::patch('update/{id}', [
                        'as' => 'admin.user.update',
                        'uses' => 'UserController@update'
                    ]);
            Route::post('active/{id}', [
                            'as' => 'admin.user.active',
                            'uses' => 'UserController@active'
                        ]);
            Route::get('create', [
                        'as' => 'admin.user.create',
                        'uses' => 'UserController@create'
                    ]);
            Route::post('store', [
                        'as' => 'admin.user.store',
                        'uses' => 'UserController@store'
                    ]);
            Route::get('show/{id}', [
                        'as' => 'admin.user.show',
                        'uses' => 'UserController@show'
                    ]);
            Route::get('profile', [
                        'as' => 'admin.user.profile',
                        'uses' => 'UserController@getProfile',
                        'middleware' => 'hasaccess:manage-profile'
                    ]);
            Route::group(['prefix'=>'profile'], function(){
                Route::post('', [
                            'as' => 'admin.user.profile.update',
                            'uses' => 'UserController@updateProfile',
                            'middleware' => 'hasaccess:manage-profile'
                        ]);
                Route::get('change-password', [
                            'as' => 'admin.user.change-password',
                            'uses' => 'UserController@changePassword',
                            'middleware' => 'hasaccess:manage-profile'
                        ]);
                Route::post('change-password', [
                            'as' => 'admin.user.change-password.update',
                            'uses' => 'UserController@updatePassword',
                            'middleware' => 'hasaccess:manage-profile'
                        ]);
                Route::get('link-account', [
                            'as' => 'admin.user.link-account',
                            'uses' => 'UserController@linkAccount',
                            'middleware' => 'hasaccess:manage-profile'
                        ]);
                Route::post('link-account', [
                            'as' => 'admin.user.link-account.store',
                            'uses' => 'UserController@storeLinkAccount',
                            'middleware' => 'hasaccess:manage-profile'
                        ]);
                Route::get('unlink-account/{id}', [
                            'as' => 'admin.user.unlink-account',
                            'uses' => 'UserController@unlinkAccount',
                            'middleware' => 'hasaccess:manage-profile'
                        ]);
            });

            // standard customer
            Route::group(['prefix'=>'standard-customer'], function(){
                Route::get('',[
                            'as'    =>  'admin.standard-customer.index',
                            'uses'  =>  'StandardCustomerController@index'
                        ]);
                Route::get('edit/{id}', [
                            'as'    =>  'admin.standard-customer.edit',
                            'uses'  =>  'StandardCustomerController@edit'
                        ]);
                Route::delete('destroy/{id}', [
                            'as'    =>  'admin.standard-customer.destroy',
                            'uses'  =>  'StandardCustomerController@destroy'
                        ]);
                Route::patch('update/{id}', [
                            'as' => 'admin.standard-customer.update',
                            'uses' => 'StandardCustomerController@update'
                        ]);
                Route::post('active/{id}', [
                            'as' => 'admin.standard-customer.active',
                            'uses' => 'StandardCustomerController@active'
                        ]);
                Route::delete('deactive/{id}', [
                            'as' => 'admin.standard-customer.deactive',
                            'uses' => 'StandardCustomerController@deactive'
                        ]);
                Route::get('create', [
                            'as' => 'admin.standard-customer.create',
                            'uses' => 'StandardCustomerController@create'
                        ]);
                Route::post('store', [
                            'as' => 'admin.standard-customer.store',
                            'uses' => 'StandardCustomerController@store'
                        ]);
                Route::get('show/{id}', [
                            'as' => 'admin.standard-customer.show',
                            'uses' => 'StandardCustomerController@show'
                        ]);
            });

            // corporate customer
            Route::group(['prefix'=>'corporate-customer'], function(){
                Route::get('',[
                            'as'    =>  'admin.corporate-customer.index',
                            'uses'  =>  'CorporateCustomerController@index'
                        ]);
                Route::get('edit/{id}', [
                            'as'    =>  'admin.corporate-customer.edit',
                            'uses'  =>  'CorporateCustomerController@edit'
                        ]);
                Route::delete('destroy/{id}', [
                            'as'    =>  'admin.corporate-customer.destroy',
                            'uses'  =>  'CorporateCustomerController@destroy'
                        ]);
                Route::patch('update/{id}', [
                            'as' => 'admin.corporate-customer.update',
                            'uses' => 'CorporateCustomerController@update'
                        ]);
                Route::post('active/{id}', [
                            'as' => 'admin.corporate-customer.active',
                            'uses' => 'CorporateCustomerController@active'
                        ]);
                Route::delete('deactive/{id}', [
                            'as' => 'admin.corporate-customer.deactive',
                            'uses' => 'CorporateCustomerController@deactive'
                        ]);
                Route::get('create', [
                            'as' => 'admin.corporate-customer.create',
                            'uses' => 'CorporateCustomerController@create'
                        ]);
                Route::post('store', [
                            'as' => 'admin.corporate-customer.store',
                            'uses' => 'CorporateCustomerController@store'
                        ]);
                Route::get('show/{id}', [
                            'as' => 'admin.corporate-customer.show',
                            'uses' => 'CorporateCustomerController@show'
                        ]);

                
            });


            // modify : fadly .start here
            // business partner
            Route::group(['prefix'=>'business-partner'], function(){
                Route::get('',[
                            'as'    =>  'admin.business-partner.index',
                            'uses'  =>  'BusinessPartnerController@index'
                        ]);
                Route::get('edit/{id}', [
                            'as'    =>  'admin.business-partner.edit',
                            'uses'  =>  'BusinessPartnerController@edit'
                        ]);
                Route::delete('destroy/{id}', [
                            'as'    =>  'admin.business-partner.destroy',
                            'uses'  =>  'BusinessPartnerController@destroy'
                        ]);
                Route::patch('update/{id}', [
                            'as' => 'admin.business-partner.update',
                            'uses' => 'BusinessPartnerController@update'
                        ]);
                Route::post('active/{id}', [
                            'as' => 'admin.business-partner.active',
                            'uses' => 'BusinessPartnerController@active'
                        ]);
                Route::delete('deactive/{id}', [
                            'as' => 'admin.business-partner.deactive',
                            'uses' => 'BusinessPartnerController@deactive'
                        ]);
                Route::get('create', [
                            'as' => 'admin.business-partner.create',
                            'uses' => 'BusinessPartnerController@create'
                        ]);
                Route::post('store', [
                            'as' => 'admin.business-partner.store',
                            'uses' => 'BusinessPartnerController@store'
                        ]);
                Route::get('show/{id}', [
                            'as' => 'admin.business-partner.show',
                            'uses' => 'BusinessPartnerController@show'
                        ]);
            });

            // modify : fadly .end here
        });

        // permisson
        Route::group(['prefix' => 'permission'], function() {
            Route::get('',['as' => 'admin.permission.index', 'uses' => 'PermissionController@index']);
            Route::get('create',['as' => 'admin.permission.create', 'uses' => 'PermissionController@create']);
            Route::post('store',['as' => 'admin.permission.store', 'uses' => 'PermissionController@store']);
            Route::post('set',['as' => 'admin.permission.set', 'uses' => 'PermissionController@setPermission']);
            Route::post('unset',['as' => 'admin.permission.unset', 'uses' => 'PermissionController@unsetPermission']);
            Route::get('manage/{slug}',['as' => 'admin.permission.manage', 'uses' => 'PermissionController@manage']);
        });
    });

    Route::post('/logout', array('as' => 'logout', 'uses' => 'Auth\LoginController@logout'));


    Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function() {
        Route::get('/setting', ['as' => 'admin.setting', 'uses' => 'Setting\SettingController@index']);

        // author : fadly .start here
        Route::group(['prefix'=>'bulk', 'namespace' => 'Master'], function(){
            Route::get('',[
                        'as'    =>  'admin.business-partner.bulk.index',
                        'uses'  =>  'BulkUserCreationController@getDataSubUser'
                    ]);
            // Author: Irvan
            Route::get('bulk-creation-user', [
                        'as' => 'admin.business-partner.bulk-user',
                        'uses' => 'BulkUserCreationController@bulkCreationUser'
                    ]);

            Route::post('store-bulk-user', [
                        'as' => 'admin.business-partner.store.bulk-user',
                        'uses' => 'BulkUserCreationController@storeBulk'
                    ]);

            Route::get('create-single-user', [
                        'as' => 'admin.business-partner.create.single-user',
                        'uses' => 'BulkUserCreationController@createSingleUser'
                    ]);

            Route::post('store-single-user', [
                        'as' => 'admin.business-partner.store.single-user',
                        'uses' => 'BulkUserCreationController@storeSingleUser'
                    ]);

            Route::get('edit/{id}', [
                        'as'    =>  'admin.business-partner.bulk.edit',
                        'uses'  =>  'BulkUserCreationController@editUser'
                    ]);
            Route::delete('destroy/{id}', [
                        'as'    =>  'admin.business-partner.bulk.destroy',
                        'uses'  =>  'BulkUserCreationController@destroyUser'
                    ]);
            Route::patch('update/{id}', [
                        'as'    =>  'admin.business-partner.bulk.update',
                        'uses'  =>  'BulkUserCreationController@updateUser'
                    ]);

            Route::get('download-sample', [
                        'as' => 'admin.business-partner.download-sample',
                        'uses' => 'BulkUserCreationController@downloadSample'
                    ]);
        });
            
        Route::group(['prefix'=>'management-group', 'namespace' => 'Master'], function(){

            Route::get('group-user', [
                        'as' => 'admin.business-partner.group-user',
                        'uses' => 'BusinessPartnerController@groupUser'
                    ]);
            Route::get('create-group-user', [
                        'as' => 'admin.business-partner.create-group-user',
                        'uses' => 'BusinessPartnerController@createGroupUser'
                    ]);

            Route::post('store-group-user', [
                        'as' => 'admin.business-partner.store.group-user',
                        'uses' => 'BusinessPartnerController@storeGroupUser'
                    ]);
            Route::get('edit/{id}', [
                        'as'    =>  'admin.business-partner.store.group-user.edit',
                        'uses'  =>  'BusinessPartnerController@editGroup'
                    ]);
            Route::delete('destroy/{id}', [
                        'as'    =>  'admin.business-partner.store.group-user.destroy',
                        'uses'  =>  'BusinessPartnerController@destroyGroup'
                    ]);
            Route::get('manage/{id}', [
                        'as'    =>  'admin.business-partner.store.group-user.manage',
                        'uses'  =>  'BusinessPartnerController@manageGroup'
                    ]);
            Route::post('set',['as' => 'admin.business-partner.store.group-user.set', 'uses' => 'BusinessPartnerController@setPermission']);
            Route::post('unset',['as' => 'admin.business-partner.store.group-user.unset', 'uses' => 'BusinessPartnerController@unsetPermission']);
            Route::patch('update/{id}', [
                        'as'    =>  'admin.business-partner.store.group-user.update',
                        'uses'  =>  'BusinessPartnerController@updateGroup'
                    ]);
            // End Irvan

        });

        Route::group(['prefix'=>'corporate/mini-store', 'namespace' => 'Master', 'middleware' => 'hasaccess:mini-store'], function(){
                
            Route::get('index',[
                        'as'    =>  'admin.corporate-customer.mini-store',
                        'uses'  =>  'CorporateCustomerController@miniStore'
                    ]);

            Route::get('autocomplete',[
                        'as'=>'autocomplete',
                        'uses'=>'CorporateCustomerController@autocompleteProductMiniStore'
                    ]);

            Route::post('enable',[
                        'as'=>'enable-product',
                        'uses'=>'CorporateCustomerController@enableProduct'
                    ]);

            Route::post('disable',[
                        'as'=>'disable-product',
                        'uses'=>'CorporateCustomerController@disableProduct'
                    ]);

            Route::post('request',[
                        'as'=>'request-product',
                        'uses'=>'CorporateCustomerController@requestProduct'
                    ]);

            Route::get('approve',[
                        'as'=>'approve-product',
                        'uses'=>'CorporateCustomerController@approveProduct'
                    ]);
        });

        Route::group(['prefix'=>'corporate/product', 'namespace' => 'Master'], function(){
                
            Route::get('requested-product',[
                        'as'    =>  'admin.corporate-customer.requested-product',
                        'uses'  =>  'CorporateCustomerController@requestedProduct'
                    ]);

            Route::post('approve/{product_code}/{user_id}',['as' => 'admin.corporate-customer.requested-product.approve', 'uses' => 'CorporateCustomerController@approveRequested']);
            
            Route::post('decline/{product_code}/{user_id}',['as' => 'admin.corporate-customer.requested-product.decline', 'uses' => 'CorporateCustomerController@declineRequested']);

            Route::get('request-product-list',[
                        'as'    =>  'admin.corporate-customer.request-product-list',
                        'uses'  =>  'CorporateCustomerController@requestProductList'
                    ]);
            Route::get('request-product-list/{id}',[
                        'as'    =>  'admin.corporate-customer.request-product-list.detail',
                        'uses'  =>  'CorporateCustomerController@requestProductListDetail'
                    ]);
        });

        Route::group(['prefix'=>'product', 'namespace' => 'Master'], function(){

            Route::get('add-product', [
                        'as' => 'admin.business-partner.create-product',
                        'uses' => 'BusinessPartnerProductController@create'
                    ]);

            Route::get('edit-product/{id}', [
                        'as' => 'admin.business-partner.edit-product',
                        'uses' => 'BusinessPartnerProductController@editProduct'
                    ]);

            Route::delete('destroy-product/{id}', [
                        'as' => 'admin.business-partner.destroy-product',
                        'uses' => 'BusinessPartnerProductController@destroyProduct'
                    ]);

            Route::get('manufacture/autocomplete', [
                        'as' => 'admin.business-partner.manufacture.autocomplete',
                        'uses' => 'BusinessPartnerProductController@manufactureAutocomplete'
                    ]);

            Route::get('category/autocomplete', [
                        'as' => 'admin.business-partner.category.autocomplete',
                        'uses' => 'BusinessPartnerProductController@categoryAutocomplete'
                    ]);

            Route::get('attribute/autocomplete', [
                        'as' => 'admin.business-partner.attribute.autocomplete',
                        'uses' => 'BusinessPartnerProductController@attributeAutocomplete'
                    ]);

            Route::get('product-option/autocomplete', [
                        'as' => 'admin.business-partner.product-option.autocomplete',
                        'uses' => 'BusinessPartnerProductController@productOptionAutocomplete'
                    ]);

            Route::get('product-related/autocomplete', [
                        'as' => 'admin.business-partner.product-related.autocomplete',
                        'uses' => 'BusinessPartnerProductController@productRelatedAutocomplete'
                    ]);

            Route::get('set-product', [
                        'as' => 'admin.business-partner.product',
                        'uses' => 'BusinessPartnerProductController@index'
                    ]);

            Route::get('bulk-product', [
                        'as' => 'admin.business-partner.bulk-product',
                        'uses' => 'BusinessPartnerProductController@bulkProduct'
                    ]);

            Route::post('bulk-product/store', [
                        'as' => 'admin.business-partner.bulk-product.store',
                        'uses' => 'BusinessPartnerProductController@bulkProductStore'
                    ]);

            Route::get('bulk-product/sample-download', [
                        'as' => 'admin.business-partner.bulk-product.sample-download',
                        'uses' => 'BusinessPartnerProductController@bulkProductSampleDownload'
                    ]);

            Route::post('store-set-product', [
                        'as' => 'admin.business-partner.product.set',
                        'uses' => 'BusinessPartnerProductController@storeProductSet'
                    ]);

            Route::get('set-product/edit/{id}', [
                        'as'    =>  'admin.business-partner.product.set.edit',
                        'uses'  =>  'BusinessPartnerProductController@editProductSet'
                    ]);
            Route::delete('set-product/destroy/{id}', [
                        'as'    =>  'admin.business-partner.product.set.destroy',
                        'uses'  =>  'BusinessPartnerProductController@destroyProductSet'
                    ]);
            

            Route::post('store', [
                        'as' => 'admin.business-partner.product.store',
                        'uses' => 'BusinessPartnerProductController@storeProduct'
                    ]);            
            Route::post('update/{id}', [
                        'as' => 'admin.business-partner.product.update',
                        'uses' => 'BusinessPartnerProductController@updateProduct'
                    ]);
            Route::get('list', [
                        'as' => 'admin.business-partner.product.list',
                        'uses' => 'BusinessPartnerProductController@productList'
                    ]);
            Route::post('reset-alert/{id}', [
                        'as' => 'admin.business-partner.restart-url-product',
                        'uses' => 'BusinessPartnerProductController@resetAlertProduct'
                    ]);

            // End Irvan

            Route::get('assign-product', [
                        'as' => 'admin.business-partner.assign-product',
                        'uses' => 'BusinessPartnerProductController@assignProduct'
                    ]);

            Route::post('store-assign-product', [
                        'as' => 'admin.business-partner.assign-product-store',
                        'uses' => 'BusinessPartnerProductController@storeProductAssign'
                    ]);

            Route::delete('destroy-product-assign/{id}', [
                        'as' => 'admin.business-partner.product.assign-destroy',
                        'uses' => 'BusinessPartnerProductController@destroyProductAssign'
                    ]);


        });

        Route::group(['prefix'=>'management-group-corporate', 'namespace' => 'Master'], function(){

            Route::get('group-user', [
                        'as' => 'admin.corporate-customer.group-user',
                        'uses' => 'CorporateCustomerController@groupUser'
                    ]);
            Route::get('create-group-user', [
                        'as' => 'admin.corporate-customer.create-group-user',
                        'uses' => 'CorporateCustomerController@createGroupUser'
                    ]);

            Route::post('store-group-user', [
                        'as' => 'admin.corporate-customer.store.group-user',
                        'uses' => 'CorporateCustomerController@storeGroupUser'
                    ]);
            Route::get('edit/{id}', [
                        'as'    =>  'admin.corporate-customer.store.group-user.edit',
                        'uses'  =>  'CorporateCustomerController@editGroup'
                    ]);
            Route::delete('destroy/{id}', [
                        'as'    =>  'admin.corporate-customer.store.group-user.destroy',
                        'uses'  =>  'CorporateCustomerController@destroyGroup'
                    ]);
            Route::get('manage/{id}', [
                        'as'    =>  'admin.corporate-customer.store.group-user.manage',
                        'uses'  =>  'CorporateCustomerController@manageGroup'
                    ]);
            Route::post('set',['as' => 'admin.corporate-customer.store.group-user.set', 'uses' => 'CorporateCustomerController@setPermission']);
            Route::post('unset',['as' => 'admin.corporate-customer.store.group-user.unset', 'uses' => 'CorporateCustomerController@unsetPermission']);
            Route::patch('update/{id}', [
                        'as'    =>  'admin.corporate-customer.store.group-user.update',
                        'uses'  =>  'CorporateCustomerController@updateGroup'
                    ]);
            // End Irvan

        });
            
        // Ali
        Route::group(['prefix'=>'coach/course', 'namespace' => 'Master\Coach'], function(){
            Route::get('',[
                        'as'    =>  'coach.course.index',
                        'uses'  =>  'CourseController@index'
                    ]);
            Route::get('edit/{id}', [
                        'as'    =>  'coach.course.edit',
                        'uses'  =>  'CourseController@edit'
                    ]);
            Route::delete('destroy/{id}', [
                        'as'    =>  'coach.course.destroy',
                        'uses'  =>  'CourseController@destroy'
                    ]);
            Route::patch('update/{id}', [
                        'as'    =>  'coach.course.update',
                        'uses'  =>  'CourseController@update'
                    ]);
            Route::get('create', [
                        'as'    =>  'coach.course.create',
                        'uses'  =>  'CourseController@create'
                    ]);
            Route::post('store', [
                        'as'    =>  'coach.course.store',
                        'uses'  =>  'CourseController@store'
                    ]);
            Route::get('show/{id}', [
                        'as'    =>  'coach.course.show',
                        'uses'  =>  'CourseController@show'
                    ]);
            Route::get('lists',[
                        'as'    => 'coach.course.lists',
                        'uses'  => 'CourseController@ajax_list'
                    ]);
        });

        Route::group(['prefix'=>'coach/schedule', 'namespace' => 'Master\Coach'], function(){
            Route::get('',[
                        'as'    =>  'coach.schedule.index',
                        'uses'  =>  'ScheduleController@index'
                    ]);
            Route::get('edit/{id}', [
                        'as'    =>  'coach.schedule.edit',
                        'uses'  =>  'ScheduleController@edit'
                    ]);
            Route::delete('destroy/{id}', [
                        'as'    =>  'coach.schedule.destroy',
                        'uses'  =>  'ScheduleController@destroy'
                    ]);
            Route::patch('update/{id}', [ 
                        'as'    =>  'coach.schedule.update',
                        'uses'  =>  'ScheduleController@update'
                    ]);
            Route::get('create/{id}', [ 
                        'as'    =>  'coach.schedule.create',
                        'uses'  =>  'ScheduleController@create'
                    ]);
            Route::post('store', [ 
                        'as'    =>  'coach.schedule.store',
                        'uses'  =>  'ScheduleController@store'
                    ]);
            Route::get('show/{id}', [ 
                        'as'    =>  'coach.schedule.show',
                        'uses'  =>  'ScheduleController@show'
                    ]);
        });
        // End Ali
        // author : fadly .end here

        Route::group(['prefix' => 'product', 'namespace' => 'Master'], function() {
            Route::get('',['as' => 'admin.order.product', 'uses' => 'OrderController@productList', 'middleware' => 'hasaccess:bulk-order']);
        });
        //order
        Route::group(['prefix' => 'order', 'namespace' => 'Master'], function() {
            Route::get('',['as'    =>  'admin.order.index','uses'  =>  'OrderController@index', 'middleware' => 'hasaccess:manage-order']);
            Route::get('detail-order/{id}',['as'    =>  'admin.order.detail','uses'  =>  'OrderController@detailOrder', 'middleware' => 'hasaccess:manage-order']);
            Route::get('edit/{id}',['as' => 'admin.order.edit', 'uses' => 'OrderController@editOrder', 'middleware' => 'hasaccess:manage-order']);
            Route::patch('update/{id}',['as' => 'admin.order.update', 'uses' => 'OrderController@updateOrder', 'middleware' => 'hasaccess:manage-order']);
            Route::delete('destroy/{id}',['as' => 'admin.order.destroy', 'uses' => 'OrderController@deleteOrder', 'middleware' => 'hasaccess:manage-order']);
            Route::patch('approve/{id}',['as' => 'admin.order.approve', 'uses' => 'OrderController@approveOrder', 'middleware' => 'hasaccess:all,manage-order']);
            Route::patch('decline/{id}',['as' => 'admin.order.decline', 'uses' => 'OrderController@declineOrder', 'middleware' => 'hasaccess:all,manage-order']);
            Route::post('send-product-order/{product_id}/{user_id}',['as' => 'admin.order.send-product-order', 'uses' => 'OrderController@sendProdcutOrder', 'middleware' => 'hasaccess:manage-order']);

            Route::group(['prefix'=>'bulk', 'middleware' => 'hasaccess:bulk-order'], function(){
                Route::get('create',['as' => 'admin.order.bulk.create', 'uses' => 'OrderController@createBulkOrder', 'middleware' => 'hasaccess:bulk-order']);
                Route::post('store',['as' => 'admin.order.bulk.store', 'uses' => 'OrderController@storeBulkOrder', 'middleware' => 'hasaccess:bulk-order']);
                Route::post('edit/{id}',['as' => 'admin.order.bulk.edit', 'uses' => 'OrderController@editBulkOrder', 'middleware' => 'hasaccess:bulk-order']);
                Route::patch('update/{id}',['as' => 'admin.order.bulk.update', 'uses' => 'OrderController@updateBulkOrder', 'middleware' => 'hasaccess:bulk-order']);
            });

            Route::get('order-history',['as' => 'admin.order.history', 'uses' => 'OrderController@orderHistory']);
            Route::get('order-history/detail/{id}',['as' => 'admin.order.history.detail', 'uses' => 'OrderController@orderHistoryDetail']);
            Route::post('order-history/add',['as' => 'admin.order.history.add', 'uses' => 'OrderController@orderHistoryAdd']);
            Route::get('detail-product',[
                'as'    =>  'admin.order.detail-product',
                'uses'  =>  'OrderController@detailProductBP',
            ]);
        });

        // Departement
        Route::group(['prefix'=>'department', 'namespace' => 'Master'], function(){
            Route::get('',[
                        'as'    =>  'admin.department.index',
                        'uses'  =>  'DepartmentController@index'
                    ]);
            Route::get('edit/{id}', [
                        'as'    =>  'admin.department.edit',
                        'uses'  =>  'DepartmentController@edit'
                    ]);
            Route::delete('destroy/{id}', [
                        'as'    =>  'admin.department.destroy',
                        'uses'  =>  'DepartmentController@destroy'
                    ]);
            Route::patch('update/{id}', [
                        'as' => 'admin.department.update',
                        'uses' => 'DepartmentController@update'
                    ]);
            Route::post('active/{id}', [
                        'as' => 'admin.department.active',
                        'uses' => 'DepartmentController@active'
                    ]);
            Route::get('create', [
                        'as' => 'admin.department.create',
                        'uses' => 'DepartmentController@create'
                    ]);
            Route::post('store', [
                        'as' => 'admin.department.store',
                        'uses' => 'DepartmentController@store'
                    ]);
            Route::get('show/{id}', [
                        'as' => 'admin.department.show',
                        'uses' => 'DepartmentController@show'
                    ]);
        });

        // Register Page
        Route::group(['prefix'=>'register-page', 'namespace' => 'Master'], function(){
            Route::get('',[
                        'as'    =>  'admin.register-page.index',
                        'uses'  =>  'RegisterPageController@index'
                    ]);
            Route::get('edit/{id}', [
                        'as'    =>  'admin.register-page.edit',
                        'uses'  =>  'RegisterPageController@edit'
                    ]);
            Route::patch('update/{id}', [
                        'as' => 'admin.register-page.update',
                        'uses' => 'RegisterPageController@update'
                    ]);
        });

        // credit
        Route::group(['prefix'=>'credit', 'namespace' => 'Master'], function() {
            Route::get('',[
                    'as'    =>  'admin.credit.index',
                    'uses'  =>  'CreditHistoryController@index',
                ]);
            Route::get('history',[
                    'as'    =>  'admin.credit.history',
                    'uses'  =>  'CreditHistoryController@history',
                ]);
            Route::post('store',[
                    'as'    =>  'admin.credit.store',
                    'uses'  =>  'CreditHistoryController@store',
                    'middleware' => 'hasaccess:all,top-up-credit'
                ]);
            Route::post('approved',[
                    'as'    =>  'admin.credit.approved',
                    'uses'  =>  'CreditHistoryController@approvedCredit',
                    'middleware' => 'hasaccess:all'
                ]);
        });

        Route::group(['prefix'=>'invoice', 'namespace' => 'Master'], function() {
            Route::get('',[
                    'as'    =>  'admin.invoice.index',
                    'uses'  =>  'InvoiceController@index',
                    'middleware' => 'hasaccess:all'
                ]);
            Route::get('{month}/{year}',[
                    'as'    =>  'admin.invoice.detail',
                    'uses'  =>  'InvoiceController@detail',
                    'middleware' => 'hasaccess:all'
                ]);
            Route::get('send-mail',[
                    'as'    =>  'admin.invoice.send-mail',
                    'uses'  =>  'InvoiceController@sendMail',
                    'middleware' => 'hasaccess:all'
                ]);
            Route::get('update',[
                    'as'    =>  'admin.invoice.update',
                    'uses'  =>  'InvoiceController@updateStatusOrder',
                    'middleware' => 'hasaccess:all'
                ]);
            Route::get('print-pdf',[
                    'as'    =>  'admin.invoice.print-pdf',
                    'uses'  =>  'InvoiceController@printPDF',
                    'middleware' => 'hasaccess:all'
                ]);
        });

        Route::group(['prefix'=>'assign-product-partner', 'namespace' => 'Master'], function() {
            Route::get('',[
                    'as'    =>  'admin.assign-product.index',
                    'uses'  =>  'ProductController@assignProduct',
                    'middleware' => 'hasaccess:all'
                ]);
            Route::get('create',[
                    'as'    =>  'admin.assign-product.create',
                    'uses'  =>  'ProductController@assignProductCreate',
                    'middleware' => 'hasaccess:all'
                ]);
            Route::get('edit/{id}',[
                    'as'    =>  'admin.assign-product.edit',
                    'uses'  =>  'ProductController@assignProductEdit',
                    'middleware' => 'hasaccess:all'
                ]);
            Route::post('store',[
                    'as'    =>  'admin.assign-product.store',
                    'uses'  =>  'ProductController@assignProductStore',
                    'middleware' => 'hasaccess:all'
                ]);
            Route::patch('update/{id}',[
                    'as'    =>  'admin.assign-product.update',
                    'uses'  =>  'ProductController@assignProductUpdate',
                    'middleware' => 'hasaccess:all'
                ]);
            Route::delete('delete/{id}',[
                    'as'    =>  'admin.assign-product.delete',
                    'uses'  =>  'ProductController@assignProductDelete',
                    'middleware' => 'hasaccess:all'
                ]);
        });

        Route::group(['prefix' => 'change-payment-method', 'namespace' => 'Master'], function() {
            Route::get('',[
                'as' => 'admin.change-payment-method.index',
                'uses' => 'UserController@changePaymentMethod',
                'middleware' => 'hasaccess:all']);

            Route::patch('approve/{id}',[
                'as' => 'admin.change-payment-method.approve',
                'uses' => 'UserController@changePaymentMethodApprove',
                'middleware' => 'hasaccess:all']);
        });

        Route::group(['prefix' => 'bulk-user', 'namespace' => 'Master', 'middleware' => 'hasaccess:all'], function() {
            Route::get('', ['as' => 'admin.bulk-user.index', 'uses' => 'BulkUserCreationController@adminCreateBulk']);
            Route::post('store', ['as' => 'admin.bulk-user.store', 'uses' => 'BulkUserCreationController@adminStoreBulk']);
        });

    });

    // fadly (route coaching role) .start here
    Route::group(['prefix' => 'coach', 'namespace' => 'Admin\Master\Coach'], function() {
        Route::get('',['as' => 'coach.schedule.course.index', 'uses' => 'CoachController@index']);
        Route::get('create',['as' => 'coach.schedule.course.create', 'uses' => 'CoachController@create']);
        
        Route::post('store',['as' => 'coach.schedule.course.store', 'uses' => 'CoachController@store']);
        Route::delete('destroy/{id}',['as' => 'coach.schedule.delete', 'uses' => 'CoachController@destroy']);
        
        Route::get('create/event/{id}',['as' => 'coach.schedule.course.create_event', 'uses' => 'CoachController@createEvent']);
        Route::post('/store/event/{id}',['as' => 'coach.schedule.course.store_event', 'uses' => 'CoachController@storeEvent']);

        Route::get('show/{id}',['as' => 'coach.schedule.show', 'uses' => 'CoachController@show']);
        Route::get('edit/{id}',['as' => 'coach.schedule.edit', 'uses' => 'CoachController@edit']);
        Route::put('update/{id}',['as' => 'coach.schedule.update', 'uses' => 'CoachController@update']);
        Route::post('event/delete/{id}',['as' => 'coach.schedule.delete_event', 'uses' => 'CoachController@deleteEvent']);
        Route::delete('schedule/reset/{id}',['as' => 'coach.schedule.reset', 'uses' => 'CoachController@resetScheduleCoach']);

    });

    Route::group(['prefix' => 'admin/coach', 'namespace' => 'Admin\Master\Coach'], function() {
        Route::get('list', [
                'as' => 'admin.coach.index',
                'uses' => 'CoachController@coachList',
                'middleware' => 'hasaccess:all'
            ]);
    });
    // fadly (route coaching role) .end here

    // Master Schedule
    Route::group(['prefix' => 'master-schedule'], function() {

        Route::get('', array('as' => 'master.schedule', 'uses' => 'Admin\Master\Coach\ScheduleController@masterSchedule'));
        Route::get('create', array('as' => 'master.schedule.create', 'uses' => 'Admin\Master\Coach\ScheduleController@masterScheduleCreate'));
        Route::post('create', array('as' => 'master.schedule.store', 'uses' => 'Admin\Master\Coach\ScheduleController@storeMasterSchedule'));
    });

    Route::delete('destroy/{id}', array('as' => 'master.schedule.destroy', 'uses' => 'Admin\Master\Coach\ScheduleController@deleteMasterSchedule'));

    // End Master Schedule

    // master Expired Schedule
    Route::group(['prefix' => 'master-schedule/expired-schedule'], function() {

        Route::get('', array('as' => 'master.schedule.expired.index', 'uses' => 'Admin\Master\Coach\ExpiredSchedulesController@index'));
        Route::get('create', array('as' => 'master.schedule.expired.create', 'uses' => 'Admin\Master\Coach\ExpiredSchedulesController@create'));
        Route::post('store', array('as' => 'master.schedule.expired.store', 'uses' => 'Admin\Master\Coach\ExpiredSchedulesController@store'));
        Route::delete('destroy/{id}', array('as' => 'master.schedule.expired.destroy', 'uses' => 'Admin\Master\Coach\ExpiredSchedulesController@destroy'));
        Route::get('edit/{id}', array('as' => 'master.schedule.expired.edit', 'uses' => 'Admin\Master\Coach\ExpiredSchedulesController@edit'));
        Route::put('update/{id}', array('as' => 'master.schedule.expired.update', 'uses' => 'Admin\Master\Coach\ExpiredSchedulesController@update'));
        
    });
    // End Expired Schedule
     
    // Yoga ( route for change role )
    Route::get( 'admin/change-role/{slug}', [ 'as' => 'change.role', 'uses' => 'Admin\Setting\LogRoleController@change_roles' ] );
     Route::group( ['prefix' => 'admin/workflow', 'namespace' => 'Admin\Setting'] , function() {
        Route::get( '/', [ 'as' => 'admin.workflow.index', 'uses' => 'WorkflowController@index' ] );
        Route::get( 'create', [ 'as' => 'admin.workflow.create', 'uses' => 'WorkflowController@create' ] );
        Route::get( 'edit/{id}', [ 'as' => 'admin.workflow.edit', 'uses' => 'WorkflowController@edit' ] );
        Route::post( 'store', [ 'as' => 'admin.workflow.store', 'uses' => 'WorkflowController@store' ] );
        Route::post( 'update/{id}', [ 'as' => 'admin.workflow.update', 'uses' => 'WorkflowController@update' ] );
        Route::delete( 'destroy/{id}', [ 'as' => 'admin.workflow.destroy', 'uses' => 'WorkflowController@destroy' ] );

        // Ajax
        Route::get( 'getproduct', [ 'as' => 'admin.workflow.getproduct', 'uses' => 'WorkflowController@getProduct' ] );
        Route::get( 'getuser', [ 'as' => 'admin.workflow.getuser', 'uses' => 'WorkflowController@getRecipe' ] );
        Route::get( 'getaccess', [ 'as' => 'admin.workflow.getaccess', 'uses' => 'WorkflowController@getAccess' ] );
     });

    // Email Template
    Route::group( ['prefix' => 'admin/emailtemplate', 'namespace' => 'Admin\Master'] , function() {
        Route::get( '/', [ 'as' => 'admin.emailtemplate.index', 'uses' => 'EmailTemplateController@index' ] );
        Route::get( 'create', [ 'as' => 'admin.emailtemplate.create', 'uses' => 'EmailTemplateController@create' ] );
        Route::get( 'edit/{id}', [ 'as' => 'admin.emailtemplate.edit', 'uses' => 'EmailTemplateController@edit' ] );
        Route::post( 'store', [ 'as' => 'admin.emailtemplate.store', 'uses' => 'EmailTemplateController@store' ] );
        Route::patch( 'update/{id}', [ 'as' => 'admin.emailtemplate.update', 'uses' => 'EmailTemplateController@update' ] );
        Route::delete( 'destroy/{id}', [ 'as' => 'admin.emailtemplate.destroy', 'uses' => 'EmailTemplateController@destroy' ] );
     });     
    //end

    Route::get('/schedule/coach/edit/{course_id}/{id}', array('as' => 'coach.schedule.order.edit', 'uses' => 'Admin\Master\Coach\ScheduleController@editScheduleOrder'));

    Route::post('/schedule/coach/update', array('as' => 'coach.schedule.order.update', 'uses' => 'Admin\Master\Coach\ScheduleController@updateScheduleOrder'));

});


Route::post('/schedule/coach/submit-order', array('as' => 'schedule.coach.submit.order', 'uses' => 'Admin\Master\Coach\ScheduleController@submitLogOrderSchedule'));

Route::get('/schedule/coach/detail/{course_id}/{id}', array('as' => 'detail.coach', 'uses' => 'Admin\Master\Coach\ScheduleController@detailCoach'));

Route::get('/schedule/{id?}', array('as' => 'coach.scheduler', 'uses' => 'Admin\Master\Coach\ScheduleController@showCoach'));




