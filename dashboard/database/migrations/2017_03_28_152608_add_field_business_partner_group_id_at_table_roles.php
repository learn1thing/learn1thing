<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldBusinessPartnerGroupIdAtTableRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roles', function (Blueprint $table) {
            $table->dropColumn(['created_by']);
        });

        Schema::table('roles', function (Blueprint $table) {
            $table->integer('business_partner_group_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('roles', function (Blueprint $table) {
            $table->integer('created_by')->nullable();
        });

        Schema::table('roles', function (Blueprint $table) {
            $table->dropColumn(['business_partner_group_id']);
        });
    }
}
