<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCreditHistories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('credit_histories', function (Blueprint $table) {
            $table->decimal('total_payment',20,0)->default(0);
            $table->integer('day_expired_credit')->nullable()->default(0);
            $table->string('status')->nullable()->default('pending');
            $table->dateTime('date_approved')->nullable();
            $table->string('type_created')->nullable()->default('admin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('credit_histories', function (Blueprint $table) {
            $table->dropColumn(['total_payment','day_expired_credit','status','type_created','date_approved']);
        });
    }
}
