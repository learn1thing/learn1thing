<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtendsFieldCoachSchedules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coach_schedules', function (Blueprint $table) {
            $table->string('location')->after('end_time')->nullable();
            $table->text('comments')->after('location')->nullable();
            $table->boolean('choice')->after('comments')->nullable();
            $table->smallInteger('status')->after('choice')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coach_schedules', function (Blueprint $table) {
            $table->dropColumn(['location','comments','choice','status']);
        });
    }
}