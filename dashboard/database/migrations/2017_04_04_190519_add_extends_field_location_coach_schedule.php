<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtendsFieldLocationCoachSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coach_schedules', function (Blueprint $table) {
            $table->smallInteger('devine_slots')->after('choice')->nullable();
            $table->integer('duration_slots')->after('devine_slots')->nullable();
            $table->integer('break_slots')->after('duration_slots')->nullable();
            $table->boolean('overlap_slots')->after('break_slots')->nullable();
            $table->integer('multiple_allow')->after('overlap_slots')->nullable();
            $table->boolean('display_appoinment')->after('multiple_allow')->nullable();
            $table->smallInteger('email_reminder')->after('display_appoinment')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coach_schedules', function (Blueprint $table) {
            $table->dropColumn(['devine_slots','duration_slots','break_slots','overlap_slots','multiple_allow','display_appoinment','email_reminder']);
        });
    }
}
