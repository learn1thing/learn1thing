<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLogOrderSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_order_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('coach_schedule_id')->nullable();
            $table->integer('course_id')->nullable();
            $table->text('code')->nullable();
            $table->boolean('status')->nullable()->default(false);
            $table->longText('details')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_order_schedules');
    }
}
