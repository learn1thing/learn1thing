<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProductAssign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_assigns', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('business_partner_id')->nullable()->default(0);
            $table->integer('corporate_id')->nullable()->default(0);
            $table->integer('product_id')->nullable()->default(0);

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_assigns');

    }
}
