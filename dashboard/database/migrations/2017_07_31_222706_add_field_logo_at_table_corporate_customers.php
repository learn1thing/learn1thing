<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldLogoAtTableCorporateCustomers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('corporate_customers', function (Blueprint $table) {
            $table->text('logo')->nullable();
            $table->text('description')->nullable();
            $table->text('banner')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('corporate_customers', function (Blueprint $table) {
            $table->dropColumn(['logo','description','banner']);
        });
    }
}
