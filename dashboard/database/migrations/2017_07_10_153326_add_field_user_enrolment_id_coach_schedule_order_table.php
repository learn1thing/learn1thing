<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldUserEnrolmentIdCoachScheduleOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coach_schedule_orders', function (Blueprint $table) {
            $table->string('user_enrolments_id')->default('[]');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coach_schedule_orders', function (Blueprint $table) {
            $table->dropColumn(['user_enrolments_id']);
        });
    }
}
