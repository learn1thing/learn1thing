<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkflowTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workflow', function (Blueprint $table) {
            $table->increments('id');
            $table->integer( 'product_category_id' )->nullable();
            $table->integer( 'product_id' )->nullable();
            $table->integer( 'action' )->nullable();
            $table->integer( 'action_template' )->nullable();
            $table->string( 'recipient_role' )->nullable();
            $table->integer( 'recipient_user_id' )->nullable();
            $table->integer( 'recipient_user' )->nullable();
            $table->string( 'recipient_department' )->nullable();
            $table->datetime( 'offset' )->nullable();
            $table->string( 'event_trigger' )->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'workflow' );
    }
}
