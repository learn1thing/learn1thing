<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomeColumnToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('fax', 20)->nullable();
            $table->string('company', 50)->nullable();
            $table->text('address_1')->nullable();
            $table->text('address_2')->nullable();
            $table->string('city', 50)->nullable();
            $table->string('post_code', 10)->nullable();
            $table->integer('country')->nullable();
            $table->string('region', 30)->nullable();
            $table->boolean('subscribe')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['fax', 'company', 'address_1', 'address_2', 'city', 'post_code', 'country', 'region', 'subscribe']);
        });
    }
}
