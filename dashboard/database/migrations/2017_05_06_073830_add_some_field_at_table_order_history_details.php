<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomeFieldAtTableOrderHistoryDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_history_details', function (Blueprint $table) {
            $table->text('link_profiler')->nullable();
            $table->text('link_course')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_history_details', function (Blueprint $table) {
            $table->dropColumn(['link_profiler','link_course']);
        });
    }
}
