<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnProductIdOnTableCoachScheduleOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coach_schedule_orders', function (Blueprint $table) {
            $table->integer('product_id')->nullable();
            $table->integer('coach_id')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->time('start_time')->nullable();
            $table->time('end_time')->nullable();
            $table->dropColumn(['order_detail_id']);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coach_schedule_orders', function (Blueprint $table) {
            $table->dropColumn(['product_id','coach_id','start_date','end_date','start_time','end_time']);
            $table->integer('order_detail_id')->nullable();
        });
    }
}
