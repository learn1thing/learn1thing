<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProductQuotas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_quotas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('product_code')->nullable()->default(0);
            $table->integer('min_quota')->nullable()->default(0);
            $table->integer('sold_quota')->nullable()->default(0);
            $table->integer('sold_quota_num')->nullable()->default(0);
            $table->boolean('is_alert')->default(false);

            $table->timestamps();
        });

        Schema::table('products', function (Blueprint $table) {
            $table->integer('time_limit')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_quotas');

        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn(['time_limit']);
        });
    }
}
