<?php

use App\Models\Master\User;
use Illuminate\Database\Seeder;
class UsersTableSeeder extends Seeder
{

    /**
     * Faker Generator
     * @var Faker\Generator
     */
    protected $faker;


    public function __construct(Faker\Generator $faker)
    {
      $this->faker = $faker;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(App\Models\Master\User::class, 1)->create();

        $user = User::create([
            'email' => $this->faker->unique()->safeEmail,
            'password' => bcrypt('password'),
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName
        ]);

        \Activation::create($user)->update(['completed_at' => new \Datetime()]);
    }
}
