<?php

use Illuminate\Database\Seeder;
use App\Models\ExpiredSchedule;

class ExpiredScheduleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('expired_schedules')->truncate();

        $schedule = ExpiredSchedule::create([
            'id' => '0',
            'business_partner_id' => '0',
            'expired' => '72'
        ]);
    }
}
