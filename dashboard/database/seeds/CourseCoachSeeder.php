<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CourseCoachSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('coach_courses')->truncate();
        
        for ($i=0; $i < 30; $i++) { 
            DB::table('coach_courses')->insert([
                    'name' => 'course_'.str_random(3),
                    'user_id' => 2,
                    'description' => 'description_'.str_random(10),
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);
        }
    }
}
