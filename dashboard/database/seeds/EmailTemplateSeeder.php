<?php

use Illuminate\Database\Seeder;

use App\Models\EmailTemplate;

class EmailTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table( 'email_templates' )->truncate();

        $datas = array(
	        		array(
	        			'title' => 'After Change Password', 
					    'slug' => 'changePassword', 
					    'subject' => 'Change Password', 
					    'message' => 'Hallo, try change password'
	        			),
	        		array(
	        			'title' => 'After Bulk Product', 
					    'slug' => 'bulkProduct', 
					    'subject' => 'Bulk Product', 
					    'message' => 'Hallo, try bulk product'
	        			),
	        		array(
	        			'title' => 'After Create User', 
					    'slug' => 'createUser', 
					    'subject' => 'Create User', 
					    'message' => 'Hallo, try create user'
	        			),
	        		array(
	        			'title' => 'Bulk Uploads User', 
					    'slug' => 'storeBulk', 
					    'subject' => 'Uploads User', 
					    'message' => 'Hallo, try Bulk Uploads User'
	        			),
	        		array(
	        			'title' => 'After Request Product', 
					    'slug' => 'requestProduct', 
					    'subject' => 'Request Product', 
					    'message' => 'Hallo, try Request Product'
	        			),
	        		array(
	        			'title' => 'Bulk Uploads Failed User', 
					    'slug' => 'storeBulk', 
					    'subject' => 'Uploads Failed User', 
					    'message' => 'Hallo, try Bulk Uploads Failed User {{ $msg }}'
	        			),
        	);
        	$path = resource_path( "views/template_email/");
        	foreach ($datas as $data) {
        		$email = EmailTemplate::create( $data );
        		if (!file_exists($path)) {
                	mkdir($path,0777, true);
            	}
	            $myfile = fopen($path.$email->slug.".blade.php", "w");
	            fwrite($myfile, $email->message);
	            fclose($myfile);
        	}
    }
}
