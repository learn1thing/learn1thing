<?php

use Illuminate\Database\Seeder;
use App\Models\PaymentMethod;

class PaymentMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // truncate table payment_methods
        PaymentMethod::query()->truncate();

        $paymentMethod = PaymentMethod::firstOrCreate([
                'name' => 'Credit Based System'
            ]);
        
        $paymentMethod = PaymentMethod::firstOrCreate([
                'name' => 'Pay As You Go'
            ]);

    }
}
