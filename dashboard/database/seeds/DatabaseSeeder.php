<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        $this->call(UsersTableSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(UpdateDescriptionPermission::class);
        $this->call(SocialSeeder::class);
        $this->call(CourseCoachSeeder::class);
        $this->call(RegisterPageSeeder::class);
        // $this->call(CountriesTableSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
