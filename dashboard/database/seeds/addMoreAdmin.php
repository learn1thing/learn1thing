<?php

use Illuminate\Database\Seeder;

class addMoreAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $super2 = Sentinel::registerAndActivate([
            'email'         => 'admin1@mailinator.com',
            'password'      => '12345678',
            'first_name'    => 'Super',
            'last_name'     => 'Admin',
        ]);

        $super_admin2 = Sentinel::findRoleBySlug('super-admin');
        $super_admin2->users()->attach($super2);

        $super3 = Sentinel::registerAndActivate([
            'email'         => 'admin2@mailinator.com',
            'password'      => '12345678',
            'first_name'    => 'Super',
            'last_name'     => 'Admin',
        ]);

        $super_admin3 = Sentinel::findRoleBySlug('super-admin');
        $super_admin3->users()->attach($super3);

        $super4 = Sentinel::registerAndActivate([
            'email'         => 'admin3@mailinator.com',
            'password'      => '12345678',
            'first_name'    => 'Super',
            'last_name'     => 'Admin',
        ]);

        $super_admin4 = Sentinel::findRoleBySlug('super-admin');
        $super_admin4->users()->attach($super4);
    }
}
