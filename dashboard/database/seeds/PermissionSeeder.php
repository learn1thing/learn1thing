<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus = array(
            [
                'name' => 'Dashboard',
                'url'  => '/',
                'slug' => 'dashboard',
            ],
            [
                'name' => 'Manage User',
                'url'  => 'admin/master/user',
                'slug' => 'manage-user',
            ],
            [
                'name' => 'Master Permission',
                'url'  => 'admin/master/permission',
                'slug' => 'manage-permission',
            ],
            [
                'name' => 'Setting',
                'url'  => 'admin/setting',
                'slug' => 'setting',
            ],
            [
                'name' => 'Manage Course',
                'url'  => 'admin/coach/course',
                'slug' => 'manage-course',
            ],
            [
                'name' => 'Manage Schedule',
                'url'  => 'admin/coach/scheule',
                'slug' => 'manage-schedule',
            ],
            [
                'name' => 'Bulk User Creation',
                'url'  => 'admin/bulk/',
                'slug' => 'bulk-user-creation',
            ],
            [
                'name' => 'Manage Group User',
                'url'  => 'admin/management-group/group-user',
                'slug' => 'manage-group-user',
            ],
            [
                'name' => 'Manage Group Corporate',
                'url'  => 'admin/management-group-corporate/group-user',
                'slug' => 'manage-group-corporate',
            ],
            [
                'name' => 'Manage Coach',
                'url'  => 'coach/',
                'slug' => 'manage-coach',
            ],
            [
                'name' => 'Manage Product',
                'url'  => 'product/',
                'slug' => 'manage-product',
            ],
            [
                'name' => 'Product Set',
                'url'  => 'product/set-product',
                'slug' => 'product-set',
            ],
            [
                'name' => 'Manage Profile',
                'url'  => 'admin/master/user/profile',
                'slug' => 'manage-profile',
            ],
            [
                'name' => 'Bulk Product',
                'url'  => 'admin/product/bulk-product',
                'slug' => 'bulk-product',
            ],
            [
                'name' => 'Manage Order History',
                'url'  => 'admin/order/order-history',
                'slug' => 'order-history',
            ],
            [
                'name' => 'Manage Department',
                'url'  => 'admin/department',
                'slug' => 'manage-department',
            ],
            [
                'name' => 'Manage Order',
                'url'  => 'admin/order',
                'slug' => 'manage-order',
            ],[
                'name' => 'Bulk Order',
                'url'  => 'admin/order/bulk',
                'slug' => 'bulk-order',
            ],[
                'name' => 'Manage Expired Schedule',
                'url'  => 'master-schedule/expired-schedule',
                'slug' => 'expired-schedule',
            ],[
                'name' => 'Mini Store Corporate',
                'url'  => 'admin/corporate/mini-store/',
                'slug' => 'mini-store',
            ],[
                'name' => 'Top Up Credit',
                'url'  => 'admin/credit/',
                'slug' => 'top-up-credit',
            ]
        );

        foreach ($menus as $menu) {
            Permission::firstOrCreate($menu);
        }
    }
}
