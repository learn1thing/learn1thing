<?php

use Illuminate\Database\Seeder;

class addAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $super2 = Sentinel::registerAndActivate([
            'email'         => 'admin2@l1t.com',
            'password'      => 'password',
            'first_name'    => 'Super',
            'last_name'     => 'Admin 2',
        ]);

        $super_admin2 = Sentinel::findRoleBySlug('super-admin');
        $super_admin2->users()->attach($super2);

        $super3 = Sentinel::registerAndActivate([
            'email'         => 'admin3@l1t.com',
            'password'      => 'password',
            'first_name'    => 'Super',
            'last_name'     => 'Admin 3',
        ]);

        $super_admin3 = Sentinel::findRoleBySlug('super-admin');
        $super_admin3->users()->attach($super3);
    }
}
