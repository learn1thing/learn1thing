<?php

use App\Models\RegisterPage;
use Illuminate\Database\Seeder;

class RegisterPageSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(App\Models\Master\User::class, 1)->create();
        $data = [
                    [
                        'image' => 'images/bg-1.jpeg',
                        'description' => 'I am an individual looking to improve myself through the various learning products.',
                        'title' => 'Individual',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ],
                    [
                        'image' => 'images/bg-3.jpeg',
                        'description' => 'I am in charge of HR and/or L&D for my company, and would be managing the learning for the employees in my organization.',
                        'title' => 'Organization',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ],
                    [
                        'image' => 'images/bg-4.jpeg',
                        'description' => 'I am a training provide or individual that wants to partner and offer my learning products and services to a global audience.',
                        'title' => 'Partners',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ],
                ];
        $user = RegisterPage::insert($data);
    }
}
