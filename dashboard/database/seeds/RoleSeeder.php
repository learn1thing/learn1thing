<?php

use Illuminate\Database\Seeder;
// use Sentinel;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('roles')->truncate();
        // Super Admin
        $role = Sentinel::getRoleRepository()->createModel()->firstOrcreate([
            'name' => 'Super Admin',
            'slug' => 'super-admin',
        ]);

        $role->addPermission('all');
        $role->save();

        // Standard Customer
        $standard_customer = Sentinel::getRoleRepository()->createModel()->firstOrcreate([
            'name' => 'Standard Customer',
            'slug' => 'standard-customer',
        ]);

        $standard_customer->addPermission('dashboard',true);
        $standard_customer->save();

        // Corporate Customer
        $corporate_customer = Sentinel::getRoleRepository()->createModel()->firstOrcreate([
            'name' => 'Corporate Customer',
            'slug' => 'corporate-customer',
        ]);

        $corporate_customer->addPermission('dashboard',true);
        $corporate_customer->save();

        // Business Partner
        $corporate_customer = Sentinel::getRoleRepository()->createModel()->firstOrcreate([
            'name' => 'Business Partner',
            'slug' => 'business-partner',
        ]);

        $corporate_customer->addPermission('dashboard',true);
        $corporate_customer->save();
    }
}
