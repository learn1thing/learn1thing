<?php

use Illuminate\Database\Seeder;
use App\Models\Schedule;

class ScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $schedule = Schedule::create([
            'schedule_date' => '2017-04-01',
            'start_time' => '12:00:00',
            'end_time' => '13:00:00'
        ]);

        $schedule = Schedule::create([
            'schedule_date' => '2017-04-01',
            'start_time' => '14:00:00',
            'end_time' => '15:00:00'
        ]);

        $schedule = Schedule::create([
            'schedule_date' => '2017-04-01',
            'start_time' => '17:00:00',
            'end_time' => '18:00:00'
        ]);

        $schedule = Schedule::create([
            'schedule_date' => '2017-04-01',
            'start_time' => '20:00:00',
            'end_time' => '21:00:00'
        ]);
    }
}
