<?php

use Illuminate\Database\Seeder;

class UpdateDescriptionPermission extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'dashboard' => 'The welcome page',
            'manage-user' => 'Feature for admin to manage user such as create, update, and delete user account information',
            'manage-permission' => 'Feature for admin to set permission/access for user',
            'manage-course' => 'Feature for business partner admin to manage course such as create and delete schedule',
            'manage-schedule' => 'Feature for business partner admin to manage course such as create and delete schedule',
            'bulk-user-creation' => 'Feature for admin to upload user data in bulk',
            'manage-group-user' => 'Feature for business partner admin to manage group user such as create, update, and delete group',
            'manage-group-corporate' => 'Feature for corporate admin to manage group user such as create, update, and delete group',
            'manage-coach' => 'Feature for business partner admin to manage coach such as create, update, and delete coach and set their schedule',
            'manage-product' => 'Feature for business partner to manage product such as create, update, and delete product',
            'product-set' => 'Feature for admin to set product',
            'manage-profile' => 'Feature for users to manage their profile such as edit their information',
            'bulk-product' => 'Feature for business partner admin to upload product data in bulk',
            'order-history' => 'Feature for business partner to manage ordered product such as view order information and change the order status and for corporate to view and access the ordered product',
            'manage-department' => 'Feature for admin to manage department such as create, update, and delete department',
            'manage-order' => 'Feature for admin to manage order',
            'bulk-order' => 'Feature for corporate to order product in bulk',
        ];
        
        foreach ($data as $key => $value) {
            \DB::table('permissions')->where('slug',$key)
                    ->update(['description' => $value]);
        }
    }
}
