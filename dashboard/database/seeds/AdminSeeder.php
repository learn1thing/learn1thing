<?php

use Illuminate\Database\Seeder;
// use Sentinel;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->truncate();
        \DB::table('role_users')->truncate();
        // \Request::request->add(['role'=>1]);

        // Super Admin
        $super = Sentinel::registerAndActivate([
            'email'         => 'admin@l1t.com',
            'password'      => 'password',
            'first_name'    => 'Super',
            'last_name'     => 'Admin',
        ]);

        $super_admin = Sentinel::findRoleBySlug('super-admin');
        $super_admin->users()->attach($super);

        $businesspartner = Sentinel::registerAndActivate([
            'email'         => 'businesspartner@l1t.com',
            'password'      => 'password',
            'first_name'    => 'Business',
            'last_name'     => 'Partner',
        ]);

        $businespartner_user = Sentinel::findRoleBySlug('business-partner');
        $businespartner_user->users()->attach($businesspartner);
    }
}
