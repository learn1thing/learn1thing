<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LoginTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLoginUser()
    {
      $this->json('POST', 'http://mdl.dev/api/v1/auth', ['name' => 'Sally'])
           ->seeJson([
              'status' => 'error',
              'error_messages' => [
                'email' => [
                  'The email field is required.'
                ],
                'password' => [
                  'The password field is required.'
                ]
              ]
           ]);

       $response = $this->call('POST', 'http://mdl.dev/api/v1/auth', ['email' => 'Taylor@gmail.com', 'password' => 'password']);
       $this->assertEquals(401, $response->status());

       $response = $this->call('POST', 'http://mdl.dev/api/v1/auth', ['email' => 'eladio68@example.org', 'password' => 'password']);
       $this->assertEquals(200, $response->status());
    }
}
